IF OBJECT_ID('[SRC].[SRC_ROW_TMP_CMIS]','U') IS NOT NULL
	DROP TABLE [SRC].[SRC_ROW_TMP_CMIS];
GO

CREATE TABLE [SRC].[SRC_ROW_TMP_CMIS](
	[StudentId] [varchar](10) NULL,
	[CourseId] [varchar](25) NULL,
	[AcademicPeriod] [varchar](10) NULL,
	[LinkCode] [varchar](20) NULL,
	[ModuleId] [varchar](25) NULL,
	[InstId] [numeric](38, 0) NULL,
	[LecturerId] [varchar](10) NULL,
	[Name] [varchar](80) NULL,
	[CMISCALC_CourseCode] [varchar](25) NULL,
	[CMISCALC_ModuleId] [varchar](25) NULL,
	[CMISCALC_ModuleInstance] [varchar](8000) NULL,
	[CMISCALC_Semester] [varchar](25) NULL,
	[CMISCALC_AcademicPeriod] [varchar](8000) NULL,
	[CMISCALC_LASTNAME] [varchar](80) NULL,
	[CMISCALC_FIRSTNAME] [varchar](80) NULL,
	[ROW_ADD_TS] [datetime] NOT NULL,
	[rnk] [bigint] NULL
)