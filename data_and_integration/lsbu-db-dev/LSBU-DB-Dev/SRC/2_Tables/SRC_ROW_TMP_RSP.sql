IF OBJECT_ID('[SRC].[SRC_ROW_TMP_RSP]','U') IS NOT NULL
	DROP TABLE [SRC].[SRC_ROW_TMP_RSP];
GO

CREATE TABLE [SRC].[SRC_ROW_TMP_RSP](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[FileDateTime] [datetime] NULL,
	[FileRowNumber] [int] NULL,
	[StudentId] [varchar](255) NULL,
	[SubfolderName] [varchar](255) NULL,
	[Filename] [varchar](255) NULL,
	[Extension] [varchar](255) NULL,
	[Base64Value] [varchar](max) NULL,
	[Column5] [varchar](255) NULL
)