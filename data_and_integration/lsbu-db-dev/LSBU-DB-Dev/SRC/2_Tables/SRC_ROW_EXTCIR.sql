IF OBJECT_ID('[SRC].[SRC_ROW_EXTCIR]','U') IS NOT NULL
	DROP TABLE [SRC].[SRC_ROW_EXTCIR];
GO

CREATE TABLE [SRC].[SRC_ROW_EXTCIR](
	[Academic_Period] [varchar](255) NULL,
	[School] [varchar](255) NULL,
	[Division] [varchar](255) NULL,
	[Course] [varchar](255) NULL,
	[Title] [varchar](255) NULL,
	[Session] [varchar](255) NULL,
	[Level] [varchar](255) NULL,
	[Module] [varchar](255) NULL,
	[Semester] [varchar](255) NULL,
	[Assessment] [varchar](255) NULL,
	[Student_ID] [varchar](255) NULL,
	[Forename] [varchar](255) NULL,
	[Surname] [varchar](255) NULL,
	[Year] [varchar](255) NULL,
	[Stage] [varchar](255) NULL,
	[Ext_Circ] [varchar](255) NULL,
	[Mitigate_Note] [varchar](255) NULL,
	[Internal_Email] [varchar](255) NULL,
	[External_Email] [varchar](255) NULL,
	[file_name] [varchar](255) NULL,
	[Module_Session] [varchar](255) NULL
) ON [PRIMARY]
GO


