CREATE OR ALTER PROCEDURE [SRC].[SRCLoadRunProcess]
        @SRCLoadRunId int output,
		@EtlRunId int,
		@SRCLoadStatus nvarchar(50),
		@SRCLoadDescription nvarchar(250)
AS

		IF (@SRCLoadStatus = 'STARTED' AND @SRCLoadRunId IS NULL)
		BEGIN

		--STARTED,PROGRESS,SUCCESS,ERRORED
		SELECT @SRCLoadRunId = NEXT VALUE FOR [CFG].[SRCLoadID_seq];
    
		INSERT INTO [CFG].[SRCLoadRunLog]
		      ([SRCLoadRunID],
			   [EtlRunId],
			   [SRCLoadStartDateTime],
			   [SRCLoadStatus], --STARTED,PROGRESS,SUCCESS,ERRORED
			   [SRCLoadEndDateTime],
			   [SRCLoadDescription]
				)
		VALUES (@SRCLoadRunId,
	            @EtlRunId,
                getdate(), --@[SRCLoadStartDateTime] datetime2,
	            @SRCLoadStatus,
                NULL,      --@[SRCLoadEndDateTime] datetime2,
	            @SRCLoadDescription
				);

		END;

		--STARTED,PROGRESS,SUCCESS,ERRORED
		IF  @SRCLoadStatus in  ('PROGRESS','SUCCESS','ERRORED') AND @SRCLoadRunId IS NOT NULL
		BEGIN
		UPDATE [CFG].[SRCLoadRunLog]
		SET [SRCLoadEndDateTime] = getdate(), [SRCLoadStatus] = @SRCLoadStatus , [SRCLoadDescription] = @SRCLoadDescription
		WHERE SRCLoadRunID = @SRCLoadRunId;

		END;

RETURN
GO


