CREATE OR ALTER VIEW [SRC].[vSessionIntakeDetails_ParentTerm_Validation] AS
SELECT
  RunID,
  FullFileName,
  SrcSystem,
  ObjectName,
  RowNo,
  FileDateTime,
  LSB_ExternalID,
  FileRowNumber,
  HashMD5,
  QL_AcadProgNameId,
  QL_AOSPeriod,
  QL_Term,
  QL_AcademicPeriod,
  QL_SessionStartDate,
  QL_SessionEndDate,
  QL_TermName,
  QL_TermType,
  TermRank
FROM [STG].[tSessionIntakeDetails_Term]
WHERE QL_TermType = 'AcademicYear'
