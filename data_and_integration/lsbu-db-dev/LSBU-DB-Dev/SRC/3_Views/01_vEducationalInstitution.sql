CREATE OR ALTER VIEW [SRC].[vEducationalInstitution] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	col1 as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14)) as HashMD5,
col1 as QL_EducationInstitutionId,
col2 as QL_EducationInstitutionName,
col3 as QL_Website,
col4 as QL_Phone,
col5 as QL_Add1,
col6 as QL_Add2,
col7 as QL_Add3,
col8 as QL_Add4,
col9 as QL_Add5,
col10 as QL_Postcode,
col11 as QL_Country,
col12 as QL_Description,
col13 as QL_CreatedDate,
col14 as QL_LastModifiedDate
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'EducationalInstitution'
GO