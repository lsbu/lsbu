
CREATE OR ALTER VIEW [SRC].[vStudent_EngagementDetails] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	CONCAT(col1,'.',col2,'.',col4,'.',col5) as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col6, col7, col8)) as HashMD5,
  col1 as Cognos_StudentID,
  col2 as Cognos_AcadProgNameId,
  col3 as Cognos_Term,
  col4 as Cognos_AOSPeriod,
  col5 as Cognos_AcademicPeriod,
  col6 as Cognos_TurnstileActivity,
  col7 as Cognos_LibraryAccessActivity,
  col8 as Cognos_MoodleActivity,
  col9 as Cognos_Submissionssum
FROM [SRC].[SRC_ROW_TMP_COGNOS]
WHERE ObjectName = 'StudentEngagementDetails'
GO


