CREATE OR ALTER VIEW [SRC].[vModuleEnrolmentDetails] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	CONCAT(col1,'.',col2,'.',col3,'.',col4,'.',col6,'.',col7,'.',col12)  as LSB_ExternalID,
	CONCAT(col2,'.',col3,'.',col7) as ModuleOffering_LSB_ExternalID,
	CONCAT(col4,'.',col7,'.', CASE
			WHEN CHARINDEX('NOV_AUG',col5) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col5) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col5) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col5) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col5) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col5) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col5) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col5) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col5) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col5) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col5) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col5) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col5) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col5) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col5) != 0 THEN 'DEC'
		   
		   END) as Term_LSB_ExternalID,
	CONCAT(col1,'.',col4,'.',col6,'.',col7)  as ProgramEnrolment_LSB_ExternalID,
	CONCAT(col1,'.',col4,'.AP.',col9) AS Affiliation_LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col8,col9,col10,col11)) as HashMD5,
  col1 as QL_StudentID,
  col2 as QL_ModuleId,
  col3 as QL_ModuleInstance,
  col4 as QL_AcadProgNameId,
  col5 as QL_Term,  
  col6 as QL_AOSPeriod,
  col7 as QL_AcademicPeriod,
  col8 as QL_EnrollmentStatus,
  col9 as QL_EnrolmentStageIndicator,
  col10 as QL_EnrolStatusDateAchieved,
  col11 as QL_Compulsory,
  col12 as QL_Semester
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'ModuleEnrolment' AND col8 != 'AUF'