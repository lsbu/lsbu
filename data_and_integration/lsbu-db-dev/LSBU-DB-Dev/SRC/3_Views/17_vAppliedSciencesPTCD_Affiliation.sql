CREATE OR ALTER VIEW [SRC].[vAppliedSciencesPTCD_Affiliation] AS
SELECT * FROM (
  SELECT
  AppliedSciencesPTCD.RunID,
  AppliedSciencesPTCD.FullFileName,
  AppliedSciencesPTCD.SrcSystem,
  AppliedSciencesPTCD.ObjectName,
  AppliedSciencesPTCD.RowNo,
  AppliedSciencesPTCD.FileDateTime,
  --CONCAT(QL_StaffId,'.',PTCDCSV_CourseCode) as LSB_ExternalID,	
  CONCAT(Staff.col1,'.',AppliedSciencesPTCD.col9) as LSB_ExternalID,						
  AppliedSciencesPTCD.FileRowNumber,
  HASHBYTES('MD5', CONCAT(Staff.col1, AppliedSciencesPTCD.col9, AppliedSciencesPTCD.col10, AppliedSciencesPTCD.col11, AppliedSciencesPTCD.col12, AppliedSciencesPTCD.col13)) as HashMD5,
  AppliedSciencesPTCD.col9 as PTCDCSV_CourseCode,
  AppliedSciencesPTCD.col10 as PTCDCSV_CourseName,
  Staff.col1 as QL_CDStaffId,
  AppliedSciencesPTCD.col11 as PTCDCSV_CDFirstName,
  AppliedSciencesPTCD.col12 as PTCDCSV_CDLastname,
  AppliedSciencesPTCD.col13 as PTCDCSV_CDEmail,
  /*ROW_NUMBER () OVER ( partition by QL_StaffId
  ORDER BY QL_StaffId
  ) StaffRank*/
  ROW_NUMBER() OVER (PARTITION BY Staff.col1,AppliedSciencesPTCD.col9 ORDER BY Staff.col1,AppliedSciencesPTCD.col9) StaffRank
  --Distict Records based on the Staff id
  FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD, [SRC].[SRC_ROW_TMP] Staff
  WHERE AppliedSciencesPTCD.col13 = Staff.col3
    AND Staff.ObjectName = 'Staff'
    AND AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD'
) AS SRC_Affiliation where StaffRank = 1
GO