CREATE OR ALTER VIEW [SRC].[vApplicationDetails_Application] AS
SELECT
  RunID,
  FullFileName,
  SrcSystem,
  ObjectName,
  RowNo,
  FileDateTime,
  --CONCAT(QL_StudentID,QL_AcadProgNameId,QL_AOSPeriod,QL_AcademicPeriod)
  CONCAT(col1,'.',col6,'.',col18,'.',col10) as LSB_ExternalID,
  CONCAT(col6,'.',col10,'.', CASE
			WHEN CHARINDEX('NOV_AUG',col17) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col17) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col17) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col17) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col17) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col17) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col17) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col17) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col17) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col17) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col17) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col17) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col17) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col17) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col17) != 0 THEN 'DEC'
		   END) as Term_LSB_ExternalID,
  FileRowNumber,
  HASHBYTES('MD5',CONCAT(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, col41, col42)) as HashMD5,
  col1 as QL_StudentID,
  col2 as QL_UCASPersonalID,
  col3 as QL_UCASApplicationCode,
  col4 as QL_UCASApplicationNumber,
  col5 as QL_ApplicationDate,
  col6 as QL_AcadProgNameId,
  col7 as QL_IsItPrimaryAcademicProg,
  col8 as QL_ApplicationStatus,
  col9 as QL_ApplicationType,
  col10 as QL_AcademicPeriod,
  col11 as QL_ProgramStartDate,
  col12 as QL_ProgramEndDate,
  col13 as QL_ModeOfStudy,
  col14 as QL_CreatedDate,
  col15 as QL_LastModifiedDate,
  col16 as QL_ApplicationSource,
  col17 as QL_Term,
  col18 as QL_AOSPeriod,
  col19 as QL_DepositStatus,
  col20 as QL_VisaStatus,
  col21 as QL_ApplicantAgentCode,
  col22 as QL_ApplicantAgencyName,
  col23 as QL_PreviousInstituteName,
  col24 as QL_UCASCourseCode,
  col25 as QL_ApplicantStageIndicator,
  col26 as QL_CriminalConvictions,
  col27 as QL_VisaType,
  col28 as QL_NominatedName,
  col29 as QL_NominatedRelationship,
  col30 as QL_AttedenceType,
  col31 as QL_SessionStartDate,
  col32 as QL_SessionEndDate,
  col33 as QL_ChoiceNumber,
  col34 as QL_CampusInformation,
  col35 as QL_AppStatusDateAchieved,
  col36 as QL_QualificationCheck,
  col37 as QL_DecisionStatus,
  col38 as QL_DecisionNotes,
  col39 as QL_DecisionDate,
  col40 as QL_QualCode,
  col41 as QL_UCASStatus,
  col42 as QL_OfferText,
  col43 as QL_CountryofBirth,
  col44 as QL_FeeCode,
  col45 as QL_Liveathome,
  col46 as QL_LateUcasApplicant,
  col47 as QL_DSDecisionstatus,
  col48 as QL_InternationalFunding,
  col49 as QL_CasNo,
  col50 as QL_CasStatus,
  col51 as QL_TotalDiscount,
  col52 as QL_VisaRequired
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'ApplicationDetails'