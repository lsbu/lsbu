CREATE OR ALTER VIEW [SRC].[vGrade] AS
SELECT
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(QL_Term,QL_OverallCourseMarks,QL_OverallCourseGrade,QL_OverallModuleMarks,QL_OverallModuleGrade,CW1,CW2,CW3,CW4,CW5,CW6,EX1,EX2,EX3,EX4,EX5)) as HashMD5,
	QL_StudentID,
	QL_ModuleId,
	QL_ModuleInstance,
	QL_AcadProgNameId,
	QL_Term,
	QL_AOSPeriod,
	QL_AcademicPeriod,
	QL_Semester,
	QL_Attempt,
	QL_OverallCourseMarks,
	QL_OverallCourseGrade,
	QL_OverallModuleMarks,
	QL_OverallModuleGrade,
	CW1,
	CW2,
	CW3,
	CW4,
	CW5,
	CW6,
	EX1,
	EX2,
	EX3,
	EX4,
	EX5,
	CONCAT(QL_AcadProgNameId,'.',QL_AcademicPeriod,'.',
		CASE
			WHEN CHARINDEX('NOV_AUG',QL_Term) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',QL_Term) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',QL_Term) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',QL_Term) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',QL_Term) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',QL_Term) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',QL_Term) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',QL_Term) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',QL_Term) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',QL_Term) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',QL_Term) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',QL_Term) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',QL_Term) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',QL_Term) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',QL_Term) != 0 THEN 'DEC'
		   
		   END) as Term_LSB_ExternalID,
	CONCAT(QL_StudentID,'.',QL_ModuleId,'.',QL_ModuleInstance,'.',QL_AcadProgNameId,'.',QL_AOSPeriod,'.',QL_AcademicPeriod,'.',QL_Semester) as ModuleEnrolment_LSB_ExternalID,
	CONCAT(QL_ModuleId,'.',QL_ModuleInstance,'.',QL_AcademicPeriod) as ModuleOffering_LSB_ExternalID,
	CONCAT(QL_StudentID,'.',QL_AcadProgNameId,'.',QL_AOSPeriod,'.',QL_AcademicPeriod) as ProgramEnrolment_LSB_ExternalID
FROM (
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) ORDER BY CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9)) as rnum,
		S.RunID,
		S.FullFileName,
		S.SrcSystem,
		S.ObjectName,
		S.RowNo,
		S.FileDateTime,
		CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) as LSB_ExternalID,
		S.FileRowNumber,
		S.col1 as QL_StudentID,
		S.col2 as QL_ModuleId,
		S.col3 as QL_ModuleInstance,
		S.col4 as QL_AcadProgNameId,
		S.col5 as QL_Term,
		S.col6 as QL_AOSPeriod,
		S.col7 as QL_AcademicPeriod,
		S.col8 as QL_Semester,
		S.col9 as QL_Attempt,
		S.col10 as QL_OverallCourseMarks,
		S.col11 as QL_OverallCourseGrade,
		S.col12 as QL_OverallModuleMarks,
		S.col13 as QL_OverallModuleGrade,
		S1.col15 as CW1,
		S2.col15 as CW2,
		S3.col15 as CW3,
		S4.col15 as CW4,
		S5.col15 as CW5,
		S6.col15 as CW6,
		S7.col15 as EX1,
		S8.col15 as EX2,
		S9.col15 as EX3,
		S10.col15 as EX4,
		S11.col15 as EX5
	FROM [SRC].[SRC_ROW_TMP] S
	LEFT JOIN [SRC].[SRC_ROW_TMP] S1 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S1.col1,'.',S1.col2,'.',S1.col3,'.',S1.col4,'.',S1.col6,'.',S1.col7,'.',S1.col8,'.',S1.col9) AND S1.col14 = 'CW1'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S2 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S2.col1,'.',S2.col2,'.',S2.col3,'.',S2.col4,'.',S2.col6,'.',S2.col7,'.',S2.col8,'.',S2.col9) AND S2.col14 = 'CW2'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S3 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S3.col1,'.',S3.col2,'.',S3.col3,'.',S3.col4,'.',S3.col6,'.',S3.col7,'.',S3.col8,'.',S3.col9) AND S3.col14 = 'CW3'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S4 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S4.col1,'.',S4.col2,'.',S4.col3,'.',S4.col4,'.',S4.col6,'.',S4.col7,'.',S4.col8,'.',S4.col9) AND S4.col14 = 'CW4'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S5 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S5.col1,'.',S5.col2,'.',S5.col3,'.',S5.col4,'.',S5.col6,'.',S5.col7,'.',S5.col8,'.',S5.col9) AND S5.col14 = 'CW5'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S6 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S6.col1,'.',S6.col2,'.',S6.col3,'.',S6.col4,'.',S6.col6,'.',S6.col7,'.',S6.col8,'.',S6.col9) AND S6.col14 = 'CW6'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S7 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S7.col1,'.',S7.col2,'.',S7.col3,'.',S7.col4,'.',S7.col6,'.',S7.col7,'.',S7.col8,'.',S7.col9) AND S7.col14 = 'EX1'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S8 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S8.col1,'.',S8.col2,'.',S8.col3,'.',S8.col4,'.',S8.col6,'.',S8.col7,'.',S8.col8,'.',S8.col9) AND S8.col14 = 'EX2'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S9 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S9.col1,'.',S9.col2,'.',S9.col3,'.',S9.col4,'.',S9.col6,'.',S9.col7,'.',S9.col8,'.',S9.col9) AND S9.col14 = 'EX3'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S10 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S10.col1,'.',S10.col2,'.',S10.col3,'.',S10.col4,'.',S10.col6,'.',S10.col7,'.',S10.col8,'.',S10.col9) AND S10.col14 = 'EX4'
	LEFT JOIN [SRC].[SRC_ROW_TMP] S11 ON CONCAT(S.col1,'.',S.col2,'.',S.col3,'.',S.col4,'.',S.col6,'.',S.col7,'.',S.col8,'.',S.col9) = CONCAT(S11.col1,'.',S11.col2,'.',S11.col3,'.',S11.col4,'.',S11.col6,'.',S11.col7,'.',S11.col8,'.',S11.col9) AND S11.col14 = 'EX5'
	WHERE S.ObjectName = 'Grade') as T1
WHERE rnum = 1