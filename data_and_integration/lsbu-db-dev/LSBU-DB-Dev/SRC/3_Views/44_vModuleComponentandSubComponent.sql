
CREATE OR ALTER    VIEW [SRC].[vModuleComponentandSubComponent] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	'UNIQUE EXTERNAL ID' as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12)) as HashMD5,
  col1 as QL_ModuleId,
  col3 as QL_ModuleInstance,
  col4 as QL_AcademicPeriod,
  col5 as QL_ModuleStartDate,
  col6 as QL_ModuleEndDate,
  col7 as QL_ComponentId,
  col8 as QL_ComponentDesc,
  col9 as QL_ComponentHandinDate,
  col10 as QL_SubComponentId,
  col11 as QL_SubComponentDesc,
  col12 as QL_SubComponentHandinDate

FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'ModuleComponentandSubComponent'
GO


