CREATE OR ALTER VIEW [SRC].[vAppliedSciencesPTCD_CaseTeamMember] AS
SELECT * FROM
(
SELECT
AppliedSciencesPTCD.RunID,
AppliedSciencesPTCD.FullFileName,
AppliedSciencesPTCD.SrcSystem,
AppliedSciencesPTCD.ObjectName,
AppliedSciencesPTCD.RowNo,
AppliedSciencesPTCD.FileDateTime,
--CONCAT(QL_StaffId,'.',QL_StudentId) as LSB_ExternalID,	
CONCAT(Staff.col1,'.',AppliedSciencesPTCD.col1) as LSB_ExternalID,						
AppliedSciencesPTCD.FileRowNumber,
NULL as HashMD5,
AppliedSciencesPTCD.col1 as PTCDCSV_StudentId,
AppliedSciencesPTCD.col2 as PTCDCSV_StudentName,
AppliedSciencesPTCD.col3 as PTCDCSV_StudentEmailAddress,
AppliedSciencesPTCD.col4 as PTCDCSV_YearofStudy,
AppliedSciencesPTCD.col5 as PTCDCSV_Session,
Staff.col1 as QL_PersonalTutorId,
AppliedSciencesPTCD.col6 as PTCDCSV_PersonalTutorFirstName,
AppliedSciencesPTCD.col7 as PTCDCSV_PersonalTutorLastName,
AppliedSciencesPTCD.col8 as PTCDCSV_PersonalTutorEmailAddress,
NULL as QL_CDStaffId,
NULL as PTCDCSV_CDFirstName,
NULL as PTCDCSV_CDLastname,
NULL as PTCDCSV_CDEmail,
NULL as QL_MLStaffId,
NULL as PTCDCSV_MLFirstName,
NULL as PTCDCSV_MLLastname,
NULL as PTCDCSV_MLEmail,
/*ROW_NUMBER () OVER ( partition by QL_StaffId
ORDER BY QL_StaffId
) StaffRank*/
ROW_NUMBER () OVER ( partition by Staff.col1,AppliedSciencesPTCD.col1
ORDER BY Staff.col1,AppliedSciencesPTCD.col1
) StaffRank 											--Distict Records based on the Staff id
FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD, [SRC].[SRC_ROW_TMP] Staff
WHERE AppliedSciencesPTCD.col8 = Staff.col3
AND Staff.ObjectName = 'Staff'
AND AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD'

) as SRC_CaseTeamMember where StaffRank = 1
UNION ALL

SELECT * FROM
(
SELECT
AppliedSciencesPTCD.RunID,
AppliedSciencesPTCD.FullFileName,
AppliedSciencesPTCD.SrcSystem,
AppliedSciencesPTCD.ObjectName,
AppliedSciencesPTCD.RowNo,
AppliedSciencesPTCD.FileDateTime,
--CONCAT(QL_StaffId,'.',QL_StudentId) as LSB_ExternalID,	
CONCAT(Staff.col1,'.',AppliedSciencesPTCD.col1) as LSB_ExternalID,						
AppliedSciencesPTCD.FileRowNumber,
NULL as HashMD5,
AppliedSciencesPTCD.col1 as PTCDCSV_StudentId,
AppliedSciencesPTCD.col2 as PTCDCSV_StudentName,
AppliedSciencesPTCD.col3 as PTCDCSV_StudentEmailAddress,
AppliedSciencesPTCD.col4 as PTCDCSV_YearofStudy,
AppliedSciencesPTCD.col5 as PTCDCSV_Session,
NULL as QL_PersonalTutorId,
NULL as PTCDCSV_PersonalTutorFirstName,
NULL as PTCDCSV_PersonalTutorLastName,
NULL as PTCDCSV_PersonalTutorEmailAddress,
Staff.col1 as QL_CDStaffId,
AppliedSciencesPTCD.col11 as PTCDCSV_CDFirstName,
AppliedSciencesPTCD.col12 as PTCDCSV_CDLastname,
AppliedSciencesPTCD.col13 as PTCDCSV_CDEmail,
NULL as QL_MLStaffId,
NULL as PTCDCSV_MLFirstName,
NULL as PTCDCSV_MLLastname,
NULL as PTCDCSV_MLEmail,
/*ROW_NUMBER () OVER ( partition by QL_StaffId
ORDER BY QL_StaffId
) StaffRank*/
ROW_NUMBER () OVER ( partition by Staff.col1,AppliedSciencesPTCD.col1
ORDER BY Staff.col1,AppliedSciencesPTCD.col1
) StaffRank 											--Distict Records based on the Staff id
FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD, [SRC].[SRC_ROW_TMP] Staff
WHERE AppliedSciencesPTCD.col13 = Staff.col3
AND Staff.ObjectName = 'Staff'
AND AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD'

) as SRC_CaseTeamMember where StaffRank = 1

UNION ALL

SELECT * FROM
(
SELECT
AppliedSciencesPTCD.RunID,
AppliedSciencesPTCD.FullFileName,
AppliedSciencesPTCD.SrcSystem,
AppliedSciencesPTCD.ObjectName,
AppliedSciencesPTCD.RowNo,
AppliedSciencesPTCD.FileDateTime,
--CONCAT(QL_StaffId,'.',QL_StudentId) as LSB_ExternalID,	
CONCAT(Staff.col1,'.',AppliedSciencesPTCD.col1) as LSB_ExternalID,						
AppliedSciencesPTCD.FileRowNumber,
NULL as HashMD5,
AppliedSciencesPTCD.col1 as PTCDCSV_StudentId,
AppliedSciencesPTCD.col2 as PTCDCSV_StudentName,
AppliedSciencesPTCD.col3 as PTCDCSV_StudentEmailAddress,
AppliedSciencesPTCD.col4 as PTCDCSV_YearofStudy,
AppliedSciencesPTCD.col5 as PTCDCSV_Session,
NULL as QL_PersonalTutorId,
NULL as PTCDCSV_PersonalTutorFirstName,
NULL as PTCDCSV_PersonalTutorLastName,
NULL as PTCDCSV_PersonalTutorEmailAddress,
NULL as QL_CDStaffId,
NULL as PTCDCSV_CDFirstName,
NULL as PTCDCSV_CDLastname,
NULL as PTCDCSV_CDEmail,
Staff.col1 as QL_MLStaffId,
AppliedSciencesML.col5 as PTCDCSV_MLFirstName,
AppliedSciencesML.col6 as PTCDCSV_MLLastname,
AppliedSciencesML.col7 as PTCDCSV_MLEmail,
/*ROW_NUMBER () OVER ( partition by QL_StaffId
ORDER BY QL_StaffId
) StaffRank*/
ROW_NUMBER () OVER ( partition by Staff.col1,AppliedSciencesPTCD.col1
ORDER BY Staff.col1,AppliedSciencesPTCD.col1
) StaffRank 											--Distict Records based on the Staff id
FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD, [SRC].[SRC_ROW_TMP] Staff, [SRC].[SRC_ROW_TMP] AppliedSciencesML -- New File added
WHERE AppliedSciencesML.col7 = Staff.col3
AND AppliedSciencesPTCD.col5 = AppliedSciencesML.col4 -- Session
and AppliedSciencesPTCD.col9 = AppliedSciencesML.col2 -- Course Code
AND AppliedSciencesML.ObjectName = 'AppliedSciencesML'
AND Staff.ObjectName = 'Staff'
AND AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD'

) as SRC_CaseTeamMember where StaffRank = 1
GO