CREATE OR ALTER VIEW [SRC].[vExetenuatingCirc] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	'UNIQUE EXTERNAL ID' as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12)) as HashMD5,
  col1 as QL_StudentID,
  col2 as QL_ModuleId,
  col3 as QL_ModuleInstance,
  col4 as QL_AcadProgNameId,
  col5 as QL_Term,
  col6 as QL_AOSPeriod,
  col7 as QL_AcademicPeriod,
  col8 as QL_Semester,
  col9 as QL_Attempt,
  col10 as QL_ComponentType,
  col11 as QL_ECStatusFlag,
  col12 as QL_ECStatusFlagDate

FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'ExetenuatingCircumstances'