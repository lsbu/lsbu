CREATE OR ALTER VIEW [SRC].[vCourseDetails] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	NULL as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1, col2, col3)) as HashMD5,
col1 as QL_CourseId,
col2 as QL_DepartmentId,
col3 as QL_CourseType
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'CourseDetails'

GO