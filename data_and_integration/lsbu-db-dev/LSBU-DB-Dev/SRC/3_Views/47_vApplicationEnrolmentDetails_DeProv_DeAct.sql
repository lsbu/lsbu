CREATE OR ALTER  VIEW [SRC].[vApplicationEnrolmentDetails_DeProv_DeAct] AS
	SELECT
			M.*,  
			HASHBYTES('MD5',CONCAT(M.QL_AcademicPeriod,
			M.QL_AcadProgNameId,
			M.QLCALC_DeProvisioningdate,
			M.QLCALC_DeProvisionedStatus)
			) AS HashMD5
	FROM 
	(
			SELECT
					T.RunID,
					T.FullFileName,
					T.SrcSystem,
					T.ObjectName,
					T.RowNo,
					T.FileDateTime,
					T.FileRowNumber,
					T.SF_UserID, 
					T.QL_StudentID,
					T.QLCALC_ContactRole,
					T.QL_AcademicPeriod,
					T.QL_AcadProgNameId,
					T.QL_ApplicationStatus,
					T.QL_AppStatusDateAchieved,
					T.QL_EnrollmentStatus,
					T.QL_EnrolStatusDateAchieved,
					T.QL_StudentGraduated,
					GETDATE() AS QLCALC_RowUpsertedDate,
					CASE 
						   WHEN T.QL_EnrollmentStatus= 'EAWD' THEN DATEADD(day, 30, CONVERT(date, T.QL_EnrolStatusDateAchieved, 103))
						   ELSE DATEADD(day, 30, CONVERT(date, T.QL_AppStatusDateAchieved, 103))
					END AS QLCALC_DeProvisioningdate,
					CASE
						WHEN A.QL_StudentID IS NOT NULL THEN 'Not To be Done'
						ELSE 'To be Done'
					END AS QLCALC_DeProvisionedStatus,
					NULL AS QLCALC_DeProvisionedStatusDate,
					T.QL_StudentID LSB_ExternalID
					FROM 
					(
						SELECT
							AEDA.RunID,
							AEDA.FullFileName,
							AEDA.SrcSystem,
							AEDA.ObjectName,
							AEDA.RowNo,
							AEDA.FileDateTime,
							AEDA.FileRowNumber,
							USR.ID SF_UserID, 
							AEDA.QL_StudentID,
							AEDA.QLCALC_ContactRole,
							AEDA.QL_AcademicPeriod,
							AEDA.QL_AcadProgNameId,
							AEDA.QL_ApplicationStatus,
							AEDA.QL_AppStatusDateAchieved,
							AEDA.QL_EnrollmentStatus,
							AEDA.QL_EnrolStatusDateAchieved,
							AEDA.QL_StudentGraduated,
							ROW_NUMBER () OVER (PARTITION BY AEDA.QL_StudentID ORDER BY 
							CASE 
								WHEN AEDA.QL_EnrollmentStatus is not null and AEDA.QL_EnrollmentStatus= 'EAWD' THEN AEDA.QL_EnrolStatusDateAchieved
								ELSE AEDA.QL_AppStatusDateAchieved  
							END DESC) AS ApplicationRank
							FROM [SRC].[vApplicationEnrolmentDetails_Affiliation] AEDA
							INNER JOIN [SFC].[Get_ID_User] USR ON AEDA.QL_StudentID= USR.LSB_UER_ExternalID__c AND (USR.IsActive= 1 AND DB_NAME() = 'diprod' OR DB_NAME() = 'didev')
							INNER JOIN [SFC].[Get_ID_Contact] C ON AEDA.QL_StudentID = C.LSB_ExternalID__c AND C.LSB_CON_ContactRole__c = 'Applicant' and USR.ContactId = c.Id
							WHERE AEDA.QLCALC_AffiliationRole = 'Applicant'
							--AND AEDA.QLCALC_AffiliationPrimaryFlag = 1
							AND AEDA.QL_AcadProgNameId IS NOT NULL
							AND 
							(
									AEDA.QL_ApplicationStatus in ('AARC','ADUP','ACD','ADBD','ADEC','ARAI','ARBD','ARBI','AREJ','AUD','AWD','AWDX')
									OR
									AEDA.QL_EnrollmentStatus in ('EAWD')

							)
			) T 
			LEFT JOIN [SRC].[vApplicationEnrolmentDetails_Affiliation] A ON A.QL_StudentID = T.QL_STUDENTID AND (A.QL_ApplicationStatus IN ('AACPT','AUF','AUI','ACF','ACI')  OR A.[QL_EnrollmentStatus] in ('EASS'))
			AND A.QLCALC_AffiliationRole = 'Applicant'
			WHERE T.ApplicationRank = 1

	) M 
