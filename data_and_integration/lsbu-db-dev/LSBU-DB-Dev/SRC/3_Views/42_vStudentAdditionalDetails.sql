
CREATE OR ALTER VIEW [SRC].[vStudentAdditionalDetails] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	'UNIQUE EXTERNAL ID' as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9)) as HashMD5,
  col1 as QL_StudentID,
  col2 as QL_RefugeeStatus,
  col3 as QL_CaringResponsibilities,
  col4 as QL_BeeninCare,
  col5 as QL_EnstrangedfromFamily,
  col6 as QL_AcadProgNameId,
  col7 as QL_Term,
  col8 as QL_AOSPeriod,
  col9 as QL_AcademicPeriod

FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'StudentAdditionalDetails'
GO


