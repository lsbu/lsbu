
CREATE OR ALTER    VIEW [SRC].[vModuleSubComponentEnrolmentDetails] AS
	SELECT 
	C.RunID,
	C.FullFileName,
	C.SrcSystem,
	C.ObjectName,
	C.RowNo,
	C.FileDateTime,
	Concat(MED.LSB_ExternalID,'.',C.QL_ComponentId,C.QL_SubComponentId) as LSB_ExternalID,
	Concat(MED.LSB_ExternalID,'.',C.QL_ComponentId) as ModuleComponent_LSB_ExternalID,
	MED.LSB_ExternalID AS ModuleEnrolment_LSB_ExternalID,
	MED.Term_LSB_ExternalID as Term_LSB_ExternalID,
	C.FileRowNumber,
	HASHBYTES('MD5',CONCAT(C.QL_ModuleStartDate,C.QL_ModuleEndDate,C.QL_SubComponentDesc,C.QL_SubComponentHandinDate)) as HashMD5,
	MED.QL_StudentID as QL_StudentId,
	C.QL_ModuleId,
	C.QL_ModuleInstance,
	C.QL_AcademicPeriod,
	C.QL_ModuleStartDate,
	C.QL_ModuleEndDate,
	C.QL_ComponentId,
	C.QL_ComponentDesc,
	C.QL_ComponentHandinDate,
	C.QL_SubComponentId,
	C.QL_SubComponentDesc,
	C.QL_SubComponentHandinDate
	FROM
	(
			SELECT 
			RunID,
			FullFileName,
			SrcSystem,
			ObjectName,
			RowNo,
			FileDateTime,
			FileRowNumber,
			col1 as QL_ModuleId,
			col2 as QL_ModuleInstance,
			col3 as QL_AcademicPeriod,
			col4 as QL_ModuleStartDate,
			col5 as QL_ModuleEndDate,
			col6 as QL_ComponentId,
			col7 as QL_ComponentDesc,
			col8 as QL_ComponentHandinDate,
			col9 as QL_SubComponentId,
			col10 as QL_SubComponentDesc,
			col11 as QL_SubComponentHandinDate,
			ROW_NUMBER () OVER (PARTITION BY col1,col2,col3,col6,col9  ORDER BY col1,col2,col3,col6,col9) AS CRank
			FROM [SRC].[SRC_ROW_TMP] 
			WHERE ObjectName = 'ModuleComponentandSubComponent' and col6 is not null and col9 is not null
	) C
	INNER JOIN [SRC].[vModuleEnrolmentDetails] MED ON MED.QL_ModuleId = C.QL_ModuleId 
	AND MED.QL_ModuleInstance = C.QL_ModuleInstance AND MED.QL_AcademicPeriod = C.QL_AcademicPeriod
	WHERE C.CRank = 1

GO


