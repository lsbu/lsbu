CREATE OR ALTER VIEW [SRC].[vUser] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	col1 as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15,col16,col17,col18,col19,col20,col21,col22,col23,col24,col25,col26,col27,col28,col29)) as HashMD5,
	col1 as QL_StudentID,
	col2 as QL_UCASPersonalID,
	col3 as QL_Title,
	col4 as QL_Initials,
	col5 as QL_FirstName,
	col6 as QL_FamiliarName,
	col7 as QL_Surname,
	col8 as QL_MiddleName,
	col9 as QL_Birthdate,
	col10 as QL_StudentStatus,
	col11 as QL_Gender,
	col12 as QL_Domicile,
	col13 as QL_Origin,
	col14 as QL_Nationality,
	col15 as QL_Disabilty,
	col16 as QL_Add1,
	col17 as QL_Add2,
	col18 as QL_Add3,
	col19 as QL_Add4,
	col20 as QL_Add5,
	col21 as QL_Postcode,
	col22 as QL_Country,
	col23 as QL_AddressType,
	col24 as QL_Phone,
	col25 as QL_LSBUEmail,
	col26 as QL_PersonalEmail,
	col27 as QL_MobileNo,
	col28 as QL_CreatedDate,
	col29 as QL_LastModifiedDate
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'OfferHolder'
GO