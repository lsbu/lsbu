CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_IndividualDetailListMulti] AS
SELECT DISTINCT
I.IndividualId,
I1.Value as [ARCHIVED_FIELDS_Archive_Box_No],
I2.Value as [ARCHIVED_FIELDS_Archive_year],
I3.Value as [ARCHIVED_FIELDS_Circulation__UDF_no_longer_used_],
I4.Value as [ARCHIVED_FIELDS_Disability_Duration],
I5.Value as [ARCHIVED_FIELDS_DSA_Year],
I6.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs__for_ALF__Type_of_Student],
I7.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs__for_ALF__Who_pays_fees],
I8.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs__for_ALF__Year_Of_Course],
I9.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Student_to_complete_ALF],
I10.Value as [ARCHIVED_FIELDS_Lockers_Locker_Allocated__Number_],
I11.Value as [ARCHIVED_FIELDS_Lockers_Locker_Waiting_List],
I12.Value as [ARCHIVED_FIELDS_Lockers_Locker],
I13.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_Home_or_International],
I14.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_Pre_Entry___Support_Worker_required],
I15.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_SA_signed],
I16.Value as [ARCHIVED_FIELDS_Old_fields_Support_Recommended],
I17.Value as [ARCHIVED_FIELDS_Old_fields_Support_worker_2012_2013],
I18.Value as [ARCHIVED_FIELDS_Old_fields_Support_Worker],
I19.Value as [ARCHIVED_FIELDS_PEEPs_2010_PEEP_Status],
I20.Value as [ARCHIVED_FIELDS_Post_Reg____No_Placement],
I21.Value as [ARCHIVED_FIELDS_Progress],
I22.Value as [ARCHIVED_FIELDS_SpLD_Marking_Policy],
I23.Value as [ARCHIVED_FIELDS_Student_in_Receipt_of_DSA],
I24.Value as [ARCHIVED_FIELDS_Temporary_Only],
I25.Value as [ARCHIVED_FIELDS_Triage_Call_info_MHWB_Appt_required],
I26.Value as [ARCHIVED_FIELDS_Triage_Call_info_Safety_concerns],
I27.Value as [ARCHIVED_FIELDS_Triage_Dates_triage_call_1_successful],
I28.Value as [ARCHIVED_FIELDS_Triage_Dates_triage_call_2_successful],
I29.Value as [ARCHIVED_FIELDS_Triage_Dates_triage_call_3_successful],
I30.Value as [Corona_query],
I31.Value as [Covid_19_Fields_Accommodation_Accommodation_Does_student_plan_to_live_in_LSBU_student_accomm],
I32.Value as [Covid_19_Fields_Accommodation_Accommodation_How_many_bedrooms_will_there_be_in_students_flat],
I33.Value as [Covid_19_Fields_Accommodation_Accommodation_Which_halls_of_residence_will_student_be_living_in],
I34.Value as [Covid_19_Fields_Accommodation_Accommodation_Will_student_be_a_Residential_Life_Ambassador__RLA_],
I35.Value as [Covid_19_Fields_Adjustments_and_interventions_Adjustments_proposed],
I36.Value as [Covid_19_Fields_Adjustments_and_interventions_Health__emotional_support__disability_team_],
I37.Value as [Covid_19_Fields_Adjustments_and_interventions_Student_accommodation_adjustments],
I38.Value as [Covid_19_Fields_Adjustments_and_interventions_Unable_to_attend___attendance_not_required_by_course_Home_or_EU_student],
I39.Value as [Covid_19_Fields_Adjustments_and_interventions_Unable_to_attend___attendance_required_by_course_Home_EU_and_Tier_4_students],
I40.Value as [Covid_19_Fields_Advisor],
I41.Value as [Covid_19_Fields_Circulated_to],
I42.Value as [Covid_19_Fields_Coronavirus_questions_Has_student_had_coronavirus],
I43.Value as [Covid_19_Fields_Covid_plan_created],
I44.Value as [Covid_19_Fields_Existing_DDS_support_Does_student_already_have_SAF_agreed_with_DDS],
I45.Value as [Covid_19_Fields_International_student_Is_student_an_international_student_studying_on_a_T4_visa],
I46.Value as [Covid_19_Fields_Referred_to],
I47.Value as [Covid_19_Fields_Risk_information_High_risk_detail],
I48.Value as [Covid_19_Fields_Risk_information_Moderate_risk_detail],
I49.Value as [Covid_19_Fields_Risk_information_Risk_category],
I50.Value as [Covid_19_Fields_Travel_How_will_student_be_travelling_to_campus],
I51.Value as [DDS_Consent_to_Share__DDS__Permission_Given_Method],
I52.Value as [DDS_DSA_BSL_Interpreter_BSL_Interpreter_Recommended],
I53.Value as [DDS_DSA_BSL_Interpreter_BSL_Supplier],
I54.Value as [DDS_DSA_DSA_Funded_By],
I55.Value as [DDS_DSA_Mentoring_Support_Mentor],
I56.Value as [DDS_DSA_Mentoring_Support_Mentor_Support_Total_Hours],
I57.Value as [DDS_DSA_Mentoring_Support_Mentoring_Type],
I58.Value as [DDS_DSA_Note_Taking_Note_taker_recommended],
I59.Value as [DDS_DSA_Specialist_Tutor_Specialist_Tutor_Recommended],
I60.Value as [DDS_DSA_Student_will_exceed_DSA_funding],
I61.Value as [DDS_DSA_Study_Skills_Tuition_Dyslexia_Tuition_Type],
I62.Value as [DDS_DSA_Study_Skills_Tuition_Dyslexia_Tutor],
I63.Value as [DDS_DSA_Study_Skills_Tuition_Total_Interim_Hours_Used],
I64.Value as [DDS_DSA_Study_Skills_Tuition_Tutor_Support_Total_Hours],
I65.Value as [DDS_DSA_Updated_QL__DSA_field_],
I66.Value as [DDS_Lockers_Lockers_Locker_Agreement_Signed],
I67.Value as [DDS_LSBU_funded_NMH_BSL_Interpreter_BSL_Interpreter_Recommended],
I68.Value as [DDS_LSBU_funded_NMH_Mentoring_Support__LSBU__Mentor_Recommended],
I69.Value as [DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taker_Recommended],
I70.Value as [DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_Supplier],
I71.Value as [DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Hours_Agreed],
I72.Value as [DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Recommended],
I73.Value as [DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Supplier],
I74.Value as [DDS_PRE_ENTRY_DP_Form_signed],
I75.Value as [DDS_PRE_ENTRY_Enrolment_Support_Required],
I76.Value as [DDS_PRE_ENTRY_Med_ev_received],
I77.Value as [DDS_PRE_ENTRY_Pre_entry_status],
I78.Value as [DDS_PRE_ENTRY_SAF_agreed],
I79.Value as [DDS_PRE_ENTRY_Support_declined],
I80.Value as [DDS_Screening_and_Ed_Psych_Assessment_EP_Assessment_Result],
I81.Value as [DDS_Screening_and_Ed_Psych_Assessment_EP_Assessor],
I82.Value as [DDS_Screening_and_Ed_Psych_Assessment_EP_Room],
I83.Value as [DDS_Screening_and_Ed_Psych_Assessment_SpLD_screening_outcome],
I84.Value as [DDS_Support_Arrangements_Assignment_Arrangements],
I85.Value as [DDS_Support_Arrangements_Examination_Arrangements],
I86.Value as [DDS_Support_Arrangements_Field_trip_support],
I87.Value as [DDS_Support_Arrangements_For_Information],
I88.Value as [DDS_Support_Arrangements_Library_Support],
I89.Value as [DDS_Support_Arrangements_Other_support_for_student],
I90.Value as [DDS_Support_Arrangements_Presentation_Arrangements],
I91.Value as [DDS_Support_Arrangements_Teaching_Arrangements],
I92.Value as [DDS_Support_Arrangements_Timetabling_and_furniture],
I93.Value as [DDS_Support_Arrangements_Updated_QL],
I94.Value as [KEY_INFORMATION],
I95.Value as [MH__WB_Academic_or_university_concerns],
I96.Value as [MH__WB_Contextual_factors],
I97.Value as [MH__WB_Counselling_Referrals_Number_of_referrals],
I98.Value as [MH__WB_Current_presenting_concerns],
I99.Value as [MH__WB_Diagnosed_MH_condition],
I100.Value as [MH__WB_Historical_presenting_concerns],
I101.Value as [MH__WB_MH__WB_Current_Risk_Identified_risk_from_others],
I102.Value as [MH__WB_MH__WB_Current_Risk_Identified_risk_to_others],
I103.Value as [MH__WB_MH__WB_Current_Risk_Identified_risk_to_self],
I104.Value as [MH__WB_MHWB_active],
I105.Value as [MH__WB_MHWB_Adviser],
I106.Value as [MH__WB_MHWB_appointments_Follow_up_booked],
I107.Value as [MH__WB_MHWB_Appt_Data_2018___2019_Interactions_this_year],
I108.Value as [MH__WB_MHWB_Appt_Data_2019___2020_Interactions_this_year],
I109.Value as [MH__WB_MHWB_Appt_Data_2020___2021_Interactions_this_year],
I110.Value as [MH__WB_MHWB_Appt_Data_Academic_year_s__seen],
I111.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Any_previous_contact_with_MHWB],
I112.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Confirmation_details_confirmed_or_updated],
I113.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_1_successful],
I114.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_2_successful],
I115.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_3_successful],
I116.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_successful],
I117.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Current_safety_concerns_identified],
I118.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_DDS_arrangements_in_place],
I119.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_DDS_contact],
I120.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_First_responder],
I121.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Method],
I122.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Nature_of_previous_contact],
I123.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_ONS__WSAS_Anxiety],
I124.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_ONS__WSAS_Happiness],
I125.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_ONS__WSAS_Life_Satisfaction],
I126.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_ONS__WSAS_Problems_impacting_on_study],
I127.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_ONS__WSAS_Worthwhile],
I128.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Outcome_MHWB],
I129.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Referred_by],
I130.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Signposted_to],
I131.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Triage_2019_2020],
I132.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Unsuccessful_contact_1_email_sent],
I133.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Unsuccessful_contact_2_email_sent],
I134.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Unsuccessful_contact_3_email_sent],
I135.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Any_previous_contact_with_MHWB],
I136.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Confirmation_details_confirmed_or_updated],
I137.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Contact_1_successful],
I138.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Contact_2_successful],
I139.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Contact_3_successful],
I140.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Current_safety_concerns_identified],
I141.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_DDS_arrangements_in_place],
I142.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_DDS_contact],
I143.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_First_responder],
I144.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Information_given_to_student],
I145.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Method],
I146.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Nature_of_previous_contact],
I147.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_ONS__WSAS_anxiety],
I148.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_ONS__WSAS_happiness],
I149.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_ONS__WSAS_life_satisfaction],
I150.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_ONS__WSAS_problems_impacting_on_study],
I151.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_ONS__WSAS_worthwhile],
I152.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Other_appointment_made],
I153.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Outcome_MHWB],
I154.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Referred_by],
I155.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Signposted_to],
I156.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Student_preferences_identified],
I157.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Triage_1],
I158.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Triage_2020___2021],
I159.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Unsuccessful_contact_1_email_sent],
I160.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Unsuccessful_contact_2_email_sent],
I161.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Unsuccessful_contact_3_email_sent],
I162.Value as [MH__WB_MHWB_Archive_A_MJ_Contact],
I163.Value as [MH__WB_MHWB_Archive_Annie_J],
I164.Value as [MH__WB_MHWB_Archive_CF_Contact],
I165.Value as [MH__WB_MHWB_Archive_Data_Protection],
I166.Value as [MH__WB_MHWB_Archive_How_did_student_find_out_about_service__if_self_referred_],
I167.Value as [MH__WB_MHWB_Archive_JS_Contact],
I168.Value as [MH__WB_MHWB_Archive_MG_contact],
I169.Value as [MH__WB_MHWB_Archive_MHWB_Contact_Initials],
I170.Value as [MH__WB_MHWB_Archive_Month_Year_of_contact],
I171.Value as [MH__WB_MHWB_Archive_NS_Contact],
I172.Value as [MH__WB_MHWB_Archive_Outcomes],
I173.Value as [MH__WB_MHWB_Archive_Presenting_Concerns],
I174.Value as [MH__WB_MHWB_Archive_Referred_From],
I175.Value as [MH__WB_MHWB_Archive_Referred_To],
I176.Value as [MH__WB_MHWB_Archive_Referred_to_WCC_active_from_010913],
I177.Value as [MH__WB_MHWB_Archive_SA_contact],
I178.Value as [MH__WB_MHWB_Archive_Would_the_student_benefit_from_counselling],
I179.Value as [MH__WB_Referred_to],
I180.Value as [MH__WB_Referrer],
I181.Value as [MH__WB_Support_intervention],
I182.Value as [MH__WB_Support_network__student_discusses_difficulties_with_],
I183.Value as [Preferred_pronouns],
I184.Value as [QL_Records_Management_Care_Leavers],
I185.Value as [QL_Records_Management_Graduate]
FROM INT.STG_MAX_IndividualDetailListMulti I
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I1 on I.IndividualId = I1.IndividualId and I1.Name = 'ARCHIVED FIELDS\Archive Box No.'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I2 on I.IndividualId = I2.IndividualId and I2.Name = 'ARCHIVED FIELDS\Archive year'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I3 on I.IndividualId = I3.IndividualId and I3.Name = 'ARCHIVED FIELDS\Circulation (UDF no longer used)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I4 on I.IndividualId = I4.IndividualId and I4.Name = 'ARCHIVED FIELDS\Disability Duration'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I5 on I.IndividualId = I5.IndividualId and I5.Name = 'ARCHIVED FIELDS\DSA Year'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I6 on I.IndividualId = I6.IndividualId and I6.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\(for ALF) Type of Student'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I7 on I.IndividualId = I7.IndividualId and I7.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\(for ALF) Who pays fees?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I8 on I.IndividualId = I8.IndividualId and I8.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\(for ALF) Year Of Course'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I9 on I.IndividualId = I9.IndividualId and I9.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Student to complete ALF?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I10 on I.IndividualId = I10.IndividualId and I10.Name = 'ARCHIVED FIELDS\Lockers\Locker Allocated (Number)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I11 on I.IndividualId = I11.IndividualId and I11.Name = 'ARCHIVED FIELDS\Lockers\Locker Waiting List'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I12 on I.IndividualId = I12.IndividualId and I12.Name = 'ARCHIVED FIELDS\Lockers\Locker?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I13 on I.IndividualId = I13.IndividualId and I13.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\Home or International?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I14 on I.IndividualId = I14.IndividualId and I14.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\Pre Entry - Support Worker required?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I15 on I.IndividualId = I15.IndividualId and I15.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\SA signed?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I16 on I.IndividualId = I16.IndividualId and I16.Name = 'ARCHIVED FIELDS\Old fields\Support Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I17 on I.IndividualId = I17.IndividualId and I17.Name = 'ARCHIVED FIELDS\Old fields\Support worker 2012-2013'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I18 on I.IndividualId = I18.IndividualId and I18.Name = 'ARCHIVED FIELDS\Old fields\Support Worker?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I19 on I.IndividualId = I19.IndividualId and I19.Name = 'ARCHIVED FIELDS\PEEPs 2010\PEEP Status'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I20 on I.IndividualId = I20.IndividualId and I20.Name = 'ARCHIVED FIELDS\Post Reg -  No Placement'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I21 on I.IndividualId = I21.IndividualId and I21.Name = 'ARCHIVED FIELDS\Progress'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I22 on I.IndividualId = I22.IndividualId and I22.Name = 'ARCHIVED FIELDS\SpLD Marking Policy'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I23 on I.IndividualId = I23.IndividualId and I23.Name = 'ARCHIVED FIELDS\Student in Receipt of DSA'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I24 on I.IndividualId = I24.IndividualId and I24.Name = 'ARCHIVED FIELDS\Temporary Only'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I25 on I.IndividualId = I25.IndividualId and I25.Name = 'ARCHIVED FIELDS\Triage\Call info\MHWB Appt required'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I26 on I.IndividualId = I26.IndividualId and I26.Name = 'ARCHIVED FIELDS\Triage\Call info\Safety concerns'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I27 on I.IndividualId = I27.IndividualId and I27.Name = 'ARCHIVED FIELDS\Triage\Dates\triage call 1 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I28 on I.IndividualId = I28.IndividualId and I28.Name = 'ARCHIVED FIELDS\Triage\Dates\triage call 2 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I29 on I.IndividualId = I29.IndividualId and I29.Name = 'ARCHIVED FIELDS\Triage\Dates\triage call 3 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I30 on I.IndividualId = I30.IndividualId and I30.Name = 'Corona query'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I31 on I.IndividualId = I31.IndividualId and I31.Name = 'Covid-19 Fields\Accommodation\Accommodation\Does student plan to live in LSBU student accomm'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I32 on I.IndividualId = I32.IndividualId and I32.Name = 'Covid-19 Fields\Accommodation\Accommodation\How many bedrooms will there be in student''s flat'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I33 on I.IndividualId = I33.IndividualId and I33.Name = 'Covid-19 Fields\Accommodation\Accommodation\Which halls of residence will student be living in'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I34 on I.IndividualId = I34.IndividualId and I34.Name = 'Covid-19 Fields\Accommodation\Accommodation\Will student be a Residential Life Ambassador (RLA)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I35 on I.IndividualId = I35.IndividualId and I35.Name = 'Covid-19 Fields\Adjustments and interventions\Adjustments proposed'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I36 on I.IndividualId = I36.IndividualId and I36.Name = 'Covid-19 Fields\Adjustments and interventions\Health & emotional support (disability team)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I37 on I.IndividualId = I37.IndividualId and I37.Name = 'Covid-19 Fields\Adjustments and interventions\Student accommodation adjustments'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I38 on I.IndividualId = I38.IndividualId and I38.Name = 'Covid-19 Fields\Adjustments and interventions\Unable to attend - attendance not required by course\Home or EU student'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I39 on I.IndividualId = I39.IndividualId and I39.Name = 'Covid-19 Fields\Adjustments and interventions\Unable to attend - attendance required by course\Home, EU, and Tier 4 students'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I40 on I.IndividualId = I40.IndividualId and I40.Name = 'Covid-19 Fields\Advisor'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I41 on I.IndividualId = I41.IndividualId and I41.Name = 'Covid-19 Fields\Circulated to'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I42 on I.IndividualId = I42.IndividualId and I42.Name = 'Covid-19 Fields\Coronavirus questions\Has student had coronavirus?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I43 on I.IndividualId = I43.IndividualId and I43.Name = 'Covid-19 Fields\Covid plan created'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I44 on I.IndividualId = I44.IndividualId and I44.Name = 'Covid-19 Fields\Existing DDS support\Does student already have SAF agreed with DDS?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I45 on I.IndividualId = I45.IndividualId and I45.Name = 'Covid-19 Fields\International student\Is student an international student studying on a T4 visa'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I46 on I.IndividualId = I46.IndividualId and I46.Name = 'Covid-19 Fields\Referred to'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I47 on I.IndividualId = I47.IndividualId and I47.Name = 'Covid-19 Fields\Risk information\High risk detail'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I48 on I.IndividualId = I48.IndividualId and I48.Name = 'Covid-19 Fields\Risk information\Moderate risk detail'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I49 on I.IndividualId = I49.IndividualId and I49.Name = 'Covid-19 Fields\Risk information\Risk category'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I50 on I.IndividualId = I50.IndividualId and I50.Name = 'Covid-19 Fields\Travel\How will student be travelling to campus?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I51 on I.IndividualId = I51.IndividualId and I51.Name = 'DDS\Consent to Share (DDS)\Permission Given Method'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I52 on I.IndividualId = I52.IndividualId and I52.Name = 'DDS\DSA\BSL Interpreter\BSL Interpreter Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I53 on I.IndividualId = I53.IndividualId and I53.Name = 'DDS\DSA\BSL Interpreter\BSL Supplier'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I54 on I.IndividualId = I54.IndividualId and I54.Name = 'DDS\DSA\DSA Funded By'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I55 on I.IndividualId = I55.IndividualId and I55.Name = 'DDS\DSA\Mentoring Support\Mentor'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I56 on I.IndividualId = I56.IndividualId and I56.Name = 'DDS\DSA\Mentoring Support\Mentor Support Total Hours'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I57 on I.IndividualId = I57.IndividualId and I57.Name = 'DDS\DSA\Mentoring Support\Mentoring Type'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I58 on I.IndividualId = I58.IndividualId and I58.Name = 'DDS\DSA\Note Taking\Note taker recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I59 on I.IndividualId = I59.IndividualId and I59.Name = 'DDS\DSA\Specialist Tutor\Specialist Tutor Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I60 on I.IndividualId = I60.IndividualId and I60.Name = 'DDS\DSA\Student will exceed DSA funding'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I61 on I.IndividualId = I61.IndividualId and I61.Name = 'DDS\DSA\Study Skills Tuition\Dyslexia Tuition Type'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I62 on I.IndividualId = I62.IndividualId and I62.Name = 'DDS\DSA\Study Skills Tuition\Dyslexia Tutor'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I63 on I.IndividualId = I63.IndividualId and I63.Name = 'DDS\DSA\Study Skills Tuition\Total Interim Hours Used'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I64 on I.IndividualId = I64.IndividualId and I64.Name = 'DDS\DSA\Study Skills Tuition\Tutor Support Total Hours'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I65 on I.IndividualId = I65.IndividualId and I65.Name = 'DDS\DSA\Updated QL (DSA field)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I66 on I.IndividualId = I66.IndividualId and I66.Name = 'DDS\Lockers\Lockers\Locker Agreement Signed'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I67 on I.IndividualId = I67.IndividualId and I67.Name = 'DDS\LSBU funded NMH\BSL Interpreter\BSL Interpreter Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I68 on I.IndividualId = I68.IndividualId and I68.Name = 'DDS\LSBU funded NMH\Mentoring Support (LSBU)\Mentor Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I69 on I.IndividualId = I69.IndividualId and I69.Name = 'DDS\LSBU funded NMH\Note Taking Support\Note taker Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I70 on I.IndividualId = I70.IndividualId and I70.Name = 'DDS\LSBU funded NMH\Note Taking Support\Note taking Supplier'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I71 on I.IndividualId = I71.IndividualId and I71.Name = 'DDS\LSBU funded NMH\Study Skills Tuition (LSBU)\Tutor Hours Agreed'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I72 on I.IndividualId = I72.IndividualId and I72.Name = 'DDS\LSBU funded NMH\Study Skills Tuition (LSBU)\Tutor Recommended'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I73 on I.IndividualId = I73.IndividualId and I73.Name = 'DDS\LSBU funded NMH\Study Skills Tuition (LSBU)\Tutor Supplier'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I74 on I.IndividualId = I74.IndividualId and I74.Name = 'DDS\PRE ENTRY\DP Form signed?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I75 on I.IndividualId = I75.IndividualId and I75.Name = 'DDS\PRE ENTRY\Enrolment Support Required?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I76 on I.IndividualId = I76.IndividualId and I76.Name = 'DDS\PRE ENTRY\Med ev received?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I77 on I.IndividualId = I77.IndividualId and I77.Name = 'DDS\PRE ENTRY\Pre-entry status'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I78 on I.IndividualId = I78.IndividualId and I78.Name = 'DDS\PRE ENTRY\SAF agreed?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I79 on I.IndividualId = I79.IndividualId and I79.Name = 'DDS\PRE ENTRY\Support declined'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I80 on I.IndividualId = I80.IndividualId and I80.Name = 'DDS\Screening and Ed Psych Assessment\EP Assessment Result'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I81 on I.IndividualId = I81.IndividualId and I81.Name = 'DDS\Screening and Ed Psych Assessment\EP Assessor'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I82 on I.IndividualId = I82.IndividualId and I82.Name = 'DDS\Screening and Ed Psych Assessment\EP Room'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I83 on I.IndividualId = I83.IndividualId and I83.Name = 'DDS\Screening and Ed Psych Assessment\SpLD screening outcome'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I84 on I.IndividualId = I84.IndividualId and I84.Name = 'DDS\Support Arrangements\Assignment Arrangements'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I85 on I.IndividualId = I85.IndividualId and I85.Name = 'DDS\Support Arrangements\Examination Arrangements'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I86 on I.IndividualId = I86.IndividualId and I86.Name = 'DDS\Support Arrangements\Field trip support'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I87 on I.IndividualId = I87.IndividualId and I87.Name = 'DDS\Support Arrangements\For Information'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I88 on I.IndividualId = I88.IndividualId and I88.Name = 'DDS\Support Arrangements\Library Support'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I89 on I.IndividualId = I89.IndividualId and I89.Name = 'DDS\Support Arrangements\Other support for student'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I90 on I.IndividualId = I90.IndividualId and I90.Name = 'DDS\Support Arrangements\Presentation Arrangements'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I91 on I.IndividualId = I91.IndividualId and I91.Name = 'DDS\Support Arrangements\Teaching Arrangements'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I92 on I.IndividualId = I92.IndividualId and I92.Name = 'DDS\Support Arrangements\Timetabling and furniture'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I93 on I.IndividualId = I93.IndividualId and I93.Name = 'DDS\Support Arrangements\Updated QL'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I94 on I.IndividualId = I94.IndividualId and I94.Name = 'KEY INFORMATION'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I95 on I.IndividualId = I95.IndividualId and I95.Name = 'MH & WB\Academic or university concerns'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I96 on I.IndividualId = I96.IndividualId and I96.Name = 'MH & WB\Contextual factors'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I97 on I.IndividualId = I97.IndividualId and I97.Name = 'MH & WB\Counselling Referrals\Number of referrals'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I98 on I.IndividualId = I98.IndividualId and I98.Name = 'MH & WB\Current presenting concerns'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I99 on I.IndividualId = I99.IndividualId and I99.Name = 'MH & WB\Diagnosed MH condition?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I100 on I.IndividualId = I100.IndividualId and I100.Name = 'MH & WB\Historical presenting concerns'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I101 on I.IndividualId = I101.IndividualId and I101.Name = 'MH & WB\MH & WB Current Risk\Identified risk from others'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I102 on I.IndividualId = I102.IndividualId and I102.Name = 'MH & WB\MH & WB Current Risk\Identified risk to others'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I103 on I.IndividualId = I103.IndividualId and I103.Name = 'MH & WB\MH & WB Current Risk\Identified risk to self'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I104 on I.IndividualId = I104.IndividualId and I104.Name = 'MH & WB\MHWB active'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I105 on I.IndividualId = I105.IndividualId and I105.Name = 'MH & WB\MHWB Adviser'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I106 on I.IndividualId = I106.IndividualId and I106.Name = 'MH & WB\MHWB appointments\Follow up booked'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I107 on I.IndividualId = I107.IndividualId and I107.Name = 'MH & WB\MHWB Appt Data\2018 - 2019\Interactions this year'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I108 on I.IndividualId = I108.IndividualId and I108.Name = 'MH & WB\MHWB Appt Data\2019 - 2020\Interactions this year'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I109 on I.IndividualId = I109.IndividualId and I109.Name = 'MH & WB\MHWB Appt Data\2020 - 2021\Interactions this year'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I110 on I.IndividualId = I110.IndividualId and I110.Name = 'MH & WB\MHWB Appt Data\Academic year(s) seen'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I111 on I.IndividualId = I111.IndividualId and I111.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Any previous contact with MHWB'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I112 on I.IndividualId = I112.IndividualId and I112.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Confirmation details confirmed or updated'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I113 on I.IndividualId = I113.IndividualId and I113.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact 1 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I114 on I.IndividualId = I114.IndividualId and I114.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact 2 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I115 on I.IndividualId = I115.IndividualId and I115.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact 3 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I116 on I.IndividualId = I116.IndividualId and I116.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I117 on I.IndividualId = I117.IndividualId and I117.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Current safety concerns identified'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I118 on I.IndividualId = I118.IndividualId and I118.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\DDS arrangements in place'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I119 on I.IndividualId = I119.IndividualId and I119.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\DDS contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I120 on I.IndividualId = I120.IndividualId and I120.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\First responder'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I121 on I.IndividualId = I121.IndividualId and I121.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Method'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I122 on I.IndividualId = I122.IndividualId and I122.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Nature of previous contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I123 on I.IndividualId = I123.IndividualId and I123.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\ONS + WSAS Anxiety'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I124 on I.IndividualId = I124.IndividualId and I124.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\ONS + WSAS Happiness'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I125 on I.IndividualId = I125.IndividualId and I125.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\ONS + WSAS Life Satisfaction'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I126 on I.IndividualId = I126.IndividualId and I126.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\ONS + WSAS Problems impacting on study'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I127 on I.IndividualId = I127.IndividualId and I127.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\ONS + WSAS Worthwhile'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I128 on I.IndividualId = I128.IndividualId and I128.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Outcome MHWB'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I129 on I.IndividualId = I129.IndividualId and I129.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Referred by'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I130 on I.IndividualId = I130.IndividualId and I130.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Signposted to'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I131 on I.IndividualId = I131.IndividualId and I131.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Triage 2019 2020'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I132 on I.IndividualId = I132.IndividualId and I132.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Unsuccessful contact 1 email sent'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I133 on I.IndividualId = I133.IndividualId and I133.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Unsuccessful contact 2 email sent'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I134 on I.IndividualId = I134.IndividualId and I134.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Unsuccessful contact 3 email sent'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I135 on I.IndividualId = I135.IndividualId and I135.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Any previous contact with MHWB'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I136 on I.IndividualId = I136.IndividualId and I136.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Confirmation details confirmed or updated'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I137 on I.IndividualId = I137.IndividualId and I137.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Contact 1 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I138 on I.IndividualId = I138.IndividualId and I138.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Contact 2 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I139 on I.IndividualId = I139.IndividualId and I139.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Contact 3 successful'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I140 on I.IndividualId = I140.IndividualId and I140.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Current safety concerns identified'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I141 on I.IndividualId = I141.IndividualId and I141.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\DDS arrangements in place'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I142 on I.IndividualId = I142.IndividualId and I142.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\DDS contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I143 on I.IndividualId = I143.IndividualId and I143.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\First responder'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I144 on I.IndividualId = I144.IndividualId and I144.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Information given to student'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I145 on I.IndividualId = I145.IndividualId and I145.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Method'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I146 on I.IndividualId = I146.IndividualId and I146.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Nature of previous contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I147 on I.IndividualId = I147.IndividualId and I147.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\ONS + WSAS anxiety'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I148 on I.IndividualId = I148.IndividualId and I148.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\ONS + WSAS happiness'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I149 on I.IndividualId = I149.IndividualId and I149.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\ONS + WSAS life satisfaction'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I150 on I.IndividualId = I150.IndividualId and I150.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\ONS + WSAS problems impacting on study'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I151 on I.IndividualId = I151.IndividualId and I151.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\ONS + WSAS worthwhile'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I152 on I.IndividualId = I152.IndividualId and I152.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Other appointment made'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I153 on I.IndividualId = I153.IndividualId and I153.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Outcome MHWB'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I154 on I.IndividualId = I154.IndividualId and I154.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Referred by'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I155 on I.IndividualId = I155.IndividualId and I155.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Signposted to'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I156 on I.IndividualId = I156.IndividualId and I156.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Student preferences identified'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I157 on I.IndividualId = I157.IndividualId and I157.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Triage 1'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I158 on I.IndividualId = I158.IndividualId and I158.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Triage 2020 - 2021'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I159 on I.IndividualId = I159.IndividualId and I159.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Unsuccessful contact 1 email sent'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I160 on I.IndividualId = I160.IndividualId and I160.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Unsuccessful contact 2 email sent'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I161 on I.IndividualId = I161.IndividualId and I161.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Unsuccessful contact 3 email sent'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I162 on I.IndividualId = I162.IndividualId and I162.Name = 'MH & WB\MHWB Archive\A-MJ Contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I163 on I.IndividualId = I163.IndividualId and I163.Name = 'MH & WB\MHWB Archive\Annie J'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I164 on I.IndividualId = I164.IndividualId and I164.Name = 'MH & WB\MHWB Archive\CF Contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I165 on I.IndividualId = I165.IndividualId and I165.Name = 'MH & WB\MHWB Archive\Data Protection'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I166 on I.IndividualId = I166.IndividualId and I166.Name = 'MH & WB\MHWB Archive\How did student find out about service (if self referred)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I167 on I.IndividualId = I167.IndividualId and I167.Name = 'MH & WB\MHWB Archive\JS Contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I168 on I.IndividualId = I168.IndividualId and I168.Name = 'MH & WB\MHWB Archive\MG contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I169 on I.IndividualId = I169.IndividualId and I169.Name = 'MH & WB\MHWB Archive\MHWB Contact Initials'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I170 on I.IndividualId = I170.IndividualId and I170.Name = 'MH & WB\MHWB Archive\Month Year of contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I171 on I.IndividualId = I171.IndividualId and I171.Name = 'MH & WB\MHWB Archive\NS Contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I172 on I.IndividualId = I172.IndividualId and I172.Name = 'MH & WB\MHWB Archive\Outcomes'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I173 on I.IndividualId = I173.IndividualId and I173.Name = 'MH & WB\MHWB Archive\Presenting Concerns'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I174 on I.IndividualId = I174.IndividualId and I174.Name = 'MH & WB\MHWB Archive\Referred From?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I175 on I.IndividualId = I175.IndividualId and I175.Name = 'MH & WB\MHWB Archive\Referred To'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I176 on I.IndividualId = I176.IndividualId and I176.Name = 'MH & WB\MHWB Archive\Referred to WCC active from 010913'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I177 on I.IndividualId = I177.IndividualId and I177.Name = 'MH & WB\MHWB Archive\SA contact'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I178 on I.IndividualId = I178.IndividualId and I178.Name = 'MH & WB\MHWB Archive\Would the student benefit from counselling?'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I179 on I.IndividualId = I179.IndividualId and I179.Name = 'MH & WB\Referred to'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I180 on I.IndividualId = I180.IndividualId and I180.Name = 'MH & WB\Referrer'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I181 on I.IndividualId = I181.IndividualId and I181.Name = 'MH & WB\Support intervention'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I182 on I.IndividualId = I182.IndividualId and I182.Name = 'MH & WB\Support network (student discusses difficulties with)'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I183 on I.IndividualId = I183.IndividualId and I183.Name = 'Preferred pronouns'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I184 on I.IndividualId = I184.IndividualId and I184.Name = 'QL Records Management\Care Leavers*'
LEFT JOIN INT.STG_MAX_IndividualDetailListMulti I185 on I.IndividualId = I185.IndividualId and I185.Name = 'QL Records Management\Graduate?*'