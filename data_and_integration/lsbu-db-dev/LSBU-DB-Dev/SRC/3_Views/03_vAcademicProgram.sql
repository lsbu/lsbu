CREATE OR ALTER VIEW [SRC].[vAcademicProgram] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	(CASE
	   WHEN col1 = 'APPRENT' THEN CONCAT(col1,col3,col4)
	   ELSE col1 
	END) as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15,col16,col17)) as HashMD5,
col1 as QL_AcadProgNameId,
col2 as QL_AcadProgName,
col3 as QL_DepartmentId,
col4 as QL_EducationInstitutionId,
col5 as QL_Website,
col6 as QL_Phone,
col7 as QL_Add1,
col8 as QL_Add2,
col9 as QL_Add3,
col10 as QL_Add4,
col11 as QL_Add5,
col12 as QL_Postcode,
col13 as QL_Country,
col14 as QL_Description,
col15 as QL_CreatedDate,
col16 as QL_LastModifiedDate,
col17 as QL_UCASCourseCode,
lower(col18) as QL_CourseAdministrator,
lower(col19) as QL_CourseDirector,
col20 as QL_TypeofCourse
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'AcademicProgram'
GO