CREATE OR ALTER VIEW [SRC].[vCognosAttendanceDetails] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	CONCAT(col1, '.', col2, '.', col3, '.', col5, '.', col6) as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12)) as HashMD5,
	col1 as Cognos_StudentID,
	col2 as Cognos_ModuleId,
	col3 as Cognos_AcadProgNameId,
	col4 as Cognos_Term,
	col5 as Cognos_AOSPeriod,
	col6 as Cognos_AcademicPeriod,
	col7 as Cognos_LecturerId,
	col8 as Cognos_ModuleLevelAttendance,
	col9 as Cognos_TotalModuleClasses,
	col10 as Cognos_ModuleLevelAttendancepercent,
	col11 as Cognos_CourseLevelAttendance,
	col12 as Cognos_CourseLevelAttendancepercent
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'StudentAttendanceDetails'