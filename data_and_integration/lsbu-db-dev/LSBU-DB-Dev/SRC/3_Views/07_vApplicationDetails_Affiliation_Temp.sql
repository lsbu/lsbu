CREATE OR ALTER VIEW [SRC].[vApplicationDetails_Affiliation_Temp] AS
SELECT * FROM 
	(
		SELECT 
		RunID,
		FullFileName,
		SrcSystem,
		ObjectName,
		RowNo,
		FileDateTime,
		--Concatenate <QL_StudentID>.<QL_AcadProgNameId>.<QL_ApplicantStageIndicator>
		CONCAT(col1,'.',col6,'.',col26) as LSB_ExternalID,
		FileRowNumber,
		--HASHBYTES('MD5',CONCAT(QL_ApplicationStatus,QL_EnrollmentStatus)) as HashMD5,
		HASHBYTES('MD5',CONCAT(col8,col14)) as HashMD5,
		col1 as QL_StudentID,
		col5 as QL_ApplicationDate,
		col6 as QL_AcadProgNameId,
		NULL as QL_DepartmentId,
		NULL as QL_EducationalInstitutionId,
		col8 as QL_ApplicationStatus,
		col10 as QL_AcademicPeriod,
		--col14 as QL_EnrollmentStatus,
		col26 as QL_ApplicantStageIndicator,
		--col27 as QL_EnrolmentStageIndicator,
		col37 as QL_AppStatusDateAchieved,
		--col38 as QL_EnrolStatusDateAchieved,
		'AcademicProgram' as QL_RecordType,
		/*ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
			ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
		) AffiliationRank*/
		ROW_NUMBER () OVER ( partition by col1,col6
			ORDER BY col1,col6 asc ,col8 desc ,col14 desc
		) AffiliationRank
		FROM [SRC].[SRC_ROW_TMP]
		WHERE ObjectName = 'ApplicationDetails' AND col26 = 'A'
	) as SRC_AffiliationApplicants where AffiliationRank = 1 -- Applicant Acad Program Affiliation

UNION ALL

SELECT * FROM 
	(
		SELECT 
		A.RunID,
		A.FullFileName,
		A.SrcSystem,
		A.ObjectName,
		A.RowNo,
		A.FileDateTime,
		--Concatenate <QL_StudentID>.<QL_DepartmentId>.<QL_ApplicantStageIndicator>
		CONCAT(A.col1,'.',B.col3,'.',A.col26) as LSB_ExternalID,
		A.FileRowNumber,
		--HASHBYTES('MD5',CONCAT(QL_ApplicationStatus,QL_EnrollmentStatus)) as HashMD5,
		HASHBYTES('MD5',CONCAT(A.col8,A.col14)) as HashMD5,
		A.col1 as QL_StudentID,
		A.col5 as QL_ApplicationDate,
		A.col6 as QL_AcadProgNameId,
		B.col3 as QL_DepartmentId,
		NULL as QL_EducationalInstitutionId,
		A.col8 as QL_ApplicationStatus,
		A.col10 as QL_AcademicPeriod,
		--A.col14 as QL_EnrollmentStatus,
		A.col26 as QL_ApplicantStageIndicator,
		--A.col27 as QL_EnrolmentStageIndicator,
		A.col37 as QL_AppStatusDateAchieved,
		--A.col38 as QL_EnrolStatusDateAchieved,
		'Department' as QL_RecordType,
		/*ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
			ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
		) AffiliationRank*/
		ROW_NUMBER () OVER ( partition by A.col1,B.col3
			ORDER BY A.col1,B.col3 asc ,A.col8 desc ,A.col14 desc
		) AffiliationRank
		FROM [SRC].[SRC_ROW_TMP] A, [SRC].[SRC_ROW_TMP] B
		WHERE A.ObjectName = 'ApplicationDetails' AND A.col26 = 'A'
		AND B.ObjectName = 'AcademicProgram' AND B.col3 != B.col4
		AND A.col6 = B.col1
	) as SRC_AffiliationApplicants where AffiliationRank = 1-- Applicant Department Affiliation

UNION ALL

SELECT * FROM 
	(
		SELECT 
		A.RunID,
		A.FullFileName,
		A.SrcSystem,
		A.ObjectName,
		A.RowNo,
		A.FileDateTime,
		--Concatenate <QL_StudentID>.<QL_EducationInstitutionId>.<QL_ApplicantStageIndicator>
		CONCAT(A.col1,'.',B.col4,'.',A.col26) as LSB_ExternalID,
		A.FileRowNumber,
		--HASHBYTES('MD5',CONCAT(QL_ApplicationStatus,QL_EnrollmentStatus)) as HashMD5,
		HASHBYTES('MD5',CONCAT(A.col8,A.col14)) as HashMD5,
		A.col1 as QL_StudentID,
		A.col5 as QL_ApplicationDate,
		A.col5 as QL_AcadProgNameId,
		B.col3 as QL_DepartmentId,
		B.col4 as QL_EducationalInstitutionId,
		A.col8 as QL_ApplicationStatus,
		A.col10 as QL_AcademicPeriod,
		--A.col14 as QL_EnrollmentStatus,
		A.col26 as QL_ApplicantStageIndicator,
		--A.col27 as QL_EnrolmentStageIndicator,
		A.col37 as QL_AppStatusDateAchieved,
		--A.col38 as QL_EnrolStatusDateAchieved,
		'EducationalInstitution' as QL_RecordType,
		/*ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
			ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
		) AffiliationRank*/
		ROW_NUMBER () OVER ( partition by A.col1,B.col4
			ORDER BY A.col1,B.col4 asc ,A.col8 desc ,A.col14 desc
		) AffiliationRank
		FROM [SRC].[SRC_ROW_TMP] A, [SRC].[SRC_ROW_TMP] B
		WHERE A.ObjectName = 'ApplicationDetails' AND A.col26 = 'A'
		AND B.ObjectName = 'AcademicProgram' 
		AND A.col6 = B.col1
	) as SRC_AffiliationApplicants where AffiliationRank = 1-- Applicant Edu Institution Affiliation

UNION ALL

SELECT * FROM 
	(
		SELECT 
		RunID,
		FullFileName,
		SrcSystem,
		ObjectName,
		RowNo,
		FileDateTime,
		--Concatenate <QL_StudentID>.<QL_AcadProgNameId>.<QL_EnrolmentStageIndicator>
		CONCAT(col1,'.',col6,'.',col27) as LSB_ExternalID,
		FileRowNumber,
		--HASHBYTES('MD5',QL_EnrollmentStatus) as HashMD5,
		HASHBYTES('MD5',CONCAT(col8,col14)) as HashMD5,
		col1 as QL_StudentID,
		col5 as QL_ApplicationDate,
		col6 as QL_AcadProgNameId,
		NULL as QL_DepartmentId,
		NULL as QL_EducationalInstitutionId,
		col8 as QL_ApplicationStatus,
		col10 as QL_AcademicPeriod,
		--col14 as QL_EnrollmentStatus,
		col26 as QL_ApplicantStageIndicator,
		--col27 as QL_EnrolmentStageIndicator,
		col37 as QL_AppStatusDateAchieved,
		--col38 as QL_EnrolStatusDateAchieved,
		'AcademicProgram' as QL_RecordType,
		/*ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
		ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
		) AffiliationRank*/
		ROW_NUMBER () OVER ( partition by col1,col6
			ORDER BY col1,col6 asc ,col8 desc ,col14 desc
		) AffiliationRank
		FROM [SRC].[SRC_ROW_TMP]
		WHERE ObjectName = 'ApplicationDetails' AND col14 = 'EFE' AND col27 = 'E'
	) as SRC_AffiliationApplicants where AffiliationRank = 1 -- Student Acad Program Affiliation

UNION ALL

SELECT * FROM 
	(
		SELECT 
		A.RunID,
		A.FullFileName,
		A.SrcSystem,
		A.ObjectName,
		A.RowNo,
		A.FileDateTime,
		--Concatenate <QL_StudentID>.<QL_AcadProgNameId>.<QL_EnrolmentStageIndicator>
		CONCAT(A.col1,'.',B.col3,'.',A.col27) as LSB_ExternalID,
		A.FileRowNumber,
		--HASHBYTES('MD5',QL_EnrollmentStatus) as HashMD5,
		HASHBYTES('MD5',CONCAT(A.col8,A.col14)) as HashMD5,
		A.col1 as QL_StudentID,
		A.col5 as QL_ApplicationDate,
		A.col6 as QL_AcadProgNameId,
		B.col3 as QL_DepartmentId,
		NULL as QL_EducationalInstitutionId,
		A.col8 as QL_ApplicationStatus,
		A.col10 as QL_AcademicPeriod,
		--A.col14 as QL_EnrollmentStatus,
		A.col26 as QL_ApplicantStageIndicator,
		--A.col27 as QL_EnrolmentStageIndicator,
		A.col37 as QL_AppStatusDateAchieved,
		--A.col38 as QL_EnrolStatusDateAchieved,
		'Department' as QL_RecordType,
		/*ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
		ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
		) AffiliationRank*/
		ROW_NUMBER () OVER ( partition by A.col1,B.col3
			ORDER BY A.col1,B.col3 asc ,A.col8 desc ,A.col14 desc
		) AffiliationRank
		FROM [SRC].[SRC_ROW_TMP] A, [SRC].[SRC_ROW_TMP] B
		WHERE A.ObjectName = 'ApplicationDetails' AND A.col14 = 'EFE' AND A.col27 = 'E'
		AND B.ObjectName = 'AcademicProgram' AND B.col3 != B.col4
		AND A.col6 = B.col1
	) as SRC_AffiliationApplicants where AffiliationRank = 1 -- Student Department Affiliation

	UNION ALL

SELECT * FROM 
	(
		SELECT 
		A.RunID,
		A.FullFileName,
		A.SrcSystem,
		A.ObjectName,
		A.RowNo,
		A.FileDateTime,
		--Concatenate <QL_StudentID>.<QL_AcadProgNameId>.<QL_EnrolmentStageIndicator>
		CONCAT(A.col1,'.',B.col4,'.',A.col27) as LSB_ExternalID,
		A.FileRowNumber,
		--HASHBYTES('MD5',QL_EnrollmentStatus) as HashMD5,
		HASHBYTES('MD5',CONCAT(A.col8,A.col14)) as HashMD5,
		A.col1 as QL_StudentID,
		A.col5 as QL_ApplicationDate,
		A.col6 as QL_AcadProgNameId,
		B.col3 as QL_DepartmentId,
		B.col4 as QL_EducationalInstitutionId,
		A.col8 as QL_ApplicationStatus,
		A.col10 as QL_AcademicPeriod,
		--A.col14 as QL_EnrollmentStatus,
		A.col26 as QL_ApplicantStageIndicator,
		--A.col27 as QL_EnrolmentStageIndicator,
		A.col37 as QL_AppStatusDateAchieved,
		--A.col38 as QL_EnrolStatusDateAchieved,
		'EducationalInstitution' as QL_RecordType,
		/*ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
		ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
		) AffiliationRank*/
		ROW_NUMBER () OVER ( partition by A.col1,B.col4
			ORDER BY A.col1,B.col4 asc ,A.col8 desc ,A.col14 desc
		) AffiliationRank
		FROM [SRC].[SRC_ROW_TMP] A, [SRC].[SRC_ROW_TMP] B
		WHERE A.ObjectName = 'ApplicationDetails' AND A.col14 = 'EFE' AND A.col27 = 'E'
		AND B.ObjectName = 'AcademicProgram' 
		AND A.col6 = B.col1
	) as SRC_AffiliationApplicants where AffiliationRank = 1 -- Student Educational Institution Affiliation

GO


