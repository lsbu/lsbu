CREATE OR ALTER VIEW [SRC].[vDepartment] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	col1 as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15)) as HashMD5,
col1 as QL_DepartmentId,
col2 as QL_DepartmentName,
col3 as QL_EducationInstitutionId,
col4 as QL_Website,
col5 as QL_Phone,
col6 as QL_Add1,
col7 as QL_Add2,
col8 as QL_Add3,
col9 as QL_Add4,
col10 as QL_Add5,
col11 as QL_Postcode,
col12 as QL_Country,
col13 as QL_Description,
col14 as QL_CreatedDate,
col15 as QL_LastModifiedDate
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'Department'
GO