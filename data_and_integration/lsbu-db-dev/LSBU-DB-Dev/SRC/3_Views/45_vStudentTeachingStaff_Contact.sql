CREATE OR ALTER VIEW [SRC].[vStudentTeachingStaff_Contact] AS
SELECT 
	Q1.RunID,
	Q1.FullFileName,
	Q1.SrcSystem,
	Q1.ObjectName,
	Q1.RowNo,
	Q1.FileDateTime,
	Q1.LSB_ExternalID,
	Q1.Account_LSB_ExternalID,
	Q1.FileRowNumber,
	Q1.HashMD5,
  CAST(NULL AS VARCHAR(4000)) AS CMIS_StudentID,
  Q1.CMIS_AcadProgNameId,
  CAST(NULL AS VARCHAR(4000)) AS CMIS_AcademicPeriod,
  CAST(NULL AS VARCHAR(4000)) AS CMIS_AOSPeriod,
  CAST(NULL AS VARCHAR(4000)) AS CMIS_ModuleId,
  CAST(NULL AS VARCHAR(4000)) AS CMIS_ModuleInstance,
  CAST(NULL AS VARCHAR(4000)) AS CMIS_Semester,
  Q1.CMIS_TeachingStaffId,
  Q1.CMIS_TeachingStaffFirstName,
  Q1.CMIS_TeachingStaffLastname,
  Q1.CMIS_TeachingStaffEmail
FROM (
SELECT 
	NULL AS RunID,
	NULL AS FullFileName,
	'CMIS' AS SrcSystem,
	NULL AS ObjectName,
	NULL AS RowNo,
	[ROW_ADD_TS] AS FileDateTime,
	CONCAT(LecturerId,'@lsbu.ac.uk') as LSB_ExternalID,
	QL_EducationInstitutionId as Account_LSB_ExternalID,
	NULL AS FileRowNumber,
	HASHBYTES('MD5',CONCAT(CMISCALC_FIRSTNAME,CMISCALC_LASTNAME)) as HashMD5,
  --[STUDENTID] as CMIS_StudentID,
  [CMISCALC_CourseCode] as CMIS_AcadProgNameId,
  --[AcademicPeriod] as CMIS_AcademicPeriod,
 -- [LINKCODE] as CMIS_AOSPeriod,
 -- CMISCALC_ModuleId as CMIS_ModuleId,
  --CMISCALC_ModuleInstance as CMIS_ModuleInstance,
  --CMISCALC_Semester as CMIS_Semester,
  LecturerId as CMIS_TeachingStaffId,
  CMISCALC_FIRSTNAME as CMIS_TeachingStaffFirstName,
  CMISCALC_LASTNAME as CMIS_TeachingStaffLastname,
  CONCAT(LecturerId,'@lsbu.ac.uk') as CMIS_TeachingStaffEmail,
  ROW_NUMBER()OVER(PARTITION BY LecturerId ORDER BY [AcademicPeriod] DESC) RNK
FROM [SRC].[SRC_ROW_TMP_CMIS]
LEFT JOIN [STG].[tAcademicProgram] ON QL_AcadProgNameId = [CMISCALC_CourseCode]
WHERE NAME NOT IN ('HPL Lecturer')
 ) Q1 WHERE Q1.RNK = 1