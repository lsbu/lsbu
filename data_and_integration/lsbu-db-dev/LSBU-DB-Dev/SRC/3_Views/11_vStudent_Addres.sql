CREATE OR ALTER VIEW [SRC].[vStudent_Address] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	CONCAT(col1, '.Permanent') as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col16,col17,col18,col19,col20,col21,col22)) as HashMD5,
	col1 as QL_StudentID,
	CONCAT(col1, '.Permanent') as QL_AddressID,
	col16 as QL_Add1,
	col17 as QL_Add2,
	col18 as QL_Add3,
	col19 as QL_Add4,
	col20 as QL_Add5,
	col21 as QL_Postcode,
	col22 as QL_Country,
	'Permanent' as QL_AddressType
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'Student'
  AND CONCAT(col16,col17,col18,col19,col20,col21,col22) IS NOT NULL
  AND CONCAT(col16,col17,col18,col19,col20,col21,col22) <> ''

UNION ALL

SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	CONCAT(col1, '.Term') as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col31,col32,col33,col34,col35,col36,col37)) as HashMD5,
	col1 as QL_StudentID,
	CONCAT(col1, '.Term') as QL_AddressID,
  	col31 as QL_Term_Add1,
	col32 as QL_Term_Add2,
	col33 as QL_Term_Add3,
	col34 as QL_Term_Add4,
	col35 as QL_Term_Add5,
	col36 as QL_Term_Postcode,
	col37 as QL_Term_Country,
	'Term' as QL_AddressType
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'Student'
  AND CONCAT(col31,col32,col33,col34,col35,col36,col37) IS NOT NULL
  AND CONCAT(col31,col32,col33,col34,col35,col36,col37) <> ''

GO