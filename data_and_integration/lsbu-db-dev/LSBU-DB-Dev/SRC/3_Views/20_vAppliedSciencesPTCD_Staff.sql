CREATE OR ALTER VIEW [SRC].[vAppliedSciencesPTCD_Staff] AS
SELECT * FROM (
  SELECT
  AppliedSciencesPTCD.RunID,
  AppliedSciencesPTCD.FullFileName,
  AppliedSciencesPTCD.SrcSystem,
  AppliedSciencesPTCD.ObjectName,
  AppliedSciencesPTCD.RowNo,
  AppliedSciencesPTCD.FileDateTime,
  StaffUser.FederationIdentifier as LSB_ExternalID,							--StaffId
  AppliedSciencesPTCD.FileRowNumber,
  --HASHBYTES('MD5',CONCAT(QL_StaffFirstName,QL_StaffLastName)) as HashMD5,
  HASHBYTES('MD5',CONCAT(StaffUser.FirstName,StaffUser.LastName)) as HashMD5,
  StaffUser.username as QL_StaffId,
  StaffUser.Email as QL_StaffEmailAddress,
  StaffUser.FirstName as QL_StaffFirstName,
  StaffUser.LastName as QL_StaffLastName,
  NULL as QL_Gender,
  StaffUser.Id as SF_UserId,
  /*ROW_NUMBER () OVER ( partition by QL_StaffId
  ORDER BY QL_StaffId
  ) StaffRank*/
  ROW_NUMBER() OVER (PARTITION BY StaffUser.username ORDER BY StaffUser.username) StaffRank
  --Distict Records based on the Staff id
  FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD
  INNER JOIN [SFC].[Get_ID_User] StaffUser ON StaffUser.FederationIdentifier = AppliedSciencesPTCD.col8
  WHERE  AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD' AND StaffUser.FederationIdentifier IS NOT NULL
) AS SRC_PersonalTutor where StaffRank = 1