CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_CaseDetailNumber] AS
SELECT DISTINCT
C.CaseId,
C1.Value as [Safety_Concern_Response_Number_of_students_affected]
FROM Maximizer.src.[Case] C
LEFT JOIN Maximizer.src.Case_DetailNumber C1 on C.CaseId = C1.CaseId and C1.Name = 'Safety Concern Response\Number of students affected'