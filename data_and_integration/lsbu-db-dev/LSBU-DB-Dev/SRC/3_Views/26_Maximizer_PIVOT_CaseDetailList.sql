CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_CaseDetailList] AS
SELECT DISTINCT
C.CaseId,
C1.Value as [Safety_Concern_Response_Student_in_halls]
FROM Maximizer.src.[Case] C
LEFT JOIN Maximizer.src.Case_DetailList C1 on C.CaseId = C1.CaseId and C1.Name = 'Safety Concern Response\Student in halls?'