CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_IndividualDetailDate] AS
SELECT DISTINCT
I.Id,
I1.Value as [1st_Contact_Date],
I2.Value as [ARCHIVED_FIELDS_ARCHIVED_Date__or_INTERRUPTED_Date_],
I3.Value as [ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Completed_Date],
I4.Value as [ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Requested_Date],
I5.Value as [ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Target_Date],
I6.Value as [ARCHIVED_FIELDS_Dis_Type_amended_date],
I7.Value as [ARCHIVED_FIELDS_Disability_Allowance_Amended_date],
I8.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_Claim_Submitted_Date],
I9.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_money_received_DATE],
I10.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_DEPOSIT_ARCHIVED],
I11.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Ed_Psych_invoice_date],
I12.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Invoice_to_Finance_Date_or_Agresso_Passed_Date],
I13.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_No_ALF___Student_Contacted_date],
I14.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Texted_to_Remind_about_EP_appointment],
I15.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_To_Be_Emailed_Re_Deposit],
I16.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Waiting_List],
I17.Value as [ARCHIVED_FIELDS_Emailed_Re_assessment_dates],
I18.Value as [ARCHIVED_FIELDS_Lockers_Key_returned_date],
I19.Value as [ARCHIVED_FIELDS_Old_fields_2011_12_DSA_Application_sent],
I20.Value as [ARCHIVED_FIELDS_Old_fields_2012_13_DSA_Application_Sent],
I21.Value as [ARCHIVED_FIELDS_Old_fields_2013_14_DSA_Application_Sent],
I22.Value as [ARCHIVED_FIELDS_Old_fields_Form_to_Fees_to_be_Stamped],
I23.Value as [ARCHIVED_FIELDS_Old_fields_NAR_Authorisation_Letter_received],
I24.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_PRE_ENTRY_ArchivedShredded_date],
I25.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_PRE_ENTRY_Data_Protection_Form_signed__recd_back_from_student],
I26.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_PRE_ENTRY_QUEST_Chased_Date],
I27.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_PRE_ENTRY_QUEST_Sent_date],
I28.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_PRE_ENTRY_SA_signed__recd_back_from_student],
I29.Value as [ARCHIVED_FIELDS_Old_fields_OPUS_Allocation_Date],
I30.Value as [ARCHIVED_FIELDS_Old_fields_OPUS_Entered_Date],
I31.Value as [ARCHIVED_FIELDS_Old_fields_Support_Worker_Allocated],
I32.Value as [ARCHIVED_FIELDS_Old_fields_Tutor_Allocated_Date],
I33.Value as [ARCHIVED_FIELDS_PEEP_Review_Date],
I34.Value as [ARCHIVED_FIELDS_PEEP_Sent_to_faculty_date],
I35.Value as [ARCHIVED_FIELDS_QLS_Flag_Added_date],
I36.Value as [ARCHIVED_FIELDS_Temporary_disability_review_date],
I37.Value as [ARCHIVED_FIELDS_Triage_Call_info_MHWB_appt_booked],
I38.Value as [ARCHIVED_FIELDS_Triage_Dates_Date_of_triage_call_1],
I39.Value as [ARCHIVED_FIELDS_Triage_Dates_Date_of_triage_call_2],
I40.Value as [ARCHIVED_FIELDS_Triage_Dates_Date_of_triage_call_3],
I41.Value as [ARCHIVED_FIELDS_Triage_Dates_Date_student_in_contact_or_staff_referral],
I42.Value as [Birth_Date],
I43.Value as [Covid_19_Fields_Covid_plan_sent],
I44.Value as [Covid_19_Fields_Date_completed],
I45.Value as [Date_Last_Contacted],
I46.Value as [DDS_Consent_to_Share__DDS__Date_of_Agreement],
I47.Value as [DDS_Data_Protection_Form_Received],
I48.Value as [DDS_DSA_2014_15_DSA_Application_Sent],
I49.Value as [DDS_DSA_2015_16_DSA_Application_Sent],
I50.Value as [DDS_DSA_2016_17_DSA_Application_sent],
I51.Value as [DDS_DSA_2017_18_DSA_Application_sent],
I52.Value as [DDS_DSA_2018_19_DSA_Application_sent],
I53.Value as [DDS_DSA_2019_20_DSA_applications_sent],
I54.Value as [DDS_DSA_2020_21_DSA_Application_sent],
I55.Value as [DDS_DSA_CLASS_Needs_Assessment_Date],
I56.Value as [DDS_DSA_DSA_Application_Received_by_DDS__date_],
I57.Value as [DDS_DSA_DSA2_received],
I58.Value as [DDS_DSA_Mentoring_Support_DATE_Mentor_allocated],
I59.Value as [DDS_DSA_Mentoring_Support_Email_Mentoring_to__Full__Sent],
I60.Value as [DDS_DSA_Needs_Assessment_Report_Received_Date],
I61.Value as [DDS_DSA_Study_Skills_Tuition_DATE_Dyslexia_Tutor_allocated],
I62.Value as [DDS_DSA_Study_Skills_Tuition_Email_Converted_to__Full__Sent],
I63.Value as [DDS_DSA_Study_Skills_Tuition_Email_Total_Interim_Hours_Used_Request],
I64.Value as [DDS_EP_Waiting_List_EP_Waiting_List],
I65.Value as [DDS_Lockers_Lockers_Locker_Assigned_Date],
I66.Value as [DDS_Lockers_Lockers_Locker_Loan_End_Date],
I67.Value as [DDS_PRE_ENTRY_PRE_ENTRY_Evidence_Received_date],
I68.Value as [DDS_PRE_ENTRY_Pre_Entry_MORE_evidence_requested_date],
I69.Value as [DDS_PRE_ENTRY_PRE_ENTRY_QUEST_Received_date],
I70.Value as [DDS_Screening_and_Ed_Psych_Assessment_Date_of_EP_Assessment],
I71.Value as [DDS_Screening_and_Ed_Psych_Assessment_Date_of_SpLD_screening],
I72.Value as [DDS_Screening_and_Ed_Psych_Assessment_Ed_Psych_report_received_date],
I73.Value as [DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_Appointment_Date],
I74.Value as [DDS_Support_Arrangements_Supp_Arrs_agreed_date],
I75.Value as [DDS_Support_Arrangements_Supp_Arrs_Updated_date_1],
I76.Value as [DDS_Support_Arrangements_SuppArr_sent_to_School_date],
I77.Value as [Import_Dates_Course_Change],
I78.Value as [Import_Dates_First_Name_Change],
I79.Value as [Import_Dates_Import_Date],
I80.Value as [Import_Dates_Last_Name_Change],
I81.Value as [Import_Dates_Stage_Code_Change],
I82.Value as [MH__WB_Counselling_Referrals_Date_of_most_recent_referral],
I83.Value as [MH__WB_MHWB_appointments_Contacted_following_referral],
I84.Value as [MH__WB_MHWB_appointments_Eighteenth_Appointment],
I85.Value as [MH__WB_MHWB_appointments_Eighth_appointment],
I86.Value as [MH__WB_MHWB_appointments_Eleventh_Appointment],
I87.Value as [MH__WB_MHWB_appointments_Fifteenth_Appointment],
I88.Value as [MH__WB_MHWB_appointments_Fifth_appointment],
I89.Value as [MH__WB_MHWB_appointments_First_appointment],
I90.Value as [MH__WB_MHWB_appointments_Fourteenth_Appointment],
I91.Value as [MH__WB_MHWB_appointments_Fourth_appointment],
I92.Value as [MH__WB_MHWB_appointments_Nineteenth_Appointment],
I93.Value as [MH__WB_MHWB_appointments_Ninth_appointment],
I94.Value as [MH__WB_MHWB_appointments_Second_appointment],
I95.Value as [MH__WB_MHWB_appointments_Seventeenth_Appointment],
I96.Value as [MH__WB_MHWB_appointments_Seventh_appointment],
I97.Value as [MH__WB_MHWB_appointments_Sixteenth_Appointment],
I98.Value as [MH__WB_MHWB_appointments_Sixth_appointment],
I99.Value as [MH__WB_MHWB_appointments_Tenth_appointment],
I100.Value as [MH__WB_MHWB_appointments_Third_appointment],
I101.Value as [MH__WB_MHWB_appointments_Thirteenth_Appointment],
I102.Value as [MH__WB_MHWB_appointments_Twelth_Appointment],
I103.Value as [MH__WB_MHWB_appointments_Twentieth_Appointment],
I104.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_1],
I105.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_2],
I106.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Contact_3],
I107.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_Date_of_contact],
I108.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_If_appointment_made_date],
I109.Value as [MH__WB_MHWB_Appt_Data_Triage_2019___2020_If_staff_referral_confirmation_sent],
I110.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Contact_1],
I111.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Contact_2],
I112.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Contact_3],
I113.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_Date_of_contact],
I114.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_If_appointment_made_date],
I115.Value as [MH__WB_MHWB_Appt_Data_Triage_2020___2021_If_staff_referral_date_confirmation_sent],
I116.Value as [MH__WB_MHWB_Archive_1st_Contact_Date],
I117.Value as [MH__WB_MHWB_Archive_Contact_End_Date],
I118.Value as [MH__WB_MHWB_Archive_Date_referred_for_counselling],
I119.Value as [MH__WB_MHWB_Archive_Date_therapy_summary_returned],
I120.Value as [MH__WB_Presenting_concerns_updated_date],
I121.Value as [QL_Records_Management_Expected_Course_End_Date],
I122.Value as [QL_Records_Management_Qual_Aim_Start_Date],
I123.Value as [QL_Records_Management_Stage_Date_Achieved]
FROM Maximizer.src.Individual I
LEFT JOIN Maximizer.src.Individual_DetailDate I1 on I.Id = I1.IndividualId and I1.Name = '1st Contact Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I2 on I.Id = I2.IndividualId and I2.Name = 'ARCHIVED FIELDS\ARCHIVED Date (or INTERRUPTED Date)'
LEFT JOIN Maximizer.src.Individual_DetailDate I3 on I.Id = I3.IndividualId and I3.Name = 'ARCHIVED FIELDS\DATA PROTECTION REQUESTS\DP Completed Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I4 on I.Id = I4.IndividualId and I4.Name = 'ARCHIVED FIELDS\DATA PROTECTION REQUESTS\DP Requested Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I5 on I.Id = I5.IndividualId and I5.Name = 'ARCHIVED FIELDS\DATA PROTECTION REQUESTS\DP Target Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I6 on I.Id = I6.IndividualId and I6.Name = 'ARCHIVED FIELDS\Dis Type amended date'
LEFT JOIN Maximizer.src.Individual_DetailDate I7 on I.Id = I7.IndividualId and I7.Name = 'ARCHIVED FIELDS\Disability Allowance Amended date'
LEFT JOIN Maximizer.src.Individual_DetailDate I8 on I.Id = I8.IndividualId and I8.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\ALF Claim Submitted Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I9 on I.Id = I9.IndividualId and I9.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\ALF money received DATE'
LEFT JOIN Maximizer.src.Individual_DetailDate I10 on I.Id = I10.IndividualId and I10.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\DEPOSIT ARCHIVED'
LEFT JOIN Maximizer.src.Individual_DetailDate I11 on I.Id = I11.IndividualId and I11.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Ed Psych invoice date'
LEFT JOIN Maximizer.src.Individual_DetailDate I12 on I.Id = I12.IndividualId and I12.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Invoice to Finance Date or Agresso Passed Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I13 on I.Id = I13.IndividualId and I13.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\No ALF - Student Contacted date'
LEFT JOIN Maximizer.src.Individual_DetailDate I14 on I.Id = I14.IndividualId and I14.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Texted to Remind about EP appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I15 on I.Id = I15.IndividualId and I15.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\To Be Emailed Re Deposit'
LEFT JOIN Maximizer.src.Individual_DetailDate I16 on I.Id = I16.IndividualId and I16.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Waiting List'
LEFT JOIN Maximizer.src.Individual_DetailDate I17 on I.Id = I17.IndividualId and I17.Name = 'ARCHIVED FIELDS\Emailed Re assessment dates'
LEFT JOIN Maximizer.src.Individual_DetailDate I18 on I.Id = I18.IndividualId and I18.Name = 'ARCHIVED FIELDS\Lockers\Key returned date'
LEFT JOIN Maximizer.src.Individual_DetailDate I19 on I.Id = I19.IndividualId and I19.Name = 'ARCHIVED FIELDS\Old fields\2011-12 DSA Application sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I20 on I.Id = I20.IndividualId and I20.Name = 'ARCHIVED FIELDS\Old fields\2012-13 DSA Application Sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I21 on I.Id = I21.IndividualId and I21.Name = 'ARCHIVED FIELDS\Old fields\2013-14 DSA Application Sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I22 on I.Id = I22.IndividualId and I22.Name = 'ARCHIVED FIELDS\Old fields\Form to Fees to be Stamped'
LEFT JOIN Maximizer.src.Individual_DetailDate I23 on I.Id = I23.IndividualId and I23.Name = 'ARCHIVED FIELDS\Old fields\NAR Authorisation Letter received'
LEFT JOIN Maximizer.src.Individual_DetailDate I24 on I.Id = I24.IndividualId and I24.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\PRE ENTRY ArchivedShredded date'
LEFT JOIN Maximizer.src.Individual_DetailDate I25 on I.Id = I25.IndividualId and I25.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\PRE ENTRY Data Protection Form signed & recd back from student'
LEFT JOIN Maximizer.src.Individual_DetailDate I26 on I.Id = I26.IndividualId and I26.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\PRE ENTRY QUEST. Chased Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I27 on I.Id = I27.IndividualId and I27.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\PRE ENTRY QUEST. Sent date'
LEFT JOIN Maximizer.src.Individual_DetailDate I28 on I.Id = I28.IndividualId and I28.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\PRE ENTRY SA signed & recd back from student'
LEFT JOIN Maximizer.src.Individual_DetailDate I29 on I.Id = I29.IndividualId and I29.Name = 'ARCHIVED FIELDS\Old fields\OPUS Allocation Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I30 on I.Id = I30.IndividualId and I30.Name = 'ARCHIVED FIELDS\Old fields\OPUS: Entered Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I31 on I.Id = I31.IndividualId and I31.Name = 'ARCHIVED FIELDS\Old fields\Support Worker Allocated'
LEFT JOIN Maximizer.src.Individual_DetailDate I32 on I.Id = I32.IndividualId and I32.Name = 'ARCHIVED FIELDS\Old fields\Tutor Allocated Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I33 on I.Id = I33.IndividualId and I33.Name = 'ARCHIVED FIELDS\PEEP Review Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I34 on I.Id = I34.IndividualId and I34.Name = 'ARCHIVED FIELDS\PEEP Sent to faculty date'
LEFT JOIN Maximizer.src.Individual_DetailDate I35 on I.Id = I35.IndividualId and I35.Name = 'ARCHIVED FIELDS\QLS Flag Added date'
LEFT JOIN Maximizer.src.Individual_DetailDate I36 on I.Id = I36.IndividualId and I36.Name = 'ARCHIVED FIELDS\Temporary disability review date'
LEFT JOIN Maximizer.src.Individual_DetailDate I37 on I.Id = I37.IndividualId and I37.Name = 'ARCHIVED FIELDS\Triage\Call info\MHWB appt booked'
LEFT JOIN Maximizer.src.Individual_DetailDate I38 on I.Id = I38.IndividualId and I38.Name = 'ARCHIVED FIELDS\Triage\Dates\Date of triage call 1'
LEFT JOIN Maximizer.src.Individual_DetailDate I39 on I.Id = I39.IndividualId and I39.Name = 'ARCHIVED FIELDS\Triage\Dates\Date of triage call 2'
LEFT JOIN Maximizer.src.Individual_DetailDate I40 on I.Id = I40.IndividualId and I40.Name = 'ARCHIVED FIELDS\Triage\Dates\Date of triage call 3'
LEFT JOIN Maximizer.src.Individual_DetailDate I41 on I.Id = I41.IndividualId and I41.Name = 'ARCHIVED FIELDS\Triage\Dates\Date student in contact or staff referral'
LEFT JOIN Maximizer.src.Individual_DetailDate I42 on I.Id = I42.IndividualId and I42.Name = 'Birth_Date*'
LEFT JOIN Maximizer.src.Individual_DetailDate I43 on I.Id = I43.IndividualId and I43.Name = 'Covid-19 Fields\Covid plan sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I44 on I.Id = I44.IndividualId and I44.Name = 'Covid-19 Fields\Date completed'
LEFT JOIN Maximizer.src.Individual_DetailDate I45 on I.Id = I45.IndividualId and I45.Name = 'Date Last Contacted'
LEFT JOIN Maximizer.src.Individual_DetailDate I46 on I.Id = I46.IndividualId and I46.Name = 'DDS\Consent to Share (DDS)\Date of Agreement'
LEFT JOIN Maximizer.src.Individual_DetailDate I47 on I.Id = I47.IndividualId and I47.Name = 'DDS\Data Protection Form Received'
LEFT JOIN Maximizer.src.Individual_DetailDate I48 on I.Id = I48.IndividualId and I48.Name = 'DDS\DSA\2014-15 DSA Application Sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I49 on I.Id = I49.IndividualId and I49.Name = 'DDS\DSA\2015-16 DSA Application Sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I50 on I.Id = I50.IndividualId and I50.Name = 'DDS\DSA\2016-17 DSA Application sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I51 on I.Id = I51.IndividualId and I51.Name = 'DDS\DSA\2017-18 DSA Application sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I52 on I.Id = I52.IndividualId and I52.Name = 'DDS\DSA\2018-19 DSA Application sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I53 on I.Id = I53.IndividualId and I53.Name = 'DDS\DSA\2019-20 DSA applications sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I54 on I.Id = I54.IndividualId and I54.Name = 'DDS\DSA\2020-21 DSA Application sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I55 on I.Id = I55.IndividualId and I55.Name = 'DDS\DSA\CLASS Needs Assessment Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I56 on I.Id = I56.IndividualId and I56.Name = 'DDS\DSA\DSA Application Received by DDS (date)'
LEFT JOIN Maximizer.src.Individual_DetailDate I57 on I.Id = I57.IndividualId and I57.Name = 'DDS\DSA\DSA2 received'
LEFT JOIN Maximizer.src.Individual_DetailDate I58 on I.Id = I58.IndividualId and I58.Name = 'DDS\DSA\Mentoring Support\DATE Mentor allocated'
LEFT JOIN Maximizer.src.Individual_DetailDate I59 on I.Id = I59.IndividualId and I59.Name = 'DDS\DSA\Mentoring Support\Email: Mentoring to "Full" Sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I60 on I.Id = I60.IndividualId and I60.Name = 'DDS\DSA\Needs Assessment Report Received Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I61 on I.Id = I61.IndividualId and I61.Name = 'DDS\DSA\Study Skills Tuition\DATE Dyslexia Tutor allocated'
LEFT JOIN Maximizer.src.Individual_DetailDate I62 on I.Id = I62.IndividualId and I62.Name = 'DDS\DSA\Study Skills Tuition\Email: Converted to "Full" Sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I63 on I.Id = I63.IndividualId and I63.Name = 'DDS\DSA\Study Skills Tuition\Email: Total Interim Hours Used Request'
LEFT JOIN Maximizer.src.Individual_DetailDate I64 on I.Id = I64.IndividualId and I64.Name = 'DDS\EP Waiting List\EP Waiting List'
LEFT JOIN Maximizer.src.Individual_DetailDate I65 on I.Id = I65.IndividualId and I65.Name = 'DDS\Lockers\Lockers\Locker Assigned Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I66 on I.Id = I66.IndividualId and I66.Name = 'DDS\Lockers\Lockers\Locker Loan End Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I67 on I.Id = I67.IndividualId and I67.Name = 'DDS\PRE ENTRY\PRE ENTRY Evidence Received date'
LEFT JOIN Maximizer.src.Individual_DetailDate I68 on I.Id = I68.IndividualId and I68.Name = 'DDS\PRE ENTRY\Pre Entry MORE evidence requested date'
LEFT JOIN Maximizer.src.Individual_DetailDate I69 on I.Id = I69.IndividualId and I69.Name = 'DDS\PRE ENTRY\PRE ENTRY QUEST. Received date'
LEFT JOIN Maximizer.src.Individual_DetailDate I70 on I.Id = I70.IndividualId and I70.Name = 'DDS\Screening and Ed Psych Assessment\Date of EP Assessment'
LEFT JOIN Maximizer.src.Individual_DetailDate I71 on I.Id = I71.IndividualId and I71.Name = 'DDS\Screening and Ed Psych Assessment\Date of SpLD screening'
LEFT JOIN Maximizer.src.Individual_DetailDate I72 on I.Id = I72.IndividualId and I72.Name = 'DDS\Screening and Ed Psych Assessment\Ed Psych report received date'
LEFT JOIN Maximizer.src.Individual_DetailDate I73 on I.Id = I73.IndividualId and I73.Name = 'DDS\Screening and Ed Psych Assessment\EP Feedback Appointment Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I74 on I.Id = I74.IndividualId and I74.Name = 'DDS\Support Arrangements\Supp Arrs agreed date'
LEFT JOIN Maximizer.src.Individual_DetailDate I75 on I.Id = I75.IndividualId and I75.Name = 'DDS\Support Arrangements\Supp. Arrs. Updated date 1'
LEFT JOIN Maximizer.src.Individual_DetailDate I76 on I.Id = I76.IndividualId and I76.Name = 'DDS\Support Arrangements\Supp.Arr sent to School date'
LEFT JOIN Maximizer.src.Individual_DetailDate I77 on I.Id = I77.IndividualId and I77.Name = 'Import Dates\Course Change'
LEFT JOIN Maximizer.src.Individual_DetailDate I78 on I.Id = I78.IndividualId and I78.Name = 'Import Dates\First Name Change'
LEFT JOIN Maximizer.src.Individual_DetailDate I79 on I.Id = I79.IndividualId and I79.Name = 'Import Dates\Import Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I80 on I.Id = I80.IndividualId and I80.Name = 'Import Dates\Last Name Change'
LEFT JOIN Maximizer.src.Individual_DetailDate I81 on I.Id = I81.IndividualId and I81.Name = 'Import Dates\Stage Code Change'
LEFT JOIN Maximizer.src.Individual_DetailDate I82 on I.Id = I82.IndividualId and I82.Name = 'MH & WB\Counselling Referrals\Date of most recent referral'
LEFT JOIN Maximizer.src.Individual_DetailDate I83 on I.Id = I83.IndividualId and I83.Name = 'MH & WB\MHWB appointments\Contacted following referral'
LEFT JOIN Maximizer.src.Individual_DetailDate I84 on I.Id = I84.IndividualId and I84.Name = 'MH & WB\MHWB appointments\Eighteenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I85 on I.Id = I85.IndividualId and I85.Name = 'MH & WB\MHWB appointments\Eighth appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I86 on I.Id = I86.IndividualId and I86.Name = 'MH & WB\MHWB appointments\Eleventh Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I87 on I.Id = I87.IndividualId and I87.Name = 'MH & WB\MHWB appointments\Fifteenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I88 on I.Id = I88.IndividualId and I88.Name = 'MH & WB\MHWB appointments\Fifth appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I89 on I.Id = I89.IndividualId and I89.Name = 'MH & WB\MHWB appointments\First appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I90 on I.Id = I90.IndividualId and I90.Name = 'MH & WB\MHWB appointments\Fourteenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I91 on I.Id = I91.IndividualId and I91.Name = 'MH & WB\MHWB appointments\Fourth appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I92 on I.Id = I92.IndividualId and I92.Name = 'MH & WB\MHWB appointments\Nineteenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I93 on I.Id = I93.IndividualId and I93.Name = 'MH & WB\MHWB appointments\Ninth appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I94 on I.Id = I94.IndividualId and I94.Name = 'MH & WB\MHWB appointments\Second appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I95 on I.Id = I95.IndividualId and I95.Name = 'MH & WB\MHWB appointments\Seventeenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I96 on I.Id = I96.IndividualId and I96.Name = 'MH & WB\MHWB appointments\Seventh appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I97 on I.Id = I97.IndividualId and I97.Name = 'MH & WB\MHWB appointments\Sixteenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I98 on I.Id = I98.IndividualId and I98.Name = 'MH & WB\MHWB appointments\Sixth appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I99 on I.Id = I99.IndividualId and I99.Name = 'MH & WB\MHWB appointments\Tenth appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I100 on I.Id = I100.IndividualId and I100.Name = 'MH & WB\MHWB appointments\Third appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I101 on I.Id = I101.IndividualId and I101.Name = 'MH & WB\MHWB appointments\Thirteenth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I102 on I.Id = I102.IndividualId and I102.Name = 'MH & WB\MHWB appointments\Twelth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I103 on I.Id = I103.IndividualId and I103.Name = 'MH & WB\MHWB appointments\Twentieth Appointment'
LEFT JOIN Maximizer.src.Individual_DetailDate I104 on I.Id = I104.IndividualId and I104.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact 1'
LEFT JOIN Maximizer.src.Individual_DetailDate I105 on I.Id = I105.IndividualId and I105.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact 2'
LEFT JOIN Maximizer.src.Individual_DetailDate I106 on I.Id = I106.IndividualId and I106.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Contact 3'
LEFT JOIN Maximizer.src.Individual_DetailDate I107 on I.Id = I107.IndividualId and I107.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\Date of contact'
LEFT JOIN Maximizer.src.Individual_DetailDate I108 on I.Id = I108.IndividualId and I108.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\If appointment made, date:'
LEFT JOIN Maximizer.src.Individual_DetailDate I109 on I.Id = I109.IndividualId and I109.Name = 'MH & WB\MHWB Appt Data\Triage 2019 - 2020\If staff referral, confirmation sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I110 on I.Id = I110.IndividualId and I110.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Contact 1'
LEFT JOIN Maximizer.src.Individual_DetailDate I111 on I.Id = I111.IndividualId and I111.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Contact 2'
LEFT JOIN Maximizer.src.Individual_DetailDate I112 on I.Id = I112.IndividualId and I112.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Contact 3'
LEFT JOIN Maximizer.src.Individual_DetailDate I113 on I.Id = I113.IndividualId and I113.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\Date of contact'
LEFT JOIN Maximizer.src.Individual_DetailDate I114 on I.Id = I114.IndividualId and I114.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\If appointment made, date'
LEFT JOIN Maximizer.src.Individual_DetailDate I115 on I.Id = I115.IndividualId and I115.Name = 'MH & WB\MHWB Appt Data\Triage 2020 - 2021\If staff referral, date confirmation sent'
LEFT JOIN Maximizer.src.Individual_DetailDate I116 on I.Id = I116.IndividualId and I116.Name = 'MH & WB\MHWB Archive\1st Contact Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I117 on I.Id = I117.IndividualId and I117.Name = 'MH & WB\MHWB Archive\Contact End Date'
LEFT JOIN Maximizer.src.Individual_DetailDate I118 on I.Id = I118.IndividualId and I118.Name = 'MH & WB\MHWB Archive\Date referred for counselling'
LEFT JOIN Maximizer.src.Individual_DetailDate I119 on I.Id = I119.IndividualId and I119.Name = 'MH & WB\MHWB Archive\Date therapy summary returned'
LEFT JOIN Maximizer.src.Individual_DetailDate I120 on I.Id = I120.IndividualId and I120.Name = 'MH & WB\Presenting concerns updated date'
LEFT JOIN Maximizer.src.Individual_DetailDate I121 on I.Id = I121.IndividualId and I121.Name = 'QL Records Management\Expected_Course_End_Date*'
LEFT JOIN Maximizer.src.Individual_DetailDate I122 on I.Id = I122.IndividualId and I122.Name = 'QL Records Management\Qual_Aim_Start_Date*'
LEFT JOIN Maximizer.src.Individual_DetailDate I123 on I.Id = I123.IndividualId and I123.Name = 'QL Records Management\Stage_Date_Achieved*'