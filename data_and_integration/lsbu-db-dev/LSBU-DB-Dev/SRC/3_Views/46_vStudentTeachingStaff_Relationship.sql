CREATE OR ALTER VIEW [SRC].[vStudentTeachingStaff_Relationship] AS
SELECT * FROM 
(
SELECT 
  NULL AS RunID,
  NULL AS FullFileName,
  'CMIS' AS SrcSystem,
  NULL AS ObjectName,
  NULL AS RowNo,
  [ROW_ADD_TS] AS FileDateTime,
  CONCAT([STUDENTID],'.',CMISCALC_ModuleId,'.',CMISCALC_ModuleInstance,'.',[CMISCALC_CourseCode],'.',[LINKCODE],'.',CMISCALC_AcademicPeriod,'.',CMISCALC_Semester,'.',LecturerId,'.CourseTutor') as LSB_ExternalID,
  CONCAT(CMISCALC_ModuleId,'.',CMISCALC_ModuleInstance,'.',CMISCALC_AcademicPeriod) as ModuleOffering_LSB_ExternalID,
  CONCAT(LecturerId,'@lsbu.ac.uk') as TeachingStaff_LSB_ExternalID,
  NULL AS FileRowNumber,
  HASHBYTES('MD5','1') as HashMD5,
  [STUDENTID] as CMIS_StudentID,
  [CMISCALC_CourseCode] as CMIS_AcadProgNameId,
  CMISCALC_AcademicPeriod as CMIS_AcademicPeriod,
  [LINKCODE] as CMIS_AOSPeriod,
  CMISCALC_ModuleId as CMIS_ModuleId,
  CMISCALC_ModuleInstance as CMIS_ModuleInstance,
  CMISCALC_Semester as CMIS_Semester,
  LecturerId as CMIS_TeachingStaffId,
  CMISCALC_FIRSTNAME as CMIS_TeachingStaffFirstName,
  CMISCALC_LASTNAME as CMIS_TeachingStaffLastname,
  CONCAT(LecturerId,'@lsbu.ac.uk') as CMIS_TeachingStaffEmail,
  ROW_NUMBER()OVER(PARTITION BY [STUDENTID],CMISCALC_ModuleId,CMISCALC_ModuleInstance,[CMISCALC_CourseCode],[LINKCODE],CMISCALC_AcademicPeriod,CMISCALC_Semester,LecturerId ORDER BY [AcademicPeriod] DESC) RNK
FROM [SRC].[SRC_ROW_TMP_CMIS] 
WHERE NAME NOT IN ('HPL Lecturer')
) Q1
WHERE Q1.RNK = 1