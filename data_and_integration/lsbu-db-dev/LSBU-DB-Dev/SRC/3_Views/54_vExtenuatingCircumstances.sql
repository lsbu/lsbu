CREATE OR ALTER VIEW [SRC].[vExtenuatingCircumstances] AS

with extcir_convert as (
select distinct
Academic_Period,
School,
Division,
Course,
Title,
Session,
Level,
Module,
Module_Session,
Semester,
Assessment,
Student_ID,
Forename,
Surname,
Year,
Stage,
Ext_Circ,
Mitigate_Note,
Internal_Email,
External_Email,
file_name,
replace(substring(Module, 0, patindex('%-%',Module)), ' ', '') as Module1,
replace(substring(Module, patindex('%-%',Module)+1, LEN(Module)), ' ', '') as Module2,
COALESCE(TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),10), 103),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),10), 103),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),10), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),7)),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),7), 103),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),8), 103),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),8), 103),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), ' ', ''), patindex('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), ' ', '')),8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), ' ', ''), patindex('%[0-9]/[0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), ' ', '')),8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] January [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('January')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] February [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('February')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] March [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('March')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] April [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('April')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] May [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('May')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] June [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('June')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] July [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('July')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] August [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('August')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] September [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('September')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] October [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('October')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] November [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('November')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9][0-9] December [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('December')+8), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] January [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('January')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] February [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('February')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] March [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('March')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] April [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('April')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] May [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('May')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] June [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('June')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] July [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('July')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] August [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('August')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] September [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('September')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] October [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('October')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] November [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('November')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9] December [0-9][0-9][0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')),len('December')+7), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Jan[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Feb[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Mar[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Apr[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]May[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Jun[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Jul[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Aug[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Sep[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Oct[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Nov[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', ''), patindex('%[0-9]Dec[0-9][0-9]%', replace(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), 'th', '')), 6), 3),
TRY_convert(date, substring(replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', ''), patindex('%[0-9]/[0-9]/[0-9][0-9]%', replace(replace(replace(replace(replace(replace(Mitigate_Note, '.', '/'), ':', ''), 'SD', ''), '-', ''), 'DD', ''), ',', '')),8), 3),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9][0-9]-[0-9][0-9]-[0-9][0-9]%', Mitigate_note),8)),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9][0-9]-[0-9][0-9]-[0-9][0-9]%', Mitigate_note),8), 103),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9][0-9]-[0-9][0-9]-[0-9][0-9]%', Mitigate_note),8), 3),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9]/[0-9][0-9][0-9][0-9]%', Mitigate_note),8)),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9]/[0-9][0-9][0-9][0-9]%', Mitigate_note),8), 103),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9]/[0-9][0-9][0-9][0-9]%', Mitigate_note),8), 3),
TRY_convert(date, substring(replace(Mitigate_Note, '..', '.'), patindex('%[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]%', replace(Mitigate_Note, '..', '.')),8)),
TRY_convert(date, substring(replace(Mitigate_Note, '..', '.'), patindex('%[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]%', replace(Mitigate_Note, '..', '.')),8), 103),
TRY_convert(date, substring(replace(Mitigate_Note, '..', '.'), patindex('%[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]%', replace(Mitigate_Note, '..', '.')),8), 3),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9]/[0-9][0-9]%', Mitigate_note),6), 3),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9]/[0-9][0-9]%', Mitigate_note),6), 103),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9]/[0-9][0-9]%', Mitigate_note),6)),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9][0-9]/[0-9][0-9]%', Mitigate_note),6), 3),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9][0-9]/[0-9][0-9]%', Mitigate_note),6), 103),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9]/[0-9][0-9]/[0-9][0-9]%', Mitigate_note),6)),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9][0-9]-[0-9]-[0-9][0-9]%', Mitigate_note),7)),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9][0-9]-[0-9]-[0-9][0-9]%', Mitigate_note),7), 103),
TRY_convert(date, substring(Mitigate_Note, patindex('%[0-9][0-9]-[0-9]-[0-9][0-9]%', Mitigate_note),7), 3),
TRY_convert(date, replace(Mitigate_Note, 'th', ''), 106),
TRY_convert(date, replace(Mitigate_Note, 'Appeal supported 27.Oct.17, uncap marks', '27 Oct 2017')),
TRY_convert(date, replace(Mitigate_Note, 'uncap mark Appeal supported 27.Oct.17', '27 Oct 2017')),
TRY_convert(date, replace(Mitigate_Note, 'Decision Date - 11.012.2019.  Claim relates to cw4 & cw5.', '11-12-2019')),
TRY_convert(date, replace(Mitigate_Note, 'Decision Date - 13.0.7.2017', '13-07-2017')),
TRY_convert(date, replace(Mitigate_Note, '11.012.2019', '11-12-2019')),
TRY_convert(date, replace(Mitigate_Note, ' Decision Date - 08.008.2018', '08-08-2018')),
TRY_convert(date, replace(Mitigate_Note, 'Decision !3.06.19', '03-06-2019')),
TRY_convert(date, replace(Mitigate_Note, 'Decision Date - 0904.2019', '09-04-2019')),
TRY_convert(date, replace(Mitigate_Note, 'Decision date: 1806.2020 - CW1', '18-06-2020')),
TRY_convert(date, replace(Mitigate_Note, 'Decision date: 1806.2020', '18-06-2020')),
TRY_convert(date, replace(Mitigate_Note, 'Decision Date: 9.10.20', '09-10-2020')),
TRY_convert(date, replace(Mitigate_Note, 'TCC_6_129 27th Jul 2018 letter (INVUED) "you will be given the opportunity to attempt the TCC_6_129 Community PNPV100 module and this will be considered as your first attempt."', '27-06-2018')),
TRY_convert(date, replace(Mitigate_Note, 'NS confirmed by email 10Apr17 that fire alarm went off whilst students were taking exam.', '10-04-2017'))) as Mitigate_Note_Date
from [SRC].[SRC_ROW_EXTCIR])

,extcir_joined as (
SELECT 
Academic_Period,
School,
Division,
Course,
Title,
Session,
Level,
Module,
Module_Session,
Semester,
Assessment,
Student_ID,
Forename,
Surname,
Year,
Stage,
Ext_Circ,
Mitigate_Note,
Internal_Email,
External_Email,
file_name,
Module1,
Module2,
Mitigate_Note_Date,
MED.LSB_ExternalID as ModuleEnrolment_LSB_ExternalId,
MCED.LSB_ExternalID as ModuleComponent_LSB_ExternalId,
MSCED.LSB_ExternalID as ModuleSubComponent_LSB_ExternalId,
ROW_NUMBER() over (partition by Student_ID, Module1, Course, Session, Academic_Period, Module_Session, Semester, Assessment, Ext_Circ ORDER BY Student_ID ) as EC_Number
FROM 
extcir_convert EC LEFT JOIN [STG].[tModuleEnrolmentDetails] MED ON 
	EC.[Academic_Period] =MED.QL_AcademicPeriod  AND 
	EC.[Course] = MED.QL_AcadProgNameId  AND   
	EC.[Session] = MED.QL_AOSPeriod  AND 
	EC.[Module1] = MED.QL_ModuleId  AND 
	EC.[Semester] = MED.QL_Semester AND
	EC.[Student_ID] = MED.QL_StudentID	AND 
	EC.Module_Session = MED.QL_ModuleInstance
LEFT JOIN [STG].[tModuleComponentEnrolmentDetails] MCED ON 
	MED.LSB_ExternalID = MCED.ModuleEnrolment_LSB_ExternalID  AND 
	EC.[Assessment] = MCED.QL_ComponentId  AND 
	EC.[Student_ID] = MCED.QL_StudentId  AND 
	EC.[Academic_Period]=MCED.QL_AcademicPeriod AND 
	EC.Module_Session = MCED.QL_ModuleInstance
LEFT JOIN [STG].[tModuleSubComponentEnrolmentDetails] MSCED ON 
	MED.LSB_ExternalID = MSCED.ModuleEnrolment_LSB_ExternalID  AND 
  MCED.LSB_ExternalID = MSCED.ModuleComponent_LSB_ExternalID AND
	EC.[Assessment] = MSCED.QL_SubComponentId  AND 
	EC.[Student_ID] = MSCED.QL_StudentId  AND 
	EC.Module_Session = MSCED.QL_ModuleInstance)


select 
Academic_Period,
School,
Division,
Course,
Title,
Session,
Level,
Module,
Module_Session,
Semester,
Assessment,
Student_ID,
Forename,
Surname,
Year,
Stage,
Ext_Circ,
Mitigate_Note,
Internal_Email,
External_Email,
file_name,
Module1,
Module2,
Mitigate_Note_Date,
ModuleEnrolment_LSB_ExternalId,
ModuleComponent_LSB_ExternalId,
ModuleSubComponent_LSB_ExternalId,
concat(Student_ID,'.',Module1,'.', Course, '.', Session, '.', Academic_Period, '.', Module_Session, '.', Semester, '.', Assessment,'.',Ext_Circ,'.',EC_Number, '.ECCase') as LSB_ExternalId
from 
extcir_joined
GO


