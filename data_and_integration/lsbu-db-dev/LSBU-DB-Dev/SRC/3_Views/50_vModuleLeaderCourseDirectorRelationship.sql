
CREATE OR ALTER   VIEW [SRC].[vModuleLeaderCourseDirectorRelationship]
AS
SELECT
	AADA.RunID,
	AADA.FullFileName,
	AADA.SrcSystem,
	AADA.ObjectName,
	AADA.RowNo,
	AADA.FileDateTime,
	CONCAT(AADA.QL_StudentID, '.', AADA.QL_AcadProgNameId,'.',AADA.QL_AOSPeriod,'.',AADA.QL_AcademicPeriod,'.',AP.QL_CourseDirector,'.CourseDirector') as LSB_ExternalID,
	NULL AS ModuleOffering_LSB_ExternalID,
	CONCAT(AP.QL_CourseDirector, '@lsbu.ac.uk') as Staff_LSB_ExternalID,
	AADA.FileRowNumber,
	null as HashMD5,
	AADA.QL_StudentID,
	AADA.QL_AcadProgNameId,
	AADA.QL_AcademicPeriod,
	AADA.QL_AOSPeriod,
	AADA.QL_ApplicationStatus,
	AADA.QL_AppStatusDateAchieved,
	AADA.QL_EnrollmentStatus,
	AADA.QL_EnrolStatusDateAchieved,
	AADA.QL_StudentEnrolledDate,
	AP.QL_CourseDirector,
	NULL AS QL_ModuleLeader
FROM SRC.vApplicationEnrolmentDetails_Affiliation AADA
INNER JOIN SRC.vAcademicProgram AP on AADA.QL_AcadProgNameId = AP.QL_AcadProgNameId AND AP.QL_CourseDirector IS NOT NULL
WHERE AADA.QL_AcadProgNameId IS NOT NULL AND AADA.QLCALC_AffiliationPrimaryFlag = 1 AND AADA.QLCALC_AffiliationRole = 'Student'

UNION ALL
SELECT
	T.RunID,
	T.FullFileName,
	T.SrcSystem,
	T.ObjectName,
	T.RowNo,
	T.FileDateTime,
	T.LSB_ExternalID,
	T.ModuleOffering_LSB_ExternalID ,
	T.Staff_LSB_ExternalID,
	T.FileRowNumber,
	T.HashMD5,
	T.QL_StudentID,
	T.QL_AcadProgNameId,
	T.QL_AcademicPeriod,
	T.QL_AOSPeriod,
	T.QL_ApplicationStatus,
	T.QL_AppStatusDateAchieved,
	T.QL_EnrollmentStatus,
	T.QL_EnrolStatusDateAchieved,
	T.QL_StudentEnrolledDate,
	T.L_CourseDirector,
	T.QL_ModuleLeader

FROM (
		SELECT
			AADA.RunID,
			AADA.FullFileName,
			AADA.SrcSystem,
			AADA.ObjectName,
			AADA.RowNo,
			AADA.FileDateTime,
			CONCAT(AADA.QL_StudentID,'.',MO.QL_ModuleId,'.',MO.QL_ModuleLeader,'.ModuleLeader') as LSB_ExternalID,
			MED.ModuleOffering_LSB_ExternalID ,
			CONCAT(MO.QL_ModuleLeader, '@lsbu.ac.uk') as Staff_LSB_ExternalID,
			AADA.FileRowNumber,
			null as HashMD5,
			AADA.QL_StudentID,
			AADA.QL_AcadProgNameId,
			AADA.QL_AcademicPeriod,
			AADA.QL_AOSPeriod,
			AADA.QL_ApplicationStatus,
			AADA.QL_AppStatusDateAchieved,
			AADA.QL_EnrollmentStatus,
			AADA.QL_EnrolStatusDateAchieved,
			AADA.QL_StudentEnrolledDate,
			NULL AS L_CourseDirector,
			MO.QL_ModuleLeader AS QL_ModuleLeader,
			ROW_NUMBER () OVER ( partition by AADA.QL_StudentID,MO.QL_ModuleId,MO.QL_ModuleLeader ORDER BY AADA.QL_AcademicPeriod DESC) MLRank
		FROM STG.tApplicationEnrolmentDetails_Affiliation AADA
		INNER JOIN STG.tModuleEnrolmentDetails MED on AADA.QL_AcadProgNameId = MED.QL_AcadProgNameId AND MED.QL_AOSPeriod = AADA.QL_AOSPeriod AND MED.QL_AcademicPeriod = AADA.QL_AcademicPeriod AND MED.QL_EnrollmentStatus = 'EFE'
		INNER JOIN STG.tModuleOffering MO ON MED.ModuleOffering_LSB_ExternalID = MO.LSB_ExternalID AND MO.QL_ModuleLeader is NOT NULL AND MO.QL_AcademicPeriod = AADA.QL_AcademicPeriod
		WHERE AADA.QL_AcadProgNameId IS NOT NULL AND AADA.QLCALC_AffiliationPrimaryFlag = 1 AND AADA.QLCALC_AffiliationRole = 'Student'
) T WHERE T.MLRank = 1
GO


