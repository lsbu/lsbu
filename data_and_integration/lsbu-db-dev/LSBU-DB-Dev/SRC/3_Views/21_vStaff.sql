CREATE OR ALTER VIEW [SRC].[vStaff] AS
SELECT
RunID,
FullFileName,
SrcSystem,
ObjectName,
RowNo,
FileDateTime,
col1 as LSB_ExternalID,
FileRowNumber,
HASHBYTES('MD5', CONCAT(col1, col2, col3, col4, col5, col6)) as HashMD5,
col1 as QL_StaffID,
lower(col2) as QL_StaffUsername,
lower(col3) as QL_StaffEmailAddress,
col4 as QL_StaffFirstName,
col5 as QL_StaffLastName,
col6 as QL_Gender
FROM SRC.SRC_ROW_TMP
WHERE ObjectName = 'Staff'