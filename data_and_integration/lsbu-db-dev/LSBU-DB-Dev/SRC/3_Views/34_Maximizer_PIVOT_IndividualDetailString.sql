CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_IndividualDetailString] AS
SELECT DISTINCT
I.Id,
I1.Value as [ARCHIVED_FIELDS_Assessing_LEA_Name],
I2.Value as [ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Notes],
I3.Value as [ARCHIVED_FIELDS_Duration_of_temporary_disability],
I4.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Ed_Psych_Invoice_number],
I5.Value as [ARCHIVED_FIELDS_Email_Address_2],
I6.Value as [ARCHIVED_FIELDS_First_language],
I7.Value as [ARCHIVED_FIELDS_Lockers_Locker_notes],
I8.Value as [ARCHIVED_FIELDS_Note_Taker_initial_hours],
I9.Value as [ARCHIVED_FIELDS_Note_Taker_s_],
I10.Value as [ARCHIVED_FIELDS_Old_fields_Dyslexia_Tutor_date_Allocated],
I11.Value as [ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Hours],
I12.Value as [ARCHIVED_FIELDS_Old_fields_Library_Support_Worker],
I13.Value as [ARCHIVED_FIELDS_Old_fields_Mentor_date_Allocated],
I14.Value as [ARCHIVED_FIELDS_Old_fields_MENTOR_NOTES],
I15.Value as [ARCHIVED_FIELDS_Old_fields_OLD_dont_use_Pre_Entry___notes],
I16.Value as [ARCHIVED_FIELDS_Old_fields_Other_Support],
I17.Value as [ARCHIVED_FIELDS_Old_fields_Tutor_Support_Notes],
I18.Value as [ARCHIVED_FIELDS_Recommendations_or_Comments],
I19.Value as [Area_Of_Study_Course_Code_],
I20.Value as [Attend_Mode],
I21.Value as [Confidential_Information],
I22.Value as [Course_In_Full],
I23.Value as [Covid_19_Fields_Coronavirus_questions_Other_coronavirus_detail__please_specify_],
I24.Value as [Covid_19_Fields_Covid_plan_created_notes],
I25.Value as [Covid_19_Fields_Further_information_Anything_else_to_know_before_we_book_appointment],
I26.Value as [Covid_19_Fields_Risk_information_Other_risk__please_specify_],
I27.Value as [Covid_19_Fields_Travel_Other_travel_detail__please_specify_],
I28.Value as [DDS_Consent_to_Share__DDS__Contact_Email],
I29.Value as [DDS_Consent_to_Share__DDS__Contact_name],
I30.Value as [DDS_Consent_to_Share__DDS__Contact_Phone],
I31.Value as [DDS_Consent_to_Share__DDS__Relationship_to_Student],
I32.Value as [DDS_DSA_CLASS_Needs_Assessment_time],
I33.Value as [DDS_DSA_CRN],
I34.Value as [DDS_DSA_DSA_Application_notes],
I35.Value as [DDS_DSA_NHS_Bursary_No],
I36.Value as [DDS_Lockers_Lockers_Locker_Number],
I37.Value as [DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_hours_agreed],
I38.Value as [DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_total_cost],
I39.Value as [DDS_PRE_ENTRY_Enrolment_Notes],
I40.Value as [DDS_PRE_ENTRY_Notes],
I41.Value as [DDS_Screening_and_Ed_Psych_Assessment_Feedback_appointment_time],
I42.Value as [DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment],
I43.Value as [DDS_Support_Arrangements_Assignment_Arrangement_Notes],
I44.Value as [DDS_Support_Arrangements_Examination_Arrangements_Notes],
I45.Value as [DDS_Support_Arrangements_For_Information_notes],
I46.Value as [DDS_Support_Arrangements_Nature_of_Disability],
I47.Value as [DDS_Support_Arrangements_Presentation_Arrangements_Notes],
I48.Value as [DDS_Support_Arrangements_QL_Disability_Fields_Disability_Allowance],
I49.Value as [DDS_Support_Arrangements_QL_Disability_Fields_Disability_Type],
I50.Value as [DDS_Support_Arrangements_Teaching_Arrangements_Notes],
I51.Value as [MH__WB_Counselling_Referrals_Corenet_ID],
I52.Value as [MH__WB_MH__WB_Current_Risk_EP_Waiting_List_notes],
I53.Value as [MH__WB_MHWB_Archive_Name_of_person_referring],
I54.Value as [MH__WB_MHWB_Archive_Who_advised_student_of_MHWB_Service],
I55.Value as [Preferred_name__if_different_],
I56.Value as [QL_Records_Management_Academic_Year],
I57.Value as [QL_Records_Management_Applicant_Stage_Code],
I58.Value as [QL_Records_Management_Applicant_Stage_Date_Achieved],
I59.Value as [QL_Records_Management_Application_Academic_Year],
I60.Value as [QL_Records_Management_Country_Of_Residence],
I61.Value as [QL_Records_Management_Ethnic_Origin],
I62.Value as [QL_Records_Management_Gender],
I63.Value as [QL_Records_Management_Geo_Location],
I64.Value as [QL_Records_Management_Nationality],
I65.Value as [QL_Records_Management_Session_Code],
I66.Value as [QL_Records_Management_Stage_Code],
I67.Value as [School_Code],
I68.Value as [Student_ID]
FROM Maximizer.src.Individual I
LEFT JOIN Maximizer.src.Individual_DetailString I1 on I.Id = I1.IndividualId and I1.Name = 'ARCHIVED FIELDS\Assessing_LEA_Name*'
LEFT JOIN Maximizer.src.Individual_DetailString I2 on I.Id = I2.IndividualId and I2.Name = 'ARCHIVED FIELDS\DATA PROTECTION REQUESTS\DP Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I3 on I.Id = I3.IndividualId and I3.Name = 'ARCHIVED FIELDS\Duration of temporary disability'
LEFT JOIN Maximizer.src.Individual_DetailString I4 on I.Id = I4.IndividualId and I4.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Ed Psych Invoice number'
LEFT JOIN Maximizer.src.Individual_DetailString I5 on I.Id = I5.IndividualId and I5.Name = 'ARCHIVED FIELDS\Email_Address_2'
LEFT JOIN Maximizer.src.Individual_DetailString I6 on I.Id = I6.IndividualId and I6.Name = 'ARCHIVED FIELDS\First language'
LEFT JOIN Maximizer.src.Individual_DetailString I7 on I.Id = I7.IndividualId and I7.Name = 'ARCHIVED FIELDS\Lockers\Locker notes'
LEFT JOIN Maximizer.src.Individual_DetailString I8 on I.Id = I8.IndividualId and I8.Name = 'ARCHIVED FIELDS\Note Taker initial hours'
LEFT JOIN Maximizer.src.Individual_DetailString I9 on I.Id = I9.IndividualId and I9.Name = 'ARCHIVED FIELDS\Note Taker(s)'
LEFT JOIN Maximizer.src.Individual_DetailString I10 on I.Id = I10.IndividualId and I10.Name = 'ARCHIVED FIELDS\Old fields\Dyslexia Tutor date Allocated'
LEFT JOIN Maximizer.src.Individual_DetailString I11 on I.Id = I11.IndividualId and I11.Name = 'ARCHIVED FIELDS\Old fields\Library Support Initial Hours'
LEFT JOIN Maximizer.src.Individual_DetailString I12 on I.Id = I12.IndividualId and I12.Name = 'ARCHIVED FIELDS\Old fields\Library Support Worker'
LEFT JOIN Maximizer.src.Individual_DetailString I13 on I.Id = I13.IndividualId and I13.Name = 'ARCHIVED FIELDS\Old fields\Mentor date Allocated'
LEFT JOIN Maximizer.src.Individual_DetailString I14 on I.Id = I14.IndividualId and I14.Name = 'ARCHIVED FIELDS\Old fields\MENTOR NOTES'
LEFT JOIN Maximizer.src.Individual_DetailString I15 on I.Id = I15.IndividualId and I15.Name = 'ARCHIVED FIELDS\Old fields\OLD don''t use\Pre Entry - notes'
LEFT JOIN Maximizer.src.Individual_DetailString I16 on I.Id = I16.IndividualId and I16.Name = 'ARCHIVED FIELDS\Old fields\Other Support'
LEFT JOIN Maximizer.src.Individual_DetailString I17 on I.Id = I17.IndividualId and I17.Name = 'ARCHIVED FIELDS\Old fields\Tutor Support Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I18 on I.Id = I18.IndividualId and I18.Name = 'ARCHIVED FIELDS\Recommendations or Comments'
LEFT JOIN Maximizer.src.Individual_DetailString I19 on I.Id = I19.IndividualId and I19.Name = 'Area_Of_Study(Course_Code)*'
LEFT JOIN Maximizer.src.Individual_DetailString I20 on I.Id = I20.IndividualId and I20.Name = 'Attend_Mode*'
LEFT JOIN Maximizer.src.Individual_DetailString I21 on I.Id = I21.IndividualId and I21.Name = 'Confidential Information'
LEFT JOIN Maximizer.src.Individual_DetailString I22 on I.Id = I22.IndividualId and I22.Name = 'Course_In_Full*'
LEFT JOIN Maximizer.src.Individual_DetailString I23 on I.Id = I23.IndividualId and I23.Name = 'Covid-19 Fields\Coronavirus questions\Other coronavirus detail (please specify)'
LEFT JOIN Maximizer.src.Individual_DetailString I24 on I.Id = I24.IndividualId and I24.Name = 'Covid-19 Fields\Covid plan created notes'
LEFT JOIN Maximizer.src.Individual_DetailString I25 on I.Id = I25.IndividualId and I25.Name = 'Covid-19 Fields\Further information\Anything else to know before we book appointment?'
LEFT JOIN Maximizer.src.Individual_DetailString I26 on I.Id = I26.IndividualId and I26.Name = 'Covid-19 Fields\Risk information\Other risk (please specify)'
LEFT JOIN Maximizer.src.Individual_DetailString I27 on I.Id = I27.IndividualId and I27.Name = 'Covid-19 Fields\Travel\Other travel detail (please specify)'
LEFT JOIN Maximizer.src.Individual_DetailString I28 on I.Id = I28.IndividualId and I28.Name = 'DDS\Consent to Share (DDS)\Contact Email'
LEFT JOIN Maximizer.src.Individual_DetailString I29 on I.Id = I29.IndividualId and I29.Name = 'DDS\Consent to Share (DDS)\Contact name'
LEFT JOIN Maximizer.src.Individual_DetailString I30 on I.Id = I30.IndividualId and I30.Name = 'DDS\Consent to Share (DDS)\Contact Phone'
LEFT JOIN Maximizer.src.Individual_DetailString I31 on I.Id = I31.IndividualId and I31.Name = 'DDS\Consent to Share (DDS)\Relationship to Student'
LEFT JOIN Maximizer.src.Individual_DetailString I32 on I.Id = I32.IndividualId and I32.Name = 'DDS\DSA\CLASS Needs Assessment time'
LEFT JOIN Maximizer.src.Individual_DetailString I33 on I.Id = I33.IndividualId and I33.Name = 'DDS\DSA\CRN'
LEFT JOIN Maximizer.src.Individual_DetailString I34 on I.Id = I34.IndividualId and I34.Name = 'DDS\DSA\DSA Application notes'
LEFT JOIN Maximizer.src.Individual_DetailString I35 on I.Id = I35.IndividualId and I35.Name = 'DDS\DSA\NHS Bursary No.'
LEFT JOIN Maximizer.src.Individual_DetailString I36 on I.Id = I36.IndividualId and I36.Name = 'DDS\Lockers\Lockers\Locker Number'
LEFT JOIN Maximizer.src.Individual_DetailString I37 on I.Id = I37.IndividualId and I37.Name = 'DDS\LSBU funded NMH\Note Taking Support\Note taking hours agreed'
LEFT JOIN Maximizer.src.Individual_DetailString I38 on I.Id = I38.IndividualId and I38.Name = 'DDS\LSBU funded NMH\Note Taking Support\Note taking total cost'
LEFT JOIN Maximizer.src.Individual_DetailString I39 on I.Id = I39.IndividualId and I39.Name = 'DDS\PRE ENTRY\Enrolment Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I40 on I.Id = I40.IndividualId and I40.Name = 'DDS\PRE ENTRY\Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I41 on I.Id = I41.IndividualId and I41.Name = 'DDS\Screening and Ed Psych Assessment\Feedback appointment time'
LEFT JOIN Maximizer.src.Individual_DetailString I42 on I.Id = I42.IndividualId and I42.Name = 'DDS\Screening and Ed Psych Assessment\Time of EP Assessment'
LEFT JOIN Maximizer.src.Individual_DetailString I43 on I.Id = I43.IndividualId and I43.Name = 'DDS\Support Arrangements\Assignment Arrangement Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I44 on I.Id = I44.IndividualId and I44.Name = 'DDS\Support Arrangements\Examination Arrangements Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I45 on I.Id = I45.IndividualId and I45.Name = 'DDS\Support Arrangements\For Information notes'
LEFT JOIN Maximizer.src.Individual_DetailString I46 on I.Id = I46.IndividualId and I46.Name = 'DDS\Support Arrangements\Nature of Disability'
LEFT JOIN Maximizer.src.Individual_DetailString I47 on I.Id = I47.IndividualId and I47.Name = 'DDS\Support Arrangements\Presentation Arrangements Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I48 on I.Id = I48.IndividualId and I48.Name = 'DDS\Support Arrangements\QL Disability Fields\Disability_Allowance*'
LEFT JOIN Maximizer.src.Individual_DetailString I49 on I.Id = I49.IndividualId and I49.Name = 'DDS\Support Arrangements\QL Disability Fields\Disability_Type*'
LEFT JOIN Maximizer.src.Individual_DetailString I50 on I.Id = I50.IndividualId and I50.Name = 'DDS\Support Arrangements\Teaching Arrangements Notes'
LEFT JOIN Maximizer.src.Individual_DetailString I51 on I.Id = I51.IndividualId and I51.Name = 'MH & WB\Counselling Referrals\Corenet ID'
LEFT JOIN Maximizer.src.Individual_DetailString I52 on I.Id = I52.IndividualId and I52.Name = 'MH & WB\MH & WB Current Risk\EP Waiting List notes'
LEFT JOIN Maximizer.src.Individual_DetailString I53 on I.Id = I53.IndividualId and I53.Name = 'MH & WB\MHWB Archive\Name of person referring'
LEFT JOIN Maximizer.src.Individual_DetailString I54 on I.Id = I54.IndividualId and I54.Name = 'MH & WB\MHWB Archive\Who advised student of MH&WB Service'
LEFT JOIN Maximizer.src.Individual_DetailString I55 on I.Id = I55.IndividualId and I55.Name = 'Preferred name (if different)'
LEFT JOIN Maximizer.src.Individual_DetailString I56 on I.Id = I56.IndividualId and I56.Name = 'QL Records Management\Academic_Year*'
LEFT JOIN Maximizer.src.Individual_DetailString I57 on I.Id = I57.IndividualId and I57.Name = 'QL Records Management\Applicant_Stage_Code*'
LEFT JOIN Maximizer.src.Individual_DetailString I58 on I.Id = I58.IndividualId and I58.Name = 'QL Records Management\Applicant_Stage_Date_Achieved*'
LEFT JOIN Maximizer.src.Individual_DetailString I59 on I.Id = I59.IndividualId and I59.Name = 'QL Records Management\Application Academic Year*'
LEFT JOIN Maximizer.src.Individual_DetailString I60 on I.Id = I60.IndividualId and I60.Name = 'QL Records Management\Country_Of_Residence*'
LEFT JOIN Maximizer.src.Individual_DetailString I61 on I.Id = I61.IndividualId and I61.Name = 'QL Records Management\Ethnic_Origin*'
LEFT JOIN Maximizer.src.Individual_DetailString I62 on I.Id = I62.IndividualId and I62.Name = 'QL Records Management\Gender*'
LEFT JOIN Maximizer.src.Individual_DetailString I63 on I.Id = I63.IndividualId and I63.Name = 'QL Records Management\Geo_Location*'
LEFT JOIN Maximizer.src.Individual_DetailString I64 on I.Id = I64.IndividualId and I64.Name = 'QL Records Management\Nationality*'
LEFT JOIN Maximizer.src.Individual_DetailString I65 on I.Id = I65.IndividualId and I65.Name = 'QL Records Management\Session_Code*'
LEFT JOIN Maximizer.src.Individual_DetailString I66 on I.Id = I66.IndividualId and I66.Name = 'QL Records Management\Stage_Code*'
LEFT JOIN Maximizer.src.Individual_DetailString I67 on I.Id = I67.IndividualId and I67.Name = 'School Code*'
LEFT JOIN Maximizer.src.Individual_DetailString I68 on I.Id = I68.IndividualId and I68.Name = 'Student_ID*'