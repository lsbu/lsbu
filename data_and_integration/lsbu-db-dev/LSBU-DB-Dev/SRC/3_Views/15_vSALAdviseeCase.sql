CREATE OR ALTER VIEW [SRC].[vSALAdviseeCase] AS 
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	FileRowNumber,
	HASHBYTES('MD5', CONCAT(col1, col5, col7, col24, col25, col26, col27)) as HashMD5,
	col1 as LSB_ExternalID,
	col1 as QL_StudentID,
	col5 as QL_FirstName,
	col7 as QL_Surname,
	col24 as QL_Phone,
	col25 as QL_LSBUEmail,
	col26 as QL_PersonalEmail,
	col27 as QL_MobileNo
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'Student'