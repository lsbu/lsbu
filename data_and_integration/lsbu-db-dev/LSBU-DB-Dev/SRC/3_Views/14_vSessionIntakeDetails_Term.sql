CREATE OR ALTER   VIEW [SRC].[vSessionIntakeDetails_Term] AS
Select * from (SELECT
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	--Concatenate <QL_AcadProgNameId>.<QL_AcademicPeriod>.<QL_AOSPeriod>
	CONCAT(col1,'.',col4,'.',CASE
			WHEN CHARINDEX('NOV_AUG',col3) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col3) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col3) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col3) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col3) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col3) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col3) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col3) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col3) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col3) != 0 THEN 'DEC'
		   
		   END) as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6)) as HashMD5,
	col1 as QL_AcadProgNameId,
	col2 as QL_AOSPeriod,
	col3 as QL_Term,
	col4 as QL_AcademicPeriod,
	col5 as QL_SessionStartDate,
	col6 as QL_SessionEndDate,
	CASE
			WHEN CHARINDEX('NOV_AUG',col3) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col3) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col3) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col3) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col3) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col3) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col3) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col3) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col3) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col3) != 0 THEN 'DEC'
		   
		   END as QL_TermName,
	'SessionIntake' as QL_TermType,
	/*ROW_NUMBER () OVER ( partition by QL_AcadProgNameId.QL_AcademicPeriod.QL_AOSPeriod
	ORDER BY QL_AcadProgNameId.QL_AcademicPeriod.QL_AOSPeriod
	) AffiliationRank*/
	ROW_NUMBER () OVER ( partition by col4,col1,CASE
			WHEN CHARINDEX('NOV_AUG',col3) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col3) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col3) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col3) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col3) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col3) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col3) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col3) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col3) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col3) != 0 THEN 'DEC'
		   
		   END
	ORDER BY col4,col1,CASE
			WHEN CHARINDEX('NOV_AUG',col3) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col3) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col3) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col3) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col3) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col3) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col3) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col3) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col3) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col3) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col3) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col3) != 0 THEN 'DEC'
		   
		   END
	) TermRank

FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'SessionIntakeDetails'
) as Term where TermRank = 1

UNION ALL

Select * from (SELECT
RunID,
FullFileName,
SrcSystem,
ObjectName,
RowNo,
FileDateTime,
--<QL_AcademicPeriod>
col4 as LSB_ExternalID,
FileRowNumber,
HASHBYTES('MD5',col4) as HashMD5,
NULL as QL_AcadProgNameId,
NULL as QL_AOSPeriod,
NULL as QL_Term,
col4 as QL_AcademicPeriod,
NULL as QL_SessionStartDate,
NULL as QL_SessionEndDate,
NULL as QL_TermName,
'AcademicYear' as TermType,
/*ROW_NUMBER () OVER ( partition by QL_AcademicPeriod
ORDER BY QL_AcademicPeriod
) AffiliationRank*/
ROW_NUMBER () OVER ( partition by col4
ORDER BY col4
) TermRank

FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'SessionIntakeDetails'
) as Term where TermRank = 1