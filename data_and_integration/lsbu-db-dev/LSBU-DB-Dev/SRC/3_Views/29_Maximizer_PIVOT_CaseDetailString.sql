CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_CaseDetailString] AS
SELECT DISTINCT
C.CaseId,
C1.Value as [Fitness_to_Study_Level_1__Informal__Lead_person_name],
C2.Value as [MHWB_Duty_Action_s__needed],
C3.Value as [MHWB_Duty_Referral_to_other_services_detail],
C4.Value as [MHWB_Duty_Referred_by_other___detail]
FROM Maximizer.src.[Case] C
LEFT JOIN Maximizer.src.Case_DetailString C1 on C.CaseId = C1.CaseId and C1.Name = 'Fitness to Study\Level 1 (Informal)\Lead person name'
LEFT JOIN Maximizer.src.Case_DetailString C2 on C.CaseId = C2.CaseId and C2.Name = 'MHWB Duty\Action(s) needed'
LEFT JOIN Maximizer.src.Case_DetailString C3 on C.CaseId = C3.CaseId and C3.Name = 'MHWB Duty\Referral to other services detail'
LEFT JOIN Maximizer.src.Case_DetailString C4 on C.CaseId = C4.CaseId and C4.Name = 'MHWB Duty\Referred by other - detail'