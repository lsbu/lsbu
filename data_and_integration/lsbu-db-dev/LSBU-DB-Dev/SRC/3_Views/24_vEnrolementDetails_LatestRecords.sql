CREATE OR ALTER VIEW [SRC].[vEnrolementDetails_LatestRecords] AS
(  
  Select A.*,D.QL_ApplicationStatus,
  (CASE
    WHEN A.QL_AcademicPeriod = C.Max1QL_AcademicPeriod AND A.QL_EnrollmentStatus = 'EFE' THEN 'TRUE'
    ELSE 'FALSE'
  END) AS CurrentStudentEnrolment,
  (CASE
    WHEN A.QL_AcademicPeriod = B.MaxQL_AcademicPeriod AND A.QL_EnrollmentStatus = 'EFE' THEN 'TRUE'
    ELSE 'FALSE'
  END) AS CurrentStudentEnrolmentCourseBased,
  (CASE
    WHEN  QL_EnrollmentStatus IN  ('EWD','EDNE','EINTC','ETROC','EINTA','ESLEP','EINTH') THEN 'TRUE'
    ELSE 'FALSE'
  END) AS StudentCeased,
  (CASE
    WHEN A.QL_EnrollmentStatus NOT IN  ('EFE','EWD','EDNE','EINTC','ETROC','EINTA','ESLEP','EINTH') AND D.QL_ApplicationStatus IS NOT NULL  THEN 'TRUE'
    ELSE 'FALSE'
  END) AS IsApplicant
  from [STG].[tEnrolmentDetails] A
  INNER JOIN
  (
    Select QL_StudentId, QL_AcadProgNameid, MAX(QL_AcademicPeriod) as MaxQL_AcademicPeriod
    FROM [STG].[tEnrolmentDetails] group by QL_StudentId, QL_AcadProgNameid 
  ) B ON 
  A.QL_StudentID = B.QL_StudentID
  AND A.QL_AcadProgNameId = B.QL_AcadProgNameId
  AND A.QL_AcademicPeriod = B.MaxQL_AcademicPeriod
  INNER JOIN
  (
    Select QL_StudentId, MAX(QL_AcademicPeriod) as Max1QL_AcademicPeriod
    FROM [STG].[tEnrolmentDetails] group by QL_StudentId
  ) C ON A.QL_StudentID = C.QL_StudentID
  LEFT OUTER JOIN [STG].[tApplicationDetails_Application] D on
  A.QL_StudentID = D.QL_StudentID
  AND A.QL_AcadProgNameId = D.QL_AcadProgNameId
  AND A.QL_AcademicPeriod = D.QL_AcademicPeriod
  AND A.QL_AOSPeriod = D.QL_AOSPeriod
)
GO


