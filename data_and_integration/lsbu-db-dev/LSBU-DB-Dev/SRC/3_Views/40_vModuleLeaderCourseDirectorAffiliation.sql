CREATE OR ALTER VIEW [SRC].[vModuleLeaderCourseDirectorAffiliation]
AS
SELECT
	AP.RunID,
	AP.FullFileName,
	AP.SrcSystem,
	AP.ObjectName,
	AP.RowNo,
	AP.FileDateTime,
	CONCAT(AP.QL_CourseDirector,'.',AP.QL_AcadProgNameId) as LSB_ExternalID,
	AP.FileRowNumber,
	null as HashMD5,
	AP.QL_AcadProgNameId,
	AP.QL_AcadProgName,
	AP.QL_DepartmentId,
	AP.QL_EducationInstitutionId,
	AP.QL_UCASCourseCode,
	AP.QL_CourseAdministrator,
	AP.QL_CourseDirector,
	AP.QL_TypeofCourse
FROM STG.tAcademicProgram AP, STG.tModuleLeaderCourseDirector MLCD
WHERE AP.QL_CourseDirector = MLCD.QL_STAFFUSERNAME
AND AP.QL_CourseDirector IS NOT NULL