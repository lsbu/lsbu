CREATE OR ALTER VIEW [SRC].[vAppliedSciencesPTCD] AS
SELECT
RunID,
FullFileName,
SrcSystem,
ObjectName,
RowNo,
FileDateTime,
col1 as LSB_ExternalID,
FileRowNumber,
HASHBYTES('MD5', CONCAT(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col3)) as HashMD5,
col1 as PTCDCSV_StudentId,
col2 as PTCDCSV_StudentName,
col3 as PTCDCSV_StudentEmailAddress,
col4 as PTCDCSV_YearofStudy,
col5 as PTCDCSV_Session,
col6 as PTCDCSV_PersonalTutorFirstName,
col7 as PTCDCSV_PersonalTutorLastName,
col8 as PTCDCSV_PersonalTutorEmailAddress,
col9 as PTCDCSV_CourseCode,
col10 as PTCDCSV_CourseName,
col11 as PTCDCSV_CDFirstName,
col12 as PTCDCSV_CDLastname,
col13 as PTCDCSV_CDEmail
FROM SRC.SRC_ROW_TMP
WHERE ObjectName = 'AppliedSciencesPTCD'
GO