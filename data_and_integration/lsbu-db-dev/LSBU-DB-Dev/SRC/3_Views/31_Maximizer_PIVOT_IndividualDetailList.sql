CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_IndividualDetailList] AS
SELECT DISTINCT
I.Id,
I1.Value as [ARCHIVED_FIELDS_Disability_Category],
I2.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_form_completed],
I3.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_Money_received],
I4.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Assessment_funded_by],
I5.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_paid],
I6.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Refunded],
I7.Value as [ARCHIVED_FIELDS_Old_fields_BSL_interpreter_recommended],
I8.Value as [ARCHIVED_FIELDS_Old_fields_Campus_support_recommended],
I9.Value as [ARCHIVED_FIELDS_Old_fields_Lab_assistant_recommended],
I10.Value as [ARCHIVED_FIELDS_Old_fields_Library_assistant_recommended],
I11.Value as [ARCHIVED_FIELDS_Old_fields_Note_taker_recommeded],
I12.Value as [ARCHIVED_FIELDS_Old_fields_Scribe_recommended],
I13.Value as [ARCHIVED_FIELDS_Old_fields_Teacher_for_the_Deaf_recommended],
I14.Value as [ARCHIVED_FIELDS_PEEP_Not_required__please_tick_one_],
I15.Value as [ARCHIVED_FIELDS_PEEPs_2010_If_Yes_which_one],
I16.Value as [ARCHIVED_FIELDS_PEEPs_2010_PEEP_Prepared_by],
I17.Value as [ARCHIVED_FIELDS_Support_Arrangements__Sympathetic_Marking],
I18.Value as [DDS_Student_Record_Status],
I19.Value as [DDS_DSA_Mentoring_Support_Mentor_recommended],
I20.Value as [DDS_DSA_Mentoring_Support_Mentoring_Supplier],
I21.Value as [DDS_DSA_Study_Skills_Tuition_Dyslexia_tutor_recommended],
I22.Value as [DDS_DSA_Study_Skills_Tuition_Tutor_Supplier],
I23.Value as [DDS_PEEPs_Student_requires_a_PEEP___referred_to_HS],
I24.Value as [DDS_PRE_ENTRY_Allocated_to_DA],
I25.Value as [DDS_Screening_and_Ed_Psych_Assessment_Day_of_the_week_of_EP],
I26.Value as [DDS_Screening_and_Ed_Psych_Assessment_EP_Cost],
I27.Value as [DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_appointment_adviser],
I28.Value as [DDS_Screening_and_Ed_Psych_Assessment_Feedback_location],
I29.Value as [DDS_Support_Arrangements_DAs_initials],
I30.Value as [DDS_Support_Arrangements_QL_Disability_Fields_DDS_Support],
I31.Value as [Level_of_Study],
I32.Value as [MH__WB_In_Halls],
I33.Value as [MH__WB_MHWB_Archive_MHWB_Status],
I34.Value as [MH__WB_MHWB_Archive_Status]
FROM Maximizer.src.Individual I
LEFT JOIN Maximizer.src.Individual_DetailList I1 on I.Id = I1.IndividualId and I1.Name = 'ARCHIVED FIELDS\Disability Category'
LEFT JOIN Maximizer.src.Individual_DetailList I2 on I.Id = I2.IndividualId and I2.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\ALF form completed'
LEFT JOIN Maximizer.src.Individual_DetailList I3 on I.Id = I3.IndividualId and I3.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\ALF Money received'
LEFT JOIN Maximizer.src.Individual_DetailList I4 on I.Id = I4.IndividualId and I4.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Assessment funded by'
LEFT JOIN Maximizer.src.Individual_DetailList I5 on I.Id = I5.IndividualId and I5.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Deposit paid'
LEFT JOIN Maximizer.src.Individual_DetailList I6 on I.Id = I6.IndividualId and I6.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Deposit Refunded'
LEFT JOIN Maximizer.src.Individual_DetailList I7 on I.Id = I7.IndividualId and I7.Name = 'ARCHIVED FIELDS\Old fields\BSL interpreter recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I8 on I.Id = I8.IndividualId and I8.Name = 'ARCHIVED FIELDS\Old fields\Campus support recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I9 on I.Id = I9.IndividualId and I9.Name = 'ARCHIVED FIELDS\Old fields\Lab assistant recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I10 on I.Id = I10.IndividualId and I10.Name = 'ARCHIVED FIELDS\Old fields\Library assistant recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I11 on I.Id = I11.IndividualId and I11.Name = 'ARCHIVED FIELDS\Old fields\Note taker recommeded'
LEFT JOIN Maximizer.src.Individual_DetailList I12 on I.Id = I12.IndividualId and I12.Name = 'ARCHIVED FIELDS\Old fields\Scribe recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I13 on I.Id = I13.IndividualId and I13.Name = 'ARCHIVED FIELDS\Old fields\Teacher for the Deaf recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I14 on I.Id = I14.IndividualId and I14.Name = 'ARCHIVED FIELDS\PEEP Not required (please tick one)'
LEFT JOIN Maximizer.src.Individual_DetailList I15 on I.Id = I15.IndividualId and I15.Name = 'ARCHIVED FIELDS\PEEPs 2010\If Yes, which one?'
LEFT JOIN Maximizer.src.Individual_DetailList I16 on I.Id = I16.IndividualId and I16.Name = 'ARCHIVED FIELDS\PEEPs 2010\PEEP Prepared by:'
LEFT JOIN Maximizer.src.Individual_DetailList I17 on I.Id = I17.IndividualId and I17.Name = 'ARCHIVED FIELDS\Support Arrangements & Sympathetic Marking'
LEFT JOIN Maximizer.src.Individual_DetailList I18 on I.Id = I18.IndividualId and I18.Name = 'DDS Student Record Status'
LEFT JOIN Maximizer.src.Individual_DetailList I19 on I.Id = I19.IndividualId and I19.Name = 'DDS\DSA\Mentoring Support\Mentor recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I20 on I.Id = I20.IndividualId and I20.Name = 'DDS\DSA\Mentoring Support\Mentoring Supplier'
LEFT JOIN Maximizer.src.Individual_DetailList I21 on I.Id = I21.IndividualId and I21.Name = 'DDS\DSA\Study Skills Tuition\Dyslexia tutor recommended'
LEFT JOIN Maximizer.src.Individual_DetailList I22 on I.Id = I22.IndividualId and I22.Name = 'DDS\DSA\Study Skills Tuition\Tutor Supplier'
LEFT JOIN Maximizer.src.Individual_DetailList I23 on I.Id = I23.IndividualId and I23.Name = 'DDS\PEEPs\Student requires a PEEP - referred to H&S'
LEFT JOIN Maximizer.src.Individual_DetailList I24 on I.Id = I24.IndividualId and I24.Name = 'DDS\PRE ENTRY\Allocated to DA'
LEFT JOIN Maximizer.src.Individual_DetailList I25 on I.Id = I25.IndividualId and I25.Name = 'DDS\Screening and Ed Psych Assessment\Day of the week of EP'
LEFT JOIN Maximizer.src.Individual_DetailList I26 on I.Id = I26.IndividualId and I26.Name = 'DDS\Screening and Ed Psych Assessment\EP Cost'
LEFT JOIN Maximizer.src.Individual_DetailList I27 on I.Id = I27.IndividualId and I27.Name = 'DDS\Screening and Ed Psych Assessment\EP Feedback appointment adviser'
LEFT JOIN Maximizer.src.Individual_DetailList I28 on I.Id = I28.IndividualId and I28.Name = 'DDS\Screening and Ed Psych Assessment\Feedback location'
LEFT JOIN Maximizer.src.Individual_DetailList I29 on I.Id = I29.IndividualId and I29.Name = 'DDS\Support Arrangements\DA''s initials'
LEFT JOIN Maximizer.src.Individual_DetailList I30 on I.Id = I30.IndividualId and I30.Name = 'DDS\Support Arrangements\QL Disability Fields\DDS_Support*'
LEFT JOIN Maximizer.src.Individual_DetailList I31 on I.Id = I31.IndividualId and I31.Name = 'Level of Study'
LEFT JOIN Maximizer.src.Individual_DetailList I32 on I.Id = I32.IndividualId and I32.Name = 'MH & WB\In Halls?'
LEFT JOIN Maximizer.src.Individual_DetailList I33 on I.Id = I33.IndividualId and I33.Name = 'MH & WB\MHWB Archive\MHWB Status'
LEFT JOIN Maximizer.src.Individual_DetailList I34 on I.Id = I34.IndividualId and I34.Name = 'MH & WB\MHWB Archive\Status'