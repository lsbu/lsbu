CREATE OR ALTER VIEW [SRC].[vAppliedSciencesML] AS
SELECT
RunID,
FullFileName,
SrcSystem,
ObjectName,
RowNo,
FileDateTime,
NULL as LSB_ExternalID,
FileRowNumber,
HASHBYTES('MD5', CONCAT(col1, col2, col3, col4, col5, col6, col7)) as HashMD5,
col1 as MLCSV_CourseCode,
col2 as MLCSV_CourseName,
col3 as MLCSV_YearofStudy,
col4 as MLCSV_Session,
col5 as MLCSV_MLFirstName,
col6 as MLCSV_MLLastName,
col7 as MLCSV_MLEmail
FROM SRC.SRC_ROW_TMP
WHERE ObjectName = 'AppliedSciencesML'
GO