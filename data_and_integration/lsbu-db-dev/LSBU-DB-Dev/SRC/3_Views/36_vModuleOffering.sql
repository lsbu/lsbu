CREATE OR ALTER VIEW [SRC].[vModuleOffering] AS
SELECT 
	RunID,
	FullFileName,
	SrcSystem,
	ObjectName,
	RowNo,
	FileDateTime,
	CONCAT(col1, '.', col3, '.', col4) as LSB_ExternalID,
	FileRowNumber,
	HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8)) as HashMD5,
	col1 as QL_ModuleId,
	col2 as QL_ModuleInstanceName,
	col3 as QL_ModuleInstance,
	col4 as QL_AcademicPeriod,
	col5 as QL_ModuleStartDate,
	col6 as QL_ModuleEndDate,
	col7 as QL_ModuleCredit,
	lower(col8) as QL_ModuleLeader
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'ModuleOffering'