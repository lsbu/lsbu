CREATE OR ALTER VIEW [SRC].[vStudent_SupportProfile] AS
SELECT 
  RunID,
  FullFileName,
  SrcSystem,
  ObjectName,
  RowNo,
  FileDateTime,
  QL_StudentID + '.SP' AS LSB_ExternalID,
  FileRowNumber,
  HASHBYTES('MD5',
    CONCAT(
      QL_Disabilty,
      QL_DisabiltyAllowance,
      QL_CareLeaver
    )
  ) AS HashMD5,
  QL_StudentID,
  QL_Disabilty,
  QL_DisabiltyAllowance,
  QL_CareLeaver
FROM [STG].[tStudent]
WHERE (QL_Disabilty IS NOT NULL
  OR QL_DisabiltyAllowance IS NOT NULL
  OR QL_CareLeaver IS NOT NULL)
GO
