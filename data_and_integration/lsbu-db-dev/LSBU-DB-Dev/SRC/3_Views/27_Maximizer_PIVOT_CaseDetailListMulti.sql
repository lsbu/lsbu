CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_CaseDetailListMulti] AS
SELECT DISTINCT
C.CaseId,
C1.Value as [Fitness_to_Study_Initial_referral_Immediate_Suspension],
C2.Value as [Fitness_to_Study_Initial_referral_Nature_of_concern],
C3.Value as [Fitness_to_Study_Initial_referral_Outcome_of_referral],
C4.Value as [Fitness_to_Study_Initial_referral_Referrer],
C5.Value as [Fitness_to_Study_Level_1__Informal__Lead_person_role],
C6.Value as [Fitness_to_Study_Level_1__Informal__Outcome],
C7.Value as [Fitness_to_Study_Level_1__Informal__Signposted_Services],
C8.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_companion_attended],
C9.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_documents_provided],
C10.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_documents_requested],
C11.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_student_attended],
C12.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_student_confirmed],
C13.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Companion_attended],
C14.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_provided],
C15.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_requested],
C16.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_attended],
C17.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_confirmed],
C18.Value as [Fitness_to_Study_Level_2__Case_Conference__Outcome_Outcome],
C19.Value as [Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services],
C20.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Companion_attended],
C21.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_provided],
C22.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_requested],
C23.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Outcome],
C24.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Student_attended],
C25.Value as [Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Companion_attended],
C26.Value as [Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_provided],
C27.Value as [Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_requested],
C28.Value as [Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Student_attended],
C29.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_companion_attended],
C30.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_documents_provided],
C31.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_documents_requested],
C32.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_student_attended],
C33.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_student_confirmed],
C34.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Companion_attended],
C35.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_provided],
C36.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_requested],
C37.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_attended],
C38.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_confirmed],
C39.Value as [Fitness_to_Study_Level_3__Panel__Outcome_Outcome],
C40.Value as [Fitness_to_Study_Level_3__Panel__Outcome_Signposted_Services],
C41.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Companion_attended],
C42.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_provided],
C43.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_requested],
C44.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Outcome_of_Review],
C45.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Signposted_Services],
C46.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attedance_confirmed],
C47.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attended],
C48.Value as [Fitness_to_Study_Level_4__Return__Interruption_outside_FtS_Reason_for_interruption],
C49.Value as [Fitness_to_Study_Level_4__Return__Interruption_type],
C50.Value as [Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Response_from_student],
C51.Value as [Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Student_planning_to_return],
C52.Value as [Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Companion_attended],
C53.Value as [Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_provided],
C54.Value as [Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_requested],
C55.Value as [Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_attended],
C56.Value as [Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_confirmed],
C57.Value as [Fitness_to_Study_Review_of_decisions_COP_favourable_to_student_Completion_of_Procedures_letter_requested],
C58.Value as [Fitness_to_Study_Review_of_decisions_Director_of_SSE_decision],
C59.Value as [Fitness_to_Study_Review_of_decisions_Review_requested_by],
C60.Value as [MHWB_Duty_Action_s__taken_by_duty_advisor],
C61.Value as [MHWB_Duty_Any_previous_contact_with_MHWB],
C62.Value as [MHWB_Duty_Assigned_to_advisor],
C63.Value as [MHWB_Duty_Contact_details_confirmed_or_updated],
C64.Value as [MHWB_Duty_Current_safety_concerns_identified_Other_concern],
C65.Value as [MHWB_Duty_Current_safety_concerns_identified_Threat_from_others],
C66.Value as [MHWB_Duty_Current_safety_concerns_identified_Threat_to_others],
C67.Value as [MHWB_Duty_Current_safety_concerns_identified_Threat_to_self],
C68.Value as [MHWB_Duty_Date_of_contact_attempt_s__with_student_1st_contact_successful],
C69.Value as [MHWB_Duty_Date_of_contact_attempt_s__with_student_2nd_contact_successful],
C70.Value as [MHWB_Duty_Date_of_contact_attempt_s__with_student_3rd_contact_successful],
C71.Value as [MHWB_Duty_Method_of_duty_referral],
C72.Value as [MHWB_Duty_Nature_of_previous_contact],
C73.Value as [MHWB_Duty_Referred_by],
C74.Value as [Products_Services],
C75.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Advisor],
C76.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Alleged_perpetrator_LSBU_staff],
C77.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Alleged_perpetrator_LSBU_student],
C78.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_police],
C79.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_Student_Complaints],
C80.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_Student_Disciplinary],
C81.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Referral_route],
C82.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Escalate_concerns],
C83.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Initial_risk_and_support_plan_complete],
C84.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Safeguarding_concern],
C85.Value as [Safety_Concern_Response_Action_plan_Action_owners],
C86.Value as [Safety_Concern_Response_Action_plan_Action_plan_required],
C87.Value as [Safety_Concern_Response_Concern_raised_by],
C88.Value as [Safety_Concern_Response_Nature_of_concern_Covid_19],
C89.Value as [Safety_Concern_Response_Nature_of_concern_Covid_19_detail],
C90.Value as [Safety_Concern_Response_Nature_of_concern_Exacerbatingcontributing_factors],
C91.Value as [Safety_Concern_Response_Nature_of_concern_Exacerbatingcontributing_factors_detail],
C92.Value as [Safety_Concern_Response_Nature_of_concern_Other_concern],
C93.Value as [Safety_Concern_Response_Nature_of_concern_Other_concern_detail],
C94.Value as [Safety_Concern_Response_Nature_of_concern_Threat_from_others],
C95.Value as [Safety_Concern_Response_Nature_of_concern_Threat_from_others_detail],
C96.Value as [Safety_Concern_Response_Nature_of_concern_Threat_to_others],
C97.Value as [Safety_Concern_Response_Nature_of_concern_Threat_to_others_detail],
C98.Value as [Safety_Concern_Response_Nature_of_concern_Threat_to_self],
C99.Value as [Safety_Concern_Response_Nature_of_concern_Threat_to_self_detail],
C100.Value as [Safety_Concern_Response_Risk_Initial_risk_or_impact_level__at_time_of_concern_],
C101.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_1],
C102.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_2],
C103.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_3],
C104.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_4],
C105.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_5],
C106.Value as [Safety_Concern_Response_Safeguarding_concern],
C107.Value as [Safety_Concern_Response_Teams_people_involved_External],
C108.Value as [Safety_Concern_Response_Teams_people_involved_LSBU]
FROM INT.STG_MAX_CaseDetailListMulti C
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C1 on C.CaseId = C1.CaseId and C1.Name = 'Fitness to Study\Initial referral\Immediate Suspension'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C2 on C.CaseId = C2.CaseId and C2.Name = 'Fitness to Study\Initial referral\Nature of concern'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C3 on C.CaseId = C3.CaseId and C3.Name = 'Fitness to Study\Initial referral\Outcome of referral'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C4 on C.CaseId = C4.CaseId and C4.Name = 'Fitness to Study\Initial referral\Referrer'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C5 on C.CaseId = C5.CaseId and C5.Name = 'Fitness to Study\Level 1 (Informal)\Lead person role'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C6 on C.CaseId = C6.CaseId and C6.Name = 'Fitness to Study\Level 1 (Informal)\Outcome'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C7 on C.CaseId = C7.CaseId and C7.Name = 'Fitness to Study\Level 1 (Informal)\Signposted Services'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C8 on C.CaseId = C8.CaseId and C8.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\2nd companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C9 on C.CaseId = C9.CaseId and C9.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\2nd documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C10 on C.CaseId = C10.CaseId and C10.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\2nd documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C11 on C.CaseId = C11.CaseId and C11.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\2nd student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C12 on C.CaseId = C12.CaseId and C12.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\2nd student confirmed'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C13 on C.CaseId = C13.CaseId and C13.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\Companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C14 on C.CaseId = C14.CaseId and C14.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\Documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C15 on C.CaseId = C15.CaseId and C15.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\Documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C16 on C.CaseId = C16.CaseId and C16.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\Student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C17 on C.CaseId = C17.CaseId and C17.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\Student confirmed'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C18 on C.CaseId = C18.CaseId and C18.Name = 'Fitness to Study\Level 2 (Case Conference)\Outcome\Outcome'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C19 on C.CaseId = C19.CaseId and C19.Name = 'Fitness to Study\Level 2 (Case Conference)\Outcome\Signposted Services'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C20 on C.CaseId = C20.CaseId and C20.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C21 on C.CaseId = C21.CaseId and C21.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C22 on C.CaseId = C22.CaseId and C22.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C23 on C.CaseId = C23.CaseId and C23.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Outcome'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C24 on C.CaseId = C24.CaseId and C24.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C25 on C.CaseId = C25.CaseId and C25.Name = 'Fitness to Study\Level 2 (Case Conference)\Second review case conference\Companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C26 on C.CaseId = C26.CaseId and C26.Name = 'Fitness to Study\Level 2 (Case Conference)\Second review case conference\Documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C27 on C.CaseId = C27.CaseId and C27.Name = 'Fitness to Study\Level 2 (Case Conference)\Second review case conference\Documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C28 on C.CaseId = C28.CaseId and C28.Name = 'Fitness to Study\Level 2 (Case Conference)\Second review case conference\Student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C29 on C.CaseId = C29.CaseId and C29.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\2nd companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C30 on C.CaseId = C30.CaseId and C30.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\2nd documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C31 on C.CaseId = C31.CaseId and C31.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\2nd documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C32 on C.CaseId = C32.CaseId and C32.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\2nd student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C33 on C.CaseId = C33.CaseId and C33.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\2nd student confirmed'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C34 on C.CaseId = C34.CaseId and C34.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\Companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C35 on C.CaseId = C35.CaseId and C35.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\Documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C36 on C.CaseId = C36.CaseId and C36.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\Documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C37 on C.CaseId = C37.CaseId and C37.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\Student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C38 on C.CaseId = C38.CaseId and C38.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\Student confirmed'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C39 on C.CaseId = C39.CaseId and C39.Name = 'Fitness to Study\Level 3 (Panel)\Outcome\Outcome'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C40 on C.CaseId = C40.CaseId and C40.Name = 'Fitness to Study\Level 3 (Panel)\Outcome\Signposted Services'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C41 on C.CaseId = C41.CaseId and C41.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C42 on C.CaseId = C42.CaseId and C42.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C43 on C.CaseId = C43.CaseId and C43.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C44 on C.CaseId = C44.CaseId and C44.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Outcome of Review'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C45 on C.CaseId = C45.CaseId and C45.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Signposted Services'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C46 on C.CaseId = C46.CaseId and C46.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Student attedance confirmed'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C47 on C.CaseId = C47.CaseId and C47.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C48 on C.CaseId = C48.CaseId and C48.Name = 'Fitness to Study\Level 4 (Return)\Interruption outside FtS\Reason for interruption'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C49 on C.CaseId = C49.CaseId and C49.Name = 'Fitness to Study\Level 4 (Return)\Interruption type'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C50 on C.CaseId = C50.CaseId and C50.Name = 'Fitness to Study\Level 4 (Return)\Interruption under FtS\Response from student'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C51 on C.CaseId = C51.CaseId and C51.Name = 'Fitness to Study\Level 4 (Return)\Interruption under FtS\Student planning to return'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C52 on C.CaseId = C52.CaseId and C52.Name = 'Fitness to Study\Level 4 (Return)\Return Fitness to Study Panel\Companion attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C53 on C.CaseId = C53.CaseId and C53.Name = 'Fitness to Study\Level 4 (Return)\Return Fitness to Study Panel\Documents provided'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C54 on C.CaseId = C54.CaseId and C54.Name = 'Fitness to Study\Level 4 (Return)\Return Fitness to Study Panel\Documents requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C55 on C.CaseId = C55.CaseId and C55.Name = 'Fitness to Study\Level 4 (Return)\Return Fitness to Study Panel\Student attended'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C56 on C.CaseId = C56.CaseId and C56.Name = 'Fitness to Study\Level 4 (Return)\Return Fitness to Study Panel\Student confirmed'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C57 on C.CaseId = C57.CaseId and C57.Name = 'Fitness to Study\Review of decisions\COP favourable to student\Completion of Procedures letter requested'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C58 on C.CaseId = C58.CaseId and C58.Name = 'Fitness to Study\Review of decisions\Director of SSE decision'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C59 on C.CaseId = C59.CaseId and C59.Name = 'Fitness to Study\Review of decisions\Review requested by'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C60 on C.CaseId = C60.CaseId and C60.Name = 'MHWB Duty\Action(s) taken by duty advisor'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C61 on C.CaseId = C61.CaseId and C61.Name = 'MHWB Duty\Any previous contact with MHWB'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C62 on C.CaseId = C62.CaseId and C62.Name = 'MHWB Duty\Assigned to advisor'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C63 on C.CaseId = C63.CaseId and C63.Name = 'MHWB Duty\Contact details confirmed or updated'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C64 on C.CaseId = C64.CaseId and C64.Name = 'MHWB Duty\Current safety concerns identified\Other concern'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C65 on C.CaseId = C65.CaseId and C65.Name = 'MHWB Duty\Current safety concerns identified\Threat from others'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C66 on C.CaseId = C66.CaseId and C66.Name = 'MHWB Duty\Current safety concerns identified\Threat to others'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C67 on C.CaseId = C67.CaseId and C67.Name = 'MHWB Duty\Current safety concerns identified\Threat to self'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C68 on C.CaseId = C68.CaseId and C68.Name = 'MHWB Duty\Date of contact attempt(s) with student\1st contact successful?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C69 on C.CaseId = C69.CaseId and C69.Name = 'MHWB Duty\Date of contact attempt(s) with student\2nd contact successful?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C70 on C.CaseId = C70.CaseId and C70.Name = 'MHWB Duty\Date of contact attempt(s) with student\3rd contact successful?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C71 on C.CaseId = C71.CaseId and C71.Name = 'MHWB Duty\Method of duty referral'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C72 on C.CaseId = C72.CaseId and C72.Name = 'MHWB Duty\Nature of previous contact'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C73 on C.CaseId = C73.CaseId and C73.Name = 'MHWB Duty\Referred by'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C74 on C.CaseId = C74.CaseId and C74.Name = 'Products/Services'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C75 on C.CaseId = C75.CaseId and C75.Name = 'Report and Support\Sexual Violence Liaison Support\Advisor'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C76 on C.CaseId = C76.CaseId and C76.Name = 'Report and Support\Sexual Violence Liaison Support\Case Management\Alleged perpetrator LSBU staff'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C77 on C.CaseId = C77.CaseId and C77.Name = 'Report and Support\Sexual Violence Liaison Support\Case Management\Alleged perpetrator LSBU student'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C78 on C.CaseId = C78.CaseId and C78.Name = 'Report and Support\Sexual Violence Liaison Support\Case Management\Reported to police'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C79 on C.CaseId = C79.CaseId and C79.Name = 'Report and Support\Sexual Violence Liaison Support\Case Management\Reported to Student Complaints'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C80 on C.CaseId = C80.CaseId and C80.Name = 'Report and Support\Sexual Violence Liaison Support\Case Management\Reported to Student Disciplinary'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C81 on C.CaseId = C81.CaseId and C81.Name = 'Report and Support\Sexual Violence Liaison Support\Referral route'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C82 on C.CaseId = C82.CaseId and C82.Name = 'Report and Support\Sexual Violence Liaison Support\Risk assessment\Escalate concerns?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C83 on C.CaseId = C83.CaseId and C83.Name = 'Report and Support\Sexual Violence Liaison Support\Risk assessment\Initial risk and support plan complete'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C84 on C.CaseId = C84.CaseId and C84.Name = 'Report and Support\Sexual Violence Liaison Support\Risk assessment\Safeguarding concern?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C85 on C.CaseId = C85.CaseId and C85.Name = 'Safety Concern Response\Action plan\Action owners'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C86 on C.CaseId = C86.CaseId and C86.Name = 'Safety Concern Response\Action plan\Action plan required?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C87 on C.CaseId = C87.CaseId and C87.Name = 'Safety Concern Response\Concern raised by'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C88 on C.CaseId = C88.CaseId and C88.Name = 'Safety Concern Response\Nature of concern\Covid-19'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C89 on C.CaseId = C89.CaseId and C89.Name = 'Safety Concern Response\Nature of concern\Covid-19 detail'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C90 on C.CaseId = C90.CaseId and C90.Name = 'Safety Concern Response\Nature of concern\Exacerbating|contributing factors'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C91 on C.CaseId = C91.CaseId and C91.Name = 'Safety Concern Response\Nature of concern\Exacerbating|contributing factors detail'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C92 on C.CaseId = C92.CaseId and C92.Name = 'Safety Concern Response\Nature of concern\Other concern'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C93 on C.CaseId = C93.CaseId and C93.Name = 'Safety Concern Response\Nature of concern\Other concern detail'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C94 on C.CaseId = C94.CaseId and C94.Name = 'Safety Concern Response\Nature of concern\Threat from others'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C95 on C.CaseId = C95.CaseId and C95.Name = 'Safety Concern Response\Nature of concern\Threat from others detail'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C96 on C.CaseId = C96.CaseId and C96.Name = 'Safety Concern Response\Nature of concern\Threat to others'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C97 on C.CaseId = C97.CaseId and C97.Name = 'Safety Concern Response\Nature of concern\Threat to others detail'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C98 on C.CaseId = C98.CaseId and C98.Name = 'Safety Concern Response\Nature of concern\Threat to self'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C99 on C.CaseId = C99.CaseId and C99.Name = 'Safety Concern Response\Nature of concern\Threat to self detail'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C100 on C.CaseId = C100.CaseId and C100.Name = 'Safety Concern Response\Risk\Initial risk or impact level (at time of concern)'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C101 on C.CaseId = C101.CaseId and C101.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 1'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C102 on C.CaseId = C102.CaseId and C102.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 2'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C103 on C.CaseId = C103.CaseId and C103.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 3'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C104 on C.CaseId = C104.CaseId and C104.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 4'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C105 on C.CaseId = C105.CaseId and C105.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 5'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C106 on C.CaseId = C106.CaseId and C106.Name = 'Safety Concern Response\Safeguarding concern?'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C107 on C.CaseId = C107.CaseId and C107.Name = 'Safety Concern Response\Teams| people involved\External'
LEFT JOIN INT.STG_MAX_CaseDetailListMulti C108 on C.CaseId = C108.CaseId and C108.Name = 'Safety Concern Response\Teams| people involved\LSBU'