CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_IndividualDetailNumber] AS
SELECT DISTINCT
I.Id,
I1.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Cost],
I2.Value as [ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Amount],
I3.Value as [ARCHIVED_FIELDS_Note_taker_value],
I4.Value as [ARCHIVED_FIELDS_Old_fields_Interim_Support_Claimed_VALUE],
I5.Value as [ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Value],
I6.Value as [DDS_DSA_Mentoring_Support_Mentor_initial_value],
I7.Value as [DDS_DSA_Mentoring_Support_Total_Interim_mentoring_hours_used],
I8.Value as [DDS_DSA_Study_Skills_Tuition_Tutor_Initial_Value],
I9.Value as [MH__WB_MHWB_Archive_Number_of_sessions]
FROM Maximizer.src.Individual I
LEFT JOIN Maximizer.src.Individual_DetailNumber I1 on I.Id = I1.IndividualId and I1.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Cost'
LEFT JOIN Maximizer.src.Individual_DetailNumber I2 on I.Id = I2.IndividualId and I2.Name = 'ARCHIVED FIELDS\ED PSYCH old UDFs\Deposit Amount'
LEFT JOIN Maximizer.src.Individual_DetailNumber I3 on I.Id = I3.IndividualId and I3.Name = 'ARCHIVED FIELDS\Note taker value'
LEFT JOIN Maximizer.src.Individual_DetailNumber I4 on I.Id = I4.IndividualId and I4.Name = 'ARCHIVED FIELDS\Old fields\Interim Support Claimed VALUE'
LEFT JOIN Maximizer.src.Individual_DetailNumber I5 on I.Id = I5.IndividualId and I5.Name = 'ARCHIVED FIELDS\Old fields\Library Support Initial Value'
LEFT JOIN Maximizer.src.Individual_DetailNumber I6 on I.Id = I6.IndividualId and I6.Name = 'DDS\DSA\Mentoring Support\Mentor initial value £'
LEFT JOIN Maximizer.src.Individual_DetailNumber I7 on I.Id = I7.IndividualId and I7.Name = 'DDS\DSA\Mentoring Support\Total Interim mentoring hours used'
LEFT JOIN Maximizer.src.Individual_DetailNumber I8 on I.Id = I8.IndividualId and I8.Name = 'DDS\DSA\Study Skills Tuition\Tutor Initial Value £'
LEFT JOIN Maximizer.src.Individual_DetailNumber I9 on I.Id = I9.IndividualId and I9.Name = 'MH & WB\MHWB Archive\Number of sessions'