CREATE OR ALTER VIEW [SRC].[vAppliedSciencesPTCD_Relationship] AS
SELECT * FROM (
  SELECT
  AppliedSciencesPTCD.RunID,
  AppliedSciencesPTCD.FullFileName,
  AppliedSciencesPTCD.SrcSystem,
  AppliedSciencesPTCD.ObjectName,
  AppliedSciencesPTCD.RowNo,
  AppliedSciencesPTCD.FileDateTime,
  --CONCAT(QL_StaffId,'.',QL_StudentId) as LSB_ExternalID,	
  Concat( AppliedSciencesPTCD.col1,'.',LEFT(StaffUser.FederationIdentifier,CHARINDEX('@',StaffUser.FederationIdentifier)-1),'.PersonalTutor') as LSB_ExternalID,						
  AppliedSciencesPTCD.FileRowNumber,
  NULL as HashMD5,
  AppliedSciencesPTCD.col1 as PTCDCSV_StudentId,
  AppliedSciencesPTCD.col2 as PTCDCSV_StudentName,
  AppliedSciencesPTCD.col3 as PTCDCSV_StudentEmailAddress,
  AppliedSciencesPTCD.col4 as PTCDCSV_YearofStudy,
  AppliedSciencesPTCD.col5 as PTCDCSV_Session,
  StaffUser.Id as QL_PersonalTutorId,
  AppliedSciencesPTCD.col6 as PTCDCSV_PersonalTutorFirstName,
  AppliedSciencesPTCD.col7 as PTCDCSV_PersonalTutorLastName,
  AppliedSciencesPTCD.col8 as PTCDCSV_PersonalTutorEmailAddress,
  /*ROW_NUMBER () OVER ( partition by QL_StaffId
  ORDER BY QL_StaffId
  ) StaffRank*/
  ROW_NUMBER () OVER (PARTITION by StaffUser.Email,AppliedSciencesPTCD.col1 ORDER BY StaffUser.Email,AppliedSciencesPTCD.col1) StaffRank
  --Distict Records based on the Staff id
  FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD, [SFC].[Get_ID_User] StaffUser
  WHERE 
  (--AppliedSciencesPTCD.col8 = StaffUser.Email or 
  AppliedSciencesPTCD.col8 = StaffUser.FederationIdentifier)
    AND AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD' AND StaffUser.FederationIdentifier IS NOT NULL
) AS SRC_Relationship where StaffRank = 1