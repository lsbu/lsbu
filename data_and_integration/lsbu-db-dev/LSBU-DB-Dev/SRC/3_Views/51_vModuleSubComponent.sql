CREATE OR ALTER    VIEW [SRC].[vModuleSubComponent] AS
SELECT 
		RunID,
		FullFileName,
		SrcSystem,
		ObjectName,
		RowNo,
		FileDateTime,
		Concat(col1,'.',col2,'.',col3,'.',col6,'.',col9) as LSB_ExternalID,
		FileRowNumber,
		HASHBYTES('MD5',CONCAT(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11)) as HashMD5,
	  col1 as QL_ModuleId,
	  col2 as QL_ModuleInstance,
	  col3 as QL_AcademicPeriod,
	  col4 as QL_ModuleStartDate,
	  col5 as QL_ModuleEndDate,
	  col6 as QL_ComponentId,
	  col7 as QL_ComponentDesc,
	  col8 as QL_ComponentHandinDate,
	  col9 as QL_SubComponentId,
	  col10 as QL_SubComponentDesc,
	  col11 as QL_SubComponentHandinDate

FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'ModuleComponentandSubComponent' and col6 is not null and col9 is not null
GO


