CREATE OR ALTER VIEW [SRC].[vEducationInstitution_Department_Union] AS
SELECT DISTINCT QL_DepartmentId, QL_DepartmentName
FROM (
	SELECT DISTINCT D.QL_DepartmentId ,D.QL_DepartmentName
	FROM [STG].[tDepartment] D
		UNION ALL
	SELECT DISTINCT EI.QL_EducationInstitutionId as QL_DepartmentId, EI.QL_EducationInstitutionName as QL_DepartmentName
	FROM [STG].[tEducationalInstitution] EI
	) A
GO
