CREATE OR ALTER VIEW [SRC].[Maximizer_PIVOT_CaseDetailDate] AS
SELECT DISTINCT
C.CaseId,
C1.Value as [Fitness_to_Study_Initial_referral_Date_of_first_referral],
C2.Value as [Fitness_to_Study_Level_1__Informal__Date_of_informal_meeting],
C3.Value as [Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference],
C4.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_date_of_invitation],
C5.Value as [Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Date_of_invitation],
C6.Value as [Fitness_to_Study_Level_2__Case_Conference__Outcome_Date_of_follow_up],
C7.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_invitation],
C8.Value as [Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_review],
C9.Value as [Fitness_to_Study_Level_3__Panel__Date_of_Panel],
C10.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_date_of_invitation],
C11.Value as [Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Date_of_invitation],
C12.Value as [Fitness_to_Study_Level_3__Panel__Outcome_Date_of_follow_up],
C13.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_invitation],
C14.Value as [Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_Review_Panel],
C15.Value as [Fitness_to_Study_Level_4__Return__Interruption_outside_FtS_Date_of_interruption],
C16.Value as [Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Date_of_interruption],
C17.Value as [Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Scheduled_date_to_contact_about_return],
C18.Value as [Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Date_of_invitation],
C19.Value as [Fitness_to_Study_Review_of_decisions_COP_unfavourable_to_student_Date_COP_letter_provided],
C20.Value as [Fitness_to_Study_Review_of_decisions_Date_review_requested],
C21.Value as [Fitness_to_Study_Review_of_decisions_Date_student_informed_of_review_outcome],
C22.Value as [MHWB_Duty_Actions_complete_by_date],
C23.Value as [MHWB_Duty_Date_added_to_duty_list],
C24.Value as [MHWB_Duty_Date_left_duty_list],
C25.Value as [MHWB_Duty_Date_of_contact],
C26.Value as [MHWB_Duty_Date_of_contact_attempt_s__with_student_1st_contact_date],
C27.Value as [MHWB_Duty_Date_of_contact_attempt_s__with_student_2nd_contact_date],
C28.Value as [MHWB_Duty_If_staff_referral__confirmation_sent],
C29.Value as [Report_and_Support_Hate_Incident_Date_of_first_referral],
C30.Value as [Report_and_Support_Sexual_Violence_Liaison_Support_Date_of_first_referral],
C31.Value as [Safety_Concern_Response_Action_plan_Date_action_plan_agreed],
C32.Value as [Safety_Concern_Response_Action_plan_Date_action_plan_updated_1],
C33.Value as [Safety_Concern_Response_Action_plan_Date_action_plan_updated_2],
C34.Value as [Safety_Concern_Response_Date_added_to_meeting],
C35.Value as [Safety_Concern_Response_Date_left_meeting],
C36.Value as [Safety_Concern_Response_Date_of_initial_concern],
C37.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_1_date],
C38.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_2_date],
C39.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_3_date],
C40.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_4_date],
C41.Value as [Safety_Concern_Response_Risk_Risk_reviews_Risk_review_date_5]
FROM Maximizer.src.[Case] C
LEFT JOIN Maximizer.src.Case_DetailDate C1 on C.CaseId = C1.CaseId and C1.Name = 'Fitness to Study\Initial referral\Date of first referral'
LEFT JOIN Maximizer.src.Case_DetailDate C2 on C.CaseId = C2.CaseId and C2.Name = 'Fitness to Study\Level 1 (Informal)\Date of informal meeting'
LEFT JOIN Maximizer.src.Case_DetailDate C3 on C.CaseId = C3.CaseId and C3.Name = 'Fitness to Study\Level 2 (Case Conference)\Date of case conference'
LEFT JOIN Maximizer.src.Case_DetailDate C4 on C.CaseId = C4.CaseId and C4.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\2nd date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C5 on C.CaseId = C5.CaseId and C5.Name = 'Fitness to Study\Level 2 (Case Conference)\Invitation and attendance\Date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C6 on C.CaseId = C6.CaseId and C6.Name = 'Fitness to Study\Level 2 (Case Conference)\Outcome\Date of follow up'
LEFT JOIN Maximizer.src.Case_DetailDate C7 on C.CaseId = C7.CaseId and C7.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C8 on C.CaseId = C8.CaseId and C8.Name = 'Fitness to Study\Level 2 (Case Conference)\Review Case Conference\Date of review'
LEFT JOIN Maximizer.src.Case_DetailDate C9 on C.CaseId = C9.CaseId and C9.Name = 'Fitness to Study\Level 3 (Panel)\Date of Panel'
LEFT JOIN Maximizer.src.Case_DetailDate C10 on C.CaseId = C10.CaseId and C10.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\2nd date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C11 on C.CaseId = C11.CaseId and C11.Name = 'Fitness to Study\Level 3 (Panel)\Invitation and attendance\Date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C12 on C.CaseId = C12.CaseId and C12.Name = 'Fitness to Study\Level 3 (Panel)\Outcome\Date of follow up'
LEFT JOIN Maximizer.src.Case_DetailDate C13 on C.CaseId = C13.CaseId and C13.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C14 on C.CaseId = C14.CaseId and C14.Name = 'Fitness to Study\Level 3 (Panel)\Review Panel Level 3\Date of Review Panel'
LEFT JOIN Maximizer.src.Case_DetailDate C15 on C.CaseId = C15.CaseId and C15.Name = 'Fitness to Study\Level 4 (Return)\Interruption outside FtS\Date of interruption'
LEFT JOIN Maximizer.src.Case_DetailDate C16 on C.CaseId = C16.CaseId and C16.Name = 'Fitness to Study\Level 4 (Return)\Interruption under FtS\Date of interruption'
LEFT JOIN Maximizer.src.Case_DetailDate C17 on C.CaseId = C17.CaseId and C17.Name = 'Fitness to Study\Level 4 (Return)\Interruption under FtS\Scheduled date to contact about return'
LEFT JOIN Maximizer.src.Case_DetailDate C18 on C.CaseId = C18.CaseId and C18.Name = 'Fitness to Study\Level 4 (Return)\Return Fitness to Study Panel\Date of invitation'
LEFT JOIN Maximizer.src.Case_DetailDate C19 on C.CaseId = C19.CaseId and C19.Name = 'Fitness to Study\Review of decisions\COP unfavourable to student\Date COP letter provided'
LEFT JOIN Maximizer.src.Case_DetailDate C20 on C.CaseId = C20.CaseId and C20.Name = 'Fitness to Study\Review of decisions\Date review requested'
LEFT JOIN Maximizer.src.Case_DetailDate C21 on C.CaseId = C21.CaseId and C21.Name = 'Fitness to Study\Review of decisions\Date student informed of review outcome'
LEFT JOIN Maximizer.src.Case_DetailDate C22 on C.CaseId = C22.CaseId and C22.Name = 'MHWB Duty\Actions complete by date'
LEFT JOIN Maximizer.src.Case_DetailDate C23 on C.CaseId = C23.CaseId and C23.Name = 'MHWB Duty\Date added to ''duty list'''
LEFT JOIN Maximizer.src.Case_DetailDate C24 on C.CaseId = C24.CaseId and C24.Name = 'MHWB Duty\Date left ''duty list'''
LEFT JOIN Maximizer.src.Case_DetailDate C25 on C.CaseId = C25.CaseId and C25.Name = 'MHWB Duty\Date of contact'
LEFT JOIN Maximizer.src.Case_DetailDate C26 on C.CaseId = C26.CaseId and C26.Name = 'MHWB Duty\Date of contact attempt(s) with student\1st contact date'
LEFT JOIN Maximizer.src.Case_DetailDate C27 on C.CaseId = C27.CaseId and C27.Name = 'MHWB Duty\Date of contact attempt(s) with student\2nd contact date'
LEFT JOIN Maximizer.src.Case_DetailDate C28 on C.CaseId = C28.CaseId and C28.Name = 'MHWB Duty\If staff referral  confirmation sent'
LEFT JOIN Maximizer.src.Case_DetailDate C29 on C.CaseId = C29.CaseId and C29.Name = 'Report and Support\Hate Incident\Date of first referral'
LEFT JOIN Maximizer.src.Case_DetailDate C30 on C.CaseId = C30.CaseId and C30.Name = 'Report and Support\Sexual Violence Liaison Support\Date of first referral'
LEFT JOIN Maximizer.src.Case_DetailDate C31 on C.CaseId = C31.CaseId and C31.Name = 'Safety Concern Response\Action plan\Date action plan agreed'
LEFT JOIN Maximizer.src.Case_DetailDate C32 on C.CaseId = C32.CaseId and C32.Name = 'Safety Concern Response\Action plan\Date action plan updated 1'
LEFT JOIN Maximizer.src.Case_DetailDate C33 on C.CaseId = C33.CaseId and C33.Name = 'Safety Concern Response\Action plan\Date action plan updated 2'
LEFT JOIN Maximizer.src.Case_DetailDate C34 on C.CaseId = C34.CaseId and C34.Name = 'Safety Concern Response\Date added to meeting'
LEFT JOIN Maximizer.src.Case_DetailDate C35 on C.CaseId = C35.CaseId and C35.Name = 'Safety Concern Response\Date left meeting'
LEFT JOIN Maximizer.src.Case_DetailDate C36 on C.CaseId = C36.CaseId and C36.Name = 'Safety Concern Response\Date of initial concern'
LEFT JOIN Maximizer.src.Case_DetailDate C37 on C.CaseId = C37.CaseId and C37.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 1 date'
LEFT JOIN Maximizer.src.Case_DetailDate C38 on C.CaseId = C38.CaseId and C38.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 2 date'
LEFT JOIN Maximizer.src.Case_DetailDate C39 on C.CaseId = C39.CaseId and C39.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 3 date'
LEFT JOIN Maximizer.src.Case_DetailDate C40 on C.CaseId = C40.CaseId and C40.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review 4 date'
LEFT JOIN Maximizer.src.Case_DetailDate C41 on C.CaseId = C41.CaseId and C41.Name = 'Safety Concern Response\Risk\Risk reviews\Risk review date 5'