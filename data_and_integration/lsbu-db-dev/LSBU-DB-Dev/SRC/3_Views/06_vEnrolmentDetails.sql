CREATE OR ALTER VIEW [SRC].[vEnrolmentDetails] AS
SELECT 
  RunID,
  FullFileName,
  SrcSystem,
  ObjectName,
  RowNo,
  FileDateTime,
  --CONCAT(QL_StudentID,QL_AcadProgNameId,QL_AOSPeriod,QL_AcademicPeriod)
  CONCAT(col1,'.',col3,'.',col5,'.',col6) AS LSB_ExternalID,
  --CONCAT(QL_StudentID,QL_AcadProgNameId,QL_AOSPeriod,QL_AcademicPeriod)
  CONCAT(col1,'.',col3,'.',col5,'.',col6) AS Application_LSB_ExternalID,
  --CONCAT(QL_StudentID,QL_AcadProgNameId,QL_EnrolmentStageIndicator)
  CONCAT(col1,'.',col3,'.AP.',col10) AS Affiliation_LSB_ExternalID,
  CONCAT(col3,'.',col6,'.', 
         CASE
			WHEN CHARINDEX('NOV_AUG',col4) != 0 THEN 'NOV_AUG'
			WHEN CHARINDEX('OCT_AUG',col4) != 0 THEN 'OCT_AUG'
           WHEN CHARINDEX('JAN',col4) != 0 THEN 'JAN'
		   WHEN CHARINDEX('FEB',col4) != 0 THEN 'FEB'
		   WHEN CHARINDEX('MAR',col4) != 0 THEN 'MAR'
		   WHEN CHARINDEX('APR',col4) != 0 THEN 'APR'
		   WHEN CHARINDEX('MAY',col4) != 0 THEN 'MAY'
		   WHEN CHARINDEX('JUN',col4) != 0 THEN 'JUN'
		   WHEN CHARINDEX('JUN',col4) != 0 THEN 'JUL'
		   WHEN CHARINDEX('JUN',col4) != 0 THEN 'AUG'
		   WHEN CHARINDEX('SEPT',col4) != 0 THEN 'SEPT'
		   WHEN CHARINDEX('SEPT',col4) != 0 THEN 'SEP'
		   WHEN CHARINDEX('OCT',col4) != 0 THEN 'OCT'
           WHEN CHARINDEX('NOV',col4) != 0 THEN 'NOV'
		   WHEN CHARINDEX('DEC',col4) != 0 THEN 'DEC'
		   
		   END) as Term_LSB_ExternalID,
  FileRowNumber,
  HASHBYTES('MD5',CONCAT(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19)) AS HashMD5,
  col1 AS QL_StudentID,
  col2 AS QL_UCASPersonalID,
  col3 AS QL_AcadProgNameId,
  col4 AS QL_Term,
  col5 AS QL_AOSPeriod,
  col6 AS QL_AcademicPeriod,
  col7 AS QL_ProgramStartDate,
  col8 AS QL_ProgramEndDate,
  col9 AS QL_ModeOfStudy,
  col10 AS QL_EnrolmentStageIndicator,
  col11 AS QL_EnrollmentStatus,
  col12 AS QL_EnrolStatusDateAchieved,
  col13 AS QL_AttedenceType,
  col14 AS QL_SessionStartDate,
  col15 AS QL_SessionEndDate,
  col16 AS QL_CampusInformation,
  col17 AS QL_CreatedDate,
  col18 AS QL_LastModifiedDate,
  col19 AS QL_StudentGraduated,
  col20 AS QL_StudentEnrolledDate,
  col21 AS QL_LatestEnrolmentRecord,
  col22 as QL_StudentGraduatingFlag
FROM [SRC].[SRC_ROW_TMP]
WHERE ObjectName = 'EnrolmentDetails'