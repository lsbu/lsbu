CREATE OR ALTER VIEW [RPT].[SRCValidationsSummary_Cognos] AS
SELECT RV.[RunID]
	,RV.SrcSystem
	,RV.ObjectName
--	,ISNULL(RVI.[FULL_FILE_NAME],(SELECT max(FULL_FILE_NAME) FROM CLT_MAIN_LIST WHERE CLT_FILE_CODE = RV.CLT_FILE_CODE)) AS FULL_FILE_NAME
	,RV.ValidationResultID
	,RV.ValidationRuleCode
	,RV.ValidationRuleDescription
	,RV.ValidationResultDescription
	,RV.ValidationResultFlag
	,RVI.ValidationDetailsDescription
	,IIF(RVI.RunID is not null,1,0) AS ERROR_COUNT
	,RVI.FileRowNumber
	,RVI.LSB_ExternalID
	,RVI.FullFileName
--	,IIF(ROW_NUMBER() OVER (PARTITION BY RV.[RUN_ID],RV.[CLT_FILE_CODE],RV.[VALIDATION_RULE_CODE] 
--				ORDER BY RV.[VALIDATION_RESULT_ID]) = 1,1,0) RULE_COUNT
FROM [RPT].[SRCValidations] RV
LEFT JOIN [RPT].[SRCValidationsItems] RVI ON RV.RunID = RVI.RunID
	AND RV.SrcSystem = RVI.SrcSystem
	AND RV.ObjectName = RVI.ObjectName
	AND RV.ValidationResultID = RVI.ValidationResultID
WHERE RV.SrcSystem = 'Cognos'
GO