IF OBJECT_ID('[RPT].SRCValidations', 'U') IS NOT NULL 
	DROP TABLE [RPT].SRCValidations
GO

CREATE TABLE [RPT].[SRCValidations](
	[RunID] [int] NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[ValidationResultID] [varchar](20) NULL,
	[ValidationRuleCode] [varchar](100) NULL,
	[ValidationRuleDescription] [varchar](4000) NULL,
	[ValidationResultDescription] [varchar](4000) NULL,
	[ValidationResultFlag] [varchar](10) NULL
) ON [PRIMARY]
GO

CREATE INDEX idx1 ON [RPT].SRCValidations(RunID,[SrcSystem],[ObjectName]);
GO
