IF OBJECT_ID('[RPT].SRCValidationsItems', 'U') IS NOT NULL 
	DROP TABLE [RPT].SRCValidationsItems
GO

CREATE TABLE [RPT].[SRCValidationsItems](
	[RunID] [int] NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[FullFileName] [varchar](150) NULL,
	[ValidationResultID] [varchar](20) NULL,
	[ValidationDetailsDescription] [varchar](4000) NULL,
	[FileRowNumber] [numeric](10, 0) NULL,
	[LSB_ExternalID] [varchar](100) NULL
) ON [PRIMARY]
GO

CREATE INDEX idx1 ON [RPT].SRCValidationsItems(RunID,SrcSystem,ObjectName,ValidationResultID);
GO

CREATE INDEX idx2 ON [RPT].SRCValidationsItems(ValidationResultID);
GO