/****** Object:  Sequence [SrcValidResultID]    Script Date: 25.11.2019 15:35:22 ******/
DROP SEQUENCE if exists [RPT].[SrcValidResultID]
GO

/****** Object:  Sequence [SrcValidResultID]    Script Date: 25.11.2019 15:35:22 ******/
CREATE SEQUENCE [RPT].[SrcValidResultID] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO