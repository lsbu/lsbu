﻿$srcpath = "D:\Data\ql_data\new"
$dstpath_dev = "D:\Data\ql_dev_data\new"
$dstpath_sit = "D:\Data\ql_sit_data\new"
$dstpath_uat = "D:\Data\ql_uat_data\new"

$filterLists = @(Get-Date -format "yyyyMMdd")

 
$fileList = Get-ChildItem -Path $srcpath -Force -Recurse
#loop the source folder files to find the match

foreach ($file in $fileList)
{
foreach($filelist in $filterLists)
{
 #$key = $file.BaseName.Substring(0,8)
 
$splitFileName = $file.BaseName.Substring($file.BaseName.LastIndexOf('_')+1, 8)

 
if ($splitFileName -in $filelist)
{
$fileName = $file.Name
 
Copy-Item -Path $($file.FullName) -Destination $dstpath_dev
Copy-Item -Path $($file.FullName) -Destination $dstpath_sit
Copy-Item -Path $($file.FullName) -Destination $dstpath_uat
}
}
}