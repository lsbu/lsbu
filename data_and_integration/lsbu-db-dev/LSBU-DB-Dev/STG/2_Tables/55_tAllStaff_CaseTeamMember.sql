IF OBJECT_ID('[STG].[tAllStaff_CaseTeamMember]','U') IS NOT NULL
	DROP TABLE [STG].[tAllStaff_CaseTeamMember];
GO

CREATE TABLE [STG].[tAllStaff_CaseTeamMember](
	[StudentId] [varchar](4000) NULL,
	[FederationIdentifier] [varchar](4011) NULL,
	[CourseTutor] [int] NULL,
	[PersonalTutor] [int] NULL,
	[ModuleLeader] [int] NULL
)