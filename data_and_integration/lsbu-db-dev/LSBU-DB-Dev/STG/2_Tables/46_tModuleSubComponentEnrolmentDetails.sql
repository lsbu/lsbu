IF OBJECT_ID('[STG].[tModuleSubComponentEnrolmentDetails]','U') IS NOT NULL
	DROP TABLE [STG].[tModuleSubComponentEnrolmentDetails];
GO

CREATE TABLE [STG].[tModuleSubComponentEnrolmentDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[ModuleComponent_LSB_ExternalID] [varchar](8000) NOT NULL,
	[ModuleEnrolment_LSB_ExternalID] [varchar](8000) NOT NULL,
	[Term_LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentId] [varchar](4000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleInstance] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_ModuleStartDate] [varchar](4000) NULL,
	[QL_ModuleEndDate] [varchar](4000) NULL,
	[QL_ComponentId] [varchar](4000) NULL,
	[QL_ComponentDesc] [varchar](4000) NULL,
	[QL_ComponentHandinDate] [varchar](4000) NULL,
	[QL_SubComponentId] [varchar](4000) NULL,
	[QL_SubComponentDesc] [varchar](4000) NULL,
	[QL_SubComponentHandinDate] [varchar](4000) NULL
) 
GO


