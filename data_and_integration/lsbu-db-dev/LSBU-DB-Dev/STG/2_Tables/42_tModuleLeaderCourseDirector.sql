IF OBJECT_ID('[STG].[tModuleLeaderCourseDirector]','U') IS NOT NULL
	DROP TABLE [STG].[tModuleLeaderCourseDirector];
GO

CREATE TABLE [STG].[tModuleLeaderCourseDirector](
	[RUNID] [int] NULL,
	[FULLFILENAME] [varchar](150) NULL,
	[SRCSYSTEM] [varchar](100) NULL,
	[OBJECTNAME] [varchar](26) NOT NULL,
	[ROWNO] [int] NULL,
	[FILEDATETIME] [datetime] NULL,
	[LSB_EXTERNALID] [varchar](4011) NOT NULL,
	[FILEROWNUMBER] [int] NULL,
	[HASHMD5] [varbinary](8000) NULL,
	[QL_STAFFID] [varchar](4000) NULL,
	[QL_STAFFUSERNAME] [varchar](4000) NULL,
	[QL_STAFFEMAILADDRESS] [varchar](4011) NULL,
	[QL_STAFFFIRSTNAME] [varchar](4000) NULL,
	[QL_STAFFLASTNAME] [varchar](4000) NULL,
	[QL_GENDER] [varchar](4000) NULL
)