IF OBJECT_ID('[STG].[tAppliedSciencesPTCD_Relationship]','U') IS NOT NULL
	DROP TABLE [STG].[tAppliedSciencesPTCD_Relationship];
GO

CREATE TABLE [STG].[tAppliedSciencesPTCD_Relationship](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](4527) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [int] NULL,
	[PTCDCSV_StudentId] [varchar](4000) NULL,
	[PTCDCSV_StudentName] [varchar](4000) NULL,
	[PTCDCSV_StudentEmailAddress] [varchar](4000) NULL,
	[PTCDCSV_YearofStudy] [varchar](4000) NULL,
	[PTCDCSV_Session] [varchar](4000) NULL,
	[QL_PersonalTutorId] [varchar](18) NULL,
	[PTCDCSV_PersonalTutorFirstName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorLastName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorEmailAddress] [varchar](4000) NULL,
	[StaffRank] [bigint] NULL
)
GO