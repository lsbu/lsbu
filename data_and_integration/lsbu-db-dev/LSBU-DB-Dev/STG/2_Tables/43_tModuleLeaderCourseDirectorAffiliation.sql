IF OBJECT_ID('[STG].[tModuleLeaderCourseDirectorAffiliation]','U') IS NOT NULL
	DROP TABLE [STG].[tModuleLeaderCourseDirectorAffiliation];
GO

CREATE TABLE [STG].[tModuleLeaderCourseDirectorAffiliation](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [int] NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_AcadProgName] [varchar](4000) NULL,
	[QL_DepartmentId] [varchar](4000) NULL,
	[QL_EducationInstitutionId] [varchar](4000) NULL,
	[QL_UCASCourseCode] [varchar](4000) NULL,
	[QL_CourseAdministrator] [varchar](4000) NULL,
	[QL_CourseDirector] [varchar](4000) NULL,
	[QL_TypeofCourse] [varchar](4000) NULL
) 
GO


