IF OBJECT_ID('[STG].[tAppliedSciencesPTCD_Affiliation]','U') IS NOT NULL
	DROP TABLE [STG].[tAppliedSciencesPTCD_Affiliation];
GO

CREATE TABLE [STG].[tAppliedSciencesPTCD_Affiliation](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[PTCDCSV_CourseCode] [varchar](4000) NULL,
	[PTCDCSV_CourseName] [varchar](4000) NULL,
	[QL_CDStaffId] [varchar](4000) NULL,
	[PTCDCSV_CDFirstName] [varchar](4000) NULL,
	[PTCDCSV_CDLastname] [varchar](4000) NULL,
	[PTCDCSV_CDEmail] [varchar](4000) NULL,
	[StaffRank] [bigint] NULL
)
GO