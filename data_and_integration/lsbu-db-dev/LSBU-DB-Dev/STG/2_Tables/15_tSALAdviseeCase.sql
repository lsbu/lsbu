IF OBJECT_ID('[STG].[tSALAdviseeCase]','U') IS NOT NULL
	DROP TABLE [STG].[tSALAdviseeCase];
GO

CREATE TABLE [STG].[tSALAdviseeCase](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[LSB_ExternalID] [varchar](4000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_FirstName] [varchar](4000) NULL,
	[QL_Surname] [varchar](4000) NULL,
	[QL_Phone] [varchar](4000) NULL,
	[QL_LSBUEmail] [varchar](4000) NULL,
	[QL_PersonalEmail] [varchar](4000) NULL,
	[QL_MobileNo] [varchar](4000) NULL
)
GO