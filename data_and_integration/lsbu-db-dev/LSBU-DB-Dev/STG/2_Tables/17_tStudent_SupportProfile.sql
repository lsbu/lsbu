IF OBJECT_ID('[STG].[tStudent_SupportProfile]','U') IS NOT NULL
	DROP TABLE [STG].[tStudent_SupportProfile];
GO

CREATE TABLE [STG].[tStudent_SupportProfile](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](4003) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_Disabilty] [varchar](4000) NULL,
	[QL_DisabiltyAllowance] [varchar](4000) NULL,
	[QL_CareLeaver] [varchar](4000) NULL
)
GO