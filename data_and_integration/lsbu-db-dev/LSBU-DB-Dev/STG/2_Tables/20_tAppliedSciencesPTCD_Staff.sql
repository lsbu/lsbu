IF OBJECT_ID('[STG].[tAppliedSciencesPTCD_Staff]','U') IS NOT NULL
	DROP TABLE [STG].[tAppliedSciencesPTCD_Staff];
GO

CREATE TABLE [STG].[tAppliedSciencesPTCD_Staff](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](512) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StaffId] [varchar](80) NULL,
	[QL_StaffEmailAddress] [varchar](128) NULL,
	[QL_StaffFirstName] [varchar](40) NULL,
	[QL_StaffLastName] [varchar](80) NULL,
	[QL_Gender] [int] NULL,
	[SF_UserId] [varchar](18) NULL,
	[StaffRank] [bigint] NULL
)
GO