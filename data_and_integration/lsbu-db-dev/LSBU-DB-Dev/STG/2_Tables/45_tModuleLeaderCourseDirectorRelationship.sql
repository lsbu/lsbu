IF OBJECT_ID('[STG].[tModuleLeaderCourseDirectorRelationship]','U') IS NOT NULL
	DROP TABLE [STG].[tModuleLeaderCourseDirectorRelationship];
GO

CREATE TABLE [STG].[tModuleLeaderCourseDirectorRelationship](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[ModuleOffering_LSB_ExternalID] [varchar](8000) NULL,
	[Staff_LSB_ExternalID] [varchar](4011) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [int] NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_ApplicationStatus] [varchar](4000) NULL,
	[QL_AppStatusDateAchieved] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_StudentEnrolledDate] [varchar](4000) NULL,
	[QL_CourseDirector] [varchar](4000) NULL,
	[QL_ModuleLeader] [varchar](4000) NULL
)
GO


