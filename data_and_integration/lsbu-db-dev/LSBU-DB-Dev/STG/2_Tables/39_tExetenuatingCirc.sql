IF OBJECT_ID('[STG].[tExetenuatingCirc]','U') IS NOT NULL
	DROP TABLE [STG].[tExetenuatingCirc];
GO

CREATE TABLE [STG].[tExetenuatingCirc](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](18) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleInstance] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_Semester] [varchar](4000) NULL,
	[QL_Attempt] [varchar](4000) NULL,
	[QL_ComponentType] [varchar](4000) NULL,
	[QL_ECStatusFlag] [varchar](4000) NULL,
	[QL_ECStatusFlagDate] [varchar](4000) NULL
)
GO


