IF OBJECT_ID('[STG].[tSessionIntakeDetails_Term]','U') IS NOT NULL
	DROP TABLE [STG].[tSessionIntakeDetails_Term];
GO

CREATE TABLE [STG].[tSessionIntakeDetails_Term](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_SessionStartDate] [varchar](4000) NULL,
	[QL_SessionEndDate] [varchar](4000) NULL,
	[QL_TermName] [varchar](7) NULL,
	[QL_TermType] [varchar](13) NOT NULL,
	[TermRank] [bigint] NULL
)
GO