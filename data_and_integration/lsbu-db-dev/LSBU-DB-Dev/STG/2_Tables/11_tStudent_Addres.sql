IF OBJECT_ID('[STG].[tStudent_Address]','U') IS NOT NULL
	DROP TABLE [STG].[tStudent_Address];
GO

CREATE TABLE [STG].[tStudent_Address](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](4010) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_AddressID] [varchar](4010) NOT NULL,
	[QL_Add1] [varchar](4000) NULL,
	[QL_Add2] [varchar](4000) NULL,
	[QL_Add3] [varchar](4000) NULL,
	[QL_Add4] [varchar](4000) NULL,
	[QL_Add5] [varchar](4000) NULL,
	[QL_Postcode] [varchar](4000) NULL,
	[QL_Country] [varchar](4000) NULL,
	[QL_AddressType] [varchar](9) NOT NULL
)
GO