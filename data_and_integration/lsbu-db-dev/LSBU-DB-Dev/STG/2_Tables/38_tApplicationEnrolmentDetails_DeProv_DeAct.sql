IF OBJECT_ID('[STG].[tApplicationEnrolmentDetails_DeProv_DeAct]','U') IS NOT NULL
	DROP TABLE [STG].[tApplicationEnrolmentDetails_DeProv_DeAct];
GO

CREATE TABLE [STG].[tApplicationEnrolmentDetails_DeProv_DeAct](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[FileRowNumber] [int] NULL,
	[SF_UserID] [varchar](18) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QLCALC_ContactRole] [nvarchar](50) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_ApplicationStatus] [varchar](4000) NULL,
	[QL_AppStatusDateAchieved] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_StudentGraduated] [varchar](4000) NULL,
	[QLCALC_RowUpsertedDate] [datetime] NOT NULL,
	[QLCALC_DeProvisioningdate] [date] NULL,
	[QLCALC_DeProvisionedStatus] [varchar](14) NOT NULL,
	[QLCALC_DeProvisionedStatusDate] [int] NULL,
	[LSB_ExternalID] [varchar](4000) NULL,
	[HashMD5] [varbinary](8000) NULL
)


