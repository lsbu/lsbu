IF OBJECT_ID('[STG].[tStudentTeachingStaff_Contact]','U') IS NOT NULL
	DROP TABLE [STG].[tStudentTeachingStaff_Contact];
GO

CREATE TABLE [STG].[tStudentTeachingStaff_Contact](
	[RunID] [int] NULL,
	[FullFileName] [int] NULL,
	[SrcSystem] [varchar](4) NOT NULL,
	[ObjectName] [int] NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NOT NULL,
	[LSB_ExternalID] [varchar](21) NOT NULL,
	[Account_LSB_ExternalID] [varchar](4000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[CMIS_StudentID] [varchar](4000) NULL,
	[CMIS_AcadProgNameId] [varchar](25) NULL,
	[CMIS_AcademicPeriod] [varchar](4000) NULL,
	[CMIS_AOSPeriod] [varchar](4000) NULL,
	[CMIS_ModuleId] [varchar](4000) NULL,
	[CMIS_ModuleInstance] [varchar](4000) NULL,
	[CMIS_Semester] [varchar](4000) NULL,
	[CMIS_TeachingStaffId] [varchar](10) NULL,
	[CMIS_TeachingStaffFirstName] [varchar](80) NULL,
	[CMIS_TeachingStaffLastname] [varchar](80) NULL,
	[CMIS_TeachingStaffEmail] [varchar](21) NOT NULL
)