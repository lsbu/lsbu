IF OBJECT_ID('[STG].[tModule]','U') IS NOT NULL
	DROP TABLE [STG].[tModule];
GO

CREATE TABLE [STG].[tModule](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](4000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleName] [varchar](4000) NULL,
	[QL_DepartmentId] [varchar](4000) NULL,
	[QL_EducationInstitutionId] [varchar](4000) NULL,
	[QL_ModuleLevel] [varchar](4000) NULL,
	[QL_ModuleStatus] [varchar](4000) NULL
)