IF OBJECT_ID('[STG].[tApplicationEnrolmentDetails_Affiliation]','U') IS NOT NULL
	DROP TABLE [STG].[tApplicationEnrolmentDetails_Affiliation];
GO

CREATE TABLE [STG].[tApplicationEnrolmentDetails_Affiliation](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_ApplicationDate] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_TypeofCourse] [varchar](4000) NULL,
	[QL_DepartmentId] [varchar](4000) NULL,
	[QL_EducationInstitutionId] [varchar](4000) NULL,
	[QL_ApplicationStatus] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_ApplicantStageIndicator] [varchar](4000) NULL,
	[QL_EnrolmentStageIndicator] [varchar](4000) NULL,
	[QL_AppStatusDateAchieved] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_StudentGraduated] [varchar](4000) NULL,
	[QL_StudentEnrolledDate] [varchar](4000) NULL,
	[QLCALC_AffiliationStartDate] [date] NULL,
	[QLCALC_AffiliationEndDate] [date] NULL,
	[QLCALC_AffiliationStatus] [varchar](7) NULL,
	[QLCALC_AffiliationPrimaryFlag] [int] NULL,
	[QLCALC_AffiliationRole] [varchar](50) NULL,
	[QLCALC_ContactRole] [varchar](50) NULL
)
GO