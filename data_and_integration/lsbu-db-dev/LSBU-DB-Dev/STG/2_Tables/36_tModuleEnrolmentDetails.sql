IF OBJECT_ID('[STG].[tModuleEnrolmentDetails]','U') IS NOT NULL
	DROP TABLE [STG].[tModuleEnrolmentDetails];
GO

CREATE TABLE [STG].[tModuleEnrolmentDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[ModuleOffering_LSB_ExternalID] [varchar](8000) NOT NULL,
	[Term_LSB_ExternalID] [varchar](8000) NOT NULL,
	[ProgramEnrolment_LSB_ExternalID] [varchar](8000) NOT NULL,
	[Affiliation_LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleInstance] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_EnrolmentStageIndicator] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_Compulsory] [varchar](4000) NULL,
	[QL_Semester] [varchar](4000) NULL
)