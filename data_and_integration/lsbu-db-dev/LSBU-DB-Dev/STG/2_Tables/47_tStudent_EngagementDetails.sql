IF OBJECT_ID('[STG].[tStudent_EngagementDetails]','U') IS NOT NULL
	DROP TABLE [STG].[tStudent_EngagementDetails];
GO

CREATE TABLE [STG].[tStudent_EngagementDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[Cognos_StudentID] [varchar](4000) NULL,
	[Cognos_AcadProgNameId] [varchar](4000) NULL,
	[Cognos_Term] [varchar](4000) NULL,
	[Cognos_AOSPeriod] [varchar](4000) NULL,
	[Cognos_AcademicPeriod] [varchar](4000) NULL,
	[Cognos_TurnstileActivity] [varchar](4000) NULL,
	[Cognos_LibraryAccessActivity] [varchar](4000) NULL,
	[Cognos_MoodleActivity] [varchar](4000) NULL,
	[Cognos_Submissionssum] [varchar](4000) NULL
)