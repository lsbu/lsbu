IF OBJECT_ID('[STG].[tAppliedSciencesPTCD_CaseTeamMember]','U') IS NOT NULL
	DROP TABLE [STG].[tAppliedSciencesPTCD_CaseTeamMember];
GO

CREATE TABLE [STG].[tAppliedSciencesPTCD_CaseTeamMember](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [int] NULL,
	[PTCDCSV_StudentId] [varchar](4000) NULL,
	[PTCDCSV_StudentName] [varchar](4000) NULL,
	[PTCDCSV_StudentEmailAddress] [varchar](4000) NULL,
	[PTCDCSV_YearofStudy] [varchar](4000) NULL,
	[PTCDCSV_Session] [varchar](4000) NULL,
	[QL_PersonalTutorId] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorFirstName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorLastName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorEmailAddress] [varchar](4000) NULL,
	[QL_CDStaffId] [varchar](4000) NULL,
	[PTCDCSV_CDFirstName] [varchar](4000) NULL,
	[PTCDCSV_CDLastname] [varchar](4000) NULL,
	[PTCDCSV_CDEmail] [varchar](4000) NULL,
	[QL_MLStaffId] [varchar](4000) NULL,
	[PTCDCSV_MLFirstName] [varchar](4000) NULL,
	[PTCDCSV_MLLastname] [varchar](4000) NULL,
	[PTCDCSV_MLEmail] [varchar](4000) NULL,
	[StaffRank] [bigint] NULL
)
GO