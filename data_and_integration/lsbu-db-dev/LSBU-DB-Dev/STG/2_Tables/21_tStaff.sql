IF OBJECT_ID('[STG].[tStaff]','U') IS NOT NULL
	DROP TABLE [STG].[tStaff];
GO

CREATE TABLE [STG].[tStaff](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](4000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StaffID] [varchar](4000) NULL,
	[QL_StaffUsername] [varchar](4000) NULL,
	[QL_StaffEmailAddress] [varchar](4000) NULL,
	[QL_StaffFirstName] [varchar](4000) NULL,
	[QL_StaffLastName] [varchar](4000) NULL,
	[QL_Gender] [varchar](4000) NULL
)
GO