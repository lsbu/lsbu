IF OBJECT_ID('[STG].[tAppliedSciencesPTCD]','U') IS NOT NULL
	DROP TABLE [STG].[tAppliedSciencesPTCD];
GO

CREATE TABLE [STG].[tAppliedSciencesPTCD](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](4000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[PTCDCSV_StudentId] [varchar](4000) NULL,
	[PTCDCSV_StudentName] [varchar](4000) NULL,
	[PTCDCSV_StudentEmailAddress] [varchar](4000) NULL,
	[PTCDCSV_YearofStudy] [varchar](4000) NULL,
	[PTCDCSV_Session] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorFirstName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorLastName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorEmailAddress] [varchar](4000) NULL,
	[PTCDCSV_CourseCode] [varchar](4000) NULL,
	[PTCDCSV_CourseName] [varchar](4000) NULL,
	[PTCDCSV_CDFirstName] [varchar](4000) NULL,
	[PTCDCSV_CDLastname] [varchar](4000) NULL,
	[PTCDCSV_CDEmail] [varchar](4000) NULL
)
GO