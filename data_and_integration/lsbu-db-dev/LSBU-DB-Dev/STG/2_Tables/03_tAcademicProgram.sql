IF OBJECT_ID('[STG].[tAcademicProgram]','U') IS NOT NULL
	DROP TABLE [STG].[tAcademicProgram];
GO

CREATE TABLE [STG].[tAcademicProgram](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_AcadProgName] [varchar](4000) NULL,
	[QL_DepartmentId] [varchar](4000) NULL,
	[QL_EducationInstitutionId] [varchar](4000) NULL,
	[QL_Website] [varchar](4000) NULL,
	[QL_Phone] [varchar](4000) NULL,
	[QL_Add1] [varchar](4000) NULL,
	[QL_Add2] [varchar](4000) NULL,
	[QL_Add3] [varchar](4000) NULL,
	[QL_Add4] [varchar](4000) NULL,
	[QL_Add5] [varchar](4000) NULL,
	[QL_Postcode] [varchar](4000) NULL,
	[QL_Country] [varchar](4000) NULL,
	[QL_Description] [varchar](4000) NULL,
	[QL_CreatedDate] [varchar](4000) NULL,
	[QL_LastModifiedDate] [varchar](4000) NULL,
	[QL_UCASCourseCode] [varchar](4000) NULL,
	[QL_CourseAdministrator] [varchar](4000) NULL,
	[QL_CourseDirector] [varchar](4000) NULL,
	[QL_TypeofCourse] [varchar](4000) NULL
)
GO