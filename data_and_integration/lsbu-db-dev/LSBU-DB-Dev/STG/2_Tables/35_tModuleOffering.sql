IF OBJECT_ID('[STG].[tModuleOffering]','U') IS NOT NULL
	DROP TABLE [STG].[tModuleOffering];
GO

CREATE TABLE [STG].[tModuleOffering](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleInstanceName] [varchar](4000) NULL,
	[QL_ModuleInstance] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_ModuleStartDate] [varchar](4000) NULL,
	[QL_ModuleEndDate] [varchar](4000) NULL,
	[QL_ModuleCredit] [varchar](4000) NULL,
	[QL_ModuleLeader] [varchar](4000) NULL
)
GO