IF OBJECT_ID('[STG].[tStudentAdditionalDetails]','U') IS NOT NULL
	DROP TABLE [STG].[tStudentAdditionalDetails];
GO

CREATE TABLE [STG].[tStudentAdditionalDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](18) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_RefugeeStatus] [varchar](4000) NULL,
	[QL_CaringResponsibilities] [varchar](4000) NULL,
	[QL_BeeninCare] [varchar](4000) NULL,
	[QL_EnstrangedfromFamily] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL
)
GO


