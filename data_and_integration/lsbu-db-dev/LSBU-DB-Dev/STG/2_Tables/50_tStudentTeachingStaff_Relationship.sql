IF OBJECT_ID('[STG].[tStudentTeachingStaff_Relationship]','U') IS NOT NULL
	DROP TABLE [STG].[tStudentTeachingStaff_Relationship];
GO

CREATE TABLE [STG].[tStudentTeachingStaff_Relationship](
	[RunID] [int] NULL,
	[FullFileName] [int] NULL,
	[SrcSystem] [varchar](4) NOT NULL,
	[ObjectName] [int] NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NOT NULL,
	[LSB_ExternalID] [varchar](8000) NOT NULL,
	[ModuleOffering_LSB_ExternalID] [varchar](8000) NOT NULL,
	[TeachingStaff_LSB_ExternalID] [varchar](21) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[CMIS_StudentID] [varchar](10) NULL,
	[CMIS_AcadProgNameId] [varchar](25) NULL,
	[CMIS_AcademicPeriod] [varchar](8000) NULL,
	[CMIS_AOSPeriod] [varchar](20) NULL,
	[CMIS_ModuleId] [varchar](25) NULL,
	[CMIS_ModuleInstance] [varchar](8000) NULL,
	[CMIS_Semester] [varchar](25) NULL,
	[CMIS_TeachingStaffId] [varchar](10) NULL,
	[CMIS_TeachingStaffFirstName] [varchar](80) NULL,
	[CMIS_TeachingStaffLastname] [varchar](80) NULL,
	[CMIS_TeachingStaffEmail] [varchar](21) NOT NULL,
	[RNK] [bigint] NULL
)