IF OBJECT_ID('[STG].[tAppliedSciencesML]','U') IS NOT NULL
	DROP TABLE [STG].[tAppliedSciencesML];
GO

CREATE TABLE [STG].[tAppliedSciencesML](
	[RunID] [int] NULL,
	[FullFileName] [varchar](150) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [int] NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[MLCSV_CourseCode] [varchar](4000) NULL,
	[MLCSV_CourseName] [varchar](4000) NULL,
	[MLCSV_YearofStudy] [varchar](4000) NULL,
	[MLCSV_Session] [varchar](4000) NULL,
	[MLCSV_MLFirstName] [varchar](4000) NULL,
	[MLCSV_MLLastName] [varchar](4000) NULL,
	[MLCSV_MLEmail] [varchar](4000) NULL
)
GO