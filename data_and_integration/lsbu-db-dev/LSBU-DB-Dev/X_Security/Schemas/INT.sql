﻿IF NOT EXISTS ( SELECT  * FROM    sys.schemas  WHERE   name = N'INT' ) 

    EXEC('CREATE SCHEMA [INT]
    AUTHORIZATION [dbo]');
GO