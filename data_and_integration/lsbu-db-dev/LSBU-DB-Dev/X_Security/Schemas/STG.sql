﻿IF NOT EXISTS ( SELECT  * FROM    sys.schemas  WHERE   name = N'STG' ) 

    EXEC('CREATE SCHEMA [STG]
    AUTHORIZATION [dbo]');
GO