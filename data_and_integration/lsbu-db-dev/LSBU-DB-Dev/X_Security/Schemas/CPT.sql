﻿IF NOT EXISTS ( SELECT  * FROM    sys.schemas  WHERE   name = N'CPT' ) 

    EXEC('CREATE SCHEMA [CPT]
    AUTHORIZATION [dbo]');
GO