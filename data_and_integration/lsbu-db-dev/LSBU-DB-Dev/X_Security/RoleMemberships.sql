﻿ALTER ROLE [db_owner] ADD MEMBER [migr_config];
GO
ALTER ROLE [db_owner] ADD MEMBER [migr_source];
GO
ALTER ROLE [db_owner] ADD MEMBER [migr_report];
GO
ALTER ROLE [db_owner] ADD MEMBER [migr_destination];
GO
ALTER ROLE [db_owner] ADD MEMBER [migr_map];
GO
ALTER ROLE [db_owner] ADD MEMBER [migr_stage];
GO
ALTER ROLE [db_owner] ADD MEMBER [migr_dst];
