# Update DB from each script files located in GitHub
$mainpath = Split-Path $script:MyInvocation.MyCommand.Path

##all on CFG schema
$ParamsCFG = @{
   'ServerInstance' = 'MS-SLFR-DBS-01';
   'Database' = 'didev';
   'Username' = 'migr_config';
   'Password' = 'LSBU!@#4config';
}

# ------ sequence
Write-Host " "
Write-Host "----   Run script of all files in: "$mainpath"\sequence\*.sql -------"
$files = Get-ChildItem $mainpath"\sequence\*.sql"
foreach ($file in $files) {
Write-Host $file
Invoke-Sqlcmd @ParamsCFG -InputFile $file
}

# ------- tables
Write-Host " "
Write-Host "----  Run script of all files in: "$mainpath"\tables\*.sql -------"
$files = Get-ChildItem $mainpath"\tables\*.sql"
foreach ($file in $files) {
Write-Host $file
Invoke-Sqlcmd @ParamsCFG -InputFile $file
}

# ------- indexes
Write-Host " "
Write-Host "----  Run script of all files in: "$mainpath"\indexes\*.sql -------"
$files = Get-ChildItem $mainpath"\indexes\*.sql"
foreach ($file in $files) {
Write-Host $file
Invoke-Sqlcmd @ParamsCFG -InputFile $file
}

# ------  view
Write-Host " "
Write-Host "----  Run script of all files in: "$mainpath"\view\*.sql -------"
$files = Get-ChildItem $mainpath"\view\*.sql"
foreach ($file in $files) {
Write-Host $file
Invoke-Sqlcmd @ParamsCFG -InputFile $file
}


# ------functions  
Write-Host " "
Write-Host "----  Run script of all files in: "$mainpath"\validate\*.sql -------"
$files = Get-ChildItem $mainpath"\functions\*.sql"
foreach ($file in $files) {
Write-Host $file
Invoke-Sqlcmd @ParamsCFG -InputFile $file
}


# ---- Procedures
Write-Host " "
Write-Host "----  Run script of all files in: "$mainpath"\clts_validate\*.sql -------"
$files = Get-ChildItem $mainpath"\clts_validate\*.sql"
foreach ($file in $files) {
Write-Host $file
Invoke-Sqlcmd @Params -InputFile $file
}

exit 0
cd $mainpath
