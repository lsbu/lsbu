﻿IF OBJECT_ID('[SFC].[Get_ID_SupportProfile]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_SupportProfile];
GO

CREATE TABLE [SFC].[Get_ID_SupportProfile](
	[Id] [varchar](18) NULL,
	[LSB_SUE_Contact__c] [varchar](18) NOT NULL,
	[LSB_SUE_Status__c] [varchar](255) NULL,
	[LSB_SUE_ExternalId__c] [varchar](90) NULL,
	[LSB_SUE_SourceSystemId__c] [varchar](90) NULL,
	[LSB_SUE_SourceSystem__c] [varchar](90) NULL
)
GO
