IF OBJECT_ID('[SFC].[Get_ID_ProgramEnrollment]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_ProgramEnrollment];
GO

CREATE TABLE [SFC].[Get_ID_ProgramEnrollment] (
    [Id] [varchar](18) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Enrollment_Status__c] [varchar](255) NULL,
	[LSB_PEN_ExternalID__c] [varchar](90) NULL,
	[LSB_PEN_SourceSystem_ID__c] [varchar](90) NULL,
	[LSB_PEN_SourceSystem__c] [varchar](255) NULL
);