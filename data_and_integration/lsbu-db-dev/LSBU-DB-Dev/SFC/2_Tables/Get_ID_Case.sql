IF OBJECT_ID('[SFC].[Get_ID_Case]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_Case];
GO

CREATE TABLE [SFC].[Get_ID_Case](
	[Id] [varchar](18) NULL,
	[ContactId] [varchar](18) NULL,
	[RecordTypeId] [varchar](18) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LSB_CAS_ExternalID__c] [varchar](90) NULL,
	[LSB_CAS_SourceSystemID__c] [varchar](90) NULL,
	[LSB_CAS_SourceSystem__c] [varchar](255) NULL
)