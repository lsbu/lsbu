IF OBJECT_ID('[SFC].[Get_ID_SupportArrangement]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_SupportArrangement];
GO

CREATE TABLE [SFC].[Get_ID_SupportArrangement](
	[Id] [varchar](18) NULL,
	[OwnerId] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[LSB_SUT_Contact__c] [varchar](18) NULL,
	[LSB_SUT_Current__c] [bit] NULL,
	[LSB_SUT_ExternalId__c] [varchar](90) NULL,
	[LSB_SUT_SourceSystemId__c] [varchar](90) NULL,
	[LSB_SUT_SourceSystem__c] [varchar](90) NULL,
	[LSB_SUT_UpdatedQLSupportArrangement__c] [bit] NULL
)