IF OBJECT_ID('[SFC].[Get_ID_SupportArrangementMaster]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_SupportArrangementMaster];
GO

CREATE TABLE [SFC].[Get_ID_SupportArrangementMaster](
	[Id] [varchar](18) NULL,
	[OwnerId] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[LSB_SUM_SupportArrangementCategory__c] [varchar](255) NULL,
	[Object_Used_In__c] [varchar](255) NULL
)