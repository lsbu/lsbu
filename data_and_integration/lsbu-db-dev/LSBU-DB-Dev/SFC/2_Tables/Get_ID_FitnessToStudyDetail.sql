IF OBJECT_ID('[SFC].[Get_ID_FitnessToStudyDetail]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_FitnessToStudyDetail];
GO

CREATE TABLE [SFC].[Get_ID_FitnessToStudyDetail](
	[Id] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[RecordTypeId] [varchar](18) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[LSB_FIL_FitnessToStudy__c] [varchar](18) NULL,
	[LSB_FIL_COPLetterRequested__c] [bit] NULL,
	[LSB_FIL_CompanionAttended__c] [bit] NULL,
	[LSB_FIL_ContactedAboutReturn__c] [bit] NULL,
	[LSB_FIL_DateCOPLetterProvided__c] [datetime] NULL,
	[LSB_FIL_DateOfFirstReferral__c] [datetime] NULL,
	[LSB_FIL_DateOfFollowUp__c] [datetime] NULL,
	[LSB_FIL_DateOfInformalMeeting__c] [datetime] NULL,
	[LSB_FIL_DateOfInterruption__c] [datetime] NULL,
	[LSB_FIL_DateOfInvitation__c] [datetime] NULL,
	[LSB_FIL_DateOfPanel__c] [datetime] NULL,
	[LSB_FIL_DateOfReviewPanel__c] [datetime] NULL,
	[LSB_FIL_DateOfReview__c] [datetime] NULL,
	[LSB_FIL_DateReferredToDeputyVChancellor__c] [datetime] NULL,
	[LSB_FIL_DateReviewRequested__c] [datetime] NULL,
	[LSB_FIL_DirectorOfSSEDecision__c] [varchar](255) NULL,
	[LSB_FIL_DocumentsProvided__c] [bit] NULL,
	[LSB_FIL_DocumentsRequested__c] [bit] NULL,
	[LSB_FIL_ExternalId__c] [varchar](255) NULL,
	[LSB_FIL_FirstMeetingAtThisLevelOrAReview__c] [varchar](255) NULL,
	[LSB_FIL_ImmediateSuspension__c] [bit] NULL,
	[LSB_FIL_InterruptionType__c] [varchar](255) NULL,
	[LSB_FIL_LeadPersonName__c] [varchar](90) NULL,
	[LSB_FIL_LeadPersonRole__c] [varchar](255) NULL,
	[LSB_FIL_MeetingRescheduled__c] [varchar](255) NULL,
	[LSB_FIL_NatureOfConcern__c] [varchar](255) NULL,
	[LSB_FIL_Outcome__c] [varchar](255) NULL,
	[LSB_FIL_ReasonForInterruption__c] [varchar](4099) NULL,
	[LSB_FIL_Referrer__c] [varchar](4099) NULL,
	[LSB_FIL_ResponseFromStudent__c] [bit] NULL,
	[LSB_FIL_ReviewRequestedBy__c] [varchar](255) NULL,
	[LSB_FIL_ScheduledDateToContactAboutRetur__c] [datetime] NULL,
	[LSB_FIL_SignpostedServices__c] [varchar](4099) NULL,
	[LSB_FIL_SourceSystemId__c] [varchar](255) NULL,
	[LSB_FIL_SourceSystem__c] [varchar](255) NULL,
	[LSB_FIL_StudentAttendanceConfirmed__c] [varchar](255) NULL,
	[LSB_FIL_StudentAttended__c] [bit] NULL,
	[LSB_FIL_StudentConfirmed__c] [varchar](255) NULL,
	[LSB_FIL_StudentInformedOfReviewOutcome__c] [datetime] NULL,
	[LSB_FIL_StudentPlanningToReturn__c] [bit] NULL
)