IF OBJECT_ID('[SFC].[Get_ID_Affiliation]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_Affiliation];
GO

CREATE TABLE [SFC].[Get_ID_Affiliation](
	[Id] [varchar](18) NULL,
	[hed__EndDate__c] [datetime] NULL,
	[hed__StartDate__c] [datetime] NULL,
	[hed__Status__c] [varchar](255) NULL,
	[LSB_AON_ExternalID__c] [varchar](90) NULL,
	[LSB_AON_SourceSystemID__c] [varchar](90) NULL,
	[LSB_AON_SourceSystem__c] [varchar](255) NULL
) ON [PRIMARY]
GO


