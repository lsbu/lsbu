IF OBJECT_ID('[SFC].[Get_ID_Account]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_Account];
GO

CREATE TABLE [SFC].[Get_ID_Account] (
    [Id] [varchar](18) NULL,
	[Name] [varchar](255) NULL,
	[LSB_ACC_ExternalID__c] [varchar](90) NULL,
	[LSB_ACC_SourceSystemID__c] [varchar](90) NULL,
	[LSB_ACC_SourceSystem__c] [varchar](255) NULL
);