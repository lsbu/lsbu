IF OBJECT_ID('[SFC].[Get_ID_Address]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_Address];
GO

CREATE TABLE [SFC].[Get_ID_Address] (
    [Id] [varchar](18) NULL,
	[hed__Default_Address__c] [bit] NULL,
	[LSB_ADS_ExternalID__c] [varchar](90) NULL,
	[LSB_ADS_SourceSystemID__c] [varchar](90) NULL,
	[LSB_ADS_SourceSystem__c] [varchar](255) NULL
);