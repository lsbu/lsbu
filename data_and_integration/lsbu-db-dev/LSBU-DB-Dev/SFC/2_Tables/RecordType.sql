IF OBJECT_ID('[SFC].[RecordType]','U') IS NOT NULL
	DROP TABLE [SFC].[RecordType];
GO

CREATE TABLE [SFC].[RecordType](
	[Id] [varchar](18) NOT NULL,
	[Name] [varchar](80) NOT NULL,
	[DeveloperName] [varchar](80) NOT NULL,
	[NamespacePrefix] [varchar](15) NULL,
	[Description] [varchar](255) NULL,
	[BusinessProcessId] [varchar](18) NULL,
	[SobjectType] [varchar](40) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedById] [varchar](18) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedById] [varchar](18) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[SystemModstamp] [datetime] NOT NULL
) ON [PRIMARY]
GO