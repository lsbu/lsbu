IF OBJECT_ID('[SFC].[Get_ID_Application]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_Application];
GO

CREATE TABLE [SFC].[Get_ID_Application] (
    [Id] [varchar](18) NULL,
	[hed__Application_Date__c] [datetime] NULL,
	[hed__Application_Status__c] [varchar](255) NULL,
	[LSB_APP_ExternalID__c] [varchar](90) NULL,
	[LSB_APP_SourceSystemID__c] [varchar](90) NULL,
	[LSB_APP_SourceSystem__c] [varchar](255) NULL
);