IF OBJECT_ID('[SFC].[Get_ID_SupportArrangementDetail]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_SupportArrangementDetail];
GO

CREATE TABLE [SFC].[Get_ID_SupportArrangementDetail](
	[Id] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[LSB_SUD_SupportArrangementPlan__c] [varchar](18) NULL,
	[LSB_SUD_SupportArrangementMaster__c] [varchar](18) NULL,
	[LSB_SUD_Type__c] [varchar](1300) NULL,
	[LSB_SUD_Active__c] [bit] NULL,
	[LSB_SUD_Notes__c] [varchar](255) NULL,
	[LSB_SUD_SupportArrangementDetailName__c] [varchar](1300) NULL,
	[LSB_SUD_EndDate__c] [datetime] NULL,
	[LSB_SUD_StartDate__c] [datetime] NULL,
	[LSB_SUD_SupportCategory__c] [varchar](255) NULL,
	[LSB_SUD_ExternalId__c] [varchar](90) NULL,
	[LSB_SUD_SourceSystemId__c] [varchar](90) NULL,
	[LSB_SUD_SourceSystem__c] [varchar](90) NULL
)