IF OBJECT_ID('[SFC].[Get_ID_AppointmentAttendee]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_AppointmentAttendee];
GO

CREATE TABLE [SFC].[Get_ID_AppointmentAttendee](
	[Id] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[sfal__Appointment__c] [varchar](18) NULL,
	[sfal__AdviseeRecord__c] [varchar](18) NULL,
	[sfal__Attendee__c] [varchar](18) NULL,
	[sfal__Comments__c] [varchar](255) NULL,
	[sfal__Role__c] [varchar](255) NULL,
	[sfal__StatusComments__c] [varchar](255) NULL,
	[sfal__StatusUpdatedBy__c] [varchar](18) NULL,
	[sfal__StatusUpdatedDate__c] [datetime] NULL,
	[sfal__Status__c] [varchar](255) NULL,
	[LSB_AEE_AppointmentStartDate__c] [datetime] NULL,
	[LSB_AEE_Reminder15MinTriggerDate__c] [datetime] NULL,
	[LSB_AEE_PausedFlowInterviewGUID__c] [varchar](255) NULL,
	[LSB_AEE_AppointmentDateTime__c] [datetime] NULL,
	[LSB_AEE_ExternalId__c] [varchar](80) NULL,
	[LSB_AEE_SourceSystemId__c] [varchar](80) NULL,
	[LSB_AEE_SourceSystem__c] [varchar](80) NULL
) ON [PRIMARY]