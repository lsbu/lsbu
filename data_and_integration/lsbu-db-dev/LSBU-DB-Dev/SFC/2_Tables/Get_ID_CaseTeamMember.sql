IF OBJECT_ID('[SFC].[Get_ID_CaseTeamMember]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_CaseTeamMember];
GO

CREATE TABLE [SFC].[Get_ID_CaseTeamMember](
	[Id] [varchar](18) NULL,
	[ParentId] [varchar](18) NULL,
	[MemberId] [varchar](18) NULL,
	[TeamTemplateMemberId] [varchar](18) NULL,
	[TeamRoleId] [varchar](18) NULL,
	[TeamTemplateId] [varchar](18) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL
);