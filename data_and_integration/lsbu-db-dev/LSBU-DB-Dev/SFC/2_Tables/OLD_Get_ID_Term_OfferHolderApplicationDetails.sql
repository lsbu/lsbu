--IF OBJECT_ID('[SFC].[Get_ID_Term_OfferHolderApplicationDetails]','U') IS NOT NULL
--	DROP TABLE [SFC].[Get_ID_Term_OfferHolderApplicationDetails];
--GO

--CREATE TABLE [SFC].[Get_ID_Term_OfferHolderApplicationDetails](
--	[Id] [varchar](18) NOT NULL,
--	[IsDeleted] [bit] NOT NULL,
--	[Name] [varchar](80) NULL,
--	[CreatedDate] [datetime] NOT NULL,
--	[CreatedById] [varchar](18) NOT NULL,
--	[LastModifiedDate] [datetime] NOT NULL,
--	[LastModifiedById] [varchar](18) NOT NULL,
--	[SystemModstamp] [datetime] NOT NULL,
--	[LastViewedDate] [datetime] NULL,
--	[LastReferencedDate] [datetime] NULL,
--	[hed__Account__c] [varchar](18) NOT NULL,
--	[hed__End_Date__c] [datetime] NULL,
--	[hed__Start_Date__c] [datetime] NULL,
--	[hed__Grading_Period_Sequence__c] [real] NULL,
--	[hed__Instructional_Days__c] [real] NULL,
--	[hed__Parent_Term__c] [varchar](18) NULL,
--	[hed__Type__c] [varchar](255) NULL,
--	[LSB_TRM_ExternalID__c] [varchar](90) NULL,
--	[LSB_TRM_SourceSystemID__c] [varchar](90) NULL,
--	[LSB_TRM_SourceSystem__c] [varchar](255) NULL
--) ON [PRIMARY]
--GO


