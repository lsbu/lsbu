IF OBJECT_ID('[SFC].[Get_ID_Term]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_Term];
GO

CREATE TABLE [SFC].[Get_ID_Term](
	[Id] [varchar](18) NULL,
	[LSB_TRM_ExternalID__c] [varchar](90) NULL,
	[LSB_TRM_SourceSystemID__c] [varchar](90) NULL,
	[LSB_TRM_SourceSystem__c] [varchar](255) NULL
) ON [PRIMARY]
GO


