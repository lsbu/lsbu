IF OBJECT_ID('[SFC].[Get_ID_Topic]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_Topic];
GO

CREATE TABLE [SFC].[Get_ID_Topic](
	[Id] [varchar](18) NULL,
	[OwnerId] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[sfal__Label__c] [varchar](255) NULL,
	[sfal__ParentTopic__c] [varchar](18) NULL,
	[sfal__SortOrder__c] [real] NULL,
	[LSB_AvailableForStudent__c] [bit] NULL,
	[LSB_TIC_AppointmentSharingSetting__c] [varchar](255) NULL
)