IF OBJECT_ID('[SFC].[Get_ID_Relationship]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_Relationship];
GO

CREATE TABLE [SFC].[Get_ID_Relationship](
	[Id] [varchar](18) NULL,
	[OwnerId] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[RecordTypeId] [varchar](18) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Description__c] [varchar](max) NULL,
	[hed__Emergency_Contact__c] [bit] NULL,
	[hed__ReciprocalRelationship__c] [varchar](18) NULL,
	[hed__RelatedContact__c] [varchar](18) NULL,
	[hed__Relationship_Explanation__c] [varchar](1300) NULL,
	[hed__SYSTEM_SystemCreated__c] [bit] NULL,
	[hed__Status__c] [varchar](255) NULL,
	[hed__Type__c] [varchar](255) NULL,
	[LSB_RTP_ConsentWithdrawn__c] [bit] NULL,
	[LSB_RTP_ExternalId__c] [varchar](90) NULL,
	[LSB_RTP_PermissionGivenMethod__c] [varchar](255) NULL,
	[LSB_RTP_SourceSystemID__c] [varchar](90) NULL,
	[LSB_RTP_SourceSystem__c] [varchar](255) NULL,
	[LSB_RTP_YouCanDiscuss__c] [varchar](4099) NULL
);