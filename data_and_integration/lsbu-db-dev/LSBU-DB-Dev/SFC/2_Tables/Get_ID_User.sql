IF OBJECT_ID('[SFC].[Get_ID_User]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_User];
GO

CREATE TABLE [SFC].[Get_ID_User](
	[Id] [varchar](18) NULL,
	[Username] [varchar](80) NULL,
	[LastName] [varchar](80) NULL,
	[FirstName] [varchar](40) NULL,
	[Name] [varchar](121) NULL,
	[Title] [varchar](80) NULL,
	[Email] [varchar](128) NULL,
	[Alias] [varchar](8) NULL,
	[CommunityNickname] [varchar](40) NULL,
	[IsActive] [bit] NULL,
	[UserRoleId] [varchar](18) NULL,
	[ProfileId] [varchar](18) NULL,
	[ContactId] [varchar](18) NULL,
	[FederationIdentifier] [varchar](512) NULL,
	[LSB_UER_ExternalID__c] [varchar](90) NULL,
	[LSB_UER_SourceSystemID__c] [varchar](90) NULL,
	[LSB_UER_SourceSystem__c] [varchar](255) NULL
);