IF OBJECT_ID('[SFC].[Get_ID_Course]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_Course];
GO

CREATE TABLE [SFC].[Get_ID_Course](
	[Id] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[hed__Account__c] [varchar](18) NULL,
	[hed__Course_ID__c] [varchar](255) NULL,
	[hed__Credit_Hours__c] [real] NULL,
	[hed__Description__c] [varchar](255) NULL,
	[hed__Extended_Description__c] [varchar](max) NULL,
	[LSB_CSE_ExternalID__c] [varchar](90) NULL,
	[LSB_CSE_SourceSystemID__c] [varchar](90) NULL,
	[LSB_CSE_SourceSystem__c] [varchar](255) NULL,
	[LSB_CSE_DepartmentCode__c] [varchar](1300) NULL,
	[LSB_CSE_ModuleLevel__c] [varchar](20) NULL,
	[LSB_CSE_ModuleStatus__c] [varchar](255) NULL,
	[LSB_CSE_SchoolCode__c] [varchar](1300) NULL
)