IF OBJECT_ID('[SFC].[Get_ID_CaseTeamRole]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_CaseTeamRole];
GO

CREATE TABLE [SFC].[Get_ID_CaseTeamRole](
	[Id] [varchar](18) NULL,
	[Name] [varchar](80) NULL,
	[AccessLevel] [varchar](40) NULL,
	[PreferencesVisibleInCSP] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL
);