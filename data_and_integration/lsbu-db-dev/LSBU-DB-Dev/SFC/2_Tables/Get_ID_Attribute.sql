IF OBJECT_ID('[SFC].[Get_ID_Attribute]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_Attribute];
GO

CREATE TABLE [SFC].[Get_ID_Attribute](
	[Id] [varchar](18) NOT NULL,
	[OwnerId] [varchar](18) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [varchar](80) NULL,
	[RecordTypeId] [varchar](18) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedById] [varchar](18) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedById] [varchar](18) NOT NULL,
	[SystemModstamp] [datetime] NOT NULL,
	[LastActivityDate] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[hed__Agency__c] [varchar](18) NULL,
	[hed__Attribute_Type__c] [varchar](255) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Description__c] [varchar](max) NULL,
	[hed__End_Date__c] [datetime] NULL,
	[hed__Expired__c] [bit] NOT NULL,
	[hed__Start_Date__c] [datetime] NULL,
	[hed__Subject_Area__c] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


