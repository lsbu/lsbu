IF OBJECT_ID('[SFC].[Get_ID_CourseOffering]','U') IS NOT NULL
	DROP TABLE [SFC].[Get_ID_CourseOffering];
GO

CREATE TABLE [SFC].[Get_ID_CourseOffering](
	[Id] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [varchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [varchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[LastViewedDate] [datetime] NULL,
	[LastReferencedDate] [datetime] NULL,
	[hed__Course__c] [varchar](18) NULL,
	[hed__Term__c] [varchar](18) NULL,
	[hed__Capacity__c] [real] NULL,
	[hed__End_Date__c] [datetime] NULL,
	[hed__Faculty__c] [varchar](18) NULL,
	[hed__Section_ID__c] [varchar](255) NULL,
	[hed__Start_Date__c] [datetime] NULL,
	[hed__Facility__c] [varchar](18) NULL,
	[hed__Time_Block__c] [varchar](18) NULL,
	[LSB_COF_ExternalID__c] [varchar](90) NULL,
	[LSB_COF_SourceSystemID__c] [varchar](90) NULL,
	[LSB_COF_SourceSystem__c] [varchar](255) NULL,
	[LSB_COF_AcademicPeriod__c] [varchar](1300) NULL,
	[LSB_COF_ModuleCredit__c] [real] NULL,
	[LSB_COF_ModuleID__c] [varchar](1300) NULL,
	[LSB_COF_ModuleInstance__c] [real] NULL,
	[LSB_COF_SessionCode__c] [varchar](10) NULL,
	[LSB_COF_SessionDescription__c] [varchar](255) NULL
)