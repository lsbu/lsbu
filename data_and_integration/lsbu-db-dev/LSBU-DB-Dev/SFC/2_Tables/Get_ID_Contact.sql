IF OBJECT_ID('[SFC].[Get_ID_Contact]','U') IS NOT NULL
  DROP TABLE [SFC].[Get_ID_Contact];
GO

CREATE TABLE [SFC].[Get_ID_Contact] (
	[Id] [varchar](18) NULL,
	[LastName] [varchar](80) NULL,
	[FirstName] [varchar](40) NULL,
	[Name] [varchar](121) NULL,
	[Email] [varchar](80) NULL,
	[Birthdate] [datetime] NULL,
	[hed__AlternateEmail__c] [varchar](80) NULL,
	[hed__UniversityEmail__c] [varchar](80) NULL,
	[LSB_ExternalID__c] [varchar](90) NULL,
	[LSB_CON_SourceSystemID__c] [varchar](90) NULL,
	[LSB_CON_User__c] [varchar](18) NULL,
	[LSB_CON_CurrentRole__c] [varchar](255) NULL,
	[LSB_CON_AdviseeCaseRecord__c] [varchar](18) NULL,
	[LSB_CON_ContactRole__c] [varchar](1300) NULL,
	[LSB_CON_SourceSystem__c] [varchar](255) NULL
);