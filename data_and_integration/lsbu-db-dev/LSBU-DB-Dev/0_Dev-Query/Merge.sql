--DELETE FROM SRC.SRC_ROW_TMP

SELECT * FROM SRC.SRC_ROW_TMP

select * FROM [CFG].[SRCLoadRunLog]
select * from [CFG].[SRCLoadFilesLog];

-------Merge Procedures
SELECT * FROM [RPT].[SRCValidations]
SELECT * FROM [RPT].[SRCValidationsItems]

--Validation View 
SELECT * FROM [RPT].[SRCValidationsSummary]

--MERGE Procedures
--exec [CFG].[MERGE_EducationalInstitution] @RunID = 1
--exec [CFG].[MERGE_Department] @RunID = 1
--exec [CFG].[MERGE_AcademicProgram] @RunID = 1

--exec [CFG].[MERGE_OfferHolder] @RunID = 1
--exec [CFG].[MERGE_OfferHolderApplicationDetails] @RunID = 1

---------Destination Table ----------
SELECT * FROM [SRC].[EducationalInstitution]
SELECT * FROM [INT].[SRC_EducationalInstitution]

SELECT * FROM [SRC].[Department]
SELECT * FROM [INT].[SRC_Department]

SELECT * FROM [SRC].[AcademicProgram]
SELECT * FROM [INT].[SRC_AcademicProgram]

SELECT * FROM [SRC].[OfferHolder]
SELECT * FROM [INT].[SRC_OfferHolder]

SELECT * FROM [SRC].[OfferHolderApplicationDetails_Affiliation]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails_Affiliation]

--SELECT * FROM [SRC].[OfferHolderApplicationDetails_ProgramEnrolment]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails_ProgramEnrolment]

SELECT * FROM [DST].[ProgramEnrollment_OfferHolderApplicationDetails]

SELECT QLAFFI.LSB_AON_ExternalID__c,QLAFFI.Id FROM [SFC].[Get_ID_Affiliation_OfferHolderApplicationDetails] AS QLAFFI --ON QLAFFI.LSB_AON_ExternalID__c
where QLAFFI.LSB_AON_ExternalID__c = '3820448.4687.E'

SELECT * FROM [SRC].[OfferHolderApplicationDetails_Application]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails_Application]

SELECT * FROM [SRC].[OfferHolderApplicationDetails_Term]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails_Term]


--------DST view ----
--EducationalInstitution
SELECT * FROM [SRC].[EducationalInstitution]
SELECT * FROM [INT].[SRC_EducationalInstitution]
SELECT * FROM [DST].[Account_EducationalInstitution]
SELECT * FROM [SFC].[Get_ID_Account_EducationInstitution]

exec [DST].[UPD_ID_Account_EducationInstitution] @RunID = 41

--Department
SELECT * FROM [SRC].[Department]
SELECT * FROM [INT].[SRC_Department]
SELECT * FROM [DST].[Account_Department]
SELECT * FROM [SFC].[Get_ID_Account_Department]

SELECT D.Name,count(*) 
FROM [DST].[Account_Department] D
GROUP BY D.Name

--exec [DST].[UPD_ID_Account_Department] @RunID = 41

---AcademicProgram
SELECT * FROM [SRC].[AcademicProgram]
--exec [CFG].[MERGE_AcademicProgram] @RunID =31
--delete FROM [INT].[SRC_AcademicProgram]
SELECT * FROM [INT].[SRC_AcademicProgram]
SELECT * FROM [DST].[Account_AcademicProgram]
SELECT * FROM [SFC].[Get_ID_Account_AcademicProgram]

SELECT * 
FROM [DST].[Account_AcademicProgram] A
WHERE A.LSB_ACC_ExternalID__c = '101'

SELECT A.LSB_ACC_ExternalID__c,count(*) 
FROM [DST].[Account_AcademicProgram] A
group by A.LSB_ACC_ExternalID__c
HAVING count(*) > 1

SELECT * from SRC.OfferHolderApplicationDetail

Select A.QL_UCASCouseCode from  (
  Select distinct QL_AcadProgNameId,QL_UCASCouseCode  
  from SRC.OfferHolderApplicationDetails ) A, SRC.AcademicProgram B 
  where A.QL_AcadProgNameId = B.QL_AcadProgNameId

--exec [DST].[UPD_ID_Account_AcademicProgram] @RunID = 31

----Contact_OfferHolder
SELECT * FROM [SRC].[OfferHolder]
  SELECT * FROM [SFC].[Get_ID_Account_AcademicProgram]
SELECT * FROM [INT].[SRC_OfferHolder]
SELECT * FROM [DST].[Contact_OfferHolder]
SELECT * FROM [SFC].[Get_ID_Contact_OfferHolder]

SELECT * FROM DST.SF_Reject_Contact

SELECT * FROM DST.SF_Reject_Affiliation

--exec [DST].[UPD_ID_Contact_OfferHolder] @RunID = 41

----Affiliation_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]

SELECT * FROM [DST].[Affiliation_OfferHolderApplicationDetails] AF
--WHERE AF.LSB_AON_ExternalID__c in ('3922615.5540.A','3920594.4598.A','4001181.5444.A','4001183.3959.A','4001298.2384.A')
WHERE AF.LSB_AON_ExternalID__c in (select R.LSB_AON_ExternalID__c from DST.SF_Reject_Affiliation R)

select * from DST.SF_Reject_Affiliation R
WHERE R.LSB_AON_ExternalID__c = '4001181.5444.A'

SELECT R.errorCode,AF.* 
FROM [DST].[Affiliation_OfferHolderApplicationDetails] AF
LEFT JOIN DST.SF_Reject_Affiliation R ON R.LSB_AON_ExternalID__c = AF.LSB_AON_ExternalID__c



SELECT * FROM [SFC].[Get_ID_Affiliation_OfferHolderApplicationDetails]

SELECT A.LSB_ExternalID__c,count(*) 
FROM [DST].[Affiliation_OfferHolderApplicationDetails] A
GROUP BY A.LSB_ExternalID__c
having count(*) > 1
--exec [DST].[UPD_ID_Affiliation_OfferHolderApplicationDetails] @RunID = 41

-----Term ---
select * from SFC.Get_ID_Account_AcademicProgram
where Name = 'London South Bank University'

--ParentTerm_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].SRC_OfferHolderApplicationDetails_Term
SELECT * FROM [DST].[TermParent_OfferHolderApplicationDetails]

SELECT * FROM [SFC].Get_ID_Term_OfferHolderApplicationDetails
where LSB_TRM_ExternalID__c is not null

--Get_ID_Term_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[Term_OfferHolderApplicationDetails]
SELECT * FROM [SFC].[Get_ID_Term_OfferHolderApplicationDetails]

----Application_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[Application_OfferHolderApplicationDetails]
SELECT * FROM [SFC].[Get_ID_Application_OfferHolderApplicationDetails]

SELECT A.LSB_APP_ExternalID__c,count(*) 
FROM [DST].[Application_OfferHolderApplicationDetails] A
group by A.LSB_APP_ExternalID__c
having count(*) > 1
--exec [DST].[UPD_ID_Application_OfferHolderApplicationDetails] @RunID =37

----ProgEnroll_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[ProgramEnrollment_OfferHolderApplicationDetails]
SELECT * FROM [SFC].[Get_ID_ProgramEnrollment_OfferHolderApplicationDetails]


--exec [DST].[UPD_ID_ProgramEnrollment_OfferHolderApplicationDetails] @RunID =37 --???
exec [DST].[UPD_ID_ProgramEnrollment_EducationInstitution] @RunID =37



-------------

select * from sfc.RecordType 

SELECT * FROM [INT].[SRC_Department] D
where D.QL_DepartmentName = 'Division Of Creative Technologies'


UPDATE [INT].[SRC_Department] 
SET QL_DepartmentName = 'Division Of Creative Technologies 2'
WHERE LSB_ExternalID = 'ICRT|IACI'

SELECT D.LSB_ACC_ExternalID__c,D.* 
FROM [DST].[Account_Department] D
where D.Name like 'Division Of Creative Technologies%'
ORDER BY D.LSB_ACC_ExternalID__c

SELECT D.Name,count(*) 
FROM [DST].[Account_Department] D
GROUP BY D.Name


SELECT SRC.LSB_ExternalID,SFID.AccountNumber,SFID.LastModifiedDate,SFID.Id
FROM [INT].[SRC_EducationalInstitution] SRC
JOIN [SFC].[Get_ID_Account_EducationInstitution] SFID
ON SRC.LSB_ExternalID = SFID.AccountNumber

UPDATE SRC
SET SRC.SFId = SFID.Id, SRC.SFUpdateDateTime = SFID.LastModifiedDate, SRC.SFStatus = 'SF_UPDATED'
FROM [INT].[SRC_EducationalInstitution] SRC
JOIN [SFC].[Get_ID_Account_EducationInstitution] SFID
ON SRC.LSB_ExternalID = SFID.AccountNumber

-

-------------------------
--Select * FROM [SRC].[EducationalInstitution]
Select * FROM [SRC].[EducationalInstitution]
--INT.SRC_EducationalInstitution

INSERT INTO INT.SRC_EducationalInstitution (
	[RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
	[FileRowNumber],[HashMD5],
	--[CurHashMD5],
	[InsetDateTime],
	--[UpdateDateTime],
	[ChangeStatus],
	[QL_EducationInstitutionId],[QL_EducationInstitutionName],[QL_Website],[QL_Phone],
	[QL_Add1],[QL_Add2],[QL_Add3],[QL_Add4],[QL_Add5],
	[QL_Postcode],[QL_Country],[QL_Description],[QL_CreatedDate],[QL_LastModifiedDate])
Select 	[RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
	[FileRowNumber],[HashMD5],
	GETDATE(),
	'NEW',
	[QL_EducationInstitutionId],[QL_EducationInstitutionName],[QL_Website],[QL_Phone],
	[QL_Add1],[QL_Add2],[QL_Add3],[QL_Add4],[QL_Add5],
	[QL_Postcode],[QL_Country],[QL_Description],[QL_CreatedDate],[QL_LastModifiedDate]
FROM [SRC].[EducationalInstitution]

SELECT * 
--DELETE
FROM INT.SRC_EducationalInstitution

----------MERGE - EducationalInstitution
MERGE INT.SRC_EducationalInstitution AS TAR
USING [SRC].[EducationalInstitution] AS SRC
ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
WHEN MATCHED AND TAR.HashMD5 <> SRC.HashMD5
	THEN UPDATE SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'UPDATE'
WHEN NOT MATCHED BY TARGET
	THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
	   [FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
	[QL_EducationInstitutionId],[QL_EducationInstitutionName],[QL_Website],[QL_Phone],
	[QL_Add1],[QL_Add2],[QL_Add3],[QL_Add4],[QL_Add5],
	[QL_Postcode],[QL_Country],[QL_Description],[QL_CreatedDate],[QL_LastModifiedDate])
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
	   SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
	SRC.[QL_EducationInstitutionId],SRC.[QL_EducationInstitutionName],SRC.[QL_Website],SRC.[QL_Phone],
	SRC.[QL_Add1],SRC.[QL_Add2],SRC.[QL_Add3],SRC.[QL_Add4],SRC.[QL_Add5],
	SRC.[QL_Postcode],SRC.[QL_Country],SRC.[QL_Description],SRC.[QL_CreatedDate],SRC.[QL_LastModifiedDate])
WHEN NOT MATCHED BY SOURCE
	THEN UPDATE SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'
;

-------------MERGE -Department
--exec [CFG].[MERGE_OfferHolderApplicationDetails] @RunID =31


SELECT * FROM [INT].[SRC_OfferHolder] as QLOH
WHERE QLOH.QL_StudentID = '4000522'
--LEFT JOIN [DST].[DICT_Contact_Nationality] DCN ON DCN.[QL_Nationality] = QLOH.QL_Nationality
--LEFT JOIN [DST].[DICT_Contact_Domicile] DCD ON DCD.[QL_Domicile] = QLOH.QL_Domicile 
--LEFT JOIN [DST].[DICT_QL_Add5_Country] DAC ON DAC.[QL_Add5] = QLOH.QL_Add5 
--LEFT JOIN [DST].[DICT_Contact_StudentStatus] DCSS ON  DCSS.[QL_StudentStatus] = QLOH.QL_StudentStatus 
--JOIN [INT].[SRC_Department] QLDE ON QLDE.QL_DepartmentId = QLAP.QL_DepartmentId

