--Initial Design / Assumptions:
/*Deduplication Logic - 

Match the QL feed Contact records with existing Contact records 
in Salesforce with LSB_CON_SourceSystem__c <> �QL� 
on basis of - Name & Email Address OR Name & Date of Birth

based on the matching criteria if the existing record is found in salesforce, 
perform an update on the same existing Contact record in Salesforce using Salesforce Id.

The feed should pull the contact records from Salesforce 
where Source System != �QL� or Source System is null. 
Fields to be pulled - Id, FirstName, LastName, Email, Birthdate, LSB_CON_SourceSystem__c, createddate

The contacts identified from the QL feed to be uploaded in Salesforce, match(exact) these contacts against the 
Contacts fetched from Salesforce from Step 1 based on following (OR) conditions - 

1. First Name, Last Name & Email Address 
2. First Name, Last Name & Date of Birth

If any match is identified, use the Salesforce Id of contact from Salesforce 
and use it to peform an update on Salesforce contact with the contact data from QL Feed.
*/

SELECT * FROM [DST].[Deduplicate_Contact_Email]
SELECT * FROM [DST].[Deduplicate_Contact_Birthdate]

select C.Id, C.Email,C.hed__AlternateEmail__c ,C.hed__Preferred_Email__c, C.hed__UniversityEmail__c,
C.FirstName,C.LastName,C.Name, C.Birthdate,C.hed__Chosen_Full_Name__c,
C.LSB_CON_SourceSystem__c, C.LSB_CON_SourceSystemID__c,C.LSB_ExternalID__c, C.AccountId
from SFC.Get_ID_Contact_OfferHolder C
WHERE C.LSB_CON_SourceSystem__c <> 'QL'

--Aantu Waday e3911569@hotmail.com.invalid 	07/10/2001		e3911569@hotmail.com.invalid
select C.Id, C.Email,C.hed__AlternateEmail__c ,C.hed__Preferred_Email__c, C.hed__UniversityEmail__c,
C.FirstName,C.LastName,C.Name, C.Birthdate,C.hed__Chosen_Full_Name__c,
C.LSB_CON_SourceSystem__c, C.LSB_CON_SourceSystemID__c,C.LSB_ExternalID__c,C.AccountId
from SFC.Get_ID_Contact_OfferHolder C
WHERE C.LSB_CON_SourceSystem__c = 'QL'

SELECT * FROM INT.SRC_OfferHolder

SELECT C.FirstName,C.LastName,C.Name, C.Birthdate ,C.Email,C.hed__UniversityEmail__c
FROM DST.Contact_OfferHolder C

--First condition 1. First Name, Last Name & Email Address
--CREATE OR ALTER VIEW [DST].[Deduplicate_Contact_Email] AS
select COH.LSB_SourceSystem__c,COH.LSB_ExternalID__c, C.LSB_CON_SourceSystem__c,C.LSB_ExternalID__c,C.Id, 
C.FirstName,C.LastName,C.Email,C.Birthdate,
C.hed__AlternateEmail__c ,C.hed__Preferred_Email__c, C.hed__UniversityEmail__c,
C.Name,C.hed__Chosen_Full_Name__c,
  C.LSB_CON_SourceSystemID__c,C.AccountId
from SFC.Get_ID_Contact_OfferHolder C
JOIN DST.Contact_OfferHolder as COH on COH.FirstName = C.FirstName AND COH.LastName = C.LastName AND COH.Email = C.Email
WHERE C.LSB_CON_SourceSystem__c <> 'QL'

--First condition 2. First Name, Last Name & Date of Birth
--CREATE OR ALTER VIEW [DST].[Deduplicate_Contact_Birthdate] AS
select COH.LSB_SourceSystem__c,COH.LSB_ExternalID__c, C.LSB_CON_SourceSystem__c,C.LSB_ExternalID__c,C.Id, 
C.FirstName,C.LastName,C.Email,C.Birthdate,
C.hed__AlternateEmail__c ,C.hed__Preferred_Email__c, C.hed__UniversityEmail__c,
C.Name, C.hed__Chosen_Full_Name__c,
  C.LSB_CON_SourceSystemID__c,C.AccountId
from SFC.Get_ID_Contact_OfferHolder C
JOIN DST.Contact_OfferHolder as COH on COH.FirstName = C.FirstName AND COH.LastName = C.LastName AND COH.Birthdate = C.Birthdate
WHERE C.LSB_CON_SourceSystem__c <> 'QL'

SELECT CSF.FirstName,CSF.LastName,CSF.Email,count(*) occurrences
from SFC.Get_ID_Contact_OfferHolder CSF
GROUP BY CSF.FirstName,CSF.LastName,CSF.Email
HAVING COUNT(*) > 1

SELECT CSF.FirstName,CSF.LastName,CSF.Birthdate,count(*) occurrences
from SFC.Get_ID_Contact_OfferHolder CSF
GROUP BY CSF.FirstName,CSF.LastName,CSF.Birthdate
HAVING COUNT(*) > 1

------------------
SELECT * FROM (
SELECT CSF.Id,CSF.FirstName,CSF.LastName,CSF.Birthdate,CSF.LSB_CON_SourceSystem__c,CSF.LSB_ExternalID__c,
Row_Number() OVER(PARTITION BY CSF.FirstName,CSF.LastName,CSF.Birthdate ORDER By CSF.FirstName,CSF.LastName) as CN
FROM SFC.Get_ID_Contact_OfferHolder CSF
) AS Q WHERE Q.CN > 1


SELECT * FROM (
SELECT CSF.Id,CSF.FirstName,CSF.LastName,CSF.Email,CSF.LSB_CON_SourceSystem__c,CSF.LSB_ExternalID__c,
Row_Number() OVER(PARTITION BY CSF.FirstName,CSF.LastName,CSF.Email ORDER By CSF.FirstName,CSF.LastName) as CN
FROM SFC.Get_ID_Contact_OfferHolder CSF
) AS Q WHERE Q.CN > 1

---------------
WITH SFCC AS (SELECT CSF.FirstName,CSF.LastName,CSF.Email,count(*) occurrences
from SFC.Get_ID_Contact_OfferHolder CSF
GROUP BY CSF.FirstName,CSF.LastName,CSF.Email
HAVING COUNT(*) > 1)
SELECT CSF.Id,CSF.FirstName,CSF.LastName,CSF.Email,CSF.LSB_CON_SourceSystem__c,CSF.LSB_ExternalID__c
FROM SFC.Get_ID_Contact_OfferHolder CSF
INNER JOIN SFCC ON SFCC.FirstName = CSF.FirstName AND SFCC.LastName = CSF.LastName AND SFCC.Email = CSF.Email

WITH SFCC AS (SELECT CSF.FirstName,CSF.LastName,CSF.Birthdate,count(*) occurrences
from SFC.Get_ID_Contact_OfferHolder CSF
GROUP BY CSF.FirstName,CSF.LastName,CSF.Birthdate
HAVING COUNT(*) > 1)
SELECT CSF.Id,CSF.FirstName,CSF.LastName,CSF.Email,CSF.LSB_CON_SourceSystem__c,CSF.LSB_ExternalID__c 
FROM SFC.Get_ID_Contact_OfferHolder CSF
INNER JOIN SFCC ON SFCC.FirstName = CSF.FirstName AND SFCC.LastName = CSF.LastName AND SFCC.Birthdate = CSF.Birthdate

-------------
WITH SFCC AS (SELECT CSF.FirstName,CSF.LastName,CSF.Birthdate,count(*) occurrences
from SFC.Get_ID_Contact_OfferHolder CSF
GROUP BY CSF.FirstName,CSF.LastName,CSF.Birthdate
HAVING COUNT(*) > 1)
SELECT CSF.FirstName,CSF.LastName,CSF.Email,CSF.LSB_SourceSystem__c,CSF.LSB_ExternalID__c
FROM DST.Contact_OfferHolder CSF
INNER JOIN SFCC ON SFCC.FirstName = CSF.FirstName AND SFCC.LastName = CSF.LastName AND SFCC.Birthdate = CSF.Birthdate

--Email
WITH SFCC AS (SELECT CSF.QL_FirstName,CSF.QL_Surname,CSF.QL_PersonalEmail,count(*) occurrences
from INT.SRC_OfferHolder CSF
GROUP BY CSF.QL_FirstName,CSF.QL_Surname,CSF.QL_PersonalEmail
HAVING COUNT(*) > 1)
SELECT CSFB.QL_FirstName,CSFB.QL_Surname,CSFB.QL_PersonalEmail,CSFB.QL_Birthdate,CSFB.LSB_ExternalID
FROM INT.SRC_OfferHolder CSFB
INNER JOIN SFCC ON CSFB.QL_FirstName = SFCC.QL_FirstName AND CSFB.QL_Surname = SFCC.QL_Surname AND CSFB.QL_PersonalEmail = SFCC.QL_PersonalEmail
ORDER BY CSFB.QL_FirstName,CSFB.QL_Surname,CSFB.QL_PersonalEmail
GO
--BirthDate
WITH SFCC AS (SELECT CSF.QL_FirstName,CSF.QL_Surname,CSF.QL_Birthdate,count(*) occurrences
from INT.SRC_OfferHolder CSF
GROUP BY CSF.QL_FirstName,CSF.QL_Surname,CSF.QL_Birthdate
HAVING COUNT(*) > 1)
SELECT CSFB.QL_FirstName,CSFB.QL_Surname,CSFB.QL_PersonalEmail,CSFB.QL_Birthdate,CSFB.LSB_ExternalID
FROM INT.SRC_OfferHolder CSFB
INNER JOIN SFCC ON CSFB.QL_FirstName = SFCC.QL_FirstName AND CSFB.QL_Surname = SFCC.QL_Surname AND CSFB.QL_Birthdate = SFCC.QL_Birthdate
ORDER BY CSFB.QL_FirstName,CSFB.QL_Surname,CSFB.QL_PersonalEmail
GO
