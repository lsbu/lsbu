--LSBU Account don't exists in QL files on Education Institution 
SELECT * FROM SRC.EducationalInstitution

--------Parent Term--------
Select distinct 
Concat('Academic Period ',QL_AcademicPeriod) as TermName, 
'School Year' as TermType, 
'LSBU' as AccountExternalId, 
QL_AcademicPeriod as ExternalId 
from SRC.OfferHolderApplicationDetails

--------Term-----------
Select distinct 
QL_AcademicPeriod as ParentTermExternalId,
CONCAT(	case
			WHEN CHARINDEX('SEPT',OHAD.QL_TERM) != 0 THEN 'SEPT'
			WHEN CHARINDEX('MAR',OHAD.QL_TERM) != 0 THEN 'MAR'
			WHEN CHARINDEX('APR',OHAD.QL_TERM) != 0 THEN 'APR'
			WHEN CHARINDEX('JAN',OHAD.QL_TERM) != 0 THEN 'JAN'
			WHEN CHARINDEX('JUN',OHAD.QL_TERM) != 0 THEN 'JUN'
			ELSE NULL
		END,
' (Academic Period ',QL_AcademicPeriod,')' ) AS TermName,
Concat(QL_AcademicPeriod,'|',QL_AcadProgNameId,'|',
case
WHEN CHARINDEX('SEPT',OHAD.QL_TERM) != 0 THEN 'SEPT'
WHEN CHARINDEX('MAR',OHAD.QL_TERM) != 0 THEN 'MAR'
WHEN CHARINDEX('APR',OHAD.QL_TERM) != 0 THEN 'APR'
WHEN CHARINDEX('JAN',OHAD.QL_TERM) != 0 THEN 'JAN'
WHEN CHARINDEX('JUN',OHAD.QL_TERM) != 0 THEN 'JUN'
ELSE NULL
END) as ExternalId,
'LSBU' as AccountExternalId
from SRC.OfferHolderApplicationDetails OHAD


-------------------
SELECT A.QL_Term, 
CASE --SEPT,MAR,APR,JAN
	WHEN A.QL_Term like '%SEPT%' THEN 'SEPT'
	WHEN A.QL_Term like '%MAR%' THEN 'MAR'
	WHEN A.QL_Term like '%APR%' THEN 'APR'
	WHEN A.QL_Term like '%JAN%' THEN 'JAN'
	WHEN A.QL_Term is null THEN A.QL_Term
END as list
FROM SRC.OfferHolderApplicationDetails A
--WHERE dbo.trim(['+@FieldName+']) NOT IN (SELECT value FROM STRING_SPLIT(@IN_ListValues, '',''))