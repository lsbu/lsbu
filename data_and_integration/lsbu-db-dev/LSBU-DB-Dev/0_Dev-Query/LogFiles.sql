
exec [SRC].[SRCLoadRunProcess] 
        @SRCLoadRunId = NULL, --@SRCLoadRunId bigint output,
		@EtlRunId = NULL,     --@EtlRunId bigint,
		@SRCLoadStatus = 'STARTED',--@SRCLoadStatus nvarchar(250), --STARTED,PROGRESS,SUCCESS,ERRORED
		@SRCLoadDescription = 'Desc 3 PROGRESS' --@SRCLoadDescription nvarchar(250)

exec [SRC].[SRCLoadRunProcess] 
        @SRCLoadRunId = 2, --@SRCLoadRunId bigint output,
		@EtlRunId = NULL,     --@EtlRunId bigint,
		@SRCLoadStatus = 'SUCCESS',--@SRCLoadStatus nvarchar(250), --STARTED,PROGRESS,SUCCESS,ERRORED
		@SRCLoadDescription = 'Desc SUCCESS' --@SRCLoadDescription nvarchar(250)

--------------------
DELETE
FROM [CFG].[SRCLoadRunLog]

DELETE
FROM [CFG].[SRCLoadFilesLog];

DELETE --SELECT *
FROM [SRC].[SRC_ROW_TMP];
--FROM [CFG].[SRC_ROW_TMP];
-----------------------

SELECT *
FROM [CFG].[SRCLoadRunLog]

SELECT *
FROM [CFG].[SRCLoadFilesLog];

SELECT *
FROM [SRC].[SRC_ROW_TMP];

--------SF-RecordType
select Id,Name,DeveloperName,SobjectType,IsActive 
FROM SFC.RecordType

-----------Firs 5 objects
SELECT * INTO INT.SRC_EducationalInstitution
--Select * 
FROM [SRC].[EducationalInstitution]

SELECT * INTO INT.SRC_Department
--Select * 
FROM [SRC].[Department]

SELECT * INTO INT.SRC_AcademicProgram
--Select * 
FROM [SRC].[AcademicProgram]
order by FileRowNumber

SELECT * INTO INT.SRC_OfferHolder
--Select * 
FROM [SRC].[OfferHolder]
order by FileRowNumber

SELECT * INTO INT.SRC_OfferHolderApplicationDetails
--Select * 
FROM [SRC].[OfferHolderApplicationDetails]
order by FileRowNumber

----------------------
DELETE FROM [RPT].[SRCValidations]
DELETE FROM [RPT].[SRCValidationsItems]

SELECT * FROM [RPT].[SRCValidationsSummary]

SELECT CFG.fx_CheckURL('http://www.microsoft.com');
SELECT CFG.fx_IsValidURL('www.microsoft.com');
SELECT CFG.fx_IsValidURL('https://www.microsoft.com');

SELECT  CFG.fx_isDate_custom('2020-12-44','YYYY-MM-DD' )

SELECT  CHARINDEX('http', 'https://www.microsoft.com') 

exec [CFG].[VA_EducationalInstitution] @RunID = 31
exec [CFG].[VA_Department] @RunID = 31
exec [CFG].[VA_AcademicProgram] @RunID = 31

exec [CFG].[VA_OfferHolder] @RunID = 31
exec [CFG].[VA_OfferHolderApplicationDetails] @RunID = 31


--DECLARE @ValidationResultID VARCHAR(20);
--EXEC GetValidResultID @ValidationResultID OUTPUT;	

----test validation
	exec rpt.ValidRule01_lookup
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'Department'
				,@ViewName = 'SRC.Department'
				,@FieldName = 'QL_EducationInstitutionId'
				,@FieldNameRelated = 'QL_EducationInstitutionId'
				,@ViewNameRelated = 'SRC.EducationalInstitution'
				,@Mandatory = 'Y';

--Is Unique
	exec CFG.validate_rule08_IsUnique 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'EducationalInstitution'
				,@ViewName = 'SRC.EducationalInstitution'
				,@FieldName = 'QL_EducationInstitutionId'
				,@Mandatory = 'Y';

--Date
	exec CFG.ValidRule05_date 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolder'
				,@ViewName = '[SRC].[OfferHolder]'
				,@FieldName = 'QL_Birthdate'
				,@DateFormat = 'DD-MM-YYYY' --YYYY-MM-DD
				,@Mandatory = 'Y';
				

	SELECT
		 TOP 100 	
		 --@IN_RUN_ID
		--,@IN_CLT_CODE
		--,@IN_VALIDATION_RESULT_ID
		SrcSystem
		,ObjectName
		,concat('Value not exists in related object, Error value: ',[QL_EducationInstitutionId])
		,FileRowNumber
		,LSB_ExternalID
	FROM SRC.Department A 
	WHERE NOT EXISTS (SELECT [QL_EducationInstitutionId] FROM SRC.EducationalInstitution R 
					   WHERE (R.[QL_EducationInstitutionId]) = (A.[QL_EducationInstitutionId]) 
						 )  
						 AND len(isnull([QL_EducationInstitutionId],'')) > 0  
						 --AND A.RUN_ID = @IN_RUN_ID 
