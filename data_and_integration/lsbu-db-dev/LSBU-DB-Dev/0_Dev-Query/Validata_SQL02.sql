
Select * 
FROM [SRC].[EducationalInstitution]

Select * 
FROM [SRC].[Department]

--SRC validation error report
SELECT * FROM [RPT].SRCValidationsSummary

SELECT * 
FROM [SRC].[EducationalInstitution] A
WHERE NOT EXISTS (SELECT R.QL_EducationInstitutionId FROM [SRC].[Department] R
					WHERE R.QL_EducationInstitutionId = A.QL_EducationInstitutionId )
--AND len(isnull(
;

SELECT * 
FROM [SRC].[Department] A
WHERE NOT EXISTS (SELECT R.QL_EducationInstitutionId FROM [SRC].[EducationalInstitution] R
					WHERE R.QL_EducationInstitutionId = A.QL_EducationInstitutionId )
;

Select * 
FROM [SRC].[AcademicProgram]

SELECT * 
FROM [SRC].[AcademicProgram] A
WHERE NOT EXISTS (SELECT R.QL_EducationInstitutionId FROM [SRC].[EducationalInstitution] R
					WHERE R.QL_EducationInstitutionId = A.QL_EducationInstitutionId )
;
