DECLARE @SRCFileStatus VARCHAR(50);
--SET @SRCFileStatus = 'aa';
--DECLARE @INTFileStatus INT; Filetransfercomplete,
execute [CFG].[SRCCheckFilesList]
	@ObjectList = 'Filetransfercomplete,EducationalInstitution,Department,AcademicProgram,OfferHolderApplicationDetails,OfferHolder'
	,@FileNo = 6
	--,@INTFileStatus = @INTFileStatus
;
SELECT @INTFileStatus;
SELECT @SRCFileStatus;


SELECT *
FROM [CFG].[SRCLoadRunLog]

SELECT *
FROM [CFG].[SRCLoadFilesLog];

SELECT *
FROM [CFG].SRCLoadFilesTmp

-----------------------------------------------
--Check we have more than 1 collection
SELECT sSRCFileObjectName,count(*)
FROM [CFG].SRCLoadFilesTmp
group by sSRCFileObjectName
having count(*) > 1

--Check we have more than 1 collection in the same time
SELECT sSRCFileObjectName,t.dSRCFileDateTime, count(*)
FROM [CFG].SRCLoadFilesTmp t
group by sSRCFileObjectName,t.dSRCFileDateTime
having count(*) > 1

--Get oldest collection 6 files
SELECT t.dSRCFileDateTime, count(*)
FROM [CFG].SRCLoadFilesTmp t
group by t.dSRCFileDateTime
having count(*) = 6

--Select oldest list
SELECT *
FROM [CFG].SRCLoadFilesTmp t
where t.sSRCFileObjectName in ('Filetransfercomplete','EducationalInstitution','Department','AcademicProgram','OfferHolderApplicationDetails','OfferHolder')
and t.dSRCFileDateTime = (SELECT min(dSRCFileDateTime) FROM [CFG].SRCLoadFilesTmp)

--Check oldest list comlete
SELECT t.sSRCFileName,count(*)
FROM [CFG].SRCLoadFilesTmp t
where t.sSRCFileObjectName in ('Filetransfercomplete','EducationalInstitution','Department','AcademicProgram','OfferHolderApplicationDetails','OfferHolder')
and t.dSRCFileDateTime = (SELECT min(dSRCFileDateTime) FROM [CFG].SRCLoadFilesTmp)
group by t.sSRCFileName
having count(*) = 1

