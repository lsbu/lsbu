﻿-----------Firs 5 objects
Select * FROM [SRC].[EducationalInstitution]

Select * FROM [SRC].[Department]

Select * FROM [SRC].[AcademicProgram]

Select * FROM [SRC].[OfferHolder]

Select * 
FROM [SRC].[OfferHolderApplicationDetails]

------==============================----------
select * from RPT.SRCValidations
select * from RPT.SRCValidationsItems

--delete from RPT.SRCValidations
--delete from RPT.SRCValidationsItems

select * from RPT.SRCValidationsSummary
------==============================----------
exec [CFG].[VA_EducationalInstitution] @RunID = 47
exec [CFG].[VA_Department] @RunID = 47
exec [CFG].[VA_AcademicProgram] @RunID = 47

exec [CFG].[VA_OfferHolder] @RunID = 47
exec [CFG].[VA_OfferHolderApplicationDetails] @RunID = 47

--------------

Select AP.LSB_ExternalID, AP.QL_DepartmentId ,AP.QL_AcadProgNameId, AP.QL_EducationInstitutionId
FROM [SRC].[AcademicProgram] AP
where AP.QL_DepartmentId = 'NENG'

Select D.LSB_ExternalID,D.QL_DepartmentId ,D.QL_DepartmentName
FROM [SRC].[Department] D
WHERE D.QL_DepartmentId = 'NENG'

Select EI.LSB_ExternalID,EI.QL_EducationInstitutionId ,EI.QL_EducationInstitutionName
FROM [SRC].[EducationalInstitution] EI
WHERE EI.QL_EducationInstitutionId  = 'NENG'

--CREATE VIEW EducationInstitution_Department_Union
SELECT DISTINCT QL_DepartmentId, QL_DepartmentName
FROM (
	SELECT DISTINCT D.QL_DepartmentId ,D.QL_DepartmentName
	FROM [SRC].[Department] D
		UNION ALL
	SELECT DISTINCT EI.QL_EducationInstitutionId as QL_DepartmentId, EI.QL_EducationInstitutionName as QL_DepartmentName
	FROM [SRC].[EducationalInstitution] EI
	) A


Select * FROM [INT].SRC_AcademicProgram A
WHERE A.QL_AcadProgName = 'Associate Student Programme (PG) - One Year (RBEA) (FT)'

Select *
FROM DST.Account_AcademicProgram A
where A.Name = 'Associate Student Programme (PG) - One Year (RBEA) (FT)'


SELECT RIGHT('ala.kot@gmail.com',LEN('ala.kot@gmail.com')- CHARINDEX('@','ala.kot@gmail.com')+1);
SELECT LEFT('ala.kot@gmail.com',LEN('ala.kot@gmail.com')- CHARINDEX('@','ala.kot@gmail.com')-2);

Select A.Name ,count(*)
FROM DST.Account_AcademicProgram A
group by A.Name
having count(*) > 1

Select A.LSB_ACC_ExternalID__c ,count(*)
FROM DST.Account_AcademicProgram A
group by A.LSB_ACC_ExternalID__c
having count(*) > 1

Select * FROM [SRC].[OfferHolder] a
where a.QL_PersonalEmail like '%@lyf.ai%'
--'jay@@lyf.ai.invalid'

select LEFT('jay@lyf.ai',LEN('jay@lyf.ai')- CHARINDEX('@','jay@lyf.ai')-2),SUBSTRING('jay@lyf.ai',1,CHARINDEX('@','jay@lyf.ai')-1)

--------
Select * 
FROM [SRC].[OfferHolderApplicationDetails]
where QL_StudentID in  (2706446,2716999,2724883,2807439,2816969)

Select * 
FROM [SRC].[OfferHolderApplicationDetails] A
where A.QL_

Select CONCAT(A.QL_StudentID,A.QL_AcadProgNameId, A.QL_ApplicationDate ),count(*)
FROM [SRC].[OfferHolderApplicationDetails] A
GROUP BY CONCAT(A.QL_StudentID,A.QL_AcadProgNameId, A.QL_ApplicationDate )

Select CONCAT(A.QL_StudentID,A.QL_AcadProgNameId, A.QL_ApplicationDate ),A.*
FROM [SRC].[OfferHolderApplicationDetails] A
where CONCAT(A.QL_StudentID,A.QL_AcadProgNameId, A.QL_ApplicationDate ) in ('2100724528409/09/2003','2500468543505/05/2020')


-------------------
Select QL_Add1 
FROM [SRC].[OfferHolder]
where QL_StudentID in ('3824459','3925340','3926503','3931221','3931392','3931752','3932732','3934388','3935008','3925247')

SELECT QL_Add1
FROM [didev].[INT].[SRC_OfferHolder] QL
where QL_StudentID in ('3824459','3925340','3926503','3931221','3931392','3931752','3932732','3934388','3935008','3925247')

select LSB_ExternalID,*
from SRC.OfferHolderApplicationDetails
WHERE LSB_ExternalID = '3932870191'

select LSB_ExternalID,count(*) 
from SRC.OfferHolderApplicationDetails
GROUP BY LSB_ExternalID
having count(*) > 1

SELECT * FROM SFC.RecordType

SELECT LSB_ExternalID,count(*) 
FROM [INT].[SRC_AcademicProgram]
GROUP BY LSB_ExternalID
having count(*) > 1

SELECT * FROM [SRC].[AcademicProgram]
WHERE LSB_ExternalID in ('APPRENT','ACC_EXT','CPD_OPEN')

SELECT * 
--delete
FROM [INT].[SRC_AcademicProgram]
WHERE LSB_ExternalID = 'APPRENT'
AND FileRowNumber > 1191


--------------
SELECT ISNUMERIC('235234')
SELECT ISNUMERIC(REPLACE(REPLACE(REPLACE('+49 1234 56 78-0','+',''),' ',''),'-','') ) 
SELECT CFG.fx_CheckPhone('+49 1234 56 78-0')
SELECT CFG.fx_CheckPhone('(234) 567-8901')
SELECT CFG.fx_CheckPhone('49123456780')



SELECT CFG.fx_HashEmail('tomasz.bugaj@gmail.com')
select RIGHT(HASHBYTES('MD5',LEFT('tomasz.bugaj@gmail.com',CHARINDEX('@','tomasz.bugaj@gmail.com')-1)),9)
select HASHBYTES('MD5',LEFT('tomasz.bugaj@gmail.com',CHARINDEX('@','tomasz.bugaj@gmail.com')-1))
SELECT CHAR(ROUND(RAND() * 93 + 33, 0))


--Lookup to other object
	exec CFG.ValidRule01_lookup
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'Department'
				,@ViewName = 'SRC.Department'
				,@FieldName = 'QL_EducationInstitutionId'
				,@FieldNameRelated = 'QL_EducationInstitutionId'
				,@ViewNameRelated = 'SRC.EducationalInstitution'
				,@Mandatory = 'Y';

--Length of TXT file				
	exec CFG.ValidRule02_length
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolderApplicationDetails'
				,@ViewName = '[SRC].[OfferHolderApplicationDetails]'
				,@FieldName = 'QL_ApplicationType'
				,@MaxLength = 100
				,@Mandatory = 'Y';

--Number(5,2)	-- sprawdzić !!!!!!!!!!
	exec CFG.ValidRule03_number 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolderApplicationDetails'
				,@ViewName = '[SRC].[OfferHolderApplicationDetails]'
				,@FieldName = 'QL_AcadProgNameId'
				,@Precision = 1
				,@Scale = 0
				,@Mandatory = 'Y';

	exec CFG.ValidRule04_picklist_custom 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolderApplicationDetails'
				,@ViewName = '[SRC].[OfferHolderApplicationDetails]'
				,@FieldName = 'QL_UCASApplicationNumber'
				,@ListValues = 'UC01,UC02,UC03,UC04,UC05,UC06,UC07,UC08,UC09' 
				,@Mandatory = 'Y';
				
	
--Date format check only 'YYYY-MM-DD' or 'MMM yy' or 'YYYY-MM-DD HH:MM:SS'
	exec CFG.ValidRule05_date 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolderApplicationDetails'
				,@ViewName = '[SRC].[OfferHolderApplicationDetails]'
				,@FieldName = 'QL_ApplicationDate'
				,@DateFormat = 'DD-MM-YYYY' --YYYY-MM-DD
				,@Mandatory = 'Y';

--OK
	exec CFG.ValidRule06_email 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolder'
				,@ViewName = '[SRC].[OfferHolder]'
				,@FieldName = 'QL_PersonalEmail'
				,@Mandatory = 'N';
				
	exec CFG.ValidRule07_phone
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'OfferHolder'
				,@ViewName = '[SRC].[OfferHolder]'
				,@FieldName = 'QL_LSBUEmail' --Phone
				,@MaxLength = 11
				,@Mandatory = 'Y';
				
	exec CFG.validate_rule08_IsUnique 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'EducationalInstitution'
				,@ViewName = 'SRC.EducationalInstitution'
				,@FieldName = 'QL_EducationInstitutionId'
				,@Mandatory = 'Y';

	exec CFG.ValidRule09_url 
				@RunID = 1
				,@SrcSystem = 'QL'
				,@ObjectName = 'AcademicProgram'
				,@ViewName = '[SRC].[AcademicProgram]'
				,@FieldName = 'QL_Website'
				,@Mandatory = 'Y';

--------Term -----
Select distinct Concat(AP.QL_EducationInstitutionId,'|',OFHD.QL_AcademicPeriod ) as ParentTermExternalId, 
case
WHEN CHARINDEX('SEPT',QL_TERM) != 0 THEN 'SEPT'
WHEN CHARINDEX('MAR',QL_TERM) != 0 THEN 'SEPT'
WHEN CHARINDEX('APR',QL_TERM) != 0 THEN 'APR'
WHEN CHARINDEX('JAN',QL_TERM) != 0 THEN 'JAN'
ELSE NULL
END AS TermName,
Concat(AP.QL_AcadProgNameId,'|',OFHD.QL_AcademicPeriod,'|',
case
WHEN CHARINDEX('SEPT',QL_TERM) != 0 THEN 'SEPT'
WHEN CHARINDEX('MAR',QL_TERM) != 0 THEN 'SEPT'
WHEN CHARINDEX('APR',QL_TERM) != 0 THEN 'APR'
WHEN CHARINDEX('JAN',QL_TERM) != 0 THEN 'JAN'
ELSE NULL
END) as ExternalId, 
OFHD.QL_AcadProgNameId as AccountExternalId

 from SRC.OfferHolderApplicationDetails OFHD, SRC.AcademicProgram AP
 where OFHD.QL_AcadProgNameId = AP.QL_AcadProgNameId

 ------------
select LSB_ExternalID,QL_FirstName,QL_Surname,QL_Add1
from [SRC].[OfferHolder]
where LSB_ExternalID in (3926225,3926638,3934172)

---LEAP-720
--When field Add4 is empty and Add5 have value field BillingCity is null.
SELECT * FROM [SRC].[AcademicProgram]

SELECT QLAP.LSB_ExternalID, QLAP.QL_Add4,QLAP.QL_Add5,
(CASE 
	WHEN QLAP.QL_Add4 IS NOT NULL AND QLAP.QL_Add5 IS NOT NULL
		THEN CONCAT(QLAP.QL_Add4,' ',QLAP.QL_Add5)
	WHEN QLAP.QL_Add4 IS NOT NULL AND QLAP.QL_Add5 IS NULL
		THEN QLAP.QL_Add4
	WHEN QLAP.QL_Add4 IS NULL AND QLAP.QL_Add5 IS NOT NULL
		THEN QLAP.QL_Add5
	ELSE QLAP.QL_Add4
END) as BillingCity
FROM [INT].[SRC_AcademicProgram] QLAP
WHERE QLAP.QL_Add5 is null
AND QLAP.QL_Add5 is not null

SELECT * FROM [DST].[Account_AcademicProgram]

---------Address

SELECT * FROM INT.SRC_Department

SELECT * FROM INT.SRC_EducationalInstitution

SELECT * FROM INT.SRC_AcademicProgram


SELECT * FROM SFC.Get_ID_Address

