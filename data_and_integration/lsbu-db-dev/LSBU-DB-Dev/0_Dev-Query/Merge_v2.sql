--DELETE FROM SRC.SRC_ROW_TMP

SELECT * FROM SRC.SRC_ROW_TMP

select * FROM [CFG].[SRCLoadRunLog]
select * from [CFG].[SRCLoadFilesLog];

-------Merge Procedures
SELECT * FROM [RPT].[SRCValidations]
SELECT * FROM [RPT].[SRCValidationsItems]

--Validation View 
SELECT * FROM [RPT].[SRCValidationsSummary]

--MERGE Procedures
--exec [CFG].[MERGE_EducationalInstitution] @RunID = 47
--exec [CFG].[MERGE_Department] @RunID = 47
--exec [CFG].[MERGE_AcademicProgram] @RunID = 47

--exec [CFG].[MERGE_OfferHolder] @RunID = 47
--exec [CFG].[MERGE_OfferHolderApplicationDetails] @RunID = 47

---------Destination Table ----------
SELECT * FROM [SRC].[EducationalInstitution]
SELECT * FROM [INT].[SRC_EducationalInstitution]

SELECT * FROM [SRC].[Department]
SELECT * FROM [INT].[SRC_Department]

SELECT * FROM [SRC].[AcademicProgram]
SELECT * FROM [INT].[SRC_AcademicProgram]

SELECT * FROM [SRC].[OfferHolder]
SELECT * FROM [INT].[SRC_OfferHolder]

SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]

WHERE ChangeStatus = 'UPDATE'

SELECT distinct A.QL_ChoiceNumber
FROM [SRC].[OfferHolderApplicationDetails] A

--------DST view ----
--EducationalInstitution
SELECT * FROM [SRC].[EducationalInstitution]
SELECT * FROM [INT].[SRC_EducationalInstitution]
SELECT * FROM [DST].[Account_EducationalInstitution]
SELECT * FROM [SFC].[Get_ID_Account_EducationInstitution]

exec [DST].[UPD_ID_Account_EducationInstitution] @RunID = 41

--Department
SELECT * FROM [SRC].[Department]
SELECT * FROM [INT].[SRC_Department]
SELECT * FROM [DST].[Account_Department]
SELECT * FROM [SFC].[Get_ID_Account_Department]

SELECT D.Name,count(*) 
FROM [DST].[Account_Department] D
GROUP BY D.Name

--exec [DST].[UPD_ID_Account_Department] @RunID = 41

---AcademicProgram
SELECT * FROM [SRC].[AcademicProgram]
--exec [CFG].[MERGE_AcademicProgram] @RunID =31
--delete FROM [INT].[SRC_AcademicProgram]
SELECT * FROM [INT].[SRC_AcademicProgram]
SELECT * FROM [DST].[Account_AcademicProgram]
SELECT * FROM [SFC].[Get_ID_Account_AcademicProgram]

SELECT * 
FROM [DST].[Account_AcademicProgram] A
WHERE A.LSB_ACC_ExternalID__c = '101'

SELECT A.LSB_ACC_ExternalID__c,count(*) 
FROM [DST].[Account_AcademicProgram] A
group by A.LSB_ACC_ExternalID__c
HAVING count(*) > 1

SELECT * from SRC.OfferHolderApplicationDetail

Select A.QL_UCASCouseCode from  (
  Select distinct QL_AcadProgNameId,QL_UCASCouseCode  
  from SRC.OfferHolderApplicationDetails ) A, SRC.AcademicProgram B 
  where A.QL_AcadProgNameId = B.QL_AcadProgNameId

--exec [DST].[UPD_ID_Account_AcademicProgram] @RunID = 31

----Contact_OfferHolder
SELECT * FROM [SRC].[OfferHolder]
  SELECT * FROM [SFC].[Get_ID_Account_AcademicProgram]
SELECT * FROM [INT].[SRC_OfferHolder]
SELECT * FROM [DST].[Contact_OfferHolder]
SELECT * FROM [SFC].[Get_ID_Contact_OfferHolder]

--exec [DST].[UPD_ID_Contact_OfferHolder] @RunID = 41

----Affiliation_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[Affiliation_OfferHolderApplicationDetails]
WHERE LSB_ExternalID__c = '3935283.4667'
SELECT * FROM [SFC].[Get_ID_Affiliation_OfferHolderApplicationDetails]

SELECT A.LSB_ExternalID__c,count(*) 
FROM [DST].[Affiliation_OfferHolderApplicationDetails] A
GROUP BY A.LSB_ExternalID__c
having count(*) > 1
--exec [DST].[UPD_ID_Affiliation_OfferHolderApplicationDetails] @RunID = 41

-----Term ---
select * from SFC.Get_ID_Account_AcademicProgram
where Name = 'London South Bank University'

--ParentTerm_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[TermParent_OfferHolderApplicationDetails]

SELECT * FROM [SFC].Get_ID_Term_OfferHolderApplicationDetails
where LSB_TRM_ExternalID__c is not null

--Get_ID_Term_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[Term_OfferHolderApplicationDetails]
SELECT * FROM [SFC].[Get_ID_Term_OfferHolderApplicationDetails]

----ApplicationParent_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[ApplicationParent_OfferHolderApplicationDetails]

SELECT QLAPP.LSB_APP_ExternalID__c,QLAPP.hed__Application_Status__c, 
SUBSTRING(QLAPP.LSB_APP_ExternalID__c,0,CHARINDEX('.',QLAPP.LSB_APP_ExternalID__c,CHARINDEX('.',QLAPP.LSB_APP_ExternalID__c,CHARINDEX('.',QLAPP.LSB_APP_ExternalID__c)+1)+1) ) 
,QLAPP.* 
FROM [SFC].[Get_ID_Application_OfferHolderApplicationDetails] QLAPP
WHERE QLAPP.hed__Application_Status__c = 'ADEF' --QLOHAD.QL_ApplicationStatus = 'ADEF' --Parenty

--3926421.5345.20/21.1FS00
SELECT * 
FROM [INT].[SRC_OfferHolderApplicationDetails]

SELECT A.LSB_APP_ExternalID__c ,count(*)
FROM [DST].[ApplicationParent_OfferHolderApplicationDetails] A
GROUP BY A.LSB_APP_ExternalID__c
HAVING count(*) > 1

SELECT * 
FROM [DST].[ApplicationParent_OfferHolderApplicationDetails] A
WHERE A.LSB_APP_ExternalID__c = '3906091.2386.1PS00'

SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails] B
WHERE B.QL_StudentID = '3906091' AND B.QL_AcadProgNameId = '2386' and B.QL_AOSPeriod = '1PS00'

Select A.LSB_ExternalID,B.LSB_ExternalID , B.QL_ApplicationStatus, A.QL_ApplicationStatus, 
A.QL_StudentID,A.QL_AcadProgNameId,A.QL_AOSPeriod,A.QL_AcademicPeriod,B.QL_AcademicPeriod
from SRC.OfferHolderApplicationDetails A, SRC.OfferHolderApplicationDetails B
where A.QL_StudentID = B.QL_StudentID 
and A.QL_AcadProgNameId = B.QL_AcadProgNameId 
and A.QL_AOSPeriod = B.QL_AOSPeriod
and B.QL_ApplicationStatus = 'ADEF' 
and A.QL_ApplicationStatus != 'ADEF'
ORDER BY A.LSB_ExternalID,B.LSB_ExternalID
AND A.QL_StudentID = '3906091' AND A.QL_AcadProgNameId = '2386' and A.QL_AOSPeriod = '1PS00'

SELECT '3906005.5009.1FS00.21/22',SUBSTRING('3906005.5009.1FS00.21/22',0,10),CHARINDEX('.','3906005.5009.1FS00.21/22',3)
,SUBSTRING('3906005.5009.1FS00.21/22',0,CHARINDEX('.','3906005.5009.1FS00.21/22',CHARINDEX('.','3906005.5009.1FS00.21/22',CHARINDEX('.','3906005.5009.1FS00.21/22')+1)+1) )
,PATINDEX('.','3906005.5009.1FS00.21/22')

----Application_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[Application_OfferHolderApplicationDetails]

SELECT A.LSB_APP_ExternalID__c ,count(*)
FROM [DST].[Application_OfferHolderApplicationDetails] A
GROUP BY A.LSB_APP_ExternalID__c
HAVING count(*) > 1

SELECT * FROM [SFC].[Get_ID_Application_OfferHolderApplicationDetails]

SELECT A.LSB_APP_ExternalID__c,count(*) 
FROM [DST].[Application_OfferHolderApplicationDetails] A
group by A.LSB_APP_ExternalID__c
having count(*) > 1
--exec [DST].[UPD_ID_Application_OfferHolderApplicationDetails] @RunID =37

----ProgEnroll_OfferHolderApplicationDetails
SELECT * FROM [SRC].[OfferHolderApplicationDetails]
SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails]
SELECT * FROM [DST].[ProgramEnrollment_OfferHolderApplicationDetails]
SELECT * FROM [SFC].[Get_ID_ProgramEnrollment_OfferHolderApplicationDetails]


--exec [DST].[UPD_ID_ProgramEnrollment_OfferHolderApplicationDetails] @RunID =37 --???
exec [DST].[UPD_ID_ProgramEnrollment_EducationInstitution] @RunID =37



-------------

select * from sfc.RecordType 

select distinct CONCAT(QLOHAD.QL_StudentID,'.',QLOHAD.QL_AcadProgNameId,'.',QLOHAD.QL_ApplicantStageIndicator)
FROM [INT].[SRC_OfferHolderApplicationDetails] AS QLOHAD
--CONCAT(QLOHAD.QL_StudentID,'.',QLOHAD.QL_AcadProgNameId,'.',QLOHAD.QL_ApplicantStageIndicator)

----Pullkit query Affiliation -----
Select 'a', * from (
Select QL_StudentID,QL_AcadProgNameId,QL_ApplicationDate as StartDate, QL_ApplicationStatus, QL_EnrollmentStatus , 'Applicant' as Role ,
Case
when QL_ApplicationStatus in  ('ACD','AREJ','AUD','AWD','AWDX') then 'Former'
when QL_EnrollmentStatus = 'EFE' and QL_ApplicationStatus != 'ADEF' then 'Former'
else 'Current'
end as Status,
Case
when QL_ApplicationStatus in  ('ACD','AREJ','AUD','AWD','AWDX') then QL_AppStatusDateAchieved
when QL_EnrollmentStatus = 'EFE' and QL_ApplicationStatus != 'ADEF' then QL_EnrolStatusDateAchieved
end as EndDate,

ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
) AffiliationRank
from SRC.OfferHolderApplicationDetails  ) Applicants where AffiliationRank = 1

UNION ALL

Select 'b',* from (
Select QL_StudentID,QL_AcadProgNameId,QL_EnrolStatusDateAchieved as StartDate, QL_ApplicationStatus, QL_EnrollmentStatus ,
'Student' as Role ,
'Current' as Status,
NULL as Enddate,
ROW_NUMBER () OVER ( partition by QL_StudentID,QL_AcadProgNameId
ORDER BY QL_StudentID,QL_AcadProgNameId asc ,QL_ApplicationStatus desc ,QL_EnrollmentStatus desc
) AffiliationRank
from SRC.OfferHolderApplicationDetails  where QL_EnrollmentStatus = 'EFE' and QL_ApplicationStatus != 'ADEF')
Students
where AffiliationRank = 1

