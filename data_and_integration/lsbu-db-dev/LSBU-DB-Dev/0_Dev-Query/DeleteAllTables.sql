--10493
SELECT * FROM SRC.SRC_ROW_TMP

select * FROM [CFG].[SRCLoadRunLog]
select * from [CFG].[SRCLoadFilesLog];

SELECT LRL.* ,LFL.sSRCFileName,LFL.iSRCFileRowNo,LFL.sSRCFileStatus
FROM [CFG].[SRCLoadRunLog] LRL
JOIN [CFG].[SRCLoadFilesLog] LFL ON LRL.SRCLoadRunID = LFL.SRCLoadRunID
WHERE LRL.SRCLoadRunID = 277

-------Merge Procedures
SELECT * FROM [RPT].[SRCValidations]
SELECT * FROM [RPT].[SRCValidationsItems]
--Validation View 
SELECT * FROM [RPT].[SRCValidationsSummary]  where ValidationResultFlag <> 'OK'  and ObjectName  = 'Student'

--Validation Procedures
--exec [CFG].[VA_EducationalInstitution] @RunID = 8
--exec [CFG].[VA_Department] @RunID = 8
--exec [CFG].[VA_AcademicProgram] @RunID = 8
--exec [CFG].[VA_OfferHolder] @RunID = 8
--exec [CFG].[VA_OfferHolderApplicationDetails] @RunID = 8

select * from src.OfferHolderApplicationDetails_Application

--------Merge Procedures
SELECT * FROM [INT].[SRC_EducationalInstitution]
SELECT * FROM [INT].[SRC_Department]
SELECT * FROM [INT].[SRC_AcademicProgram]
SELECT * FROM [INT].SRC_Student
SELECT * FROM [INT].SRC_OfferHolderApplicationDetails_Affiliation
SELECT * FROM [INT].SRC_OfferHolderApplicationDetails_Application
SELECT * FROM [INT].SRC_OfferHolderApplicationDetails_ProgramEnrolment
SELECT * FROM [INT].SRC_OfferHolderApplicationDetails_Term

-----
--'NO CHANGE', 'NEW'
UPDATE [INT].[SRC_EducationalInstitution] SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_Department]  SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_AcademicProgram]  SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_OfferHolder]  SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_OfferHolderApplicationDetails_Affiliation]  SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_OfferHolderApplicationDetails_Application]  SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_OfferHolderApplicationDetails_ProgramEnrolment]  SET ChangeStatus = 'NEW'
UPDATE [INT].[SRC_OfferHolderApplicationDetails_Term]  SET ChangeStatus = 'NEW'

-----

select * from dst.Account_EducationalInstitution

SELECT * FROM [INT].SRC_OfferHolderApplicationDetails_Term


SELECT QLCC.QL_AcadProgNameId,QLCC.QL_UCASCourseCode,count(*)
FROM [INT].SRC_OfferHolderApplicationDetails_Application QLCC
GROUP BY QLCC.QL_AcadProgNameId,QLCC.QL_UCASCourseCode
HAVING count(*) > 1

--------------
--MERGE Procedures
--exec [CFG].[MERGE_EducationalInstitution] @RunID = 47
--exec [CFG].[MERGE_Department] @RunID = 47
--exec [CFG].[MERGE_AcademicProgram] @RunID = 47

--exec [CFG].[MERGE_OfferHolder] @RunID = 47
exec [CFG].[MERGE_OfferHolderApplicationDetails_Affiliation] @RunID = 2
exec [CFG].[MERGE_OfferHolderApplicationDetails_Application] @RunID = 2
exec [CFG].[MERGE_OfferHolderApplicationDetails_ProgramEnrolment] @RunID = 8
exec [CFG].[MERGE_OfferHolderApplicationDetails_Term] @RunID = 2


-------------------------------
--DELETE FROM SRC.SRC_ROW_TMP

--DELETE FROM [CFG].[SRCLoadRunLog]
--DELETE from [CFG].[SRCLoadFilesLog];

DELETE FROM [RPT].[SRCValidations]
DELETE FROM [RPT].[SRCValidationsItems]

----
DELETE FROM [INT].[SRC_EducationalInstitution]
DELETE FROM [INT].[SRC_Department]
DELETE FROM [INT].[SRC_AcademicProgram]
DELETE FROM [INT].SRC_Student
DELETE FROM [INT].SRC_Student_Address
DELETE FROM [INT].[SRC_ApplicationEnrolmentDetails_Affiliation]
DELETE FROM [INT].SRC_ApplicationDetails_Application
DELETE FROM [INT].SRC_EnrolmentDetails
DELETE FROM [INT].SRC_SessionIntakeDetails_Term
DELETE FROM [INT].SRC_saladvisee
--DELETE FROM [INT].SRC_SIDEnquiryDetails_Case
DELETE FROM [INT].SRC_Address
DELETE FROM [INT].SRC_AppliedSciencesPTCD_Affiliation
DELETE FROM [INT].SRC_AppliedSciencesPTCD_CaseTeamMember
DELETE FROM [INT].SRC_AppliedSciencesPTCD_Relationship
DELETE FROM [INT].SRC_AppliedSciencesPTCD_Staff
DELETE FROM [INT].[SRC_Student_SupportProfile]




--REJECT---
select * from DST.SF_Reject_Account_EducationInstitution
select * from DST.SF_Reject_Account_Department
select * from DST.SF_Reject_Account_AcademicProgram

select * from DST.SF_Reject_Contact
select * from DST.SF_Reject_Affiliation

select * from DST.SF_Reject_ApplicationParent
select * from DST.SF_Reject_Application

select * from DST.SF_Reject_ProgramEnrollment
select * from DST.SF_Reject_TermParent
select * from DST.SF_Reject_Term

--------
SELECT * FROM [INT].[SRC_OfferHolder] C
WHERE C.LSB_ExternalID = '9954623'

--UPDATE [INT].[SRC_OfferHolder] SET ChangeStatus = 'UPDATE' WHERE LSB_ExternalID = '9954622'

select * from DST.Deduplicate_Contact_Email

--Jerzy Sosna e9954622@pwc.com
SELECT c.hed__AlternateEmail__c,c.FirstName,c.LastName,c.Id,c.LSB_CON_SourceSystem__c,c.LSB_ExternalID__c 
,c.LSB_CON_ContactDataId__c,c.hed__Contact_JSON__c,c.LSB_CON_SourceSystemID__c
FROM SFC.Get_ID_Contact_OfferHolder c
where c.FirstName = 'TOM'

SELECT C.LSB_ExternalID__c,C.LSB_SourceSystem__c, C.FirstName,C.LastName,C.Email
FROM DST.Contact_OfferHolder C

SELECT OH.LSB_ExternalID, OH.QL_FirstName, OH.QL_Surname,  OH.QL_PersonalEmail ,OH.QL_StudentID,OH.ChangeStatus
FROM [INT].[SRC_OfferHolder] OH
where OH.QL_FirstName = 'Tom'
--WHERE OH.LSB_ExternalID = '9954622'

----------------
--QA - 0053M000000caEbQAI
--DEV - 0053M000000SWytQAG
SELECT * FROM SFC.SF_Users
where Name like 'Data Integration User'

-------------
Select StudentRole.Role,StudentRole.StudentId,StudentRole.Rank from 
(
 Select SUBSTRING(LSB_AON_ExternalID__c,1,CHARINDEX('.', LSB_AON_ExternalID__c,1)-1) as StudentId, hed__Role__c as Role , 
 ROW_NUMBER () OVER ( partition by SUBSTRING(LSB_AON_ExternalID__c,1,CHARINDEX('.', LSB_AON_ExternalID__c,1)-1)
		ORDER BY SUBSTRING(LSB_AON_ExternalID__c,1,CHARINDEX('.', LSB_AON_ExternalID__c,1)-1) ASC, ISNULL(hed__EndDate__c,TRY_CONVERT(date, '01/12/2999', 103 ) ) ASC
	) Rank
 from [DST].[Affiliation_OfferHolderApplicationDetails]
 ) as StudentRole 
 where Rank = 1

select A.hed__Role__c, SUBSTRING(A.LSB_AON_ExternalID__c,1,CHARINDEX('.', A.LSB_AON_ExternalID__c,1)-1) as StudentId
,A.hed__StartDate__c,A.hed__EndDate__c
,A.*
from [DST].[Affiliation_OfferHolderApplicationDetails] A
where SUBSTRING(A.LSB_AON_ExternalID__c,1,CHARINDEX('.', A.LSB_AON_ExternalID__c,1)-1) --= '3820448'
in ('3317670','3425977','3426873','3517251','3523481','3608213','3618868','3626301','3627143','3823146')
order by SUBSTRING(A.LSB_AON_ExternalID__c,1,CHARINDEX('.', A.LSB_AON_ExternalID__c,1)-1),A.hed__EndDate__c

select A.hed__Role__c, SUBSTRING(A.LSB_AON_ExternalID__c,1,CHARINDEX('.', A.LSB_AON_ExternalID__c,1)-1) as StudentId
--,A.hed__StartDate__c
--,A.hed__EndDate__c
,max(ISNULL(A.hed__EndDate__c,TRY_CONVERT(date, '01/12/2999', 103 ) ) ) as max_date
--,TRY_CONVERT(date, '01/12/2999', 103 ) 
from [DST].[Affiliation_OfferHolderApplicationDetails] A
group by A.hed__Role__c, SUBSTRING(A.LSB_AON_ExternalID__c,1,CHARINDEX('.', A.LSB_AON_ExternalID__c,1)-1)

Select StudentRole.Role,StudentRole.StudentId,StudentRole.Rank from 
(
 Select SUBSTRING(LSB_AON_ExternalID__c,1,CHARINDEX('.', LSB_AON_ExternalID__c,1)-1) as StudentId, hed__Role__c as Role , 
 ROW_NUMBER () OVER ( partition by SUBSTRING(LSB_AON_ExternalID__c,1,CHARINDEX('.', LSB_AON_ExternalID__c,1)-1)
		ORDER BY SUBSTRING(LSB_AON_ExternalID__c,1,CHARINDEX('.', LSB_AON_ExternalID__c,1)-1) ASC, ISNULL(hed__EndDate__c,TRY_CONVERT(date, '01/12/2999', 103 ) ) ASC
	) Rank
 from [DST].[Affiliation_OfferHolderApplicationDetails]
 ) as StudentRole 
 where Rank = 1

SELECT *
FROM SRC.OfferHolderApplicationDetails_Affiliation AF
WHERE AF.LSB_ExternalID = '3820448.4687.E'

SELECT *
FROM [DST].[Affiliation_OfferHolderApplicationDetails] AF
WHERE AF.LSB_AON_ExternalID__c = '3820448.4687.E'


 --CONCAT(ISNULL(QLAP.QL_Add1, ''), ISNULL(QLAP.QL_Add2, ''), ISNULL(QLAP.QL_Add3, '')) as BillingStreet,

DECLARE @ad1 VARCHAR(20) = 'A';
DECLARE @ad2 VARCHAR(20) = 'B';
DECLARE @ad3 VARCHAR(20) =  null;

SELECT STUFF(CONCAT(' ' + NULLIF(@ad1, ''),' ' + NULLIF(@ad2, ''),' ' + NULLIF(@ad3, '')),1,1,'') as bb
--CONCAT(COALESCE(@ad1,''),' ',COALESCE(@ad2,''),', ',COALESCE(@ad3,'') ) as aa

 CONCAT(' ' , @ad1, @ad2, @ad3) as BillingStreet2,
 CONCAT(ISNULL(@ad1, ''), ISNULL(@ad2, ''), ISNULL(@ad3, '')) as BillingStreet3,
 CONCAT(ISNULL(@ad1, ''), ISNULL(@ad2, ''), ISNULL(@ad3, '')) as BillingStreet4

 select 
 (CASE 
	WHEN QLDE.QL_Add1 IS NOT NULL AND QLDE.QL_Add2 IS NOT NULL AND QLDE.QL_Add3 IS NOT NULL 
		THEN CONCAT(QLDE.QL_Add1,' ',QLDE.QL_Add2,' ',QLDE.QL_Add3)
	WHEN QLDE.QL_Add1 IS NOT NULL AND QLDE.QL_Add2 IS NOT NULL AND QLDE.QL_Add3 IS NULL
		THEN CONCAT(QLDE.QL_Add1,' ',QLDE.QL_Add2)
	WHEN QLDE.QL_Add1 IS NOT NULL AND QLDE.QL_Add2 IS NULL AND QLDE.QL_Add3 IS NOT NULL
		THEN CONCAT(QLDE.QL_Add1,' ',QLDE.QL_Add3)
	ELSE QLDE.QL_Add1
END) as BillingStreet
--------------------------

SELECT * FROM DST.SF_Reject_Account_AcademicProgram
SELECT * FROM DST.SF_Reject_Contact

SELECT * FROM [INT].[SRC_OfferHolderApplicationDetails_Term] AS QLOHAD
SELECT * FROM [DST].[Term_OfferHolderApplicationDetails]

SELECT * FROM SFC.Get_ID_Account_AcademicProgram GIAP
WHERE GIAP.LSB_ACC_ExternalID__c in (4830,5674)

SELECT * FROM INT.SRC_AcademicProgram AP
WHERE AP.LSB_ExternalID in ('4830','5674')
