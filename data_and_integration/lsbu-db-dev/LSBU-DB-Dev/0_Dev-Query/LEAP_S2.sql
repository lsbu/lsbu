--LEAP-711
--Fields BillingPostalCode and BillingCountry on records with record types  'Univeristy Departament' and 'Academic Program' are empty in Salesforce.
--Fields have value in views [DST].[Account_AcademicProgram] and [DST].[Account_Department] 
SELECT * FROM [DST].[Account_Department]
SELECT * FROM [DST].[Account_AcademicProgram]


--LEAP-725
--Field hed_Admission_Date_c has no value (NULL) in view [DST].[ProgramEnrollment_OfferHolderApplicationDetails]
--According to mapping field hed_Admission_Date_c should be mapped to QL_AdmissionDate, but in input file there is only QL_ApplicationDate.
SELECT * FROM [DST].[ProgramEnrollment_OfferHolderApplicationDetails]

--LEAP-709
/*
Value of LSB_SourceSystemID_c (LSB_ACC_SourceSystemID_c) is incorrect on Account object in Salesforce for record types:
Academic Program
University Department
Educational Institution

Value of the field is 'QL' but according to mapping it should be: value of:

QL_AcadProgNameId (for Academic Program)
QL_DepartmentId  (for University Department)
QL_EducationInstitutionId (for Educational Institution)
*/

SELECT * FROM [SRC].[AcademicProgram]
SELECT * FROM [INT].[SRC_AcademicProgram] QLAP

SELECT * FROM [DST].[Account_EducationalInstitution]
SELECT * FROM [DST].[Account_Department]
SELECT * FROM [DST].[Account_AcademicProgram]


---LEAP-720
--When field Add4 is empty and Add5 have value field BillingCity is null.
SELECT * FROM [SRC].[AcademicProgram]

SELECT QLAP.LSB_ExternalID, QLAP.QL_Add4,QLAP.QL_Add5,
(CASE 
	WHEN QLAP.QL_Add4 IS NOT NULL AND QLAP.QL_Add5 IS NOT NULL
		THEN CONCAT(QLAP.QL_Add4,' ',QLAP.QL_Add5)
	WHEN QLAP.QL_Add4 IS NOT NULL AND QLAP.QL_Add5 IS NULL
		THEN QLAP.QL_Add4
	WHEN QLAP.QL_Add4 IS NULL AND QLAP.QL_Add5 IS NOT NULL
		THEN QLAP.QL_Add5
	ELSE QLAP.QL_Add4
END) as BillingCity
FROM [INT].[SRC_AcademicProgram] QLAP
WHERE QLAP.QL_Add5 is null
AND QLAP.QL_Add5 is not null

SELECT * FROM [DST].[Account_AcademicProgram]
------------------