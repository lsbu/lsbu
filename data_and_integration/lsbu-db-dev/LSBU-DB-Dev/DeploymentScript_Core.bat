echo off

rem sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "SAVE_VALIDATION_RESULT.sql"

echo on

rem Schemas -----------------------------------------------------------------------------------
cd %MAINPATH%\X_Security\Schemas
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem Dbo -----------------------------------------------------------------------------------
cd %MAINPATH%\Dbo
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem RPT -----------------------------------------------------------------------------------
cd %MAINPATH%\RPT\1_Sequences
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\RPT\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\RPT\3_Views
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem STG -----------------------------------------------------------------------------------
cd %MAINPATH%\STG\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem CFG -----------------------------------------------------------------------------------
cd %MAINPATH%\CFG\1_Sequences
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\CFG\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\CFG\4_SRCValidProc
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\CFG\5_Functions
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\CFG\5_SRCObjectValidProc
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\CFG\6_StoredProcedures
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem INT -----------------------------------------------------------------------------------
cd %MAINPATH%\INT\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\INT\6_StoredProcedures
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem SFC -----------------------------------------------------------------------------------
cd %MAINPATH%\SFC\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem SRC -----------------------------------------------------------------------------------
cd %MAINPATH%\SRC\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\SRC\3_Views
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\SRC\5_Procedures
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

rem DST -----------------------------------------------------------------------------------
cd %MAINPATH%\DST\2_Tables
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\DST\3_Views
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%\DST\6_StoredProcedures
for %%G in (*.sql) do sqlcmd /S %SERVERNAME% /d %DBNAME% -U %DBUSER% -P %PASSWORD% -i "%%G"

cd %MAINPATH%