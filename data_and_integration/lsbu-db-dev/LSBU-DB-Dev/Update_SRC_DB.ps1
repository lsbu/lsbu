# Update DB from each script files located in GitHub
$mainpath = Split-Path $script:MyInvocation.MyCommand.Path"\SRC"

#all on SRC schema
#$PatchSRC = "\SRC";
$ParamsSRC = @{
   'ServerInstance' = 'MS-SLFR-DBS-01';
   'Database' = 'didev';
   'Username' = 'migr_source';
   'Password' = 'LSBU!@#4source';
}

# ------ sequence
#Write-Host " "
#Write-Host "----   Run script of all files in: "$mainpath"\sequence\*.sql -------"
#$files = Get-ChildItem $mainpath"\sequence\*.sql"
#foreach ($file in $files) {
#Write-Host $file
#Invoke-Sqlcmd @ParamsSRC -InputFile $file
#}

# ------- tables
#Write-Host " "
#Write-Host "----  Run script of all files in: "$mainpath"\tables\*.sql -------"
#$files = Get-ChildItem $mainpath"\tables\*.sql"
#foreach ($file in $files) {
#Write-Host $file
#Invoke-Sqlcmd @ParamsSRC -InputFile $file
#}

# ------- indexes
#Write-Host " "
#Write-Host "----  Run script of all files in: "$mainpath"\indexes\*.sql -------"
#$files = Get-ChildItem $mainpath"\indexes\*.sql"
#foreach ($file in $files) {
#Write-Host $file
#Invoke-Sqlcmd @Params -InputFile $file
#}

# ------  view
#Write-Host " "
#Write-Host "----  Run script of all files in: "$mainpath"\view\*.sql -------"
#$files = Get-ChildItem $mainpath"\view\*.sql"
#foreach ($file in $files) {
#Write-Host $file
#Invoke-Sqlcmd @Params -InputFile $file
#}


# ------   functions  - another user
#Write-Host " "
#Write-Host "----  Run script of all files in: "$mainpath"\validate\*.sql -------"
#$files = Get-ChildItem $mainpath"\functions\*.sql"
#foreach ($file in $files) {
#Write-Host $file
#Invoke-Sqlcmd @ParamsDBO -InputFile $file
#}


# ---- clts_validate
#Write-Host " "
#Write-Host "----  Run script of all files in: "$mainpath"\clts_validate\*.sql -------"
#$files = Get-ChildItem $mainpath"\clts_validate\*.sql"
#foreach ($file in $files) {
#Write-Host $file
#Invoke-Sqlcmd @Params -InputFile $file
#}

exit 0
cd $mainpath
