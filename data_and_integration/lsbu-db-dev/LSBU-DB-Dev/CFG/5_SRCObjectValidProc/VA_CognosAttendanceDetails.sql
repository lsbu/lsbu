CREATE OR ALTER PROCEDURE [CFG].[VA_CognosAttendanceDetails]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'Cognos';
DECLARE @ObjectName VARCHAR(100) = 'StudentAttendanceDetails';
DECLARE @ViewName VARCHAR(100) = 'STG.tCognosAttendanceDetails';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--ModuleOffering
	col1 as Cognos_StudentID,
	col2 as Cognos_ModuleId,
	col3 as Cognos_AcadProgNameId,
	col4 as Cognos_Term,
	col5 as Cognos_AOSPeriod,
	col6 as Cognos_AcademicPeriod,
	col7 as Cognos_LecturerId,
	col8 as Cognos_ModuleLevelAttendance,
	col9 as Cognos_TotalModuleClasses,
	col10 as Cognos_ModuleLevelAttendancepercent,
	col11 as Cognos_CourseLevelAttendance,
	col12 as Cognos_CourseLevelAttendancepercent
*/

--Cognos_StudentID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = 'STG.tStudent'
				,@Mandatory = 'Y';

	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_StudentID'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'Y';

--Cognos_ModuleId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_ModuleId'
				,@FieldNameRelated = 'QL_ModuleId'
				,@ViewNameRelated = 'STG.tModuleOffering'
				,@Mandatory = 'N';

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_ModuleId'
				,@MaxLength = 255
				,@Mandatory = 'N';

--Cognos_AcadProgNameId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = 'STG.tAcademicProgram'
				,@Mandatory = 'Y';

	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AcadProgNameId'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'Y';

--Cognos_Term
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y';

--Cognos_AOSPeriod
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AOSPeriod'
				,@MaxLength = 255
				,@Mandatory = 'N';

--Cognos_AcademicPeriod
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AcademicPeriod'
				,@MaxLength = 255
				,@Mandatory = 'N';

--Cognos_LecturerId -- no validation

--Cognos_ModuleLevelAttendance
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_ModuleLevelAttendance'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--Cognos_TotalModuleClasses
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_TotalModuleClasses'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--Cognos_ModuleLevelAttendancepercent
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_ModuleLevelAttendancepercent'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--Cognos_CourseLevelAttendance
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_CourseLevelAttendance'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--Cognos_CourseLevelAttendancepercent
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_CourseLevelAttendancepercent'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';
end;