
CREATE OR ALTER     PROCEDURE [CFG].[VA_StudentAdditionalDetails]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'StudentAdditionalDetails';
DECLARE @ViewName VARCHAR(100) = 'STG.tStudentAdditionalDetails';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--StudentAdditionalDetails
  col1 as QL_StudentID,
  col2 as QL_RefugeeStatus,
  col3 as QL_CaringResponsibilities,
  col4 as QL_BeeninCare,
  col5 as QL_EnstrangedfromFamily,
  col6 as QL_AcadProgNameId,
  col7 as QL_Term,
  col8 as QL_AOSPeriod,
  col9 as QL_AcademicPeriod,
*/

--QL_StudentID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

--QL_RefugeeStatus - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_RefugeeStatus'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_CaringResponsibilities - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CaringResponsibilities'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_BeeninCare - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_BeeninCare'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_EnstrangedfromFamily - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnstrangedfromFamily'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_AcadProgNameId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--QL_Term like 'SEPT','MAR','APR','JAN', 'JUN'
	exec CFG.ValidRule51_QL_Term 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y';

--QL_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

end;