CREATE OR ALTER PROCEDURE [CFG].[VA_AppliedSciencesPTCD]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'SAL';
DECLARE @ObjectName VARCHAR(100) = 'AppliedSciencesPTCD';
DECLARE @ViewName VARCHAR(100) = 'STG.tAppliedSciencesPTCD';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--SessionIntakeDetails
col1 as PTCDCSV_StudentId,
col2 as PTCDCSV_StudentName,
col3 as PTCDCSV_StudentEmailAddress,
col4 as PTCDCSV_YearofStudy,
col5 as PTCDCSV_Session,
col6 as PTCDCSV_PersonalTutorFirstName,
col7 as PTCDCSV_PersonalTutorLastName,
col8 as PTCDCSV_PersonalTutorEmailAddress,
col9 as PTCDCSV_CourseCode,
col10 as PTCDCSV_CourseName,
col11 as PTCDCSV_CDFirstName,
col12 as PTCDCSV_CDLastname,
col13 as PTCDCSV_CDEmail,
*/

--PTCDCSV_StudentId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_StudentId'
				,@FieldNameRelated = 'LSB_ExternalID'
				,@ViewNameRelated = 'SRC.vStudent'
				,@Mandatory = 'Y';
			
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_StudentId'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'Y';

--PTCDCSV_StudentName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_StudentName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--PTCDCSV_StudentEmailAddress
	exec CFG.ValidRule06_email 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_StudentEmailAddress'
				,@Mandatory = 'Y';

--PTCDCSV_YearofStudy
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_YearofStudy'
				,@MaxLength = 10
				,@Mandatory = 'N';

--PTCDCSV_Session
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_Session'
				,@MaxLength = 255
				,@Mandatory = 'N';

--PTCDCSV_PersonalTutorFirstName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_PersonalTutorFirstName'
				,@MaxLength = 40
				,@Mandatory = 'N';

--PTCDCSV_PersonalTutorLastName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_PersonalTutorLastName'
				,@MaxLength = 40
				,@Mandatory = 'N';

--PTCDCSV_PersonalTutorEmailAddress
	exec CFG.ValidRule06_email 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_PersonalTutorEmailAddress'
				,@Mandatory = 'N';

	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_PersonalTutorEmailAddress'
				,@FieldNameRelated = 'FederationIdentifier'
				,@ViewNameRelated = 'SFC.Get_ID_User'
				,@Mandatory = 'N';

--PTCDCSV_CourseCode
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_CourseCode'
				,@MaxLength = 255
				,@Mandatory = 'N';

--PTCDCSV_CourseName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_CourseName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--PTCDCSV_CDFirstName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_CDFirstName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--PTCDCSV_CDLastname
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_CDLastname'
				,@MaxLength = 255
				,@Mandatory = 'N';

--PTCDCSV_CDEmail
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_CDEmail'
				,@FieldNameRelated = 'FederationIdentifier'
				,@ViewNameRelated = 'SFC.Get_ID_User'
				,@Mandatory = 'N';
	
	exec CFG.ValidRule06_email 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'PTCDCSV_CDEmail'
				,@Mandatory = 'N';
end;