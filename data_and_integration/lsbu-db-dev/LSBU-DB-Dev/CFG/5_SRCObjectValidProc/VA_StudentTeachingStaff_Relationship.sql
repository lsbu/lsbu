
CREATE OR ALTER     PROCEDURE [CFG].[VA_StudentTeachingStaff_Relationship]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'CMIS';
DECLARE @ObjectName VARCHAR(100) = 'StudentTeachingStaff_Relationship';
DECLARE @ViewName   VARCHAR(100) = 'STG.tStudentTeachingStaff_Relationship';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--StudentTeachingStaff_Contact
  col1 as CMIS_StudentID,
  col2 as CMIS_AcadProgNameId,
  col3 as CMIS_AcademicPeriod,
  col4 as CMIS_AOSPeriod,
  col5 as CMIS_ModuleId,
  col6 as CMIS_ModuleInstance,
  col7 as CMIS_Semester,
  col8 as CMIS_TeachingStaffId,
  col9 as CMIS_TeachingStaffFirstName,
  col10 as CMIS_TeachingStaffLastname,
  col11 as CMIS_TeachingStaffEmail,
*/

--CMIS_StudentID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

--CMIS_AcadProgNameId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--CMIS_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CMIS_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';
  
-- CMIS_ModuleId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_ModuleId'
				,@FieldNameRelated = 'QL_ModuleId'
				,@ViewNameRelated = '[STG].[tModuleOffering]'
				,@Mandatory = 'N';

-- ModuleOffering_LSB_ExternalID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'ModuleOffering_LSB_ExternalID'
				,@FieldNameRelated = 'LSB_ExternalID'
				,@ViewNameRelated = '[STG].[tModuleOffering]'
				,@Mandatory = 'N';

-- TeachingStaff_LSB_ExternalID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'TeachingStaff_LSB_ExternalID'
				,@FieldNameRelated = 'LSB_ExternalID'
				,@ViewNameRelated = '[STG].[tStudentTeachingStaff_Contact]'
				,@Mandatory = 'N';

--CMIS_ModuleInstance - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_ModuleInstance'
				,@MaxLength = 255
				,@Mandatory = 'N';

--CMIS_Semester - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_Semester'
				,@MaxLength = 255
				,@Mandatory = 'N';

--CMIS_TeachingStaffId - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_TeachingStaffId'
				,@MaxLength = 255
				,@Mandatory = 'N';

--CMIS_TeachingStaffFirstName - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_TeachingStaffFirstName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--CMIS_TeachingStaffLastname - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_TeachingStaffLastname'
				,@MaxLength = 255
				,@Mandatory = 'N';

--CMIS_TeachingStaffEmail - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CMIS_TeachingStaffEmail'
				,@MaxLength = 255
				,@Mandatory = 'N';

end;