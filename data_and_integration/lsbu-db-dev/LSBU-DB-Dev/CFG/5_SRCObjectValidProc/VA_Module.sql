CREATE OR ALTER PROCEDURE [CFG].[VA_Module]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'Module';
DECLARE @ViewName VARCHAR(100) = 'STG.tModule';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--Module
  col1 as QL_ModuleId,
  col2 as QL_ModuleName,
  col3 as QL_DepartmentId,
  col4 as QL_EducationInstitutionId,
  col5 as QL_ModuleLevel,
  col6 as QL_ModuleStatus
*/

--QL_ModuleId -- INTEGER, Not Null
	exec CFG.ValidRule08_IsUnique 
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleId'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_ModuleName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_DepartmentId - INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DepartmentId'
				,@FieldNameRelated = 'QL_DepartmentId'
				,@ViewNameRelated = 'STG.tDepartment'
				,@Mandatory = 'Y';

--QL_EducationInstitutionId - INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EducationInstitutionId'
				,@FieldNameRelated = 'QL_EducationInstitutionId'
				,@ViewNameRelated = 'STG.tEducationalInstitution'
				,@Mandatory = 'Y';

--QL_ModuleLevel - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleLevel'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ModuleStatus - VARCHAR(40)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleStatus'
				,@MaxLength = 40
				,@Mandatory = 'N';

end;