CREATE OR ALTER PROCEDURE [CFG].[VA_ClearValidationTables]
			(
			@RunID INTEGER
			)
AS
BEGIN

DELETE FROM [RPT].[SRCValidations];
DELETE FROM [RPT].[SRCValidationsItems];

END;
GO