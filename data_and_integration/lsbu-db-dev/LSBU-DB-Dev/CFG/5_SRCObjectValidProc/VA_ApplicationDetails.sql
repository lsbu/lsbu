CREATE OR ALTER PROCEDURE [CFG].[VA_ApplicationDetails] (
  @RunID INTEGER
)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'ApplicationDetails';
DECLARE @ViewName VARCHAR(100) = 'STG.tApplicationDetails_Application';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--OfferHolderApplicationDetails
	col1 as QL_StudentID,
	col2 as QL_UCASPersonalID,
	col3 as QL_UCASApplicationCode,
	col4 as QL_UCASApplicationNumber,
	col5 as QL_ApplicationDate,
	col6 as QL_AcadProgNameId,
	col7 as QL_IsItPrimaryAcademicProg,
	col8 as QL_ApplicationStatus,
	col9 as QL_ApplicationType,
	col10 as QL_AcademicPeriod,
	col11 as QL_ProgramStartDate,
	col12 as QL_ProgramEndDate,
	col13 as QL_ModeOfStudy,
	col15 as QL_CreatedDate,
	col16 as QL_LastModifiedDate,
	col17 as QL_ApplicationSource,
	col18 as QL_Term,
	col19 as QL_AOSPeriod,
	col20 as QL_DepositStatus,
	col21 as QL_VisaStatus,
	col22 as QL_ApplicantAgentCode,
	col23 as QL_ApplicantAgencyName,
	col24 as QL_PreviousInstituteName,
	col25 as QL_UCASCourseCode,
	col26 as QL_ApplicantStageIndicator,
	col28 as QL_CriminalConvictions,
	col29 as QL_VisaType,
	col30 as QL_NominatedName,
	col31 as QL_NominatedRelationship,
	col32 as QL_AttedenceType,
	col33 as QL_SessionStartDate,
	col34 as QL_SessionEndDate,
	col35 as QL_ChoiceNumber,
	col36 as QL_CampusInformation,
	col37 as QL_AppStatusDateAchieved,
	col39 as QL_QualificationCheck,
*/

--LSB_ExternalID, Not Null , Unique
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_StudentID - INTEGER
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'Y';

	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

--QL_UCASPersonalID - INTEGER
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASPersonalID'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--QL_UCASApplicationCode - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASApplicationCode'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_UCASApplicationNumber
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASApplicationNumber'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_ApplicationDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicationDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_AcadProgNameId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--QL_IsItPrimaryAcademicProg
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_IsItPrimaryAcademicProg'
				,@ListValues = 'TRUE,FALSE'
				,@Mandatory = 'N';

--QL_ApplicationStatus
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicationStatus'
				,@ListValues = 'AACPT,AALT,AAPP,AARC,ACD,ACF,ACI,ACO,ADBD,ADCC,ADEC,ADEF,ADEPCO,ADEPP,ADEPUF,ADEPUO,ADUP,AINT,AINTV,ARAI,ARBD,ARBI,AREJ,AREL,AUD,AUF,AUI,AUKP,AUO,AWD,AWDX'
				,@Mandatory = 'Y';

--QL_ApplicationType
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicationType'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_AcademicPeriod
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_ProgramStartDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ProgramStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ProgramEndDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ProgramEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ModeOfStudy
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModeOfStudy'
				,@MaxLength = 50
				,@Mandatory = 'N';
/*
--QL_EnrollmentStatus - 'A,P,S'--'Applicant,Prospect,Student'
--Field removed in new mapping, this data is now available in the EnrolmentDetails entity
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrollmentStatus'
				,@ListValues = 'EADZ,EAPP,EAPPT,EASS,EAWD,EAZZ,EBAC,EBIB,EBIE,ECCNR,ECCP,EDISSO,EDNE,EDUP,EEXC,EEXO,EFAFU,EFE,EFMA,EGRAD,EINS,EINTA,EINTH,ENA,ENBAC,EOCS,EOER,ERCOU,ERORU,ESD,ESLEP,ESUS,ETROC,EVOID,EWD,EZZZ'
				,@Mandatory = 'N';*/

--QL_CreatedDate - Application Created in the Source System	DATETIME - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CreatedDate'
				,@Mandatory = 'N';

--QL_LastModifiedDate - Application Lastmodified in the Source System	DATETIME  - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LastModifiedDate'
				,@Mandatory = 'N';

--QL_ApplicationSource - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicationSource'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_Term like 'SEPT','MAR','APR','JAN'
	exec CFG.ValidRule51_QL_Term 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y';

--QL_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'Y';

--QL_DepositStatus - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DepositStatus'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_VisaStatus - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_VisaStatus'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ApplicantAgentCode - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicantAgentCode'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_ApplicantAgencyName - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicantAgencyName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_PreviousInstituteName - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_PreviousInstituteName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_UCASCourseCode - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASCourseCode'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ApplicantStageIndicator - Picklist(A), VARCHAR(255)
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ApplicantStageIndicator'
				,@ListValues = 'A'
				,@Mandatory = 'Y';

/*
--QL_EnrolmentStageIndicator - Picklist(E or Null), VARCHAR(255)
--Field removed in new mapping, this data is now available in the EnrolmentDetails entity
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrolmentStageIndicator'
				,@ListValues = 'E'
				,@Mandatory = 'N'; */

--QL_CriminalConvictions - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CriminalConvictions'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_VisaType - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_VisaType'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_NominatedName - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_NominatedName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_NominatedRelationship - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_NominatedRelationship'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_AttedenceType - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AttedenceType'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_SessionStartDate - DATE('dd/mm/yyyy')
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SessionStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_SessionEndDate - DATE('dd/mm/yyyy')
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SessionEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ChoiceNumber -VARCHAR(255)
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ChoiceNumber'
				,@Precision = 10
				,@Scale = 0
				,@Mandatory = 'N';

--QL_CampusInformation - Picklist(C,D,E,L,M,S,V,X)
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CampusInformation'
				,@ListValues = 'C,D,E,L,M,S,V,X'
				,@Mandatory = 'N';

--QL_AppStatusDateAchieved - DATETIME
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AppStatusDateAchieved'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

/*				
--QL_EnrolStatusDateAchieved - DATETIME
--Field removed in new mapping, this data is now available in the EnrolmentDetails entity
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrolStatusDateAchieved'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N'; */

--QL_QualificationCheck - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_QualificationCheck'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_DecisionStatus
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DecisionStatus'
				,@ListValues = 'AO14,AO15,AO16,AO17,AO18,AO19,AO20,AO21,AO22,AO23,AO24,AO25,AO26,AO3,AO4,AO5,AO6,AO7,AO8,AO9,N14,N20,A10,A11,A12,A13,AO2,AO27,ENR1,ENR2,ENR3,ENR4,N1,N10,N11,N12,N13,N15,N16,N17,N18,N19,N2,N21,N22,N23,N24,N25,N3,N4,N5,N6,N7,N8,N9,AO1'
				,@Mandatory = 'N';

--QL_QualificationCheck
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DecisionNotes'
				,@MaxLength = 1000
				,@Mandatory = 'N';

--QL_DecisionDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DecisionDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_QualCode
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_QualCode'
				,@ListValues = 'UOS,LOW T,UNC-1,UOS-S,REJL2'
				,@Mandatory = 'N';

--QL_UCASStatus
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASStatus'
				,@ListValues = '010,020,030,040,050,060,070,080,090,100,110,120,130,140,150,160,170'
				,@Mandatory = 'N';

--QL_RejectCode
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OfferText'
				,@MaxLength = 592
				,@Mandatory = 'N';

--QL_CountryofBirth
	/*exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CountryofBirth'
				,@MaxLength = 255
				,@Mandatory = 'N';*/

--QL_FeeCode
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_FeeCode'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_Liveathome
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Liveathome'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_LateUcasApplicant
exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LateUcasApplicant'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_DSDecisionstatus
exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DSDecisionstatus'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_InternationalFunding
exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_InternationalFunding'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_CasNo
/*exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OfferText'
				,@MaxLength = 592
				,@Mandatory = 'N';

--QL_CasStatus
exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OfferText'
				,@MaxLength = 592
				,@Mandatory = 'N';

--QL_TotalDiscount
exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OfferText'
				,@MaxLength = 592
				,@Mandatory = 'N';

--QL_VisaRequired
exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OfferText'
				,@MaxLength = 592
				,@Mandatory = 'N';*/

end;