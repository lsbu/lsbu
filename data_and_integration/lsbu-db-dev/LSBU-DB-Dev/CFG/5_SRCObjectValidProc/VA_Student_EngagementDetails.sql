CREATE OR ALTER PROCEDURE [CFG].[VA_Student_EngagementDetails]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'Cognos';
DECLARE @ObjectName VARCHAR(100) = 'StudentEngagementDetails';
DECLARE @ViewName VARCHAR(100) =   'STG.tStudent_EngagementDetails';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*
  --StudentEngagementDetails
  col1 as Cognos_StudentID,
  col2 as Cognos_AcadProgNameId,
  col3 as Cognos_Term,
  col4 as Cognos_AOSPeriod,
  col5 as Cognos_AcademicPeriod,
  col6 as Cognos_TurnstileActivity,
  col7 as Cognos_LibraryAccessActivity,
  col8 as Cognos_MoodleActivity
*/

-- Cognos_StudentID --> INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

--Cognos_AcadProgNameId --> INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--Cognos_Term --> Picklist(SEPT,MAR,APR,JAN,JUN),  Not Null
	exec CFG.ValidRule51_QL_Term 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y';

--Cognos_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--Cognos_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';


--Cognos_TurnstileActivity - INTEGER
	exec CFG.ValidRule03_number 
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_TurnstileActivity'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--Cognos_LibraryAccessActivity - INTEGER
	exec CFG.ValidRule03_number 
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_LibraryAccessActivity'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--Cognos_MoodleActivity - INTEGER
	exec CFG.ValidRule03_number 
			 	 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Cognos_MoodleActivity'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

end;