CREATE OR ALTER PROCEDURE [CFG].[VA_AcademicProgram]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'AcademicProgram';
DECLARE @ViewName VARCHAR(100) = 'STG.tAcademicProgram';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--AcademicProgram
col1 as QL_AcadProgNameId,
col2 as QL_AcadProgName,
col3 as QL_DepartmentId,
col4 as QL_EducationInstitutionId,
col5 as QL_Website,
col6 as QL_Phone,
col7 as QL_Add1,
col8 as QL_Add2,
col9 as QL_Add3,
col10 as QL_Add4,
col11 as QL_Add5,
col12 as QL_Postcode,
col13 as QL_Country,
col14 as QL_Description,
col15 as QL_CreatedDate,
col16 as QL_LastModifiedDate,
col17 as QL_UCASCourseCode,
col18 as QL_CourseAdministrator,
col19 as QL_CourseDirector,
col20 as QL_TypeofCourse
*/

--LSB_ExternalID
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_AcadProgNameId - INTEGER, Not Null , Unique
/*	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@Mandatory = 'Y';
				*/
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@MaxLength = 255
				,@Mandatory = 'Y';

--QL_AcadProgName - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_DepartmentId - INTEGER, Not Null , Unique
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DepartmentId'
				,@FieldNameRelated = 'QL_DepartmentId'
				,@ViewNameRelated = 'SRC.vEducationInstitution_Department_Union'
				,@Mandatory = 'Y';

--QL_EducationInstitutionId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EducationInstitutionId'
				,@FieldNameRelated = 'QL_EducationInstitutionId'
				,@ViewNameRelated = 'STG.tEducationalInstitution'
				,@Mandatory = 'Y';

-- QL_Website - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Website'
				,@MaxLength = 255
				,@Mandatory = 'N';

	exec CFG.ValidRule09_url
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Website'
				,@Mandatory = 'N';

-- QL_Phone,
	exec CFG.ValidRule07_phone
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Phone' --Phone
				,@MaxLength = 40
				,@Mandatory = 'N';

-- QL_Add1 - VARCHAR(150)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add1'
				,@MaxLength = 150
				,@Mandatory = 'N';

-- QL_Add2 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add2'
				,@MaxLength = 50
				,@Mandatory = 'N';

-- QL_Add3 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add3'
				,@MaxLength = 50
				,@Mandatory = 'N';

-- QL_Add4 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add4'
				,@MaxLength = 50
				,@Mandatory = 'N';

-- QL_Add5 - VARCHAR(40)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add5'
				,@MaxLength = 40
				,@Mandatory = 'N';

-- QL_Postcode - VARCHAR(20)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Postcode'
				,@MaxLength = 20
				,@Mandatory = 'N';

-- QL_Country - VARCHAR(80)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Country'
				,@MaxLength = 80
				,@Mandatory = 'N';

-- QL_Description - VARCHAR(200)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Description'
				,@MaxLength = 200
				,@Mandatory = 'N';


--QL_CreatedDate - Application Created in the Source System	DATETIME - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CreatedDate'
				,@Mandatory = 'N';

--QL_LastModifiedDate - Application Lastmodified in the Source System	DATETIME  - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LastModifiedDate'
				,@Mandatory = 'N';

--QL_UCASCourseCode 
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASCourseCode'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_CourseAdministrator
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CourseAdministrator'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_CourseDirector
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CourseDirector'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_TypeofCourse
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_TypeofCourse'
				,@ListValues = 'Undergraduate,PostGraduate,Apprenticeship'
				,@Mandatory = 'N';

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_TypeofCourse'
				,@MaxLength = 255
				,@Mandatory = 'N';
end;