CREATE OR ALTER PROCEDURE [CFG].[VA_Staff]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'Staff';
DECLARE @ViewName VARCHAR(100) = 'STG.tStaff';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--SessionIntakeDetails
QL_StaffID,
QL_StaffUsername,
QL_StaffEmailAddress,
QL_StaffFirstName,
QL_StaffLastName,
QL_Gender
*/

--QL_StaffId
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffId'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffId'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'Y';

--QL_StaffUsername
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffUsername'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffUsername'
				,@MaxLength = 255
				,@Mandatory = 'Y';

--QL_StaffEmailAddress
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffEmailAddress'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffEmailAddress'
				,@MaxLength = 255
				,@Mandatory = 'Y';

--QL_StaffFirstName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffFirstName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_StaffLastName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StaffLastName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_Gender
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Gender'
				,@MaxLength = 255
				,@Mandatory = 'N';
end;
