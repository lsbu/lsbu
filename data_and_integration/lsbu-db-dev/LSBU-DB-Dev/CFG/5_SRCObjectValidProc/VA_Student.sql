CREATE OR ALTER PROCEDURE [CFG].[VA_Student]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'Student';
DECLARE @ViewName VARCHAR(100) = 'STG.tStudent';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--Student
col1 as QL_StudentID,
col2 as QL_UCASPersonalID,
col3 as QL_Title,
col4 as QL_Initials,
col5 as QL_FirstName,
col6 as QL_FamiliarName,
col7 as QL_Surname,
col8 as QL_MiddleName,
col9 as QL_Birthdate,
col10 as QL_StudentStatus,
col11 as QL_Gender,
col12 as QL_Domicile,
col13 as QL_Origin,
col14 as QL_Nationality,
col15 as QL_Disabilty,
col16 as QL_Add1,
col17 as QL_Add2,
col18 as QL_Add3,
col19 as QL_Add4,
col20 as QL_Add5,
col21 as QL_Postcode,
col22 as QL_Country,
col23 as QL_AddressType,
col24 as QL_Phone,
col25 as QL_LSBUEmail,
col26 as QL_PersonalEmail,
col27 as QL_MobileNo,
col28 as QL_CreatedDate,
col29 as QL_LastModifiedDate,
col30 as QL_DisabiltyAllowance,
col31 as QL_Term_Add1,
col32 as QL_Term_Add2,
col33 as QL_Term_Add3,
col34 as QL_Term_Add4,
col35 as QL_Term_Add5,
col36 as QL_Term_Postcode,
col37 as QL_Term_Country,
col38 as QL_StudentUsername,
col39 as QL_DDSMarking,
col40 as QL_CareLeaver
*/

--LSB_ExternalID
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_StudentID - INTEGER, Not Null , Unique
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@Mandatory = 'Y';

--QL_UCASPersonalID - INTEGER, Not Null , Unique
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASPersonalID'
				,@Mandatory = 'N';

--QL_Title - VARCHAR(128)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Title'
				,@MaxLength = 128
				,@Mandatory = 'N';

--QL_Initials - VARCHAR(10)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Initials'
				,@MaxLength = 10
				,@Mandatory = 'N';


--QL_FirstName - VARCHAR(40) and QL_Surname - VARCHAR(80)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_FirstName'
				,@MaxLength = 40
				,@Mandatory = 'N';

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Surname'
				,@MaxLength = 80
				,@Mandatory = 'N';

	exec CFG.ValidRule12_dependant_fields
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName1 = 'QL_FirstName'
				,@FieldName2 = 'QL_Surname';

--QL_FamiliarName - VARCHAR(40)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_FamiliarName'
				,@MaxLength = 40
				,@Mandatory = 'N';

--QL_MiddleName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_MiddleName'
				,@MaxLength = 40
				,@Mandatory = 'N';

--QL_Birthdate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Birthdate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_StudentStatus - PickList  (H,O,U,E,N,C) can be NUll
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentStatus'
				,@ListValues = 'H,O,U,E,N,C' 
				,@Mandatory = 'N';

--QL_Gender - PickList(Male,Female,Other) was 'F,M,O'
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Gender'
				,@ListValues = 'F,M,O'
				,@Mandatory = 'N';

--QL_Domicile
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Domicile'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_Origin
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Origin'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_Nationality
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Nationality'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_Disabilty
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Disabilty'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Add1 - VARCHAR(150)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add1'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Add2 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add2'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Add3 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add3'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Add4 - VARCHAR(40)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add4'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Add5 - VARCHAR(40)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add5'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Postcode - VARCHAR(20)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Postcode'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Country - VARCHAR(80)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Country'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_AddressType
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AddressType'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_Phone
	exec CFG.ValidRule07_phone
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Phone'
				,@MaxLength = 11
				,@Mandatory = 'N';
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Phone'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_LSBUEmail & QL_PersonalEmail
	exec CFG.ValidRule06_email 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LSBUEmail'
				,@Mandatory = 'N';

	exec CFG.ValidRule06_email 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_PersonalEmail'
				,@Mandatory = 'N';

	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_PersonalEmail'
				,@Mandatory = 'N'
				,@Warning = 1;

	exec CFG.ValidRule12_dependant_fields
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName1 = 'QL_LSBUEmail'
				,@FieldName2 = 'QL_PersonalEmail';
--QL_MobileNo
	exec CFG.ValidRule07_phone
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_MobileNo' --Phone
				,@MaxLength = 11
				,@Mandatory = 'N';
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_MobileNo'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_CreatedDate - Application Created in the Source System	DATETIME - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CreatedDate'
				,@Mandatory = 'N';

--QL_LastModifiedDate - Application Lastmodified in the Source System	DATETIME  - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LastModifiedDate'
				,@Mandatory = 'N';

--col30 as QL_DisabiltyAllowance,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DisabiltyAllowance'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col31 as QL_Term_Add1,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Add1'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col32 as QL_Term_Add2,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Add2'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col33 as QL_Term_Add3,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Add3'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col34 as QL_Term_Add4,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Add4'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col35 as QL_Term_Add5,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Add5'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col36 as QL_Term_Postcode,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Postcode'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col37 as QL_Term_Country,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term_Country'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col38 as QL_StudentUsername,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentUsername'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col39 as QL_DDSMarking,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DDSMarking'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col40 as QL_CareLeaver,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CareLeaver'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col42 as QL_MobileNo_Status
	exec CFG.ValidRule52_QL_Student
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName;

--col43 as QL_OptOut_SU,
  	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OptOut_SU'
				,@ListValues = 'F,T'
				,@Mandatory = 'N';

--col45 as QL_Ethinicity
  	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Ethinicity'
				,@ListValues = 'Hispanic or Latino,Not Hispanic or Latino,10,11,12,13,14,15,16,19,21,22,29,31,32,33,34,39,41,42,43,49,50,80,90,98'
				,@Mandatory = 'N';
end;