CREATE OR ALTER PROCEDURE [CFG].[VA_SessionIntakeDetails]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'SessionIntakeDetails';
DECLARE @ViewName VARCHAR(100) = 'STG.tSessionIntakeDetails_Term';
DECLARE @ViewName_Child VARCHAR(100) = 'SRC.vSessionIntakeDetails_ChildTerm_Validation';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--SessionIntakeDetails
col1 as QL_AcadProgNameId,
col2 as QL_AOSPeriod,
col3 as QL_Term,
col4 as QL_AcademicPeriod,
col5 as QL_SessionStartDate,
col6 as QL_SessionEndDate,
*/

--LSB_ExternalID
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_AcadProgNameId - INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName_Child
				,@FieldName = 'QL_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = 'STG.tAcademicProgram'
				,@Mandatory = 'Y';

--QL_AOSPeriod - VARCHAR(50), Not Null
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName_Child
				,@FieldName = 'QL_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'Y';

--QL_Term - VARCHAR(255), Not Null
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName_Child
				,@FieldName = 'QL_Term'
				,@MaxLength = 255
				,@Mandatory = 'Y';

--QL_AcademicPeriod - VARCHAR(255), Not Null
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName_Child
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 255
				,@Mandatory = 'Y';

--QL_SessionStartDate - DATE
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName_Child
				,@FieldName = 'QL_SessionStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_SessionEndDate - DATE
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName_Child
				,@FieldName = 'QL_SessionEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

end;
