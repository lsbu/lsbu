CREATE OR ALTER PROCEDURE [CFG].[VA_ModuleEnrolmentDetails]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'ModuleEnrolmentDetails';
DECLARE @ViewName   VARCHAR(100) = 'STG.tModuleEnrolmentDetails';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--ModuleEnrolmentDetails
  col1 as QL_StudentID,
  col2 as QL_ModuleId,
  col3 as QL_ModuleInstance,
  col4 as QL_AcadProgNameId,
  col5 as QL_Term,  
  col6 as QL_AOSPeriod,
  col7 as QL_AcademicPeriod,
  col8 as QL_EnrollmentStatus,
  col13 as QL_EnrolmentStageIndicator,
  col9 as QL_EnrolStatusDateAchieved,
  col10 as QL_Compulsory
*/

-- QL_StudentID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

-- QL_ModuleId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleId'
				,@FieldNameRelated = 'QL_ModuleId'
				,@ViewNameRelated = '[STG].[tModuleOffering]'
				,@Mandatory = 'N';

--QL_AcadProgNameId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--QL_Term like 'SEPT','MAR','APR','JAN', 'JUN'
	exec CFG.ValidRule51_QL_Term 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y';

--QL_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_EnrollmentStatus
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrollmentStatus'
				,@ListValues = 'EADZ,EAPP,EAPPT,EASS,EAWD,EAZZ,EBAC,EBIB,EBIE,ECCNR,ECCP,EDISSO,EDNE,EDUP,EEXC,EEXO,EFAFU,EFE,EFMA,EGRAD,EINS,EINTA,EINTH,ENA,ENBAC,EOCS,EOER,ERCOU,ERORU,ESD,ESLEP,ESUS,ETROC,EVOID,EWD,EZZZ'
				,@Mandatory = 'Y';

--QL_EnrolmentStageIndicator
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrolmentStageIndicator'
				,@ListValues = 'E'
				,@Mandatory = 'N';
        

--QL_EnrolStatusDateAchieved
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrolStatusDateAchieved'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

end;