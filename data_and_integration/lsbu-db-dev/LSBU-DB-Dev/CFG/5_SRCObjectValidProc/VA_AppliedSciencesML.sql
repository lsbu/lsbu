CREATE OR ALTER PROCEDURE [CFG].[VA_AppliedSciencesML]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'SAL';
DECLARE @ObjectName VARCHAR(100) = 'AppliedSciencesML';
DECLARE @ViewName VARCHAR(100) = 'STG.tAppliedSciencesML';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--SessionIntakeDetails
col1 as MLCSV_CourseCode,
col2 as MLCSV_CourseName,
col3 as MLCSV_YearofStudy,
col4 as MLCSV_Session,
col5 as MLCSV_MLFirstName,
col6 as MLCSV_MLLastName,
col7 as MLCSV_MLEmail
*/

--col1 as MLCSV_CourseCode
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_CourseCode'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col2 as MLCSV_CourseName,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_CourseName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col3 as MLCSV_YearofStudy,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_YearofStudy'
				,@MaxLength = 10
				,@Mandatory = 'N';

--col4 as MLCSV_Session,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_Session'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col5 as MLCSV_MLFirstName,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_MLFirstName'
				,@MaxLength = 255
				,@Mandatory = 'N';

--col6 as MLCSV_MLLastName,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_MLLastName'
				,@MaxLength = 255
				,@Mandatory = 'N';
		
--col7 as MLCSV_MLEmail
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'MLCSV_MLEmail'
				,@FieldNameRelated = 'Email'
				,@ViewNameRelated = 'SFC.Get_ID_User'
				,@Mandatory = 'N';
end;
GO