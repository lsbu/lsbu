
CREATE OR ALTER     PROCEDURE [CFG].[VA_ModuleComponentandSubComponent]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'ModuleComponentandSubComponent';
DECLARE @ViewName VARCHAR(100) = 'STG.tModuleComponentandSubComponent';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--ExetenuatingCirc
  col1 as QL_ModuleId,
  col3 as QL_ModuleInstance,
  col4 as QL_AcademicPeriod,
  col5 as QL_ModuleStartDate,
  col6 as QL_ModuleEndDate,
  col7 as QL_ComponentId,
  col8 as QL_ComponentDesc,
  col9 as QL_ComponentHandinDate,
  col10 as QL_SubComponentId,
  col11 as QL_SubComponentDesc,
  col12 as QL_SubComponentHandinDate,
*/

-- QL_ModuleId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleId'
				,@FieldNameRelated = 'QL_ModuleId'
				,@ViewNameRelated = '[STG].[tModuleOffering]'
				,@Mandatory = 'N';

--QL_ModuleInstance - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleInstance'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_ModuleStartDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ModuleEndDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ComponentId - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ComponentId'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ComponentDesc - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ComponentDesc'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ComponentHandinDate - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ComponentHandinDate'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_SubComponentId - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SubComponentId'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_SubComponentDesc - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SubComponentDesc'
				,@MaxLength = 255
				,@Mandatory = 'N';
        
--QL_SubComponentHandinDate - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SubComponentHandinDate'
				,@MaxLength = 255
				,@Mandatory = 'N';

end;