CREATE OR ALTER PROCEDURE [CFG].[VA_Grade]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'Grade';
DECLARE @ViewName VARCHAR(100) = 'STG.tGrade';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--Grade
  col1 as QL_StudentID,
  col2 as QL_ModuleId,
  col3 as QL_ModuleInstance,
  col4 as QL_AcadProgNameId,
  col5 as QL_Term,
  col6 as QL_AOSPeriod,
  col7 as QL_AcademicPeriod,
  col8 as QL_Semester,
  col9 as QL_Attempt,
  col10 as QL_OverallCourseMarks,
  col11 as QL_OverallCourseGrade,
  col12 as QL_OverallModuleMarks,
  col13 as QL_OverallModuleGrade,
  CW1,
  CW2,
  CW3,
  CW4,
  CW5,
  CW6,
  EX1,
  EX2,
  EX3,
  EX4,
  EX5
*/

--QL_StudentID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@FieldNameRelated = 'QL_StudentID'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

-- QL_ModuleId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleId'
				,@FieldNameRelated = 'QL_ModuleId'
				,@ViewNameRelated = '[STG].[tModuleOffering]'
				,@Mandatory = 'Y';

--QL_ModuleInstance - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleInstance'
				,@MaxLength = 255
				,@Mandatory = 'Y';


--QL_AcadProgNameId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--QL_Term like 'SEPT','MAR','APR','JAN', 'JUN'
	exec CFG.ValidRule51_QL_Term 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y';

--QL_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'Y';

--QL_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'Y';

--QL_Semester- VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Semester'
				,@MaxLength = 50
				,@Mandatory = 'Y';

--QL_Attempt- VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Attempt'
				,@MaxLength = 50
				,@Mandatory = 'Y';


--QL_OverallCourseMarks- VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OverallCourseMarks'
				,@MaxLength = 50
				,@Mandatory = 'N';


--QL_OverallCourseGrade- VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OverallCourseGrade'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_OverallModuleMarks- VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OverallModuleMarks'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_OverallModuleGrade- VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_OverallModuleGrade'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CW1,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CW1'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CW2,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CW2'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CW3,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CW3'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CW4,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CW4'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CW5,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CW5'
				,@MaxLength = 50
				,@Mandatory = 'N';

--CW6,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'CW6'
				,@MaxLength = 50
				,@Mandatory = 'N';

--EX1,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'EX1'
				,@MaxLength = 50
				,@Mandatory = 'N';

--EX2,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'EX2'
				,@MaxLength = 50
				,@Mandatory = 'N';

--EX3,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'EX3'
				,@MaxLength = 50
				,@Mandatory = 'N';

--EX4,
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'EX4'
				,@MaxLength = 50
				,@Mandatory = 'N';

--EX5
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'EX5'
				,@MaxLength = 50
				,@Mandatory = 'N';

end;