CREATE OR ALTER PROCEDURE [CFG].[VA_ModuleOffering]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'ModuleOffering';
DECLARE @ViewName VARCHAR(100) = 'STG.tModuleOffering';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--ModuleOffering
	col1 as QL_ModuleId,
	col2 as QL_ModuleInstanceName,
	col3 as QL_ModuleInstance,
	col4 as QL_AcademicPeriod,
	col5 as QL_ModuleStartDate,
	col6 as QL_ModuleEndDate,
	col7 as QL_ModuleCredit,
	col8 as QL_ModuleLeader
*/

--QL_ModuleId -- INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleId'
				,@FieldNameRelated = 'QL_ModuleId'
				,@ViewNameRelated = '[STG].[tModule]'
				,@Mandatory = 'N';

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleId'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ModuleInstanceName
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleInstanceName'
				,@MaxLength = 255
				,@Mandatory = 'N';
/*
--QL_ModuleInstance
	exec CFG.ValidRule03_number
				 @RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleInstance'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N'
*/
--QL_AcademicPeriod
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_ModuleStartDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ModuleEndDate
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ModuleCredit
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleCredit'
				,@MaxLength = 20
				,@Mandatory = 'N';

--QL_ModuleLeader
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModuleLeader'
				,@MaxLength = 255
				,@Mandatory = 'N';

end;