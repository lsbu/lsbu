CREATE OR ALTER PROCEDURE [CFG].[VA_EducationalInstitution](
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'EducationalInstitution';
DECLARE @ViewName VARCHAR(100) = 'STG.tEducationalInstitution';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

END;

BEGIN

/*--SRC.EducationalInstitution
col1 as QL_EducationInstitutionId,
col2 as QL_EducationInstitutionName,
col3 as QL_Website,
col4 as QL_Phone,
col5 as QL_Add1,
col6 as QL_Add2,
col7 as QL_Add3,
col8 as QL_Add4,
col9 as QL_Add5,
col10 as QL_Postcode,
col11 as QL_Country,
col12 as QL_Description,
col13 as QL_CreatedDate,
col14 as QL_LastModifiedDate,
*/

--LSB_ExternalID
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_EducationInstitutionId
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EducationInstitutionId'
				,@Mandatory = 'Y';
-- check QL_EducationInstitutionId is connected to SRC.AcademicProgram.QL_EducationInstitutionId
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EducationInstitutionId'
				,@FieldNameRelated = 'QL_EducationInstitutionId'
				,@ViewNameRelated = 'STG.tAcademicProgram'
				,@Mandatory = 'Y';

-- QL_EducationInstitutionName - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EducationInstitutionName'
				,@MaxLength = 255
				,@Mandatory = 'N';

-- QL_Website - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Website'
				,@MaxLength = 255
				,@Mandatory = 'N';

	exec CFG.ValidRule09_url
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Website'
				,@Mandatory = 'N';

-- QL_Phone,
	exec CFG.ValidRule07_phone
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Phone' --Phone
				,@MaxLength = 40
				,@Mandatory = 'N';

-- QL_Add1 - VARCHAR(150)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add1'
				,@MaxLength = 150
				,@Mandatory = 'N';

-- QL_Add2 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add2'
				,@MaxLength = 50
				,@Mandatory = 'N';

-- QL_Add3 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add3'
				,@MaxLength = 50
				,@Mandatory = 'N';

-- QL_Add4 - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add4'
				,@MaxLength = 50
				,@Mandatory = 'N';

-- QL_Add5 - VARCHAR(40)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Add5'
				,@MaxLength = 40
				,@Mandatory = 'N';

-- QL_Postcode - VARCHAR(20)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Postcode'
				,@MaxLength = 20
				,@Mandatory = 'N';

-- QL_Country - VARCHAR(80)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Country'
				,@MaxLength = 80
				,@Mandatory = 'N';

-- QL_Description - VARCHAR(200)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Description'
				,@MaxLength = 200
				,@Mandatory = 'N';

--QL_CreatedDate - Application Created in the Source System	DATETIME - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CreatedDate'
				,@Mandatory = 'N';

--QL_LastModifiedDate - Application Lastmodified in the Source System	DATETIME  - Should be empty
	exec CFG.ValidRule10_mustBeBlank
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LastModifiedDate'
				,@Mandatory = 'N';

				
end;