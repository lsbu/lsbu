CREATE OR ALTER PROCEDURE [CFG].[VA_CourseDetails]
			(
			@RunID INTEGER
			)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'CourseDetails';
DECLARE @ViewName VARCHAR(100) = 'SRC.vCourseDetails';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--CourseDetails
col1 as QL_CourseId,
col2 as QL_DepartmentId,
col3 as QL_CourseType
*/

--LSB_ExternalID
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_CourseId - INTEGER, Not Null
	exec CFG.ValidRule08_IsUnique
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CourseId'
				,@Mandatory = 'Y';

--QL_DepartmentId - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DepartmentId'
				,@MaxLength = 50
				,@Mandatory = 'Y';

	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_DepartmentId'
				,@FieldNameRelated = 'QL_DepartmentId'
				,@ViewNameRelated = 'SRC.vDepartment'
				,@Mandatory = 'Y';

--QL_CourseType - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CourseType'
				,@MaxLength = 50
				,@Mandatory = 'Y';

end;
GO