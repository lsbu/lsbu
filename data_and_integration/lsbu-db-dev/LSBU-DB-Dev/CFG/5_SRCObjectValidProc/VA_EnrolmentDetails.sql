CREATE OR ALTER PROCEDURE [CFG].[VA_EnrolmentDetails] (
  @RunID INTEGER
)
AS
BEGIN

DECLARE @SrcSystem  VARCHAR(100) = 'QL';
DECLARE @ObjectName VARCHAR(100) = 'EnrolmentDetails';
DECLARE @ViewName VARCHAR(100) = 'STG.tEnrolmentDetails';

DELETE FROM [RPT].[SRCValidations]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName
;

DELETE FROM [RPT].[SRCValidationsItems]
WHERE SrcSystem = @SrcSystem
AND ObjectName = @ObjectName;

END;

BEGIN

/*--EnrolmentDetails
col1 as QL_StudentID,
col2 as QL_UCASPersonalID,
col3 as QL_AcadProgNameId,
col4 as QL_Term,
col5 as QL_AOSPeriod,
col6 as QL_AcademicPeriod,
col7 as QL_ProgramStartDate,
col8 as QL_ProgramEndDate,
col9 as QL_ModeOfStudy,
col10 as QL_EnrolmentStageIndicator,
col11 as QL_EnrollmentStatus,
col12 as QL_EnrolStatusDateAchieved,
col13 as QL_AttedenceType,
col14 as QL_SessionStartDate,
col15 as QL_SessionEndDate,
col16 as QL_CampusInformation,
col17 as QL_CreatedDate,
col18 as QL_LastModifiedDate,
col19 as QL_StudentGraduated
*/

--LSB_ExternalID
	exec CFG.ValidRule08_IsUnique 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'LSB_ExternalID'
				,@Mandatory = 'Y'
				,@CaseSensitive = 'Y'

--QL_StudentId - INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentId'
				,@FieldNameRelated = 'QL_StudentId'
				,@ViewNameRelated = '[STG].[tStudent]'
				,@Mandatory = 'Y';

--Term_LSB_ExternalID
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'Term_LSB_ExternalID'
				,@FieldNameRelated = 'LSB_TRM_ExternalID__c'
				,@ViewNameRelated = '[SFC].[Get_ID_Term]'
				,@Mandatory = 'N';

	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentID'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'Y';

--QL_UCASPersonalID - INTEGER
	exec CFG.ValidRule03_number 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_UCASPersonalID'
				,@Precision = 50
				,@Scale = 0
				,@Mandatory = 'N';

--QL_AcadProgNameId - INTEGER, Not Null
	exec CFG.ValidRule01_lookup
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcadProgNameId'
				,@FieldNameRelated = 'QL_AcadProgNameId'
				,@ViewNameRelated = '[STG].[tAcademicProgram]'
				,@Mandatory = 'Y';

--QL_Term - Picklist(SEPT,MAR,APR,JAN, JUN), Not Null
	exec CFG.ValidRule51_QL_Term
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_Term'
				,@ListValues = 'SEPT,MAR,APR,JAN,JUN'
				,@Mandatory = 'Y'

--QL_AOSPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AOSPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_AcademicPeriod - VARCHAR(50)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AcademicPeriod'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_ProgramStartDate - DATE(DD/MM/YYYY)
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ProgramStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ProgramEndDate - DATE(DD/MM/YYYY)
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ProgramEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_ModeOfStudy - VARCHAR(50)
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModeOfStudy'
				,@ListValues = 'PT,FT,OFT,D'
				,@Mandatory = 'N';

	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_ModeOfStudy'
				,@MaxLength = 50
				,@Mandatory = 'N';

--QL_EnrolmentStageIndicator - Picklist(E)
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrolmentStageIndicator'
				,@ListValues = 'E'
				,@Mandatory = 'N';

--QL_EnrollmentStatus - Picklist ('EADZ','EAPP','EAPPT','EASS','EAWD','EAZZ','EBAC','EBIB','EBIE','ECCNR','ECCP','EDISSO','EDNE','EDUP','EEXC','EEXO','EFAFU','EFE','EFMA','EGRAD','EINS','EINTA','EINTH','ENA','ENBAC','EOCS','EOER','ERCOU','ERORU','ESD','ESLEP','ESUS','ETROC','EVOID','EWD','EZZZ'), NOT NULL
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrollmentStatus'
				,@ListValues = 'EADZ,EAPP,EAPPT,EASS,EAWD,EAZZ,EBAC,EBIB,EBIE,ECCNR,ECCP,EDISSO,EDNE,EDUP,EEXC,EEXO,EFAFU,EFE,EFMA,EGRAD,EINS,EINTA,EINTH,ENA,ENBAC,EOCS,EOER,ERCOU,ERORU,ESD,ESLEP,ESUS,ETROC,EVOID,EWD,EZZZ'
				,@Mandatory = 'Y';

--QL_EnrolStatusDateAchieved - DATETIME(YYYY-MM-DD HH:MM:SS)
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_EnrolStatusDateAchieved'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_AttedenceType - VARCHAR(255)
	exec CFG.ValidRule02_length
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_AttedenceType'
				,@MaxLength = 255
				,@Mandatory = 'N';

--QL_SessionStartDate - DATETIME
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SessionStartDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_SessionEndDate - DATETIME
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_SessionEndDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

--QL_CampusInformation - Picklist(C,D,E,L,M,S,V,X)
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CampusInformation'
				,@ListValues = 'C,D,E,L,M,S,V,X'
				,@Mandatory = 'N';

--QL_CreatedDate - DATETIME
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_CreatedDate'
				,@DateFormat = 'YYYY-MM-DD HH:MM:SS'
				,@Mandatory = 'N';

--QL_LastModifiedDate - DATETIME
	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LastModifiedDate'
				,@DateFormat = 'YYYY-MM-DD HH:MM:SS'
				,@Mandatory = 'N';

--col19 as QL_StudentGraduated
	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentGraduated'
				,@ListValues = 'Yes,No'
				,@Mandatory = 'N';

	exec CFG.ValidRule05_date 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentEnrolledDate'
				,@DateFormat = 'DD/MM/YYYY'
				,@Mandatory = 'N';

	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_LatestEnrolmentRecord'
				,@ListValues = 'Yes,No'
				,@Mandatory = 'N';

	exec CFG.ValidRule04_picklist_custom 
				@RunID = @RunID
				,@SrcSystem = @SrcSystem
				,@ObjectName = @ObjectName
				,@ViewName = @ViewName
				,@FieldName = 'QL_StudentGraduatingFlag'
				,@ListValues = 'Yes,No'
				,@Mandatory = 'N';
end;
