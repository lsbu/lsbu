CREATE OR ALTER PROCEDURE [CFG].[ValidRule01_lookup] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName VARCHAR(100)
	,@ViewNameRelated VARCHAR(100)
	,@FieldNameRelated VARCHAR(100)
	,@Mandatory varchar(1)  -- Y/N
	,@Warning bit = 0
	)
AS
BEGIN
	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_Lookup_',@FieldName);
	DECLARE @ValidationRuleDescription VARCHAR(200) = CONCAT ('Checking if the value of field [',@FieldName,'] exists in related object');
	DECLARE @ValidationResultFlag VARCHAR(50) = 'ERROR'

	IF @Warning = 1
	BEGIN
		SET @ValidationResultFlag = 'WARNING'
	END
	
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

	--PRINT @VALIDATION_RULE_CODE+' '+@VALIDATION_RULE_DECRIPTION	

/* START checking lookup value */
--IF @MANDATORY='N'
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

	set @sql_command =
	N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT 	
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Missing lookup value: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +' A 
	WHERE NOT EXISTS (SELECT ['+@FieldNameRelated+'] FROM '+ @ViewNameRelated +' R 
					   WHERE (R.['+@FieldNameRelated+']) = (A.['+@FieldName+']) 
						) ' +
        ' AND len(isnull(['+@FieldName+'],'''')) > 0 ';

--PRINT @sql_command;
--PRINT @@ValidationResultID;

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription =
				concat('Missing lookup values from field [',@FieldName,'] in object ',@ViewNameRelated,'.',@FieldNameRelated);
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag
        END;
/* END checking lookup value */
		
/* START  MANDATORY check NULL or Empty */
IF @Mandatory='Y'
BEGIN
		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;

		set @sql_command =
		N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in mandatory field: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +' A 
	WHERE len(isnull(['+@FieldName+'],'''')) = 0 ';

--PRINT @sql_command;
--PRINT @@ValidationResultID;

			EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
												@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
			SET @ErrRowCount = @@ROWCOUNT;
			SET @ErrSum = @ErrSum + @ErrRowCount;
			IF @ErrRowCount > 0
			BEGIN
				SET @ValidationRuleDescription = CONCAT('isMandtory=',iif(@Mandatory='Y','Y','N'))
				SET @ValidationResultDescription =
					concat('NULL or blank value in mandatory field [',@FieldName,'] in object [',@ViewName,']','');
				EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag
			END;
END;
/* END MANDATORY */
		
PRINT @ErrSum;

		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK Lookup','');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'

		END;
END