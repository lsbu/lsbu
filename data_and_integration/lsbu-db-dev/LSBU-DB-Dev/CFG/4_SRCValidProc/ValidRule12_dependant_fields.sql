CREATE OR ALTER PROCEDURE [CFG].[ValidRule12_dependant_fields] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName1 VARCHAR(100)
	,@FieldName2 VARCHAR(100)
	)
AS
BEGIN
	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_DependantFields_',@FieldName1,' : ',@FieldName2);
	DECLARE @ValidationRuleDescription VARCHAR(255) = CONCAT('Checking if at least one of the following fields [',@FieldName1,'], [',@FieldName2,'] is not NULL or blank.');
				
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

/* START checking text field*/
BEGIN
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

	set @sql_command =
	N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank values in both '+char(39)+char(39)+'['+@FieldName1+']'+char(39)+char(39)+' and '+char(39)+char(39)+'['+@FieldName2+']'+char(39)+char(39)+': '',['+@FieldName1+'], ['+@FieldName2+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName + ' 
	WHERE len(isnull(dbo.trim(['+@FieldName1+']),'''')) = 0 
	  and len(isnull(dbo.trim(['+@FieldName2+']),'''')) = 0 ';


		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription =
				concat('Values in both fields [',@FieldName1,'], [',@FieldName2,'] are NULL or blank');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
        END;
END
/* END checking text field*/

/* START checking text field*/
BEGIN
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

	set @sql_command =
	N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in '',case when len(isnull(dbo.trim(['+@FieldName1+']),'''')) = 0 then '+char(39)+'['+@FieldName1+']'+char(39)+' else '+char(39)+'['+@FieldName2+']'+char(39)+' end,'': '', case when len(isnull(dbo.trim(['+@FieldName1+']),'''')) = 0 then ['+@FieldName1+'] else ['+@FieldName2+'] end)
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName + ' 
	WHERE (len(isnull(dbo.trim(['+@FieldName1+']),'''')) = 0 
	  and len(isnull(dbo.trim(['+@FieldName2+']),'''')) <> 0)
	  or
	  (len(isnull(dbo.trim(['+@FieldName1+']),'''')) <> 0 
	  and len(isnull(dbo.trim(['+@FieldName2+']),'''')) = 0) ';


		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription =
				concat('One of the following fields [',@FieldName1,'], [',@FieldName2,'] is NULL or blank');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'WARNING'
        END;
END
/* END checking text field*/
END