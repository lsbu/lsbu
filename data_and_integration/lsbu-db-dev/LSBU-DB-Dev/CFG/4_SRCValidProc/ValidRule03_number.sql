CREATE OR ALTER PROCEDURE [CFG].[ValidRule03_number] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName VARCHAR(100)
	,@Precision INT
	,@Scale INT
	,@Mandatory varchar(1)  -- Y/N
	)
AS
/*
validate text field, 
- check if is mandatory
- check @PRECISION
- check SCALE
 */
BEGIN

BEGIN
	DECLARE @ValidationRuleCode VARCHAR(100) = CONCAT(@SrcSystem,'_',@ObjectName,'_Number_',@FieldName);
	DECLARE @ValidationRuleDescription VARCHAR(100) = CONCAT('Checking if the value of field [',@FieldName,'] is Numeric(',@Precision,',',@Scale,')');
			
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT =0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

/* START checking text field, length and mandatory if need */
	begin

		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

		set @sql_command =
		concat(N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value is not Numeric: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
		WHERE CFG.fx_CheckNumber(['+@FieldName+']) = 0 
		    AND dbo.trim(['+@FieldName+']) is not null  
		    AND CFG.fx_isNumeric_custom(['+@FieldName+'],',@Precision,',',@Scale,') = ''N'' ' );

--PRINT @sql_command;
		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription = concat('Non Numeric(',@Precision,',',@Scale,') values in field [',@FieldName,'] in object [',@ViewName,']');

			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
        END;
	END;

/*Error in Excel 25037,010000000002 */
	BEGIN

		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

		SET @sql_command =
		concat(N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value is not Numeric: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
		WHERE CFG.fx_CheckNumber(['+@FieldName+']) = 1 
		    AND dbo.trim(['+@FieldName+']) is not null  
		    AND CFG.fx_isNumeric_custom(['+@FieldName+'],',@Precision,',',@Scale,') = ''N'' ');

--PRINT @sql_command;
		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			SET @ValidationResultDescription =  concat('Non Numeric(',@Precision,',',@Scale,') values in field [',@FieldName,'] in object [',@ViewName,']');

			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
        END;
	END;

/* END checking text field, length and mandatory if need*/
IF @Mandatory='Y'
	begin
		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

		set @sql_command =
		N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT	
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in mandatory field: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
		WHERE CFG.fx_CheckNumber(['+@FieldName+']) = 0
		    AND len(isnull(dbo.trim(['+@FieldName+']),'''')) = 0 ';

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			SET @ValidationResultDescription = concat('NULL or blank value in mandatory field [',@FieldName,'] in object [',@ViewName,']','');
			SET @ValidationRuleDescription = CONCAT('isMandtory=',iif(@Mandatory='Y','Y','N'))
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
	        END;
		END;
		/* END checking text field, length and mandatory if need*/

	
END

		/* END checking text field, length and mandatory if need*/
		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK Number format','');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'
					
		END;
END;
GO