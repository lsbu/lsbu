CREATE OR ALTER PROCEDURE [CFG].[ValidRule52_QL_Student] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	)
AS
BEGIN
 	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_QL_MobileNo');
	DECLARE @ValidationRuleDescription VARCHAR(100) = 'Checking if the value of field QL_MobileNo is correct.'
			
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

/* START checking text field, length and mandatory if need */
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;

	set @sql_command =
	'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT	
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value incorrect in field QL_MobileNo: '', [QL_MobileNo_Status])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
	WHERE QL_MobileNo_Status != ''Pass'' AND QL_MobileNo_Status IS NOT NULL';

--PRINT @sql_command;

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription = 'Value of field [QL_MobileNo_Status] has incorrect value';

			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'WARNING'
        END;
		/* END checking text field, length and mandatory if need*/

		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK values','');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'

		END;

END