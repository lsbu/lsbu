CREATE OR ALTER PROCEDURE [CFG].[ValidRule11_picklist_like] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName VARCHAR(100)
	,@ListValues VARCHAR(8000)
	,@Mandatory varchar(1)  -- Y/N
	)
AS
/*
validate checkBox field, 
- example @LIST_VALUES = 'aaa,bbb,ccc' comma separator
 */
BEGIN
 	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_PickList_',@FieldName);
	DECLARE @ValidationRuleDescription VARCHAR(100) = CONCAT ('Checking if the value of field [',@FieldName,'] exists in picklist(',@ListValues,')')
			
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

/* START checking text field, length and mandatory if need */
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;

	set @sql_command =
	N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value missing in picklist: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
	WHERE dbo.trim(['+@FieldName+']) NOT IN (SELECT value FROM STRING_SPLIT(@IN_ListValues, '',''))
        AND len(isnull(dbo.trim(['+@FieldName+']),'''')) <> 0 ';

--PRINT @sql_command;

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100), @IN_ListValues VARCHAR(8000)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID, @ListValues;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription = concat('Value of field [',@FieldName,'] does not exist in picklist(',@ListValues,')');

			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
        END;
		/* END checking text field, length and mandatory if need*/
		
/* START checking text field, length and mandatory if need */
IF @MANDATORY='Y'
	begin
	--	EXEC GET_VALIDATION_RESULT_ID @VALIDATION_RESULT_ID OUTPUT;	

		set @sql_command =
		N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in mandatory field: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
	WHERE len(isnull(dbo.trim(['+@FieldName+']),'''')) = 0  ';

--print @sql_command;

			EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100), @IN_ListValues VARCHAR(8000)',
												@RunID, @SrcSystem, @ObjectName, @ValidationResultID,@ListValues;
			SET @ErrRowCount = @@ROWCOUNT;
			SET @ErrSum = @ErrSum + @ErrRowCount;
			IF @ErrRowCount > 0
			BEGIN
				SET @ValidationResultDescription = concat('NULL or blank value in mandatory field [',@FieldName,'] in object [',@ViewName,']','');
				SET @ValidationRuleDescription = CONCAT('isMandtory=',iif(@Mandatory='Y','Y','N'))
				EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
			END;
		END;
		/* END checking text field, length and mandatory if need*/


		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK PickList','');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'

		END;

END
GO