CREATE OR ALTER PROCEDURE [CFG].[ValidRule05_date] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName VARCHAR(100)
	,@DateFormat VARCHAR(20)
	,@Mandatory varchar(1)  -- Y/N
	)
AS
BEGIN
	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_Date_',@FieldName);
	DECLARE @ValidationRuleDescription VARCHAR(100) = CONCAT ('Checking if the value of field [',@FieldName,'] is a Date(',@DateFormat,')');
			
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

	/* START checking text field, length and mandatory if need */
BEGIN
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

		set @sql_command =
		concat(N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value has incorrect date format: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM ', @ViewName ,' 
	WHERE dbo.trim([',@FieldName,']) is not null  
	    AND CFG.fx_isDate_custom([',@FieldName,'],''',@DateFormat,''') = ''N'' ');

--PRINT @sql_command;
		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription =
			    concat('Value of field [',@FieldName,'] in object [',@ViewName,'] has incorrect Date(',@DateFormat,') format');

			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
        END;

		END;

/* END checking text field, length and mandatory if need*/
IF @Mandatory='Y'
	begin
		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

		set @sql_command =
		N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in mandatory field: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
	WHERE len(isnull(dbo.trim(['+@FieldName+']),'''')) = 0 ';

--PRINT @sql_command;

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			SET @ValidationResultDescription =
			    concat('NULL or blank value in mandatory field [',@FieldName,'] in object [',@ViewName,']','');
			SET @ValidationRuleDescription = CONCAT('isMandtory=',iif(@Mandatory='Y','Y','N'))	
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
		END;
	END;
		/* END checking text field, length and mandatory if need*/

		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK Date format','');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'
					
		END;
END
GO