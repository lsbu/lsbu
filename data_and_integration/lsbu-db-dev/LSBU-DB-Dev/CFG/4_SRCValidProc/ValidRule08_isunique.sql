CREATE OR ALTER PROCEDURE [CFG].[ValidRule08_IsUnique] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName VARCHAR(100)
	,@Mandatory varchar(1)  -- Y/N
	,@CaseSensitive VARCHAR(1) = 'N'
	,@Warning BIT = 0
	)
AS
BEGIN
/* validate field is Unique, 
 */
 	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_IsUnique_',@FieldName);
	DECLARE @ValidationRuleDescription VARCHAR(100) = CONCAT (
			'Checking if the value of field [',@FieldName,'] is unique',
			IIF(@CaseSensitive='Y',' CASE_SENSITIVE=''Y''',''))
	DECLARE @ValidationResultFlag VARCHAR(50) = 'ERROR'
	
	IF @Warning = 1
	BEGIN
		SET @ValidationResultFlag = 'WARNING'
	END

	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);

	DECLARE @CsPart NVARCHAR(100)  =   IIF(@CaseSensitive='Y',' COLLATE Latin1_General_CS_AS ','');

/* START checking text field, length and mandatory if need */
--IF @MANDATORY='N'
		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

	set @sql_command =
	N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value is not unique: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
	WHERE dbo.trim('+@FieldName+') IN (
          SELECT isnull('+@FieldName+'  '+@CsPart+',''NULL_ERROR'')
            FROM '+ @ViewName +'
        GROUP BY '+@FieldName+' '+@CsPart+'
          HAVING count(*) > 1) ';

--PRINT @sql_command;

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			 SET @ValidationResultDescription = concat('Value of field [',@FieldName,'] in object [',@ViewName,'] is not unique');

			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag
        END;
/* END checking text field, length and mandatory if need*/
		
/* START checking text field, length and mandatory if need */
IF @Mandatory='Y'
	begin
		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

		set @sql_command =
		N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT 	
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in mandatory field: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
		WHERE isnull(dbo.trim('+@FieldName+'),''NULL_ERROR'') = ''NULL_ERROR'' ';

--PRINT @sql_command

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			SET @ValidationResultDescription = concat('NULL or blank value in mandatory field [',@FieldName,'] in object [',@ViewName,']','');
			SET @ValidationRuleDescription = CONCAT('isMandtory=',iif(@Mandatory='Y','Y','N'))
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
			END;
		END;
		/* END checking text field, length and mandatory if need*/

		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK is Unique','');
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'
					
		END;

END