CREATE OR ALTER  PROCEDURE CFG.[SaveValidationResult] (
	@RunID INTEGER
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ValidationResultID VARCHAR(100)
	,@ValidationRuleCode VARCHAR(100)
	,@ValidationRuleDescription VARCHAR(4000)
	,@ValidationResultDescription VARCHAR(4000)
	,@ValidationResultFlag VARCHAR(10)
	)
AS
BEGIN
INSERT INTO [RPT].[SRCValidations](
	[RunID],
	[SrcSystem],
	[ObjectName],
	[ValidationResultID],
	[ValidationRuleCode],
	[ValidationRuleDescription],
	[ValidationResultDescription],
	[ValidationResultFlag]
	) VALUES (@RunID 
			,@SrcSystem
			,@ObjectName
			,@ValidationResultID 
			,@ValidationRuleCode 
			,@ValidationRuleDescription 
			,@ValidationResultDescription 
			,@ValidationResultFlag 
	);
END
GO