CREATE OR ALTER PROCEDURE [CFG].[ValidRule07_phone] (
	@RunID integer
	,@SrcSystem VARCHAR(100)
	,@ObjectName VARCHAR(100)
	,@ViewName VARCHAR(100)
	,@FieldName VARCHAR(100)
	,@MaxLength INT = 11
	,@Mandatory varchar(1)  -- Y/N
	)
AS
/*--https://help.salesforce.com/articleView?language=en_US&type=1&mode=1&id=000330422
Phone numbers containing 9 digits
-  Data will be saved as is without any formatting (Ex: 123456789)
Phone numbers containing 10 digits
-  Phone numbers beginning with 1: Data will be saved as is without any formatting. (Ex: 1234567890)
-  Phone numbers beginning with any other digit: Data will be saved in a format using parenthesis (Ex: (234) 567-8901)
Note: To use a custom formatted phone number, enter a “+” before the number (Ex: +49 1234 56 78-0)
Phone numbers containing 11 digits
-   Phone numbers beginning with 1: Data will be saved as a 10 digit number using parenthesis, removing the first digit (1).
      Ex: A phone number entered as 12345678901 will be saved as (234) 567-8901
-    Phone numbers beginning with any other digit: Data will be stored as is without any formatting:
        Ex: A phone number entered as 23456789012 will be saved as 23456789012.
Note: To use a custom formatted phone number, enter a “+” before the digits in the number (Ex: +49 1234 56 78-0)
*/

BEGIN
	DECLARE @ValidationRuleCode VARCHAR(100) = concat(@SrcSystem,'_',@ObjectName,'_Phone_',@FieldName);
	DECLARE @ValidationRuleDescription VARCHAR(100) = CONCAT('Checking if the value of field [',@FieldName,'] has correct phone number format');
				
	DECLARE @ValidationResultID VARCHAR(20);
	DECLARE @ErrRowCount BIGINT, @ErrSum BIGINT = 0;
	declare @sql_command Nvarchar(4000);
	DECLARE @ValidationResultDescription VARCHAR(4000);
	DECLARE @UpdQuery Nvarchar(4000);



begin
	/* START checking MarketISO is not blank */
	EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;

	set @sql_command =
	N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''Value has incorrect phone number format: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName + ' 
	WHERE len(['+@FieldName+']) > @IN_MaxLength
	    AND CFG.fx_CheckPhone(['+@FieldName+']) = ''N'' ';

--PRINT @sql_command
			EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100), @IN_MaxLength INTEGER',
												@RunID, @SrcSystem, @ObjectName, @ValidationResultID, @MaxLength;
			SET @ErrRowCount = @@ROWCOUNT;
			SET @ErrSum = @ErrSum + @ErrRowCount;
			IF @ErrRowCount > 0
			BEGIN
				 SET @ValidationResultDescription =
				 concat('Value of field [',@FieldName,'] has incorrect phone number format');

				EXEC CFG.SaveValidationResult 
					 @RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'WARNING'

				set @UpdQuery =
				'UPDATE t1
				SET '+@FieldName+' = NULL
				FROM '+ @ViewName + ' as t1
				WHERE len(['+@FieldName+']) > @IN_MaxLength
				AND CFG.fx_CheckPhone(['+@FieldName+']) = ''N'' ';

				EXECUTE sp_executesql @UpdQuery, N' @IN_MaxLength INTEGER', @MaxLength;

			END;

/* END checking MarketISO is not blank */
END;
IF @Mandatory='Y'
	begin
		EXEC CFG.GetValidResultID @ValidationResultID OUTPUT;	

	set @sql_command =
		N'INSERT INTO [RPT].[SRCValidationsItems] (
		RunID
		,SrcSystem
		,ObjectName
		,ValidationResultID
		,FullFileName
		,ValidationDetailsDescription
		,FileRowNumber
		,LSB_ExternalID )
	SELECT 	
		 @IN_RunID
		,@IN_SrcSystem
		,@IN_ObjectName
		,@IN_ValidationResultID
		,FullFileName
		,concat(''NULL or blank value in mandatory field: '',['+@FieldName+'])
		,FileRowNumber
		,LSB_ExternalID
	FROM '+ @ViewName +'
		WHERE isnull(dbo.trim('+@FieldName+'),''NULL_ERROR'') = ''NULL_ERROR'' ';

--PRINT @sql_command

		EXECUTE sp_executesql @sql_command, N' @IN_RunID integer, @IN_SrcSystem VARCHAR(100), @IN_ObjectName VARCHAR(100), @IN_ValidationResultID VARCHAR(100)',
											@RunID, @SrcSystem, @ObjectName, @ValidationResultID;
		SET @ErrRowCount = @@ROWCOUNT;
		SET @ErrSum = @ErrSum + @ErrRowCount;
		IF @ErrRowCount > 0
		BEGIN
			SET @ValidationResultDescription = concat('NULL or blank value in mandatory field [',@FieldName,'] in object [',@ViewName,']','');
			SET @ValidationRuleDescription = CONCAT('isMandtory=',iif(@Mandatory='Y','Y','N'))
			EXEC CFG.SaveValidationResult 
					@RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'ERROR'
			END;
		END;
		/* END checking text field, length and mandatory if need*/
	
		IF @ErrSum = 0
		BEGIN
			SET @ValidationResultDescription =
				concat('OK Phone format','');
			EXEC CFG.SaveValidationResult 
					 @RunID 
					,@SrcSystem
					,@ObjectName
					,@ValidationResultID 
					,@ValidationRuleCode 
					,@ValidationRuleDescription 
					,@ValidationResultDescription 
					,@ValidationResultFlag = 'OK'

		END;
END