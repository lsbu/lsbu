CREATE OR ALTER PROCEDURE CFG.[GetValidResultID] @RESULT VARCHAR(20) OUTPUT
AS
BEGIN
	DECLARE @I INT;

	-- Declare the return variable here
	SET @I = NEXT VALUE
	FOR [RPT].[SrcValidResultID];
	-- Add the T-SQL statements to compute the return value here
	SET @RESULT = CONCAT (
			'Result-'
			,@I
			);

	-- Return the result of the function
	RETURN;
END
GO