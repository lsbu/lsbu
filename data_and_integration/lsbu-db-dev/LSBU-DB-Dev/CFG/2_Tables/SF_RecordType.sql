﻿CREATE TABLE [CFG].[SF_RecordType] (
    [DeveloperName]     VARCHAR (80)  NOT NULL,
    [NamespacePrefix]   VARCHAR (15)  NULL,
    [BusinessProcessId] VARCHAR (18)  NULL,
    [SobjectType]       VARCHAR (40)  NOT NULL,
    [Description]       VARCHAR (255) NULL,
    [Id]                VARCHAR (18)  NOT NULL,
    [CreatedDate]       DATETIME      NOT NULL,
    [CreatedById]       VARCHAR (18)  NOT NULL,
    [LastModifiedDate]  DATETIME      NOT NULL,
    [LastModifiedById]  VARCHAR (18)  NOT NULL,
    [SystemModstamp]    DATETIME      NOT NULL,
    [Name]              VARCHAR (80)  NOT NULL,
    [IsActive]          BIT           NOT NULL
);