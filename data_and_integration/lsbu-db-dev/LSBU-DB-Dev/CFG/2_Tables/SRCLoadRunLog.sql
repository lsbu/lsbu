IF OBJECT_ID('[CFG].[SRCLoadRunLog]','U') IS NOT NULL
	DROP TABLE [CFG].[SRCLoadRunLog];
GO

CREATE TABLE [CFG].[SRCLoadRunLog](
    [SRCLoadRunID] [int] PRIMARY KEY,
	[EtlRunId] [int] NULL,
	[SRCLoadStartDateTime] [datetime] NULL,
	[SRCLoadStatus] [varchar](50) NULL, --STARTED,PROGRESS,SUCCESS,ERRORED
	[SRCLoadEndDateTime] [datetime] NULL,
	[SRCLoadDescription] [varchar](250) NULL,
) ON [PRIMARY]
GO
