IF OBJECT_ID('[CFG].[QL_ApplicationStatusContactRole]','U') IS NOT NULL
	DROP TABLE [CFG].[QL_ApplicationStatusContactRole];
GO

CREATE TABLE [CFG].[QL_ApplicationStatusContactRole](
	[ApplicationStatus] [nvarchar](50) NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[ContactRole] [nvarchar](50) NOT NULL,
	[TransactionStatus] [nvarchar](50) NOT NULL,
	[Rank] [nvarchar](50) NOT NULL
)
GO

INSERT INTO  [CFG].[QL_ApplicationStatusContactRole] values

('AUF','Applicant','Applicant','Continue','1'),
('ACF','Applicant','Applicant','Continue','2'),
('ACI','Applicant','Applicant','Continue','3'),
('AUI','Applicant','Applicant','Continue','4'),
('AUO','Applicant','Applicant','Continue','5'),
('ACO','Applicant','Applicant','Continue','6'),
('ADEF','Applicant','Applicant','Continue','32'),
('AINT','Applicant','Applicant','Continue','7'),
('AINTV','Applicant','Applicant','Continue','8'),
('AAPP','Applicant','Applicant','Start','9'),
('AACPT','Applicant','Applicant','Continue','10'),
('AALT','Applicant','Applicant','Continue','11'),
('AARC','Applicant','Applicant','Continue','12'),
('ACD','Applicant','Applicant','Stop','13'),
('ADBD','Applicant','Applicant','Stop','14'),
('ADCC','Applicant','Applicant','Continue','15'),
('ADEC','Applicant','Applicant','Stop','16'),
('ADEPCO','Applicant','Applicant','Continue','18'),
('ADEPP','Applicant','Applicant','Continue','19'),
('ADEPUF','Applicant','Applicant','Continue','20'),
('ADEPUO','Applicant','Applicant','Continue','21'),
('ADUP','Applicant','Applicant','Continue','22'),
('ARAI','Applicant','Applicant','Stop','23'),
('ARBD','Applicant','Applicant','Stop','24'),
('ARBI','Applicant','Applicant','Stop','25'),
('AREJ','Applicant','Applicant','Stop','26'),
('AREL','Applicant','Applicant','Continue','27'),
('AUD','Applicant','Applicant','Stop','28'),
('AUKP','Applicant','Applicant','Continue','29'),
('AWD','Applicant','Applicant','Stop','30'),
('AWDX','Applicant','Applicant','Stop','31')
GO 