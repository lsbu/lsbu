﻿CREATE TABLE [CFG].[SF_DefaultUser] (
    [ProfileId] VARCHAR (18)                                     NOT NULL,
    [Username]  VARCHAR (80)                                     NOT NULL,
    [Email]     VARCHAR (128) MASKED WITH (FUNCTION = 'email()') NOT NULL,
    [UserType]  VARCHAR (40)                                     NULL,
    [Id]        VARCHAR (18)                                     NOT NULL,
    [Name]      VARCHAR (121)                                    NOT NULL
);