IF OBJECT_ID('[CFG].[QL_ContactRoleRank]','U') IS NOT NULL
  DROP TABLE [CFG].[QL_ContactRoleRank];
GO

CREATE TABLE [CFG].[QL_ContactRoleRank] (
  [ContactRole] [varchar](50) NOT NULL,
  [Rank] [int] NOT NULL,
  CONSTRAINT PK_QL_ContactRoleRank PRIMARY KEY CLUSTERED (ContactRole)
) ON [PRIMARY]
GO

INSERT INTO [CFG].[QL_ContactRoleRank] (
  [ContactRole],
  [Rank]
) VALUES
('Student', 1),
('Applicant', 2),
('Prospect', 3),
('Graduate', 4),
('Student - Interrupted', 5),
('Student - Withdrawn', 6),
('External to LSBU', 7),
('Staff', 8)
GO
