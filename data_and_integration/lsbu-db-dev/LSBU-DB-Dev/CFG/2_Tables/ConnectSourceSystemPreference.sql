IF OBJECT_ID('[CFG].[ConnectSourceSystemPreference]','U') IS NOT NULL
	DROP TABLE [CFG].[ConnectSourceSystemPreference];
GO
	
CREATE TABLE [CFG].[ConnectSourceSystemPreference](
	[SourceSystem] [nvarchar](50) NULL,
	[Rank] [nvarchar](9) NULL
)
GO

insert into CFG.ConnectSourceSystemPreference
(SourceSystem
,Rank)
values
('QL','1'),
('CMIS','2'),
('Salesforce','4'),
('Connect','5'),
('ASPTCDCSV','3')