IF OBJECT_ID('[CFG].[QL_EnrolmentStatusContactRole]','U') IS NOT NULL
  DROP TABLE [CFG].[QL_EnrolmentStatusContactRole];
GO

CREATE TABLE [CFG].[QL_EnrolmentStatusContactRole] (
  [EnrolmentStatus] [varchar](10) NOT NULL,
  [Role] [varchar](50) NOT NULL,
  [ContactRole] [varchar](50) NOT NULL,
  [TransactionStatus] [varchar](50) NOT NULL,
  [Rank] [int] NOT NULL,
  CONSTRAINT PK_QL_EnrolmentStatusContactRole PRIMARY KEY CLUSTERED (EnrolmentStatus)
) ON [PRIMARY]
GO

INSERT INTO [CFG].[QL_EnrolmentStatusContactRole] (
  [EnrolmentStatus],
  [Role],
  [ContactRole],
  [TransactionStatus],
  [Rank]
) VALUES
('EAPP', 'Applicant', 'Applicant', 'Continue', 3),
('EASS', 'Applicant', 'Applicant', 'Continue', 3),
('EAWD', 'Applicant', 'Applicant', 'Stop', 3),
('EAZZ', 'Applicant', 'Applicant', 'Continue', 3),
('ECCNR', 'Student', 'Student - Interrupted', 'Stop', 5),
('EDNE', 'Student', 'Student', 'Stop', 7),
('EEXC', 'Student', 'Student', 'Stop', 7),
('EFE', 'Student', 'Student', 'Start', 1),
('EINTA', 'Student', 'Student - Interrupted', 'Stop', 5),
('EOER', 'Applicant', 'Applicant', 'Continue', 3),
('ESLEP', 'Student', 'Student - Interrupted', 'Stop', 5),
('ESUS', 'Student', 'Student', 'Stop', 5),
('ETR0C', 'Student', 'Student', 'Stop', 5),
('ETROC', 'Student', 'Student', 'Stop', 5),
('EWD', 'Student', 'Student - Withdrawn', 'Stop', 7),
('EZZZ', 'Student', 'Student', 'Stop', 7)
GO
