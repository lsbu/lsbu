﻿CREATE TABLE [CFG].[SF_PermissionSet] (
    [Label]                VARCHAR (80)                                                      NOT NULL,
    [LicenseId]            VARCHAR (18)                                                      NULL,
    [ProfileId]            VARCHAR (18)                                                      NULL,
    [PermissionSetGroupId] VARCHAR (18)                                                      NULL,
    [Type]                 VARCHAR (255)                                                     NULL,
    [NamespacePrefix]      VARCHAR (15)                                                      NULL,
    [Description]          VARCHAR (255) MASKED WITH (FUNCTION = 'partial(1, "XXXXXXX", 1)') NULL,
    [Id]                   VARCHAR (18)                                                      NOT NULL,
    [Name]                 VARCHAR (80)                                                      NOT NULL
);