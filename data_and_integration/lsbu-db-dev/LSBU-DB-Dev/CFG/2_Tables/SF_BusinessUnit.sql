﻿CREATE TABLE [CFG].[SF_BusinessUnit] (
    [CurrencyIsoCode]           VARCHAR (3)   NULL,
    [RIG_Business_Pillar__c]    VARCHAR (255) NULL,
    [RIG_External_ID__c]        VARCHAR (100) NULL,
    [RIG_SourceSystem_ID__c]    VARCHAR (100) NULL,
    [RIG_SourceSystem__c]       VARCHAR (100) NULL,
    [OwnerId]                   VARCHAR (18)  NOT NULL,
    [IsDeleted]                 BIT           NOT NULL,
    [LastActivityDate]          DATETIME      NULL,
    [LastViewedDate]            DATETIME      NULL,
    [LastReferencedDate]        DATETIME      NULL,
    [RIG_Business_Unit_Code__c] VARCHAR (20)  NULL,
    [RIG_Description__c]        VARCHAR (255) NULL,
    [RIG_Division__c]           VARCHAR (255) NULL,
    [RIG_Status__c]             VARCHAR (255) NULL,
    [Id]                        VARCHAR (18)  NOT NULL,
    [CreatedDate]               DATETIME      NOT NULL,
    [CreatedById]               VARCHAR (18)  NOT NULL,
    [LastModifiedDate]          DATETIME      NOT NULL,
    [LastModifiedById]          VARCHAR (18)  NOT NULL,
    [SystemModstamp]            DATETIME      NOT NULL,
    [Name]                      VARCHAR (80)  NULL
);