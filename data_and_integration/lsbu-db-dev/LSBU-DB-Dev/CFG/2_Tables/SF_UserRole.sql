﻿CREATE TABLE [CFG].[SF_UserRole] (
    [Id]                               VARCHAR (18) NOT NULL,
    [Name]                             VARCHAR (80) NOT NULL,
    [ParentRoleId]                     VARCHAR (18) NULL,
    [RollupDescription]                VARCHAR (80) NULL,
    [OpportunityAccessForAccountOwner] VARCHAR (40) NOT NULL,
    [CaseAccessForAccountOwner]        VARCHAR (40) NULL,
    [ContactAccessForAccountOwner]     VARCHAR (40) NULL,
    [ForecastUserId]                   VARCHAR (18) NULL,
    [MayForecastManagerShare]          BIT          NOT NULL,
    [LastModifiedDate]                 DATETIME     NOT NULL,
    [LastModifiedById]                 VARCHAR (18) NOT NULL,
    [SystemModstamp]                   DATETIME     NOT NULL,
    [DeveloperName]                    VARCHAR (80) NULL,
    [PortalAccountId]                  VARCHAR (18) NULL,
    [PortalType]                       VARCHAR (40) NULL,
    [PortalAccountOwnerId]             VARCHAR (18) NULL
);