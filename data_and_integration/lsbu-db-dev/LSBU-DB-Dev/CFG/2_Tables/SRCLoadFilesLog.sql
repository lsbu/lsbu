IF OBJECT_ID('[CFG].[SRCLoadFilesLog]','U') IS NOT NULL
	DROP TABLE [CFG].[SRCLoadFilesLog];
GO

CREATE TABLE [CFG].[SRCLoadFilesLog](
    [SRCLoadRunID] [int] NULL,
	[EtlRunId] [int] NULL,
	[sSRCFileName] [varchar](150) NULL,
	[sSRCFileSRCSystem] [varchar](10) NULL,
	[sSRCFileObjectName] [varchar](50) NULL,
	[iSRCFileRowNo] [int] NULL,
	[sSRCFileDateTime] [varchar](50) NULL,
	[dSRCFileDateTime] [datetime] NULL,
	[sSRCFileStatus] [varchar](50) NULL,
	[sSRCFileDescription] [varchar](150) NULL,
	[dSRCFileLoadStartDT] [datetime] NULL,
	[dSRCFileLoadEndDT] [datetime] NULL,
) ON [PRIMARY]
GO


