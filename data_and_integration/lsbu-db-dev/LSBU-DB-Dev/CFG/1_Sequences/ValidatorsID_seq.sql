﻿IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CFG].[ValidatorsID_seq]') AND type = 'SO')
CREATE SEQUENCE [CFG].[ValidatorsID_seq]
    AS BIGINT
    INCREMENT BY 1
    MINVALUE 1
    CACHE 10000
GO