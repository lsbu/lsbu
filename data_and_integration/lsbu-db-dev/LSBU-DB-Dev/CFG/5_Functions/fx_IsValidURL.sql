CREATE OR ALTER FUNCTION CFG.fx_IsValidURL (@Url VARCHAR(200))
RETURNS INT
AS
BEGIN
    DECLARE @IsValidUrl bit = 0

	IF (@Url LIKE 'http://%'
	    AND CHARINDEX('http://', @Url) >= 1
        )
    BEGIN
        SET @IsValidUrl = 1
    END

		IF (@Url LIKE 'https://%'
	    AND CHARINDEX('https://', @Url) >= 1
        )
    BEGIN
        SET @IsValidUrl = 1
    END


	--Not valid return 0
    RETURN @IsValidUrl

END
