CREATE OR ALTER FUNCTION CFG.fx_CheckURL(@URL VARCHAR(300))
RETURNS BIT
AS
BEGIN
  DECLARE @Object INT
  DECLARE @Return TINYINT
  DECLARE @Valid BIT SET @Valid = 0 --default to false
  
  --create the XMLHTTP object
  EXEC @Return = sp_oacreate 'MSXML2.ServerXMLHTTP.3.0', @Object OUTPUT
  IF @Return = 0
  BEGIN
    DECLARE @Method VARCHAR(350)
    --define setTimeouts method
    --Resolve, Connect, Send, Receive
    SET @Method = 'setTimeouts(45000, 45000, 45000, 45000)'
    --set the timeouts
    EXEC @Return = sp_oamethod @Object, @Method
    IF @Return = 0
    BEGIN
      --define open method
      SET @Method = 'open("GET", "' + @URL + '", false)'
  
      --Open the connection
      EXEC @Return = sp_oamethod @Object, @Method
    END
  
    IF @Return = 0
    BEGIN
      --SEND the request
      EXEC @Return = sp_oamethod @Object, 'send()'
    END
  
    IF @Return = 0
    BEGIN
      DECLARE @Output INT
      EXEC @Return = sp_oamethod @Object, 'status', @Output OUTPUT
  
      IF @Output = 200
      BEGIN
        SET @Valid = 1
      END
    END
  END
  
  --destroy the object
  EXEC sp_oadestroy @Object
  RETURN (@Valid)
END