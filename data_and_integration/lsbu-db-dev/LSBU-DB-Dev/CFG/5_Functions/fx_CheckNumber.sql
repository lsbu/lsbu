CREATE OR ALTER FUNCTION CFG.fx_CheckNumber(@number VARCHAR(255)) 
RETURNS bit
AS
BEGIN
    DECLARE @IsValidNumber bit = 0

    IF (@number not like '%[0]{11}\d$%' 
        AND LEN(@number) > 12
        )
    BEGIN
        SET @IsValidNumber = 1
    END
    RETURN @IsValidNumber
END