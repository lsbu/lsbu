CREATE OR ALTER FUNCTION [CFG].[fx_PercentageTwoStringMatching]
(
    @string1 NVARCHAR(450)
   ,@string2 NVARCHAR(450)
)
RETURNS INT
AS
BEGIN

	DECLARE @res int 

	IF (@string1+@string2) IS NULL
	BEGIN 
		SET @res = NULL
	END
	ELSE 
	BEGIN
		DECLARE @levenShteinNumber INT

		DECLARE @string1Length INT = LEN(@string1)
		, @string2Length INT = LEN(@string2)
		DECLARE @maxLengthNumber INT = CASE WHEN @string1Length > @string2Length THEN @string1Length ELSE @string2Length END

		SELECT @levenShteinNumber = [CFG].[fx_Levenshtein](@string1, @string2)

		DECLARE @percentageOfBadCharacters INT = @levenShteinNumber * 100 / @maxLengthNumber

		SET @res =  100 - @percentageOfBadCharacters
		--DECLARE @percentageOfGoodCharacters INT = 100 - @percentageOfBadCharacters

		-- Return the result of the function
		--RETURN @percentageOfGoodCharacters
	END

	return @res
END