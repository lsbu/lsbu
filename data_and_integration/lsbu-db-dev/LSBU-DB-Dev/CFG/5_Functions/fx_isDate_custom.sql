CREATE OR ALTER function CFG.fx_isDate_custom(@str_value varchar(1000), @DATE_FORMAT VARCHAR(20))
returns varchar(1)
as
begin
declare @result varchar(1) = 'N';

	IF @DATE_FORMAT = 'YYYY-MM-DD' 
	BEGIN
		if try_CONVERT(date, @str_value, 23) is null  -- STYLE 23 MEANS 'YYYY-MM-DD'
			return 'N';
		if @str_value NOT like '[0-9][0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9]'
			return 'N';
		else
		RETURN 'Y'
	END;

	IF @DATE_FORMAT = 'YYYY-MM-DD HH:MM:SS'
	BEGIN
		if try_CONVERT(DATETIME, @str_value) is null  -- STYLE 'YYYY-MM-DD HH:MM:SS'
			return 'N';
		if @str_value NOT like '[0-9][0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9][ ][0-9][0-9][:][0-9][0-9][:][0-9][0-9]'
			return 'N';
		else
		RETURN 'Y'
	END;

	IF @DATE_FORMAT = 'MMM yy'             -- STYLE 'MMM yy'
	BEGIN
		if @str_value NOT LIKE '[a-zA-Z][a-zA-Z][a-zA-Z][ ][0-9][0-9]'
			return 'N';
		if SUBSTRING(@str_value,1,3) NOT IN ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')
			return 'N';
	    else
		RETURN 'Y'
	END;

	IF @DATE_FORMAT = 'D/M/YYYY' 
	BEGIN
		if TRY_CONVERT(DATETIME, @str_value, 103) is null  -- STYLE 'D/M/YYYY'
			return 'N';
		if @str_value NOT LIKE '%[0-9][/]%[0-9][/][0-9][0-9][0-9][0-9]'
			return 'N';
		else
		RETURN 'Y'
	END;

	IF @DATE_FORMAT = 'DD/MM/YY' 
	BEGIN
		if TRY_CONVERT(DATETIME, @str_value, 3) is null  -- STYLE 'DD/MM/YY'
			return 'N';
		if @str_value NOT LIKE '[0-9][0-9][/][0-9][0-9][/][0-9][0-9]'
			return 'N';
		else
		RETURN 'Y'
	END;

	IF @DATE_FORMAT = 'DD/MM/YYYY' 
	BEGIN
		if TRY_CONVERT(DATETIME, @str_value, 103) is null  -- STYLE 'DD/MM/YYYY'
			return 'N';
		if @str_value NOT LIKE '[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]'
			return 'N';
		else
		RETURN 'Y'
	END;

	IF @DATE_FORMAT = 'MM/DD/YYYY' 
	BEGIN
		if TRY_CONVERT(DATETIME, @str_value, 101) is null  -- STYLE 'MM/DD/YYYY'
			return 'N';
		if @str_value NOT LIKE '[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]'
			return 'N';
		else
		RETURN 'Y'
	END;
return 'N'; -- return N by default because required mask may not be implemanted

end
GO


