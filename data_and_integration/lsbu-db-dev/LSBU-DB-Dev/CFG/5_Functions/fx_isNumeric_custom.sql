CREATE OR ALTER FUNCTION CFG.fx_isNumeric_custom(
					@str_value varchar(1000)
					,@precision integer 
					,@scale int
					)
returns varchar(1)
AS
BEGIN
declare @result varchar(1) = 'Y';
declare @str_value2 varchar(1000) = replace(@str_value,',','.');

	--if try_parse(@str_value2 as numeric(38,10)) is null
	IF TRY_CAST(@str_value2 AS DECIMAL(38,10)) IS NULL
		RETURN 'N'

DECLARE @LENGTH INT = LEN(dbo.TRIM(@str_value2));
DECLARE @sepPosition INT = CHARINDEX('.',@str_value2);
DECLARE @ScaleInNumber INT = LEN(SUBSTRING(@str_value2,@sepPosition+1,20));
DECLARE @PrecisionInNumber INT = LEN(LEFT(@str_value2,CASE WHEN @sepPosition > 0 THEN @sepPosition-1 ELSE 0 END));

	IF @scale > 0 AND @ScaleInNumber > @scale
		RETURN CONCAT('aN ',@LENGTH,' ',@sepPosition,' ',@ScaleInNumber,' ',@PrecisionInNumber);
	IF @scale > 0 AND @PrecisionInNumber > @precision
		RETURN CONCAT('bN ',@LENGTH,' ',@sepPosition,' ',@ScaleInNumber,' ',@PrecisionInNumber);

	IF @scale = 0 AND @ScaleInNumber > @scale
		RETURN CONCAT('cN ',@LENGTH,' ',@sepPosition,' ',@ScaleInNumber,' ',@PrecisionInNumber);
	IF @scale = 0 AND @PrecisionInNumber >= @precision
		RETURN CONCAT('dN ',@LENGTH,' ',@sepPosition,' ',@ScaleInNumber,' ',@PrecisionInNumber);

--RETURN CONCAT('Y ',@LENGTH,' ',@sepPosition,' ',@ScaleInNumber,' ',@PrecisionInNumber)
RETURN 'Y';
END
GO