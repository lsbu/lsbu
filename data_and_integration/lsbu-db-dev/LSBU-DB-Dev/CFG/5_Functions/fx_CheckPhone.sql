CREATE OR ALTER FUNCTION CFG.fx_CheckPhone(
					@str_value varchar(20)
					)
returns varchar(1)
AS

/*--https://help.salesforce.com/articleView?language=en_US&type=1&mode=1&id=000330422
Phone numbers containing 9 digits
-  Data will be saved as is without any formatting 
(Ex: 123456789)
Phone numbers containing 10 digits
-  Phone numbers beginning with 1: Data will be saved as is without any formatting. 
(Ex: 1234567890)
-  Phone numbers beginning with any other digit: Data will be saved in a format using parenthesis 
(Ex: (234) 567-8901)
Note: To use a custom formatted phone number, enter a “+” before the number 
(Ex: +49 1234 56 78-0)
Phone numbers containing 11 digits
-   Phone numbers beginning with 1: Data will be saved as a 10 digit number using parenthesis, removing the first digit (1).
      Ex: A phone number entered as 12345678901 will be saved as (234) 567-8901
-    Phone numbers beginning with any other digit: Data will be stored as is without any formatting:
        Ex: A phone number entered as 23456789012 will be saved as 23456789012.
Note: To use a custom formatted phone number, enter a “+” before the digits in the number 
(Ex: +49 1234 56 78-0)
*/
BEGIN
--DECLARE @str_value2 varchar(20) = replace(replace(replace(replace(replace(@str_value,'+',''),' ',''),'-',''),'(',''),')','');
--DECLARE @str_value2 varchar(20) = replace(replace(replace(replace(@str_value,'+',''),' ',''),'(',''),')','');
DECLARE @return varchar(20) = 'N';

	IF ISNUMERIC(@str_value) = 1
	BEGIN
		SET @return = 'Y';
	END
	IF ISNUMERIC(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@str_value,'+',''),' ',''),'-',''),')',''),'(','') ) = 1
	BEGIN
		SET @return = 'Y';
	END

RETURN @return;
END
GO
