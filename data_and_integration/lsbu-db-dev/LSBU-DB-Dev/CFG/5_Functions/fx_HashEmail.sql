CREATE OR ALTER FUNCTION [CFG].[fx_HashEmail](@Email VARCHAR(255), @SalesforceID VARCHAR(100)) 
RETURNS VARCHAR(255)
AS
BEGIN
    DECLARE @InvalidEmail VARCHAR(255);
	DECLARE @EmailHost VARCHAR(99);
	DECLARE @EmailName VARCHAR(99);

IF @Email IS NOT NULL AND @Email LIKE '%@%'
BEGIN
	SELECT @EmailHost = RIGHT(@email,LEN(@email)- CHARINDEX('@',@email)+1);
	SELECT @EmailName = SUBSTRING(@email,1,CHARINDEX('@',@email)-1);
	
	SET @InvalidEmail = CONCAT('E',SUBSTRING(@SalesforceId, 0, IIF(CHARINDEX('@',@SalesforceId,0) = 0, len(@SalesforceId), CHARINDEX('@',@SalesforceId,0))), @EmailHost, '.invalid');
	--SET @InvalidEmail = CONCAT('E',@SalesforceId, @EmailHost, '.invalid');
	
END;
ELSE IF @Email IS NULL 
BEGIN
		SET @InvalidEmail = NULL;
END;	

    RETURN SUBSTRING(@InvalidEmail,1,255)
END