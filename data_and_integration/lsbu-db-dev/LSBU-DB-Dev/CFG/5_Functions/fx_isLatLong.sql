CREATE OR ALTER FUNCTION CFG.fx_isLatLong(
					@str_value varchar(100)
					)
returns varchar(1)
AS

/*(First should be From -90 to 90 after semicolon should be from -180 to 180)
Example data (31.87469;-116.59497, 31.8771483;-116.582412)
*/
BEGIN

DECLARE @Lat varchar(50) = substring(@str_value, 0, charindex(';', @str_value));
DECLARE @Long varchar(50) = substring(@str_value, charindex(';', @str_value) + 1, len(@str_value) - charindex(';', @str_value) + 1);

	IF (TRY_PARSE(@Lat AS NUMERIC(20, 10)) BETWEEN -90 AND 90) AND (TRY_PARSE(@Long AS NUMERIC(20, 10)) BETWEEN -180 AND 180)
	   RETURN 'Y'

RETURN 'N';
END
GO