--DROP PROCEDURE [CFG].[SRCCheckFilesList];
CREATE OR ALTER PROCEDURE [CFG].[SRCCheckFilesList]
	@ObjectList VARCHAR(8000)
	,@FileNo INT
	--,@INTFileStatus INT OUTPUT	
AS

BEGIN

	DECLARE @RowCount BIGINT
	DECLARE @sql_command Nvarchar(4000);
	DECLARE @SRCFileStatus VARCHAR(50)

	set @sql_command =	N'SELECT t.sSRCFileName,count(*) as COUNT
		FROM [CFG].SRCLoadFilesTmp t
		WHERE t.sSRCFileObjectName IN (SELECT value FROM STRING_SPLIT(@IN_ObjectList, '',''))
			AND t.dSRCFileDateTime = (SELECT min(dSRCFileDateTime) FROM [CFG].SRCLoadFilesTmp)
			GROUP BY t.sSRCFileName
			HAVING count(*) = 1';

PRINT @sql_command;

	EXECUTE sp_executesql @sql_command,  N'@IN_ObjectList VARCHAR(4000)',
							@ObjectList;

		SET @RowCount = @@ROWCOUNT;
		SET @SRCFileStatus = 'ERROR';
		--SET @INTFileStatus = 0;

		IF @RowCount = @FileNo 
		BEGIN 
			SET @SRCFileStatus = 'OK';
			--SET @INTFileStatus = 1;
		END;

PRINT @RowCount;
PRINT @FileNo;

END;

PRINT @SRCFileStatus;

--RETURN @SRCFileStatus;
--RETURN

GO


