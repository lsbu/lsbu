CREATE OR ALTER PROCEDURE [MNT].[DropFunctions]
	@schema nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;

	declare @sname nvarchar(32), @fname nvarchar(255), @query nvarchar(max)
	
	declare funtions_cur cursor for
		select 
			s.name,
			f.name 
		from sys.all_objects f
		join sys.schemas s 
			on f.schema_id = s.schema_id 
		where s.name = @schema
		and f.type in ('AF','FN','FS','FT','IF','TF')

	open funtions_cur
	fetch next from funtions_cur 
	into @sname, @fname

	while @@FETCH_STATUS = 0
	begin
		set @query = 'drop function '+@sname+'.'+@fname
		begin try
			exec sp_executesql @query
		end try
		begin catch
			print 'could not drop function '+@sname+'.'+@fname+'!'
		end catch
		
		fetch next from funtions_cur 
		into @sname, @fname
	end
	
	close funtions_cur
	deallocate funtions_cur
END
