CREATE OR ALTER PROCEDURE [MNT].[DropTables]
	@schema nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;

	declare @sname nvarchar(32), @tname nvarchar(255), @query nvarchar(max)
	
	declare tables_cur cursor for
		select 
			s.name, 
			t.name 
		from sys.tables t 
		join sys.schemas s 
			on t.schema_id = s.schema_id 
		where s.name = @schema
		order by 
		(select count(1)
		from sys.sql_expression_dependencies A, sys.objects B
		where referenced_id = t.object_id 
		and A.referencing_id = B.object_id)

	open tables_cur
	fetch next from tables_cur 
	into @sname, @tname

	while @@FETCH_STATUS = 0
	begin
		set @query = 'drop table '+@sname+'.'+@tname
		begin try
			exec sp_executesql @query
		end try
		begin catch
			print 'could not drop table '+@sname+'.'+@tname+'!'
		end catch

		fetch next from tables_cur 
		into @sname, @tname
	end
	
	close tables_cur
	deallocate tables_cur
END
