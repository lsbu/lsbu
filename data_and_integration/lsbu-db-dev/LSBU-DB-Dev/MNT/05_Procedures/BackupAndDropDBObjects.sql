CREATE OR ALTER PROCEDURE [MNT].[BackupAndDropDBObjects]
  @schema nvarchar(32),
  @appendedText nvarchar(128) = '',
  @backup bit = 1,
  @dropAfterBackup bit = 0
AS
BEGIN
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  
  DECLARE @schemaList TABLE (Id INT IDENTITY(1,1), SchemaName NVARCHAR(10))
  IF @schema IS NULL
  BEGIN
    INSERT INTO @schemaList (SchemaName) VALUES ('SFC');
    INSERT INTO @schemaList (SchemaName) VALUES ('RPT');
    INSERT INTO @schemaList (SchemaName) VALUES ('DST');
    INSERT INTO @schemaList (SchemaName) VALUES ('INT');
    INSERT INTO @schemaList (SchemaName) VALUES ('SRC');
    INSERT INTO @schemaList (SchemaName) VALUES ('CFG');
  END ELSE BEGIN
    INSERT INTO @schemaList (SchemaName) VALUES (@schema)
  END
  
  DECLARE @schemaName NVARCHAR(10)

  DECLARE db_cursor CURSOR FOR
    SELECT SchemaName
    FROM @schemaList
    ORDER BY Id
  OPEN db_cursor
  FETCH NEXT FROM db_cursor INTO @schemaName

  WHILE @@FETCH_STATUS = 0   
  BEGIN
    DECLARE @message NVARCHAR(255) = N'Schema: ' + @schemaName;
    RAISERROR (@message, 0, 1) WITH NOWAIT;

    IF @backup = 1
    BEGIN
      EXEC MNT.BackupTables @schemaName, @appendedText
    END
  
    IF @dropAfterBackup = 1
    BEGIN
      EXEC MNT.DropProcedures @schemaName
      EXEC MNT.DropViews @schemaName
      EXEC MNT.DropFunctions @schemaName
      EXEC MNT.DropTables @schemaName
    END

    FETCH NEXT FROM db_cursor INTO @schemaName
  END   

  CLOSE db_cursor   
  DEALLOCATE db_cursor

END
GO
