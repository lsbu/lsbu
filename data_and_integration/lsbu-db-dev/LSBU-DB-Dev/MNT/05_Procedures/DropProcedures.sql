CREATE OR ALTER PROCEDURE [MNT].[DropProcedures]
	@schema nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;

	declare @sname nvarchar(32), @pname nvarchar(255), @query nvarchar(max)
	
	declare procedures_cur cursor for
		select 
			s.name, 
			p.name 
		from sys.procedures p 
		join sys.schemas s 
			on p.schema_id = s.schema_id 
		where s.name = @schema
		
	open procedures_cur
	fetch next from procedures_cur 
	into @sname, @pname

	while @@FETCH_STATUS = 0
	begin
		set @query = 'drop procedure '+@sname+'.'+@pname
		begin try
			exec sp_executesql @query
		end try
		begin catch
			print 'could not drop procedure '+@sname+'.'+@pname+'!'
		end catch
		
		fetch next from procedures_cur 
		into @sname, @pname
	end
	
	close procedures_cur
	deallocate procedures_cur
END
GO