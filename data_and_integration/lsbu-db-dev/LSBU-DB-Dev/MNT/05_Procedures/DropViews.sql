CREATE OR ALTER PROCEDURE [MNT].[DropViews]
	@schema nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;

	declare @sname nvarchar(32), @vname nvarchar(255), @query nvarchar(max)
	
	declare views_cur cursor for
		select 
			s.name, 
			v.name 
		from sys.views v 
		join sys.schemas s 
			on v.schema_id = s.schema_id 
		where s.name = @schema

	open views_cur
	fetch next from views_cur 
	into @sname, @vname

	while @@FETCH_STATUS = 0
	begin
		set @query = 'drop view '+@sname+'.'+@vname
		begin try
			exec sp_executesql @query
		end try
		begin catch
			print 'could not drop view '+@sname+'.'+@vname+'!'
		end catch

		fetch next from views_cur 
		into @sname, @vname
	end
	
	close views_cur
	deallocate views_cur
END
GO