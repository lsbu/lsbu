CREATE OR ALTER PROCEDURE [MNT].[BackupTables]
	@schema nvarchar(32),
	@appendedText nvarchar(128)
AS
BEGIN
	SET NOCOUNT ON;

	declare @sname nvarchar(32), @tname nvarchar(255), @query nvarchar(max)
	
	declare tables_cur cursor for
		select 
			s.name, 
			t.name 
		from sys.tables t 
		join sys.schemas s 
			on t.schema_id = s.schema_id 
		where s.name = @schema
		order by 
		(select count(1)
		from sys.sql_expression_dependencies A, sys.objects B
		where referenced_id = t.object_id 
		and A.referencing_id = B.object_id) desc

	open tables_cur
	fetch next from tables_cur 
	into @sname, @tname

	while @@FETCH_STATUS = 0
	begin
		set @query = 'select * into BCK.' + @sname + '_' + @tname + '_' + replace(convert(nvarchar, getdate(),111),'/','')+replace(convert(nvarchar, getdate(),108),':','')+isnull(@appendedText, '')+' from '+@sname+'.'+@tname
		
		exec sp_executesql @query

		fetch next from tables_cur 
		into @sname, @tname
	end
	
	close tables_cur
	deallocate tables_cur
END
