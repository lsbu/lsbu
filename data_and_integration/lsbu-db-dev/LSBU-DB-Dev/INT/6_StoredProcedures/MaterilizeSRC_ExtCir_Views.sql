CREATE OR ALTER PROCEDURE [INT].[MaterilizeSRC_ExtCir_Views]
AS
DECLARE 
 @cms nvarchar(max) = N''
BEGIN
	----------------------------------------------------------------------------------- 

  SET @cms = 'DROP TABLE IF EXISTS STG.tExtenuatingCircumstances;
				SELECT * INTO STG.tExtenuatingCircumstances FROM SRC.vExtenuatingCircumstances;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
END;