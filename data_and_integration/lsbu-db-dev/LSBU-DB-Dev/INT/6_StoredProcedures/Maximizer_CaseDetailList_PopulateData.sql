CREATE OR ALTER PROCEDURE INT.Maximizer_CaseDetailList_PopulateData
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE INT.SRC_Maximizer_PIVOT_CaseDetailList

	INSERT INTO INT.SRC_Maximizer_PIVOT_CaseDetailList
		([CaseId],
		[Safety_Concern_Response_Student_in_halls])
	SELECT
		[CaseId],
		[Safety_Concern_Response_Student_in_halls]
	FROM SRC.Maximizer_PIVOT_CaseDetailList

END
GO