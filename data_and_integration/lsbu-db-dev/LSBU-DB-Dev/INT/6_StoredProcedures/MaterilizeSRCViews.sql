
CREATE OR ALTER   PROCEDURE [INT].[MaterilizeSRCViews]
AS
DECLARE 
 @cms nvarchar(max) = N'',
 @RC int
BEGIN
    ----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tEducationalInstitution;
				SELECT * INTO STG.tEducationalInstitution FROM SRC.vEducationalInstitution;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tDepartment;
				SELECT * INTO STG.tDepartment FROM SRC.vDepartment;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tAcademicProgram;
				SELECT * INTO STG.tAcademicProgram FROM SRC.vAcademicProgram;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tEnrolmentDetails;
				SELECT * INTO STG.tEnrolmentDetails FROM SRC.vEnrolmentDetails;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tApplicationDetails_Application;
				SELECT * INTO STG.tApplicationDetails_Application FROM SRC.vApplicationDetails_Application;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tStudent_Address;
				SELECT * INTO STG.tStudent_Address FROM SRC.vStudent_Address;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tStudent;
				SELECT * INTO STG.tStudent FROM SRC.vStudent;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tSessionIntakeDetails_Term;
				SELECT * INTO STG.tSessionIntakeDetails_Term FROM SRC.vSessionIntakeDetails_Term;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------

	--LEAP-7204
	--SET @cms = 'DROP TABLE IF EXISTS STG.tAppliedSciencesPTCD_Affiliation;
	--			SELECT * INTO STG.tAppliedSciencesPTCD_Affiliation FROM SRC.vAppliedSciencesPTCD_Affiliation;';
	
	--PRINT @cms
	--EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tSALAdviseeCase;
				SELECT * INTO STG.tSALAdviseeCase FROM SRC.vSALAdviseeCase;';
	
	PRINT @cms
	EXEC sp_executesql @cms

	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tStaff;
				SELECT * INTO STG.tStaff FROM SRC.vStaff;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tAppliedSciencesPTCD_Staff;
				SELECT * INTO STG.tAppliedSciencesPTCD_Staff FROM SRC.vAppliedSciencesPTCD_Staff;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tAppliedSciencesPTCD_CaseTeamMember;
				SELECT * INTO STG.tAppliedSciencesPTCD_CaseTeamMember FROM SRC.vAppliedSciencesPTCD_CaseTeamMember;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tAppliedSciencesPTCD_Relationship;
				SELECT * INTO STG.tAppliedSciencesPTCD_Relationship FROM SRC.vAppliedSciencesPTCD_Relationship;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tAppliedSciencesPTCD;
				SELECT * INTO STG.tAppliedSciencesPTCD FROM SRC.vAppliedSciencesPTCD;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tAppliedSciencesML;
				SELECT * INTO STG.tAppliedSciencesML FROM SRC.vAppliedSciencesML;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tEnrolementDetails_LatestRecords;
				SELECT * INTO STG.tEnrolementDetails_LatestRecords FROM SRC.vEnrolementDetails_LatestRecords;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tModuleOffering;
				SELECT * INTO STG.tModuleOffering FROM SRC.vModuleOffering;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------

  EXECUTE @RC =  [INT].[spApplicationEnrolmentDetails_Affiliation];

	----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS STG.tStudent_SupportProfile;
				SELECT * INTO STG.tStudent_SupportProfile FROM SRC.vStudent_SupportProfile;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------

	--MOVED TO SEPARATE JOB FOR COGNOS INTEGRATION
 -- SET @cms = 'DROP TABLE IF EXISTS STG.tStudent_EngagementDetails;
	--			SELECT * INTO STG.tStudent_EngagementDetails FROM SRC.vStudent_EngagementDetails;';
	
	--PRINT @cms
	--EXEC sp_executesql @cms
	-------------------------------------------------------------------------------------
    SET @cms = 'DROP TABLE IF EXISTS STG.tModule;
				SELECT * INTO STG.tModule FROM SRC.vModule;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
    SET @cms = 'DROP TABLE IF EXISTS STG.tModuleEnrolmentDetails;
				SELECT * INTO STG.tModuleEnrolmentDetails FROM SRC.vModuleEnrolmentDetails;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
    SET @cms = 'DROP TABLE IF EXISTS STG.tGrade;
				SELECT * INTO STG.tGrade FROM SRC.vGrade;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tStudentAdditionalDetails;
				SELECT * INTO STG.tStudentAdditionalDetails FROM SRC.vStudentAdditionalDetails;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tExetenuatingCirc;
				SELECT * INTO STG.tExetenuatingCirc FROM SRC.vExetenuatingCirc;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tModuleComponentandSubComponent;
				SELECT * INTO STG.tModuleComponentandSubComponent FROM SRC.vModuleComponentandSubComponent;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tModuleComponentEnrolmentDetails;
				SELECT * INTO STG.tModuleComponentEnrolmentDetails FROM SRC.vModuleComponentEnrolmentDetails;';  --New Added
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tModuleSubComponentEnrolmentDetails;
				SELECT * INTO STG.tModuleSubComponentEnrolmentDetails FROM SRC.vModuleSubComponentEnrolmentDetails;';  -- New Added
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
	SET @cms = 'DROP TABLE IF EXISTS [STG].[tModuleLeaderCourseDirector];
				SELECT * INTO [STG].[tModuleLeaderCourseDirector] FROM [SRC].[vModuleLeaderCourseDirector];'; --New Added
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS [STG].[tModuleLeaderCourseDirectorRelationship];
				SELECT * INTO[STG].[tModuleLeaderCourseDirectorRelationship] FROM [SRC].[vModuleLeaderCourseDirectorRelationship];';  --New Added
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------
	SET @cms = 'DROP TABLE IF EXISTS [STG].[tModuleLeaderCourseDirectorAffiliation];
				SELECT * INTO [STG].[tModuleLeaderCourseDirectorAffiliation] FROM [SRC].[vModuleLeaderCourseDirectorAffiliation];';   --New Added
	
	PRINT @cms
	EXEC sp_executesql @cms
	-----------------------------------------------------------------------------------

    SET @cms = 'DROP TABLE IF EXISTS STG.tApplicationEnrolmentDetails_DeProv_DeAct;
				SELECT * INTO STG.tApplicationEnrolmentDetails_DeProv_DeAct FROM SRC.vApplicationEnrolmentDetails_DeProv_DeAct;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 	

END;