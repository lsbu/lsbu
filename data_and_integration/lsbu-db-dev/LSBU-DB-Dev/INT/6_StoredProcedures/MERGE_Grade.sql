CREATE OR ALTER PROCEDURE [INT].[MERGE_Grade](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_Grade AS TAR

				USING (SELECT * FROM [STG].[tGrade] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tGrade] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
	      [QL_StudentID]=SRC.[QL_StudentID],
	      [QL_ModuleId]=SRC.[QL_ModuleId],
	      [QL_ModuleInstance]=SRC.[QL_ModuleInstance],
	      [QL_AcadProgNameId]=SRC.[QL_AcadProgNameId],
	      [QL_Term]=SRC.[QL_Term],
	      [QL_AOSPeriod]=SRC.[QL_AOSPeriod],
	      [QL_AcademicPeriod]=SRC.[QL_AcademicPeriod],
	      [QL_Semester]=SRC.[QL_Semester],
	      [QL_Attempt]=SRC.[QL_Attempt],
	      [QL_OverallCourseMarks]=SRC.[QL_OverallCourseMarks],
	      [QL_OverallCourseGrade]=SRC.[QL_OverallCourseGrade],
	      [QL_OverallModuleMarks]=SRC.[QL_OverallModuleMarks],
	      [QL_OverallModuleGrade]=SRC.[QL_OverallModuleGrade],
	      [CW1]=SRC.[CW1],
		    [CW2]=SRC.[CW2],
		    [CW3]=SRC.[CW3],
		    [CW4]=SRC.[CW4],
		    [CW5]=SRC.[CW5],
		    [CW6]=SRC.[CW6],
		    [EX1]=SRC.[EX1],
		    [EX2]=SRC.[EX2],
		    [EX3]=SRC.[EX3],
		    [EX4]=SRC.[EX4],
		    [EX5]=SRC.[EX5],
		    [Term_LSB_ExternalID]=SRC.[Term_LSB_ExternalID],
		    [ModuleEnrolment_LSB_ExternalID]=SRC.[ModuleEnrolment_LSB_ExternalID],
		    [ModuleOffering_LSB_ExternalID]=SRC.[ModuleOffering_LSB_ExternalID],
        [ProgramEnrolment_LSB_ExternalID]=SRC.[ProgramEnrolment_LSB_ExternalID]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
				[FileRowNumber],
        [HashMD5],
        [InsetDateTime],
        [ChangeStatus],
		  	[QL_StudentID],
			  [QL_ModuleId],
			  [QL_ModuleInstance],
			  [QL_AcadProgNameId],
			  [QL_Term],
			  [QL_AOSPeriod],
			  [QL_AcademicPeriod],
			  [QL_Semester],
			  [QL_Attempt],
			  [QL_OverallCourseMarks],
			  [QL_OverallCourseGrade],
			  [QL_OverallModuleMarks],
			  [QL_OverallModuleGrade],
			  [CW1],
			  [CW2],
			  [CW3],
			  [CW4],
			  [CW5],
			  [CW6],
			  [EX1],
			  [EX2],
			  [EX3],
			  [EX4],
			  [EX5],
			  [Term_LSB_ExternalID],
			  [ModuleEnrolment_LSB_ExternalID],
			  [ModuleOffering_LSB_ExternalID],
        [ProgramEnrolment_LSB_ExternalID]
        )
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
		SRC.[FileRowNumber],
    SRC.[HashMD5],
    GETDATE(),
    'NEW',
		SRC.[QL_StudentID],
		SRC.[QL_ModuleId],
		SRC.[QL_ModuleInstance],
		SRC.[QL_AcadProgNameId],
		SRC.[QL_Term],
		SRC.[QL_AOSPeriod],
		SRC.[QL_AcademicPeriod],
		SRC.[QL_Semester],
		SRC.[QL_Attempt],
		SRC.[QL_OverallCourseMarks],
		SRC.[QL_OverallCourseGrade],
		SRC.[QL_OverallModuleMarks],
		SRC.[QL_OverallModuleGrade],
		SRC.[CW1],
		SRC.[CW2],
		SRC.[CW3],
		SRC.[CW4],
		SRC.[CW5],
		SRC.[CW6],
		SRC.[EX1],
		SRC.[EX2],
		SRC.[EX3],
		SRC.[EX4],
		SRC.[EX5],
		SRC.[Term_LSB_ExternalID],
		SRC.[ModuleEnrolment_LSB_ExternalID],
		SRC.[ModuleOffering_LSB_ExternalID],
    SRC.[ProgramEnrolment_LSB_ExternalID]
    )
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;