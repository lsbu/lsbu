CREATE OR ALTER PROCEDURE INT.Maximizer_CaseDetailNumber_PopulateData
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE INT.SRC_Maximizer_PIVOT_CaseDetailNumber

	INSERT INTO INT.SRC_Maximizer_PIVOT_CaseDetailNumber
		([CaseId],
		[Safety_Concern_Response_Number_of_students_affected])
	SELECT
		[CaseId],
		[Safety_Concern_Response_Number_of_students_affected]
	FROM SRC.Maximizer_PIVOT_CaseDetailNumber

END
GO