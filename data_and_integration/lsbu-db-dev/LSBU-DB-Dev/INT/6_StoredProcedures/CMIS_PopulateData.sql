CREATE OR ALTER PROCEDURE [INT].[CMIS_PopulateData]
AS
BEGIN
	----------------------------------------------------------------------------------- 
       DROP TABLE IF EXISTS [SRC].[SRC_ROW_TMP_CMIS];
       SELECT DISTINCT
        Q1.StudentId,
        Q1.CourseId,
        Q1.AcademicPeriod,
        Q1.LinkCode,
        Q1.ModuleId,
        Q1.InstId,
        Q1.LecturerId,
        Q1.Name,
        Q1.CMISCALC_CourseCode,--substring(Q1.CMISCALC_CourseCode,0, charindex('_',(Q1.CMISCALC_CourseCode))) as CMISCALC_CourseCode,
       Q1.CMISCALC_ModuleId,-- substring(Q1.CMISCALC_ModuleId,0,(len(Q1.CMISCALC_ModuleId)- charindex('_',reverse(Q1.CMISCALC_ModuleId))+1)) as CMISCALC_ModuleId,
        Q1.CMISCALC_ModuleInstance,
        Q1.CMISCALC_Semester,
        Q1.CMISCALC_AcademicPeriod,
        Q1.CMISCALC_LASTNAME,
        Q1.CMISCALC_FIRSTNAME,
        Q1.ROW_ADD_TS,         
        RANK()OVER(Partition by Q1.StudentId, CMISCALC_ModuleId, Q1.CMISCALC_ModuleInstance, Q1.CMISCALC_CourseCode, Q1.LinkCode, Q1.CMISCALC_AcademicPeriod, Q1.CMISCALC_Semester, Q1.LecturerId order by Q1.CMISCALC_LASTNAME, Q1.CMISCALC_FIRSTNAME) rnk
       INTO [SRC].[SRC_ROW_TMP_CMIS]
       FROM (
        SELECT 
         T.StudentId as StudentId,
         T.CourseId as CourseId,
         T.AcademicPeriod as AcademicPeriod,
         T.LinkCode as LinkCode,
         T.ModuleId as ModuleId,
         T.InstId as InstId,
         T.LecturerId as LecturerId,
         T.Name as Name,
         left(T.[COURSEID], len(T.[COURSEID]) - charindex('_', reverse(T.[COURSEID]) + '_')) as CMISCALC_CourseCode,
         left(T.[MODULEID], len(T.[MODULEID]) - charindex('_', reverse(T.[MODULEID]) + '_')) as CMISCALC_ModuleId,
         replace(T.InstId,'-','') as CMISCALC_ModuleInstance,
         right(T.[MODULEID], charindex('_', reverse(T.[MODULEID])) - 1) AS CMISCALC_Semester,
         REPLACE(SUBSTRING([ACADEMICPERIOD],3,LEN([ACADEMICPERIOD])),'-','/') AS CMISCALC_AcademicPeriod,
         CASE
           WHEN CHARINDEX(',',T.[NAME]) != 0 THEN LEFT(T.[NAME],CHARINDEX(',',T.[NAME])-1)
           ELSE T.[NAME]
         END AS CMISCALC_LASTNAME,
         CASE
           WHEN CHARINDEX(',',T.[NAME]) != 0 THEN right(T.[NAME], charindex(',', reverse(T.[NAME]))-2)
           ELSE T.[NAME]
         END AS CMISCALC_FIRSTNAME,
         GETDATE() ROW_ADD_TS          
        FROM OPENQUERY
         (CMISPROD, 
         'SELECT DISTINCT
            Students.StudentId,
            Students.CourseId,
            Students.SetId as AcademicPeriod,
            Students.LinkCode,
            StuModules.ModuleId,
            StuModules.InstId,
            Timetable.LecturerId,
            Lecturer.Name
          FROM CMIS_ADMIN.Students, CMIS_ADMIN.StuModules, CMIS_ADMIN.Timetable, CMIS_ADMIN.Lecturer
          WHERE TimeTable.SetId = Students.SetId
           AND Timetable.ModuleId = StuModules.ModuleId
           AND TimeTable.InstId = StuModules.InstId
           AND Timetable.ModGrpCode = StuModules.ModGrpCode
           AND StuModules.Studentid = Students.StudentId
           AND TimeTable.Lecturerid = Lecturer.LecturerId') T
          WHERE T.AcademicPeriod IN (SELECT *  FROM OPENQUERY(CMISPROD,'SELECT DISTINCT Students.SetId FROM CMIS_ADMIN.Students ORDER BY 1 DESC FETCH NEXT 2 ROWS ONLY'))
          ) Q1;

	----------------------------------------------------------------------------------- 
END;
