CREATE OR ALTER PROCEDURE INT.Maximizer_CaseDetailString_PopulateData
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE INT.SRC_Maximizer_PIVOT_CaseDetailString

	INSERT INTO INT.SRC_Maximizer_PIVOT_CaseDetailString
		([CaseId],
		[Fitness_to_Study_Level_1__Informal__Lead_person_name],
		[MHWB_Duty_Action_s__needed],
		[MHWB_Duty_Referral_to_other_services_detail],
		[MHWB_Duty_Referred_by_other___detail])
	SELECT
		[CaseId],
		[Fitness_to_Study_Level_1__Informal__Lead_person_name],
		[MHWB_Duty_Action_s__needed],
		[MHWB_Duty_Referral_to_other_services_detail],
		[MHWB_Duty_Referred_by_other___detail]
	FROM SRC.Maximizer_PIVOT_CaseDetailString

END
GO