CREATE OR ALTER PROCEDURE [INT].[MaterilizeSRC_QLCMISSharepoint_CaseTeamMember_Views]
AS

BEGIN

	DROP TABLE IF EXISTS STG.tAllStaff_CaseTeamMember;
				
	SELECT T.*,null as CourseTutor, null as PersonalTutor, null as ModuleLeader INTO STG.tAllStaff_CaseTeamMember FROM 
	(	
					Select DISTINCT
					CMIS_StudentId as StudentId,
					CMIS_TeachingStaffEmail as FederationIdentifier
					from  [STG].[tStudentTeachingStaff_Relationship] -- Course Tutor
					UNION 
					SELECT DISTINCT
					PTCDCSV_StudentId AS StudentId,
					PTCDCSV_PersonalTutorEmailAddress AS FederationIdentifier
					FROM [STG].[tAppliedSciencesPTCD_Relationship] -- Personal Tutor
					UNION  
					SELECT DISTINCT
					QL_StudentID AS StudentId,
					Staff_LSB_ExternalID as FederationIdentifier
					FROM [STG].[tModuleLeaderCourseDirectorRelationship] where  QL_ModuleLeader IS NOT NULL
	) T ;
	UPDATE ASCT 
	SET CourseTutor = '1' 
	FROM STG.tAllStaff_CaseTeamMember ASCT 
	WHERE EXISTS (
					SELECT 1 from  [STG].[tStudentTeachingStaff_Relationship] 
					WHERE ASCT.StudentId = CMIS_StudentId  AND ASCT.FederationIdentifier =  CMIS_TeachingStaffEmail
	);
	UPDATE ASCT 
	SET PersonalTutor = '1' 
	FROM STG.tAllStaff_CaseTeamMember ASCT 
	WHERE EXISTS (
					SELECT DISTINCT SrcSystem from  [STG].[tAppliedSciencesPTCD_Relationship] 
					WHERE ASCT.StudentId = PTCDCSV_StudentId  AND ASCT.FederationIdentifier =  PTCDCSV_PersonalTutorEmailAddress
	);
	UPDATE ASCT 
	SET ModuleLeader = '1' 
	FROM STG.tAllStaff_CaseTeamMember ASCT 
	WHERE EXISTS (
					SELECT DISTINCT SrcSystem from  [STG].[tModuleLeaderCourseDirectorRelationship] 
					WHERE ASCT.StudentId = QL_StudentID  AND ASCT.FederationIdentifier =  Staff_LSB_ExternalID AND QL_ModuleLeader IS NOT NULL
	);
END;
