CREATE OR ALTER PROCEDURE [INT].[MERGE_Student_EngagementDetails](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_Student_EngagementDetails AS TAR

				USING (SELECT * FROM [STG].[tStudent_EngagementDetails] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tStudent_EngagementDetails] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
        [Cognos_StudentID]=SRC.[Cognos_StudentID],
	      [Cognos_AcadProgNameId]=SRC.[Cognos_AcadProgNameId],
	      [Cognos_Term]=SRC.[Cognos_Term],
	      [Cognos_AOSPeriod]=SRC.[Cognos_AOSPeriod],
	      [Cognos_AcademicPeriod]=SRC.[Cognos_AcademicPeriod],
	      [Cognos_TurnstileActivity]=SRC.[Cognos_TurnstileActivity],
        [Cognos_LibraryAccessActivity]=SRC.[Cognos_LibraryAccessActivity],
        [Cognos_MoodleActivity]=SRC.[Cognos_MoodleActivity],
        [Cognos_Submissionssum]=SRC.[Cognos_Submissionssum]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
				[FileRowNumber],
        [HashMD5],
        [InsetDateTime],
        [ChangeStatus],
        [Cognos_StudentID],
	      [Cognos_AcadProgNameId],
	      [Cognos_Term],
	      [Cognos_AOSPeriod],
	      [Cognos_AcademicPeriod],
	      [Cognos_TurnstileActivity],
        [Cognos_LibraryAccessActivity],
        [Cognos_MoodleActivity],
        [Cognos_Submissionssum]
        )
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
		SRC.[FileRowNumber],
    SRC.[HashMD5],
    GETDATE(),
    'NEW',
    SRC.[Cognos_StudentID],
	  SRC.[Cognos_AcadProgNameId],
	  SRC.[Cognos_Term],
	  SRC.[Cognos_AOSPeriod],
	  SRC.[Cognos_AcademicPeriod],
	  SRC.[Cognos_TurnstileActivity],
    SRC.[Cognos_LibraryAccessActivity],
    SRC.[Cognos_MoodleActivity],
    SRC.[Cognos_Submissionssum]
    )
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;