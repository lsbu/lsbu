CREATE OR ALTER PROCEDURE [INT].[MERGE_SALAdvisee](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_SALAdvisee] AS TAR
				USING (SELECT * FROM [STG].[tSALAdviseeCase] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tSALAdviseeCase] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_StudentID]=SRC.[QL_StudentID],
				[QL_FirstName]=SRC.[QL_FirstName],
				[QL_Surname]=SRC.[QL_Surname],
				[QL_Phone]=SRC.[QL_Phone],
				[QL_LSBUEmail]=SRC.[QL_LSBUEmail],
				[QL_PersonalEmail]=SRC.[QL_PersonalEmail],
				[QL_MobileNo]=SRC.[QL_MobileNo]
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID]
					,[FullFileName]
					,[SrcSystem]
					,[ObjectName]
					,[RowNo]
					,[FileDateTime]
					,[LSB_ExternalID]
					,[FileRowNumber]
					,[HashMD5]
					,[InsetDateTime]
					,[ChangeStatus]
					,[QL_StudentID]
					,[QL_FirstName]
					,[QL_Surname]
					,[QL_Phone]
					,[QL_LSBUEmail]
					,[QL_PersonalEmail]
					,[QL_MobileNo])
		VALUES	(SRC.[RunID]
				,SRC.[FullFileName]
				,SRC.[SrcSystem]
				,SRC.[ObjectName]
				,SRC.[RowNo]
				,SRC.[FileDateTime]
				,SRC.[LSB_ExternalID]
				,SRC.[FileRowNumber]
				,SRC.[HashMD5]
				,GETDATE()
				,'NEW'
				,SRC.[QL_StudentID]
				,SRC.[QL_FirstName]
				,SRC.[QL_Surname]
				,SRC.[QL_Phone]
				,SRC.[QL_LSBUEmail]
				,SRC.[QL_PersonalEmail]
				,SRC.[QL_MobileNo])
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;

END;
GO