CREATE OR ALTER   PROCEDURE [INT].[MERGE_Student](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_Student] AS TAR
				USING (SELECT * FROM [STG].[tStudent] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tStudent] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_StudentID]=SRC.[QL_StudentID],
				[QL_UCASPersonalID]=SRC.[QL_UCASPersonalID],
				[QL_Title]=SRC.[QL_Title],
				[QL_Initials]=SRC.[QL_Initials],
				[QL_FirstName]=SRC.[QL_FirstName],
				[QL_FamiliarName]=SRC.[QL_FamiliarName],
				[QL_Surname]=SRC.[QL_Surname],
				[QL_MiddleName]=SRC.[QL_MiddleName],
				[QL_Birthdate]=SRC.[QL_Birthdate],
				[QL_StudentStatus]=SRC.[QL_StudentStatus],
				[QL_Gender]=SRC.[QL_Gender],
				[QL_Domicile]=SRC.[QL_Domicile],
				[QL_Origin]=SRC.[QL_Origin],
				[QL_Nationality]=SRC.[QL_Nationality],
				[QL_Disabilty]=SRC.[QL_Disabilty],
				[QL_Add1]=SRC.[QL_Add1],
				[QL_Add2]=SRC.[QL_Add2],
				[QL_Add3]=SRC.[QL_Add3],
				[QL_Add4]=SRC.[QL_Add4],
				[QL_Add5]=SRC.[QL_Add5],
				[QL_Postcode]=SRC.[QL_Postcode],
				[QL_Country]=SRC.[QL_Country],
				[QL_AddressType]=SRC.[QL_AddressType],
				[QL_Phone]=SRC.[QL_Phone],
				[QL_LSBUEmail]=SRC.[QL_LSBUEmail],
				[QL_PersonalEmail]=SRC.[QL_PersonalEmail],
				[QL_MobileNo]=SRC.[QL_MobileNo],
				[QL_CreatedDate]=SRC.[QL_CreatedDate],
				[QL_LastModifiedDate]=SRC.[QL_LastModifiedDate],
				[QL_DisabiltyAllowance]=SRC.[QL_DisabiltyAllowance],
				[QL_Term_Add1]=SRC.[QL_Term_Add1],
				[QL_Term_Add2]=SRC.[QL_Term_Add2],
				[QL_Term_Add3]=SRC.[QL_Term_Add3],
				[QL_Term_Add4]=SRC.[QL_Term_Add4],
				[QL_Term_Add5]=SRC.[QL_Term_Add5],
				[QL_Term_Postcode]=SRC.[QL_Term_Postcode],
				[QL_Term_Country]=SRC.[QL_Term_Country],
				[QL_StudentUsername]=SRC.[QL_StudentUsername],
				[QL_DDSMarking]=SRC.[QL_DDSMarking],
				[QL_CareLeaver]=SRC.[QL_CareLeaver],
        [QL_OptOut_SU]=SRC.[QL_OptOut_SU],
        [QL_New_Returning_Flag]=SRC.[QL_New_Returning_Flag],
        [QL_Ethinicity]=SRC.[QL_Ethinicity]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[QL_StudentID],[QL_UCASPersonalID],[QL_Title],[QL_Initials],
					[QL_FirstName],[QL_FamiliarName],[QL_Surname],[QL_MiddleName],
					[QL_Birthdate],[QL_StudentStatus],[QL_Gender],[QL_Domicile],
					[QL_Origin],[QL_Nationality],[QL_Disabilty],
					[QL_Add1],[QL_Add2],[QL_Add3],[QL_Add4],[QL_Add5],
					[QL_Postcode],[QL_Country],[QL_AddressType],[QL_Phone],
					[QL_LSBUEmail],[QL_PersonalEmail],[QL_MobileNo],
					[QL_CreatedDate],[QL_LastModifiedDate],
					[QL_DisabiltyAllowance],[QL_Term_Add1],
					[QL_Term_Add2],[QL_Term_Add3],
					[QL_Term_Add4],[QL_Term_Add5],
					[QL_Term_Postcode],[QL_Term_Country],
					[QL_StudentUsername],[QL_DDSMarking],[QL_CareLeaver],
          [QL_OptOut_SU], [QL_New_Returning_Flag], [QL_Ethinicity]
					)
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[QL_StudentID],SRC.[QL_UCASPersonalID],SRC.[QL_Title],SRC.[QL_Initials],
				SRC.[QL_FirstName],SRC.[QL_FamiliarName],SRC.[QL_Surname],SRC.[QL_MiddleName],
				SRC.[QL_Birthdate],SRC.[QL_StudentStatus],SRC.[QL_Gender],SRC.[QL_Domicile],
				SRC.[QL_Origin],SRC.[QL_Nationality],SRC.[QL_Disabilty],
				SRC.[QL_Add1],SRC.[QL_Add2],SRC.[QL_Add3],SRC.[QL_Add4],SRC.[QL_Add5],
				SRC.[QL_Postcode],SRC.[QL_Country],SRC.[QL_AddressType],SRC.[QL_Phone],
				SRC.[QL_LSBUEmail],SRC.[QL_PersonalEmail],SRC.[QL_MobileNo],
				SRC.[QL_CreatedDate],SRC.[QL_LastModifiedDate],
				
				SRC.[QL_DisabiltyAllowance],SRC.[QL_Term_Add1],
				SRC.[QL_Term_Add2],SRC.[QL_Term_Add3],
				SRC.[QL_Term_Add4],SRC.[QL_Term_Add5],
				SRC.[QL_Term_Postcode],SRC.[QL_Term_Country],
				SRC.[QL_StudentUsername],SRC.[QL_DDSMarking],SRC.[QL_CareLeaver],
        SRC.[QL_OptOut_SU], SRC.[QL_New_Returning_Flag], SRC.[QL_Ethinicity]
				)
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;


END;

