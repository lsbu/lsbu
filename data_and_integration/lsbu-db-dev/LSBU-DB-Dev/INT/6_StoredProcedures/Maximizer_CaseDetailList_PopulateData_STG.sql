CREATE OR ALTER PROCEDURE [INT].[Maximizer_CaseDetailList_PopulateData_STG]
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE INT.STG_MAX_CaseDetailListMulti
	
	;WITH CTE1 AS (
		SELECT DISTINCT 
			CO.CaseId, 
			CO.Name,
			dbo.TRIM(STUFF(
				(SELECT 
					';' + Value
				FROM Maximizer.src.Case_DetailListMulti CI 
				WHERE CI.CaseId = CO.CaseId AND CI.Name = CO.Name 
				FOR XML PATH('')), 1, 1, '')) [Value]
		FROM Maximizer.src.Case_DetailListMulti CO
		JOIN (SELECT 
				name, 
				CaseId 
			 FROM Maximizer.src.Case_DetailListMulti 
			 GROUP BY Name, CaseId 
			 HAVING count(1) > 1) AS T1 ON CO.CaseId = T1.CaseId AND CO.Name = T1.Name),
	CTE2 AS (
		SELECT 
			CDLM.CaseId, 
			CDLM.Name, 
			CDLM.Value 
		FROM Maximizer.src.Case_DetailListMulti CDLM
		WHERE NOT EXISTS (SELECT 1 FROM CTE1 WHERE CDLM.CaseId = CTE1.CaseId AND CDLM.Name = CTE1.Name)
		UNION
		SELECT
			CTE1.CaseId,
			CTE1.Name,
			CTE1.Value
		FROM CTE1
	)
	INSERT INTO INT.STG_MAX_CaseDetailListMulti
		(CaseId
		,Name
		,Value)
	SELECT 
		CaseId,
		Name,
		Value
	FROM CTE2
END