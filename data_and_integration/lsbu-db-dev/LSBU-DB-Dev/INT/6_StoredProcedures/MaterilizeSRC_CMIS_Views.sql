CREATE OR ALTER PROCEDURE [INT].[MaterilizeSRC_CMIS_Views]
AS
DECLARE 
 @cms nvarchar(max) = N'',
 @RC int
BEGIN
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tStudentTeachingStaff_Contact;
				SELECT * INTO STG.tStudentTeachingStaff_Contact FROM SRC.vStudentTeachingStaff_Contact;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
    SET @cms = 'DROP TABLE IF EXISTS STG.tStudentTeachingStaff_Relationship;
				SELECT * INTO STG.tStudentTeachingStaff_Relationship FROM SRC.vStudentTeachingStaff_Relationship;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
END;