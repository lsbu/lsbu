
CREATE OR ALTER     PROCEDURE [INT].[MERGE_ExetenuatingCirc](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_ExetenuatingCirc AS TAR

				USING (SELECT * FROM [STG].[tExetenuatingCirc] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tExetenuatingCirc] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
        [QL_StudentID]=SRC.[QL_StudentID],
        [QL_ModuleId]=SRC.[QL_ModuleId],
        [QL_ModuleInstance]=SRC.[QL_ModuleInstance],
        [QL_AcadProgNameId]=SRC.[QL_AcadProgNameId],
        [QL_Term]=SRC.[QL_Term],
        [QL_AOSPeriod]=SRC.[QL_AOSPeriod],
        [QL_AcademicPeriod]=SRC.[QL_AcademicPeriod],
        [QL_Semester]=SRC.[QL_Semester],
        [QL_Attempt]=SRC.[QL_Attempt],
        [QL_ComponentType]=SRC.[QL_ComponentType],
        [QL_ECStatusFlag]=SRC.[QL_ECStatusFlag],
        [QL_ECStatusFlagDate]=SRC.[QL_ECStatusFlagDate]


		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
				[FileRowNumber],
        [HashMD5],
        [InsetDateTime],
        [ChangeStatus],
        [QL_StudentID],
        [QL_ModuleId],
        [QL_ModuleInstance],
        [QL_AcadProgNameId],
        [QL_Term],
        [QL_AOSPeriod],
        [QL_AcademicPeriod],
        [QL_Semester],
        [QL_Attempt],
        [QL_ComponentType],
        [QL_ECStatusFlag],
        [QL_ECStatusFlagDate]

        )
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
		SRC.[FileRowNumber],
    SRC.[HashMD5],
    GETDATE(),
    'NEW',
    SRC.[QL_StudentID],
    SRC.[QL_ModuleId],
    SRC.[QL_ModuleInstance],
    SRC.[QL_AcadProgNameId],
    SRC.[QL_Term],
    SRC.[QL_AOSPeriod],
    SRC.[QL_AcademicPeriod],
    SRC.[QL_Semester],
    SRC.[QL_Attempt],
    SRC.[QL_ComponentType],
    SRC.[QL_ECStatusFlag],
    SRC.[QL_ECStatusFlagDate]

    )
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;

