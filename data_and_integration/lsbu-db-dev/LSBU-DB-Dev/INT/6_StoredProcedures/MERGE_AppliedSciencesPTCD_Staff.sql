CREATE OR ALTER PROCEDURE [INT].[MERGE_AppliedSciencesPTCD_Staff](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_AppliedSciencesPTCD_Staff] AS TAR
				USING (SELECT * FROM  [STG].tAppliedSciencesPTCD_Staff CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].tAppliedSciencesPTCD_Staff group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_StaffId]=SRC.[QL_StaffId],
				[QL_StaffEmailAddress]=SRC.[QL_StaffEmailAddress],
				[QL_StaffFirstName]=SRC.[QL_StaffFirstName],
				[QL_StaffLastName]=SRC.[QL_StaffLastName],
				[QL_Gender]=SRC.[QL_Gender],
				[SF_UserId]=SRC.[SF_UserId],
				[StaffRank]=SRC.[StaffRank]
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[QL_StaffId],
					[QL_StaffEmailAddress],
					[QL_StaffFirstName],
					[QL_StaffLastName],
					[QL_Gender],
					[SF_UserId],
					[StaffRank]
					)
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[QL_StaffId],
				SRC.[QL_StaffEmailAddress],
				SRC.[QL_StaffFirstName],
				SRC.[QL_StaffLastName],
				SRC.[QL_Gender],
				SRC.[SF_UserId],
				SRC.[StaffRank]
				)
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;
END;
GO