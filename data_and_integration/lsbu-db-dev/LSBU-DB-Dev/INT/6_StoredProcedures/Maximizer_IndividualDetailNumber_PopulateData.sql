CREATE OR ALTER PROCEDURE [INT].[Maximizer_IndividualDetailNumber_PopulateData]
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE INT.SRC_Maximizer_PIVOT_IndividualDetailNumber

	INSERT INTO INT.SRC_Maximizer_PIVOT_IndividualDetailNumber
		([Id],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Cost],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Amount],
		[ARCHIVED_FIELDS_Note_taker_value],
		[ARCHIVED_FIELDS_Old_fields_Interim_Support_Claimed_VALUE],
		[ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Value],
		[DDS_DSA_Mentoring_Support_Mentor_initial_value],
		[DDS_DSA_Mentoring_Support_Total_Interim_mentoring_hours_used],
		[DDS_DSA_Study_Skills_Tuition_Tutor_Initial_Value],
		[MH__WB_MHWB_Archive_Number_of_sessions])
	SELECT
		[Id],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Cost],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Amount],
		[ARCHIVED_FIELDS_Note_taker_value],
		[ARCHIVED_FIELDS_Old_fields_Interim_Support_Claimed_VALUE],
		[ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Value],
		[DDS_DSA_Mentoring_Support_Mentor_initial_value],
		[DDS_DSA_Mentoring_Support_Total_Interim_mentoring_hours_used],
		[DDS_DSA_Study_Skills_Tuition_Tutor_Initial_Value],
		[MH__WB_MHWB_Archive_Number_of_sessions]
	FROM SRC.Maximizer_PIVOT_IndividualDetailNumber

END