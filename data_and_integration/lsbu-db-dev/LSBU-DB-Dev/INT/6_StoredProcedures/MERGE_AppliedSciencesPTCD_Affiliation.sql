CREATE OR ALTER PROCEDURE [INT].[MERGE_AppliedSciencesPTCD_Affiliation](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_AppliedSciencesPTCD_Affiliation] AS TAR
				USING (SELECT * FROM  [STG].[tAppliedSciencesPTCD_Affiliation] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tAppliedSciencesPTCD_Affiliation] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[PTCDCSV_CourseCode]=SRC.[PTCDCSV_CourseCode],
				[PTCDCSV_CourseName]=SRC.[PTCDCSV_CourseName],
				[QL_CDStaffId]=SRC.[QL_CDStaffId],
				[PTCDCSV_CDFirstName]=SRC.[PTCDCSV_CDFirstName],
				[PTCDCSV_CDLastname]=SRC.[PTCDCSV_CDLastname],
				[PTCDCSV_CDEmail]=SRC.[PTCDCSV_CDEmail],
				[StaffRank]=SRC.[StaffRank]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[PTCDCSV_CourseCode],
					[PTCDCSV_CourseName],
					[QL_CDStaffId],
					[PTCDCSV_CDFirstName],
					[PTCDCSV_CDLastname],
					[PTCDCSV_CDEmail],
					[StaffRank]
					)
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[PTCDCSV_CourseCode],
				SRC.[PTCDCSV_CourseName],
				SRC.[QL_CDStaffId],
				SRC.[PTCDCSV_CDFirstName],
				SRC.[PTCDCSV_CDLastname],
				SRC.[PTCDCSV_CDEmail],
				SRC.[StaffRank]
				)
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;
END;
GO