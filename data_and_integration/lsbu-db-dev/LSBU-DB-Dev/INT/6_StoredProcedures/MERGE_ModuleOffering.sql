CREATE OR ALTER PROCEDURE [INT].[MERGE_ModuleOffering](
			@RunID INTEGER
			)
AS
BEGIN
		MERGE [INT].[SRC_ModuleOffering] AS TAR
				USING (SELECT * FROM [STG].[tModuleOffering]
					   WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tModuleOffering] group by LSB_ExternalID having count(1)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
					(CASE
						WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
						WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
					END),
				[QL_ModuleId]=SRC.[QL_ModuleId],
				[QL_ModuleInstanceName]=SRC.[QL_ModuleInstanceName],
				[QL_ModuleInstance]=SRC.[QL_ModuleInstance],
				[QL_AcademicPeriod]=SRC.[QL_AcademicPeriod],
				[QL_ModuleStartDate]=SRC.[QL_ModuleStartDate],
				[QL_ModuleEndDate]=SRC.[QL_ModuleEndDate],
				[QL_ModuleCredit]=SRC.[QL_ModuleCredit],
				[QL_ModuleLeader]=SRC.[QL_ModuleLeader]
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[QL_ModuleId],[QL_ModuleInstanceName],[QL_ModuleInstance],[QL_AcademicPeriod],
					[QL_ModuleStartDate],[QL_ModuleEndDate],[QL_ModuleCredit],[QL_ModuleLeader])
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[QL_ModuleId],SRC.[QL_ModuleInstanceName],SRC.[QL_ModuleInstance],SRC.[QL_AcademicPeriod],
				SRC.[QL_ModuleStartDate],SRC.[QL_ModuleEndDate],SRC.[QL_ModuleCredit],SRC.[QL_ModuleLeader])
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), 
				TAR.ChangeStatus = 'DELETE';
END;