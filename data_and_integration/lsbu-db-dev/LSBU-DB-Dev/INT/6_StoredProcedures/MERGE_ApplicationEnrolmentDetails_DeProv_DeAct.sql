CREATE OR ALTER     PROCEDURE [INT].[MERGE_ApplicationEnrolmentDetails_DeProv_DeAct](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_ApplicationEnrolmentDetails_DeProv_DeAct AS TAR

				USING (SELECT * FROM [STG].[tApplicationEnrolmentDetails_DeProv_DeAct] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tApplicationEnrolmentDetails_DeProv_DeAct] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[SF_UserID]=SRC.[SF_UserID],
				[QL_StudentID]=SRC.[QL_StudentID],
				[QLCALC_ContactRole]=SRC.[QLCALC_ContactRole],
				[QL_AcademicPeriod] = SRC.[QL_AcademicPeriod],
				[QL_AcadProgNameId] = SRC.[QL_AcadProgNameId],
				[QL_ApplicationStatus]=SRC.[QL_ApplicationStatus],
				[QL_AppStatusDateAchieved]=SRC.[QL_AppStatusDateAchieved],
				[QL_EnrollmentStatus]=SRC.[QL_EnrollmentStatus],
				[QL_EnrolStatusDateAchieved]=SRC.[QL_EnrolStatusDateAchieved],
				[QL_StudentGraduated]=SRC.[QL_StudentGraduated],
				[QLCALC_RowUpsertedDate]= SRC.[QLCALC_RowUpsertedDate],
				[QLCALC_DeProvisioningdate] = SRC.[QLCALC_DeProvisioningdate],
				[QLCALC_DeProvisionedStatus] = SRC.[QLCALC_DeProvisionedStatus]


		WHEN NOT MATCHED BY TARGET
			THEN INSERT 
			(
				[RunID],
				[FullFileName],
				[SrcSystem],
				[ObjectName],
				[RowNo],
				[FileDateTime],
				[LSB_ExternalID],
				[FileRowNumber],
				[HashMD5],
				[InsetDateTime],
				[ChangeStatus],
				[SF_UserID],
				[QL_StudentID],
				[QLCALC_ContactRole],
				[QL_AcademicPeriod],
				[QL_AcadProgNameId],
				[QL_ApplicationStatus],
				[QL_AppStatusDateAchieved],
				[QL_EnrollmentStatus],
				[QL_EnrolStatusDateAchieved],
				[QL_StudentGraduated],
				[QLCALC_RowUpsertedDate],
				[QLCALC_DeProvisioningdate],
				[QLCALC_DeProvisionedStatus]
		)
		VALUES 
		(
			   SRC.[RunID],
				SRC.[FullFileName],
				SRC.[SrcSystem],
				SRC.[ObjectName],
				SRC.[RowNo],
				SRC.[FileDateTime],
				SRC.[LSB_ExternalID],
					SRC.[FileRowNumber],
				SRC.[HashMD5],
				GETDATE(),
				'NEW',
				SRC.[SF_UserID],
				SRC.[QL_StudentID],
				SRC.[QLCALC_ContactRole],
				SRC.[QL_AcademicPeriod],
				SRC.[QL_AcadProgNameId],
				SRC.[QL_ApplicationStatus],
				SRC.[QL_AppStatusDateAchieved],
				SRC.[QL_EnrollmentStatus],
				SRC.[QL_EnrolStatusDateAchieved],
				SRC.[QL_StudentGraduated],
				SRC.[QLCALC_RowUpsertedDate],
				SRC.[QLCALC_DeProvisioningdate],
				SRC.[QLCALC_DeProvisionedStatus]

	)
    WHEN NOT MATCHED BY SOURCE
	THEN UPDATE 
	SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;