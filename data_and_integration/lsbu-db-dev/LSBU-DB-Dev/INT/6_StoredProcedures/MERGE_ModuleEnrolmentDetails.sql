CREATE OR ALTER PROCEDURE [INT].[MERGE_ModuleEnrolmentDetails](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].SRC_ModuleEnrolmentDetails AS TAR
				USING (SELECT * FROM [STG].[tModuleEnrolmentDetails] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tModuleEnrolmentDetails] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
        [ModuleOffering_LSB_ExternalID]=SRC.[ModuleOffering_LSB_ExternalID],
        [Term_LSB_ExternalID]=SRC.[Term_LSB_ExternalID],
        [ProgramEnrolment_LSB_ExternalID]=SRC.[ProgramEnrolment_LSB_ExternalID],
        [Affiliation_LSB_ExternalID]= SRC.[Affiliation_LSB_ExternalID], 
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
        [QL_StudentID]=SRC.[QL_StudentID],
        [QL_ModuleId]=SRC.[QL_ModuleId],
        [QL_ModuleInstance]=SRC.[QL_ModuleInstance],
        [QL_AcadProgNameId]=SRC.[QL_AcadProgNameId],
        [QL_Term]=SRC.[QL_Term],
        [QL_AOSPeriod]=SRC.[QL_AOSPeriod],
        [QL_AcademicPeriod]=SRC.[QL_AcademicPeriod],
        [QL_EnrollmentStatus]=SRC.[QL_EnrollmentStatus],
        [QL_EnrolmentStageIndicator]= SRC.[QL_EnrolmentStageIndicator],
        [QL_EnrolStatusDateAchieved]=SRC.[QL_EnrolStatusDateAchieved],
        [QL_Compulsory]=SRC.[QL_Compulsory],
        [QL_Semester]=SRC.[QL_Semester]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
          [ModuleOffering_LSB_ExternalID], [Term_LSB_ExternalID],[ProgramEnrolment_LSB_ExternalID],[Affiliation_LSB_ExternalID],  
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[QL_StudentID], [QL_ModuleId], [QL_ModuleInstance], [QL_AcadProgNameId], [QL_Term], [QL_AOSPeriod], [QL_AcademicPeriod], [QL_EnrollmentStatus], [QL_EnrolmentStageIndicator],
          [QL_EnrolStatusDateAchieved], [QL_Compulsory], [QL_Semester])
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID], 
        SRC.[ModuleOffering_LSB_ExternalID], SRC.[Term_LSB_ExternalID], SRC.[ProgramEnrolment_LSB_ExternalID],SRC.[Affiliation_LSB_ExternalID], 
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[QL_StudentID], SRC.[QL_ModuleId], SRC.[QL_ModuleInstance], SRC.[QL_AcadProgNameId], SRC.[QL_Term], SRC.[QL_AOSPeriod], SRC.[QL_AcademicPeriod], SRC.[QL_EnrollmentStatus], [QL_EnrolmentStageIndicator],
        SRC.[QL_EnrolStatusDateAchieved], SRC.[QL_Compulsory], SRC.[QL_Semester])
    WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;