
CREATE OR ALTER       PROCEDURE [INT].[MERGE_StudentTeachingStaff_Relationship](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_StudentTeachingStaff_Relationship AS TAR

				USING (SELECT * FROM [STG].[tStudentTeachingStaff_Relationship] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tStudentTeachingStaff_Relationship] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				--[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
        [ChangeStatus]= 'NO CHANGE',
        /*
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
         */
				[CMIS_StudentID]=SRC.[CMIS_StudentID],
        [CMIS_AcadProgNameId]=SRC.[CMIS_AcadProgNameId],
        [CMIS_AcademicPeriod]=SRC.[CMIS_AcademicPeriod],
        [CMIS_AOSPeriod]=SRC.[CMIS_AOSPeriod],
        [CMIS_ModuleId]=SRC.[CMIS_ModuleId],
        [CMIS_ModuleInstance]=SRC.[CMIS_ModuleInstance],
        [CMIS_Semester]=SRC.[CMIS_Semester],
        [CMIS_TeachingStaffId]=SRC.[CMIS_TeachingStaffId],
        [CMIS_TeachingStaffFirstName]=SRC.[CMIS_TeachingStaffFirstName],
        [CMIS_TeachingStaffLastname]=SRC.[CMIS_TeachingStaffLastname],
        [CMIS_TeachingStaffEmail]=SRC.[CMIS_TeachingStaffEmail],
        [ModuleOffering_LSB_ExternalID]=SRC.[ModuleOffering_LSB_ExternalID],
        [TeachingStaff_LSB_ExternalID]=SRC.[TeachingStaff_LSB_ExternalID]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
				[FileRowNumber],
        --[HashMD5],
        [InsetDateTime],
        [ChangeStatus],
        [CMIS_StudentID],
        [CMIS_AcadProgNameId],
        [CMIS_AcademicPeriod],
        [CMIS_AOSPeriod],
        [CMIS_ModuleId],
        [CMIS_ModuleInstance],
        [CMIS_Semester],
        [CMIS_TeachingStaffId],
        [CMIS_TeachingStaffFirstName],
        [CMIS_TeachingStaffLastname],
        [CMIS_TeachingStaffEmail],
        [ModuleOffering_LSB_ExternalID],
        [TeachingStaff_LSB_ExternalID])
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
		SRC.[FileRowNumber],
    --SRC.[HashMD5],
    GETDATE(),
    'NEW',
    SRC.[CMIS_StudentID],
    SRC.[CMIS_AcadProgNameId],
    SRC.[CMIS_AcademicPeriod],
    SRC.[CMIS_AOSPeriod],
    SRC.[CMIS_ModuleId],
    SRC.[CMIS_ModuleInstance],
    SRC.[CMIS_Semester],
    SRC.[CMIS_TeachingStaffId],
    SRC.[CMIS_TeachingStaffFirstName],
    SRC.[CMIS_TeachingStaffLastname],
    SRC.[CMIS_TeachingStaffEmail],
    SRC.[ModuleOffering_LSB_ExternalID],
    SRC.[TeachingStaff_LSB_ExternalID]
)
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;

