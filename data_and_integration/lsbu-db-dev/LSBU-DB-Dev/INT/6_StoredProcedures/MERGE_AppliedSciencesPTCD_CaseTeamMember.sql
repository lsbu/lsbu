CREATE OR ALTER PROCEDURE [INT].[MERGE_AppliedSciencesPTCD_CaseTeamMember](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_AppliedSciencesPTCD_CaseTeamMember] AS TAR
				USING  (SELECT * FROM  [SRC].[vAppliedSciencesPTCD_CaseTeamMember] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [SRC].[vAppliedSciencesPTCD_CaseTeamMember] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[PTCDCSV_StudentId]=SRC.[PTCDCSV_StudentId],
				[PTCDCSV_StudentName]=SRC.[PTCDCSV_StudentName],
				[PTCDCSV_StudentEmailAddress]=SRC.[PTCDCSV_StudentEmailAddress],
				[PTCDCSV_YearofStudy]=SRC.[PTCDCSV_YearofStudy],
				[PTCDCSV_Session]=SRC.[PTCDCSV_Session],
				[QL_PersonalTutorId]=SRC.[QL_PersonalTutorId],
				[PTCDCSV_PersonalTutorFirstName]=SRC.[PTCDCSV_PersonalTutorFirstName],
				[PTCDCSV_PersonalTutorLastName]=SRC.[PTCDCSV_PersonalTutorLastName],
				[PTCDCSV_PersonalTutorEmailAddress]=SRC.[PTCDCSV_PersonalTutorEmailAddress],
				[QL_CDStaffId]=SRC.[QL_CDStaffId],
				[PTCDCSV_CDFirstName]=SRC.[PTCDCSV_CDFirstName],
				[PTCDCSV_CDLastname]=SRC.[PTCDCSV_CDLastname],
				[PTCDCSV_CDEmail]=SRC.[PTCDCSV_CDEmail],
				[QL_MLStaffId]=SRC.[QL_MLStaffId],
				[PTCDCSV_MLFirstName]=SRC.[PTCDCSV_MLFirstName],
				[PTCDCSV_MLLastname]=SRC.[PTCDCSV_MLLastname],
				[PTCDCSV_MLEmail]=SRC.[PTCDCSV_MLEmail],
				[StaffRank]=SRC.[StaffRank]
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[PTCDCSV_StudentId],
					[PTCDCSV_StudentName],
					[PTCDCSV_StudentEmailAddress],
					[PTCDCSV_YearofStudy],
					[PTCDCSV_Session],
					[QL_PersonalTutorId],
					[PTCDCSV_PersonalTutorFirstName],
					[PTCDCSV_PersonalTutorLastName],
					[PTCDCSV_PersonalTutorEmailAddress],
					[QL_CDStaffId],
					[PTCDCSV_CDFirstName],
					[PTCDCSV_CDLastname],
					[PTCDCSV_CDEmail],
					[QL_MLStaffId],
					[PTCDCSV_MLFirstName],
					[PTCDCSV_MLLastname],
					[PTCDCSV_MLEmail],
					[StaffRank])
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[PTCDCSV_StudentId],
				SRC.[PTCDCSV_StudentName],
				SRC.[PTCDCSV_StudentEmailAddress],
				SRC.[PTCDCSV_YearofStudy],
				SRC.[PTCDCSV_Session],
				SRC.[QL_PersonalTutorId],
				SRC.[PTCDCSV_PersonalTutorFirstName],
				SRC.[PTCDCSV_PersonalTutorLastName],
				SRC.[PTCDCSV_PersonalTutorEmailAddress],
				SRC.[QL_CDStaffId],
				SRC.[PTCDCSV_CDFirstName],
				SRC.[PTCDCSV_CDLastname],
				SRC.[PTCDCSV_CDEmail],
				SRC.[QL_MLStaffId],
				SRC.[PTCDCSV_MLFirstName],
				SRC.[PTCDCSV_MLLastname],
				SRC.[PTCDCSV_MLEmail],
				SRC.[StaffRank])
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;
END;
GO