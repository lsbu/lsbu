CREATE OR ALTER PROCEDURE [INT].[MERGE_Department](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].SRC_Department AS TAR
				USING (SELECT * FROM [STG].[tDepartment] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tDepartment] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_DepartmentId]=SRC.[QL_DepartmentId],
				[QL_DepartmentName]=SRC.[QL_DepartmentName],
				[QL_EducationInstitutionId]=SRC.[QL_EducationInstitutionId],
				[QL_Website]=SRC.[QL_Website],
				[QL_Phone]=SRC.[QL_Phone],
				[QL_Add1]=SRC.[QL_Add1],
				[QL_Add2]=SRC.[QL_Add2],
				[QL_Add3]=SRC.[QL_Add3],
				[QL_Add4]=SRC.[QL_Add4],
				[QL_Add5]=SRC.[QL_Add5],
				[QL_Postcode]=SRC.[QL_Postcode],
				[QL_Country]=SRC.[QL_Country],
				[QL_Description]=SRC.[QL_Description],
				[QL_CreatedDate]=SRC.[QL_CreatedDate],
				[QL_LastModifiedDate]=SRC.[QL_LastModifiedDate]
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[QL_DepartmentId],[QL_DepartmentName],[QL_EducationInstitutionId],[QL_Website],
					[QL_Phone],[QL_Add1],[QL_Add2],[QL_Add3],[QL_Add4],[QL_Add5],[QL_Postcode],
					[QL_Country],[QL_Description],[QL_CreatedDate],[QL_LastModifiedDate])
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.[QL_DepartmentId],SRC.[QL_DepartmentName],SRC.[QL_EducationInstitutionId],SRC.[QL_Website],
				SRC.[QL_Phone],SRC.[QL_Add1],SRC.[QL_Add2],SRC.[QL_Add3],SRC.[QL_Add4],SRC.[QL_Add5],SRC.[QL_Postcode],
				SRC.[QL_Country],SRC.[QL_Description],SRC.[QL_CreatedDate],SRC.[QL_LastModifiedDate])
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;


END;

GO