CREATE OR ALTER PROCEDURE [INT].[MERGE_Student_SupportProfile] (
  @RunID INTEGER
)
AS

BEGIN

  MERGE [INT].SRC_Student_SupportProfile AS TAR
    USING (
      SELECT *
      FROM [STG].[tStudent_SupportProfile] t
      WHERE t.LSB_ExternalID NOT IN (
          SELECT LSB_ExternalID
          FROM [STG].[tStudent_SupportProfile]
          GROUP BY LSB_ExternalID
          HAVING COUNT(1) > 1
        )
    ) AS SRC
      ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
    WHEN MATCHED
      THEN UPDATE SET 
        [RunID] = SRC.[RunID],
        [FullFileName] = SRC.[FullFileName],
        [SrcSystem] = SRC.[SrcSystem],
        [ObjectName] = SRC.[ObjectName],
        [RowNo] = SRC.[RowNo],
        [FileDateTime] = SRC.[FileDateTime],
        [FileRowNumber] = SRC.[FileRowNumber],
        [HashMD5] = SRC.[HashMD5],
        [UpdateDateTime] = GETDATE(), 
        [ChangeStatus] = (
          CASE
            WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
            WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
          END
        ),
        [QL_StudentID] = SRC.[QL_StudentID],
        [QL_Disabilty] = SRC.[QL_Disabilty],
        [QL_DisabiltyAllowance] = SRC.[QL_DisabiltyAllowance],
        [QL_CareLeaver] = SRC.[QL_CareLeaver]
    WHEN NOT MATCHED BY TARGET
      THEN INSERT (
        [RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
        [FileRowNumber],
        [HashMD5],
        [UpdateDateTime],
        [ChangeStatus],
        [QL_StudentID],
        [QL_Disabilty],
        [QL_DisabiltyAllowance],
        [QL_CareLeaver]
      ) VALUES (
        SRC.[RunID],
        SRC.[FullFileName],
        SRC.[SrcSystem],
        SRC.[ObjectName],
        SRC.[RowNo],
        SRC.[FileDateTime],
        SRC.[LSB_ExternalID],
        SRC.[FileRowNumber],
        SRC.[HashMD5],
        GETDATE(), 
        'NEW',
        SRC.[QL_StudentID],
        SRC.[QL_Disabilty],
        SRC.[QL_DisabiltyAllowance],
        SRC.[QL_CareLeaver]
      )
    WHEN NOT MATCHED BY SOURCE
      THEN UPDATE 
      SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';

END;
