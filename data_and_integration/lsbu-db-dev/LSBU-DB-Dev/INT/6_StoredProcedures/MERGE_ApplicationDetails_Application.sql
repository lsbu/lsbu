CREATE OR ALTER PROCEDURE [INT].[MERGE_ApplicationDetails_Application] (
  @RunID INTEGER
)
AS
BEGIN

    MERGE [INT].[SRC_ApplicationDetails_Application] AS TAR
      USING (SELECT * FROM [STG].[tApplicationDetails_Application] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tApplicationDetails_Application] group by LSB_ExternalID having count(*)>1)) AS SRC
        ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
    WHEN MATCHED 
      THEN UPDATE 
         SET 
          [RunID] = SRC.[RunID],
          [FullFileName] = SRC.[FullFileName],
          [SrcSystem] = SRC.[SrcSystem],
          [ObjectName] = SRC.[ObjectName],
          [RowNo] = SRC.[RowNo],
          [FileDateTime] = SRC.[FileDateTime],
          [Term_LSB_ExternalID] = SRC.[Term_LSB_ExternalID],
          [FileRowNumber] = SRC.[FileRowNumber],
          [HashMD5] = SRC.[HashMD5],
          [UpdateDateTime] = GETDATE(),
          [ChangeStatus] = (
            CASE
              WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
              WHEN TAR.HashMD5 = SRC.HashMD5 THEN 'NO CHANGE'
            END
          ),
          [QL_StudentID] = SRC.[QL_StudentID],
          [QL_UCASPersonalID] = SRC.[QL_UCASPersonalID],
          [QL_UCASApplicationCode] = SRC.[QL_UCASApplicationCode],
          [QL_UCASApplicationNumber] = SRC.[QL_UCASApplicationNumber],
          [QL_ApplicationDate] = SRC.[QL_ApplicationDate],
          [QL_AcadProgNameId] = SRC.[QL_AcadProgNameId],
          [QL_IsItPrimaryAcademicProg] = SRC.[QL_IsItPrimaryAcademicProg],
          [QL_ApplicationStatus] = SRC.[QL_ApplicationStatus],
          [QL_ApplicationType] = SRC.[QL_ApplicationType],
          [QL_AcademicPeriod] = SRC.[QL_AcademicPeriod],
          [QL_ProgramStartDate] = SRC.[QL_ProgramStartDate],
          [QL_ProgramEndDate] = SRC.[QL_ProgramEndDate],
          [QL_ModeOfStudy] = SRC.[QL_ModeOfStudy],
          [QL_CreatedDate] = SRC.[QL_CreatedDate],
          [QL_LastModifiedDate] = SRC.[QL_LastModifiedDate],
          [QL_ApplicationSource] = SRC.[QL_ApplicationSource],
          [QL_Term] = SRC.[QL_Term],
          [QL_AOSPeriod] = SRC.[QL_AOSPeriod],
          [QL_DepositStatus] = SRC.[QL_DepositStatus],
          [QL_VisaStatus] = SRC.[QL_VisaStatus],
          [QL_ApplicantAgentCode] = SRC.[QL_ApplicantAgentCode],
          [QL_ApplicantAgencyName] = SRC.[QL_ApplicantAgencyName],
          [QL_PreviousInstituteName] = SRC.[QL_PreviousInstituteName],
          [QL_UCASCourseCode] = SRC.[QL_UCASCourseCode],
          [QL_ApplicantStageIndicator] = SRC.[QL_ApplicantStageIndicator],
          [QL_CriminalConvictions] = SRC.[QL_CriminalConvictions],
          [QL_VisaType] = SRC.[QL_VisaType],
          [QL_NominatedName] = SRC.[QL_NominatedName],
          [QL_NominatedRelationship] = SRC.[QL_NominatedRelationship],
          [QL_AttedenceType] = SRC.[QL_AttedenceType],
          [QL_SessionStartDate] = SRC.[QL_SessionStartDate],
          [QL_SessionEndDate] = SRC.[QL_SessionEndDate],
          [QL_ChoiceNumber] = SRC.[QL_ChoiceNumber],
          [QL_CampusInformation] = SRC.[QL_CampusInformation],
          [QL_AppStatusDateAchieved] = SRC.[QL_AppStatusDateAchieved],
          [QL_QualificationCheck] = SRC.[QL_QualificationCheck],
          [QL_DecisionStatus] = SRC.[QL_DecisionStatus],
          [QL_DecisionNotes] = SRC.[QL_DecisionNotes],
          [QL_DecisionDate] = SRC.[QL_DecisionDate],
          [QL_QualCode] = SRC.[QL_QualCode],
          [QL_UCASStatus] = SRC.[QL_UCASStatus],
          [QL_OfferText] = SRC.[QL_OfferText],
		  [QL_CountryofBirth] = SRC.[QL_CountryofBirth],
		  [QL_FeeCode] = SRC.[QL_FeeCode],
		  [QL_Liveathome] = SRC.[QL_Liveathome],
		  [QL_LateUcasApplicant] = SRC.[QL_LateUcasApplicant],
		  [QL_DSDecisionstatus] = SRC.[QL_DSDecisionstatus],
		  [QL_InternationalFunding] = SRC.[QL_InternationalFunding],
		  [QL_CasNo] = SRC.[QL_CasNo],
		  [QL_CasStatus] = SRC.[QL_CasStatus],
		  [QL_TotalDiscount] = SRC.[QL_TotalDiscount],
		  [QL_VisaRequired] = SRC.[QL_VisaRequired]
  WHEN NOT MATCHED BY TARGET
    THEN INSERT (
      [RunID],
      [FullFileName],
      [SrcSystem],
      [ObjectName],
      [RowNo],
      [FileDateTime],
      [LSB_ExternalID],
      [Term_LSB_ExternalID],
      [FileRowNumber],
      [HashMD5],
      [UpdateDateTime],
      [ChangeStatus],
      [QL_StudentID],
      [QL_UCASPersonalID],
      [QL_UCASApplicationCode],
      [QL_UCASApplicationNumber],
      [QL_ApplicationDate],
      [QL_AcadProgNameId],
      [QL_IsItPrimaryAcademicProg],
      [QL_ApplicationStatus],
      [QL_ApplicationType],
      [QL_AcademicPeriod],
      [QL_ProgramStartDate],
      [QL_ProgramEndDate],
      [QL_ModeOfStudy],
      [QL_CreatedDate],
      [QL_LastModifiedDate],
      [QL_ApplicationSource],
      [QL_Term],
      [QL_AOSPeriod],
      [QL_DepositStatus],
      [QL_VisaStatus],
      [QL_ApplicantAgentCode],
      [QL_ApplicantAgencyName],
      [QL_PreviousInstituteName],
      [QL_UCASCourseCode],
      [QL_ApplicantStageIndicator],
      [QL_CriminalConvictions],
      [QL_VisaType],
      [QL_NominatedName],
      [QL_NominatedRelationship],
      [QL_AttedenceType],
      [QL_SessionStartDate],
      [QL_SessionEndDate],
      [QL_ChoiceNumber],
      [QL_CampusInformation],
      [QL_AppStatusDateAchieved],
      [QL_QualificationCheck],
      [QL_DecisionStatus],
      [QL_DecisionNotes],
      [QL_DecisionDate],
      [QL_QualCode],
      [QL_UCASStatus],
      [QL_OfferText],
	  [QL_CountryofBirth],
	  [QL_FeeCode],
	  [QL_Liveathome],
	  [QL_LateUcasApplicant],
	  [QL_DSDecisionstatus],
	  [QL_InternationalFunding],
	  [QL_CasNo],
	  [QL_CasStatus],
	  [QL_TotalDiscount],
	  [QL_VisaRequired]
    ) VALUES (
      SRC.[RunID],
      SRC.[FullFileName],
      SRC.[SrcSystem],
      SRC.[ObjectName],
      SRC.[RowNo],
      SRC.[FileDateTime],
      SRC.[LSB_ExternalID],
      SRC.[Term_LSB_ExternalID],
      SRC.[FileRowNumber],
      SRC.[HashMD5],
      GETDATE(),
      'NEW',
      SRC.[QL_StudentID],
      SRC.[QL_UCASPersonalID],
      SRC.[QL_UCASApplicationCode],
      SRC.[QL_UCASApplicationNumber],
      SRC.[QL_ApplicationDate],
      SRC.[QL_AcadProgNameId],
      SRC.[QL_IsItPrimaryAcademicProg],
      SRC.[QL_ApplicationStatus],
      SRC.[QL_ApplicationType],
      SRC.[QL_AcademicPeriod],
      SRC.[QL_ProgramStartDate],
      SRC.[QL_ProgramEndDate],
      SRC.[QL_ModeOfStudy],
      SRC.[QL_CreatedDate],
      SRC.[QL_LastModifiedDate],
      SRC.[QL_ApplicationSource],
      SRC.[QL_Term],
      SRC.[QL_AOSPeriod],
      SRC.[QL_DepositStatus],
      SRC.[QL_VisaStatus],
      SRC.[QL_ApplicantAgentCode],
      SRC.[QL_ApplicantAgencyName],
      SRC.[QL_PreviousInstituteName],
      SRC.[QL_UCASCourseCode],
      SRC.[QL_ApplicantStageIndicator],
      SRC.[QL_CriminalConvictions],
      SRC.[QL_VisaType],
      SRC.[QL_NominatedName],
      SRC.[QL_NominatedRelationship],
      SRC.[QL_AttedenceType],
      SRC.[QL_SessionStartDate],
      SRC.[QL_SessionEndDate],
      SRC.[QL_ChoiceNumber],
      SRC.[QL_CampusInformation],
      SRC.[QL_AppStatusDateAchieved],
      SRC.[QL_QualificationCheck],
      SRC.[QL_DecisionStatus],
      SRC.[QL_DecisionNotes],
      SRC.[QL_DecisionDate],
      SRC.[QL_QualCode],
      SRC.[QL_UCASStatus],
      SRC.[QL_OfferText],
	  SRC.[QL_CountryofBirth],
	  SRC.[QL_FeeCode],
	  SRC.[QL_Liveathome],
	  SRC.[QL_LateUcasApplicant],
	  SRC.[QL_DSDecisionstatus],
	  SRC.[QL_InternationalFunding],
	  SRC.[QL_CasNo],
	  SRC.[QL_CasStatus],
	  SRC.[QL_TotalDiscount],
	  SRC.[QL_VisaRequired]
    )
    WHEN NOT MATCHED BY SOURCE
      THEN UPDATE 
      SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;

END;
GO