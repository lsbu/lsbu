CREATE OR ALTER PROCEDURE [INT].[MERGE_AllStaff_CaseTeamMember] (
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_AllStaff_CaseTeamMember] AS TAR
				USING  (SELECT * FROM  STG.tAllStaff_CaseTeamMember CTM) AS SRC
				ON (TAR.StudentId = SRC.StudentId AND TAR.FederationIdentifier = SRC.FederationIdentifier AND TAR.[CourseTutor] = SRC.[CourseTutor] AND TAR.[PersonalTutor] = SRC.[PersonalTutor] AND  TAR.[ModuleLeader] = SRC.[ModuleLeader]) 
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 'NO CHANGE'
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([StudentId],
						[FederationIdentifier],
						[CourseTutor],
						[PersonalTutor],
						[ModuleLeader],
						[InsetDateTime],
						[UpdateDateTime],
						[ChangeStatus])
		VALUES (SRC.[StudentId],
				SRC.[FederationIdentifier],
				SRC.[CourseTutor],
				SRC.[PersonalTutor],
				SRC.[ModuleLeader],
				GETDATE(),
				GETDATE(),
				'NEW')
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;
END;
