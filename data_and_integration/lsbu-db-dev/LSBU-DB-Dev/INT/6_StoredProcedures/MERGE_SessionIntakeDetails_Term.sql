CREATE OR ALTER PROCEDURE [INT].[MERGE_SessionIntakeDetails_Term](
			@RunID VARCHAR(100)
			)
AS

BEGIN

		MERGE [INT].SRC_SessionIntakeDetails_Term AS TAR
				USING (SELECT * FROM [STG].[tSessionIntakeDetails_Term] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tSessionIntakeDetails_Term] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_AcadProgNameId] = SRC.[QL_AcadProgNameId],
				[QL_AOSPeriod] = SRC.[QL_AOSPeriod],
				[QL_Term] = SRC.[QL_Term],
				[QL_AcademicPeriod] = SRC.[QL_AcademicPeriod],
				[QL_SessionStartDate] = SRC.[QL_SessionStartDate],
				[QL_SessionEndDate] = SRC.[QL_SessionEndDate],
				[QL_TermName] = SRC.[QL_TermName],
				[QL_TermType] = SRC.[QL_TermType],
				[TermRank] = SRC.[TermRank]
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
				[RunID],
				[FullFileName],
				[SrcSystem],
				[ObjectName],
				[RowNo],
				[FileDateTime],
				[LSB_ExternalID],
				[FileRowNumber],
				[HashMD5],
				[UpdateDateTime] ,
				[ChangeStatus] ,
				[QL_AcadProgNameId],
				[QL_AOSPeriod],
				[QL_Term],
				[QL_AcademicPeriod],
				[QL_SessionStartDate],
				[QL_SessionEndDate],
				[QL_TermName],
				[QL_TermType],
				[TermRank]
					)
		VALUES (
				SRC.[RunID],
				SRC.[FullFileName],
				SRC.[SrcSystem],
				SRC.[ObjectName],
				SRC.[RowNo],
				SRC.[FileDateTime],
				SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],
				SRC.[HashMD5],
				 GETDATE(), 
				 'NEW',
				SRC.[QL_AcadProgNameId],
				SRC.[QL_AOSPeriod],
				SRC.[QL_Term],
				SRC.[QL_AcademicPeriod],
				SRC.[QL_SessionStartDate],
				SRC.[QL_SessionEndDate],
				SRC.[QL_TermName],
				SRC.[QL_TermType],
				SRC.[TermRank]
				)
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;
END;

GO