CREATE OR ALTER PROCEDURE [INT].[MERGE_ApplicationDetails_Term](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].[SRC_ApplicationDetails_Term] AS TAR
				USING [SRC].[vApplicationDetails_Term] AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_AcadProgNameId]=SRC.[QL_AcadProgNameId],
				[QL_AcademicPeriod]=SRC.[QL_AcademicPeriod],
				[QL_Term]=SRC.[QL_Term],
				[QL_TermName] = SRC.[QL_TermName],
				[QL_AOSPeriod]=SRC.[QL_AOSPeriod],
				[QL_SessionStartDate]=SRC.[QL_SessionStartDate],
				[QL_SessionEndDate]=SRC.[QL_SessionEndDate],
				[QL_TermType]=SRC.[QL_TermType]
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
				[RunID],
				[FullFileName],
				[SrcSystem],
				[ObjectName],
				[RowNo],
				[FileDateTime],
				[LSB_ExternalID],
				[FileRowNumber],
				[HashMD5],
				[UpdateDateTime] ,
				[ChangeStatus] ,
				[QL_AcadProgNameId],
				[QL_AcademicPeriod],
				[QL_Term],
				[QL_TermName],
				[QL_AOSPeriod],
				[QL_SessionStartDate],
				[QL_SessionEndDate],
				[QL_TermType]

					)
		VALUES (
				SRC.[RunID],
				SRC.[FullFileName],
				SRC.[SrcSystem],
				SRC.[ObjectName],
				SRC.[RowNo],
				SRC.[FileDateTime],
				SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],
				SRC.[HashMD5],
				 GETDATE(), 
				 'NEW',
				SRC.[QL_AcadProgNameId],
				SRC.[QL_AcademicPeriod],
				SRC.[QL_Term],
				SRC.[QL_TermName],
				SRC.[QL_AOSPeriod],
				SRC.[QL_SessionStartDate],
				SRC.[QL_SessionEndDate],
				SRC.[QL_TermType]

				)
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET [UpdateDateTime] = GETDATE(), [ChangeStatus] = 'DELETE'  ;


END;

GO