CREATE OR ALTER PROCEDURE [INT].[Connect_PopulateData]
AS
BEGIN
	
	DROP TABLE IF EXISTS INT.SRC_Connect_Salesforce_Migration

	/*INSERT INTO INT.SRC_Connect_Salesforce_Migration
		([Address_Line_1]
      ,[Address_Line_2]
      ,[City]
      ,[Country]
      ,[Current_Source_Code]
      ,[DOB]
      ,[E_mail]
      ,[First_Name]
      ,[Gender]
      ,[High_School_Address1]
      ,[High_School_Address2]
      ,[High_School_City]
      ,[High_School_Code]
      ,[High_School_Name]
      ,[High_School_State]
      ,[High_School_Zip]
      ,[Connect_ID]
      ,[QL_Student_ID]
      ,[Last_Name]
      ,[Middle_Name]
      ,[Nationality]
      ,[Phone]
      ,[Company_Name]
      ,[County]
      ,[Division_1]
      ,[Division_2]
      ,[Emergency_Contact_Name]
	  ,[Emergency_Contact_FirstName]
	  ,[Emergency_Contact_LastName]
      ,[Emergency_Contact_Number]
      ,[Emergency_Contact_Relationship]
      ,[Employer_Sponsored_Study]
      ,[Employer_Employee]
      ,[Gecko___Prospectus_Format]
      ,[Gecko___Prospectus_Request]
      ,[Gecko_ID]
      ,[Gecko_Response_ID]
      ,[Hear_about_us_]
      ,[Home_Overseas]
      ,[International_Dial_Code]
      ,[Mobile_Number]
      ,[Nature_of_Enquiry]
      ,[Postcode]
      ,[Posted_Prospectus]
      ,[Printed_prospectus]
      ,[School_1]
      ,[School_2]
      ,[School_College_Name]
      ,[Skype_Name]
      ,[Specify_if__other_]
      ,[Subject_Area_1]
      ,[Subject_Area_2]
      ,[Title]
      ,[Which_year_are_you_considering_University_entry_]
      ,[your_current_school_college_university_]
      ,[Your_LSBU_student_ID_if_applicable]
      ,[Your_UCAS_No_if_applicable]
      ,[last_Clearing_Interest_form_submission]
      ,[last_Contact_Us_form_submission]
      ,[last_Croydon_Interest_form_submission]
      ,[last_Gecko_Form_submission]
      ,[last_Get_In_Touch_Interest_form_submission]
      ,[last_Outreach_and_Recruitment_form_submission]
      ,[last_Prospectus_Interest_form_submission]
      ,[last_Register_Your_Interest_form_submission]
      ,[Special_requirements]
      ,[Consent_Channels]
      ,[Consent_Content]
      ,[Consent_Oral_Statement]
      ,[Int_Country_Of_Residence]
      ,[Int_Event_Country]
      ,[Int_Event_Type]
      ,[Int_FURTHERINFORMATION]
      ,[Int_HEARABOUTUS_]
      ,[Int_HEARABOUTUSOTHER]
      ,[Int_INTAKEMONTH]
      ,[Int_RESIDENTIALCOUNTRY]
      ,[Are_you_considering_Apprenticeship_]
      ,[Are_you_in_Clearing_]
      ,[GA___Campaign_Content]
      ,[GA___Campaign_Medium]
      ,[GA___Campaign_Name]
      ,[GA___Campaign_Source]
      ,[GA___Campaign_Term]
      ,[LSBU_Group_Institute]
      ,[Registration_Pathway]
      ,[Already_have_all_your_qualifications_]
      ,[Applied_To_LSBU]
      ,[Campus_Interested_In]
      ,[Course_Name]
      ,[Criminal_Conviction_Questionnaire]
      ,[Current_level_of_education]
      ,[Gecko_Form_Entry]
      ,[Gecko_Form_Submission_Log]
      ,[Method_Engagement___Events]
      ,[One_to_One_Discussion_List]
      ,[One_to_One_Discussion_Textbox]
      ,[Communication_Status_Tag]
      ,[Contact_Status_Tag]
      ,[Engagement_Status_Tag]
      ,[Event_Status_Tag]
      ,[Ask_a_question]
      ,[Contact_Preference_Center__Channel]
      ,[Contact_Preference_Center__Topic]
      ,[Created_Date]
      ,[Gecko_Events_Hub_URL]
      ,[E_mail_Opt_In]
      ,[Phone_Opt_In]
      ,[SMS_Opt_In]
      ,[Post_Opt_In]
      ,[Subscription__The_Tea_Newsletter])*/

	SELECT [Address_Line_1]
      ,[Address_Line_2]
      ,[City]
      ,[Country]
      ,[Current_Source_Code]
      ,CASE
			WHEN TRY_CONVERT(date,[DOB]) > GETDATE() THEN NULL
			WHEN TRY_CONVERT(date,[DOB]) < '1900-01-01' THEN NULL
			ELSE DOB
		END as DOB
      ,CAST(CASE 
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'gmail' THEN REPLACE(SM.E_mail, 'gmail', 'gmail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'yahoo' THEN REPLACE(SM.E_mail, 'yahoo', 'yahoo.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'outlookcom' THEN REPLACE(SM.E_mail, 'outlookcom', 'outlook.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'dg' THEN REPLACE(SM.E_mail, 'dg', 'dg.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'outlook' THEN REPLACE(SM.E_mail, 'outlook', 'outlook.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'hotmail' THEN REPLACE(SM.E_mail, 'hotmail', 'hotmail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'aolcom' THEN REPLACE(SM.E_mail, 'aolcom', 'aol.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'hotm' THEN REPLACE(SM.E_mail, 'hotm', 'hot.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'hot' THEN REPLACE(SM.E_mail, 'hot', 'hot.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'ymail' THEN REPLACE(SM.E_mail, 'ymail', 'ymail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'hotmailcom' THEN REPLACE(SM.E_mail, 'hotmailcom', 'hotmail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'outlook' THEN REPLACE(SM.E_mail, 'outlook', 'outlook.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'btinternet' THEN REPLACE(SM.E_mail, 'btinternet', 'btinternet.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'yahol' THEN REPLACE(SM.E_mail, 'yahol', 'yahol.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'talktalk' THEN REPLACE(SM.E_mail, 'talktalk', 'talktalk.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'gmailcom' THEN REPLACE(SM.E_mail, 'gmailcom', 'gmail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'icloud' THEN REPLACE(SM.E_mail, 'icloud', 'icloud.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'rocketmail' THEN REPLACE(SM.E_mail, 'rocketmail', 'rocketmail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'janice' THEN REPLACE(SM.E_mail, 'janice', 'janice.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'live' THEN REPLACE(SM.E_mail, 'live', 'live.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'skhs' THEN REPLACE(SM.E_mail, 'skhs', 'skhs.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'ribstonhall' THEN REPLACE(SM.E_mail, 'ribstonhall', 'ribstonhall.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'oan6th' THEN REPLACE(SM.E_mail, 'oan6th', 'oan6th.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'aloha-college' THEN REPLACE(SM.E_mail, 'aloha-college', 'aloha-college.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'psychotherapy' THEN REPLACE(SM.E_mail, 'psychotherapy', 'psychotherapy.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'liveit' THEN REPLACE(SM.E_mail, 'liveit', 'liveit.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'yahoocom' THEN REPLACE(SM.E_mail, 'yahoocom', 'yahoo.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'goldsworthyjoanne928' THEN REPLACE(SM.E_mail, 'goldsworthyjoanne928', 'goldsworthyjoanne928.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'gmial' THEN REPLACE(SM.E_mail, 'gmial', 'gmail.com')
        WHEN SUBSTRING(SM.E_mail, (LEN(SM.E_mail)- CHARINDEX('@', REVERSE(SM.E_mail))+2),255) = 'abv' THEN REPLACE(SM.E_mail, 'abv', 'abv.com')
        ELSE SM.E_mail 
       END as varchar(255)) AS [E_mail]
      ,
	  case when [Last_Name] is null and charindex(' ',SM.First_Name)>0 
	  then  
			  CASE 
			   WHEN SM.Middle_Name IS NOT NULL THEN CONCAT( substring(SM.First_Name,0,charindex(' ',SM.First_Name)),' ',SM.Middle_Name)
			   ELSE substring(SM.First_Name,0,charindex(' ',SM.First_Name))
			  END

	else 
	  CASE 
			   WHEN SM.Middle_Name IS NOT NULL THEN CONCAT(  SM.First_Name,' ',SM.Middle_Name)
			   ELSE SM.First_Name
			  END
	end 
		AS [First_Name]
      ,CAST(CASE SM.Gender
       WHEN 'F' THEN 'Female' 
       WHEN 'M' THEN 'Male' ELSE 'Other' end as varchar(255)) AS [Gender]
      ,[High_School_Address1]
      ,[High_School_Address2]
      ,[High_School_City]
      ,[High_School_Code]
      ,[High_School_Name]
      ,[High_School_State]
      ,[High_School_Zip]
      ,[Connect_ID]
      ,[QL_Student_ID]
      ,RTRIM(LTRIM(case 
				when [Last_Name] is null and charindex(' ',SM.First_Name)>0 
				then substring(SM.First_Name,charindex(' ',SM.First_Name)+1,100)
				when [Last_Name] is null 
				then coalesce(SM.First_Name,'N/A')
				else [Last_Name] end )) as [Last_Name]
      ,[Middle_Name]
      ,[Nationality]
      ,[Phone]
      ,[Company_Name]
      ,[County]
      ,[Division_1]
      ,[Division_2]
      ,[Emergency_Contact_Name]
	  ,rtrim(ltrim(case (len(SM.Emergency_Contact_Name)-len(replace(SM.Emergency_Contact_Name,' ','')))
		 when 1 then 
			substring(SM.Emergency_Contact_Name, 0, charindex(' ',SM.Emergency_Contact_Name))
		 when 2 then ( case when SM.Emergency_Contact_Name like '%dr%' then substring(replace(SM.Emergency_Contact_Name,'dr ',''),0, charindex(' ',replace(SM.Emergency_Contact_Name,'dr ','')) )
						when SM.Emergency_Contact_Name  not like '%+%' and isnumeric(replace(SM.Emergency_Contact_Name,' ',''))=0 then substring(SM.Emergency_Contact_Name, 0, charindex(' ',SM.Emergency_Contact_Name))
						else null end
						)
		 when 0 then ( case when SM.Emergency_Contact_Name  not like '%+%'
							and SM.Emergency_Contact_Name  not like '%xx%'
						and isnumeric(replace(SM.Emergency_Contact_Name,' ',''))=0 then SM.Emergency_Contact_Name
						else null end
						)

			else null end))  as [Emergency_Contact_FirstName]
	  ,rtrim(ltrim(
		case (len(SM.Emergency_Contact_Name)-len(replace(SM.Emergency_Contact_Name,' ','')))
		 when 1 then 
			substring(SM.Emergency_Contact_Name,  charindex(' ',SM.Emergency_Contact_Name) , 100)
		 when 2 then ( case when SM.Emergency_Contact_Name like '%dr%' then substring(replace(SM.Emergency_Contact_Name,'dr ',''), charindex(' ',replace(SM.Emergency_Contact_Name,'dr ','')) ,100)
						when SM.Emergency_Contact_Name  not like '%+%' and isnumeric(replace(SM.Emergency_Contact_Name,' ',''))=0 then substring(SM.Emergency_Contact_Name,  charindex(' ',SM.Emergency_Contact_Name),100)
						else null end
						)
		 when 0 then Emergency_Contact_Name
		else Emergency_Contact_Name end))  AS [Emergency_Contact_LastName]
      ,[Emergency_Contact_Number]
      ,[Emergency_Contact_Relationship]
      ,[Employer_Sponsored_Study]
      ,[Employer_Employee]
      ,[Gecko___Prospectus_Format]
      ,[Gecko___Prospectus_Request]
      ,[Gecko_ID]
      ,[Gecko_Response_ID]
      ,[Hear_about_us_]
      ,[Home_Overseas]
      ,[International_Dial_Code]
      ,CAST(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		SM.Mobile_Number
		, '+', '')
		, '(', '')
		, ')', '')
		, '-', '')
		, ' ', '')
		, char(160), '') as varchar(40)) AS [Mobile_Number]
	  ,[Nature_of_Enquiry]
      ,[Postcode]
      ,[Posted_Prospectus]
      ,[Printed_prospectus]
      ,[School_1]
      ,[School_2]
      ,[School_College_Name]
      ,[Skype_Name]
      ,[Specify_if__other_]
      ,[Subject_Area_1]
      ,[Subject_Area_2]
      ,[Title]
      ,[Which_year_are_you_considering_University_entry_]
      ,[your_current_school_college_university_]
      ,[Your_LSBU_student_ID_if_applicable]
      ,[Your_UCAS_No_if_applicable]
      ,[last_Clearing_Interest_form_submission]
      ,[last_Contact_Us_form_submission]
      ,[last_Croydon_Interest_form_submission]
      ,[last_Gecko_Form_submission]
      ,[last_Get_In_Touch_Interest_form_submission]
      ,[last_Outreach_and_Recruitment_form_submission]
      ,[last_Prospectus_Interest_form_submission]
      ,[last_Register_Your_Interest_form_submission]
      ,[Special_requirements]
      ,[Consent_Channels]
      ,[Consent_Content]
      ,[Consent_Oral_Statement]
      ,[Int_Country_Of_Residence]
      ,[Int_Event_Country]
      ,[Int_Event_Type]
      ,[Int_FURTHERINFORMATION]
      ,[Int_HEARABOUTUS_]
      ,[Int_HEARABOUTUSOTHER]
      ,[Int_INTAKEMONTH]
      ,[Int_RESIDENTIALCOUNTRY]
      ,[Are_you_considering_Apprenticeship_]
      ,[Are_you_in_Clearing_]
      ,[GA___Campaign_Content]
      ,[GA___Campaign_Medium]
      ,[GA___Campaign_Name]
      ,[GA___Campaign_Source]
      ,[GA___Campaign_Term]
      ,[LSBU_Group_Institute]
      ,[Registration_Pathway]
      ,[Already_have_all_your_qualifications_]
      ,[Applied_To_LSBU]
      ,[Campus_Interested_In]
      ,[Course_Name]
      ,[Criminal_Conviction_Questionnaire]
      ,[Current_level_of_education]
      ,[Gecko_Form_Entry]
      ,[Gecko_Form_Submission_Log]
      ,[Method_Engagement___Events]
      ,[One_to_One_Discussion_List]
      ,[One_to_One_Discussion_Textbox]
      ,[Communication_Status_Tag]
      ,[Contact_Status_Tag]
      ,[Engagement_Status_Tag]
      ,[Event_Status_Tag]
      ,[Ask_a_question]
      ,[Contact_Preference_Center__Channel]
      ,[Contact_Preference_Center__Topic]
      ,[Created_Date]
      ,[Gecko_Events_Hub_URL]
      ,[E_mail_Opt_In]
      ,[Phone_Opt_In]
      ,[SMS_Opt_In]
      ,[Post_Opt_In]
      ,[Subscription__The_Tea_Newsletter]

	INTO INT.SRC_Connect_Salesforce_Migration
  FROM [SRC].[Salesforce_Migration] SM

  update   INT.SRC_Connect_Salesforce_Migration
  set First_Name='Alz Jennifer Abike Abiodun Abimbola'
where connect_id='897809'

  update  INT.SRC_Connect_Salesforce_Migration
  set First_Name=left(First_Name,40)
where len(First_Name)>40



END
