
CREATE OR ALTER     PROCEDURE [INT].[MERGE_StudentTeachingStaff_Contact](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_StudentTeachingStaff_Contact AS TAR

				USING (SELECT * FROM [STG].[tStudentTeachingStaff_Contact] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tStudentTeachingStaff_Contact] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[Account_LSB_ExternalID]=src.[Account_LSB_ExternalID],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[CMIS_StudentID]=SRC.[CMIS_StudentID],
        [CMIS_AcadProgNameId]=SRC.[CMIS_AcadProgNameId],
        [CMIS_AcademicPeriod]=SRC.[CMIS_AcademicPeriod],
        [CMIS_AOSPeriod]=SRC.[CMIS_AOSPeriod],
        [CMIS_ModuleId]=SRC.[CMIS_ModuleId],
        [CMIS_ModuleInstance]=SRC.[CMIS_ModuleInstance],
        [CMIS_Semester]=SRC.[CMIS_Semester],
        [CMIS_TeachingStaffId]=SRC.[CMIS_TeachingStaffId],
        [CMIS_TeachingStaffFirstName]=SRC.[CMIS_TeachingStaffFirstName],
        [CMIS_TeachingStaffLastname]=SRC.[CMIS_TeachingStaffLastname],
        [CMIS_TeachingStaffEmail]=SRC.[CMIS_TeachingStaffEmail]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
		[Account_LSB_ExternalID],
				[FileRowNumber],
        [HashMD5],
        [InsetDateTime],
        [ChangeStatus],
        [CMIS_StudentID],
        [CMIS_AcadProgNameId],
        [CMIS_AcademicPeriod],
        [CMIS_AOSPeriod],
        [CMIS_ModuleId],
        [CMIS_ModuleInstance],
        [CMIS_Semester],
        [CMIS_TeachingStaffId],
        [CMIS_TeachingStaffFirstName],
        [CMIS_TeachingStaffLastname],
        [CMIS_TeachingStaffEmail])
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
	SRC.[Account_LSB_ExternalID],
		SRC.[FileRowNumber],
    SRC.[HashMD5],
    GETDATE(),
    'NEW',
    SRC.[CMIS_StudentID],
    SRC.[CMIS_AcadProgNameId],
    SRC.[CMIS_AcademicPeriod],
    SRC.[CMIS_AOSPeriod],
    SRC.[CMIS_ModuleId],
    SRC.[CMIS_ModuleInstance],
    SRC.[CMIS_Semester],
    SRC.[CMIS_TeachingStaffId],
    SRC.[CMIS_TeachingStaffFirstName],
    SRC.[CMIS_TeachingStaffLastname],
    SRC.[CMIS_TeachingStaffEmail]
)
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;

