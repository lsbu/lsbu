CREATE OR ALTER PROCEDURE [INT].[MERGE_CourseConnection](
			@RunID INTEGER
			)
AS

BEGIN

		MERGE [INT].SRC_CourseConnection AS TAR
				USING (SELECT * FROM [SRC].[vCourseConnection] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [SRC].[vCourseConnection] group by LSB_ExternalID having count(*)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_CourseConnectionID]=SRC.[QL_CourseConnectionID]
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
				[RunID],
				[FullFileName],
				[SrcSystem],
				[ObjectName],
				[RowNo],
				[FileDateTime],
				[LSB_ExternalID],
				[FileRowNumber],
				[HashMD5],
				[UpdateDateTime] ,
				[ChangeStatus] ,
				[QL_CourseConnectionID]
				)
		VALUES (
				SRC.[RunID],
				SRC.[FullFileName],
				SRC.[SrcSystem],
				SRC.[ObjectName],
				SRC.[RowNo],
				SRC.[FileDateTime],
				SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],
				SRC.[HashMD5],
				 GETDATE(), 
				 'NEW',
				SRC.[QL_CourseConnectionID])
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE'  ;
END;
GO