CREATE OR ALTER PROCEDURE INT.Maximizer_IndividualDetailList_PopulateData
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE INT.SRC_Maximizer_PIVOT_IndividualDetailList

	INSERT INTO INT.SRC_Maximizer_PIVOT_IndividualDetailList
		([Id],
		[MH__WB_MHWB_Archive_Status],
		[ARCHIVED_FIELDS_PEEPs_2010_PEEP_Prepared_by],
		[Level_of_Study],
		[MH__WB_In_Halls],
		[DDS_DSA_Study_Skills_Tuition_Tutor_Supplier],
		[ARCHIVED_FIELDS_Old_fields_Scribe_recommended],
		[DDS_DSA_Study_Skills_Tuition_Dyslexia_tutor_recommended],
		[ARCHIVED_FIELDS_Old_fields_BSL_interpreter_recommended],
		[DDS_PRE_ENTRY_Allocated_to_DA],
		[DDS_Support_Arrangements_QL_Disability_Fields_DDS_Support],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Refunded],
		[DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_appointment_adviser],
		[ARCHIVED_FIELDS_PEEPs_2010_If_Yes_which_one],
		[ARCHIVED_FIELDS_Old_fields_Library_assistant_recommended],
		[ARCHIVED_FIELDS_Old_fields_Teacher_for_the_Deaf_recommended],
		[ARCHIVED_FIELDS_Old_fields_Lab_assistant_recommended],
		[ARCHIVED_FIELDS_Support_Arrangements__Sympathetic_Marking],
		[ARCHIVED_FIELDS_Old_fields_Campus_support_recommended],
		[DDS_DSA_Mentoring_Support_Mentoring_Supplier],
		[DDS_Support_Arrangements_DAs_initials],
		[DDS_Screening_and_Ed_Psych_Assessment_Day_of_the_week_of_EP],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_paid],
		[ARCHIVED_FIELDS_Disability_Category],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_form_completed],
		[ARCHIVED_FIELDS_Old_fields_Note_taker_recommeded],
		[DDS_DSA_Mentoring_Support_Mentor_recommended],
		[DDS_Student_Record_Status],
		[ARCHIVED_FIELDS_PEEP_Not_required__please_tick_one_],
		[DDS_Screening_and_Ed_Psych_Assessment_Feedback_location],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_Money_received],
		[MH__WB_MHWB_Archive_MHWB_Status],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Assessment_funded_by],
		[DDS_Screening_and_Ed_Psych_Assessment_EP_Cost],
		[DDS_PEEPs_Student_requires_a_PEEP___referred_to_HS])
	SELECT
		[Id],
		[MH__WB_MHWB_Archive_Status],
		[ARCHIVED_FIELDS_PEEPs_2010_PEEP_Prepared_by],
		[Level_of_Study],
		[MH__WB_In_Halls],
		[DDS_DSA_Study_Skills_Tuition_Tutor_Supplier],
		[ARCHIVED_FIELDS_Old_fields_Scribe_recommended],
		[DDS_DSA_Study_Skills_Tuition_Dyslexia_tutor_recommended],
		[ARCHIVED_FIELDS_Old_fields_BSL_interpreter_recommended],
		[DDS_PRE_ENTRY_Allocated_to_DA],
		[DDS_Support_Arrangements_QL_Disability_Fields_DDS_Support],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Refunded],
		[DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_appointment_adviser],
		[ARCHIVED_FIELDS_PEEPs_2010_If_Yes_which_one],
		[ARCHIVED_FIELDS_Old_fields_Library_assistant_recommended],
		[ARCHIVED_FIELDS_Old_fields_Teacher_for_the_Deaf_recommended],
		[ARCHIVED_FIELDS_Old_fields_Lab_assistant_recommended],
		[ARCHIVED_FIELDS_Support_Arrangements__Sympathetic_Marking],
		[ARCHIVED_FIELDS_Old_fields_Campus_support_recommended],
		[DDS_DSA_Mentoring_Support_Mentoring_Supplier],
		[DDS_Support_Arrangements_DAs_initials],
		[DDS_Screening_and_Ed_Psych_Assessment_Day_of_the_week_of_EP],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_paid],
		[ARCHIVED_FIELDS_Disability_Category],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_form_completed],
		[ARCHIVED_FIELDS_Old_fields_Note_taker_recommeded],
		[DDS_DSA_Mentoring_Support_Mentor_recommended],
		[DDS_Student_Record_Status],
		[ARCHIVED_FIELDS_PEEP_Not_required__please_tick_one_],
		[DDS_Screening_and_Ed_Psych_Assessment_Feedback_location],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_Money_received],
		[MH__WB_MHWB_Archive_MHWB_Status],
		[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Assessment_funded_by],
		[DDS_Screening_and_Ed_Psych_Assessment_EP_Cost],
		[DDS_PEEPs_Student_requires_a_PEEP___referred_to_HS]
	FROM SRC.Maximizer_PIVOT_IndividualDetailList

END
GO