CREATE OR ALTER PROCEDURE [INT].[MERGE_EnrolmentDetails] (
    @RunID INTEGER
)
AS

BEGIN

  MERGE [INT].SRC_EnrolmentDetails AS TAR
    USING (SELECT * FROM [STG].[tEnrolmentDetails] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tEnrolmentDetails] group by LSB_ExternalID having count(*)>1)) AS SRC
      ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
    WHEN MATCHED
      THEN UPDATE SET 
        [RunID] = SRC.[RunID],
        [FullFileName] = SRC.[FullFileName],
        [SrcSystem] = SRC.[SrcSystem],
        [ObjectName] = SRC.[ObjectName],
        [RowNo] = SRC.[RowNo],
        [FileDateTime] = SRC.[FileDateTime],
        [Application_LSB_ExternalID] = SRC.[Application_LSB_ExternalID],
	    	[Affiliation_LSB_ExternalID] = SRC.[Affiliation_LSB_ExternalID],
        [Term_LSB_ExternalID] = SRC.[Term_LSB_ExternalID],
        [FileRowNumber] = SRC.[FileRowNumber],
        [HashMD5] = SRC.[HashMD5],
        [UpdateDateTime] = GETDATE(), 
        [ChangeStatus] = (
          CASE
            WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
            WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
          END),
        [QL_StudentID] = SRC.[QL_StudentID],
        [QL_UCASPersonalID] = SRC.[QL_UCASPersonalID],
        [QL_AcadProgNameId] = SRC.[QL_AcadProgNameId],
        [QL_Term] = SRC.[QL_Term],
        [QL_AOSPeriod] = SRC.[QL_AOSPeriod],
        [QL_AcademicPeriod] = SRC.[QL_AcademicPeriod],
        [QL_ProgramStartDate] = SRC.[QL_ProgramStartDate],
        [QL_ProgramEndDate] = SRC.[QL_ProgramEndDate],
        [QL_ModeOfStudy] = SRC.[QL_ModeOfStudy],
        [QL_EnrolmentStageIndicator] = SRC.[QL_EnrolmentStageIndicator],
        [QL_EnrollmentStatus] = SRC.[QL_EnrollmentStatus],
        [QL_EnrolStatusDateAchieved] = SRC.[QL_EnrolStatusDateAchieved],
        [QL_AttedenceType] = SRC.[QL_AttedenceType],
        [QL_SessionStartDate] = SRC.[QL_SessionStartDate],
        [QL_SessionEndDate] = SRC.[QL_SessionEndDate],
        [QL_CampusInformation] = SRC.[QL_CampusInformation],
        [QL_CreatedDate] = SRC.[QL_CreatedDate],
        [QL_LastModifiedDate] = SRC.[QL_LastModifiedDate],
	    [QL_StudentGraduated] = SRC.[QL_StudentGraduated],
		[QL_StudentGraduatingFlag] = SRC.[QL_StudentGraduatingFlag]
    WHEN NOT MATCHED BY TARGET
      THEN INSERT (
        [RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
        [Application_LSB_ExternalID],
		    [Affiliation_LSB_ExternalID],
        [Term_LSB_ExternalID],
        [FileRowNumber],
        [HashMD5],
        [UpdateDateTime] ,
        [ChangeStatus] ,
        [QL_StudentID],
        [QL_UCASPersonalID],
        [QL_AcadProgNameId],
        [QL_Term],
        [QL_AOSPeriod],
        [QL_AcademicPeriod],
        [QL_ProgramStartDate],
        [QL_ProgramEndDate],
        [QL_ModeOfStudy],
        [QL_EnrolmentStageIndicator],
        [QL_EnrollmentStatus],
        [QL_EnrolStatusDateAchieved],
        [QL_AttedenceType],
        [QL_SessionStartDate],
        [QL_SessionEndDate],
        [QL_CampusInformation],
        [QL_CreatedDate],
        [QL_LastModifiedDate],
		[QL_StudentGraduated],
		[QL_StudentGraduatingFlag]
      ) VALUES (
        SRC.[RunID],
        SRC.[FullFileName],
        SRC.[SrcSystem],
        SRC.[ObjectName],
        SRC.[RowNo],
        SRC.[FileDateTime],
        SRC.[LSB_ExternalID],
        SRC.[Application_LSB_ExternalID],
	    	SRC.[Affiliation_LSB_ExternalID],
        SRC.[Term_LSB_ExternalID],
        SRC.[FileRowNumber],
        SRC.[HashMD5],
        GETDATE(), 
        'NEW',
        SRC.[QL_StudentID],
        SRC.[QL_UCASPersonalID],
        SRC.[QL_AcadProgNameId],
        SRC.[QL_Term],
        SRC.[QL_AOSPeriod],
        SRC.[QL_AcademicPeriod],
        SRC.[QL_ProgramStartDate],
        SRC.[QL_ProgramEndDate],
        SRC.[QL_ModeOfStudy],
        SRC.[QL_EnrolmentStageIndicator],
        SRC.[QL_EnrollmentStatus],
        SRC.[QL_EnrolStatusDateAchieved],
        SRC.[QL_AttedenceType],
        SRC.[QL_SessionStartDate],
        SRC.[QL_SessionEndDate],
        SRC.[QL_CampusInformation],
        SRC.[QL_CreatedDate],
        SRC.[QL_LastModifiedDate],
		SRC.[QL_StudentGraduated],
		SRC.[QL_StudentGraduatingFlag]
      )
    WHEN NOT MATCHED BY SOURCE
      THEN UPDATE 
      SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';

END;