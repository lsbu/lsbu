CREATE OR ALTER PROCEDURE [INT].[MERGE_ApplicationEnrolmentDetails_Affiliation] (
  @RunID INTEGER
)
AS
BEGIN

  MERGE [INT].SRC_ApplicationEnrolmentDetails_Affiliation AS TAR
    USING (
      SELECT *
      FROM [STG].[tApplicationEnrolmentDetails_Affiliation] t
      WHERE t.LSB_ExternalID NOT IN (
          SELECT LSB_ExternalID
          FROM [STG].[tApplicationEnrolmentDetails_Affiliation]
          GROUP BY LSB_ExternalID
          HAVING COUNT(1) > 1
        )
    ) AS SRC
      ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
  WHEN MATCHED
    THEN UPDATE SET 
      [RunID] = SRC.[RunID],
      [FullFileName] = SRC.[FullFileName],
      [SrcSystem] = SRC.[SrcSystem],
      [ObjectName] = SRC.[ObjectName],
      [RowNo] = SRC.[RowNo],
      [FileDateTime] = SRC.[FileDateTime],
      [FileRowNumber] = SRC.[FileRowNumber],
      [HashMD5] = SRC.[HashMD5],
      [UpdateDateTime] = GETDATE(), 
      [ChangeStatus] = (
        CASE
          WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'      -- The record is updated only based on certain Applicant Status or Enrolment Status values.
          WHEN TAR.HashMD5 = SRC.HashMD5 THEN 'NO CHANGE'
        END
      ),
      [QL_StudentID] = SRC.[QL_StudentID],
      [QL_ApplicationDate] = SRC.[QL_ApplicationDate],
      [QL_AcadProgNameId] = SRC.[QL_AcadProgNameId],
      [QL_TypeofCourse] = SRC.[QL_TypeofCourse],
      [QL_DepartmentId] = SRC.[QL_DepartmentId],
      [QL_EducationInstitutionId] = SRC.[QL_EducationInstitutionId],
      [QL_ApplicationStatus] = SRC.[QL_ApplicationStatus],
      [QL_AOSPeriod] = SRC.[QL_AOSPeriod],
      [QL_AcademicPeriod] = SRC.[QL_AcademicPeriod],
      [QL_EnrollmentStatus] = SRC.[QL_EnrollmentStatus],
      [QL_ApplicantStageIndicator] = SRC.[QL_ApplicantStageIndicator],
      [QL_EnrolmentStageIndicator] = SRC.[QL_EnrolmentStageIndicator],
      [QL_AppStatusDateAchieved] = SRC.[QL_AppStatusDateAchieved],
      [QL_EnrolStatusDateAchieved] = SRC.[QL_EnrolStatusDateAchieved],
      [QL_StudentGraduated] = SRC.[QL_StudentGraduated],
      [QL_StudentEnrolledDate] = SRC.[QL_StudentEnrolledDate],
      [QLCALC_AffiliationStartDate] = SRC.[QLCALC_AffiliationStartDate],
      [QLCALC_AffiliationEndDate] =  SRC.[QLCALC_AffiliationEndDate],
      [QLCALC_AffiliationStatus] = SRC.[QLCALC_AffiliationStatus],
      [QLCALC_AffiliationPrimaryFlag] = SRC.[QLCALC_AffiliationPrimaryFlag],
      [QLCALC_AffiliationRole] = SRC.[QLCALC_AffiliationRole],
      [QLCALC_ContactRole] = SRC.[QLCALC_ContactRole]
  WHEN NOT MATCHED BY TARGET
    THEN INSERT (
      [RunID],
      [FullFileName],
      [SrcSystem],
      [ObjectName],
      [RowNo],
      [FileDateTime],
      [LSB_ExternalID],
      [FileRowNumber],
      [HashMD5],
      [UpdateDateTime] ,
      [ChangeStatus] ,
      [QL_StudentID],
      [QL_ApplicationDate],
      [QL_AcadProgNameId],
      [QL_TypeofCourse],
      [QL_DepartmentId],
      [QL_EducationInstitutionId],
      [QL_ApplicationStatus],
      [QL_AOSPeriod],
      [QL_AcademicPeriod],
      [QL_EnrollmentStatus],
      [QL_ApplicantStageIndicator],
      [QL_EnrolmentStageIndicator],
      [QL_AppStatusDateAchieved],
      [QL_EnrolStatusDateAchieved],
      [QL_StudentGraduated],
      [QL_StudentEnrolledDate],
      [QLCALC_AffiliationStartDate],
      [QLCALC_AffiliationEndDate],
      [QLCALC_AffiliationStatus],
      [QLCALC_AffiliationPrimaryFlag],
      [QLCALC_AffiliationRole],
      [QLCALC_ContactRole]
    ) VALUES (
      SRC.[RunID],
      SRC.[FullFileName],
      SRC.[SrcSystem],
      SRC.[ObjectName],
      SRC.[RowNo],
      SRC.[FileDateTime],
      SRC.[LSB_ExternalID],
      SRC.[FileRowNumber],
      SRC.[HashMD5],
      GETDATE(), 
      'NEW',
      SRC.[QL_StudentID],
      SRC.[QL_ApplicationDate],
      SRC.[QL_AcadProgNameId],
      SRC.[QL_TypeofCourse],
      SRC.[QL_DepartmentId],
      SRC.[QL_EducationInstitutionId],
      SRC.[QL_ApplicationStatus],
      SRC.[QL_AOSPeriod],
      SRC.[QL_AcademicPeriod],
      SRC.[QL_EnrollmentStatus],
      SRC.[QL_ApplicantStageIndicator],
      SRC.[QL_EnrolmentStageIndicator],
      SRC.[QL_AppStatusDateAchieved],
      SRC.[QL_EnrolStatusDateAchieved],
      SRC.[QL_StudentGraduated],
      SRC.[QL_StudentEnrolledDate],
      SRC.[QLCALC_AffiliationStartDate],
      SRC.[QLCALC_AffiliationEndDate],
      SRC.[QLCALC_AffiliationStatus],
      SRC.[QLCALC_AffiliationPrimaryFlag],
      SRC.[QLCALC_AffiliationRole],
      SRC.[QLCALC_ContactRole]
    )
  WHEN NOT MATCHED BY SOURCE
    THEN UPDATE 
    SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;
GO