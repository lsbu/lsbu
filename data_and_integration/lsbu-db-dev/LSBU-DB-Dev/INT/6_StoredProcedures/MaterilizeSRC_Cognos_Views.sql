CREATE OR ALTER PROCEDURE [INT].[MaterilizeSRC_Cognos_Views]
AS
DECLARE 
 @cms nvarchar(max) = N'',
 @RC int
BEGIN
	----------------------------------------------------------------------------------- 

  SET @cms = 'DROP TABLE IF EXISTS STG.tStudent_EngagementDetails;
				SELECT * INTO STG.tStudent_EngagementDetails FROM SRC.vStudent_EngagementDetails;';
	
	PRINT @cms
	EXEC sp_executesql @cms
	----------------------------------------------------------------------------------- 
END;