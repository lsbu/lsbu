CREATE OR ALTER PROCEDURE [INT].[Maximizer_IndividualDetailList_PopulateData_STG]
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM Maximizer.src.Individual_DetailListMulti WHERE LEN(IndividualId) > 224 OR LEN(Name) > 224 OR LEN(Value) > 224)
		BEGIN
			RAISERROR('Data length in one of the columns in table Individual_DetailListMulti exceeds maximum length of 224 characters!',16,-1);
		END
	ELSE
		BEGIN
			SELECT 
				CAST(IndividualId AS NVARCHAR(224)) AS IndividualId, 
				CAST(Name AS NVARCHAR(224)) AS [Name], 
				CAST(Value AS NVARCHAR(224)) AS [Value]
			INTO #TMP 
			FROM Maximizer.src.Individual_DetailListMulti

			TRUNCATE TABLE INT.STG_MAX_IndividualDetailListMulti
	
			;WITH CTE1 AS (
				SELECT DISTINCT 
					IO.IndividualId, 
					IO.Name,
					dbo.TRIM(STUFF(
						(SELECT 
							';' + Value
						FROM #TMP II 
						WHERE II.IndividualId = IO.IndividualId AND II.Name = IO.Name 
						FOR XML PATH('')), 1, 1, '')) [Value]
				FROM #TMP IO
				JOIN (SELECT 
						name, 
						IndividualId
					 FROM #TMP 
					 GROUP BY Name, IndividualId 
					 HAVING count(1) > 1) AS T1 ON IO.IndividualId = T1.IndividualId AND IO.Name = T1.Name),
			CTE2 AS (
				SELECT 
					IDLM.IndividualId, 
					IDLM.Name, 
					IDLM.Value 
				FROM #TMP IDLM
				WHERE NOT EXISTS (SELECT 1 FROM CTE1 WHERE IDLM.IndividualId = CTE1.IndividualId AND IDLM.Name = CTE1.Name)
				UNION
				SELECT
					CTE1.IndividualId,
					CTE1.Name,
					CTE1.Value
				FROM CTE1
			)
			INSERT INTO INT.STG_MAX_IndividualDetailListMulti
				(IndividualId
				,Name
				,Value)
			SELECT 
				IndividualId,
				Name,
				Value
			FROM CTE2
		END
END