CREATE OR ALTER PROCEDURE [INT].[MERGE_Module](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_Module AS TAR

				USING (SELECT * FROM [STG].[tModule] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tModule] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
				[QL_ModuleId]=SRC.[QL_ModuleId],
        [QL_ModuleName]=SRC.[QL_ModuleName],
        [QL_DepartmentId]=SRC.[QL_DepartmentId],
        [QL_EducationInstitutionId]=SRC.[QL_EducationInstitutionId],
        [QL_ModuleLevel]=SRC.[QL_ModuleLevel],
        [QL_ModuleStatus]=SRC.[QL_ModuleStatus]

		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
				[FileRowNumber],
        [HashMD5],
        [InsetDateTime],
        [ChangeStatus],
				[QL_ModuleId], 
        [QL_ModuleName], 
        [QL_DepartmentId], 
        [QL_EducationInstitutionId], 
        [QL_ModuleLevel], 
        [QL_ModuleStatus])
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
		SRC.[FileRowNumber],
    SRC.[HashMD5],
    GETDATE(),
    'NEW',
		SRC.[QL_ModuleId], 
    SRC.[QL_ModuleName], 
    SRC.[QL_DepartmentId], 
    SRC.[QL_EducationInstitutionId], 
    SRC.[QL_ModuleLevel], 
    SRC.[QL_ModuleStatus])
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;