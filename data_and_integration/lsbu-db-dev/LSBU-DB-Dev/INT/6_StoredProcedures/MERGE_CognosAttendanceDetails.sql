CREATE OR ALTER   PROCEDURE [INT].[MERGE_CognosAttendanceDetails](
			@RunID INTEGER
			)
AS
BEGIN
		MERGE [INT].[SRC_CognosAttendanceDetails] AS TAR
				USING (SELECT * FROM [STG].[tCognosAttendanceDetails]
					   WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tCognosAttendanceDetails] group by LSB_ExternalID having count(1)>1)) AS SRC
				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
					(CASE
						WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
						WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
					END),
				[Cognos_StudentID]=SRC.Cognos_StudentID,
				[Cognos_ModuleId]=SRC.Cognos_ModuleId,
				[Cognos_AcadProgNameId]=SRC.Cognos_AcadProgNameId,
				[Cognos_Term]=SRC.Cognos_Term,
				[Cognos_AOSPeriod]=SRC.Cognos_AOSPeriod,
				[Cognos_AcademicPeriod]=SRC.Cognos_AcademicPeriod,
				[Cognos_LecturerId]=SRC.Cognos_LecturerId,
				[Cognos_ModuleLevelAttendance]=SRC.Cognos_ModuleLevelAttendance,
				[Cognos_TotalModuleClasses]=SRC.Cognos_TotalModuleClasses,
				[Cognos_ModuleLevelAttendancepercent]=SRC.Cognos_ModuleLevelAttendancepercent,
				[Cognos_CourseLevelAttendance]=SRC.Cognos_CourseLevelAttendance,
				[Cognos_CourseLevelAttendancepercent]=SRC.Cognos_CourseLevelAttendancepercent
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([RunID],[FullFileName],[SrcSystem],[ObjectName],[RowNo],[FileDateTime],[LSB_ExternalID],
					[FileRowNumber],[HashMD5],[InsetDateTime],[ChangeStatus],
					[Cognos_StudentID],[Cognos_ModuleId],[Cognos_AcadProgNameId],[Cognos_Term],[Cognos_AOSPeriod],[Cognos_AcademicPeriod],[Cognos_LecturerId],[Cognos_ModuleLevelAttendance],
					[Cognos_TotalModuleClasses],[Cognos_ModuleLevelAttendancepercent],[Cognos_CourseLevelAttendance],[Cognos_CourseLevelAttendancepercent])
		VALUES (SRC.[RunID],SRC.[FullFileName],SRC.[SrcSystem],SRC.[ObjectName],SRC.[RowNo],SRC.[FileDateTime],SRC.[LSB_ExternalID],
				SRC.[FileRowNumber],SRC.[HashMD5],GETDATE(),'NEW',
				SRC.Cognos_StudentID,SRC.Cognos_ModuleId,SRC.Cognos_AcadProgNameId,SRC.Cognos_Term,SRC.Cognos_AOSPeriod,SRC.Cognos_AcademicPeriod,SRC.Cognos_LecturerId,SRC.Cognos_ModuleLevelAttendance,
				SRC.Cognos_TotalModuleClasses,SRC.Cognos_ModuleLevelAttendancepercent,SRC.Cognos_CourseLevelAttendance,SRC.Cognos_CourseLevelAttendancepercent)
		WHEN NOT MATCHED BY SOURCE
			THEN UPDATE 
			SET TAR.UpdateDateTime = GETDATE(), 
				TAR.ChangeStatus = 'DELETE';
END;