
CREATE OR ALTER     PROCEDURE [INT].[MERGE_StudentAdditionalDetails](
			@RunID INTEGER
			)
AS

BEGIN
		MERGE [INT].SRC_StudentAdditionalDetails AS TAR

				USING (SELECT * FROM [STG].[tStudentAdditionalDetails] CTM
						WHERE LSB_ExternalID NOT IN (SELECT LSB_ExternalID FROM [STG].[tStudentAdditionalDetails] group by LSB_ExternalID having count(*)>1)) AS SRC

				ON (TAR.LSB_ExternalID = SRC.LSB_ExternalID)
		WHEN MATCHED 
			THEN UPDATE 
			   SET 
				[RunID]=SRC.[RunID],
				[FullFileName]=SRC.[FullFileName],
				[SrcSystem]=SRC.[SrcSystem],
				[ObjectName]=SRC.[ObjectName],
				[RowNo]=SRC.[RowNo],
				[FileDateTime]=SRC.[FileDateTime],
				[FileRowNumber]=SRC.[FileRowNumber],
				[HashMD5]=SRC.[HashMD5],
				[UpdateDateTime] = GETDATE(), 
				[ChangeStatus]= 
									(CASE
										WHEN TAR.HashMD5 <> SRC.HashMD5 THEN 'UPDATE'
										WHEN TAR.HashMD5  = SRC.HashMD5 THEN 'NO CHANGE'
									END),
        [QL_StudentID]=SRC.[QL_StudentID],
        [QL_RefugeeStatus]=SRC.[QL_RefugeeStatus],
        [QL_CaringResponsibilities]=SRC.[QL_CaringResponsibilities],
        [QL_BeeninCare]=SRC.[QL_BeeninCare],
        [QL_EnstrangedfromFamily]=SRC.[QL_EnstrangedfromFamily],
        [QL_AcadProgNameId]=SRC.[QL_AcadProgNameId],
        [QL_Term]=SRC.[QL_Term],
        [QL_AOSPeriod]=SRC.[QL_AOSPeriod],
        [QL_AcademicPeriod]=SRC.[QL_AcademicPeriod]


		WHEN NOT MATCHED BY TARGET
		THEN INSERT 
       ([RunID],
        [FullFileName],
        [SrcSystem],
        [ObjectName],
        [RowNo],
        [FileDateTime],
        [LSB_ExternalID],
				[FileRowNumber],
        [HashMD5],
        [InsetDateTime],
        [ChangeStatus],
        [QL_StudentID],
        [QL_RefugeeStatus],
        [QL_CaringResponsibilities],
        [QL_BeeninCare],
        [QL_EnstrangedfromFamily],
        [QL_AcadProgNameId],
        [QL_Term],
        [QL_AOSPeriod],
        [QL_AcademicPeriod]

        )
		VALUES 
   (SRC.[RunID],
    SRC.[FullFileName],
    SRC.[SrcSystem],
    SRC.[ObjectName],
    SRC.[RowNo],
    SRC.[FileDateTime],
    SRC.[LSB_ExternalID],
		SRC.[FileRowNumber],
    SRC.[HashMD5],
    GETDATE(),
    'NEW',
    SRC.[QL_StudentID],
    SRC.[QL_RefugeeStatus],
    SRC.[QL_CaringResponsibilities],
    SRC.[QL_BeeninCare],
    SRC.[QL_EnstrangedfromFamily],
    SRC.[QL_AcadProgNameId],
    SRC.[QL_Term],
    SRC.[QL_AOSPeriod],
    SRC.[QL_AcademicPeriod]

    )
    WHEN NOT MATCHED BY SOURCE
		THEN UPDATE 
		SET TAR.UpdateDateTime = GETDATE(), TAR.ChangeStatus = 'DELETE';
END;

