IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_IndividualDetailList]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_IndividualDetailList];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_IndividualDetailList](
	[Id] [nvarchar](max) NULL,
	[MH__WB_MHWB_Archive_Status] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_PEEPs_2010_PEEP_Prepared_by] [nvarchar](max) NULL,
	[Level_of_Study] [nvarchar](max) NULL,
	[MH__WB_In_Halls] [nvarchar](max) NULL,
	[DDS_DSA_Study_Skills_Tuition_Tutor_Supplier] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Scribe_recommended] [nvarchar](max) NULL,
	[DDS_DSA_Study_Skills_Tuition_Dyslexia_tutor_recommended] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_BSL_interpreter_recommended] [nvarchar](max) NULL,
	[DDS_PRE_ENTRY_Allocated_to_DA] [nvarchar](max) NULL,
	[DDS_Support_Arrangements_QL_Disability_Fields_DDS_Support] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Refunded] [nvarchar](max) NULL,
	[DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_appointment_adviser] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_PEEPs_2010_If_Yes_which_one] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Library_assistant_recommended] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Teacher_for_the_Deaf_recommended] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Lab_assistant_recommended] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Support_Arrangements__Sympathetic_Marking] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Campus_support_recommended] [nvarchar](max) NULL,
	[DDS_DSA_Mentoring_Support_Mentoring_Supplier] [nvarchar](max) NULL,
	[DDS_Support_Arrangements_DAs_initials] [nvarchar](max) NULL,
	[DDS_Screening_and_Ed_Psych_Assessment_Day_of_the_week_of_EP] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_paid] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Disability_Category] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_form_completed] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Note_taker_recommeded] [nvarchar](max) NULL,
	[DDS_DSA_Mentoring_Support_Mentor_recommended] [nvarchar](max) NULL,
	[DDS_Student_Record_Status] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_PEEP_Not_required__please_tick_one_] [nvarchar](max) NULL,
	[DDS_Screening_and_Ed_Psych_Assessment_Feedback_location] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_ALF_Money_received] [nvarchar](max) NULL,
	[MH__WB_MHWB_Archive_MHWB_Status] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Assessment_funded_by] [nvarchar](max) NULL,
	[DDS_Screening_and_Ed_Psych_Assessment_EP_Cost] [nvarchar](max) NULL,
	[DDS_PEEPs_Student_requires_a_PEEP___referred_to_HS] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO