IF OBJECT_ID('[INT].[SRC_Student_SupportProfile]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Student_SupportProfile];
GO

CREATE TABLE [INT].[SRC_Student_SupportProfile] (
  [RunID] [int] NULL,
  [FullFileName] [varchar](100) NULL,
  [SrcSystem] [varchar](100) NULL,
  [ObjectName] [varchar](100) NULL,
  [RowNo] [int] NULL,
  [FileDateTime] [datetime] NULL,
  [LSB_ExternalID] [varchar](400) NOT NULL,
  [FileRowNumber] [int] NULL,
  [HashMD5] [varbinary](8000) NULL,
  [InsetDateTime] [datetime] NULL,
  [UpdateDateTime] [datetime] NULL,
  [ChangeStatus] [varchar](100) NULL,
  [SFHashMD5] [varbinary](8000) NULL,
  [SFUpdateDateTime] [datetime] NULL,
  [SFStatus] [varchar](100) NULL,
  [SFId] [varchar](100) NULL,
  [QL_StudentID] [varchar](4000) NULL,
  [QL_Disabilty] [varchar](4000) NULL,
  [QL_DisabiltyAllowance] [varchar](4000) NULL,
  [QL_CareLeaver] [varchar](4000) NULL
)