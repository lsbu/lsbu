IF OBJECT_ID('[INT].[SRC_AllStaff_CaseTeamMember]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_AllStaff_CaseTeamMember];
GO

CREATE TABLE [INT].[SRC_AllStaff_CaseTeamMember](
	[StudentId] [varchar](4000) NULL,
	[FederationIdentifier] [varchar](4011) NULL,
	[CourseTutor] [int] NULL,
	[PersonalTutor] [int] NULL,
	[ModuleLeader] [int] NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL
)