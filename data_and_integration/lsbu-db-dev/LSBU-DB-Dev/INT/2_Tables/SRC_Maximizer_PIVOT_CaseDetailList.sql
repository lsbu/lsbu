IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_CaseDetailList]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailList];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailList](
	[CaseId] [nvarchar](max) NULL,
	[Safety_Concern_Response_Student_in_halls] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO