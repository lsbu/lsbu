IF OBJECT_ID('[INT].[SRC_ApplicationEnrolmentDetails_DeProv_DeAct]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ApplicationEnrolmentDetails_DeProv_DeAct];
GO

CREATE TABLE [INT].[SRC_ApplicationEnrolmentDetails_DeProv_DeAct](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](400) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SF_UserID] [varchar](4000) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QLCALC_ContactRole] [varchar](50) NULL,
	[QL_AcademicPeriod] [varchar](50) NULL,
	[QL_AcadProgNameId] [varchar](50) NULL,
	[QL_ApplicationStatus] [varchar](4000) NULL,
	[QL_AppStatusDateAchieved] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_StudentGraduated] [varchar](4000) NULL,
	[QLCALC_RowUpsertedDate] [datetime] NULL,
	[QLCALC_DeProvisioningdate] [datetime] NULL,
	[QLCALC_DeProvisionedStatus] [varchar](50) NULL,
	[QLCALC_DeProvisionedStatusDate] [datetime] NULL
)