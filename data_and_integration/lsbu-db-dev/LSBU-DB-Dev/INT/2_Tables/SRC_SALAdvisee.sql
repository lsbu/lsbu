IF OBJECT_ID('[INT].[SRC_SALAdvisee]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_SALAdvisee];
GO

CREATE TABLE [INT].[SRC_SALAdvisee](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_FirstName] [varchar](4000) NULL,
	[QL_Surname] [varchar](4000) NULL,
	[QL_Phone] [varchar](4000) NULL,
	[QL_LSBUEmail] [varchar](4000) NULL,
	[QL_PersonalEmail] [varchar](4000) NULL,
	[QL_MobileNo] [varchar](4000) NULL
)