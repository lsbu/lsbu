IF OBJECT_ID('[INT].[SRC_ApplicationDetails]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ApplicationDetails];
GO

CREATE TABLE [INT].[SRC_ApplicationDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](400) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL, --change tracking
	[UpdateDateTime] [datetime] NULL,  --change tracking
	[ChangeStatus] [varchar](100) NULL,  --change tracking
	--------
	[SFHashMD5] [varbinary](8000) NULL,  --SF MD5
	[SFUpdateDateTime] [datetime] NULL,  --SF Tracking
	[SFStatus] [varchar](100) NULL,  --SF Status
	[SFId] [varchar](100) NULL,  --SF ID
	-----------
	[QL_StudentID] [varchar](4000) NULL,
	[QL_PersonalID] [varchar](4000) NULL,
	[QL_ApplicationCode] [varchar](4000) NULL,
	[QL_ApplicationNumber] [varchar](4000) NULL,
	[QL_ApplicationDate] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_IsItPrimaryAcademicProg] [varchar](4000) NULL,
	[QL_ApplicationStatus] [varchar](4000) NULL,
	[QL_ApplicationType] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_ProgramStartDate] [varchar](4000) NULL,
	[QL_ProgramEndDate] [varchar](4000) NULL,
	[QL_ModeOfStudy] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_CreatedDate] [varchar](4000) NULL,
	[QL_LastModifiedDate] [varchar](4000) NULL,
	[QL_ApplicationSource] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_DepositStatus] [varchar](4000) NULL,
	[QL_VisaStatus] [varchar](4000) NULL,
	[QL_ApplicantAgentCode] [varchar](4000) NULL,
	[QL_ApplicantAgencyName] [varchar](4000) NULL,
	[QL_PreviousInstituteName] [varchar](4000) NULL,
	[QL_UCASCourseCode] [varchar](4000) NULL,
	[QL_ApplicantStageIndicator] [varchar](4000) NULL,
	[QL_EnrolmentStageIndicator] [varchar](4000) NULL,
	[QL_CriminalConvictions] [varchar](4000) NULL,
	[QL_VisaType] [varchar](4000) NULL,
	[QL_NominatedName] [varchar](4000) NULL,
	[QL_NominatedRelationship] [varchar](4000) NULL,
	[QL_AttedenceType] [varchar](4000) NULL,
	[QL_SessionStartDate] [varchar](4000) NULL,
	[QL_SessionEndDate] [varchar](4000) NULL,
	[QL_ChoiceNumber] [varchar](4000) NULL,
	[QL_CampusInformation] [varchar](4000) NULL,
	[QL_AppStatusDateAchieved] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_QualificationCheck] [varchar](4000) NULL
) ON [PRIMARY]
GO


