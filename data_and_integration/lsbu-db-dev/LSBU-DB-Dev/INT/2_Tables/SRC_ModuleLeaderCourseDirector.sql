IF OBJECT_ID('[INT].[SRC_ModuleLeaderCourseDirector]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ModuleLeaderCourseDirector];
GO

CREATE TABLE [INT].[SRC_ModuleLeaderCourseDirector](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_StaffID] [varchar](4000) NULL,
	[QL_StaffUsername] [varchar](4000) NULL,
	[QL_StaffEmailAddress] [varchar](4000) NULL,
	[QL_StaffFirstName] [varchar](4000) NULL,
	[QL_StaffLastName] [varchar](4000) NULL,
	[QL_Gender] [varchar](4000) NULL
)