IF OBJECT_ID('[INT].[SRC_ModuleComponentEnrolmentDetails]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ModuleComponentEnrolmentDetails];
GO

CREATE TABLE [INT].[SRC_ModuleComponentEnrolmentDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[ModuleEnrolment_LSB_ExternalID] [varchar](4000) NULL,
	[Term_LSB_ExternalID] [varchar](4000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleInstance] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_ModuleStartDate] [varchar](4000) NULL,
	[QL_ModuleEndDate] [varchar](4000) NULL,
	[QL_ComponentId] [varchar](4000) NULL,
	[QL_ComponentDesc] [varchar](4000) NULL,
	[QL_ComponentHandinDate] [varchar](4000) NULL
)