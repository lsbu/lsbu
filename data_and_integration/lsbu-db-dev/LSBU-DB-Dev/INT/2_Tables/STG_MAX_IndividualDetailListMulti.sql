IF OBJECT_ID('[INT].[STG_MAX_IndividualDetailListMulti]','U') IS NOT NULL
	DROP TABLE [INT].[STG_MAX_IndividualDetailListMulti];
GO

CREATE TABLE INT.STG_MAX_IndividualDetailListMulti
(IndividualId NVARCHAR(MAX)
,Name NVARCHAR(MAX)
,Value NVARCHAR(MAX))