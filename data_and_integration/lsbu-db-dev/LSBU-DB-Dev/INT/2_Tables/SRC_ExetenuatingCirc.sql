IF OBJECT_ID('[INT].[SRC_ExetenuatingCirc]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ExetenuatingCirc];
GO

CREATE TABLE [INT].[SRC_ExetenuatingCirc](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_StudentID] [varchar](100) NULL,
	[QL_ModuleId] [varchar](100) NULL,
	[QL_ModuleInstance] [varchar](100) NULL,
	[QL_AcadProgNameId] [varchar](100) NULL,
	[QL_Term] [varchar](100) NULL,
	[QL_AOSPeriod] [varchar](100) NULL,
	[QL_AcademicPeriod] [varchar](100) NULL,
	[QL_Semester] [varchar](100) NULL,
	[QL_Attempt] [varchar](100) NULL,
	[QL_ComponentType] [varchar](100) NULL,
	[QL_ECStatusFlag] [varchar](100) NULL,
	[QL_ECStatusFlagDate] [varchar](100) NULL
)


