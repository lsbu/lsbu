IF OBJECT_ID('[INT].[SRC_User]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_User];
GO

CREATE TABLE [INT].[SRC_User](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](401) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL, --change tracking
	[UpdateDateTime] [datetime] NULL,  --change tracking
	[ChangeStatus] [varchar](100) NULL,  --change tracking --------
	[SFHashMD5] [varbinary](8000) NULL,  --SF MD5
	[SFUpdateDateTime] [datetime] NULL,  --SF Tracking
	[SFStatus] [varchar](100) NULL,  --SF Status
	[SFId] [varchar](100) NULL,  --SF ID -----------
	[QL_UserId] [varchar](4000) NULL,
	[QL_UserName] [varchar](4000) NULL
) ON [PRIMARY]
GO


