IF OBJECT_ID('[INT].[SRC_AppliedSciencesPTCD_Affiliation]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_AppliedSciencesPTCD_Affiliation];
GO

CREATE TABLE [INT].[SRC_AppliedSciencesPTCD_Affiliation](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](400) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[PTCDCSV_CourseCode] [varchar](4000) NULL,
	[PTCDCSV_CourseName] [varchar](4000) NULL,
	[QL_CDStaffId] [varchar](4000) NULL,
	[PTCDCSV_CDFirstName] [varchar](4000) NULL,
	[PTCDCSV_CDLastname] [varchar](4000) NULL,
	[PTCDCSV_CDEmail] [varchar](4000) NULL,
	[StaffRank] [varchar](4000) NULL,
)