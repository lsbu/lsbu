IF OBJECT_ID('[INT].[SRC_StudentTeachingStaff_Relationship]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_StudentTeachingStaff_Relationship];
GO

CREATE TABLE [INT].[SRC_StudentTeachingStaff_Relationship](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[ModuleOffering_LSB_ExternalID] [varchar](200) NULL,
	[TeachingStaff_LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[CMIS_StudentID] [varchar](4000) NULL,
	[CMIS_AcadProgNameId] [varchar](4000) NULL,
	[CMIS_AcademicPeriod] [varchar](4000) NULL,
	[CMIS_AOSPeriod] [varchar](4000) NULL,
	[CMIS_ModuleId] [varchar](4000) NULL,
	[CMIS_ModuleInstance] [varchar](4000) NULL,
	[CMIS_Semester] [varchar](4000) NULL,
	[CMIS_TeachingStaffId] [varchar](4000) NULL,
	[CMIS_TeachingStaffFirstName] [varchar](4000) NULL,
	[CMIS_TeachingStaffLastname] [varchar](4000) NULL,
	[CMIS_TeachingStaffEmail] [varchar](4000) NULL
)


