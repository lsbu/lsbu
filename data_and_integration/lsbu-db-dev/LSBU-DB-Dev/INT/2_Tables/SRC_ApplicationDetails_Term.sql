IF OBJECT_ID('[INT].[SRC_ApplicationDetails_Term]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ApplicationDetails_Term];
GO

CREATE TABLE [INT].[SRC_ApplicationDetails_Term](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](400) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_TermName] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_SessionStartDate] [varchar](4000) NULL,
	[QL_SessionEndDate] [varchar](4000) NULL,
	[QL_TermType] [varchar](4000) NULL
) ON [PRIMARY]
GO


