IF OBJECT_ID('[INT].[SRC_Module]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Module];
GO

CREATE TABLE [INT].[SRC_Module](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleName] [varchar](4000) NULL,
	[QL_DepartmentId] [varchar](4000) NULL,
	[QL_EducationInstitutionId] [varchar](4000) NULL,
	[QL_ModuleLevel] [varchar](4000) NULL,
	[QL_ModuleStatus] [varchar](4000) NULL
)