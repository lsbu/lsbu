IF OBJECT_ID('[INT].[SRC_CognosAttendanceDetails]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_CognosAttendanceDetails];
GO

CREATE TABLE [INT].[SRC_CognosAttendanceDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[Cognos_StudentID] [varchar](4000) NULL,
	[Cognos_ModuleId] [varchar](4000) NULL,
	[Cognos_AcadProgNameId] [varchar](4000) NULL,
	[Cognos_Term] [varchar](4000) NULL,
	[Cognos_AOSPeriod] [varchar](4000) NULL,
	[Cognos_AcademicPeriod] [varchar](4000) NULL,
	[Cognos_LecturerId] [varchar](4000) NULL,
	[Cognos_ModuleLevelAttendance] [varchar](4000) NULL,
	[Cognos_TotalModuleClasses] [varchar](4000) NULL,
	[Cognos_ModuleLevelAttendancepercent] [varchar](4000) NULL,
	[Cognos_CourseLevelAttendance] [varchar](4000) NULL,
	[Cognos_CourseLevelAttendancepercent] [varchar](4000) NULL
) 