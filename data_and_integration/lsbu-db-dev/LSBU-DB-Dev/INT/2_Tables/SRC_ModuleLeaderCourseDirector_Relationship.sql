IF OBJECT_ID('[INT].[SRC_ModuleLeaderCourseDirector_Relationship]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ModuleLeaderCourseDirector_Relationship];
GO

CREATE TABLE [INT].[SRC_ModuleLeaderCourseDirector_Relationship](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[ModuleOffering_LSB_ExternalID] [varchar](200) NULL,
	[Staff_LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_StudentId] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_ApplicationStatus] [varchar](4000) NULL,
	[QL_AppStatusDateAchieved] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_StudentEnrolledDate] [varchar](4000) NULL,
	[QL_CourseDirector] [varchar](4000) NULL,
	[QL_ModuleLeader] [varchar](4000) NULL
)