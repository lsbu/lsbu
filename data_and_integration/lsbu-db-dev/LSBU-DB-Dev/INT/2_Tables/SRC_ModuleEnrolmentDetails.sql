IF OBJECT_ID('[INT].[SRC_ModuleEnrolmentDetails]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_ModuleEnrolmentDetails];
GO

CREATE TABLE [INT].[SRC_ModuleEnrolmentDetails](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[ModuleOffering_LSB_ExternalID] [varchar](4000) NULL,
	[Term_LSB_ExternalID] [varchar](4000) NULL,
	[ProgramEnrolment_LSB_ExternalID] [varchar](4000) NULL,
	[Affiliation_LSB_ExternalID] [varchar](4000) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[QL_StudentID] [varchar](4000) NULL,
	[QL_ModuleId] [varchar](4000) NULL,
	[QL_ModuleInstance] [varchar](4000) NULL,
	[QL_AcadProgNameId] [varchar](4000) NULL,
	[QL_Term] [varchar](4000) NULL,
	[QL_AOSPeriod] [varchar](4000) NULL,
	[QL_AcademicPeriod] [varchar](4000) NULL,
	[QL_EnrollmentStatus] [varchar](4000) NULL,
	[QL_EnrolmentStageIndicator] [varchar](4000) NULL,
	[QL_EnrolStatusDateAchieved] [varchar](4000) NULL,
	[QL_Compulsory] [varchar](10) NULL,
	[QL_Semester] [varchar](10) NULL
)