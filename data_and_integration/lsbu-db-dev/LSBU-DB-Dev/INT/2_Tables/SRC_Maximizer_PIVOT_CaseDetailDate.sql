IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_CaseDetailDate]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailDate];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailDate](
	[CaseId] [nvarchar](max) NULL,
	[Fitness_to_Study_Initial_referral_Date_of_first_referral] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_1__Informal__Date_of_informal_meeting] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Outcome_Date_of_follow_up] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_review] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Date_of_Panel] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Outcome_Date_of_follow_up] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_Review_Panel] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_outside_FtS_Date_of_interruption] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Date_of_interruption] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Scheduled_date_to_contact_about_return] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Date_of_invitation] [nvarchar](max) NULL,
	[Fitness_to_Study_Review_of_decisions_COP_unfavourable_to_student_Date_COP_letter_provided] [nvarchar](max) NULL,
	[Fitness_to_Study_Review_of_decisions_Date_review_requested] [nvarchar](max) NULL,
	[Fitness_to_Study_Review_of_decisions_Date_student_informed_of_review_outcome] [nvarchar](max) NULL,
	[MHWB_Duty_Actions_complete_by_date] [nvarchar](max) NULL,
	[MHWB_Duty_Date_added_to_duty_list] [nvarchar](max) NULL,
	[MHWB_Duty_Date_left_duty_list] [nvarchar](max) NULL,
	[MHWB_Duty_Date_of_contact] [nvarchar](max) NULL,
	[MHWB_Duty_Date_of_contact_attempt_s__with_student_1st_contact_date] [nvarchar](max) NULL,
	[MHWB_Duty_Date_of_contact_attempt_s__with_student_2nd_contact_date] [nvarchar](max) NULL,
	[MHWB_Duty_If_staff_referral__confirmation_sent] [nvarchar](max) NULL,
	[Report_and_Support_Hate_Incident_Date_of_first_referral] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Date_of_first_referral] [nvarchar](max) NULL,
	[Safety_Concern_Response_Action_plan_Date_action_plan_agreed] [nvarchar](max) NULL,
	[Safety_Concern_Response_Action_plan_Date_action_plan_updated_1] [nvarchar](max) NULL,
	[Safety_Concern_Response_Action_plan_Date_action_plan_updated_2] [nvarchar](max) NULL,
	[Safety_Concern_Response_Date_added_to_meeting] [nvarchar](max) NULL,
	[Safety_Concern_Response_Date_left_meeting] [nvarchar](max) NULL,
	[Safety_Concern_Response_Date_of_initial_concern] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_1_date] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_2_date] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_3_date] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_4_date] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_date_5] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]