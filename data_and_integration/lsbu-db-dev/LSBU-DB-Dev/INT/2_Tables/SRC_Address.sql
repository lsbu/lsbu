IF OBJECT_ID('[INT].[SRC_Address]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Address];
GO

CREATE TABLE [INT].[SRC_Address](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](200) NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL, --change tracking
	[UpdateDateTime] [datetime] NULL,  --change tracking
	[ChangeStatus] [varchar](100) NULL,  --change tracking --------
	[SFHashMD5] [varbinary](8000) NULL,  --SF MD5
	[SFUpdateDateTime] [datetime] NULL,  --SF Tracking
	[SFStatus] [varchar](100) NULL,  --SF Status
	[SFId] [varchar](100) NULL,  --SF ID -----------
	[QL_StudentID] [varchar](4000) NULL,
	[QL_UCASPersonalID] [varchar](4000) NULL,
	[QL_Title] [varchar](4000) NULL,
	[QL_Initials] [varchar](4000) NULL,
	[QL_FirstName] [varchar](4000) NULL,
	[QL_FamiliarName] [varchar](4000) NULL,
	[QL_Surname] [varchar](4000) NULL,
	[QL_MiddleName] [varchar](4000) NULL,
	[QL_Birthdate] [varchar](4000) NULL,
	[QL_StudentStatus] [varchar](4000) NULL,
	[QL_Gender] [varchar](4000) NULL,
	[QL_Domicile] [varchar](4000) NULL,
	[QL_Origin] [varchar](4000) NULL,
	[QL_Nationality] [varchar](4000) NULL,
	[QL_Disabilty] [varchar](4000) NULL,
	[QL_Add1] [varchar](4000) NULL,
	[QL_Add2] [varchar](4000) NULL,
	[QL_Add3] [varchar](4000) NULL,
	[QL_Add4] [varchar](4000) NULL,
	[QL_Add5] [varchar](4000) NULL,
	[QL_Postcode] [varchar](4000) NULL,
	[QL_Country] [varchar](4000) NULL,
	[QL_AddressType] [varchar](4000) NULL,
	[QL_Phone] [varchar](4000) NULL,
	[QL_LSBUEmail] [varchar](4000) NULL,
	[QL_PersonalEmail] [varchar](4000) NULL,
	[QL_MobileNo] [varchar](4000) NULL,
	[QL_CreatedDate] [varchar](4000) NULL,
	[QL_LastModifiedDate] [varchar](4000) NULL
) ON [PRIMARY]
GO


