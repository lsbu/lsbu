IF OBJECT_ID('[INT].[SRC_Department]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Department];
GO

CREATE TABLE [INT].[SRC_Department](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](401) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL, --change tracking
	[UpdateDateTime] [datetime] NULL,  --change tracking
	[ChangeStatus] [varchar](100) NULL,  --change tracking --------
	[SFHashMD5] [varbinary](8000) NULL,  --SF MD5
	[SFUpdateDateTime] [datetime] NULL,  --SF Tracking
	[SFStatus] [varchar](100) NULL,  --SF Status
	[SFId] [varchar](100) NULL,  --SF ID -----------
	[QL_DepartmentId] [varchar](4000) NULL,
	[QL_DepartmentName] [varchar](4000) NULL,
	[QL_EducationInstitutionId] [varchar](4000) NULL,
	[QL_Website] [varchar](4000) NULL,
	[QL_Phone] [varchar](4000) NULL,
	[QL_Add1] [varchar](4000) NULL,
	[QL_Add2] [varchar](4000) NULL,
	[QL_Add3] [varchar](4000) NULL,
	[QL_Add4] [varchar](4000) NULL,
	[QL_Add5] [varchar](4000) NULL,
	[QL_Postcode] [varchar](4000) NULL,
	[QL_Country] [varchar](4000) NULL,
	[QL_Description] [varchar](4000) NULL,
	[QL_CreatedDate] [varchar](4000) NULL,
	[QL_LastModifiedDate] [varchar](4000) NULL
) ON [PRIMARY]
GO


