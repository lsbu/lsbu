IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_CaseDetailListMulti]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailListMulti];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailListMulti](
	[CaseId] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Initial_risk_or_impact_level__at_time_of_concern_] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_documents_provided] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Companion_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_1__Informal__Outcome] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_attended] [nvarchar](max) NULL,
	[Safety_Concern_Response_Teams_people_involved_LSBU] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Escalate_concerns] [nvarchar](max) NULL,
	[Safety_Concern_Response_Concern_raised_by] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Covid_19] [nvarchar](max) NULL,
	[Fitness_to_Study_Review_of_decisions_COP_favourable_to_student_Completion_of_Procedures_letter_requested] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_attended] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Exacerbatingcontributing_factors] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_student_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Student_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_provided] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_type] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Alleged_perpetrator_LSBU_student] [nvarchar](max) NULL,
	[MHWB_Duty_Date_of_contact_attempt_s__with_student_1st_contact_successful] [nvarchar](max) NULL,
	[Fitness_to_Study_Initial_referral_Referrer] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_provided] [nvarchar](max) NULL,
	[MHWB_Duty_Current_safety_concerns_identified_Threat_to_self] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_student_attended] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Threat_to_self_detail] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Initial_risk_and_support_plan_complete] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Exacerbatingcontributing_factors_detail] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_student_confirmed] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Signposted_Services] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Companion_attended] [nvarchar](max) NULL,
	[Safety_Concern_Response_Action_plan_Action_plan_required] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Companion_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_requested] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_companion_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_provided] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_3] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_1] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Other_concern] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Outcome_Signposted_Services] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_student_confirmed] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_confirmed] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Threat_to_self] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_1__Informal__Lead_person_role] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_requested] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Advisor] [nvarchar](max) NULL,
	[MHWB_Duty_Action_s__taken_by_duty_advisor] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attedance_confirmed] [nvarchar](max) NULL,
	[MHWB_Duty_Current_safety_concerns_identified_Threat_to_others] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Alleged_perpetrator_LSBU_staff] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Student_attended] [nvarchar](max) NULL,
	[MHWB_Duty_Contact_details_confirmed_or_updated] [nvarchar](max) NULL,
	[Safety_Concern_Response_Action_plan_Action_owners] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Threat_to_others] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Companion_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Response_from_student] [nvarchar](max) NULL,
	[Fitness_to_Study_Initial_referral_Nature_of_concern] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_4] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Outcome] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_2] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services] [nvarchar](max) NULL,
	[MHWB_Duty_Any_previous_contact_with_MHWB] [nvarchar](max) NULL,
	[MHWB_Duty_Date_of_contact_attempt_s__with_student_3rd_contact_successful] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_documents_provided] [nvarchar](max) NULL,
	[MHWB_Duty_Method_of_duty_referral] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_requested] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_2nd_documents_requested] [nvarchar](max) NULL,
	[MHWB_Duty_Referred_by] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Threat_from_others] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Companion_attended] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Covid_19_detail] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Outcome_Outcome] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_companion_attended] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_police] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_provided] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_Student_Disciplinary] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Student_planning_to_return] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_Student_Complaints] [nvarchar](max) NULL,
	[MHWB_Duty_Current_safety_concerns_identified_Threat_from_others] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_provided] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_provided] [nvarchar](max) NULL,
	[Fitness_to_Study_Initial_referral_Outcome_of_referral] [nvarchar](max) NULL,
	[MHWB_Duty_Assigned_to_advisor] [nvarchar](max) NULL,
	[Safety_Concern_Response_Safeguarding_concern] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Other_concern_detail] [nvarchar](max) NULL,
	[MHWB_Duty_Nature_of_previous_contact] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_requested] [nvarchar](max) NULL,
	[Fitness_to_Study_Review_of_decisions_Review_requested_by] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_requested] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Referral_route] [nvarchar](max) NULL,
	[Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Safeguarding_concern] [nvarchar](max) NULL,
	[Safety_Concern_Response_Teams_people_involved_External] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_2nd_documents_requested] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_confirmed] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_requested] [nvarchar](max) NULL,
	[MHWB_Duty_Current_safety_concerns_identified_Other_concern] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_4__Return__Interruption_outside_FtS_Reason_for_interruption] [nvarchar](max) NULL,
	[Fitness_to_Study_Initial_referral_Immediate_Suspension] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_1__Informal__Signposted_Services] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_confirmed] [nvarchar](max) NULL,
	[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_5] [nvarchar](max) NULL,
	[MHWB_Duty_Date_of_contact_attempt_s__with_student_2nd_contact_successful] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Companion_attended] [nvarchar](max) NULL,
	[Fitness_to_Study_Review_of_decisions_Director_of_SSE_decision] [nvarchar](max) NULL,
	[Products_Services] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Threat_to_others_detail] [nvarchar](max) NULL,
	[Safety_Concern_Response_Nature_of_concern_Threat_from_others_detail] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_2__Case_Conference__Outcome_Outcome] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Outcome_of_Review] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO