IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_CaseDetailNumber]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailNumber];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailNumber](
	[CaseId] [nvarchar](max) NULL,
	[Safety_Concern_Response_Number_of_students_affected] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO