IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_IndividualDetailNumber]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_IndividualDetailNumber];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_IndividualDetailNumber](
	[Id] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Cost] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Deposit_Amount] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Note_taker_value] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Interim_Support_Claimed_VALUE] [nvarchar](max) NULL,
	[ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Value] [nvarchar](max) NULL,
	[DDS_DSA_Mentoring_Support_Mentor_initial_value] [nvarchar](max) NULL,
	[DDS_DSA_Mentoring_Support_Total_Interim_mentoring_hours_used] [nvarchar](max) NULL,
	[DDS_DSA_Study_Skills_Tuition_Tutor_Initial_Value] [nvarchar](max) NULL,
	[MH__WB_MHWB_Archive_Number_of_sessions] [nvarchar](max) NULL
)