IF OBJECT_ID('[INT].[SRC_Maximizer_PIVOT_CaseDetailString]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailString];
GO

CREATE TABLE [INT].[SRC_Maximizer_PIVOT_CaseDetailString](
	[CaseId] [nvarchar](max) NULL,
	[Fitness_to_Study_Level_1__Informal__Lead_person_name] [nvarchar](max) NULL,
	[MHWB_Duty_Action_s__needed] [nvarchar](max) NULL,
	[MHWB_Duty_Referral_to_other_services_detail] [nvarchar](max) NULL,
	[MHWB_Duty_Referred_by_other___detail] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO