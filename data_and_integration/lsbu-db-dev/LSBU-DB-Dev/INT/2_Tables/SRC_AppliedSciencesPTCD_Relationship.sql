IF OBJECT_ID('[INT].[SRC_AppliedSciencesPTCD_Relationship]','U') IS NOT NULL
	DROP TABLE [INT].[SRC_AppliedSciencesPTCD_Relationship];
GO

CREATE TABLE [INT].[SRC_AppliedSciencesPTCD_Relationship](
	[RunID] [int] NULL,
	[FullFileName] [varchar](100) NULL,
	[SrcSystem] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[RowNo] [int] NULL,
	[FileDateTime] [datetime] NULL,
	[LSB_ExternalID] [varchar](400) NOT NULL,
	[FileRowNumber] [int] NULL,
	[HashMD5] [varbinary](8000) NULL,
	[InsetDateTime] [datetime] NULL,
	[UpdateDateTime] [datetime] NULL,
	[ChangeStatus] [varchar](100) NULL,
	[SFHashMD5] [varbinary](8000) NULL,
	[SFUpdateDateTime] [datetime] NULL,
	[SFStatus] [varchar](100) NULL,
	[SFId] [varchar](100) NULL,
	[PTCDCSV_StudentId] [varchar](4000) NULL,
	[PTCDCSV_StudentName] [varchar](4000) NULL,
	[PTCDCSV_StudentEmailAddress] [varchar](4000) NULL,
	[PTCDCSV_YearofStudy] [varchar](4000) NULL,
	[PTCDCSV_Session] [varchar](4000) NULL,
	[QL_PersonalTutorId] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorFirstName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorLastName] [varchar](4000) NULL,
	[PTCDCSV_PersonalTutorEmailAddress] [varchar](4000) NULL,
	[StaffRank] [varchar](4000) NULL
)