@echo off
setlocal

rem --------------------------------------------------------------------------------------------------
set DBUSER=
set PASSWORD=
set SERVERNAME=
set DBNAME=
rem --------------------------------------------------------------------------------------------------

echo ServerName:%SERVERNAME%
echo DatabaseName:%DBNAME%
pause

set MAINPATH=%~dp0.
for /f "tokens=2 delims==" %%I in ('wmic os get localdatetime /format:list') do set START_DATETIME=%%I
set TIMESTAMP=%START_DATETIME:~0,8%_%START_DATETIME:~8,6%

cd /D %MAINPATH%
DeploymentScript_Core.bat > %DBNAME%__%TIMESTAMP%.log 2>&1

:Exit
  endlocal
