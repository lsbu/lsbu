CREATE OR ALTER PROCEDURE  [DST].[ReplaceRejectTable]
		@tableName nvarchar(128)
AS
DECLARE @cms nvarchar(max) = N''
BEGIN
	SET @cms = 'DROP TABLE IF EXISTS DST.'+@tableName+';
				SELECT * INTO DST.'+@tableName+' FROM DST.'+@tableName+'_tmp;';
	
	PRINT @cms
	EXEC sp_executesql @cms
END
GO


