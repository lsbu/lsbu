IF OBJECT_ID('[DST].[DICT_Contact_StudentStatus]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Contact_StudentStatus];
GO

CREATE TABLE [DST].[DICT_Contact_StudentStatus](
	[QL_StudentStatus] [varchar](255) NULL,
	[LSB_CON_StudentStatus__c] [varchar](255) NULL
) ON [PRIMARY]
GO

INSERT INTO [DST].[DICT_Contact_StudentStatus]
  ( [QL_StudentStatus], [LSB_CON_StudentStatus__c])
VALUES
 ('H','Home'),
 ('O','Overseas'),
 ('N','Home'),
 ('U','UK Based Overseas'),
 ('E','EU'),
 ('C','Channel I/IoM')
GO
