IF OBJECT_ID('[DST].[DICT_Additional_Support]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Additional_Support];
GO

CREATE TABLE [DST].[DICT_Additional_Support](
	[Source_val] [varchar](255) NULL,
	[DST_val] [varchar](255) NULL
)
GO

INSERT INTO [DST].[DICT_Additional_Support]
VALUES
('0','0'),
('1','1'),
('135 for S1','135'),
('15','15'),
('166.5 for S1','166.5'),
('180','180'),
('2','2'),
('3','3'),
('44319','44319'),
('30 x 1','30'),
('4','4'),
('5','5'),
('92 for S1','92'),
('Study Assistant - ','0')
GO
