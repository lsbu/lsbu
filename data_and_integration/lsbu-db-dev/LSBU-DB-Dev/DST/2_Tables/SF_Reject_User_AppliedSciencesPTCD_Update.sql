IF OBJECT_ID('[DST].[SF_Reject_User_AppliedSciencesPTCD_Update]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_User_AppliedSciencesPTCD_Update];
GO

CREATE TABLE [DST].[SF_Reject_User_AppliedSciencesPTCD_Update](
	[Id] [varchar](18) NOT NULL,
	[ContactId] [varchar](18) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)