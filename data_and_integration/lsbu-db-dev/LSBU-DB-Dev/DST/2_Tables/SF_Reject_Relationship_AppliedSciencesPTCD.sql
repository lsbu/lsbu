IF OBJECT_ID('[DST].[SF_Reject_Relationship_AppliedSciencesPTCD]','U') IS NOT NULL
  DROP TABLE [DST].[SF_Reject_Relationship_AppliedSciencesPTCD];
GO

CREATE TABLE [DST].[SF_Reject_Relationship_AppliedSciencesPTCD](
	[LSB_RTP_ExternalId__c] [varchar](90) NULL,
	[LSB_RTP_SourceSystemID__c] [varchar](90) NULL,
	[LSB_RTP_SourceSystem__c] [varchar](255) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Description__c] [varchar](max) NULL,
	[hed__ReciprocalRelationship__c] [varchar](18) NULL,
	[hed__RelatedContact__c] [varchar](18) NULL,
	[hed__Relationship_Explanation__c] [varchar](1300) NULL,
	[hed__Status__c] [varchar](255) NULL,
	[hed__Type__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
);