IF OBJECT_ID('[DST].[SF_Reject_Delete_CaseTeamMember_AppliedSciencesPTCD]','U') IS NOT NULL
  DROP TABLE [DST].[SF_Reject_Delete_CaseTeamMember_AppliedSciencesPTCD];
GO

CREATE TABLE [DST].[SF_Reject_Delete_CaseTeamMember_AppliedSciencesPTCD](
	[Id] [varchar](18) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
);