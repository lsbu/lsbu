IF OBJECT_ID('[DST].[SF_Reject_Load_CaseTeamMember_ModuleLeaderCourseDirector]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Load_CaseTeamMember_ModuleLeaderCourseDirector];
GO

CREATE TABLE [DST].[SF_Reject_Load_CaseTeamMember_ModuleLeaderCourseDirector](
	[ParentId] [varchar](18) NULL,
	[MemberId] [varchar](18) NULL,
	[TeamTemplateMemberId] [varchar](18) NULL,
	[TeamRoleId] [varchar](18) NULL,
	[TeamTemplateId] [varchar](18) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)