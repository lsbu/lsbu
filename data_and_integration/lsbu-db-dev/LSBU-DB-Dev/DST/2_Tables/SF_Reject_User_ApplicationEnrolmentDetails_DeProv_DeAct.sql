IF OBJECT_ID('[DST].[SF_Reject_User_ApplicationEnrolmentDetails_DeProv_DeAct]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_User_ApplicationEnrolmentDetails_DeProv_DeAct];
GO

CREATE TABLE [DST].[SF_Reject_User_ApplicationEnrolmentDetails_DeProv_DeAct](
	[Id] [varchar](18) NOT NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)