IF OBJECT_ID('[DST].[SF_Reject_SupportProfile_Student]','U') IS NOT NULL
  DROP TABLE [DST].[SF_Reject_SupportProfile_Student];
GO

CREATE TABLE [DST].[SF_Reject_SupportProfile_Student] (
  [LSB_SUE_ExternalID__c] [varchar](90) NULL,
  [LSB_SUE_SourceSystemID__c] [varchar](90) NULL,
  [LSB_SUE_SourceSystem__c] [varchar](255) NULL,
  [Name] [varchar](80) NULL,
  [LSB_SUE_Contact__c] [varchar](18) NULL,
  [LSB_SUE_Status__c] [varchar](255) NULL,
  [errorCode] [varchar](255) NULL,
  [errorFields] [varchar](255) NULL,
  [errorMessage] [varchar](8000) NULL
)
GO
