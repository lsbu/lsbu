IF OBJECT_ID('[DST].[DICT_Additional_Support_EP_Room]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Additional_Support_EP_Room];
GO


CREATE TABLE [DST].[DICT_Additional_Support_EP_Room](
	[Source_val] [varchar](255) NULL,
	[DST_val] [varchar](255) NULL
) ON [PRIMARY]
GO


INSERT INTO [DST].[DICT_Additional_Support_EP_Room]
  ( [Source_val], [DST_val])
VALUES
('Borough Road Building-BR 328','Borough Road Building-BR 328'),
('Caxton House- DDS Room','Caxton House- DDS Room'),
('Caxton House- Room G6','Caxton House- Room G6'),
('K 610 in Keyworth Centre','K 610 in Keyworth Centre'),
('K2 Building, Room V202','K2 Building- Room V202'),
('K2 Building, Room V217','K2 Building- Room V217'),
('K2 Building, Room V401','K2 Building- Room V401'),
('K2 Building, Room V402','K2 Building- Room V402'),
('K2 Building, Room V403','K2 Building- Room V403'),
('K2 Building, Room V404','K2 Building- Room V404'),
('K2 Building, Room V405','K2 Building- Room V405'),
('K2 Building, Room V406','K2 Building- Room V406'),
('K2 Building, Room V407','K2 Building- Room V407'),
('K2 Building, Room V408','K2 Building- Room V408'),
('K2 Building, Room V409','K2 Building- Room V409'),
('K-309 in Keyworth Centre','K-309 in Keyworth Centre'),
('Keyworth Centre  Room K411','Keyworth Centre Room K411'),
('Keyworth Centre, K601','Keyworth Centre- K601'),
('Keyworth Centre: K613','Keyworth Centre- K613'),
('Keyworth Centre Room K201','Keyworth Centre Room K201'),
('Keyworth Centre Room K209','Keyworth Centre Room K209'),
('Keyworth Centre Room K211','Keyworth Centre Room K211'),
('Keyworth Centre Room K224','Keyworth Centre Room K224'),
('Keyworth Centre Room K301','Keyworth Centre Room K301'),
('Keyworth Centre Room K309','Keyworth Centre Room K309'),
('Keyworth Centre Room K402','Keyworth Centre Room K402'),
('Keyworth Centre Room K409','Keyworth Centre Room K409'),
('Keyworth Centre Room K411','Keyworth Centre Room K411'),
('Keyworth Centre Room K413','Keyworth Centre Room K413'),
('Keyworth Centre Room K417','Keyworth Centre Room K417'),
('Keyworth Centre Room K419','Keyworth Centre Room K419'),
('Keyworth Centre Room K420','Keyworth Centre Room K420'),
('Keyworth Centre Room K501','Keyworth Centre Room K501'),
('Keyworth Centre Room, K501','Keyworth Centre Room- K501'),
('Keyworth Centre Room K502','Keyworth Centre Room K502'),
('Keyworth Centre Room K509','Keyworth Centre Room K509'),
('Keyworth Centre, Room K509','Keyworth Centre- Room K509'),
('Keyworth Centre Room K511','Keyworth Centre Room K511'),
('Keyworth Centre Room K513','Keyworth Centre Room K513'),
('Keyworth Centre Room K515','Keyworth Centre Room K515'),
('Keyworth Centre Room K517','Keyworth Centre Room K517'),
('Keyworth Centre Room K601','Keyworth Centre Room K601'),
('Keyworth Centre Room K602','Keyworth Centre Room K602'),
('Keyworth Centre Room K609','Keyworth Centre Room K609'),
('Keyworth centre Room K610','Keyworth centre Room K610'),
('Keyworth Centre Room K611','Keyworth Centre Room K611'),
('Keyworth Centre Room K613','Keyworth Centre Room K613'),
('Keyworth Centre Room K615','Keyworth Centre Room K615'),
('Keyworth Centre Room K617','Keyworth Centre Room K617'),
('Keyworth Centre Room K620','Keyworth Centre Room K620'),
('London Road - LR 237','London Road - LR 237'),
('London Road - LR 257','London Road - LR 257'),
('London Road - LR 338','London Road - LR 338'),
('London Road Building - LR 119','London Road Building - LR 119'),
('London Road Building - LR 229','London Road Building - LR 229'),
('London Road Building - LR 252','London Road Building - LR 252'),
('London Road Building - LR400','London Road Building - LR400'),
('London Road Building LR-334','London Road Building LR-334'),
('London Road Building LR-398','London Road Building LR-398'),
('London Road: LR-A4','London Road: LR-A4'),
('London Road: LR-A7','London Road: LR-A7'),
('Perry Library (please report to DDS Office)','Perry Library (please report to DDS Office)'),
('Remote','Remote'),
('Student Life Centre Room T128 - Report to Helpdesk','Student Life Centre Room T128 - Report to Helpdesk'),
('Student Life Centre Room T130 - Report to Helpdesk','Student Life Centre Room T130 - Report to Helpdesk'),
('Student Life Centre Room T132 - Report to Helpdesk','Student Life Centre Room T132 - Report to Helpdesk'),
('Tech - 1B27','Tech - 1B27'),
('Tech GB13','Tech GB13'),
('TECH GD07 -A','TECH GD07 -A'),
('Technopark 1A13','Technopark 1A13'),
('To be confirmed - (DDS will confirm the location before your appointment)','To be confirmed - (DDS will confirm the location before your appointment)')
GO

