IF OBJECT_ID('[DST].[SF_Reject_Affiliation]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Affiliation];
GO

CREATE TABLE [DST].[SF_Reject_Affiliation](
	[hed__Account__c] [varchar](18) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Description__c] [varchar](max) NULL,
	[hed__EndDate__c] [datetime] NULL,
	[hed__Primary__c] [bit] NULL,
	[hed__Role__c] [varchar](255) NULL,
	[hed__StartDate__c] [datetime] NULL,
	[hed__Status__c] [varchar](255) NULL,
	[LSB_AON_ExternalID__c] [varchar](90) NULL,
	[LSB_AON_SourceSystemID__c] [varchar](90) NULL,
	[LSB_AON_SourceSystem__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)