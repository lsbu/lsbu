﻿IF OBJECT_ID('[DST].[SF_Reject_TermParent]','U') IS NOT NULL
  DROP TABLE [DST].[SF_Reject_TermParent];
GO

CREATE TABLE [DST].[SF_Reject_TermParent] (
    [Name]                            VARCHAR (80)   NULL,
    [hed__Account__c]                 VARCHAR (18)   NULL,
    [hed__End_Date__c]                DATETIME       NULL,
    [hed__Start_Date__c]              DATETIME       NULL,
    [hed__Grading_Period_Sequence__c] REAL           NULL,
    [hed__Instructional_Days__c]      REAL           NULL,
    [hed__Parent_Term__c]             VARCHAR (18)   NULL,
    [hed__Type__c]                    VARCHAR (255)  NULL,
    [LSB_TRM_ExternalID__c]           VARCHAR (90)   NULL,
    [LSB_TRM_SourceSystemID__c]       VARCHAR (90)   NULL,
    [LSB_TRM_SourceSystem__c]         VARCHAR (255)  NULL,
    [errorCode]                       VARCHAR (255)  NULL,
    [errorFields]                     VARCHAR (255)  NULL,
    [errorMessage]                    VARCHAR (8000) NULL
);

