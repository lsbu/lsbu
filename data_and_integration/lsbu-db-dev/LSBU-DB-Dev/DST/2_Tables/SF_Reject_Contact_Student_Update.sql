﻿IF OBJECT_ID('[DST].[SF_Reject_Contact_Student_Update]','U') IS NOT NULL
  DROP TABLE [DST].[SF_Reject_Contact_Student_Update];
GO

CREATE TABLE [DST].[SF_Reject_Contact_Student_Update](
	[Id] [varchar](18) NULL,
	[LSB_CON_User__c] [varchar](18) NULL,
	[LSB_CON_AdviseeCaseRecord__c] [varchar](18) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
);

