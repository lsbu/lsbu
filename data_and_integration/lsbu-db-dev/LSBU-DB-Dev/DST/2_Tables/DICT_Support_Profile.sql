IF OBJECT_ID('[DST].[DICT_Support_Profile]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Support_Profile];
GO

CREATE TABLE [DST].[DICT_Support_Profile](
	[Adviser] [varchar](255) NULL,
	[User_name] [varchar](255) NULL
)
GO

INSERT INTO [DST].[DICT_Support_Profile]
	([Adviser],
	[User_name])
VALUES
('Stephen Anderson', 	NULL),
('Mary Atito', 	'atitom2'),
('Hayley Mullender', 	'MULLENDH'),
('Tayla Vella', 	NULL),
('Eleanor Braganza', 	'braganze'),
('Nicola Smith', 	'smithn8'),
('Macey Ho', 	'hom4'),
('Makeba Garraway', 	NULL),
('Alexander Ross', 	NULL),
('Annie Jennings', 	'JENNINA3'),
('Rosie Holden', 	'holdenr3'),
('Ben Walford', 	NULL)