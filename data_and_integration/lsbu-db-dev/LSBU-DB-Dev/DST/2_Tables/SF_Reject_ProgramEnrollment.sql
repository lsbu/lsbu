IF OBJECT_ID('[DST].[SF_Reject_ProgramEnrollment]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_ProgramEnrollment];
GO

CREATE TABLE [DST].[SF_Reject_ProgramEnrollment](
	[hed__End_Date__c] [datetime] NULL,
	[hed__Enrollment_Status__c] [varchar](255) NULL,
	[LSB_PEN_EnrolmentStatusChangeDate__c] [datetime] NULL,
	[hed__Affiliation__c] [varchar](18) NULL,
	[hed__Application_Submitted_Date__c] [date] NULL,
	[hed__Class_Standing__c] [varchar](255) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Account__c] [varchar](18) NULL,
	[hed__Start_Date__c] [date] NULL,
	[LSB_PEN_SourceSystem__c] [varchar](255) NULL,
	[LSB_PEN_SourceSystem_ID__c] [varchar](90) NULL,
	[LSB_PEN_ExternalID__c] [varchar](90) NULL,
	[LSB_PEN_Application__c] [varchar](18) NULL,
	[LSB_PEN_ModeofStudy__c] [varchar](255) NULL,
	[LSB_PEN_EnrolmentType__c] [varchar](255) NULL,
	[LSB_PEN_CampusInformation__c] [varchar](255) NULL,
	[LSB_PEN_SessionCode__c] [varchar](90) NULL,
	[LSB_PEN_SessionDescription__c] [varchar](255) NULL,
	[LSB_PEN_Term__c] [varchar](18) NULL,
	[LSB_PEN_Graduated__c] [bit] NOT NULL,
	[LSB_PEN_LatestEnrolmentRecord__c] [bit] NOT NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)