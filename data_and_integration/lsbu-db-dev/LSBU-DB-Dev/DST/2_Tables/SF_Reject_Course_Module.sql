IF OBJECT_ID('[DST].[SF_Reject_Course_Module]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Course_Module];
GO

CREATE TABLE [DST].[SF_Reject_Course_Module](
	[Name] [varchar](80) NULL,
	[hed__Account__c] [varchar](18) NULL,
	[hed__Course_ID__c] [varchar](255) NULL,
	[LSB_CSE_ExternalID__c] [varchar](90) NULL,
	[LSB_CSE_SourceSystemID__c] [varchar](90) NULL,
	[LSB_CSE_SourceSystem__c] [varchar](255) NULL,
	[LSB_CSE_ModuleLevel__c] [varchar](20) NULL,
	[LSB_CSE_ModuleStatus__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)