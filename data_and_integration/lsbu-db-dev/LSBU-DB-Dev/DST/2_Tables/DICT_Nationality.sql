IF OBJECT_ID('[DST].[DICT_Nationality]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Nationality];
GO

CREATE TABLE [DST].[DICT_Nationality](
	[SourceValue] [varchar](255) NULL,
	[DestinationValue] [varchar](255) NULL
) ON [PRIMARY]
GO

INSERT INTO [DST].[DICT_Nationality] VALUES
('ABU 1764A','Abu Dhabi'),
('ABYS1648A','Abyssinia'),
('ADEN1601A','Aden'),
('AFAR1749A','Afars and Issas Territory'),
('AFGH1602A','Afghanistan'),
('AFRI9999A','Africa, not otherwise specified'),
('AJMA1764A','Ajman'),
('ALBA1603A','Albania'),
('ALGE1604A','Algeria'),
('AMER1796A','American Trust Territories'),
('AMER1796B','American Samoa'),
('ANDO1605A','Andorra'),
('ANGO1606A','Angola'),
('ANGU1824A','Anguilla'),
('ANTA1801A','Antarctica British'),
('ANTA1822A','Antarctica French'),
('ANTA9999A','Antarctica and Oceania, not otherwise specified'),
('ANTI1607A','Antigua and Barbuda'),
('ANTI1637A','Antilles(Netherlands)'),
('ARGE1608A','Argentina'),
('ARME1836A','Armenia'),
('ARUB1637A','Aruba'),
('ASCE1735A','Ascension'),
('ASIA9999A','Asia (Except Middle East), not otherwise specified'),
('AUST1609A','Australia'),
('AUST1610A','Austria'),
('AZER1837A','Azerbaijan'),
('AZOR1728A','Azores'),
('BAHA1611A','Bahamas'),
('BAHR1612A','Bahrain'),
('BALE1751A','Balearic Islands'),
('BANG1787A','Bangladesh'),
('BARB1607A','Barbuda and Antigua'),
('BARB1613A','Barbados'),
('BASU1690A','Basutoland'),
('BECH1618A','Bechuanaland'),
('BELA1838A','Belarus'),
('BELG1614A','Belgium'),
('BELG1633A','Belgian Congo'),
('BELI1668A','Belize'),
('BENI1640A','Benin'),
('BERM1615A','Bermuda'),
('BHUT1616A','Bhutan'),
('BOLI1617A','Bolivia'),
('BONA1637A','Bonaire'),
('BORN1673A','Borneo South'),
('BORN1698A','Borneo North'),
('BOSN1853A','Bosnia and Herzegovina'),
('BOTS1618A','Botswana'),
('BRAZ1619A','Brazil'),
('BRIT1665A','British Guiana'),
('BRIT1668A','British Honduras'),
('BRIT1776A','British Virgin Islands'),
('BRIT1801A','British Antarctica'),
('BRIT1801B','British Antarctic Territory'),
('BRIT1829A','British Indian Ocean Territory'),
('BRUN1620A','Brunei'),
('BULG1621A','Bulgaria'),
('BURK1769A','Burkina-Faso'),
('BURK1769B','Burkina'),
('BURM1622A','Burma'),
('BURU1623A','Burundi'),
('BYEL1838A','Byelorussia'),
('CABI1606A','Cabinda'),
('CAMB1624A','Cambodia'),
('CAME1625A','Cameroon'),
('CANA1626A','Canada'),
('CANA1751A','Canary Islands'),
('CAPE1788A','Cape Verde Islands'),
('CARI9999A','Caribbean, not otherwise specified'),
('CARO1862A','Caroline Islands'),
('CAYM1789A','Cayman Islands'),
('CELE1673A','Celebes'),
('CENT1627A','Central African Republic'),
('CENT9999A','Central America, not otherwise specified'),
('CEUT1751A','Ceuta'),
('CEYL1628A','Ceylon'),
('CHAD1629A','Chad'),
('CHAG1829A','Chagos Archipelago'),
('CHAN3826A','Channel Islands (The)'),
('CHAN3826B','Guernsey'),
('CHAN3826C','Jersey'),
('CHAN3826D','CHAN3826D'),
('CHAN9999A','Channel Islands, not otherwise specified'),
('CHIL1630A','Chile'),
('CHIN1631A','China (People''s Republic of)'),
('CHIN1652A','China (Taiwan)'),
('CHRI1609A','Christmas Island'),
('CHUU1862A','Chuuk'),
('CIS 1772A','CIS (Commonwealth of Independent States)'),
('COCO1609A','Cocos Islands'),
('COKE1714A','Cokelau'),
('COLO1632A','Colombia'),
('COMO1804A','Comoros'),
('CONG1633A','Congo (Democratic Republic)'),
('CONG1634A','Congo (People''s Republic)'),
('CONG1634B','Congolese Republic Brazzaville'),
('COOK1714A','Cook Islands'),
('CORF1661A','Corfu'),
('COST1635A','Costa Rica'),
('COTE1679A','Cote d''lvore'),
('CRET1661A','Crete'),
('CROA1834A','Croatia'),
('CUBA1636A','Cuba'),
('CURA1637A','Curacao (Dutch)'),
('CYPR1638A','Cyprus, not otherwise specified'),
('CYPR1638B','Cyprus (European Union)'),
('CYPR1638C','Cyprus (Non-European Union)'),
('CZEC1639A','Czech Republic'),
('CZEC1639B','Czechoslovakia'),
('CZEC9999A','Czechoslovakia not otherwise specified'),
('DAHO1640A','Dahomey'),
('DEMO1685A','Democratic People''s Republic of Korea'),
('DENM1641A','Denmark'),
('DJIB1749A','Djibouti'),
('DOMI1642A','Dominica'),
('DOMI1643A','Dominican Republic'),
('DUBA1764A','Dubai'),
('DUTC1637A','Dutch Curacao'),
('DUTC1637B','Dutch West Indies'),
('DUTC1753A','Dutch Guiana'),
('EAST1673A','East Timor'),
('EAST1707A','East Africa (Portugese)'),
('EAST1787A','East Pakistan'),
('ECUA1645A','Ecuador'),
('EGYP1768A','Egypt'),
('EIRE1676A','Eire'),
('EL S1646A','El Salvador'),
('ELLI1647A','Ellice Islands'),
('ENGL5826A','England'),
('EQUA1790A','Equatorial Guinea'),
('ERIT1860A','Eritrea'),
('ESTO1831A','Estonia'),
('ETHI1648A','Ethiopia'),
('EURO9999A','Europe, not otherwise specified'),
('EURO9999B','European Union, not otherwise specified'),
('FALK1649A','Falkland Islands'),
('FARO1828A','Faroe Islands'),
('FIJI1650A','Fiji'),
('FINL1651A','Finland'),
('FINL1651B','Aland Islands {Ahvenamaa}'),
('FORM1652A','Formosa'),
('FRAN1653A','France'),
('FREN1653A','French Guiana'),
('FREN1653B','French West Indies'),
('FREN1653C','French Overseas Depts (DCMS)'),
('FREN1749A','French Territory of the Afars and Issas'),
('FREN1749B','French Somaliland'),
('FREN1821A','French Territorial Collectives'),
('FREN1822A','French Possessions nor elsewhere classified'),
('FREN1822B','French Overseas Territories (TCMS)'),
('FREN1822C','French Polynesia'),
('FUJA1764A','Fujairah'),
('FUTU1822A','Futuna (Wallis and )'),
('GABO1654A','Gabon'),
('GALA1645A','Galapagos'),
('GAMB1655A','Gambia'),
('GEOR1847A','Georgia'),
('GERM1656A','Germany (West)'),
('GERM1656B','Germany (Bundesrepublik)'),
('GERM1656C','Germany'),
('GERM1656D','Germany Federal Republic of'),
('GERM1657A','Germany (Democraticrepublik)'),
('GERM1657B','Germany (East)'),
('GHAN1658A','Ghana'),
('GIBR1659A','Gibraltar'),
('GILB1660A','Gilbert Islands'),
('GOLD1658A','Gold Coast'),
('GREA1692A','Great Socialist People''s Libyan Arab Jamahiriya'),
('GREE1661A','Greece'),
('GREE1828A','Greenland'),
('GREN1662A','Grenada'),
('GREN1738A','Grenadines'),
('GUAD1653A','Guadeloupe'),
('GUAM1796A','Guam'),
('GUAT1663A','Guatemala'),
('GUIA1653A','Guiana (French)'),
('GUIA1665A','Guiana (British)'),
('GUIA1753A','Guiana (Dutch)'),
('GUIN1664A','Guinea'),
('GUIN1664B','Guinea (French)'),
('GUIN1790A','Guinea (Equatorial)'),
('GUIN1790B','Guinea (Spanish)'),
('GUIN1802A','Guinea-Bissau'),
('GUIN1802B','Guinea (Portuguese)'),
('GUYA1665A','Guyana'),
('HAIT1666A','Haiti'),
('HOLL1710A','Holland'),
('HOLY1678A','Holy See'),
('HOND1667A','Honduras'),
('HOND1668A','Honduras British'),
('HONG1669A','Hong Kong'),
('HUNG1670A','Hungary'),
('ICEL1671A','Iceland'),
('INDI1672A','India'),
('INDI1829A','Indian Ocean Territory (British)'),
('INDO1673A','Indonesia'),
('IRAN1674A','Iran'),
('IRAQ1675A','Iraq'),
('IREL1676A','Ireland (Republic of)'),
('IRIA1673A','Irian Jaya'),
('IRIA1673B','Irian Barat'),
('IRIA1673C','Irian (West)'),
('IRIS1676A','Irish Republic'),
('ISLE4826A','Isle of Man (The)'),
('ISRA1677A','Israel'),
('ITAL1678A','Italy'),
('IVOR1679A','Ivory Coast'),
('JAMA1680A','Jamaica'),
('JAPA1681A','Japan'),
('JAVA1673A','Java'),
('JORD1682A','Jordan'),
('KALI1673A','Kalimantan (South Borneo)'),
('KAMP1624A','Kampuchea'),
('KAZA1839A','Kazakhstan'),
('KEEL1609A','Keeling Islands'),
('KENY1683A','Kenya'),
('KHME1624A','Khmer Republic'),
('KIRG1840A','Kirgizia'),
('KIRI1660A','Kiribati'),
('KORE1684A','Korea South'),
('KORE1685A','Korea North'),
('KOSR1862A','Kosrae'),
('KUWA1686A','Kuwait'),
('KYRG0417A','Kyrgyz Republic'),
('KYRG1840A','Kyrgyzstan'),
('LAO 1687A','Lao People''s Democratic Republic'),
('LAOS1687A','Laos'),
('LATV1832A','Latvia'),
('LEBA1688A','Lebanon'),
('LEEW1689A','Leeward Islands (not otherwise specified)'),
('LESO1690A','Lesotho'),
('LIBE1691A','Liberia'),
('LIBY1692A','Libya'),
('LIBY1692B','Libyan Arab Jamahiriya, Great Socialist People''s'),
('LIEC1827A','Liechtenstein'),
('LITH1833A','Lithuania'),
('LOYA1822A','Loyalty Islands'),
('LUXE1693A','Luxembourg'),
('MACA1694A','Macao'),
('MACE0807A','Macedonia, The Former Yugoslav Republic'),
('MACE1851A','Macedonia (Skopje)'),
('MADA1695A','Madagascar'),
('MADE1728A','Madeira'),
('MALA1695A','Malagasy Republic'),
('MALA1696A','Malawi'),
('MALA1698A','Malaya'),
('MALA1698B','Malaysia'),
('MALD1793A','Maldive Islands'),
('MALD1793B','Maldives'),
('MALI1699A','Mali'),
('MALT1700A','Malta'),
('MALU1673A','Maluku'),
('MARQ1822A','Marquesas Islands'),
('MARS1861A','Marshall Islands'),
('MART1653A','Martinique'),
('MAUR1701A','Mauritania'),
('MAUR1702A','Mauritius'),
('MAYO1821A','Mayotte'),
('MELI1751A','Melilla'),
('MEXI1703A','Mexico'),
('MICR1862A','Micronesia'),
('MIDD9999A','Middle East, not otherwise specified'),
('MIQU1653A','Miquelon (St Pierre and )'),
('MOLD1841A','Moldavia'),
('MOLD1841B','Moldova'),
('MOLL1673A','Molluccas'),
('MONA1825A','Monaco'),
('MONG1704A','Mongolia'),
('MONS1705A','Monserrat'),
('MONT9999A','Montenegro'),
('MORO1706A','Morocco'),
('MOZA1707A','Mozambique'),
('MUSC1708A','Muscat and Oman'),
('MYAN1622A','Myanmar'),
('NAMI1798A','Namibia'),
('NAUR1805A','Nauru'),
('NEPA1709A','Nepal'),
('NETH1637A','Netherlands Antilles'),
('NETH1710A','Netherlands'),
('NEW 1713A','New Hebrides'),
('NEW 1714A','New Zealand'),
('NEW 1723A','New Guinea'),
('NEW 1822A','New Caledonia'),
('NICA1715A','Nicaragua'),
('NIGE1716A','Niger'),
('NIGE1717A','Nigeria'),
('NIUE1714A','Niue'),
('NORF1609A','Norfolk Island'),
('NORT1685A','North Korea'),
('NORT1698A','North Borneo'),
('NORT1771A','Northern Marianas'),
('NORT1779A','North Yemen'),
('NORT1781A','North Rhodesia'),
('NORT8826A','Northern Ireland'),
('NORT9999A','North America, not otherwise specified'),
('NORW1718A','Norway'),
('NORW1718B','Svalbard And Jan Mayen'),
('NOT 1782A','Not known'),
('NYAS1696A','Nyasaland'),
('OMAN1708A','Oman'),
('OUTE1704A','Outer Mongolia'),
('PACI1796A','Pacific Territories US'),
('PAKI1721A','Pakistan (West)'),
('PAKI1721B','Pakistan'),
('PAKI1787A','Pakistan (East)'),
('PALA1796A','Palau'),
('PANA1722A','Panama'),
('PAPU1723A','Papua New Guinea'),
('PARA1724A','Paraguay'),
('PEOP1601A','People''s Democratic Republic of Yemen'),
('PERS1674A','Persia'),
('PERU1725A','Peru'),
('PHIL1726A','Philippines'),
('PITC1823A','Pitcairn Islands'),
('POLA1727A','Poland'),
('POLY1822A','Polynesia (French)'),
('PONA1862A','Ponape'),
('PORT1606A','Portuguese West Africa'),
('PORT1673A','Portuguese Timor'),
('PORT1707A','Portuguese East Africa'),
('PORT1728A','Portugal'),
('PORT1802A','Portuguese Guinea'),
('PRIN1803A','Principe (Sao Tome)'),
('PUER1730A','Puerto Rico'),
('QATA1731A','Qatar'),
('RAS 1764A','Ras al-Kaimah'),
('REPU1684A','Republic of Korea (South Korea)'),
('REUN1653A','Reunion'),
('RHOD1732A','Rhodesia Southern'),
('RHOD1781A','Rhodesia Northern'),
('ROMA1733A','Romania'),
('RUMA1733A','Rumania'),
('RUSS1842A','Russia'),
('RWAN1734A','Rwanda'),
('SABA1637A','Saba'),
('SABA1698A','Sabah'),
('SAHA1706A','Sahara (West)'),
('SALV1646A','Salvador'),
('SAMO1741A','Samoa Western'),
('SAMO1796A','Samoa (US)'),
('SAN 1826A','San Marino'),
('SAO 1803A','Sao Tome and Principe'),
('SARA1698A','Sarawak'),
('SAUD1743A','Saudi Arabia'),
('SCOT7826A','Scotland'),
('SENE1655A','Senegambia'),
('SENE1785A','Senegal'),
('SERB1780A','Serbia And Montenegro'),
('SEYC1744A','Seychelles'),
('SHAR1764A','Sharjah'),
('SIAM1760A','Siam'),
('SIER1745A','Sierra Leone'),
('SIKK1672A','Sikkim'),
('SING1746A','Singapore'),
('SKOP1851A','Skopje'),
('SLOV1835A','Slovenia'),
('SLOV1850A','Slovakia'),
('SOCI1822A','Society Islands'),
('SOCO1601A','Socotra'),
('SOLO1747A','Solomon Islands'),
('SOMA0706A','Somaliland'),
('SOMA1748A','Somali Democratic Republic'),
('SOMA1748B','Somalia'),
('SOMA1749A','Somaliland (French)'),
('SOUT1601A','South Yemen'),
('SOUT1673A','South Borneo'),
('SOUT1684A','South Korea'),
('SOUT1732A','Southern Rhodesia'),
('SOUT1750A','South Africa'),
('SOUT1798A','South West Africa'),
('SOUT1830A','South Georgia and the South Sandwich Islands'),
('SOUT9999A','South America, not otherwise specified'),
('SPAI1751A','Spain'),
('SPAI1751B','Spain (Except Canary Islands)'),
('SPAI1751C','Spain, not otherwise specified'),
('SPAN1790A','Spanish Guinea'),
('SRI 1628A','Sri Lanka'),
('ST E1637A','St Eustatius'),
('ST H1735A','St Helena (incl deps)'),
('ST K1736A','St Kitts Nevis'),
('ST L1737A','St Lucia'),
('ST M1637A','St Martin (South)'),
('ST M1653A','St Martin (North)'),
('ST P1653A','St Pierre and Miquelon'),
('ST V1738A','St Vincent and the Grenadines'),
('ST. 1736A','St. Christopher and Nevis'),
('STAT1783A','Stateless'),
('SUDA1752A','Sudan'),
('SULA1673A','Sulawesi'),
('SUMA1673A','Sumatra'),
('SURI1753A','Surinam'),
('SURI1753B','Suriname'),
('SWAZ1754A','Swaziland'),
('SWED1755A','Sweden'),
('SWIT1756A','Switzerland'),
('SYRI1757A','Syrian Arab Republic'),
('SYRI1757B','Syria'),
('TAHI1822A','Tahiti'),
('TAIW1652A','Taiwan'),
('TAJI1843A','Tajikistan'),
('TANG1706A','Tangier'),
('TANG1759A','Tanganyika'),
('TANZ1759A','Tanzania'),
('TCHA1629A','Tchad'),
('THAI1760A','Thailand'),
('TIBE1631A','Tibet'),
('TIMO1673A','Timor (West)'),
('TIMO1673B','Timor (East) Portuguese'),
('TOGO1762A','Togo'),
('TOKE1714A','Tokelau'),
('TONG1784A','Tonga'),
('TRIN1763A','Trinidad and Tobago'),
('TRIS1735A','Tristan de Cunha'),
('TRUC1764A','Trucial States'),
('TRUK1862A','Truk'),
('TRUS1796A','Trust Territories American'),
('TUNI1765A','Tunisia'),
('TURK1766A','Turkey'),
('TURK1799A','Turks and Caicos Islands'),
('TURK1844A','Turkmenistan'),
('TUVA1647A','Tuvalu'),
('UGAN1767A','Uganda'),
('UKRA1845A','Ukraine'),
('UMM 1764A','Umm al-Qaiwan'),
('UNIT1764A','United Arab Emirates'),
('UNIT1768A','United Arab Republic'),
('UNIT1771A','United States'),
('UNIT2826A','United Kingdom (excluding the Channel Islands and IOM)'),
('UNIT2826B','United Kingdom, not otherwise specified'),
('UPPE1769A','Upper Volta'),
('URUG1770A','Uruguay'),
('URUN1623A','Urundi'),
('US T1796A','US Trust Territories of the Pacific Islands'),
('USA 1771A','USA'),
('USSR1772A','USSR'),
('USSR1772B','USSR (not otherwise specified)'),
('UZBE1846A','Uzbekistan'),
('VANU1713A','Vanuatu'),
('VATI1678A','Vatican City'),
('VENE1773A','Venezuela'),
('VIET1774A','Vietnam'),
('VIET1774B','Vietnam (North)'),
('VIRG1771A','Virgin Is (US)'),
('VIRG1776A','Virgin Is (British)'),
('WALE6826A','Wales'),
('WALL1822A','Wallis and Futuna'),
('WALV1750A','Walvis Bay'),
('WEST1606A','West Africa (Portuguese)'),
('WEST1673A','West Timor'),
('WEST1673B','West Irian'),
('WEST1706A','Western Sahara'),
('WEST1741A','Westem Samoa'),
('WEST1777A','West Indies (not otherwise specified)'),
('WEST9999A','West Bank (inc. East Jerusalem) and Gaza Strip'),
('WIND1778A','Windward Islands (not elsewhere specified)'),
('YAP 1862A','Yap'),
('YEME1601A','Yemen (Republic of)'),
('YEME1601B','Yemen (People''s Democratic Republic)'),
('YEME1601C','Yemen (South)'),
('YEME1779A','Yemen (North)'),
('YEME1779B','Yemen Arab Republic'),
('YUGO1780A','Yugoslavia'),
('YUGO9999A','Yugoslavia not otherwise specified'),
('ZAIR1633A','Zaire'),
('ZAMB1781A','Zambia'),
('ZANZ1759A','Zanzibar'),
('ZIMB1732A','Zimbabwe')
GO
