IF OBJECT_ID('[DST].[SF_Reject_ProgramEnrollment_StudentEngagementDetails]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_ProgramEnrollment_StudentEngagementDetails];
GO

CREATE TABLE [DST].[SF_Reject_ProgramEnrollment_StudentEngagementDetails](
	[Id] [varchar](18) NULL,
	[LSB_PEN_NoMoodleLogins__c] [real] NULL,
	[LSB_PEN_NoOfLIbrarySwipeIns__c] [real] NULL,
	[LSB_PEN_NoTurnstileSwipreIns__c] [real] NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)