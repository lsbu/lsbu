IF OBJECT_ID('[DST].[SF_Reject_ProgramEnrollment_Cog_StudentEngagementDetails]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_ProgramEnrollment_Cog_StudentEngagementDetails];
GO

CREATE TABLE [DST].[SF_Reject_ProgramEnrollment_Cog_StudentEngagementDetails](
	[Id] [varchar](18) NOT NULL,
	[LSB_PEN_NoMoodleLogins__c] [real] NULL,
	[LSB_PEN_NoOfLIbrarySwipeIns__c] [real] NULL,
	[LSB_PEN_NoTurnstileSwipreIns__c] [real] NULL,
	[errorCode] [varchar](255) NOT NULL,
	[errorFields] [varchar](255) NOT NULL,
	[errorMessage] [varchar](8000) NOT NULL
)