IF OBJECT_ID('[DST].[SF_Reject_Delete_Contact_StudentTeachingStaff]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Delete_Contact_StudentTeachingStaff];
GO

CREATE TABLE [DST].[SF_Reject_Delete_Contact_StudentTeachingStaff](
	[Id] [varchar](18) NOT NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](800) NULL
)