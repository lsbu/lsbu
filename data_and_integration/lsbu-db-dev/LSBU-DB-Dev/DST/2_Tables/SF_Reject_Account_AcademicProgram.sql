IF OBJECT_ID('[DST].[SF_Reject_Account_AcademicProgram]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Account_AcademicProgram];
GO

CREATE TABLE [DST].[SF_Reject_Account_AcademicProgram](
	[Name] [varchar](255) NULL,
	[RecordTypeId] [varchar](18) NULL,
	[ParentId] [varchar](18) NULL,
	[BillingStreet] [varchar](255) NULL,
	[BillingCity] [varchar](40) NULL,
	[BillingPostalCode] [varchar](20) NULL,
	[BillingCountry] [varchar](80) NULL,
	[Phone] [varchar](40) NULL,
	[Fax] [varchar](40) NULL,
	[AccountNumber] [varchar](40) NULL,
	[Website] [varchar](255) NULL,
	[Sic] [varchar](20) NULL,
	[Industry] [varchar](40) NULL,
	[AnnualRevenue] [decimal](18, 0) NULL,
	[NumberOfEmployees] [int] NULL,
	[TickerSymbol] [varchar](20) NULL,
	[Description] [varchar](max) NULL,
	[Rating] [varchar](40) NULL,
	[Site] [varchar](80) NULL,
	[AccountSource] [varchar](40) NULL,
	[SicDesc] [varchar](80) NULL,
	[hed__Primary_Contact__c] [varchar](18) NULL,
	[hed__Current_Address__c] [varchar](18) NULL,
	[hed__Billing_County__c] [varchar](80) NULL,
	[hed__Shipping_County__c] [varchar](80) NULL,
	[LSB_ACC_ExternalID__c] [varchar](90) NOT NULL,
	[LSB_ACC_SourceSystemID__c] [varchar](90) NULL,
	[LSB_ACC_SourceSystem__c] [varchar](255) NULL,
	[LSB_ACC_CourseCode__c] [varchar](90) NULL,
	[LSB_ACC_UCASCourseCode__c] [varchar](90) NULL,
	[LSB_ACC_TypeofCourse__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)