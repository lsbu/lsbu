IF OBJECT_ID('[DST].[SF_Reject_Term]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Term];
GO

CREATE TABLE [DST].[SF_Reject_Term](
	[Name] [varchar](80) NULL,
	[hed__Account__c] [varchar](18) NULL,
	[hed__End_Date__c] [datetime2](0) NULL,
	[hed__Start_Date__c] [datetime2](0) NULL,
	[hed__Grading_Period_Sequence__c] [real] NULL,
	[hed__Instructional_Days__c] [real] NULL,
	[hed__Parent_Term__c] [varchar](18) NULL,
	[hed__Type__c] [varchar](255) NULL,
	[LSB_TRM_ExternalID__c] [varchar](90) NULL,
	[LSB_TRM_SourceSystemID__c] [varchar](90) NULL,
	[LSB_TRM_SourceSystem__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)