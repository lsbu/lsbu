IF OBJECT_ID('[DST].[SF_Reject_Relationship_StudentTeachingStaff]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Relationship_StudentTeachingStaff];
GO

CREATE TABLE [DST].[SF_Reject_Relationship_StudentTeachingStaff](
	[hed__Contact__c] [varchar](18) NOT NULL,
	[hed__RelatedContact__c] [varchar](18) NULL,
	[hed__Status__c] [varchar](255) NULL,
	[hed__Type__c] [varchar](255) NULL,
	[LSB_RTP_ExternalId__c] [varchar](90) NULL,
	[LSB_RTP_SourceSystemID__c] [varchar](90) NULL,
	[LSB_RTP_SourceSystem__c] [varchar](255) NULL,
	[LSB_RTP_Module__c] [varchar](18) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](800) NULL
)