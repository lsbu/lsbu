IF OBJECT_ID('[DST].[SF_Reject_Address]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Address];
GO

CREATE TABLE [DST].[SF_Reject_Address](
	[hed__Address_Type__c] [varchar](255) NULL,
	[hed__Latest_End_Date__c] [datetime] NULL,
	[hed__Latest_Start_Date__c] [datetime] NULL,
	[hed__MailingCity__c] [varchar](255) NULL,
	[hed__MailingCountry__c] [varchar](255) NULL,
	[hed__MailingPostalCode__c] [varchar](255) NULL,
	[hed__MailingState__c] [varchar](255) NULL,
	[hed__MailingStreet2__c] [varchar](255) NULL,
	[hed__MailingStreet__c] [varchar](255) NULL,
	[hed__Parent_Account__c] [varchar](18) NULL,
	[hed__Seasonal_End_Day__c] [varchar](255) NULL,
	[hed__Seasonal_End_Month__c] [varchar](255) NULL,
	[hed__Seasonal_End_Year__c] [real] NULL,
	[hed__Seasonal_Start_Day__c] [varchar](255) NULL,
	[hed__Seasonal_Start_Month__c] [varchar](255) NULL,
	[hed__Seasonal_Start_Year__c] [real] NULL,
	[hed__Parent_Contact__c] [varchar](18) NULL,
	[LSB_ADS_ExternalID__c] [varchar](90) NULL,
	[LSB_ADS_SourceSystemID__c] [varchar](90) NULL,
	[LSB_ADS_SourceSystem__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)