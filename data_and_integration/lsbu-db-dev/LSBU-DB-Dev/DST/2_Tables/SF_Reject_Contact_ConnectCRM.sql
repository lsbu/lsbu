IF OBJECT_ID('[DST].[SF_Reject_Contact_ConnectCRM]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Contact_ConnectCRM];
GO

CREATE TABLE [DST].[SF_Reject_Contact_ConnectCRM](
	[AccountId] [varchar](18) NULL,
	[HomePhone] [varchar](40) NULL,
	[hed__PreferredPhone__c] [varchar](255) NULL,
	[LSB_CON_SourceSystem__c] [varchar](255) NULL,
	[LSB_CON_SourceSystemID__c] [varchar](90) NULL,
	[LSB_ExternalID__c] [varchar](90) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)