IF OBJECT_ID('[DST].[SF_Reject_CourseOffering_ModuleOffering]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_CourseOffering_ModuleOffering];
GO

CREATE TABLE [DST].[SF_Reject_CourseOffering_ModuleOffering](
	[Name] [varchar](80) NULL,
	[hed__Course__c] [varchar](18) NULL,
	[hed__Term__c] [varchar](18) NULL,
	[hed__Capacity__c] [real] NULL,
	[hed__End_Date__c] [datetime] NULL,
	[hed__Faculty__c] [varchar](18) NULL,
	[hed__Section_ID__c] [varchar](255) NULL,
	[hed__Start_Date__c] [datetime] NULL,
	[hed__Facility__c] [varchar](18) NULL,
	[hed__Time_Block__c] [varchar](18) NULL,
	[LSB_COF_ExternalID__c] [varchar](90) NULL,
	[LSB_COF_SourceSystemID__c] [varchar](90) NULL,
	[LSB_COF_SourceSystem__c] [varchar](255) NULL,
	[LSB_COF_ModuleCredit__c] [real] NULL,
	[LSB_COF_ModuleInstance__c] [real] NULL,
	[LSB_COF_SessionCode__c] [varchar](10) NULL,
	[LSB_COF_SessionDescription__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)