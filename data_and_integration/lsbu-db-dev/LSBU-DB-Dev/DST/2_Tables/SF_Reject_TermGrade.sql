IF OBJECT_ID('[DST].[SF_Reject_TermGrade]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_TermGrade];
GO

CREATE TABLE [DST].[SF_Reject_TermGrade](
	[hed__Course_Connection__c] [varchar](18) NULL,
	[hed__Credits_Attempted__c] [real] NULL,
	[hed__Credits_Earned__c] [real] NULL,
	[hed__Letter_Grade__c] [varchar](255) NULL,
	[hed__Numerical_Grade__c] [real] NULL,
	[hed__Percent_Grade__c] [real] NULL,
	[hed__Result__c] [varchar](255) NULL,
	[hed__Term_Grade_Type__c] [varchar](255) NULL,
	[hed__Term__c] [varchar](18) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Course_Offering__c] [varchar](18) NULL,
	[LSB_TDE_CourseWork1__c] [varchar](20) NULL,
	[LSB_TDE_CourseWork2__c] [varchar](20) NULL,
	[LSB_TDE_CourseWork3__c] [varchar](20) NULL,
	[LSB_TDE_CourseWork4__c] [varchar](20) NULL,
	[LSB_TDE_CourseWork5__c] [varchar](20) NULL,
	[LSB_TDE_CourseWork6__c] [varchar](20) NULL,
	[LSB_TDE_Exam1__c] [varchar](20) NULL,
	[LSB_TDE_Exam2__c] [varchar](20) NULL,
	[LSB_TDE_Exam3__c] [varchar](20) NULL,
	[LSB_TDE_Exam4__c] [varchar](20) NULL,
	[LSB_TDE_Exam5__c] [varchar](20) NULL,
	[LSB_TDE_Attempt__c] [varchar](20) NULL,
	[LSB_TDE_ModuleMarks__c] [varchar](20) NULL,
	[LSB_TDE_ModuleGrade__c] [varchar](255) NULL,
	[LSB_TDE_SourceSystem__c] [varchar](255) NULL,
	[LSB_TDE_SourceSystemId__c] [varchar](255) NULL,
	[LSB_TDE_ExternalId__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](800) NULL
)
