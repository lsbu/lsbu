IF OBJECT_ID('[DST].[DICT_Additional_Support_FeedbackLocation]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Additional_Support_FeedbackLocation];
GO


CREATE TABLE [DST].[DICT_Additional_Support_FeedbackLocation](
	[Source_val] [varchar](255) NULL,
	[DST_val] [varchar](255) NULL
) ON [PRIMARY]
GO


INSERT INTO [DST].[DICT_Additional_Support_FeedbackLocation]
  ( [Source_val], [DST_val])
VALUES
('at Havering Campus.','at Havering Campus'),
('at the Student Life Centre helpdesk (Southwark Campus).','at the Student Life Centre helpdesk (Southwark Campus)'),
('by email.','by email'),
('by phone.','by phone')
GO

