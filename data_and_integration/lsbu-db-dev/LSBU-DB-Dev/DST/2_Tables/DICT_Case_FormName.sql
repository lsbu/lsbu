IF OBJECT_ID('[DST].[DICT_Case_FormName]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Case_FormName];
GO

CREATE TABLE [DST].[DICT_Case_FormName](
	[Source_val] [varchar](255) NULL,
	[DST_val] [varchar](255) NULL
) ON [PRIMARY]
GO

INSERT INTO [DST].[DICT_Case_FormName] values 
('Ask A Question Clearing Form','Clearing Interest Form'),
('Clearing call back request Form','Clearing Call Back Request Form'),
('Clearing How-to Guide Form','Clearing How-to Guide Form'),
('Contact Us Form','Contact Us Form'),
('Croydon Interest Form','Croydon Interest Form'),
('EU Recruitment Event Form','Prospect Event Registration Form'),
('Event Registration','Prospect Event Registration Form'),
('Gecko Event Form','Prospect Event Registration Form'),
('Get In Touch (INT/EU) Course Form','Get In Touch Interest Form'),
('Get In Touch (UK/EU) Course Form','Get In Touch Interest Form'),
('International Recruitment Event Form','Prospect Event Registration Form'),
('Personalised Prospectus Form','Prospectus Request Form'),
('Register Your Interest Form','Generic Interest Form'),
('Request A Prospectus Form','Prospectus Request Form'),
('Study Nursing Interest Form','Generic Interest Form'),
('The Tea Newsletter','Generic Interest Form'),
('UCAS Exhibition Form','Student Recruitment & Outreach Registration Interest Form'),
('UK Recruitment Event Form','Student Recruitment & Outreach Event Registration Form'),
('University Open Day (PC) Form','University Open Day Event Registration Form'),
('University Open Day (QR) Form','University Open Day Event Registration Form')
GO