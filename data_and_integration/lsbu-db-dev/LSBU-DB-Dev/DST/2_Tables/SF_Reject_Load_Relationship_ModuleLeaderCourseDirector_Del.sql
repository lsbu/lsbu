IF OBJECT_ID('[DST].[SF_Reject_Load_Relationship_ModuleLeaderCourseDirector_Del]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Load_Relationship_ModuleLeaderCourseDirector_Del];
GO

CREATE TABLE [DST].[SF_Reject_Load_Relationship_ModuleLeaderCourseDirector_Del](
	[Id] [varchar](18) NOT NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)