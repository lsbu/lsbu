﻿IF OBJECT_ID('[DST].[SF_Reject_LSBU_Account]','U') IS NOT NULL
  DROP TABLE [DST].[SF_Reject_LSBU_Account];
GO

CREATE TABLE [DST].[SF_Reject_LSBU_Account] (
    [Name]                      VARCHAR (255)  NULL,
    [RecordTypeId]              VARCHAR (18)   NULL,
    [ParentId]                  VARCHAR (18)   NULL,
    [BillingStreet]             VARCHAR (255)  NULL,
    [BillingCity]               VARCHAR (40)   NULL,
    [BillingState]              VARCHAR (80)   NULL,
    [BillingPostalCode]         VARCHAR (20)   NULL,
    [BillingCountry]            VARCHAR (80)   NULL,
    [BillingGeocodeAccuracy]    VARCHAR (40)   NULL,
    [ShippingStreet]            VARCHAR (255)  NULL,
    [ShippingCity]              VARCHAR (40)   NULL,
    [ShippingState]             VARCHAR (80)   NULL,
    [ShippingPostalCode]        VARCHAR (20)   NULL,
    [ShippingCountry]           VARCHAR (80)   NULL,
    [ShippingGeocodeAccuracy]   VARCHAR (40)   NULL,
    [Phone]                     VARCHAR (40)   NULL,
    [Fax]                       VARCHAR (40)   NULL,
    [AccountNumber]             VARCHAR (40)   NULL,
    [Website]                   VARCHAR (255)  NULL,
    [Sic]                       VARCHAR (20)   NULL,
    [Industry]                  VARCHAR (40)   NULL,
    [TickerSymbol]              VARCHAR (20)   NULL,
    [Description]               VARCHAR (MAX)  NULL,
    [Site]                      VARCHAR (80)   NULL,
    [AccountSource]             VARCHAR (40)   NULL,
    [SicDesc]                   VARCHAR (80)   NULL,
    [hed__Primary_Contact__c]   VARCHAR (18)   NULL,
    [hed__Current_Address__c]   VARCHAR (18)   NULL,
    [LSB_ACC_ExternalID__c]     VARCHAR (90)   NOT NULL,
    [LSB_ACC_SourceSystemID__c] VARCHAR (90)   NULL,
    [LSB_ACC_SourceSystem__c]   VARCHAR (255)  NULL,
    [LSB_ACC_CourseCode__c]     VARCHAR (90)   NULL,
    [LSB_ACC_UCASCourseCode__c] VARCHAR (90)   NULL,
    [errorCode]                 VARCHAR (255)  NULL,
    [errorFields]               VARCHAR (255)  NULL,
    [errorMessage]              VARCHAR (8000) NULL,
    PRIMARY KEY CLUSTERED ([LSB_ACC_ExternalID__c] ASC)
);

