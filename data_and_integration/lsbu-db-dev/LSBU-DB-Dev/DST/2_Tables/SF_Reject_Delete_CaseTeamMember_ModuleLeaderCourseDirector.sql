IF OBJECT_ID('[DST].[SF_Reject_Delete_CaseTeamMember_ModuleLeaderCourseDirector]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Delete_CaseTeamMember_ModuleLeaderCourseDirector];
GO

CREATE TABLE [DST].[SF_Reject_Delete_CaseTeamMember_ModuleLeaderCourseDirector](
	[Id] [varchar](18) NOT NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)