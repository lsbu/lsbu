IF OBJECT_ID('[DST].[SF_Reject_Address_Update]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Address_Update];
GO

CREATE TABLE [DST].[SF_Reject_Address_Update](
	[hed__Default_Address__c] [bit] NULL,
	[Id] [varchar](18) NOT NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](8000) NULL
)