IF OBJECT_ID('[DST].[DICT_Invalid_Students]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Invalid_Students];
GO

CREATE TABLE [DST].[DICT_Invalid_Students](
	[Student_ID] [varchar](255) NULL
) ON [PRIMARY]
GO


INSERT INTO [DST].[DICT_Invalid_Students](Student_ID)
VALUES
(' 392123'),
('3722237'),
('5555555'),
('1200000'),
('1234567'),
('1300000'),
('341109a'),
('123456'),
('353322a'),
('0000')

GO

