IF OBJECT_ID('[DST].[DICT_HearAboutUs]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_HearAboutUs];
GO


CREATE TABLE [DST].[DICT_HearAboutUs](
	[Source_val] [varchar](255) NULL,
	[DST_val] [varchar](255) NULL
) ON [PRIMARY]
GO

INSERT INTO [DST].[DICT_HearAboutUs] VALUES
('Advertisement','Advertisement'),
('Brentwood Gazette','Newspaper/Magazine'),
('Careers advisor or teacher','Careers advisor or teacher'),
('Door-to-door leaflet','Leaflet'),
('Education directory','Education directory'),
('Email/Newsletter','Email/Newsletter'),
('Employer','Employer'),
('Exhibition / Interview programme','Exhibition / Interview programme'),
('Exhibition/Interview programme','Exhibition / Interview programme'),
('Facebook','Social Media'),
('Friend or relative','Friend or relative'),
('Friend/family','Friend or relative'),
('Google','Social Media'),
('GP Surgery','Other'),
('HE education fair','Open Event/Education Fair'),
('Hobsons website/guide','Internet search'),
('Hospital Poster','Advertisement'),
('Ilford Recorder','Newspaper/Magazine'),
('Internet / web','Internet search'),
('Internet search','Internet search'),
('Leaflet drop','Leaflet'),
('Leaflet','Leaflet'),
('London Buses','Advertisement'),
('LSBU Agent','LSBU Agent'),
('LSBU Alumni','LSBU Alumni'),
('LSBU prospectus','LSBU prospectus'),
('Newspaper/Magazine','Newspaper/Magazine'),
('Open day/Education fair','Open Event/Education Fair'),
('Open day/information evening','Open Event/Education Fair'),
('Other','Other'),
('Promotional email','Email/Newsletter'),
('Prospects.ac.uk','Internet search'),
('Publications/Advertisement','Advertisement'),
('Romford Recorder','Newspaper/Magazine'),
('School visit','School visit'),
('Thurrock Gazette','Newspaper/Magazine'),
('Tube/train/bus poster','Advertisement'),
('Twitter','Social Media'),
('UCAS handbook','UCAS'),
('UCAS listing','UCAS'),
('UCAS','UCAS'),
('Website/Search Engine','Internet search'),
('West Essex Guardian Series','Newspaper/Magazine'),
('YouTube','Social Media')
GO