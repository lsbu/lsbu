IF OBJECT_ID('[DST].[DICT_AppointmentEnquiryCategory_TopicSubtopic]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_AppointmentEnquiryCategory_TopicSubtopic];
GO

CREATE TABLE [DST].[DICT_AppointmentEnquiryCategory_TopicSubtopic](
	[Category] [nvarchar](128),
	[Topic] [nvarchar](128),
	[Subtopic] [nvarchar](128)
)
GO

INSERT INTO DST.DICT_AppointmentEnquiryCategory_TopicSubtopic
(Category
,Topic
,Subtopic)
VALUES
('Casework','SKIP APPOINTMENT RECORD', 'SKIP APPOINTMENT RECORD'),
('Counselling','SKIP APPOINTMENT RECORD', 'SKIP APPOINTMENT RECORD'),
('Covid 19 Support','Disability & Dyslexia Support', 'Disability & Dyslexia appointment'),
('Disability & Dyslexia support','Disability & Dyslexia Support', 'Disability & Dyslexia appointment'),
('Disabled Student Allowance','Disability & Dyslexia Support', 'DSA application appointment'),
('EP Feedback','Disability & Dyslexia Support', 'EON feedback appointment'),
('Halls Students','Mental Health & Wellbeing Support', 'Mental Health appointment'),
('Havering Appointment','Disability & Dyslexia Support', 'Disability & Dyslexia appointment'),
('Havering MHWB','Mental Health & Wellbeing Support', 'Mental Health appointment'),
('HSC Students','Mental Health & Wellbeing Support', 'Mental Health appointment'),
('Mental Health & Wellbeing','Mental Health & Wellbeing Support', 'Mental Health appointment'),
('MHWB Telephone Appointment','Mental Health & Wellbeing Support', 'Mental Health appointment'),
('Pre Entry Support','Disability & Dyslexia Support', 'Pre Entry Advice'),
('Quick Query','SKIP APPOINTMENT RECORD', 'SKIP APPOINTMENT RECORD'),
('SPLD Screening Assessment','Disability & Dyslexia Support', 'Dyslexia screening and feedback appointment'),
('Support Arrangements','Disability & Dyslexia Support', 'Support arrangements review'),
(NULL,'SKIP APPOINTMENT RECORD', 'SKIP APPOINTMENT RECORD')