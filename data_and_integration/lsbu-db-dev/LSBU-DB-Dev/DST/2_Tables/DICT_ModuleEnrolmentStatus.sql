
IF OBJECT_ID('[DST].[DICT_ModuleEnrolmentStatus]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_ModuleEnrolmentStatus];
GO


CREATE TABLE [DST].[DICT_ModuleEnrolmentStatus](
	[Source_val] [varchar](255) NULL,
	[DST_val] [varchar](255) NULL
) ON [PRIMARY]
GO


INSERT INTO [DST].[DICT_ModuleEnrolmentStatus] 
VALUES
('EADZ','Deposit Applicant/ New Entrant'),
('EAPP','Nomination / Application'),
('EAPPT','Cancelled - Transfer'),
('EASS','Pre-Registered'),
('EAWD','Application Withdrawn'),
('EAZZ','Applicant / New Entrant'),
('EBAC','Expected To Enrol / First'),
('EBIB','Bursary Interruption Begins'),
('EBIE','Bursary Interruption Ends'),
('ECCNR','Cancelled - Course Never Ran'),
('ECCP','Course Change Pending'),
('EDISSO','Dissertation only'),
('EDNE','Did Not Enrol'),
('EDUP','Duplicate Record - Do Not Use'),
('EEXC','Excluded'),
('EEXO','Exam only'),
('EFAFU','False Active Due Fault By User'),
('EFE','Fully Enroled'),
('EFMA','Fail To Meet Attend Requirements'),
('EGRAD','Graduate'),
('EINS','INSET Sleeping'),
('EINTA','Agreed Interruption of Studies'),
('EINTH','Interruption - Health reasons'),
('ENA','Never attended'),
('ENBAC','Expected To Enrol / NO BACS'),
('EOCS','Other Course Sleeping'),
('EOER','Outstanding enrol requirements'),
('ERCOU','Failed to cancel booking'),
('ERORU','Rolled Over, Result Unknown'),
('ESD','Enroled For Study Day Only'),
('ESLEP','Interruption: Inactive Year'),
('ESUS','Suspended by University'),
('ETROC','Transferred To Other Course'),
('EVOID','Void'),
('EWD','Withdrawn'),
('EZZZ','Student Not Yet Enroled')
GO
