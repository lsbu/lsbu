IF OBJECT_ID('[DST].[DICT_Support_Profile_AllocatedToDA]','U') IS NOT NULL
	DROP TABLE [DST].[DICT_Support_Profile_AllocatedToDA];
GO

CREATE TABLE [DST].[DICT_Support_Profile_AllocatedToDA](
	[AllocatedToDA_Person] [varchar](255) NULL,
	[User_Name] [varchar](255) NULL
) ON [PRIMARY]
GO

INSERT INTO [DST].[DICT_Support_Profile_AllocatedToDA] (AllocatedToDA_Person, User_Name)
VALUES 
('John Muya', 'muyaj'),
('Clive Cort', NULL),
('Ruth Macleod', NULL),
('Ellena Wilkinson', NULL),
('Paul Loader', NULL),
('Tom Crawshaw', NULL),
('Josh Ward', 'wardj15'),
('Jennifer Afram', 'aframj'),
('Jennifer Richards', 'richaj17'),
('Louisa Beejay', NULL),
('Steve Short', NULL),
('Rosie Holden', 'thompj29'),
('Jonathan Thompson', 'thompj29'),
('Joanna Hastwell', NULL)
GO