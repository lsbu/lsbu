IF OBJECT_ID('[DST].[SF_Reject_Case_ExtCirc]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Case_ExtCirc];
GO

CREATE TABLE [DST].[SF_Reject_Case_ExtCirc](
	[ContactId] [varchar](50) NULL,
	[Status] [varchar](200) NULL,
	[Subject] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
	[ClosedDate] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LSB_CAS_ApprovalPendingReason__c] [varchar](500) NULL,
	[LSB_CAS_RelatedAssessment__c] [varchar](500) NULL,
	[LSB_CAS_RelatedModule__c] [varchar](500) NULL,
	[LSB_CAS_RequestType__c] [varchar](500) NULL,
	[LSB_CAS_RejectionReason__c] [varchar](500) NULL,
	[LSB_CAS_ExternalID__c] [varchar](500) NULL,
	[LSB_CAS_SourceSystemID__c] [varchar](500) NULL,
	[errorCode] [varchar](500) NULL,
	[errorFields] [varchar](500) NULL,
	[errorMessage] [varchar](8000) NULL
) 