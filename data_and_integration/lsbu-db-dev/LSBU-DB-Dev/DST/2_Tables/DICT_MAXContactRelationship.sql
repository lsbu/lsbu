IF OBJECT_ID('DST.DICT_MAXContactRelationship','U') IS NOT NULL
	DROP TABLE DST.DICT_MAXContactRelationship;
GO

create table DST.DICT_MAXContactRelationship
([DDS_Consent_to_Share__DDS__Relationship_to_Student] varchar(255),
[Relationship1] varchar(255))
GO

TRUNCATE TABLE DST.DICT_MAXContactRelationship
GO

INSERT INTO DST.DICT_MAXContactRelationship
([DDS_Consent_to_Share__DDS__Relationship_to_Student],
[Relationship1])
VALUES
('Course Coach', 'Education Professional Advisor'),
('(Unknown)', 'NULL'),
('?', 'NULL'),
('?? [Email suggests they are from the technology company]', 'NULL'),
('Assessor', 'Professional Advisor Other'),
('Auntie', 'Aunt'),
('Care Coordinator', 'Social care Professional Advisor'),
('Carer', 'Social care Professional Advisor'),
('Community Mental Health Team', 'Social care Professional Advisor'),
('Daughter and carer', 'Daughter'),
('Disability Advisor', 'Professional Advisor Other'),
('Dyslexia Support Worker', 'Health Professional Advisor'),
('Employment, Training and Education Advisor', 'Advisor'),
('Father', 'Father'),
('Father & Mother', 'Father'),
('Father | Mental Health Keyworker', 'Father'),
('Father and God Mother', 'Father'),
('Father/Mother', 'Father'),
('Foster mother', 'Mother'),
('GP', 'Health Professional Advisor'),
('Grandmother', 'Grandmother'),
('Husband', 'Husband'),
('lesleychodgson@hotmail.com', 'NULL'),
('Mental Health Social Worker and psychiatrist', 'Advisor'),
('Mother', 'Mother'),
('Mother & Father', 'Mother'),
('Mother and Father', 'Mother'),
('Mother and sister', 'Mother'),
('Mother, Case Manager, Neuropsychologist', 'Mother'),
('Mother/Father', 'Mother'),
('Mum', 'Mother'),
('Mum + Sister', 'Mother'),
('Mum and dad', 'Mother'),
('Mum and specialist teacher', 'Mother'),
('No details provided', 'NULL'),
('No info given', 'NULL'),
('No other details provided', 'NULL'),
('Not stated', 'NULL'),
('Occupational Therapist; Key Worker (nurse); Acting Care Coordinator', 'Social care Professional Advisor'),
('Parent', 'Mother/Father'),
('Parent?', 'Mother/Father'),
('Parents', 'Mother'),
('Partner', 'Husband/Wife'),
('Personal Advisor', 'Professional Advisor Other'),
('see form', 'NULL'),
('Sister', 'Family member'),
('Social Worker', 'Social care Professional Advisor'),
('Specialist Consultant', 'Health Professional Advisor'),
('Specialist Diabetic Nurse', 'Health Professional Advisor'),
('Spouse', 'Husband/Wife'),
('Teacher', 'Education Professional Advisor'),
('Unknown', 'NULL'),
('Wife', 'Wife'),
('Case Manager', 'Advisor'),
('Neuropsychologist', 'Health Professional Advisor'),
('Occupational Therapist', 'Social care Professional Advisor'),
('Key Worker (nurse)', 'Health Professional Advisor'),
('Mental Health Social Worker', 'Advisor'),
('psychiatrist', 'Health Professional Advisor')