IF OBJECT_ID('[DST].[SF_Reject_CourseEnrollment_ModuleEnrolmentDetails]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_CourseEnrollment_ModuleEnrolmentDetails];
GO

CREATE TABLE [DST].[SF_Reject_CourseEnrollment_ModuleEnrolmentDetails](
	[hed__Account__c] [varchar](18) NULL,
	[hed__Affiliation__c] [varchar](18) NULL,
	[hed__Contact__c] [varchar](18) NULL,
	[hed__Course_Offering__c] [varchar](18) NULL,
	[hed__Program_Enrollment__c] [varchar](18) NULL,
	[LSB_CCN_ExternalID__c] [varchar](90) NULL,
	[LSB_CCN_SourceSystemID__c] [varchar](90) NULL,
	[LSB_CCN_SourceSystem__c] [varchar](255) NULL,
	[LSB_CCN_CompulsoryOptional__c] [varchar](255) NULL,
	[LSB_CCN_ModuleEnrolmentStatus__c] [varchar](255) NULL,
	[LSB_CCN_ModuleEnrolmentStatusdateach__c] [datetime] NULL,
	[LSB_CCN_SessionCode__c] [varchar](255) NULL,
	[LSB_CCN_SessionDescription__c] [varchar](255) NULL,
	[LSB_CCN_Term__c] [varchar](18) NULL,
	[LSB_CCN_Semester__c] [varchar](20) NULL,
	[LSB_CCN_Resit__c] [bit] NOT NULL,
	[LSB_CCN_ModuleGrade__c] [varchar](20) NULL,
	[LSB_CCN_ModuleMarks__c] [varchar](20) NULL,
	[RecordTypeId] [varchar](18) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](800) NULL
)