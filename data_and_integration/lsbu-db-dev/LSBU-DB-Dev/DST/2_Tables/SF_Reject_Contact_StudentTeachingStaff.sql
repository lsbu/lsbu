IF OBJECT_ID('[DST].[SF_Reject_Contact_StudentTeachingStaff]','U') IS NOT NULL
	DROP TABLE [DST].[SF_Reject_Contact_StudentTeachingStaff];
GO

CREATE TABLE [DST].[SF_Reject_Contact_StudentTeachingStaff](
	[AccountId] [varchar](18) NULL,
	[LastName] [varchar](80) NULL,
	[FirstName] [varchar](40) NULL,
	[Email] [varchar](80) NULL,
	[hed__AlternateEmail__c] [varchar](80) NULL,
	[hed__Preferred_Email__c] [varchar](255) NULL,
	[hed__UniversityEmail__c] [varchar](80) NULL,
	[LSB_ExternalID__c] [varchar](90) NULL,
	[LSB_CON_SourceSystemID__c] [varchar](90) NULL,
	[LSB_CON_CurrentRole__c] [varchar](255) NULL,
	[LSB_CON_SourceSystem__c] [varchar](255) NULL,
	[errorCode] [varchar](255) NULL,
	[errorFields] [varchar](255) NULL,
	[errorMessage] [varchar](800) NULL
)