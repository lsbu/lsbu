CREATE OR ALTER    VIEW [DST].[SupportArrangementDetails_MAXSupportArrangements] AS

WITH Support_Arrangements_Updated_QL AS 
(SELECT * from (
	SELECT Name,IndividualId,Value , ROW_NUMBER () OVER ( partition by IndividualId, Name order by IndividualId ) rnk
	FROM   Maximizer.src.Individual_DetailListMulti  
	WHERE  Name = 'DDS\Support Arrangements\Updated QL') z 
 WHERE z.rnk = 1) 
------------------------------------------------- Assignment Arrangements	-----------------------------------------
SELECT																																										
		 CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Assignment Arrangements.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(IDS.[DDS_Support_Arrangements_Assignment_Arrangement_Notes] as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Assignment Arrangements') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
    ,IDLM.Value AS [DDS_Support_Arrangements_Assignment_Arrangements]
    ,REPLACE(IDLM.Value,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
    	--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Assignment Arrangements'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
  AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Assignment Arrangements'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Assignment_Arrangements'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL

 -------------------------------------------------- Examination Arrangements	-----------------------------------------
  SELECT			
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Examination Arrangements.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(IDS.[DDS_Support_Arrangements_Examination_Arrangements_Notes] as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Examination Arrangements') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
    ,IDLM.Value AS [DDS_Support_Arrangements_Examination_Arrangements]
		,REPLACE(IDLM.Value,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
    ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]

 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Examination Arrangements'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
  AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Examination Arrangements'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Examination_Arrangements'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL
 ----------------------------------------------------- Field Trip Support ----------------------------------------------

 SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Field Trip Support.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(NULL as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Field Trip Support') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_Field_trip_support]
		,REPLACE(IDLM.Value ,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Field trip support'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
   AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Field Trip Support'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Field_trip_support'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL

 ---------------------------- For Information---------------------------------------------------------------------------------
   SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.For Information.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(IDS.[DDS_Support_Arrangements_For_Information_Notes] as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.For Information') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_For_Information]
		,REPLACE(IDLM.Value,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\For Information'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
   AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'For Information'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_For_Information' 
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL
 ----------------------------------------------------------- Library Support ---------------------------------------
   SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Library Support.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(NULL as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Library Support') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_Library_Support]
		,REPLACE(IDLM.Value,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Library Support'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
   AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Library Support'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Library_Support'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL
 ------------------------------------ Other Support ------------------------------------------------------------------------------------
 SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Other Support.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(NULL as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Other Support') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_Other_support_for_student]
		,REPLACE(IDLM.Value ,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Other support for student'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
   AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Other Support'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Other_support_for_student'
   AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
   AND IDLM.Value NOT IN ('* None')

 UNION ALL
 --------------------------------------------------------- Presentation Arrangements---------------------------------------------------------------
 SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Presentation Arrangements.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST([DDS_Support_Arrangements_Presentation_Arrangements_Notes] as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Presentation Arrangements') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_Presentation_Arrangements]
		,REPLACE(IDLM.Value ,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Presentation Arrangements'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
  AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Presentation Arrangements'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Presentation_Arrangements'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL
 ---------------------------------------------------------------------------- Teaching Arrangements------------------------------------------------------------------------------ !!!!!!!!!!!!!!!
 SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Teaching Arrangements.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST([DDS_Support_Arrangements_Teaching_Arrangements_Notes] as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Teaching Arrangements') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_Teaching_Arrangements]
		,REPLACE(IDLM.Value,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c												-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Teaching Arrangements'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
   AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Teaching Arrangements'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Teaching_Arrangements'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

 UNION ALL
 ---------------------------------------------------------------------- Timetabling and Furniture-------------------------------------------------------------------------
 SELECT																
		CAST(1 as BIT) AS LSB_SUD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SUD_Contact__c
		,IDS.Student_ID
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATETIME) AS CreatedDate
		,CAST(NULL as DATE) AS LSB_SUD_EndDate__c
		,CAST(CONCAT(IDS.Student_ID,'.Timetabling and Furniture.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SUD_ExternalId__c
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		--,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_Updated_date_1] AS DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(255)) AS Name																-- Auto Number Field
		,CAST(NULL as varchar(255)) AS LSB_SUD_Notes__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SUD_SourceSystem__c
		,CAST(CONCAT(IDS.Student_ID,'.Timetabling and Furniture') as varchar(255)) AS LSB_SUD_SourceSystemId__c
		,CAST(IDD.[DDS_Support_Arrangements_Supp_Arrs_agreed_date] AS DATE)  AS LSB_SUD_StartDate__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SUD_SupportArrangementMaster__c
		,IDLM.Value AS [DDS_Support_Arrangements_Timetabling_and_furniture]
		,REPLACE(IDLM.Value ,'* ','') as DDS_Support_Arrangements_Assignment_Arrangements_Replaced
		--,CAST(NULL as varchar(255)) AS LSB_SUD_SupportArrangementDetailName__c						-- Formula Field
		,CAST(GISA.Id as varchar(18)) AS LSB_SUD_SupportArrangementPlan__c --updateable: false
		,CAST(GISAM.LSB_SUM_SupportArrangementCategory__c as varchar(255)) AS LSB_SUD_SupportCategory__c
		--,CAST(NULL as varchar(255)) AS LSB_SUD_Type__c			 --updateable: false									-- Formula Field
		--,CAST((SELECT TOP 1 IDLM2.Value FROM Maximizer.src.Individual_DetailListMulti IDLM2 WHERE  IDS.ID = IDLM2.IndividualId AND IDLM2.Name = 'DDS\Support Arrangements\Updated QL' ) AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
		,CAST(IDLM2.Value AS varchar(255)) AS [DDS_Support_Arrangements_Updated_QL]
        ,CAST(IDD.DDS_Support_Arrangements_SuppArr_sent_to_School_date as date) as [DDS_Support_Arrangements_SuppArr_sent_to_School_date]
 FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
 INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId AND IDLM.Name = 'DDS\Support Arrangements\Timetabling and furniture'
 LEFT JOIN Support_Arrangements_Updated_QL IDLM2 ON IDS.ID = IDLM2.IndividualId 
 INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value 
 LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailDate] IDD ON IDS.ID = IDD.ID
 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_SupportArrangement] GISA ON CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') = GISA.[LSB_SUT_ExternalId__c]
 LEFT JOIN [SFC].[Get_ID_SupportArrangementMaster] GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Arrangement'
   AND GISAM.LSB_SUM_SupportArrangementCategory__c = 'Timetabling and Furniture'
 WHERE DICT_SSD.Fieldname = 'DDS_Support_Arrangements_Timetabling_and_furniture'
  AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])

GO


