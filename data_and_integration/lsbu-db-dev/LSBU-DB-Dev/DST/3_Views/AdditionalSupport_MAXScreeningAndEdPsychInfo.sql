CREATE OR ALTER VIEW [DST].[AdditionalSupport_MAXScreeningAndEdPsychInfo] AS
SELECT

	--CAST(NULL AS VARCHAR(18)) AS CreatedById
	--,CAST(NULL AS DATETIME) AS CreatedDate
	--,CAST(NULL AS VARCHAR(18)) AS Id
	--,CAST(NULL AS BIT) AS IsDeleted
	--,CAST(NULL AS DATE) AS LastActivityDate
	--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
	--,CAST(NULL AS DATETIME) AS LastModifiedDate
	--,CAST(NULL AS DATETIME) AS LastReferencedDate
	--,CAST(NULL AS DATETIME) AS LastViewedDate
	 CAST(NULL AS REAL) AS LSB_ART_BSLHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS REAL) AS LSB_ART_BSLHours__c
	--,CAST(NULL AS BIT) AS LSB_ART_BSLInterpreterRecommendedLSBU__c
	--,CAST(NULL AS BIT) AS LSB_ART_BSLInterpreterRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_BSLSupplierLSBUFunded__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_BSLSupplier__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_BSLTotalCostLSBUFundedNMH__c
	,CAST(GIDC.Id AS VARCHAR(18)) AS LSB_ART_Contact__c

  ,DATEADD(HOUR,-1,(CONVERT(DATETIME, IDD.DDS_Screening_and_Ed_Psych_Assessment_Date_of_EP_Assessment, 121) + 
   CONVERT(DATETIME, TRY_CONVERT(TIME, REPLACE(REPLACE(REPLACE(
    case 
     when DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment in ('re-book', 'Please book wt Peter or Alec', 'Please book assessment', 'Please book', 'MUST BE WITH KATH OR CLAIRE', 'Book with Alec or Peter', 'ADHD query') then '0:00' 
     when DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment = '9.30 - 11am' then '9:30'
     when DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment in ('12.30 - 2pm', '12 midday') then '12:30'
     when DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment = '12:40:' then '12:40'
     when DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment = '0930' then '09:30'
     when DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment in ('10-12', '10') then '10:00'
    else DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment 
   end, ' ', ''), '.', ':'), ';', ':'))))) as LSB_ART_DateOfEPAssessment__c

	,CAST(IDD.DDS_Screening_and_Ed_Psych_Assessment_Date_of_SpLD_screening AS DATETIME) AS LSB_ART_DateOfSpLDScreening__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_DyslexiaTuitionType__c
	,CAST(NULL AS DATE) AS LSB_ART_DyslexiaTutorAllocated__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_DyslexiaTutorRecommended__c
	,CAST(IDD.DDS_Screening_and_Ed_Psych_Assessment_Ed_Psych_report_received_date AS DATE) AS LSB_ART_EdPsychReportReceivedDate__c
	,CAST(NULL AS DATE) AS LSB_ART_EmailConvertedToFullSent__c
	,CAST(NULL AS DATE) AS LSB_ART_EmailMentoringToFullSent__c
  ,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Assessment_Result, 'dysgraphia.', 'Dysgraphia'), 'Cancelled appointment - student did not rebook', 'Cancelled - Student did not rebook'), 'Please rebook EP assessment and Feedback appointment', 'Rebook EP Assessment and Feedback'), 'Maths Weakness', 'Math Weakness'), 'Cancelled Appointment - No Support Required', 'Cancelled - No Support Required'),'Please book EP assessment and Feedback appointment', 'Book EP Assessment and Feedback') AS VARCHAR(255)) AS LSB_ART_EPAssessmentResult__c
	,CAST(IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Assessor AS VARCHAR(255)) AS LSB_ART_EPAssessor__c
	,CAST(IDL.DDS_Screening_and_Ed_Psych_Assessment_EP_Cost AS VARCHAR(255)) AS LSB_ART_EPCost__c
	,CAST(IDL.DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_appointment_adviser AS VARCHAR(255)) AS LSB_ART_EPFeedbackAppointmentAdviser__c
	,CAST(IDD.DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_Appointment_Date AS DATE) AS LSB_ART_EPFeedbackAppointmentDate__c
  ,CASE IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Room
     WHEN 'Keyworth Centre: K613' THEN 'Keyworth Centre- K613'
     WHEN 'Keyworth Centre  Room K411' THEN 'Keyworth Centre Room K411'
     WHEN 'Perry Library (please report to DDS Office)' THEN 'Perry Library (Report to DDS Office)'
     WHEN 'Keyworth Centre Room K211;Student Life Centre Room T130 - Report to Helpdesk' THEN 'Keyworth Centre Room K211;SlC Room T130 - Report to Helpdesk'
     WHEN 'Student Life Centre Room T128 - Report to Helpdesk' THEN 'SLC Room T128 - Report to Helpdesk'
     WHEN 'Student Life Centre Room T130 - Report to Helpdesk' THEN 'SLC Room T130 - Report to Helpdesk'
     WHEN 'Student Life Centre Room T132 - Report to Helpdesk' THEN 'SLC Room T132 - Report to Helpdesk'
     WHEN 'To be confirmed - (DDS will confirm the location before your appointment)' THEN 'DDS to confirm location'
   ELSE CAST(REPLACE(IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Room, ',', '-')AS VARCHAR(255)) END AS LSB_ART_EPRoom__c
	,CAST(CONCAT(IDS.Student_ID,'.ScreeningAndEdPsych.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY I.Created)) AS VARCHAR(90)) AS LSB_ART_ExternalId__c
  ,CAST((SELECT DICT_ASF.DST_val FROM DST.DICT_Additional_Support_FeedbackLocation DICT_ASF WHERE IDL.DDS_Screening_and_Ed_Psych_Assessment_Feedback_location= DICT_ASF.Source_val)AS VARCHAR(255)) AS LSB_ART_FeedbackLocation__c
	--,CAST(NULL AS BIT) AS LSB_ART_LockerAgreementSigned__c
	,CAST(NULL AS DATE) AS LSB_ART_LockerAssignedDate__c
	,CAST(NULL AS DATE) AS LSB_ART_LockerLoanEndDate__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_LockerNumber__c
	,CAST(NULL AS DATE) AS LSB_ART_MentorAllocated__c
	,CAST(NULL AS REAL) AS LSB_ART_MentorHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentoringSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentoringType__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_MentorInitialValue__c
	--,CAST(NULL AS BIT) AS LSB_ART_MentorRecommendedLSBUFunded__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorSupportTotalHours__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_MentorTotalCost__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_Mentor__c
	--,CAST(NULL AS BIT) AS LSB_ART_NoteTakerRecommendedLSBUFunded__c
	--,CAST(NULL AS VARCHAR(255)) AS LSB_ART_NoteTakerRecommended__c
	,CAST(NULL AS REAL) AS LSB_ART_NoteTakingHoursAgreedLSBUFunded__c
	,CAST(NULL AS REAL) AS LSB_ART_NoteTakingHours__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_NoteTakingSupplierLSBUFundedNMH__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_NoteTakingTotalCostLSBUFundedNMH__c
	,CAST(CONCAT(IDS.Student_ID,'.ScreeningAndEdPsych.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY I.Created)) AS VARCHAR(90)) AS LSB_ART_SourceSystemId__c
	,CAST('Maximiser' AS VARCHAR(90)) AS LSB_ART_SourceSystem__c
	--,CAST(NULL AS BIT) AS LSB_ART_SpecialistTutorRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_SpecialistTutorSupplier__c
	,CAST(NULL AS REAL) AS LSB_ART_SpecialistTutorSupportHours__c
	,CAST(IDLM.DDS_Screening_and_Ed_Psych_Assessment_SpLD_screening_outcome AS VARCHAR(255)) AS LSB_ART_SpLDScreeningOutcome__c
	,CAST(NULL AS DATE) AS LSB_ART_TotalInterimHoursUsedRequest__c
	,CAST(NULL AS REAL) AS LSB_ART_TotalInterimHoursUsed__c
	,CAST(NULL AS REAL) AS LSB_ART_TotalInterimMentoringHoursUsed__c
	,CAST(NULL AS REAL) AS LSB_ART_TutorHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_TutorInitialValue__c
	--,CAST(NULL AS BIT) AS LSB_ART_TutorRecommendedLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupplierLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupportTotalHours__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_TutorTotalCostLSBUFundedNMH__c
	--,CAST(NULL AS VARCHAR(80)) AS Name
	--,CAST(NULL AS VARCHAR(18)) AS OwnerId
	,CAST((SELECT id FROM sfc.RecordType WHERE DeveloperName = 'LSB_ART_ScreeningAndEdPsychInformation') AS VARCHAR(18)) AS RecordTypeId
	--,CAST(NULL AS DATETIME) AS SystemModstamp

FROM Maximizer.src.Individual I
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON I.Id = IDS.Id
 JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON I.Id = IDLM.Id
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON I.Id = IDD.Id
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailList IDL ON I.Id = IDL.Id
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailNumber IDN ON I.Id = IDN.Id
WHERE (
 DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment IS NOT NULL OR 
 IDD.DDS_Screening_and_Ed_Psych_Assessment_Date_of_SpLD_screening IS NOT NULL OR
 IDD.DDS_Screening_and_Ed_Psych_Assessment_Ed_Psych_report_received_date IS NOT NULL OR
 IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Assessment_Result IS NOT NULL OR
 IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Assessor IS NOT NULL OR
 IDL.DDS_Screening_and_Ed_Psych_Assessment_EP_Cost IS NOT NULL OR
 IDL.DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_appointment_adviser IS NOT NULL OR
 IDD.DDS_Screening_and_Ed_Psych_Assessment_EP_Feedback_Appointment_Date IS NOT NULL OR
 IDLM.DDS_Screening_and_Ed_Psych_Assessment_EP_Room IS NOT NULL OR
 IDL.DDS_Screening_and_Ed_Psych_Assessment_Feedback_location IS NOT NULL OR 
 IDLM.DDS_Screening_and_Ed_Psych_Assessment_SpLD_screening_outcome IS NOT NULL
 )