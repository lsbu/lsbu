
CREATE OR ALTER VIEW [DST].[ContentVersion_MAXEmails]
AS
SELECT
	CreatedDate
	,LastmodifiedDate
	,createdbyId
	,LastModifiedById
	,FirstPublishLocationId
	,LSB_CVR_DocumentType__c
	,LSB_CVR_ExternalId__c
	,LSB_CVR_SourceSystemId__c
	,LSB_CVR_SourceSystem__c
	,SharingOption
	,SharingPrivacy
	,PathOnClient
	,Title
	,VersionData
FROM
(
SELECT 
CAST(NULL as datetime) AS CreatedDate
,CAST(NULL as datetime) AS LastmodifiedDate
,CAST(NULL as Varchar(18)) AS CreatedById						-- New Field
,CAST(NULL as Varchar(18)) AS LastModifiedById				-- New Field
,CAST(GISP.Id as varchar(18)) FirstPublishLocationId
,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(IDS.Student_ID,'.',ED.StudentId,'.MaxEmail.',ROW_NUMBER() OVER(PARTITION BY (IDS.Student_ID) ORDER BY IDS.Student_ID )) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(ED.StudentId AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('Maximiser' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST(NULL AS VARCHAR(255)) SharingPrivacy
,CAST(ED.Filename AS VARCHAR(500)) AS PathOnClient
,CAST(
		CASE 
			WHEN ED.SubfolderName IS NOT NULL THEN CONCAT(CONVERT(varchar,GETDATE(),20),' - ',SUBSTRING(ED.Filename,1,CHARINDEX('.',ED.Filename)-1),' (Email Attachments)')
			ELSE CONCAT(CONVERT(varchar,GETDATE(),20),' - ',SUBSTRING(ED.Filename,1,CHARINDEX('.',ED.Filename)-1)) 
			END AS VARCHAR(255)) AS Title
,CAST(REPLACE(ED.Base64Value, CHAR(10), '') as varchar(max))  as VersionData
FROM [Maximizer].[src].[EmailData] ED
INNER JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON IDS.Id = ED.StudentId
INNER JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = CONCAT(IDS.Student_ID,'.SP')
where  CHARINDEX('.',ED.Filename) not in ('0','1','2','3','4','5')
) AS T1
WHERE T1.LSB_CVR_ExternalId__c NOT IN (select LSB_CVR_ExternalId__c from sfc.Get_ID_ContentVersion WHERE LSB_CVR_ExternalID__c IS NOT NULL)

GO


