CREATE OR ALTER VIEW [DST].[Deduplicate_Contact_Email] AS
--First condition 1. First Name, Last Name & Email Address
select C.Id,COH.LSB_SourceSystem__c,COH.LSB_ExternalID__c, C.LSB_CON_SourceSystem__c,
C.FirstName,C.LastName,C.Email,C.Birthdate
from SFC.Get_ID_Contact C
JOIN DST.Contact_Student as COH on COH.FirstName = C.FirstName AND COH.LastName = C.LastName AND COH.Email = C.Email
LEFT JOIN SFC.Get_ID_Contact CI on COH.LSB_ExternalID__c = CI.LSB_ExternalID__c
WHERE ISNULL(C.LSB_CON_SourceSystem__c,'NULL') <> 'QL'
AND CI.LSB_ExternalID__c IS NULL