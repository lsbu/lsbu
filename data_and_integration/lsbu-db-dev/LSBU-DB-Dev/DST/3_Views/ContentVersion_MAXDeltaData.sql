
CREATE OR ALTER VIEW [DST].[ContentVersion_MAXDeltaData]
AS
SELECT
	CreatedDate
	,LastmodifiedDate
	,createdbyId
	,LastModifiedById
	,FirstPublishLocationId
	,LSB_CVR_DocumentType__c
	,LSB_CVR_ExternalId__c
	,LSB_CVR_SourceSystemId__c
	,LSB_CVR_SourceSystem__c
	,SharingOption
	,SharingPrivacy
	,PathOnClient
	,Title
	,VersionData
FROM
(
/*SELECT 
CAST(NULL as datetime) AS CreatedDate
,CAST(NULL as datetime) AS LastmodifiedDate
,CAST(NULL as Varchar(18)) AS CreatedById						-- New Field
,CAST(NULL as Varchar(18)) AS LastModifiedById				-- New Field
,CAST('a1P25000002MhlhEAC' as varchar(18)) FirstPublishLocationId
,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST('TESTERMXZ1' AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(ID.DocumentId AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('Maximiser' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST(NULL AS VARCHAR(255)) SharingPrivacy
,CAST('saf.rtf' AS VARCHAR(500)) AS [PathOnClient]
,CAST(CONCAT(CONVERT(varchar,ID.Date,20),' - ',ID.NAME) AS VARCHAR(255)) AS Title
,REPLACE(IDD.DocData, CHAR(10), '')  as VersionData
FROM [Maximizer].[src].[Individual_Document] ID
INNER JOIN [Maximizer].[src].[Individual_Document_DocData] IDD on ID.DocumentId = IDD.DocumentId
WHERE ID.DocumentId  = '2091913251'*/

/*SELECT CAST(NULL as datetime) AS CreatedDate
		,CAST(NULL as datetime) AS LastmodifiedDate
      ,[createdbyId]
      ,[LastModifiedById]
      ,'a1P25000002MhlhEAC' as [FirstPublishLocationId]
      ,[LSB_CVR_DocumentType__c]
      ,[LSB_CVR_ExternalId__c]
      ,[LSB_CVR_SourceSystemId__c]
      ,[LSB_CVR_SourceSystem__c]
      ,[SharingOption]
      ,[SharingPrivacy]
      ,CAST('saf.rtf' AS VARCHAR(500)) AS [PathOnClient]
      ,[Title]
      ,[VersionData]
  FROM [didev].[DST].[ContentVersion_MAXDocuments_temp] where [LSB_CVR_ExternalId__c]='2003747.2091913251.1'*/

SELECT 
CAST(NULL as datetime) AS CreatedDate
,CAST(NULL as datetime) AS LastmodifiedDate
,CAST(NULL as Varchar(18)) AS CreatedById						-- New Field
,CAST(NULL as Varchar(18)) AS LastModifiedById				-- New Field
,CAST(GISP.Id as varchar(18)) FirstPublishLocationId
,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(SUBSTRING(DD.StudentID,1,7),'.MaxDelta.',ROW_NUMBER() OVER(PARTITION BY (SUBSTRING(DD.StudentID,1,7)) ORDER BY SUBSTRING(DD.StudentID,1,7) )) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(SUBSTRING(DD.StudentID,1,7) AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('MAXIMISER' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST(NULL AS VARCHAR(255)) SharingPrivacy
,CAST(DD.Filename AS VARCHAR(500)) AS PathOnClient
,CAST(CONCAT(CONVERT(varchar,GETDATE(),20),' - ',DD.Filename) AS VARCHAR(255)) AS Title
,CAST(REPLACE(DD.Base64Value, CHAR(10), '') as varchar(max))  as VersionData
FROM [Maximizer].[src].[DeltaData] DD
INNER JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = CONCAT(SUBSTRING(DD.StudentID,1,7),'.SP')

) AS T1
WHERE T1.LSB_CVR_ExternalId__c NOT IN (select LSB_CVR_ExternalId__c from sfc.Get_ID_ContentVersion WHERE LSB_CVR_ExternalID__c IS NOT NULL)
GO


