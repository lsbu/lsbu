CREATE OR ALTER VIEW [DST].[Affiliation_AppliedSciencesPTCD] AS
SELECT
	--CAST(null as varchar(1300)) AS hed__Affiliation_Type__c
	CAST(GIDC.Id as varchar(18)) AS hed__Contact__c
	,CAST(null as varchar(max)) AS hed__Description__c
	,CAST(CASE 
			WHEN AFL.ChangeStatus = 'New' THEN null
			WHEN AFL.ChangeStatus = 'Delete' AND GIDA.hed__Status__c = 'Current' THEN AFL.UpdateDateTime
			WHEN AFL.ChangeStatus = 'Delete' AND GIDA.hed__Status__c = 'Former' THEN GIDA.hed__EndDate__c
			ELSE RJAF.hed__EndDate__c
		END as date) AS hed__EndDate__c
	,CAST(GIDAC.Id as varchar(18)) AS hed__Account__c
	,CAST(CASE
			WHEN AFL.ChangeStatus = 'New' THEN 1
			WHEN AFL.ChangeStatus = 'Delete' THEN 0
			ELSE RJAF.hed__Primary__c
		END as bit) AS hed__Primary__c
	,CAST('Course Director' as varchar(255)) AS hed__Role__c
	,CAST(AFL.InsetDateTime as date) AS hed__StartDate__c
	,CAST(CASE
			WHEN AFL.ChangeStatus = 'New' THEN 'Current'
			WHEN AFL.ChangeStatus = 'Delete' THEN 'Former'
			ELSE RJAF.hed__Status__c
		END as varchar(255)) AS hed__Status__c
	,CAST('ASPTCDCSV' as varchar(255)) AS LSB_AON_SourceSystem__c
	,CAST(CONCAT(AFL.QL_CDStaffId,'.',AFL.PTCDCSV_CourseCode) as varchar(90)) AS LSB_AON_SourceSystemID__c        
	,CAST(CONCAT(AFL.QL_CDStaffId,'.',AFL.PTCDCSV_CourseCode) as varchar(90)) AS LSB_AON_ExternalID__c        
FROM [INT].[SRC_AppliedSciencesPTCD_Affiliation] AFL
INNER JOIN [SFC].[Get_ID_Contact] GIDC ON AFL.QL_CDStaffId = GIDC.LSB_ExternalID__c
LEFT JOIN [SFC].[Get_ID_Account] GIDAC ON AFL.PTCDCSV_CourseCode = GIDAC.LSB_ACC_ExternalID__c
LEFT JOIN [SFC].[Get_ID_Affiliation] GIDA ON AFL.LSB_ExternalID = GIDA.LSB_AON_ExternalID__c
LEFT JOIN [DST].[SF_Reject_Affiliation_AppliedSciencesPTCD] RJAF ON RJAF.LSB_AON_ExternalID__c = AFL.LSB_ExternalID
WHERE ( AFL.ChangeStatus IN ('NEW','DELETE')
OR RJAF.LSB_AON_ExternalID__c IS NOT NULL
OR (GIDA.Id IS NULL AND AFL.ChangeStatus <> 'DELETE')
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = AFL.QL_CDStaffId
				AND	  VALID.ObjectName = 'Staff'
				AND	  VALID.SrcSystem = 'QL')