CREATE OR ALTER VIEW [DST].[Case_ConnectCRM] AS
SELECT 
  LSB_CAS_AddressLine1__c
 ,LSB_CAS_AddressLine2__c
 ,LSB_CAS_City__c
 ,LSB_CAS_Country__c
 ,LSB_CAS_DateOfBirth__c
 ,iif( db_name() in ('diprod','dipreprod'),SuppliedEmail, CFG.fx_HashEmail(SuppliedEmail,LSB_CAS_LSBUStudentId__c)) as SuppliedEmail
 ,LSB_CAS_Gender__c
 ,LSB_CAS_SourceSystemID__c
 ,LSB_CAS_ExternalID__c
 ,LSB_CAS_SourceSystem__c
 ,OwnerId
 ,LSB_CAS_LSBUStudentId__c
 ,LSB_CAS_FirstName__c
 ,LSB_CAS_LastName__c 
 ,LSB_CAS_Nationality__c
 ,LSB_CAS_ProspectusFormat__c
 ,LSB_CAS_ProspectusRequest__c
 ,LSB_CAS_GeckoID__c
 ,LSB_CAS_GeckoResponseID__c
 ,CAST(DHAU.DST_val as varchar(255)) AS LSB_CAS_HearAboutUs__c
 ,LSB_CAS_UkEuOrInternationalStudent__c
 ,LSB_CAS_InternationalDialCode__c
 ,LSB_CAS_MobilePhone__c
 ,LSB_CAS_LevelOfIntendedStudy__c
 ,LSB_CAS_Postcode__c
 ,LSB_CAS_OtherCurrentLastSchool__c
 ,LSB_CAS_SkypeName__c
 ,LSB_CAS_SubjectAreaOfInterest__c
 ,LSB_CAS_SubjectArea2__c
 ,LSB_CAS_Title__c
 ,LSB_CAS_YearOfIntendedStudy__c
 ,LSB_CAS_UCASNo__c
 ,CreatedDate
 ,LSB_CAS_SpecialRequirements__c
 ,LSB_CAS_LSBUCanContactMeAbout__c
 ,LSB_CAS_LSBUCanContactMeVia__c
 ,CAST(DCOR.DST_val as varchar(255)) AS LSB_CAS_CountryOfResidence__c
 ,Description
 ,LSB_CAS_SessionIntake__c
 ,LSB_CAS_Apprenticeship__c
 ,LSB_CAS_LSBUGroupInstitute__c
 ,LSB_CAS_RegistrationPathway__c
 ,LSB_CAS_CampusInterestedIn__c
 ,LSB_CAS_CurrentHighestLevelOfEducation__c
 ,DCF.DST_val AS LSB_CAS_FormName__c
 ,LSB_CAS_QueriesAndComments__c
 ,LSB_ChannelOfPreference__c
 ,LSB_CAS_CompanyName__c
 ,LSB_CAS_EmployerEmployee__c
 ,LSB_CAS_SpecifyIfOther__c
 ,LSB_CAS_AppliedToLSBU__c
 ,Origin
 ,RecordTypeId
 ,Status
 ,CAST(CONCAT(CreatedDate,'-','Prospect Capture Form','-',LSB_CAS_LSBUStudentId__c) as varchar(255)) as Subject
 ,LSB_CAS_ClosedReason__c
 ,ClosedDate
 ,Q1.ContactId AS ContactId
 ,LSB_CAS_EmergencyContactName__c
 ,LSB_CAS_EmergencyPhoneNo__c
 ,LSB_CAS_EmergencyContactType__c
 ,rtrim(ltrim(LSB_CAS_EmergencyContactFirstName__c)) as LSB_CAS_EmergencyContactFirstName__c
 ,rtrim(ltrim(LSB_CAS_EmergencyContactLastName__c)) as LSB_CAS_EmergencyContactLastName__c
 ,LSB_CAS_EmailOptIn__c
 ,LSB_CAS_PhoneOptIn__c
 ,LSB_CAS_SMSOptIn__c
 ,LSB_CAS_PostOptIn__c

FROM (

------------------------------------------Join with Contact based on Email---------------------------------------------
SELECT 
  LSB_CAS_AddressLine1__c
 ,LSB_CAS_AddressLine2__c
 ,LSB_CAS_City__c
 ,LSB_CAS_Country__c
 ,LSB_CAS_DateOfBirth__c
 ,SuppliedEmail
 ,LSB_CAS_Gender__c
 ,LSB_CAS_SourceSystemID__c
 ,LSB_CAS_ExternalID__c
 ,LSB_CAS_SourceSystem__c
 ,OwnerId
 ,LSB_CAS_LSBUStudentId__c
 ,LSB_CAS_FirstName__c
 ,LSB_CAS_LastName__c 
 ,LSB_CAS_Nationality__c
 ,LSB_CAS_ProspectusFormat__c
 ,LSB_CAS_ProspectusRequest__c
 ,LSB_CAS_GeckoID__c
 ,LSB_CAS_GeckoResponseID__c
 ,LSB_CAS_HearAboutUs__c
 ,LSB_CAS_UkEuOrInternationalStudent__c1
 ,LSB_CAS_UkEuOrInternationalStudent__c
 ,LSB_CAS_InternationalDialCode__c
 ,LSB_CAS_MobilePhone__c
 ,LSB_CAS_LevelOfIntendedStudy__c
 ,LSB_CAS_Postcode__c
 ,LSB_CAS_OtherCurrentLastSchool__c
 ,LSB_CAS_SkypeName__c
 ,LSB_CAS_SubjectAreaOfInterest__c
 ,LSB_CAS_SubjectArea2__c
 ,LSB_CAS_Title__c
 ,LSB_CAS_YearOfIntendedStudy__c
 ,LSB_CAS_UCASNo__c
 , case when CreatedDate> CAST( GETDATE() AS Date ) then CAST( GETDATE() AS Date ) else CreatedDate end as CreatedDate
 ,LSB_CAS_SpecialRequirements__c
 ,LSB_CAS_LSBUCanContactMeAbout__c
 ,LSB_CAS_LSBUCanContactMeVia__c
 ,LSB_CAS_CountryOfResidence__c
 ,Description
 ,LSB_CAS_SessionIntake__c
 ,LSB_CAS_Apprenticeship__c
 ,LSB_CAS_LSBUGroupInstitute__c
 ,LSB_CAS_RegistrationPathway__c
 ,LSB_CAS_CampusInterestedIn__c
 ,LSB_CAS_CurrentHighestLevelOfEducation__c
 ,LSB_CAS_FormName__c
 ,LSB_CAS_QueriesAndComments__c
 ,LSB_ChannelOfPreference__c
 ,LSB_CAS_CompanyName__c
 ,LSB_CAS_EmployerEmployee__c
 ,LSB_CAS_SpecifyIfOther__c
 ,LSB_CAS_AppliedToLSBU__c
 ,Origin
 ,RecordTypeId
 ,Status
 ,LSB_CAS_ClosedReason__c
 , case when ClosedDate> CAST( GETDATE() AS Date ) then CAST( GETDATE() AS Date ) else ClosedDate end as ClosedDate
 ,ContactId
 ,LSB_CAS_EmergencyContactName__c
 ,LSB_CAS_EmergencyPhoneNo__c
 ,LSB_CAS_EmergencyContactType__c
 ,LSB_CAS_EmergencyContactFirstName__c
 ,LSB_CAS_EmergencyContactLastName__c
 ,LSB_CAS_EmailOptIn__c
 ,LSB_CAS_PhoneOptIn__c
 ,LSB_CAS_SMSOptIn__c
 ,LSB_CAS_PostOptIn__c
 FROM
 (
SELECT 
  CAST(SM.Address_Line_1 as varchar(255)) AS LSB_CAS_AddressLine1__c
 ,CAST(SM.Address_Line_2 as varchar(255)) AS LSB_CAS_AddressLine2__c
 ,CAST(SM.City as varchar(255)) AS LSB_CAS_City__c
 ,CAST(SM.Country as varchar(255)) AS LSB_CAS_Country__c
 ,CAST(CASE 
	WHEN TRY_CONVERT(DATE, SM.DOB) > GETDATE() THEN NULL
	ELSE SM.DOB 
	END as date) AS LSB_CAS_DateOfBirth__c
  ,CAST(SM.E_mail as varchar(255)) AS SuppliedEmail
 ,CAST(SM.Gender as varchar(255)) AS LSB_CAS_Gender__c
 ,CAST(SM.Connect_ID as varchar(18)) AS LSB_CAS_SourceSystemID__c
 ,CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(18)) AS LSB_CAS_ExternalID__c
 ,CAST('Connect' as varchar(18)) as LSB_CAS_SourceSystem__c
 ,CAST(OWN.ID as varchar(18)) as OwnerId
 ,CAST(COALESCE(SM.QL_Student_ID, ST.LSB_ExternaliD__C) as varchar(18)) AS LSB_CAS_LSBUStudentId__c
 ,CAST(SM.First_Name as varchar(255)) AS LSB_CAS_FirstName__c
 ,CAST(SM.Last_Name as varchar(255)) AS LSB_CAS_LastName__c 
 ,CAST(DNT.DestinationValue as varchar(255)) AS LSB_CAS_Nationality__c
 ,CAST(CASE
        WHEN CHARINDEX('~', SM.Gecko___Prospectus_Format) != 0 THEN SUBSTRING(SM.Gecko___Prospectus_Format, (LEN(SM.Gecko___Prospectus_Format)- CHARINDEX('~', REVERSE(SM.Gecko___Prospectus_Format))+2),255) 
        ELSE SM.Gecko___Prospectus_Format 
      END as varchar(255))AS LSB_CAS_ProspectusFormat__c

 ,CAST(CASE
        WHEN CHARINDEX('~', SM.Gecko___Prospectus_Request) != 0 THEN SUBSTRING(SM.Gecko___Prospectus_Request, (LEN(SM.Gecko___Prospectus_Request)- CHARINDEX('~', REVERSE(SM.Gecko___Prospectus_Request))+2),255) 
        ELSE SM.Gecko___Prospectus_Request
      END as varchar(255)) AS LSB_CAS_ProspectusRequest__c
 
 ,CAST(SM.Gecko_ID as varchar(255)) AS LSB_CAS_GeckoID__c
 ,CAST(SM.Gecko_Response_ID as varchar(255)) AS LSB_CAS_GeckoResponseID__c
 ,CAST(COALESCE(SM.Hear_about_us_,SM.INT_Hearaboutus_) as varchar(255)) AS LSB_CAS_HearAboutUs__c
 ,CAST(SM.Home_Overseas as varchar(255)) LSB_CAS_UkEuOrInternationalStudent__c1
 ,CAST(CASE SM.Home_Overseas
        WHEN 'Home' THEN 'UK Prospective Student'
        WHEN 'EU' THEN 'EU Prospective Student' 
        WHEN 'Overseas' THEN 'International Prospective Student' 
       ELSE NULL END as varchar(255)) LSB_CAS_UkEuOrInternationalStudent__c
 ,CAST(SM.International_Dial_Code as varchar(255)) AS LSB_CAS_InternationalDialCode__c
 ,CAST(SM.Mobile_Number as varchar(40)) AS LSB_CAS_MobilePhone__c
 ,CAST(SM.Nature_Of_Enquiry as varchar(255)) AS LSB_CAS_LevelOfIntendedStudy__c
 ,CAST(SM.Postcode as varchar(40)) AS LSB_CAS_Postcode__c
 ,CAST(COALESCE(SM.School_College_Name, SM.Your_current_school_college_university_) as varchar(255)) AS LSB_CAS_OtherCurrentLastSchool__c
 ,CAST(SM.Skype_Name as varchar(255)) AS LSB_CAS_SkypeName__c
 ,CAST(REPLACE(SM.Subject_Area_1, 'Managment and HR', 'Management and HR') as varchar(255)) AS LSB_CAS_SubjectAreaOfInterest__c
 ,CAST(SM.Subject_Area_2 as varchar(255)) AS LSB_CAS_SubjectArea2__c
 ,CAST(SM.Title as varchar(255)) AS LSB_CAS_Title__c
 ,CAST(SM.Which_year_are_you_considering_University_entry_ as varchar(40)) AS LSB_CAS_YearOfIntendedStudy__c
 ,CAST(SM.Your_UCAS_No_if_applicable as varchar(100)) AS LSB_CAS_UCASNo__c
 /*
 ,CAST(COALESCE(SM.last_Clearing_Interest_form_submission, SM.last_Clearing_Interest_form_submission
           ,SM.last_Contact_Us_form_submission, SM.last_Contact_Us_form_submission
           ,SM.last_Croydon_Interest_form_submission, SM.last_Croydon_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Get_In_Touch_Interest_form_submission, SM.last_Get_In_Touch_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Outreach_and_Recruitment_form_submission, SM.last_Outreach_and_Recruitment_form_submission
           ,SM.last_Prospectus_Interest_form_submission, SM.last_Prospectus_Interest_form_submission
           ,SM.last_Register_Your_Interest_form_submission) as datetime)  AS CreatedDate
  */
 ,CAST(CASE
		WHEN TRY_CONVERT(DATETIME, SM.Created_Date) > GETDATE() THEN GETDATE()
		ELSE TRY_CONVERT(DATETIME, SM.Created_Date)
		END as datetime) AS CreatedDate
 ,CAST(SM.Special_requirements as varchar(4000))AS LSB_CAS_SpecialRequirements__c
 ,CAST(REPLACE(REPLACE(REPLACE(SM.Consent_Channels, '~', ';'), 'Text Messages', 'SMS'), 'Letters and Mailings', 'Post') as varchar(255)) AS  LSB_CAS_LSBUCanContactMeVia__c
 ,CAST(REPLACE(REPLACE(SM.Consent_Content, '~', ';'), 'General', NULL) as varchar(255)) AS LSB_CAS_LSBUCanContactMeAbout__c
 ,CAST(COALESCE(SM.Int_Country_Of_Residence, SM.Int_RESIDENTIALCOUNTRY) as varchar(255)) AS LSB_CAS_CountryOfResidence__c
 ,CAST(SM.Int_FURTHERINFORMATION as varchar(4000)) AS Description
 ,CAST(UPPER(SM.Int_INTAKEMONTH) as varchar(255)) AS LSB_CAS_SessionIntake__c
 ,CAST(CASE WHEN SM.Are_you_considering_Apprenticeship_ = 'Yes' THEN 1 else 0 end  as BIT) AS LSB_CAS_Apprenticeship__c
 ,CAST(SM.LSBU_Group_Institute as varchar(255)) AS LSB_CAS_LSBUGroupInstitute__c
 ,CAST(SM.Registration_Pathway as varchar(255)) AS LSB_CAS_RegistrationPathway__c
 ,CAST(SM.Campus_Interested_In as varchar(255)) AS LSB_CAS_CampusInterestedIn__c
 ,CAST(CASE SM.Current_level_of_education
        WHEN 'Access' THEN '	Level 3 - A Level, BTEC, NVQ,' ELSE NULL END as varchar(255)) AS LSB_CAS_CurrentHighestLevelOfEducation__c
 ,CAST(CASE
        WHEN CHARINDEX('~', SM.Gecko_form_Entry) != 0 THEN SUBSTRING(SM.Gecko_form_Entry, (LEN(SM.Gecko_form_Entry)- CHARINDEX('~', REVERSE(SM.Gecko_form_Entry))+2),255) 
        ELSE SM.Gecko_form_Entry 
      END as varchar(255))AS LSB_CAS_FormName__c
 ,CAST(SM.Ask_a_question as varchar(4000)) LSB_CAS_QueriesAndComments__c
 ,CAST(SM.Contact_Preference_Center__Channel as varchar(255)) AS LSB_ChannelOfPreference__c
 ,CAST(SM.Company_name as varchar(255)) AS LSB_CAS_CompanyName__c
 ,CAST(SM.Employer_Employee as varchar(255)) AS LSB_CAS_EmployerEmployee__c
 ,CAST(SM.Specify_if__other_ as varchar(255)) AS LSB_CAS_SpecifyIfOther__c
 , CAST(CASE SM.Applied_To_LSBU
         WHEN 'Has Applied To LSBU' THEN 1 ELSE 0
        END as BIT) AS LSB_CAS_AppliedToLSBU__c
 ,CAST('Gecko Forms' as varchar(255)) AS Origin
 ,CAST((SELECT Id FROM SFC.RecordType WHERE Name like 'Prospect Capture Form') as varchar(18)) AS RecordTypeId
 ,CAST('Closed' as varchar(255)) AS Status
 ,CAST('Resolved' as varchar(255)) AS LSB_CAS_ClosedReason__c
 /*
 ,CAST(COALESCE(SM.last_Clearing_Interest_form_submission, SM.last_Clearing_Interest_form_submission
           ,SM.last_Contact_Us_form_submission, SM.last_Contact_Us_form_submission
           ,SM.last_Croydon_Interest_form_submission, SM.last_Croydon_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Get_In_Touch_Interest_form_submission, SM.last_Get_In_Touch_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Outreach_and_Recruitment_form_submission, SM.last_Outreach_and_Recruitment_form_submission
           ,SM.last_Prospectus_Interest_form_submission, SM.last_Prospectus_Interest_form_submission
           ,SM.last_Register_Your_Interest_form_submission) as datetime) AS ClosedDate
 */
 ,CAST(CASE
		WHEN TRY_CONVERT(DATETIME, SM.Created_Date) > GETDATE() THEN GETDATE()
		ELSE TRY_CONVERT(DATETIME, SM.Created_Date)
		END as datetime) AS ClosedDate
 ,CAST(ST.Id as varchar(18)) AS ContactId
 ,CAST(SM.Emergency_Contact_Name as varchar(255))AS LSB_CAS_EmergencyContactName__c
 ,CAST(SM.Emergency_Contact_Number as varchar(255))AS LSB_CAS_EmergencyPhoneNo__c
 ,CAST(DCRL.DestinationValue as varchar(255)) AS LSB_CAS_EmergencyContactType__c
 , CAST(SM.Emergency_Contact_FirstName as varchar(255))  as LSB_CAS_EmergencyContactFirstName__c
 ,CAST(SM.Emergency_Contact_LastName as varchar(255))  AS LSB_CAS_EmergencyContactLastName__c,
 CAST(CASE SM.E_mail_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_EmailOptIn__c,
 CAST(CASE SM.Phone_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_PhoneOptIn__c,
 CAST(CASE SM.SMS_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_SMSOptIn__c,
 CAST(CASE SM.Post_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_PostOptIn__c,
 ROW_NUMBER() OVER (PARTITION BY SM.Connect_ID ORDER BY CSSP.RANK ASC, SM.CREATED_DATE DESC) ROWN
FROM  INT.SRC_Connect_Salesforce_Migration SM 
 JOIN [SFC].[get_ID_USER] OWN on  OWN.username like 'dataintegrationuser@lsbu.com%'
 JOIN  SFC.[Get_ID_Contact] ST ON (SM.E_mail = ST.hed__AlternateEmail__c )   --Changed from INNER to LEFT
  LEFT JOIN [CFG].[ConnectSourceSystemPreference] CSSP ON ST.LSB_CON_SourceSystem__c = CSSP.SourceSystem
 LEFT JOIN [DST].[DICT_Nationality] DNT ON SM.Nationality = DNT.SourceValue
 LEFT JOIN [DST].[DICT_ConnectCRMContactRelationship] DCRL ON DCRL.SourceValue = SM.Emergency_Contact_Relationship
 WHERE NOT EXISTS (SELECT 1 FROM SFC.[Get_ID_Contact] GIC WHERE SM.QL_Student_ID = GIC.LSB_ExternalID__c )
 ) C WHERE C.ROWN = 1
 
 UNION ALL
 ------------------------------------------Join with Contact based on Student Id---------------------------------------------
 SELECT 
  CAST(SM.Address_Line_1 as varchar(255)) AS LSB_CAS_AddressLine1__c
 ,CAST(SM.Address_Line_2 as varchar(255)) AS LSB_CAS_AddressLine2__c
 ,CAST(SM.City as varchar(255)) AS LSB_CAS_City__c
 ,CAST(SM.Country as varchar(255)) AS LSB_CAS_Country__c
 ,CAST(CASE 
	WHEN TRY_CONVERT(DATE, SM.DOB) > GETDATE() THEN NULL
	ELSE SM.DOB 
	END as date) AS LSB_CAS_DateOfBirth__c
  ,CAST(SM.E_mail as varchar(255)) AS SuppliedEmail
 ,CAST(SM.Gender as varchar(255)) AS LSB_CAS_Gender__c
 ,CAST(SM.Connect_ID as varchar(18)) AS LSB_CAS_SourceSystemID__c
 ,CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(18)) AS LSB_CAS_ExternalID__c
 ,CAST('Connect' as varchar(18)) as LSB_CAS_SourceSystem__c
 ,CAST(OWN.ID as varchar(18)) as OwnerId
 ,CAST(COALESCE(SM.QL_Student_ID, ST.LSB_ExternaliD__C) as varchar(18)) AS LSB_CAS_LSBUStudentId__c
 ,CAST(SM.First_Name as varchar(255)) AS LSB_CAS_FirstName__c
 ,CAST(SM.Last_Name as varchar(255)) AS LSB_CAS_LastName__c 
 ,CAST(DNT.DestinationValue as varchar(255)) AS LSB_CAS_Nationality__c
 ,CAST(CASE
        WHEN CHARINDEX('~', SM.Gecko___Prospectus_Format) != 0 THEN SUBSTRING(SM.Gecko___Prospectus_Format, (LEN(SM.Gecko___Prospectus_Format)- CHARINDEX('~', REVERSE(SM.Gecko___Prospectus_Format))+2),255) 
        ELSE SM.Gecko___Prospectus_Format 
      END as varchar(255))AS LSB_CAS_ProspectusFormat__c

 ,CAST(CASE
        WHEN CHARINDEX('~', SM.Gecko___Prospectus_Request) != 0 THEN SUBSTRING(SM.Gecko___Prospectus_Request, (LEN(SM.Gecko___Prospectus_Request)- CHARINDEX('~', REVERSE(SM.Gecko___Prospectus_Request))+2),255) 
        ELSE SM.Gecko___Prospectus_Request
      END as varchar(255)) AS LSB_CAS_ProspectusRequest__c
 
 ,CAST(SM.Gecko_ID as varchar(255)) AS LSB_CAS_GeckoID__c
 ,CAST(SM.Gecko_Response_ID as varchar(255)) AS LSB_CAS_GeckoResponseID__c
 ,CAST(COALESCE(SM.Hear_about_us_,SM.INT_Hearaboutus_) as varchar(255)) AS LSB_CAS_HearAboutUs__c
 ,CAST(SM.Home_Overseas as varchar(255)) LSB_CAS_UkEuOrInternationalStudent__c1
 ,CAST(CASE SM.Home_Overseas
        WHEN 'Home' THEN 'UK Prospective Student'
        WHEN 'EU' THEN 'EU Prospective Student' 
        WHEN 'Overseas' THEN 'International Prospective Student' 
       ELSE NULL END as varchar(255)) LSB_CAS_UkEuOrInternationalStudent__c
 ,CAST(SM.International_Dial_Code as varchar(255)) AS LSB_CAS_InternationalDialCode__c
 ,CAST(SM.Mobile_Number as varchar(40)) AS LSB_CAS_MobilePhone__c
 ,CAST(SM.Nature_Of_Enquiry as varchar(255)) AS LSB_CAS_LevelOfIntendedStudy__c
 ,CAST(SM.Postcode as varchar(40)) AS LSB_CAS_Postcode__c
 ,CAST(COALESCE(SM.School_College_Name, SM.Your_current_school_college_university_) as varchar(255)) AS LSB_CAS_OtherCurrentLastSchool__c
 ,CAST(SM.Skype_Name as varchar(255)) AS LSB_CAS_SkypeName__c
 ,CAST(REPLACE(SM.Subject_Area_1, 'Managment and HR', 'Management and HR') as varchar(255)) AS LSB_CAS_SubjectAreaOfInterest__c
 ,CAST(SM.Subject_Area_2 as varchar(255)) AS LSB_CAS_SubjectArea2__c
 ,CAST(SM.Title as varchar(255)) AS LSB_CAS_Title__c
 ,CAST(SM.Which_year_are_you_considering_University_entry_ as varchar(40)) AS LSB_CAS_YearOfIntendedStudy__c
 ,CAST(SM.Your_UCAS_No_if_applicable as varchar(100)) AS LSB_CAS_UCASNo__c
 /*
 ,CAST(COALESCE(SM.last_Clearing_Interest_form_submission, SM.last_Clearing_Interest_form_submission
           ,SM.last_Contact_Us_form_submission, SM.last_Contact_Us_form_submission
           ,SM.last_Croydon_Interest_form_submission, SM.last_Croydon_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Get_In_Touch_Interest_form_submission, SM.last_Get_In_Touch_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Outreach_and_Recruitment_form_submission, SM.last_Outreach_and_Recruitment_form_submission
           ,SM.last_Prospectus_Interest_form_submission, SM.last_Prospectus_Interest_form_submission
           ,SM.last_Register_Your_Interest_form_submission) as datetime)  AS CreatedDate
  */
 ,TRY_CONVERT(DATETIME, CASE WHEN SM.created_date > GETDATE() THEN TRY_CONVERT(DATETIME, CAST(GETDATE() AS DATE)) ELSE SM.created_date END) AS CreatedDate
 ,CAST(SM.Special_requirements as varchar(4000))AS LSB_CAS_SpecialRequirements__c
 ,CAST(REPLACE(REPLACE(REPLACE(SM.Consent_Channels, '~', ';'), 'Text Messages', 'SMS'), 'Letters and Mailings', 'Post') as varchar(255)) AS  LSB_CAS_LSBUCanContactMeVia__c
 ,CAST(REPLACE(REPLACE(SM.Consent_Content, '~', ';'), 'General', NULL) as varchar(255)) AS LSB_CAS_LSBUCanContactMeAbout__c
 ,CAST(COALESCE(SM.Int_Country_Of_Residence, SM.Int_RESIDENTIALCOUNTRY) as varchar(255)) AS LSB_CAS_CountryOfResidence__c
 ,CAST(SM.Int_FURTHERINFORMATION as varchar(4000)) AS Description
 ,CAST(UPPER(SM.Int_INTAKEMONTH) as varchar(255)) AS LSB_CAS_SessionIntake__c
 ,CAST(CASE WHEN SM.Are_you_considering_Apprenticeship_ = 'Yes' THEN 1 else 0 end  as BIT) AS LSB_CAS_Apprenticeship__c
 ,CAST(SM.LSBU_Group_Institute as varchar(255)) AS LSB_CAS_LSBUGroupInstitute__c
 ,CAST(SM.Registration_Pathway as varchar(255)) AS LSB_CAS_RegistrationPathway__c
 ,CAST(SM.Campus_Interested_In as varchar(255)) AS LSB_CAS_CampusInterestedIn__c
 ,CAST(CASE SM.Current_level_of_education
        WHEN 'Access' THEN '	Level 3 - A Level, BTEC, NVQ,' ELSE NULL END as varchar(255)) AS LSB_CAS_CurrentHighestLevelOfEducation__c
 ,CAST(CASE
        WHEN CHARINDEX('~', SM.Gecko_form_Entry) != 0 THEN SUBSTRING(SM.Gecko_form_Entry, (LEN(SM.Gecko_form_Entry)- CHARINDEX('~', REVERSE(SM.Gecko_form_Entry))+2),255) 
        ELSE SM.Gecko_form_Entry 
      END as varchar(255))AS LSB_CAS_FormName__c
 ,CAST(SM.Ask_a_question as varchar(4000)) LSB_CAS_QueriesAndComments__c
 ,CAST(SM.Contact_Preference_Center__Channel as varchar(255)) AS LSB_ChannelOfPreference__c
 ,CAST(SM.Company_name as varchar(255)) AS LSB_CAS_CompanyName__c
 ,CAST(SM.Employer_Employee as varchar(255)) AS LSB_CAS_EmployerEmployee__c
 ,CAST(SM.Specify_if__other_ as varchar(255)) AS LSB_CAS_SpecifyIfOther__c
 , CAST(CASE SM.Applied_To_LSBU
         WHEN 'Has Applied To LSBU' THEN 1 ELSE 0
        END as BIT) AS LSB_CAS_AppliedToLSBU__c
 ,CAST('Gecko Forms' as varchar(255)) AS Origin
 ,CAST((SELECT Id FROM SFC.RecordType WHERE Name like 'Prospect Capture Form') as varchar(18)) AS RecordTypeId
 ,CAST('Closed' as varchar(255)) AS Status
 ,CAST('Resolved' as varchar(255)) AS LSB_CAS_ClosedReason__c
 /*
 ,CAST(COALESCE(SM.last_Clearing_Interest_form_submission, SM.last_Clearing_Interest_form_submission
           ,SM.last_Contact_Us_form_submission, SM.last_Contact_Us_form_submission
           ,SM.last_Croydon_Interest_form_submission, SM.last_Croydon_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Get_In_Touch_Interest_form_submission, SM.last_Get_In_Touch_Interest_form_submission
           ,SM.last_Gecko_form_submission, SM.last_Gecko_form_submission
           ,SM.last_Outreach_and_Recruitment_form_submission, SM.last_Outreach_and_Recruitment_form_submission
           ,SM.last_Prospectus_Interest_form_submission, SM.last_Prospectus_Interest_form_submission
           ,SM.last_Register_Your_Interest_form_submission) as datetime) AS ClosedDate
 */
 ,TRY_CONVERT(DATETIME, CASE WHEN SM.created_date > GETDATE() THEN TRY_CONVERT(DATETIME, CAST(GETDATE() AS DATE)) ELSE SM.created_date END) AS ClosedDate
 ,CAST(ST.Id as varchar(18)) AS ContactId
 ,CAST(SM.Emergency_Contact_Name as varchar(255))AS LSB_CAS_EmergencyContactName__c
 ,CAST(SM.Emergency_Contact_Number as varchar(255))AS LSB_CAS_EmergencyPhoneNo__c
 ,CAST(DCRL.DestinationValue as varchar(255)) AS LSB_CAS_EmergencyContactType__c
 , CAST(SM.Emergency_Contact_FirstName as varchar(255))  as LSB_CAS_EmergencyContactFirstName__c
 ,CAST(SM.Emergency_Contact_LastName as varchar(255))  AS LSB_CAS_EmergencyContactLastName__c,
 CAST(CASE SM.E_mail_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_EmailOptIn__c,
 CAST(CASE SM.Phone_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_PhoneOptIn__c,
 CAST(CASE SM.SMS_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_SMSOptIn__c,
 CAST(CASE SM.Post_Opt_In WHEN 'Yes' THEN 1 ELSE 0 END AS BIT) AS LSB_CAS_PostOptIn__c
FROM INT.SRC_Connect_Salesforce_Migration SM
 JOIN [SFC].[get_ID_USER] OWN on  OWN.username like 'dataintegrationuser@lsbu.com%'
 JOIN  SFC.[Get_ID_Contact] ST ON SM.QL_Student_ID = ST.LSB_ExternalID__c   --Changed from INNER to LEFT
 LEFT JOIN [DST].[DICT_Nationality] DNT ON SM.Nationality = DNT.SourceValue
 LEFT JOIN [DST].[DICT_ConnectCRMContactRelationship] DCRL ON DCRL.SourceValue = SM.Emergency_Contact_Relationship
 WHERE SM.QL_Student_ID is NOT NULL

 ) Q1
 LEFT JOIN [DST].[DICT_HearAboutUs] DHAU ON DHAU.Source_val = Q1.LSB_CAS_HearAboutUs__c
 LEFT JOIN [DST].[DICT_CountryOfResidence] DCOR ON DCOR.Source_val = Q1.LSB_CAS_CountryOfResidence__c
 LEFT JOIN [DST].[DICT_Case_FormName] DCF ON DCF.Source_val = Q1.LSB_CAS_FormName__c