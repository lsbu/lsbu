CREATE OR ALTER VIEW [DST].[Case_MAXCoronaEnquiry] AS
SELECT 
	AccountId
	,AssetId
	,Origin
	,Reason
	,RecordTypeId
	,LSB_CAS_CaseResponded__c
	,SourceId
	,hed__Category__c
	,LSB_CAS_ClosedReason__c
	,LSB_CAS_ClosureComment__c
	,LSB_CAS_FirstName__c
	,LSB_CAS_LastName__c
	,ContactId
	,LSB_CAS_DateOfBirth__c
	,ClosedDate
	,CreatedDate
	,EntitlementId
	,SlaExitDate
	,SlaStartDate
	,Comments
	,LSB_CAS_KnowledgeArticleLink__c
	,Language
	,LastModifiedDate
	,LSB_CAS_LevelOfIntendedStudy__c
	,hed__Location__c
	,LSB_CAS_LSBUCanContactMeVia__c
	,LSB_CAS_LSBUCanContactMeAbout__c
	,LSB_CAS_ExternalID__c
	,LSB_CAS_Response__c
	,LSB_CAS_SourceSystem__c
	,LSB_CAS_SourceSystemID__c
	,Description
	,hed__Occurrence_Date__c
	,LSB_CAS_Over48Hours__c
	,ParentId
	,Priority
	,LSB_CAS_SLAViolation__c
	,Status
	,Subject
	,LSB_CAS_SubjectAreaOfInterest__c
	,LSB_CAS_Topic__c
	,Type
	,LSB_CAS_UkEuOrInternationalStudent__c
	,SuppliedCompany
	,SuppliedEmail
	,LSB_CAS_YearOfIntendedStudy__c
	,sfal__StudentID__c
FROM 
(SELECT
	 CAST(NULL as varchar(18)) as AccountId
	,CAST(NULL as varchar(18)) as AssetId
	--,CAST(NULL as bit) as LSB_CAS_AvoidValidationField__c
	--,CAST(NULL as varchar(18)) as BusinessHoursId
	--,CAST(NULL as varchar(30)) as CaseNumber
	,CAST('Maximiser' as varchar(255)) as Origin
	--,CAST(NULL as varchar(18)) as OwnerId
	--,CAST(NULL as bit) as LSB_CAS_CaseOwnerIsUser__c
	,CAST(NULL as varchar(255)) as Reason
	,CAST(RT.Id as varchar(18)) as RecordTypeId
	,CAST(NULL as varchar(1300)) as LSB_CAS_CaseResponded__c
	,CAST(NULL as varchar(18)) as SourceId
	,CAST(NULL as varchar(255)) as hed__Category__c
	,CAST('Resolved' as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(NULL as varchar(max)) as LSB_CAS_ClosureComment__c
	--,CAST(NULL as varchar(80)) as ContactEmail
	--,CAST(NULL as varchar(40)) as ContactFax
	,CAST(NULL as varchar(255)) as LSB_CAS_FirstName__c
	,CAST(NULL as varchar(255)) as LSB_CAS_LastName__c
	--,CAST(NULL as varchar(40)) as ContactMobile
	,CAST(GIDC.Id as varchar(18)) as ContactId
	--,CAST(NULL as varchar(40)) as ContactPhone
	--,CAST(NULL as varchar(18)) as CreatedById
	,CAST(NULL as date) as LSB_CAS_DateOfBirth__c
	,CAST(DATEADD(dd, -1, GETDATE()) as datetime) as ClosedDate
	,CAST(DATEADD(dd, -1, GETDATE()) as datetime) as CreatedDate
	,CAST(NULL as varchar(18)) as EntitlementId
	,CAST(NULL as datetime) as SlaExitDate
	,CAST(NULL as datetime) as SlaStartDate
	--,CAST(NULL as bit) as IsEscalated
	--,CAST(NULL as bit) as LSB_CAS_ForReview__c
	,CAST(NULL as varchar(4000))as Comments
	--,CAST(NULL as bit) as LSB_CAS_InternationalEnquiry__c
	--,CAST(NULL as bit) as LSB_CAS_IsMyCase__c
	,CAST(NULL as varchar(255)) as LSB_CAS_KnowledgeArticleLink__c
	,CAST(NULL as varchar(40)) as Language
	,CAST(NULL as date) as LastModifiedDate
	,CAST(NULL as varchar(255)) as LSB_CAS_LevelOfIntendedStudy__c
	,CAST(NULL as varchar(255)) as hed__Location__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeVia__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeAbout__c
	,CAST(CONCAT(IDS.Student_ID,'.CoronaEnquiry.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID) ) as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST('Your case has been closed please contact us if your query is outstanding' as varchar(max)) as LSB_CAS_Response__c
	,CAST('Maximiser' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(CONCAT(IDS.Student_ID,'.CoronaEnquiry') as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST(IDLSM.[Corona_query] as varchar(max)) as Description
	,CAST(NULL as datetime) as hed__Occurrence_Date__c
	,CAST(NULL as varchar(1300))as LSB_CAS_Over48Hours__c
	,CAST(NULL as varchar(18)) as ParentId
	,CAST(NULL as varchar(255)) as Priority
	--,CAST(NULL as bit) as LSB_CAS_FollowUpRequired__c
	,CAST(NULL as varchar(255)) as LSB_CAS_SLAViolation__c
	,CAST('Closed' as varchar(255)) as Status
	,CAST(CONCAT(GIDC.Name, ' - ', IDS.Student_ID, ' - Corona Query Case - ', DATEADD(dd, -1, GETDATE())) as varchar(255)) as Subject
	,CAST(NULL as varchar(255)) as LSB_CAS_SubjectAreaOfInterest__c
	,CAST(NULL as varchar(255)) as LSB_CAS_Topic__c
	,CAST(NULL as varchar(255)) as Type
	,CAST(NULL as varchar(255)) as LSB_CAS_UkEuOrInternationalStudent__c
	,CAST(NULL as varchar(80)) as SuppliedCompany
	,CAST(NULL as varchar(80)) as SuppliedEmail
	--,CAST(NULL as bit) as LSB_CAS_WillProvideFeedbackViaEmail__c
	,CAST(NULL as varchar(255)) as LSB_CAS_YearOfIntendedStudy__c
	,CAST(IDS.Student_ID as varchar(255)) as sfal__StudentID__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLSM
INNER JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS ON IDS.ID = IDLSM.ID 
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_Enquiry'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
WHERE IDLSM.[Corona_query] IS NOT NULL) as T1
WHERE T1.LSB_CAS_ExternalID__c NOT IN (select LSB_CAS_ExternalID__c from sfc.Get_ID_Case WHERE LSB_CAS_ExternalID__c IS NOT NULL)