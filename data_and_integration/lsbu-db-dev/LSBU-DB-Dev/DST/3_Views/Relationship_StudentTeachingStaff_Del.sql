CREATE OR ALTER VIEW [DST].[Relationship_StudentTeachingStaff_Del] AS
SELECT 
  r.id AS ID 
 /* ,CAST(GIDC.Id as varchar(18)) AS hed__Contact__c
  ,CAST(RLC.ID as varchar(90)) AS hed__RelatedContact__c
  ,CAST(CASE 
         WHEN ChangeStatus = 'DELETE' THEN 'Former' 
         ELSE 'Current' 
        END as varchar(255)) AS hed__Status__c
  ,CAST('Personal Tutor' as varchar(255)) AS hed__Type__c
  ,CAST('CMIS' as varchar(255)) AS LSB_RTP_SourceSystem__c
  ,CAST(STR.LSB_ExternalID as varchar(90)) AS LSB_RTP_SourceSystemID__c 
  ,CAST(STR.LSB_ExternalID as varchar(90)) AS LSB_RTP_ExternalId__c
  ,CAST(COF.Id as varchar(18)) AS LSB_RTP_Module__c*/

FROM [INT].[SRC_StudentTeachingStaff_Relationship] as STR
/* LEFT JOIN SFC.Get_ID_Contact GIDC ON STR.CMIS_StudentID = GIDC.LSB_ExternalID__c
 JOIN SFC.Get_ID_Contact RLC ON STR.TeachingStaff_LSB_ExternalID = RLC.LSB_ExternalID__c
 JOIN SFC.Get_ID_CourseOffering COF ON COF.LSB_COF_ExternalID__c = STR.ModuleOffering_LSB_ExternalID*/
 JOIN SFC.Get_ID_Relationship r on r.LSB_RTP_ExternalId__c=STR.LSB_ExternalID

WHERE (STR.ChangeStatus IN ('DELETE')

--or exists ( select *  from DST.Contact_StudentTeachingStaff_Del C where c.ID = GIDC.Id  )

)


