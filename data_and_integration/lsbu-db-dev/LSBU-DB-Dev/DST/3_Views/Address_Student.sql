CREATE OR ALTER VIEW [DST].[Address_Student] AS
SELECT distinct
  CAST(QLSA.QL_AddressType as varchar(255)) as [hed__Address_Type__c],
  CAST(CASE
	  	WHEN QLSA.QL_AddressType = 'Permanent' THEN 1
		  ELSE 0
	    END as bit) as [hed__Default_Address__c],-- [bit] NOT NULL,
  CAST(null as date) as [hed__Latest_End_Date__c],-- [datetime] NULL,
  CAST(null as date) as [hed__Latest_Start_Date__c],-- [datetime] NULL,
  CAST(SUBSTRING(QLSA.QL_Add4,1,40) as varchar(255)) as [hed__MailingCity__c],-- [varchar](255) NULL,
  CAST(ISNULL(DAC_Country.[MailingCountry], DAC_Add5.[MailingCountry]) as varchar(255)) as [hed__MailingCountry__c],-- [varchar](255) NULL,
  CAST(QLSA.QL_Postcode as varchar(255)) as [hed__MailingPostalCode__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__MailingState__c],-- [varchar](255) NULL,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLSA.QL_Add1, ''),' ' + NULLIF(QLSA.QL_Add2, ''),' ' + NULLIF(QLSA.QL_Add3, '')),1,1,'') as varchar(255)) as [hed__MailingStreet__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__MailingStreet2__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Parent_Account__c],-- [varchar](18) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_End_Day__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_End_Month__c],-- [varchar](255) NULL,
  CAST(null as real) as [hed__Seasonal_End_Year__c],-- [real] NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_Start_Day__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_Start_Month__c],-- [varchar](255) NULL,
  CAST(null as real) as [hed__Seasonal_Start_Year__c],-- [real] NULL,
  CAST(GIDC.Id as varchar(18)) as [hed__Parent_Contact__c],-- [varchar](18) NULL,
  CAST(QLSA.LSB_ExternalID as varchar(90)) as [LSB_ADS_ExternalID__c],-- [varchar](90) NULL,
  CAST(QLSA.QL_AddressID as varchar(90)) as [LSB_ADS_SourceSystemID__c],-- [varchar](90) NULL,
  CAST(QLSA.SrcSystem as varchar(255)) as [LSB_ADS_SourceSystem__c] -- [varchar](255) NULL

FROM [INT].[SRC_Student_Address] as QLSA
  LEFT JOIN [DST].[DICT_QL_Add5_Country] DAC_Add5 ON DAC_Add5.[QL_Add5] = QLSA.QL_Add5
  LEFT JOIN [DST].[DICT_QL_Add5_Country] DAC_Country ON DAC_Country.[QL_Add5] = QLSA.QL_Country
  INNER JOIN SFC.Get_ID_Contact GIDC ON GIDC.LSB_ExternalID__c = QLSA.QL_StudentID
WHERE ( QLSA.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select R.LSB_ADS_ExternalID__c from DST.SF_Reject_Address R WHERE QLSA.LSB_ExternalID = R.LSB_ADS_ExternalID__c)
OR
 (NOT EXISTS (SELECT * FROM [SFC].[Get_ID_Address] GIAA WHERE GIAA.LSB_ADS_ExternalID__c = QLSA.LSB_ExternalID) AND QLSA.ChangeStatus <> 'DELETE')
OR 
EXISTS (Select GIA.LSB_ADS_ExternalID__c 
		FROM DST.SF_Reject_Address_Update RU
		JOIN sfc.Get_ID_Address GIA ON RU.Id = GIA.Id
		WHERE GIA.LSB_ADS_ExternalID__c = QLSA.LSB_ExternalID)
)
GO
