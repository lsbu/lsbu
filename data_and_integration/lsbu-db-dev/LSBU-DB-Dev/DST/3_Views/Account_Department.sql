CREATE OR ALTER VIEW [DST].[Account_Department] AS
SELECT 
  CAST(QLDE.QL_DepartmentName as varchar(255))as Name,
  CAST((select Id from sfc.RecordType WHERE DeveloperName = 'University_Department') as varchar(18)) as RecordTypeId,
  CAST(QLEI.Id as varchar(18)) as ParentId,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLDE.QL_Add1, ''),' ' + NULLIF(QLDE.QL_Add2, ''),' ' + NULLIF(QLDE.QL_Add3, '')),1,1,'') as varchar(255)) as BillingStreet,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLDE.QL_Add4, ''),' ' + NULLIF(QLDE.QL_Add5, '')),1,1,'') as varchar(40)) as BillingCity,
  CAST(QLDE.QL_Postcode as varchar(20)) as BillingPostalCode,
  CAST(QLDE.QL_Country as varchar(80))as BillingCountry,
  CAST(QLDE.QL_Phone as varchar(40))as Phone,
  CAST(null as varchar(40)) as Fax,
  CAST(null as varchar(40))as AccountNumber,
  CAST(QLDE.QL_Website as varchar(255)) as Website,
  CAST(null as varchar(20)) as Sic,
  CAST(null as varchar(255)) as Industry,
  CAST(null as decimal(18, 0)) as AnnualRevenue,
  CAST(null as int) as NumberOfEmployees,
  CAST(null as varchar(20)) as TickerSymbol,
  CAST(QLDE.QL_Description as varchar(max))as Description,
  CAST(null as varchar(255)) as Rating,
  CAST(null as varchar(80)) as Site,
  CAST(null as varchar(255)) as AccountSource,
  CAST(null as varchar(80)) as SicDesc,
  CAST(null as varchar(18)) as hed__Primary_Contact__c,
  CAST(null as varchar(18)) as hed__Current_Address__c,
  CAST(null as varchar(80)) as hed__Shipping_County__c,
  CAST(QLDE.LSB_ExternalID as varchar(90)) as LSB_ACC_ExternalID__c,
  CAST(QLDE.QL_DepartmentId as varchar(90)) as LSB_ACC_SourceSystemID__c,
  CAST(QLDE.SrcSystem as varchar(255)) as LSB_ACC_SourceSystem__c,
  CAST(QLDE.QL_DepartmentId as varchar(90)) as LSB_ACC_DepartmentCode__c
FROM [INT].[SRC_Department] as QLDE
JOIN [SFC].Get_ID_Account QLEI ON QLDE.QL_EducationInstitutionId = QLEI.LSB_ACC_ExternalID__c
WHERE (
QLDE.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select LSB_ACC_ExternalID__c from DST.SF_Reject_Account_Department WHERE QLDE.LSB_ExternalID = LSB_ACC_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLDE.LSB_ExternalID
				AND	  VALID.ObjectName = 'Department'
				AND	  VALID.SrcSystem = 'QL')
GO