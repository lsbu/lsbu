
CREATE OR ALTER    VIEW [DST].[Contact_StudentTeachingStaff_Del] AS
SELECT
C.ID,
 CAST(null as varchar(18)) as AccountId,
 CAST(STC.CMIS_TeachingStaffLastname as varchar(80)) as LastName,
 CAST(STC.CMIS_TeachingStaffFirstName as varchar(40)) as FirstName,
 CAST(STC.CMIS_TeachingStaffEmail as varchar(80)) as Email,
 CAST(STC.CMIS_TeachingStaffEmail as varchar(80)) as hed__AlternateEmail__c,
 CAST('University Email' as varchar(255)) as hed__Preferred_Email__c, 
 CAST(STC.CMIS_TeachingStaffEmail as varchar(80)) as hed__UniversityEmail__c,
 CAST(STC.SrcSystem as varchar(255)) as LSB_SourceSystem__c,
 CAST(STC.CMIS_TeachingStaffId as varchar(90)) as LSB_SourceSystemID__c,
 CAST(STC.LSB_ExternalID as varchar(90)) as LSB_ExternalID__c,
 CAST('Staff' as varchar(255)) as LSB_CON_CurrentRole__c
FROM [INT].[SRC_StudentTeachingStaff_Contact] as STC
  JOIN SFC.Get_ID_Contact C ON C.LSB_ExternalID__c=STC.LSB_ExternalID
WHERE ( STC.ChangeStatus IN ('DELETE')

)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
			WHERE VALID.ValidationResultFlag = 'ERROR'
			AND   VALID.LSB_ExternalID = STC.LSB_ExternalID
			AND	  VALID.ObjectName = 'StudentTeachingStaff_Contact'
			AND	  VALID.SrcSystem = 'CMIS')



