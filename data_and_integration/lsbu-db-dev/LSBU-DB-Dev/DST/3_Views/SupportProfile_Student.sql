CREATE OR ALTER VIEW [DST].[SupportProfile_Student] AS
SELECT
  CONVERT(varchar(18), gic.Id) AS LSB_SUE_Contact__c,
  CONVERT(nvarchar(80), gic.Name + ' - ' + sp.QL_StudentID + ' - Support Profile') AS Name,
  CONVERT(nvarchar(255), CASE WHEN gisp.Id IS NULL THEN 'New' ELSE gisp.LSB_SUE_Status__c END) AS LSB_SUE_Status__c,
  CONVERT(nvarchar(90), sp.SrcSystem) AS LSB_SUE_SourceSystem__c,
  CONVERT(nvarchar(90), sp.LSB_ExternalID) AS LSB_SUE_SourceSystemId__c,
  CONVERT(nvarchar(90), sp.LSB_ExternalID) AS LSB_SUE_ExternalId__c,
  CAST(SGICA.Id AS VARCHAR(18)) AS hed__Course_Enrollment__c
FROM [INT].[SRC_Student_SupportProfile] AS sp
  LEFT JOIN [SFC].[Get_ID_SupportProfile] AS gisp ON gisp.LSB_SUE_ExternalId__c = sp.LSB_ExternalID
  JOIN [SFC].[Get_ID_Contact] AS gic ON gic.LSB_ExternalID__c = sp.QL_StudentID
  LEFT JOIN [SFC].[Get_ID_Case] SGICA ON SGICA.ContactId = gic.Id AND SGICA.RecordTypeId in (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord') 
WHERE (
    sp.ChangeStatus IN ('NEW','UPDATE')
    OR EXISTS (SELECT 1 FROM [DST].[SF_Reject_SupportProfile_Student] r WHERE sp.LSB_ExternalID = r.LSB_SUE_ExternalId__c)
  )
  AND NOT EXISTS (
    SELECT 1
    FROM [RPT].[SRCValidationsSummary] vs
    WHERE vs.ValidationResultFlag = 'ERROR'
      AND	vs.SrcSystem = 'QL'
      AND	vs.ObjectName = 'Student'
      AND vs.LSB_ExternalID = sp.QL_StudentID
  )
GO