CREATE OR ALTER VIEW [DST].[SupportArrangement_MAXSupportArrangements] AS
Select * FROM 	
(	SELECT																			-- Assignment Arrangements																									
			--CAST(NULL as varchar(18)) AS [OwnerId]
			--,CAST(NULL as varchar(255)) AS [IsDeleted]
       CAST(CONCAT(GIDC.FirstName,' ', GIDC.LastName,'-',IDS.Student_ID, '- Support Arrangement')as varchar(255)) AS [Name]
			--,CAST(NULL as DATETIME) AS [CreatedDate]
			--,CAST(NULL as varchar(18)) AS [CreatedById]
			--,CAST(NULL as DATETIME) AS [LastModifiedDate]
			--,CAST(NULL as varchar(18)) AS [LastModifiedById]
			,CAST(GIDC.Id as varchar(18)) AS [LSB_SUT_Contact__c]
			,CAST(1 as BIT) AS [LSB_SUT_Current__c]
			,CAST(CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') as varchar(255)) AS [LSB_SUT_ExternalId__c]
			,CAST(CONCAT(IDS.Student_ID,'.Maximiser Support Arrangement') as varchar(255)) AS [LSB_SUT_SourceSystemId__c]
			,CAST('Maximiser' as varchar(255)) AS [LSB_SUT_SourceSystem__c]
			,CAST((CASE WHEN UPPER(IDLM.[DDS_Support_Arrangements_Updated_QL]) = 'YES' THEN 1 ELSE 0 END) as BIT) AS [LSB_SUT_UpdatedQLSupportArrangement__c]
			, RANK() OVER ( PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID, IDLM.[DDS_Support_Arrangements_Updated_QL] DESC) AS SA_RANK

	 FROM [SRC].[Maximizer_PIVOT_IndividualDetailString] IDS
	 INNER JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON IDS.ID = IDLM.ID
	 LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c 
	 where 
    IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students]) AND (
	  IDLM.[DDS_Support_Arrangements_Assignment_Arrangements] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Examination_Arrangements] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Field_trip_support] IS NOT NULL
		OR IDLM.[DDS_Support_Arrangements_For_Information] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Library_Support] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Other_support_for_student] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Presentation_Arrangements] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Teaching_Arrangements] IS NOT NULL
	  OR IDLM.[DDS_Support_Arrangements_Timetabling_and_furniture] IS NOT NULL
    )
) A where A.SA_RANK = 1