CREATE OR ALTER VIEW [DST].[ContentVersion_MAXNotes]
AS
SELECT
	CreatedDate
	,Lastmodifieddate
	,CreatedbyId						-- NewField to be mapped
	,LastmodifiedbyId				-- NewField to be mapped
	,FirstPublishLocationId
	,LSB_CVR_DocumentType__c
	,LSB_CVR_ExternalId__c
	,LSB_CVR_SourceSystemId__c
	,LSB_CVR_SourceSystem__c
	,SharingOption
	,SharingPrivacy
	,PathOnClient
	,Title
	,CONCAT('Contents: ',T1.VersionData) as VersionData
FROM
(------------------------------------------------Support Profile Notes-----------------------------------------------------------------------------
SELECT 
CAST(MSIN.Date as datetime) AS CreatedDate
,CAST(MSIN.Date as datetime) AS Lastmodifieddate
,CAST(CbyId.Id as varchar(18)) AS CreatedbyId
,CAST(CbyId.Id as varchar(18)) AS LastmodifiedbyId
--,CAST('snote' AS VARCHAR(255)) AS FileExtension
--,CAST('SNOTE' AS VARCHAR(255)) AS FileTYPE
,CAST(GISP.Id as varchar(18)) FirstPublishLocationId
,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(IDS.Student_ID,'.',MSIN.IndividualId,'.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID,MSIN.IndividualId ORDER BY IDS.Student_ID,MSIN.IndividualId)) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(MSIN.IndividualId AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('Maximiser' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST(NULL AS VARCHAR(255)) SharingPrivacy
,CAST(CONCAT(IDS.Student_ID,'-MaximiserNotes.snote') AS VARCHAR(500)) AS PathOnClient
,CAST(CONCAT(CONVERT(varchar,MSIN.Date,20),' - ',IDS.Student_ID,' - ',MSIN.Type,' - MaximiserNotes') AS VARCHAR(255)) AS Title
,CAST(
REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( MSIN.Text, '<html>', ''), '</html>', ''), '<head>', ''), '</head>', ''), '<body>', ''), '</body>', ''), '<p>', ''), '</p>', '') , '<', ''), '>', '')
,'&', '&amp;')
,'''', '&#39;')
,'"','&quot;')
AS VARCHAR(max)) as VersionData
FROM [Maximizer].[src].[Individual_NOTE] MSIN
--INNER JOIN [Maximizer].[src].[Individual_Note_Base64] MSINB on MSIN.IndividualId = MSIN.IndividualId
INNER JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON IDS.Id = MSIN.IndividualId 
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = CONCAT(IDS.Student_ID,'.SP')
LEFT JOIN SFC.Get_ID_User CbyId ON MSIN.Creator = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))

UNION ALL
------------------------------------------------Case Notes-----------------------------------------------------------------------------
SELECT 
		CAST(MSIN.Date as datetime) AS CreatedDate
		,CAST(MSIN.Date as datetime) AS Lastmodifieddate
		,CAST(CbyId.Id as varchar(18)) AS CreatedbyId
		,CAST(CbyId.Id as varchar(18)) AS LastmodifiedbyId
		--,CAST('snote' AS VARCHAR(255)) AS FileExtension
		--,CAST('SNOTE' AS VARCHAR(255)) AS FileTYPE
		,CAST(GIC.Id as varchar(18)) FirstPublishLocationId
		,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
		,CAST(CONCAT(C.CaseId,'.',ROW_NUMBER() OVER (PARTITION BY C.CaseId ORDER BY C.CaseId)) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
		,CAST(C.CaseId AS VARCHAR(255))LSB_CVR_SourceSystemId__c
		,CAST('Maximiser' as Varchar(255)) as LSB_CVR_SourceSystem__c
		,CAST(NULL AS VARCHAR(255)) as SharingOption
		,CAST(NULL AS VARCHAR(255)) SharingPrivacy
		,CAST(CONCAT(C.CaseId,'-MaximiserNotes.snote') AS VARCHAR(500)) AS PathOnClient
		,CAST(CONCAT(CONVERT(varchar,MSIN.Date,20),' - ',C.CaseNumber,' - ',MSIN.Type,' - MaximiserNotes') AS VARCHAR(255)) AS Title
		,CAST(
		REPLACE(REPLACE(REPLACE(
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( MSIN.Text, '<html>', ''), '</html>', ''), '<head>', ''), '</head>', ''), '<body>', ''), '</body>', ''), '<p>', ''), '</p>', '') , '<', ''), '>', '')
		,'&', '&amp;')
		,'''', '&#39;')
		,'"','&quot;')
		AS VARCHAR(max)) as VersionData
FROM [Maximizer].[src].[Case_NOTE] MSIN
--INNER JOIN [Maximizer].[src].[Case_Note_Base64] MSINB on MSIN.CaseId = MSINB.CaseId
INNER JOIN [Maximizer].src.[Case] C ON C.CaseId = MSIN.CaseId
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_SourceSystemID__c = C.CaseNumber
LEFT JOIN SFC.Get_ID_User CbyId ON MSIN.Creator = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))) AS T1
WHERE T1.LSB_CVR_ExternalId__c NOT IN (select LSB_CVR_ExternalId__c from sfc.Get_ID_ContentVersion WHERE LSB_CVR_ExternalID__c IS NOT NULL)