CREATE OR ALTER VIEW [DST].[Term_Grade] AS
SELECT 
	--CAST(NULL AS VARCHAR(18)) AS CreatedById
	--,CAST(NULL AS DATETIME) AS CreatedDate
	CAST(GIDC.Id AS VARCHAR(18)) AS hed__Contact__c
	,CAST(GIDCE.Id AS VARCHAR(18)) AS hed__Course_Connection__c
	,CAST(GIDCO.Id AS VARCHAR(18)) AS hed__Course_Offering__c
	,CAST(NULL AS REAL) AS hed__Credits_Attempted__c
	,CAST(GIDCO2.LSB_COF_ModuleCredit__c AS REAL) AS hed__Credits_Earned__
	,CAST(NULL AS VARCHAR(255)) AS hed__Letter_Grade__c
	,CAST(NULL AS REAL) AS hed__Numerical_Grade__c
	,CAST(NULL AS REAL) AS hed__Percent_Grade__c
	,CAST(NULL AS VARCHAR(255)) AS hed__Result__c
	,CAST(NULL AS VARCHAR(255)) AS hed__Term_Grade_Type__c
	,CAST(GIT.Id AS VARCHAR(18)) AS hed__Term__c			-- New Field map
	--,CAST(NULL AS VARCHAR(18)) AS Id
	--,CAST(NULL AS BIT) AS IsDeleted
	--,CAST(NULL AS DATE) AS LastActivityDate
	--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
	--,CAST(NULL AS DATETIME) AS LastModifiedDate
	--,CAST(NULL AS DATETIME) AS LastReferencedDate
	--,CAST(NULL AS DATETIME) AS LastViewedDate
	--,CAST(NULL AS VARCHAR(80)) AS Name
	--,CAST(NULL AS VARCHAR(18)) AS OwnerId
	--,CAST(NULL AS DATETIME) AS SystemModstamp
	--,CAST(NULL AS VARCHAR(1300)) AS LSB_TDE_AcademicPeriod__c
	--,CAST(NULL AS VARCHAR(18)) AS LSB_TDE_Term__c
	--,CAST(NULL AS VARCHAR(1300)) AS LSB_TDE_CourseName__c
	--,CAST(NULL AS VARCHAR(1300)) AS LSB_TDE_ModuleId__c
	,CAST(ISNULL(QLGD.CW1, 'N/A') AS VARCHAR(20)) AS LSB_TDE_CourseWork1__c
	,CAST(ISNULL(QLGD.CW2, 'N/A') AS VARCHAR(20)) AS LSB_TDE_CourseWork2__c
	,CAST(ISNULL(QLGD.CW3, 'N/A') AS VARCHAR(20)) AS LSB_TDE_CourseWork3__c
	,CAST(ISNULL(QLGD.CW4, 'N/A') AS VARCHAR(20)) AS LSB_TDE_CourseWork4__c
	,CAST(ISNULL(QLGD.CW5, 'N/A') AS VARCHAR(20)) AS LSB_TDE_CourseWork5__c
	,CAST(ISNULL(QLGD.CW6, 'N/A') AS VARCHAR(20)) AS LSB_TDE_CourseWork6__c
	,CAST(ISNULL(QLGD.EX1, 'N/A') AS VARCHAR(20)) AS LSB_TDE_Exam1__c
	,CAST(ISNULL(QLGD.EX2, 'N/A') AS VARCHAR(20)) AS LSB_TDE_Exam2__c
	,CAST(ISNULL(QLGD.EX3, 'N/A') AS VARCHAR(20)) AS LSB_TDE_Exam3__c
	,CAST(ISNULL(QLGD.EX4, 'N/A') AS VARCHAR(20)) AS LSB_TDE_Exam4__c
	,CAST(ISNULL(QLGD.EX5, 'N/A') AS VARCHAR(20)) AS LSB_TDE_Exam5__c
	--,CAST(NULL AS REAL) AS LSB_TDE_ModuleCredit__c
	,CAST(QLGD.QL_Attempt AS VARCHAR(255)) AS LSB_TDE_Attempt__c
	,CAST(QLGD.QL_OverallModuleGrade AS VARCHAR(255)) AS LSB_TDE_ModuleGrade__c
	,CAST(QLGD.QL_OverallModuleMarks AS VARCHAR(20)) AS LSB_TDE_ModuleMarks__c
	--,CAST(NULL AS VARCHAR(1300)) AS LSB_TDE_Semester__c
	--,CAST(NULL AS BIT) AS LSB_TDE_Resit__c
	,CAST('QL' AS VARCHAR(255)) AS LSB_TDE_SourceSystem__c
	,CAST(QLGD.LSB_ExternalID AS VARCHAR(255)) AS LSB_TDE_SourceSystemId__c
	,CAST(QLGD.LSB_ExternalID AS VARCHAR(255)) AS LSB_TDE_ExternalId__c
  ,CAST(QLGD.ProgramEnrolment_LSB_ExternalID AS VARCHAR(255)) AS ProgramEnrolment_LSB_ExternalID
  ,CAST(SGICA.Id AS VARCHAR(18)) AS LSB_PEN_AdviseeCaseRecord__c
FROM [INT].[SRC_Grade] AS QLGD
INNER JOIN SFC.Get_ID_Contact GIDC ON QLGD.QL_StudentID = GIDC.LSB_ExternalID__c
INNER JOIN SFC.Get_ID_CourseEnrollment GIDCE ON QLGD.ModuleEnrolment_LSB_ExternalID = GIDCE.LSB_CCN_ExternalID__c  -- Join set to INNER from LEFT
INNER JOIN SFC.Get_ID_CourseOffering GIDCO ON QLGD.ModuleOffering_LSB_ExternalID = GIDCO.LSB_COF_ExternalID__c
LEFT JOIN SFC.Get_ID_CourseOffering GIDCO2 ON QLGD.ModuleOffering_LSB_ExternalID = GIDCO2.LSB_COF_ExternalID__c and QLGD.QL_OverallModuleGrade = 'P'
INNER JOIN SFC.Get_ID_Term GIT on QLGD.Term_LSB_ExternalID = GIT.LSB_TRM_ExternalID__c    -- New Join 
LEFT JOIN [SFC].[Get_ID_Case] SGICA ON SGICA.ContactId = GIDC.Id AND SGICA.RecordTypeId in (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord') 
WHERE (QLGD.ChangeStatus IN ('NEW','UPDATE') 
	OR EXISTS (Select R.LSB_TDE_ExternalId__c from DST.SF_Reject_TermGrade R WHERE QLGD.LSB_ExternalID = R.LSB_TDE_ExternalId__c)
	OR (NOT EXISTS (SELECT * FROM [SFC].Get_ID_TermGrade GITG WHERE GITG.LSB_TDE_ExternalId__c = QLGD.LSB_ExternalID) AND QLGD.ChangeStatus <> 'DELETE'))
	AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLGD.LSB_ExternalID
				AND	  VALID.ObjectName = 'Grade'
				AND	  VALID.SrcSystem = 'QL')