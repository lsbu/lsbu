CREATE OR ALTER VIEW [DST].[Contact_ConnectCRM] AS
SELECT
	CAST(null as varchar(18)) as AccountId,
	CAST(SM.Emergency_Contact_Number as varchar(40)) as HomePhone,
	CAST(NULL as varchar(40)) as MobilePhone, -- New Field
	CAST(IIF(SM.Emergency_Contact_Number is not null, 'Home', null) as varchar(255)) as hed__PreferredPhone__c,
	CAST(SM.Emergency_Contact_FirstName as varchar(80)) as FirstName,  -- New Field
	CAST(SM.Emergency_Contact_LastName as varchar(80)) as LastName,
	CAST('Connect' as varchar(255)) as LSB_SourceSystem__c,
	CAST(SM.Connect_ID as varchar(90)) as LSB_SourceSystemID__c,
	CAST(CONCAT(SM.Connect_ID, SM.Emergency_Contact_Relationship, '.Connect') as varchar(90)) as LSB_ExternalID__c,
	CAST('External to LSBU' as varchar(255)) as LSB_CON_CurrentRole__c,
	CAST( null as varchar(18) )LSB_CON_User__c,
	CAST(NULL as varchar(80)) as hed__AlternateEmail__c,  -- New Field
	CAST(NULL as varchar(255)) as hed__Preferred_Email__c, -- New Field
	CAST(NULL as varchar(255)) as hed__Gender__c,-- New Field
	CAST(NULL as date) as Birthdate -- New Field
FROM INT.SRC_Connect_Salesforce_Migration SM
WHERE SM.Emergency_Contact_Name is not null and SM.Emergency_Contact_Relationship is not null

UNION ALL

SELECT 
	AccountId,
	HomePhone,
	MobilePhone, -- New Field
	hed__PreferredPhone__c,
	FirstName,  -- New Field
	LastName,
	LSB_SourceSystem__c,
	LSB_SourceSystemID__c,
	LSB_ExternalID__c,
	LSB_CON_CurrentRole__c,
	LSB_CON_User__c,
	hed__AlternateEmail__c,  -- New Field
	hed__Preferred_Email__c, -- New Field
	hed__Gender__c,-- New Field
	Birthdate -- New Field
FROM
(
	
	SELECT
		CAST(null as varchar(18)) as AccountId,
		CAST(NULL as varchar(40)) as HomePhone,
		CAST(SM.Mobile_Number as varchar(40)) as MobilePhone, -- New Field
		CAST(IIF(SM.Mobile_Number is not null, 'Mobile', null) as varchar(255)) as hed__PreferredPhone__c,
		CAST(SM.First_Name as varchar(80)) as FirstName,  -- New Field
		CAST(SM.Last_Name as varchar(80)) as LastName,
		CAST('Connect' as varchar(255)) as LSB_SourceSystem__c,
		CAST(COALESCE(SM.QL_STUDENT_ID,Concat(SM.Connect_ID,'.Connect')) as varchar(90)) as LSB_SourceSystemID__c,
		CAST(COALESCE(SM.QL_STUDENT_ID,Concat(SM.Connect_ID,'.Connect')) as varchar(90)) as LSB_ExternalID__c,
		CAST('Prospect' as varchar(255)) as LSB_CON_CurrentRole__c,
		CAST( null as varchar(18) )LSB_CON_User__c,
		CAST(SM.E_mail as varchar(80)) as hed__AlternateEmail__c,  -- New Field
		CAST(
			CASE 
			WHEN SM.E_mail IS NOT NULL THEN 'Personal Email' --'Alternate'
			ELSE NULL
		END as varchar(255)) as hed__Preferred_Email__c, -- New Field
		CAST(SM.Gender as varchar(255)) as hed__Gender__c,-- New Field
		CAST(CASE 
			WHEN TRY_CONVERT(DATE, SM.DOB) > GETDATE() THEN NULL
			ELSE SM.DOB 
			END as date) as Birthdate, -- New Field
		ROW_NUMBER() OVER(PARTITION BY SM.E_mail  ORDER BY SM.[Created_Date] DESC)  AS R
		FROM INT.SRC_Connect_Salesforce_Migration SM WHERE SM.E_mail IS NOT NULL
		AND NOT EXISTS (SELECT 1 FROM SFC.[Get_ID_Contact] GIC WHERE SM.QL_Student_ID = GIC.LSB_ExternalID__c )
		AND NOT EXISTS (SELECT 1 FROM SFC.[Get_ID_Contact] GIC WHERE lower(SM.E_mail) = lower(GIC.hed__AlternateEmail__c) AND LSB_CON_SourceSystem__c != 'Connect' )
		AND NOT EXISTS (SELECT 1 FROM SFC.[Get_ID_Contact] GIC WHERE lower(SM.E_mail) = lower(GIC.Email) AND LSB_CON_SourceSystem__c != 'Connect'  )
) T WHERE T.R = 1