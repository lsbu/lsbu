
/****** Object:  View [DST].[SupportProfileDetails_MAXSupportProfile_MHWB_DDS_DSA]    Script Date: 2021-05-13 12:33:47 ******/

CREATE OR ALTER  VIEW [DST].[SupportProfileDetails_MAXSupportProfile_MHWB_DDS_DSA] AS
------------------------------------------------------------Current Presenting Concerns	---------------------------------------------------------------------------------
SELECT 
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CAST(0 as BIT) AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAFundedBy__c
		,CAST(NULL as DATE) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SPD_SupportMaster__c
		,DICT_SSD.DST_val as Concerns
		,CAST('Presenting Concerns' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
	    ,CAST(/*(CASE WHEN (IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others != 'None identified') THEN 1 ELSE 0 END))*/ 0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others != 'None identified' THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self != 'None identified'  THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(NULL as varchar(18)) AS SupportProfileDSA__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.CurrentPresentingConcerns.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.CurrentPresentingConcerns' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(
				CONCAT(
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL THEN CONCAT('Threat From Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others  IS NOT NULL THEN CONCAT('Threat To Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self  IS NOT NULL THEN CONCAT('Threat To Self -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self) ELSE NULL END
						) as varchar(MAX)
			) AS LSB_SPD_ThreatDetails__c
		,CAST(NULL AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(NULL AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId and IDLM.Name = 'MH & WB\Current presenting concerns'
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM2 ON IDS.ID = IDLM2.Id 
INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val= IDLM.Value
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_MHWB'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN SFC.Get_ID_SupportArrangementMaster GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Profile' and GISAM.LSB_SUM_SupportArrangementCategory__c= 'Presenting Concerns'
LEFT JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID
WHERE DICT_SSD.Fieldname = 'MH__WB_Current_presenting_concerns' and DICT_SSD.Source_val != 'None'
 AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
  AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------Historical Presenting Concerns	---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CAST(0 as BIT) AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAFundedBy__c
		,CAST(NULL as DATE) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SPD_SupportMaster__c
		,DICT_SSD.DST_val as Concerns
		,CAST('Presenting Concerns' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
	    ,CAST(/*(CASE WHEN (IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others != 'None identified') THEN 1 ELSE 0 END))*/ 0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others != 'None identified' THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self != 'None identified'  THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(NULL as varchar(18)) AS SupportProfileDSA__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.HistoricalPresentingConcerns.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.HistoricalPresentingConcerns' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(
				CONCAT(
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL THEN CONCAT('Threat From Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others  IS NOT NULL THEN CONCAT('Threat To Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self  IS NOT NULL THEN CONCAT('Threat To Self -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self) ELSE NULL END
						) as varchar(MAX)
			) AS LSB_SPD_ThreatDetails__c
		,CAST(NULL AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(NULL AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId and IDLM.Name = 'MH & WB\Historical presenting concerns'
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM2 ON IDS.ID = IDLM2.Id 
INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val= IDLM.Value
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_MHWB'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN SFC.Get_ID_SupportArrangementMaster GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Profile' and GISAM.LSB_SUM_SupportArrangementCategory__c= 'Presenting Concerns'
LEFT JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID 
WHERE DICT_SSD.FieldName= 'MH__WB_Historical_presenting_concerns' and DICT_SSD.Source_val != 'None'
 AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
  AND GISP.ID IS NOT NULL

UNION ALL

------------------------------------------------------------Risk Factors---------------------------------------------------------------------------------
/* not found */
------------------------------------------------------------Contextual Factors---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CAST(0 as BIT) AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAFundedBy__c
		,CAST(NULL as DATE) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SPD_SupportMaster__c
		,DICT_SSD.DST_val as Concerns
    ,CAST('Contextual Factors' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
	   ,CAST(/*(CASE WHEN (IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others != 'None identified') THEN 1 ELSE 0 END))*/ 0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others != 'None identified' THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self != 'None identified'  THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(NULL as varchar(18)) AS SupportProfileDSA__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.Contextualfactors.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.Contextualfactors' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(
				CONCAT(
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL THEN CONCAT('Threat From Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others  IS NOT NULL THEN CONCAT('Threat To Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self  IS NOT NULL THEN CONCAT('Threat To Self -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self) ELSE NULL END
						) as varchar(MAX)
			) AS LSB_SPD_ThreatDetails__c
		,CAST(NULL AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(NULL AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
    
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId and IDLM.Name = 'MH & WB\Contextual factors'
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM2 ON IDS.ID = IDLM2.Id 
INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON IDLM.Value = DICT_SSD.Source_val
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_MHWB'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN SFC.Get_ID_SupportArrangementMaster GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Profile' and GISAM.LSB_SUM_SupportArrangementCategory__c= 'Contextual Factors'
LEFT JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID
WHERE DICT_SSD.Fieldname= 'MH__WB_Contextual_factors' AND DICT_SSD.Source_val != 'None'
 AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
  AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------Academic or University Concern---------------------------------------------------------------------------------
SELECT 
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(IDD.MH__WB_Presenting_concerns_updated_date as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CAST(0 as BIT) AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAFundedBy__c
		,CAST(NULL as DATE) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SPD_SupportMaster__c
 		,DICT_SSD.DST_val as Concerns
		,CAST('Academic or University Concern' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
	   ,CAST(/*(CASE WHEN (IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others != 'None identified') THEN 1 ELSE 0 END))*/ 0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others != 'None identified' THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(/*(CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self IS NOT NULL AND IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self != 'None identified'  THEN 1 ELSE 0 END)*/ 0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(NULL as varchar(18)) AS SupportProfileDSA__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.AcademicorUniversityConcern.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.AcademicorUniversityConcern' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(
				CONCAT(
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL THEN CONCAT('Threat From Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others  IS NOT NULL THEN CONCAT('Threat To Others -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others,CHAR(13)) ELSE NULL END,
						CASE WHEN IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self  IS NOT NULL THEN CONCAT('Threat To Self -> ',IDLM2.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self) ELSE NULL END
						) as varchar(MAX)
			) AS LSB_SPD_ThreatDetails__c
			,CAST(NULL AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(NULL AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN Maximizer.src.Individual_DetailListMulti IDLM ON IDS.ID = IDLM.IndividualId and IDLM.Name = 'MH & WB\Academic or university concerns'
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM2 ON IDS.ID = IDLM2.Id 
INNER JOIN DST.DICT_SPROF_SARR_Details DICT_SSD ON DICT_SSD.Source_val = IDLM.Value
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_MHWB'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN SFC.Get_ID_SupportArrangementMaster GISAM ON GISAM.Name = DICT_SSD.DST_val and GISAM.Object_Used_In__c = 'Support Profile' and GISAM.LSB_SUM_SupportArrangementCategory__c= 'Academic or University Concern'
LEFT JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID
WHERE DICT_SSD.fieldname = 'MH__WB_Academic_or_university_concerns' AND DICT_SSD.Source_val != 'None'
 AND IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
  AND GISP.ID IS NOT NULL

UNION ALL

----------------no change beyond this---------
------------------------------------------------------------ Disability and Dyslexia Support ---------------------------------------------------------------------------------
  SELECT 
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CAST(1 as BIT) AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAFundedBy__c
		,CAST(NULL as DATE) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(IDD.DDS_Support_Arrangements_Supp_Arrs_agreed_date as DATE) AS LSB_SPD_StartDate__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(GISAM.Id as varchar(18)) AS LSB_SPD_SupportMaster__c
		,DNOD.Nature_of_Disability_UNique_list_of_value as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDDS__c
		,CAST(NULL as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(IDS.DDS_Support_Arrangements_Nature_of_Disability as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.DDS.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID)) as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.DDS' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(NULL AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(NULL AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
  FROM (
  SELECT 
    ID
   ,Student_ID
   ,DDS_Support_Arrangements_Nature_of_Disability AS LSB_SPD_Notes__c
	 ,CONCAT(Student_ID,'.DDS.',ROW_NUMBER() OVER(PARTITION BY Student_ID ORDER BY Student_ID)) AS LSB_SPD_ExternalId__c
	 ,Student_ID + '.DDS' AS LSB_SPD_SourceSystemId__c
   ,CASE 
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE 'Hirchsprungs Disease%' THEN  'Hirchsprungs Disease'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '% syndrome, Musculoskeletal Chronic Pain Syndrome, Congenital Hip Dysplasia, Benign Joint Hypermobility Syndrome, Congenital Hip Dysplasia%' THEN 'Asperger?s syndrome, Musculoskeletal Chronic Pain Syndrome, Congenital Hip Dysplasia, Benign Joint Hypermobility Syndrome, Congenital Hip Dysplasia'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '%s Disease, Lung Disease%' THEN 'Chronic Abdominal Pain, Hirchsprung?s Disease, Lung Disease'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '%affecting fine finger control%' THEN 'Chronic Health condition ? affecting fine finger control'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '%syndrome, hypermobility syndrome, Irlen syndrome%' THEN 'Asperger?s syndrome, hypermobility syndrome, Irlen syndrome'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '%Pervasive Development Disorder Not Otherwise Specified (PDDNOS)%' THEN 'Pervasive Development Disorder Not Otherwise Specified (PDDNOS), Attention Deficit Hyperactivity Disorder (ADHD) and Oppositional Defiant Disorder (ODD). Joshua has Sensory Integration Difficulties.'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '%Slow processin%' THEN 'SpLD - Slow processing speed'
    WHEN DDS_Support_Arrangements_Nature_of_Disability LIKE '%Indicators of SpLD % Dyslexia and Dyspraxia%' THEN 'Indicators of SpLD - Dyslexia and Dyspraxia'
    ELSE DDS_Support_Arrangements_Nature_of_Disability
   END AS DDS_Support_Arrangements_Nature_of_Disability
  FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString]
  WHERE DDS_Support_Arrangements_Nature_of_Disability IS NOT NULL
   AND Student_ID NOT IN (SELECT Student_ID FROM DST.DICT_Invalid_Students)
   ) IDS 
    LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DDS'
    LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
    LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
    LEFT JOIN DST.DICT_NatureOfdisability DNOD ON DNOD.Nature_of_Disability_UNique_list_of_value = LTRIM(IDS.DDS_Support_Arrangements_Nature_of_Disability)
    LEFT JOIN SFC.Get_ID_SupportArrangementMaster GISAM ON GISAM.Name =DNOD.DDSSFValue and GISAM.Object_Used_In__c = 'Support Profile' and GISAM.LSB_SUM_SupportArrangementCategory__c= 'Disability and Dyslexia Support'
    LEFT JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID
   WHERE GISP.ID IS NOT NULL

UNION ALL

-----------------------------------------------------------------------DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CAST(1 as BIT) AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(IDLM.DDS_DSA_DSA_Funded_By as varchar(255)) AS LSB_SPD_DSAFundedBy__c
		,CAST(IDD.DDS_DSA_DSA2_received as DATE) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(IDLM.DDS_DSA_Student_will_exceed_DSA_funding as varchar(255)) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2014_15_DSA_Application_Sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(IDS.DDS_DSA_DSA_Application_notes as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID 
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL and IDD.DDS_DSA_DSA2_received is not null

/*
------------------------------------------------------------2014-15 DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CASE UPPER(DDAS.DDS_DSA_2014_15_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2014_15_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2014_15_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2014_15_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2014-15DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2014-15DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2014_15_DSA_Application_Sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2014_15_DSA_Application_Sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2014_15_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2014_15_DSA_Application_Sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------2015-16 DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById		
		,CASE UPPER(DDAS.DDS_DSA_2015_16_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2015_16_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2015_16_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2015_16_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2015-16DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2015-16DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2015_16_DSA_Application_Sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2015_16_DSA_Application_Sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2015_16_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2015_16_DSA_Application_Sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL


UNION ALL
------------------------------------------------------------2016-17 DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CASE UPPER(DDAS.DDS_DSA_2016_17_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2016_17_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2016_17_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2016_17_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2016-17DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2016-17DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2016_17_DSA_Application_Sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2016_17_DSA_Application_Sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2016_17_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2016_17_DSA_Application_Sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------2017-18 DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CASE UPPER(DDAS.DDS_DSA_2017_18_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2017_18_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2017_18_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2017_18_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2017-18DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2017-18DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2017_18_DSA_Application_Sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2017_18_DSA_Application_Sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2017_18_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2017_18_DSA_Application_Sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------2018-19 DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CASE UPPER(DDAS.DDS_DSA_2018_19_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2018_19_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2018_19_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2018_19_DSA_Application_Sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2018-19DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2018-19DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2018_19_DSA_Application_Sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2018_19_DSA_Application_Sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2018_19_DSA_Application_Sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2018_19_DSA_Application_Sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------2019-20 DSA---------------------------------------------------------------------------------
SELECT
		--CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CASE UPPER(DDAS.DDS_DSA_2019_20_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2019_20_DSA_application_sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2019_20_DSA_application_sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2019_20_DSA_application_sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2019-20DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2019-20DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2019_20_DSA_applications_sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2019_20_DSA_application_sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2019_20_DSA_application_sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2019_20_DSA_applications_sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL

UNION ALL
------------------------------------------------------------2020-21 DSA---------------------------------------------------------------------------------
SELECT
		-- CAST(NULL as varchar(255)) AS Name
		 CAST(RT.Id as varchar(18)) AS RecordTypeId
		--,CAST(NULL as DATETIME) AS CreatedDate
		--,CAST(NULL as varchar(18)) AS CreatedById
		--,CAST(NULL as DATETIME) AS LastModifiedDate
		--,CAST(NULL as varchar(18)) AS LastModifiedById
		,CASE UPPER(DDAS.DDS_DSA_2020_21_DSA_Application_Sent)
           WHEN 'NO'  THEN CAST(0 as BIT)
	       WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	     END AS LSB_SPD_Active__c
		,CAST(GIDC.Id as varchar(18)) AS LSB_SPD_Contact__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2020_21_DSA_application_sent = 'TRUE'
				THEN IDLM.DDS_DSA_DSA_Funded_By 
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_DSAFundedBy__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2020_21_DSA_application_sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA2_received 
				ELSE NULL
				END 
				as DATE
			) AS LSB_SPD_DSA2Received__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_DutyListPriorityLevel__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_DutyListNotes__c
		,CAST(0 as BIT) AS LSB_SPD_DutyList__c
		,CAST(NULL as DATE) AS LSB_SPD_EndDate__c
		,CAST(NULL as DATE) AS LSB_SPD_StartDate__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2020_21_DSA_application_sent = 'TRUE'
				THEN IDLM.DDS_DSA_Student_will_exceed_DSA_funding
				ELSE NULL
				END 
				as varchar(255)
			) AS LSB_SPD_StudentWillExceedDSAFunding__c
		,CAST(NULL as varchar(18)) AS LSB_SPD_SupportMaster__c
		,NULL as Concerns
		,CAST('Disability and Dyslexia Support' as varchar(255)) AS LSB_SPD_SupportProfileCategory__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_SupportProfileDetailsName__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatFromOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToOthers__c
		,CAST(0 as BIT) AS LSB_SPD_ThreatToSelf__c
		,CAST(NULL as varchar(18)) AS SupportProfileDDS__c
		,CAST(GISP.Id as varchar(18)) AS SupportProfileDSA__c
		,CAST(NULL as varchar(18)) AS SupportProfileMHWB__c
		,CAST(NULL as varchar(255)) AS LSB_SPD_Notes__c
		,CAST(CONCAT(IDS.Student_ID,'.2020-21DSA.',ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY IDS.Student_ID))as varchar(255)) AS LSB_SPD_ExternalId__c
		,CAST(IDS.Student_ID + '.2020-21DSA' as varchar(255)) AS LSB_SPD_SourceSystemId__c
		,CAST('Maximiser' as varchar(255)) AS LSB_SPD_SourceSystem__c
		,CAST(NULL as varchar(MAX)) AS LSB_SPD_ThreatDetails__c
		,CAST(IDD.DDS_DSA_2020_21_DSA_application_sent AS DATE) AS LSB_SPD_YearsDSAApplicationSent__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2020_21_DSA_application_sent = 'TRUE'
				THEN IDS.DDS_DSA_DSA_Application_notes 
				ELSE NULL
				END
			 as varchar(255)) AS LSB_SPD_DSAApplicationNotes__c
		,CAST(
				CASE WHEN DDAS.DDS_DSA_2020_21_DSA_application_sent = 'TRUE'
				THEN IDD.DDS_DSA_DSA_Application_Received_by_DDS__date_ 
				ELSE NULL
				END
			  AS DATE) AS LSB_SPD_DSAApplicationReceivedByDDS__c
		,CAST(GISP.ID as varchar(18)) AS  SupportProfileMaster__c
FROM [INT].[SRC_Maximizer_PIVOT_IndividualDetailString] IDS
INNER JOIN [INT].SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON IDS.ID = IDD.ID AND IDD.DDS_DSA_2019_20_DSA_applications_sent IS NOT NULL
LEFT JOIN [INT].[SRC_Maximizer_PIVOT_IndividualDetailListMulti] IDLM ON IDS.ID = IDLM.ID
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_SPD_DSA'
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = IDS.Student_ID + '.SP'
LEFT JOIN  DST.DICT_DSA_APPLICATION_SENT DDAS ON IDS.ID = DDAS.ID
WHERE IDS.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
 AND GISP.ID IS NOT NULL
*/


GO


