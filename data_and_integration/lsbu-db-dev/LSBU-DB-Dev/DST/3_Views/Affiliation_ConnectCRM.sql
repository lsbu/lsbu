CREATE OR ALTER VIEW [DST].[Affiliation_ConnectCRM] AS
SELECT
	CAST(GIDC.ContactId as varchar(18)) AS hed__Contact__c
	,CAST(GIDA.Id as varchar(18)) AS hed__Account__c
	,CAST(IIF(GIDCT.LSB_CON_CurrentRole__c = 'Prospect', 1, 0) as bit) AS hed__Primary__c
	,CAST('Prospect' as varchar(255)) AS hed__Role__c
	,CAST(GIDC.CreatedDate as date) AS hed__StartDate__c -- to do
	,CAST('Current' as varchar(255)) AS hed__Status__c
	,CAST('Connect' as varchar(255)) AS LSB_AON_SourceSystem__c
	,CAST(CONCAT(SM.Connect_ID, '.', DSACS.DST_val, '.SA1') as varchar(90)) AS LSB_AON_SourceSystemID__c        
	,CAST(CONCAT(SM.Connect_ID, '.', DSACS.DST_val, '.SA1') as varchar(90)) AS LSB_AON_ExternalID__c
	,CAST('Primary' as varchar(255)) AS LSB_AON_SubjectAreaOfInterestType__c  --New Field
FROM SRC.Salesforce_Migration SM
LEFT JOIN DST.DICT_SubjectArea_Connect_to_SF DSACS ON SM.Subject_Area_1 = DSACS.SRC_val
LEFT JOIN SFC.Get_ID_Case GIDC ON CAST(SM.Connect_ID as varchar)+'.Connect' = GIDC.LSB_CAS_ExternalID__c
LEFT JOIN SFC.Get_ID_Account GIDA ON DSACS.DST_val = GIDA.LSB_ACC_ExternalID__c
JOIN SFC.Get_ID_Contact GIDCT ON GIDC.ContactId = GIDCT.Id
WHERE SM.Subject_Area_1 is not null
UNION
SELECT
	CAST(GIDC.ContactId as varchar(18)) AS hed__Contact__c
	,CAST(GIDA.Id as varchar(18)) AS hed__Account__c
	,CAST(0 as bit) AS hed__Primary__c
	,CAST('Prospect' as varchar(255)) AS hed__Role__c
	,CAST(GIDC.CreatedDate as date) AS hed__StartDate__c -- to do
	,CAST('Current' as varchar(255)) AS hed__Status__c
	,CAST('Connect' as varchar(255)) AS LSB_AON_SourceSystem__c
	,CAST(CONCAT(SM.Connect_ID, '.', DSACS.DST_val, '.SA2') as varchar(90)) AS LSB_AON_SourceSystemID__c        
	,CAST(CONCAT(SM.Connect_ID, '.', DSACS.DST_val, '.SA2') as varchar(90)) AS LSB_AON_ExternalID__c
	,CAST('Secondary' as varchar(255)) AS LSB_AON_SubjectAreaOfInterestType__c --New Field
FROM SRC.Salesforce_Migration SM
LEFT JOIN DST.DICT_SubjectArea_Connect_to_SF DSACS ON SM.Subject_Area_2 = DSACS.SRC_val
LEFT JOIN SFC.Get_ID_Case GIDC ON CAST(SM.Connect_ID as varchar)+'.Connect' = GIDC.LSB_CAS_ExternalID__c
LEFT JOIN SFC.Get_ID_Account GIDA ON DSACS.DST_val = GIDA.LSB_ACC_ExternalID__c
JOIN SFC.Get_ID_Contact GIDCT ON GIDC.ContactId = GIDCT.Id
WHERE SM.Subject_Area_2 is not null