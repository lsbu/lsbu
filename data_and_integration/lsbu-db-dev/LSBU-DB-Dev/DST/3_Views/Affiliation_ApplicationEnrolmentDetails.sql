CREATE OR ALTER VIEW [DST].[Affiliation_ApplicationEnrolmentDetails] AS
SELECT 
  CAST(COALESCE(QLAP.Id, QLAPD.Id, QLAPEI.Id) as varchar(18)) AS hed__Account__c, --Use this to Look up Academic Program record in the Account object and fetch the Salesforce Id
  CAST(QLOH.Id as varchar(18)) AS hed__Contact__c, --Use this to Look up Contact record in the Contact object and fetch the Salesforce Id
  CAST(NULL as varchar(max)) AS hed__Description__c,
  QLOHAD.QLCALC_AffiliationEndDate AS hed__EndDate__c,
  QLOHAD.QLCALC_AffiliationPrimaryFlag AS hed__Primary__c,
  CAST(QLOHAD.QLCALC_AffiliationRole as varchar(255)) AS hed__Role__c,
  case  when QLOHAD.QLCALC_AffiliationStartDate<'19000101' then null else QLOHAD.QLCALC_AffiliationStartDate end  AS hed__StartDate__c,
  CONVERT(varchar(255), QLOHAD.QLCALC_AffiliationStatus) AS hed__Status__c,
  CAST(QLOHAD.SrcSystem as varchar(255)) AS LSB_AON_SourceSystem__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_AON_SourceSystemID__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_AON_ExternalID__c,
  QL_StudentID
FROM [INT].[SRC_ApplicationEnrolmentDetails_Affiliation] AS QLOHAD
  INNER JOIN [SFC].Get_ID_Contact AS QLOH ON QLOHAD.[QL_StudentID] = QLOH.LSB_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAP ON QLOHAD.[QL_AcadProgNameId] = QLAP.LSB_ACC_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAPD ON QLOHAD.[QL_DepartmentId] = QLAPD.LSB_ACC_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAPEI ON QLOHAD.[QL_EducationInstitutionId] = QLAPEI.LSB_ACC_ExternalID__c
WHERE QLOHAD.LSB_ExternalID like '%A'
  AND
  ( 
    QLOHAD.ChangeStatus IN ('NEW','UPDATE')
    OR EXISTS (Select LSB_AON_ExternalID__c from DST.SF_Reject_Affiliation WHERE QLOHAD.LSB_ExternalID = LSB_AON_ExternalID__c)
	  OR (NOT EXISTS (SELECT * FROM [SFC].[Get_ID_Affiliation] GIA WHERE GIA.LSB_AON_ExternalID__c = QLOHAD.LSB_ExternalID) AND QLOHAD.ChangeStatus <> 'DELETE')
  )
UNION ALL
SELECT
  CAST(COALESCE(QLAP.Id, QLAPD.Id, QLAPEI.Id) as varchar(18)) AS hed__Account__c, --Use this to Look up Academic Program record in the Account object and fetch the Salesforce Id
  CAST(QLOH.Id as varchar(18)) AS hed__Contact__c, --Use this to Look up Contact record in the Contact object and fetch the Salesforce Id
  CAST(NULL as varchar(max)) AS hed__Description__c,
  QLOHAD.QLCALC_AffiliationEndDate AS hed__EndDate__c,
  QLOHAD.QLCALC_AffiliationPrimaryFlag AS hed__Primary__c,
  CAST(QLOHAD.QLCALC_AffiliationRole as varchar(255)) AS hed__Role__c,
  case  when QLOHAD.QLCALC_AffiliationStartDate<'19000101' then null else QLOHAD.QLCALC_AffiliationStartDate end  AS hed__StartDate__c,
  CONVERT(varchar(255), QLOHAD.QLCALC_AffiliationStatus) AS hed__Status__c,
  CAST(QLOHAD.SrcSystem as varchar(255)) AS LSB_AON_SourceSystem__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_AON_SourceSystemID__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_AON_ExternalID__c,
  QL_StudentID
FROM [INT].[SRC_ApplicationEnrolmentDetails_Affiliation] AS QLOHAD
  INNER JOIN [SFC].Get_ID_Contact AS QLOH ON QLOHAD.[QL_StudentID] = QLOH.LSB_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAP ON QLOHAD.[QL_AcadProgNameId] = QLAP.LSB_ACC_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAPD ON QLOHAD.[QL_DepartmentId] = QLAPD.LSB_ACC_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAPEI ON QLOHAD.[QL_EducationInstitutionId] = QLAPEI.LSB_ACC_ExternalID__c
WHERE QLOHAD.LSB_ExternalID like '%E'
  AND
    (QLOHAD.ChangeStatus IN ('NEW','UPDATE')
    OR EXISTS  (Select LSB_AON_ExternalID__c from DST.SF_Reject_Affiliation WHERE QLOHAD.LSB_ExternalID = LSB_AON_ExternalID__c)
	  OR (NOT EXISTS (SELECT * FROM [SFC].[Get_ID_Affiliation] GIA WHERE GIA.LSB_AON_ExternalID__c = QLOHAD.LSB_ExternalID) AND QLOHAD.ChangeStatus <> 'DELETE'))
    
GO
