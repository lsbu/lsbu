CREATE OR ALTER VIEW [DST].[User_AppliedSciencesPTCD_Update] AS
SELECT DISTINCT
	   u.Id, 
	   c.Id As ContactId 
FROM [INT].[SRC_AppliedSciencesPTCD_Staff] as ST
JOIN sfc.Get_ID_Contact c ON c.LSB_ExternalID__c = st.LSB_ExternalID
JOIN sfc.Get_ID_User u ON u.LSB_UER_ExternalID__c = st.LSB_ExternalID
WHERE u.ContactId IS NULL