CREATE OR ALTER VIEW [DST].[Deduplicate_Contact_Birthdate] AS
--First condition 2. First Name, Last Name & Date of Birth
select C.Id,COH.LSB_SourceSystem__c,COH.LSB_ExternalID__c, C.LSB_CON_SourceSystem__c,
C.FirstName,C.LastName,C.Email,C.Birthdate
from SFC.Get_ID_Contact C
JOIN DST.Contact_Student as COH on COH.FirstName = C.FirstName AND COH.LastName = C.LastName AND COH.Birthdate = C.Birthdate
WHERE ISNULL(C.LSB_CON_SourceSystem__c,'NULL') <> 'QL'
