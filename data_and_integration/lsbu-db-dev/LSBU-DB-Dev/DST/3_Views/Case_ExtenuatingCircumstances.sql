
CREATE OR ALTER view [DST].[Case_ExtenuatingCircumstances] as

with pre_sql as (
select Academic_Period,
sfcContact.Id as ContactId, 
School,
Division,
Course,
Title,
Session,
Level,
Module,
Semester,
Assessment,
Student_ID,
Forename,
Surname,
Year,
Stage,
Ext_Circ,
Mitigate_Note,
Internal_Email,
External_Email,
file_name,
Module1,
Module2,
Mitigate_Note_Date, 
LSB_ExternalId,
ModuleEnrolment_LSB_ExternalId,
ModuleComponent_LSB_ExternalId,
ModuleSubComponent_LSB_ExternalId,
MED.Id as LSB_CAS_RelatedModule__c,
MCED.Id as ModuleComponent_SFID,
MCSED.Id as ModuleSubComponent_SFID,
RT.Id as RecordTypeId,
SFID.Id as Id
from 
[STG].[tExtenuatingCircumstances] stg
join [SFC].[Get_ID_Contact] sfcContact on stg.Student_ID=sfcContact.LSB_ExternalID__c
JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_ECClaim'
LEFT JOIN [SFC].[Get_ID_CourseEnrollment] MED on MED.LSB_CCN_ExternalID__c=stg.ModuleEnrolment_LSB_ExternalId
LEFT JOIN [SFC].[Get_ID_CourseEnrollment] MCED on MCED.LSB_CCN_ExternalID__c=stg.ModuleComponent_LSB_ExternalId 
LEFT JOIN [SFC].[Get_ID_CourseEnrollment] MCSED on MCSED.LSB_CCN_ExternalID__c=stg.ModuleSubComponent_LSB_ExternalId
LEFT JOIN [SFC].[Get_ID_Case] SFID on SFID.LSB_CAS_ExternalID__c=stg.LSB_ExternalId  
where Ext_Circ NOT IN ('AMFM','APAP','REJECTED2','REJ_DNM2','SUPPORTED2','PENDING2'))


select
CAST(LSB_CAS_RelatedModule__c as varchar(18)) as LSB_CAS_RelatedModule__c,
CAST(CASE WHEN ModuleComponent_SFID is null THEN ModuleSubComponent_SFID ELSE ModuleComponent_SFID END as varchar(18)) as LSB_CAS_RelatedAssessment__c,
ContactId as ContactId, 
CAST(CASE WHEN Ext_Circ in ('REJECTED','REJ_DDSLSR','REJ_DNM','REJ_DNME','REJ_DNMS','REJ_REV','REJ_RIE') THEN Ext_Circ
	ELSE null end as varchar(255)) as LSB_CAS_RejectionReason__c,
CAST(CASE WHEN Ext_Circ in ('SUPPAPPEAL','SUPPORTED','SUPP_BRV','SUPP_COV19','SUPP_DIS','SUPP_ERR','SUPP_FIN','SUPP_GEND','SUPP_HLTH','SUPP_HSE',
							'SUPP_MAT','SUPP_REV','SUPP_SCK','SUPP_TRANS','SUPP_WRK','PENDING','PEND_ETF','PEND_ETF_I','PEND_ETF_M',
							'PEND_ETF_O','PEND_ETF_P','PEND_ETF_R','PEND_ETF_S','PEND_ETF_U','UNOTED') THEN Ext_Circ
	WHEN Ext_Circ = 'UNOTED' THEN 'PENDING'
	ELSE null end as varchar(255)) as LSB_CAS_ApprovalPendingReason__c, 
CAST(CASE WHEN LSB_CAS_RelatedModule__c is null and (ModuleComponent_SFID is not null or ModuleSubComponent_SFID is not null) THEN
	CONCAT('WAS PREVIOUSLY ASSESSMENT: ',  Assessment, ' PLEASE IGNORE RELATED ASSESSMENT FIELD ', Mitigate_Note)
	WHEN LSB_CAS_RelatedModule__c is not null and (ModuleComponent_SFID is null and ModuleSubComponent_SFID is null) THEN 
	CONCAT('WAS PREVIOUSLY MODULE: ',  Module, ', MODULE SESSION: ', Module1, ', ASSESSMENT: ', Assessment ,' PLEASE IGNORE RELATED MODULE, MODULE SESSION AND RELATED ASSESSMENT FIELDS ', Mitigate_Note)
	ELSE Mitigate_Note END as varchar(8000)) as Description,
CAST(CASE WHEN Mitigate_Note_Date is null THEN 
			CASE WHEN Academic_Period = '20/21' THEN '2021-07-05' 
			WHEN Academic_Period = '19/20' THEN '2020-07-03'
			WHEN Academic_Period = '18/19' THEN '2019-07-04' 
			WHEN Academic_Period = '17/18' THEN '2018-07-06' 
			WHEN Academic_Period = '16/17' THEN '2017-07-03' 
			ELSE '2016-07-04' END 
	ELSE Mitigate_Note_Date END as date) AS ClosedDate, 
CAST('Extenuating circumstances claim' as varchar(255)) as LSB_CAS_RequestType__c,
CAST(CONCAT('Extenuating circumstances claim',' - ',Assessment,' - ',
	CASE WHEN (Module LIKE '%R1%' OR Module LIKE '%R2%' OR Module LIKE '%R3%') THEN 'Resit' 
	ELSE 'Main' END) AS VARCHAR(255)) as Subject,
CAST(CASE WHEN Ext_Circ in ('SUPPAPPEAL','SUPPORTED','SUPP_BRV','SUPP_COV19','SUPP_DIS','SUPP_ERR','SUPP_FIN','SUPP_GEND','SUPP_HLTH','SUPP_HSE',
						              	'SUPP_MAT','SUPP_REV','SUPP_SCK','SUPP_TRANS','SUPP_WRK') THEN 'Request Supported'
	        WHEN Ext_Circ in ('REJECTED','REJ_DDSLSR','REJ_DNM','REJ_DNME','REJ_DNMS','REJ_REV','REJ_RIE') THEN 'Request Rejected' 
          WHEN Ext_Circ in ('PENDING','PEND_ETF','PEND_ETF_I','PEND_ETF_M','PEND_ETF_O','PEND_ETF_P','PEND_ETF_R','PEND_ETF_S','PEND_ETF_U','UNOTED') THEN 'Request Pending' 
	ELSE 'Closed' END AS VARCHAR(255)) AS Status,
CAST(CASE WHEN Mitigate_Note_Date is null THEN 
			CASE WHEN Academic_Period = '20/21' THEN '2021-07-05' 
			WHEN Academic_Period = '19/20' THEN '2020-07-03'
			WHEN Academic_Period = '18/19' THEN '2019-07-04' 
			WHEN Academic_Period = '17/18' THEN '2018-07-06' 
			WHEN Academic_Period = '16/17' THEN '2017-07-03' 
			ELSE '2016-07-04' END 
	ELSE Mitigate_Note_Date END as date) AS CreatedDate,
CAST(stg.LSB_ExternalId as varchar(90)) AS LSB_CAS_ExternalID__c,
CAST('QL' as varchar(2)) AS LSB_CAS_SourceSystem__c,
CAST(stg.LSB_ExternalId as varchar(90)) AS LSB_CAS_SourceSystemID__c,
CAST(RecordTypeId as varchar(18)) as RecordTypeId,
CAST(stg.Id as varchar(18)) as Id
from pre_sql stg
GO


