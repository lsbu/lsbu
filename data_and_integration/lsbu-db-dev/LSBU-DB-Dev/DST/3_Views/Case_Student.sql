CREATE OR ALTER VIEW [DST].[Case_Student] AS
SELECT
	 CAST(NULL as varchar(18)) as AccountId
	,CAST(NULL as varchar(18)) as AssetId
	,CAST('QL' as varchar(255)) as Origin
	,CAST(OWN.ID as varchar(18)) as OwnerId
	,CAST(NULL as varchar(255)) as Reason
	,CAST((select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord') as varchar(18)) as RecordTypeId
	,CAST(NULL as varchar(1300))as LSB_CAS_CaseResponded__c
	,CAST(NULL as varchar(18)) as SourceId
	,CAST(NULL as varchar(255)) as hed__Category__c
	,CAST(NULL as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(NULL as varchar(max)) as LSB_CAS_ClosureComment__c
	,CAST(NULL as varchar(255)) as LSB_CAS_FirstName__c
	,CAST(NULL as varchar(255)) as LSB_CAS_LastName__c
	,CAST(GIDC.Id as varchar(18)) as ContactId
	,CAST(NULL as date) as LSB_CAS_DateOfBirth__c
	,CAST(NULL as datetime) as ClosedDate
	,CAST(NULL as varchar(18)) as EntitlementId
	,CAST(NULL as datetime) as SlaExitDate
	,CAST(NULL as datetime) as SlaStartDate
	,CAST(NULL as varchar(4000))as Comments
	,CAST(NULL as varchar(255)) as LSB_CAS_KnowledgeArticleLink__c
	,CAST(NULL as varchar(40)) as Language
	,CAST(NULL as varchar(255)) as LSB_CAS_LevelOfIntendedStudy__c
	,CAST(NULL as varchar(255)) as hed__Location__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeVia__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeAbout__c
	,CAST(SAA.LSB_ExternalID+'_Case' as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST(NULL as varchar(max)) as LSB_CAS_Response__c
	,CAST(SAA.SrcSystem as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(CONCAT(SAA.LSB_ExternalID, '_Case') as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST(NULL as varchar(max)) as Description
	,CAST(NULL as datetime) as hed__Occurrence_Date__c
	,CAST(NULL as varchar(1300))as LSB_CAS_Over48Hours__c
	,CAST(NULL as varchar(18)) as ParentId
	,CAST(NULL as varchar(255)) as Priority
	,CAST(NULL as varchar(255)) as LSB_CAS_SLAViolation__c
	,CAST('Active' as varchar(255)) as Status
	,CAST(CONCAT(SAA.QL_FirstName , SAA.QL_Surname, ' - ', SAA.QL_StudentID) as varchar(255)) as Subject
	,CAST(NULL as varchar(255)) as LSB_CAS_SubjectAreaOfInterest__c
	,CAST(NULL as varchar(255)) as LSB_CAS_Topic__c
	,CAST(NULL as varchar(255)) as Type
	,CAST(NULL as varchar(255)) as LSB_CAS_UkEuOrInternationalStudent__c
	,CAST(NULL as varchar(80)) as SuppliedCompany
	,CAST(NULL as varchar(80)) as SuppliedEmail
	,CAST(NULL as varchar(255)) as LSB_CAS_YearOfIntendedStudy__c
	,CAST(SAA.QL_StudentID as varchar(255)) as sfal__StudentID__c
	,CAST(
			CASE WHEN GIDC.LSB_CON_CurrentRole__c LIKE 'Student%' then 'Student'
			 WHEN GIDC.LSB_CON_CurrentRole__c = 'Graduate' then 'Student'
			ELSE GIDC.LSB_CON_CurrentRole__c
			
			END as varchar(255)) as LSB_CAS_ContactRole__c   -- New Field Map
FROM		[INT].[SRC_SALAdvisee] SAA
  LEFT JOIN	[SFC].[Get_ID_Contact] GIDC on SAA.QL_StudentID = GIDC.LSB_ExternalID__c
  INNER JOIN sfc.get_ID_USER OWN on  OWN.username like 'dataintegrationuser@lsbu.com%'
WHERE (SAA.ChangeStatus IN ('NEW', 'UPDATED')
  OR EXISTS (SELECT RC.LSB_CAS_ExternalID__c FROM DST.SF_Reject_Case RC WHERE SAA.LSB_ExternalID = RC.LSB_CAS_ExternalID__c)
  OR (NOT EXISTS (SELECT * FROM [SFC].[Get_ID_Case] GIDCA WHERE GIDCA.LSB_CAS_ExternalID__c = SAA.LSB_ExternalID+'_Case') AND SAA.ChangeStatus <> 'DELETE'))
GO