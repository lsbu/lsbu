CREATE OR ALTER VIEW [DST].[SupportProfile_MAXSupportProfile] AS

/*
 This CTE was created because of duplicates in [INT.SRC_Maximizer_PIVOT_IndividualDetailString] table 
 For each Student_Id we should get only one row. In case of multiple rows existance - we choice the one with the largest number of felled fields
*/
WITH IndividualDetails_cte AS (

 SELECT
 qr_2.* 
FROM (
SELECT 
 qr_1.*,
 ROW_NUMBER() OVER(PARTITION BY qr_1.Student_ID ORDER BY qr_1.data_cnt DESC) rnk
FROM (
 SELECT 
  *,
  IIF(ARCHIVED_FIELDS_Assessing_LEA_Name is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Duration_of_temporary_disability is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Ed_Psych_Invoice_number is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Email_Address_2 is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_First_language is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Lockers_Locker_notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Note_Taker_initial_hours is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Note_Taker_s_ is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Dyslexia_Tutor_date_Allocated is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Hours is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Library_Support_Worker is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Mentor_date_Allocated is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_MENTOR_NOTES is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_OLD_dont_use_Pre_Entry___notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Other_Support is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Tutor_Support_Notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Recommendations_or_Comments is NULL, 0, 1)+
  IIF(Area_Of_Study_Course_Code_ is NULL, 0, 1)+
  IIF(Attend_Mode is NULL, 0, 1)+
  IIF(Confidential_Information is NULL, 0, 1)+
  IIF(Course_In_Full is NULL, 0, 1)+
  IIF(Covid_19_Fields_Coronavirus_questions_Other_coronavirus_detail__please_specify_ is NULL, 0, 1)+
  IIF(Covid_19_Fields_Covid_plan_created_notes is NULL, 0, 1)+
  IIF(Covid_19_Fields_Further_information_Anything_else_to_know_before_we_book_appointment is NULL, 0, 1)+
  IIF(Covid_19_Fields_Risk_information_Other_risk__please_specify_ is NULL, 0, 1)+
  IIF(Covid_19_Fields_Travel_Other_travel_detail__please_specify_ is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_Email is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_name is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_Phone is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Relationship_to_Student is NULL, 0, 1)+
  IIF(DDS_DSA_CLASS_Needs_Assessment_time is NULL, 0, 1)+
  IIF(DDS_DSA_CRN is NULL, 0, 1)+
  IIF(DDS_DSA_DSA_Application_notes is NULL, 0, 1)+
  IIF(DDS_DSA_NHS_Bursary_No is NULL, 0, 1)+
  IIF(DDS_Lockers_Lockers_Locker_Number is NULL, 0, 1)+
  IIF(DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_hours_agreed is NULL, 0, 1)+
  IIF(DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_total_cost is NULL, 0, 1)+
  IIF(DDS_PRE_ENTRY_Enrolment_Notes is NULL, 0, 1)+
  IIF(DDS_PRE_ENTRY_Notes is NULL, 0, 1)+
  IIF(DDS_Screening_and_Ed_Psych_Assessment_Feedback_appointment_time is NULL, 0, 1)+
  IIF(DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Assignment_Arrangement_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Examination_Arrangements_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_For_Information_notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Nature_of_Disability is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Presentation_Arrangements_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_QL_Disability_Fields_Disability_Allowance is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_QL_Disability_Fields_Disability_Type is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Teaching_Arrangements_Notes is NULL, 0, 1)+
  IIF(MH__WB_Counselling_Referrals_Corenet_ID is NULL, 0, 1)+
  IIF(MH__WB_MH__WB_Current_Risk_EP_Waiting_List_notes is NULL, 0, 1)+
  IIF(MH__WB_MHWB_Archive_Name_of_person_referring is NULL, 0, 1)+
  IIF(MH__WB_MHWB_Archive_Who_advised_student_of_MHWB_Service is NULL, 0, 1)+
  IIF(Preferred_name__if_different_ is NULL, 0, 1)+
  IIF(QL_Records_Management_Academic_Year is NULL, 0, 1)+
  IIF(QL_Records_Management_Applicant_Stage_Code is NULL, 0, 1)+
  IIF(QL_Records_Management_Applicant_Stage_Date_Achieved is NULL, 0, 1)+
  IIF(QL_Records_Management_Application_Academic_Year is NULL, 0, 1)+
  IIF(QL_Records_Management_Country_Of_Residence is NULL, 0, 1)+
  IIF(QL_Records_Management_Ethnic_Origin is NULL, 0, 1)+
  IIF(QL_Records_Management_Gender is NULL, 0, 1)+
  IIF(QL_Records_Management_Geo_Location is NULL, 0, 1)+
  IIF(QL_Records_Management_Nationality is NULL, 0, 1)+
  IIF(QL_Records_Management_Session_Code is NULL, 0, 1)+
  IIF(QL_Records_Management_Stage_Code is NULL, 0, 1)+
  IIF(School_Code is NULL, 0, 1) as data_cnt
 FROM INT.SRC_Maximizer_PIVOT_IndividualDetailString
 WHERE Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students]))  qr_1
 ) qr_2  WHERE qr_2.rnk= 1
)




SELECT
--CAST(NULL AS VARCHAR(18)) AS CreatedById
--,CAST(NULL AS DATETIME) AS CreatedDate
CAST(gisp.Id AS VARCHAR(18)) AS Id
--,CAST(NULL AS BIT) AS IsDeleted
--,CAST(NULL AS DATETIME) AS LastActivityDate
--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
--,CAST(NULL AS DATETIME) AS LastModifiedDate
--,CAST(NULL AS DATETIME) AS LastReferencedDate
--,CAST(NULL AS DATETIME) AS LastViewedDate
--,CAST(NULL AS DECIMAL(18, 0)) AS LSB_SUE_ActualExpenditure__c
 ,CASE 
  WHEN midl.DDS_PRE_ENTRY_Allocated_to_DA IN ('Not applicable', 'Passed to Reception to make appointment', 'Student contacted for more information')
THEN CAST(midl.DDS_PRE_ENTRY_Allocated_to_DA AS VARCHAR(4099)) ELSE NULL END AS LSB_SUE_AllocatedToDA__c
 ,(SELECT ContactId FROM sfc.Get_ID_User CNT WHERE LEFT(CNT.USERNAME, CHARINDEX('@',CNT.USERNAME)-1) = DICT_ToDa.User_name) AS LSB_SUE_PrimaryPreEntryAdvisor__c

--,CAST(NULL AS VARCHAR(1300)) AS LSB_SUE_CareLeavers__c
,CONVERT(DATETIME, midd.DDS_DSA_CLASS_Needs_Assessment_Date, 121) +
CASE
    WHEN TRY_CONVERT(TIME, REPLACE(REPLACE(mids.DDS_DSA_CLASS_Needs_Assessment_time, ' ', ''), '.', ':')) IS NULL THEN mids.DDS_DSA_CLASS_Needs_Assessment_time
    ELSE CONVERT(DATETIME, TRY_CONVERT(TIME, REPLACE(REPLACE(mids.DDS_DSA_CLASS_Needs_Assessment_time, ' ', ''), '.', ':')))
  END AS LSB_SUE_CLASSNeedsAssessmentDate__c
,CAST(gic.Id AS VARCHAR(18)) AS LSB_SUE_Contact__c
,CAST(mids.MH__WB_Counselling_Referrals_Corenet_ID AS VARCHAR(255)) AS LSB_SUE_CoreNetID__c
--,CAST(NULL AS VARCHAR(1300)) AS LSB_SUE_CRN__c
--,CAST(NULL AS BIT) AS LSB_SUE_Current__c
,CONVERT(DATE, midd.DDS_DSA_DSA_Application_Received_by_DDS__date_, 121) AS LSB_SUE_DataProtectionFormReceived__c
,CONVERT(DATE, midd.MH__WB_Counselling_Referrals_Date_of_most_recent_referral, 121) AS LSB_SUE_DateOfMostRecentReferral__c
--,CAST(NULL AS VARCHAR(255)) AS LSB_SUE_DDSSupport__c
,CAST(REPLACE(REPLACE(REPLACE(midlm.MH__WB_Diagnosed_MH_condition, N'Bi-polar disorder (inc. cyclothymia)' , N'Bi-Polar Disorder'), N'Psychotic illness (inc. schizophrenia, and schizo-affective disorder)', N'Psychotic Illness'), N'Anxiety disorders (including stress)', N'Anxiety Disorders') AS VARCHAR(4099)) AS LSB_SUE_DiagnosedMHCondition__c
--,CAST(NULL AS VARCHAR(1300)) AS LSB_SUE_DisabilityAllowance__c
--,CAST(NULL AS VARCHAR(1300)) AS LSB_SUE_DisabilityType__c
--,CAST(NULL AS VARCHAR(1300)) AS LSB_SUE_Disability__c
,CAST(midlm.DDS_PRE_ENTRY_DP_Form_signed AS VARCHAR(255)) AS LSB_SUE_DPFormSigned__c
--,CONVERT(DATE, midd.DDS_DSA_DSA2_received, 121) AS LSB_SUE_DSA2Received__c
--,CAST(mids.DDS_DSA_DSA_Application_notes AS VARCHAR(255)) AS LSB_SUE_DSAApplicationNotes__c
--,CONVERT(DATE, midd.DDS_DSA_DSA_Application_Received_by_DDS__date_, 121) AS LSB_SUE_DSAApplicationReceivedByDDS__c
--,CAST(midlm.DDS_DSA_DSA_Funded_By AS VARCHAR(255)) AS LSB_SUE_DSAFundedBy__c
,CAST(mids.DDS_PRE_ENTRY_Enrolment_Notes AS VARCHAR(90)) AS LSB_SUE_EnrolmentNotes__c
,CAST(CASE midlm.DDS_PRE_ENTRY_Enrolment_Support_Required WHEN 'Yes' THEN 1 WHEN 'No' THEN 0 ELSE 0 END AS BIT) AS LSB_SUE_EnrolmentSupportRequired__c
--,CAST(NULL AS VARCHAR(4099)) AS LSB_SUE_EPAssessmentResult__c
,CAST(mids.MH__WB_MH__WB_Current_Risk_EP_Waiting_List_notes AS VARCHAR(255)) AS LSB_SUE_EPWaitingListNotes__c
,CONVERT(DATE, midd.DDS_EP_Waiting_List_EP_Waiting_List, 121) AS LSB_SUE_EPWaitingList__c
,CONVERT(DATE, midd.DDS_PRE_ENTRY_PRE_ENTRY_Evidence_Received_date, 121) AS LSB_SUE_EvidenceReceivedDate__c
--,CAST(NULL AS DECIMAL(18, 0)) AS LSB_SUE_ExpectedExpenditure__c
,CAST(mids.Student_ID + '.SP' AS VARCHAR(90)) AS LSB_SUE_ExternalID__c
--,CAST(NULL AS VARCHAR(4099)) AS LSB_SUE_ImpactOfDisability__c
,CAST(CASE midlm.DDS_PRE_ENTRY_Med_ev_received WHEN 'Yes' THEN 1 WHEN 'No' THEN 0 ELSE 0 END AS BIT) AS LSB_SUE_MedEvReceived__c
--,CAST(NULL AS BIT) AS LSB_SUE_MHWBActive__c
,CONVERT(DATE, midd.DDS_PRE_ENTRY_Pre_Entry_MORE_evidence_requested_date, 121) AS LSB_SUE_MoreEvidenceRequestedDate__c
--,CAST(NULL AS VARCHAR(4099)) AS LSB_SUE_NatureOfDisability__c
--,CAST(NULL AS VARCHAR(MAX)) AS LSB_SUE_NextSteps__c
--,CAST(NULL AS VARCHAR(1300)) AS LSB_SUE_NHSBursaryNo__c
,CASE midlm.MH__WB_Counselling_Referrals_Number_of_referrals
   WHEN '1st Referral' THEN CAST(1 AS REAL)
   WHEN '2nd Referral' THEN CAST(2 AS REAL)
   WHEN '3rd Referral' THEN CAST(3 AS REAL) 
   WHEN '1st Referral;2nd Referral' THEN CAST(2 AS REAL) ELSE CAST(NULL AS REAL) 
  END AS LSB_SUE_NumberOfReferrals__c
,CAST(mids.DDS_PRE_ENTRY_Notes AS VARCHAR(MAX)) AS LSB_SUE_PreEntryNotes__c
,CAST(midlm.DDS_PRE_ENTRY_Pre_entry_status AS VARCHAR(4099)) AS LSB_SUE_PreEntryStatus__c
--,CAST(NULL AS VARCHAR(MAX)) AS LSB_SUE_Preferences__c
,CAST(ISNULL((SELECT Id FROM SFC.Get_ID_User GID WHERE SUBSTRING(GID.Username,1,(CHARINDEX('@',GID.Username)-1)) = DSP1.DestinationValue),'') AS VARCHAR(18)) AS LSB_SUE_PrimaryAdvisor__c
,CAST(ISNULL((SELECT Id FROM SFC.Get_ID_User GID WHERE SUBSTRING(GID.Username,1,(CHARINDEX('@',GID.Username)-1)) = DSP2.user_name),'') AS VARCHAR(18)) as LSB_SUE_PrimaryDDSAdvisor__c -- New
,CAST(CASE midlm.DDS_PRE_ENTRY_SAF_agreed WHEN 'Yes' THEN 1 WHEN 'No' THEN 0 ELSE 0 END AS BIT) AS LSB_SUE_SAFAgreed__c
,CAST(mids.Student_ID + '.SP' AS VARCHAR(90)) AS LSB_SUE_SourceSystemId__c
,CAST(CASE WHEN gisp.Id IS NULL THEN 'Maximiser' ELSE gisp.LSB_SUE_SourceSystem__c END AS VARCHAR(90)) AS LSB_SUE_SourceSystem__c
,CONVERT(nvarchar(255), CASE WHEN gisp.Id IS NULL THEN 'New' ELSE gisp.LSB_SUE_Status__c END) AS LSB_SUE_Status__c
--,CAST(midl.MH__WB_In_Halls AS VARCHAR(1300)) AS LSB_SUE_StudentInHalls__c
--,CAST(midlm.DDS_DSA_Student_will_exceed_DSA_funding AS VARCHAR(255)) AS LSB_SUE_StudentWillExceedDSAFunding__c
,CAST(CASE midlm.DDS_PRE_ENTRY_Support_declined WHEN 'Yes' THEN 1 WHEN 'No' THEN 0 ELSE 0 END AS BIT) AS LSB_SUE_SupportDeclined__c
,CAST(midlm.MH__WB_Support_intervention AS VARCHAR(4099)) AS LSB_SUE_SupportIntervention__c
,CAST(REPLACE(REPLACE(midlm.MH__WB_Support_network__student_discusses_difficulties_with_, 'Religious leader(Imam/pastor/priest/swami/rabbi etc)', 'Religious Leader'), 'Online support (i.e. forums)', 'Online Support') AS VARCHAR(4099)) AS LSB_SUE_SupportNetwork__c
,CAST(CASE midlm.DDS_DSA_Updated_QL__DSA_field_ WHEN 'Yes' THEN 1 WHEN 'No' THEN 0 ELSE 0 END AS BIT) AS LSB_SUE_UpdatedQLDSAField__c
--,CAST(NULL AS VARCHAR(4099)) AS LSB_SUE_YearsDSAApplicationSent__c
--,midlm.KEY_INFORMATION AS [NOT MAPPED YET]
,CONVERT(nvarchar(80), gic.Name + ' - ' + mids.Student_ID + ' - Support Profile') AS Name
--,CAST(NULL AS VARCHAR(18)) AS OwnerId
--,CAST(NULL AS DATETIME) AS SystemModstamp
FROM Maximizer.src.Individual mi
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailDate midd ON midd.Id = mi.Id
LEFT JOIN IndividualDetails_cte mids ON mids.Id = mi.Id
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailList midl ON midl.Id = mi.Id
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti midlm ON midlm.Id = mi.Id
LEFT JOIN SFC.Get_ID_Contact AS gic ON gic.LSB_ExternalID__c= mids.Student_ID
LEFT JOIN SFC.Get_ID_SupportProfile AS gisp ON gisp.LSB_SUE_ExternalID__c = mids.Student_ID + '.SP'
LEFT JOIN DST.DICT_MHWB_Adviser DSP1 ON DSP1.SourceValue = REPLACE(REPLACE(REPLACE(REPLACE(midlm.MH__WB_MHWB_Adviser, 'Stephen Anderson', ''), 'Tayla Vella', ''), 'Makeba Garraway', ''), 'Ben Walford', '')
LEFT JOIN DST.DICT_Support_Profile DSP2 ON DSP2.Adviser = midl.DDS_Support_Arrangements_DAs_initials
LEFT JOIN DST.DICT_Support_Profile_AllocatedToDA DICT_ToDa ON DICT_ToDa.AllocatedToDA_Person = midl.DDS_PRE_ENTRY_Allocated_to_DA
WHERE gisp.Id IS NOT NULL