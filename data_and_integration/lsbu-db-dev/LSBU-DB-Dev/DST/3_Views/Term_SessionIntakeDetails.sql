CREATE OR ALTER   VIEW [DST].[Term_SessionIntakeDetails] AS
SELECT
	CAST(QLSIDT.QL_TermName as varchar(80)) [Name],
	CAST(GIDAAP.Id as varchar(18)) AS [hed__Account__c],
	TRY_CONVERT(date, QLSIDT.QL_SessionEndDate, 103) AS [hed__End_Date__c],
	TRY_CONVERT(date, QLSIDT.QL_SessionStartDate, 103) AS [hed__Start_Date__c],
	CAST(NULL as real) AS [hed__Grading_Period_Sequence__c],
	CAST(NULL as real) AS [hed__Instructional_Days__c],
	CAST(GIDT.Id as varchar(18)) AS [hed__Parent_Term__c],
	CAST('Session Intake' as varchar(255)) AS [hed__Type__c],
	CAST(QLSIDT.LSB_ExternalID as varchar(90)) AS [LSB_TRM_ExternalID__c],
	CAST(QLSIDT.LSB_ExternalID as varchar(90)) AS [LSB_TRM_SourceSystemID__c],
	CAST(QLSIDT.SrcSystem as varchar(255)) AS [LSB_TRM_SourceSystem__c]
FROM [INT].[SRC_SessionIntakeDetails_Term] AS QLSIDT
LEFT JOIN SFC.Get_ID_Term GIDT ON QLSIDT.QL_AcademicPeriod = GIDT.LSB_TRM_ExternalID__c
INNER JOIN SFC.Get_ID_Account GIDAAP ON QLSIDT.QL_AcadProgNameId = GIDAAP.LSB_ACC_ExternalID__c
WHERE QLSIDT.QL_TermType = 'SessionIntake' AND
	(QLSIDT.ChangeStatus IN ('NEW','UPDATE') 
	OR EXISTS (Select R.LSB_TRM_ExternalID__c from DST.SF_Reject_Term R WHERE QLSIDT.LSB_ExternalID = R.LSB_TRM_ExternalID__c)
	OR (NOT EXISTS (SELECT * FROM [SFC].Get_ID_Term GIT WHERE GIT.LSB_TRM_ExternalID__c = QLSIDT.LSB_ExternalID) AND QLSIDT.ChangeStatus <> 'DELETE'))
	AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLSIDT.LSB_ExternalID
				AND	  VALID.ObjectName = 'SessionIntakeDetails'
				AND	  VALID.SrcSystem = 'QL')
GO
