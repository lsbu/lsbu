CREATE OR ALTER VIEW [DST].[Account_EducationalInstitution] AS
SELECT 
  CAST(QLEI.QL_EducationInstitutionName as varchar(255)) as Name,
  CAST((select Id from sfc.RecordType WHERE DeveloperName = 'Educational_Institution') as varchar(18)) as RecordTypeId,
  CAST(null as varchar(18)) as ParentId,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLEI.QL_Add1, ''),' ' + NULLIF(QLEI.QL_Add2, ''),' ' + NULLIF(QLEI.QL_Add3, '')),1,1,'') as varchar(255)) as BillingStreet,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLEI.QL_Add4, ''),' ' + NULLIF(QLEI.QL_Add5, '')),1,1,'') as varchar(40)) as BillingCity,
  CAST(QLEI.QL_Postcode as varchar(20)) as BillingPostalCode,
  CAST(QLEI.QL_Country as varchar(80)) as BillingCountry,
  CAST(QLEI.QL_Phone as varchar(40)) as Phone,
  CAST(null as varchar(40)) as Fax,
  CAST(null as varchar(40))as AccountNumber,
  CAST(QLEI.QL_Website as varchar(255)) as Website,
  CAST(null as varchar(20)) as Sic,
  CAST(null as varchar(255)) as Industry,
  CAST(null as decimal(18, 0)) as AnnualRevenue,
  CAST(null as int) as NumberOfEmployees,
  CAST(null as varchar(20)) as TickerSymbol,
  CAST(QLEI.QL_Description as varchar(max)) as Description,
  CAST(null as varchar(255)) as Rating,
  CAST(null as varchar(80)) as Site,
  CAST(null as varchar(255)) as AccountSource,
  CAST(null as varchar(80)) as SicDesc,
  CAST(null as varchar(18)) as hed__Primary_Contact__c,
  CAST(null as varchar(18)) as hed__Current_Address__c,
  CAST(null as varchar(80)) as hed__Shipping_County__c,
  CAST(QLEI.QL_EducationInstitutionId as varchar(50)) as hed__School_Code__c,
  CAST(QLEI.LSB_ExternalID as varchar(90)) as LSB_ACC_ExternalID__c,
  CAST(QLEI.QL_EducationInstitutionId as varchar(90)) as LSB_ACC_SourceSystemID__c,
  CAST(QLEI.SrcSystem as varchar(255)) as LSB_ACC_SourceSystem__c
FROM [INT].[SRC_EducationalInstitution] as QLEI
WHERE (
QLEI.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select LSB_ACC_ExternalID__c from DST.SF_Reject_Account_EducationInstitution WHERE QLEI.LSB_ExternalID = LSB_ACC_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLEI.LSB_ExternalID
				AND	  VALID.ObjectName = 'EducationalInstitution'
				AND	  VALID.SrcSystem = 'QL')
GO