CREATE OR ALTER VIEW [DST].[Covid19_MAXCovid19] AS
SELECT
	--CAST(NULL AS VARCHAR(18)) AS CreatedById
	--,CAST(NULL AS DATETIME) AS CreatedDate
	--,CAST(NULL AS VARCHAR(18)) AS Id
	--,CAST(NULL AS bit) AS IsDeleted
	--,CAST(NULL AS date) AS LastActivityDate
	--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
	--,CAST(NULL AS datetime) AS LastModifiedDate
	CAST(REPLACE(REPLACE(REPLACE(IDLM.Covid_19_Fields_Adjustments_and_interventions_Adjustments_proposed
		, 'Student not able to wear face covering on campus for health reasons', 'Student not able to wear face covering on campus for health reasons wherever possible student to present online')
		, 'Wherever possible student to present online', 'Student not able to wear face covering on campus for health reasons wherever possible student to present online')
		, 'Student not able to wear face covering on campus for health reasons Student not able to wear face covering on campus for health reasons wherever possible student to present online'
  ,'Student not able to wear face covering on campus for health reasons wherever possible student to present online'  ) AS VARCHAR(4099)) AS LSB_COV_AdjustmentsProposed__c
	,CAST(IDS.Covid_19_Fields_Further_information_Anything_else_to_know_before_we_book_appointment AS VARCHAR(255)) AS LSB_COV_AnythingElseToKnowBeforeAppoint__c
	,CAST(CASE
			WHEN IDLM.Covid_19_Fields_Adjustments_and_interventions_Unable_to_attend___attendance_not_required_by_course_Home_or_EU_student IS NOT NULL THEN 0
			WHEN IDLM.Covid_19_Fields_Adjustments_and_interventions_Unable_to_attend___attendance_required_by_course_Home_EU_and_Tier_4_students IS NOT NULL THEN 1
			ELSE 0
		  END AS bit) AS LSB_COV_AttendanceRequiredByCourse__c
	,CAST(IDLM.Covid_19_Fields_Circulated_to AS VARCHAR(4099)) AS LSB_COV_CirculatedTo__c
	,CAST(GIDC.Id AS VARCHAR(18)) AS LSB_COV_Contact__c
	,CAST(IDS.Covid_19_Fields_Covid_plan_created_notes AS VARCHAR(255)) AS LSB_COV_CovidPlanCreatedNotes__c
	,CAST(IDLM.Covid_19_Fields_Covid_plan_created AS VARCHAR(4099)) AS LSB_COV_CovidPlanCreated__c
	,CAST(IDD.Covid_19_Fields_Covid_plan_sent AS date) AS LSB_COV_CovidPlanSent__c
	,CAST(IDD.Covid_19_Fields_Date_completed AS date) AS LSB_COV_DateCompleted__c
	,CAST(NULL AS date) AS LSB_COV_DateUpdated__c --Missing field in source
	,CAST(CONCAT(IDS.Student_ID, '.', CAST(ROW_NUMBER() OVER(PARTITION BY IDS.Student_ID ORDER BY I.CreatedDate) as varchar)) AS VARCHAR(90)) AS LSB_COV_ExternalId__c --Need mapping for this
	,CAST(IDLM.Covid_19_Fields_Coronavirus_questions_Has_student_had_coronavirus AS VARCHAR(4099)) AS LSB_COV_HasStudentHadCoronavirus__c
	,CAST(IDLM.Covid_19_Fields_Adjustments_and_interventions_Health__emotional_support__disability_team_ AS VARCHAR(4099)) AS LSB_COV_HealthEmotionalSupportDDS__c
	,CAST(IDLM.Covid_19_Fields_Risk_information_High_risk_detail AS VARCHAR(4099)) AS LSB_COV_HighRiskDetail__c
	,CAST(IDLM.Covid_19_Fields_Travel_How_will_student_be_travelling_to_campus AS VARCHAR(4099)) AS LSB_COV_HowDoesStudentTravelToCampus__c
	,CAST(IDLM.Covid_19_Fields_Accommodation_Accommodation_Will_student_be_a_Residential_Life_Ambassador__RLA_ AS VARCHAR(255)) AS LSB_COV_IsAResidentialLifeAmbassadorRLA__c
	,CAST(IDLM.Covid_19_Fields_International_student_Is_student_an_international_student_studying_on_a_T4_visa AS VARCHAR(4099)) AS LSB_COV_IsTheStudentStudyingOnAT4_visa__c
	,CAST(IDLM.Covid_19_Fields_Risk_information_Moderate_risk_detail AS VARCHAR(4099)) AS LSB_COV_ModerateRiskDetail__c
	,CAST(IDLM.Covid_19_Fields_Accommodation_Accommodation_How_many_bedrooms_will_there_be_in_students_flat AS VARCHAR(4099)) AS LSB_COV_NumberOfBedroomsInStudentsFlat__c
	,CAST(IDS.Covid_19_Fields_Coronavirus_questions_Other_coronavirus_detail__please_specify_ AS VARCHAR(255)) AS LSB_COV_OtherCoronavirusDetail__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_COV_OtherInternationalStudentDetail__c --missing column in source 
	,CAST(IDS.Covid_19_Fields_Risk_information_Other_risk__please_specify_ AS VARCHAR(255)) AS LSB_COV_OtherRiskPleaseSpecify__c
	,CAST(IDS.Covid_19_Fields_Travel_Other_travel_detail__please_specify_ AS VARCHAR(255)) AS LSB_COV_OtherTravelDetailPleaseSpecify__c
	,CAST(IDLM.Covid_19_Fields_Referred_to AS VARCHAR(4099)) AS LSB_COV_ReferredTo__c
	,CAST(IDLM.Covid_19_Fields_Risk_information_Risk_category AS VARCHAR(4099)) AS LSB_COV_RiskCategory__c
	,CAST(IDLM.Covid_19_Fields_Existing_DDS_support_Does_student_already_have_SAF_agreed_with_DDS AS VARCHAR(4099)) AS LSB_COV_SAFAlreadyAgreedWithDDS__c
	,CAST(CONCAT(IDS.Student_ID, '.Maximiser') AS VARCHAR(90)) AS LSB_COV_SourceSystemId__c
	,CAST('Maximiser' AS VARCHAR(90)) AS LSB_COV_SourceSystem__c
	,CAST(IDLM.Covid_19_Fields_Adjustments_and_interventions_Student_accommodation_adjustments AS VARCHAR(4099)) AS LSB_COV_StudentAccommodationAdjustments__c
	--,CAST(NULL AS VARCHAR(1300)) AS LSB_COV_StudentFeeBasis__c
	,CAST(IDLM.Covid_19_Fields_Accommodation_Accommodation_Which_halls_of_residence_will_student_be_living_in AS VARCHAR(4099)) AS LSB_COV_StudentsPlannedHallOfResidence__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_COV_UnableToAttendHomeEUAndTier4__c
	,CAST(REPLACE(IDLM.Covid_19_Fields_Adjustments_and_interventions_Unable_to_attend___attendance_not_required_by_course_Home_or_EU_student, 'For Semester 1, student can access teaching and learning online', 'For Semester 1, student can access teaching and learning online') AS VARCHAR(255)) AS LSB_COV_UnableToAttendHomeOrEUStudent__c
	,CAST(REPLACE(IDLM.Covid_19_Fields_Adjustments_and_interventions_Unable_to_attend___attendance_required_by_course_Home_EU_and_Tier_4_students, 'Discussion with course team (and Int team if Tier 4) if all options exhausted', 'Remote study Semester 1 after leaving UK, return to campus once risk is reduced') AS VARCHAR(255)) AS LSB_COV_UnableToAttendStudentOnTier4Visa__c
	,CAST(IDLM.Covid_19_Fields_Accommodation_Accommodation_Does_student_plan_to_live_in_LSBU_student_accomm AS VARCHAR(4099)) AS LSB_COV_WillLiveInLSBUStudentAccom__c
	,CAST(CONCAT(IDS.Student_ID, ' - Maximizer Covid 19 Case') AS VARCHAR(80)) AS Name
	--,CAST(NULL AS VARCHAR(18)) AS OwnerId
	--,CAST(NULL AS datetime) AS SystemModstamp
FROM Maximizer.src.Individual I
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON I.Id = IDD.Id
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON I.Id = IDLM.Id
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON I.Id = IDS.Id
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c