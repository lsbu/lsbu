CREATE OR ALTER VIEW [DST].[Relationship_StudentTeachingStaff] AS
SELECT 
  CAST(GIDC.Id as varchar(18)) AS hed__Contact__c
  ,CAST(RLC.ID as varchar(90)) AS hed__RelatedContact__c
  ,CAST(CASE 
         WHEN ChangeStatus = 'DELETE' THEN 'Former' 
         ELSE 'Current' 
        END as varchar(255)) AS hed__Status__c
  ,CAST('Course Tutor' as varchar(255)) AS hed__Type__c  -- Changed
  ,CAST('CMIS' as varchar(255)) AS LSB_RTP_SourceSystem__c
  ,CAST(STR.LSB_ExternalID as varchar(90)) AS LSB_RTP_SourceSystemID__c 
  ,CAST(STR.LSB_ExternalID as varchar(90)) AS LSB_RTP_ExternalId__c
  ,CAST(COF.Id as varchar(18)) AS LSB_RTP_Module__c
  ,(select id from sfc.RecordType	where name like 'Relationship'	and SobjectType='hed__Relationship__c') as RecordTypeId
FROM [INT].[SRC_StudentTeachingStaff_Relationship] as STR
 JOIN SFC.Get_ID_Contact GIDC ON STR.CMIS_StudentID = GIDC.LSB_ExternalID__c
 JOIN SFC.Get_ID_Contact RLC ON STR.TeachingStaff_LSB_ExternalID = RLC.LSB_ExternalID__c
 JOIN SFC.Get_ID_CourseOffering COF ON COF.LSB_COF_ExternalID__c = STR.ModuleOffering_LSB_ExternalID
WHERE (STR.ChangeStatus IN ('NEW','UPDATE')
 OR
 EXISTS (Select R.LSB_RTP_ExternalId__c from [DST].[SF_Reject_Relationship_StudentTeachingStaff]  R WHERE STR.LSB_ExternalID = R.LSB_RTP_ExternalId__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
			WHERE VALID.ValidationResultFlag = 'ERROR'
			AND   VALID.LSB_ExternalID = STR.LSB_ExternalID
			AND	  VALID.ObjectName = 'StudentTeachingStaff_Relationship'
			AND	  VALID.SrcSystem = 'CMIS')