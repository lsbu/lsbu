CREATE OR ALTER VIEW [DST].[CaseTeamMember_AllStaff_Ins] AS
SELECT
 	CAST(null as varchar(18)) AS Id,
	CAST(GIU.Id as varchar(18)) AS MemberId,
	CAST(GIDCA.Id as varchar(18)) AS ParentId,
	CAST(CTR.Id as varchar(18)) AS TeamRoleId,
	CAST(null as varchar(18)) AS TeamTemplateId,
	CAST(null as varchar(18)) AS TeamTemplateMemberId
FROM [INT].[SRC_AllStaff_CaseTeamMember] CTM
  JOIN [SFC].[Get_ID_Contact] GIDC ON CTM.StudentId = GIDC.LSB_ExternalID__c
  JOIN [SFC].[Get_ID_User] GIU ON CTM.FederationIdentifier = GIU.FederationIdentifier
  JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
  JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name =
  CASE
	WHEN CTM.PersonalTutor = '1' AND CTM.ModuleLeader is null and CTM.CourseTutor is null THEN  'Personal Tutor'
	WHEN CTM.PersonalTutor is null AND CTM.ModuleLeader is null and CTM.CourseTutor = '1' THEN  'Course Tutor'
	WHEN CTM.PersonalTutor is null AND CTM.ModuleLeader = '1' and CTM.CourseTutor is null THEN  'Module Leader'
	WHEN CTM.PersonalTutor = '1' AND CTM.ModuleLeader = '1' and CTM.CourseTutor is null THEN  'Module Leader / Personal Tutor'
	WHEN CTM.PersonalTutor is null AND CTM.ModuleLeader = '1' and CTM.CourseTutor = '1' THEN  'Course Tutor / Module Leader'
	WHEN CTM.PersonalTutor = '1' AND CTM.ModuleLeader is null and CTM.CourseTutor = '1' THEN  'Course Tutor / Personal Tutor'
	WHEN CTM.PersonalTutor = '1' AND CTM.ModuleLeader = '1' and CTM.CourseTutor = '1' THEN  'Course Tutor / Module Leader / Personal Tutor'

  END
WHERE CTM.ChangeStatus IN ('NEW','NO CHANGE')
AND
NOT EXISTS (SELECT 1 from [SFC].[Get_ID_CaseTeamMember] GICTM WHERE GIU.Id = GICTM.MemberId and GIDCA.Id =  GICTM.ParentId )