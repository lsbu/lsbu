CREATE OR ALTER VIEW DST.LSBU_AppliedSciences_PT_CD_MissingStudentList AS
Select 
	QL_StudentID as StudentId,
	QL_AOSPeriod as SessionCode,
	QL_EnrollmentStatus as EnrolmentStatus,
	A.QL_AcadProgNameId  as CourseCode,
	B.QL_AcadProgName AS CourseName
FROM [STG].[tEnrolementDetails_LatestRecords] A , [STG].[tAcademicProgram] B
where A.QL_AcadProgNameId = B.QL_AcadProgNameId
	AND B.QL_EducationInstitutionId = 'SASC'
	AND A.IsApplicant = 'FALSE'
	AND NOT EXISTS (SELECT COL1 FROM [SRC].[SRC_ROW_TMP] AppliedSciencesPTCD WHERE AppliedSciencesPTCD.ObjectName = 'AppliedSciencesPTCD' AND COL1 = A.QL_StudentId)
GO