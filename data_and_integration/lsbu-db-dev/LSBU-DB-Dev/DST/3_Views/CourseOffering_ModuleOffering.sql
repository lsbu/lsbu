CREATE OR ALTER VIEW [DST].[CourseOffering_ModuleOffering] AS
SELECT DISTINCT
   CAST(NULL AS REAL) AS hed__Capacity__c
  ,CAST(GIDC.Id AS VARCHAR(18)) AS hed__Course__c
  ,CONVERT(DATE, QLMO.QL_ModuleEndDate, 103) AS hed__End_Date__c
  ,CAST(NULL AS VARCHAR(18)) AS hed__Facility__c
  ,CAST(NULL AS VARCHAR(18)) AS hed__Faculty__c 
  ,CAST(NULL AS VARCHAR(255)) AS hed__Section_ID__c
  ,CONVERT(DATE, QLMO.QL_ModuleStartDate, 103) AS hed__Start_Date__c
  ,CAST(GIDT.Id AS VARCHAR(18)) AS hed__Term__c
  ,CAST(NULL AS VARCHAR(18)) AS hed__Time_Block__c
  ,CAST(QLMO.QL_ModuleCredit AS REAL) AS LSB_COF_ModuleCredit__c
  ,CAST(QLMO.QL_ModuleInstance AS VARCHAR(90)) AS LSB_COF_ModuleInstance__c
  ,CAST(NULL AS VARCHAR(10)) AS LSB_COF_SessionCode__c
  ,CAST(NULL AS VARCHAR(255)) AS LSB_COF_SessionDescription__c
  ,CAST(QLMO.LSB_ExternalID AS VARCHAR(90)) AS LSB_COF_ExternalID__c
  ,CAST(QLMO.LSB_ExternalID AS VARCHAR(90)) AS LSB_COF_SourceSystemID__c
  ,CAST('QL' AS VARCHAR(255)) AS LSB_COF_SourceSystem__c
  ,CAST(QLMO.QL_ModuleInstanceName AS VARCHAR(80)) AS Name
FROM [INT].[SRC_ModuleOffering] QLMO
 JOIN SFC.Get_ID_Course GIDC ON QLMO.QL_ModuleId = GIDC.LSB_CSE_ExternalID__c
 LEFT JOIN SFC.Get_ID_Term GIDT ON QLMO.QL_AcademicPeriod = GIDT.LSB_TRM_ExternalID__c
WHERE (
  QLMO.ChangeStatus IN ('NEW','UPDATE')
  OR EXISTS (Select R.LSB_COF_ExternalID__c from DST.SF_Reject_CourseOffering_ModuleOffering R WHERE QLMO.LSB_ExternalID = R.LSB_COF_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLMO.LSB_ExternalID
				AND	  VALID.ObjectName = 'ModuleOffering'
				AND	  VALID.SrcSystem = 'QL')