CREATE OR ALTER VIEW [DST].[Account_AcademicProgram] AS
SELECT 
  CAST(QLAP.QL_AcadProgName as varchar(255)) as Name,
  CAST((select Id from sfc.RecordType WHERE DeveloperName = 'Academic_Program') as varchar(18)) as RecordTypeId,
  CAST(QLDE.id AS varchar(18)) as ParentId,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLAP.QL_Add1, ''),' ' + NULLIF(QLAP.QL_Add2, ''),' ' + NULLIF(QLAP.QL_Add3, '')),1,1,'') as varchar(255)) as BillingStreet,
  CAST(STUFF(CONCAT(' ' + NULLIF(QLAP.QL_Add4, ''),' ' + NULLIF(QLAP.QL_Add5, '')),1,1,'') as varchar(40))as BillingCity,
  CAST(QLAP.QL_Postcode as varchar(20)) as BillingPostalCode,
  CAST(QLAP.QL_Country as varchar(80))as BillingCountry,
  CAST(QLAP.QL_Phone as varchar(40))as Phone,
  CAST(null as varchar(40)) as Fax,
  CAST(NULL as varchar(40))as AccountNumber,
  CAST(QLAP.QL_Website as varchar(255)) as Website,
  CAST(null as varchar(20)) as Sic,
  CAST(null as varchar(255)) as Industry,
  CAST(null as decimal(18, 0)) as AnnualRevenue,
  CAST(null as int) as NumberOfEmployees,
  CAST(null as varchar(20)) as TickerSymbol,
  CAST(QLAP.QL_Description as varchar(max))as Description,
  CAST(null as varchar(255)) as Rating,
  CAST(null as varchar(80)) as Site,
  CAST(null as varchar(255)) as AccountSource,
  CAST(null as varchar(80)) as SicDesc,
  CAST(null as varchar(18)) as hed__Primary_Contact__c,
  CAST(null as varchar(18)) as hed__Current_Address__c,
  CAST(null as varchar(80)) as hed__Billing_County__c,
  CAST(null as varchar(80)) as hed__Shipping_County__c,
  CAST(QLAP.QL_AcadProgNameId as varchar(90)) as LSB_ACC_CourseCode__c,
  CAST ((SELECT max(QL_UCASCourseCode) 
         FROM (Select distinct QL_AcadProgNameId,QL_UCASCourseCode 
               from INT.SRC_ApplicationDetails_Application) QLCC  
         WHERE QLCC.QL_AcadProgNameId = QLAP.QL_AcadProgNameId) as varchar(90))as LSB_ACC_UCASCourseCode__c,
  CAST(QLAP.LSB_ExternalID as varchar(90)) as LSB_ACC_ExternalID__c,
  CAST(QLAP.QL_AcadProgNameId as varchar(90)) as LSB_ACC_SourceSystemID__c,
  CAST(QLAP.SrcSystem as varchar(255)) as LSB_ACC_SourceSystem__c,
  CAST(QL_CourseAdministrator as varchar(255)) LSB_ACC_CourseAdministrator__c,
  CAST(QL_CourseDirector  as varchar(255)) LSB_ACC_CourseDirector__c,
  CAST(QL_TypeofCourse  as varchar(255)) LSB_ACC_TypeofCourse__c
FROM [INT].[SRC_AcademicProgram] as QLAP
  JOIN [SFC].Get_ID_Account QLDE ON QLDE.LSB_ACC_ExternalID__c = QLAP.QL_DepartmentId
--JOIN (Select distinct QL_AcadProgNameId,QL_UCASCourseCode from INT.SRC_OfferHolderApplicationDetails) as QLCC  ON QLAP.QL_AcadProgNameId = QLCC.QL_AcadProgNameId
WHERE 
( 
QLAP.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select LSB_ACC_ExternalID__c from DST.SF_Reject_Account_AcademicProgram WHERE QLAP.LSB_ExternalID = LSB_ACC_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLAP.LSB_ExternalID
				AND	  VALID.ObjectName = 'AcademicProgram'
				AND	  VALID.SrcSystem = 'QL')
GO