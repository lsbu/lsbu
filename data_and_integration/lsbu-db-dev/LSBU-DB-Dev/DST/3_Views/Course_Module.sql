CREATE OR ALTER VIEW [DST].[Course_Module] AS
SELECT DISTINCT

   CAST(MO.QL_ModuleId AS VARCHAR(18)) AS hed__Course_ID__c
  ,CAST(MO.QL_ModuleName AS VARCHAR(80)) AS Name
  ,CAST(MO.QL_ModuleLevel AS VARCHAR(20)) AS LSB_CSE_ModuleLevel__c
  ,CAST(MO.QL_ModuleStatus AS VARCHAR(255)) AS LSB_CSE_ModuleStatus__c
  ,CAST(QLAP.Id as varchar(18)) AS hed__Account__c
  ,CAST(MO.LSB_ExternalID as varchar(90)) AS LSB_CSE_SourceSystemID__c
  ,CAST(MO.LSB_ExternalID as varchar(90)) as LSB_CSE_ExternalID__c
  ,CAST(MO.SrcSystem as varchar(255)) AS LSB_CSE_SourceSystem__c 

FROM [INT].[SRC_Module] MO
 LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAP ON MO.QL_DepartmentId = QLAP.LSB_ACC_ExternalID__c
WHERE (
  MO.ChangeStatus IN ('NEW','UPDATE')
  OR EXISTS (Select 1 from DST.SF_Reject_Course_Module R WHERE MO.LSB_ExternalID = R.LSB_CSE_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = MO.LSB_ExternalID
				AND	  VALID.ObjectName = 'Module'
				AND	  VALID.SrcSystem = 'QL')