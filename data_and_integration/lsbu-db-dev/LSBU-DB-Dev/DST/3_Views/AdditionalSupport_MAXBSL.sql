CREATE OR ALTER   VIEW [DST].[AdditionalSupport_MAXBSL] AS
SELECT
	-- CAST(NULL AS VARCHAR(18)) AS CreatedById
	--,CAST(NULL AS DATETIME) AS CreatedDate
	--,CAST(NULL AS VARCHAR(18)) AS Id
	--,CAST(NULL AS BIT) AS IsDeleted
	--,CAST(NULL AS DATE) AS LastActivityDate
	--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
	--,CAST(NULL AS DATETIME) AS LastModifiedDate
	--,CAST(NULL AS DATETIME) AS LastReferencedDate
	--,CAST(NULL AS DATETIME) AS LastViewedDate
	 CAST(NULL AS REAL) AS LSB_ART_BSLHoursAgreedLSBUFundedNMH__c -- Field does not exists in Maximizer
	,CAST(NULL AS REAL) AS LSB_ART_BSLHours__c -- Field does not exists in Maximizer 
	,CASE UPPER(IDLM.DDS_LSBU_funded_NMH_BSL_Interpreter_BSL_Interpreter_Recommended)
      WHEN 'NO'  THEN CAST(0 as BIT)
	    WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	 END AS LSB_ART_BSLInterpreterRecommendedLSBU__c
    ,CASE UPPER(IDLM.DDS_DSA_BSL_Interpreter_BSL_Interpreter_Recommended)
      WHEN 'NO'  THEN CAST(0 as BIT)
	    WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
	 END AS LSB_ART_BSLInterpreterRecommended__c -- Field cannot be NULL
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_BSLSupplierLSBUFunded__c -- Field does not exists in Maximizer 
	,CAST(IDLM.DDS_DSA_BSL_Interpreter_BSL_Supplier AS VARCHAR(255)) AS LSB_ART_BSLSupplier__c
	--,CAST(NULL AS DECIMAL) AS LSB_ART_BSLTotalCostLSBUFundedNMH__c -- Field does not exists in Maximizer -- Field cannot be NULL
	,CAST(GIDC.Id AS VARCHAR(18)) AS LSB_ART_Contact__c
	,CAST(NULL AS DATETIME) AS LSB_ART_DateOfEPAssessment__c
	,CAST(NULL AS DATETIME) AS LSB_ART_DateOfSpLDScreening__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_DyslexiaTuitionType__c
	,CAST(NULL AS DATE) AS LSB_ART_DyslexiaTutorAllocated__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_DyslexiaTutorRecommended__c
	,CAST(NULL AS DATE) AS LSB_ART_EdPsychReportReceivedDate__c
	,CAST(NULL AS DATE) AS LSB_ART_EmailConvertedToFullSent__c
	,CAST(NULL AS DATE) AS LSB_ART_EmailMentoringToFullSent__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPAssessmentResult__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPAssessor__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPCost__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPFeedbackAppointmentAdviser__c
	,CAST(NULL AS DATE) AS LSB_ART_EPFeedbackAppointmentDate__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPRoom__c
	,CAST(CONCAT(IDS.Student_ID,'.BSL.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY I.Created)) AS VARCHAR(90)) AS LSB_ART_ExternalId__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_FeedbackLocation__c
	--,CAST(NULL AS BIT) AS LSB_ART_LockerAgreementSigned__c
	,CAST(NULL AS DATE) AS LSB_ART_LockerAssignedDate__c
	,CAST(NULL AS DATE) AS LSB_ART_LockerLoanEndDate__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_LockerNumber__c
	,CAST(NULL AS DATE) AS LSB_ART_MentorAllocated__c
	,CAST(NULL AS REAL) AS LSB_ART_MentorHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentoringSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentoringType__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_MentorInitialValue__c
	--,CAST(NULL AS BIT) AS LSB_ART_MentorRecommendedLSBUFunded__c 
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorSupportTotalHours__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_MentorTotalCost__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_Mentor__c
	--,CAST(NULL AS BIT) AS LSB_ART_NoteTakerRecommendedLSBUFunded__c 
	--,CAST(NULL AS VARCHAR(255)) AS LSB_ART_NoteTakerRecommended__c  
	,CAST(NULL AS REAL) AS LSB_ART_NoteTakingHoursAgreedLSBUFunded__c
	,CAST(NULL AS REAL) AS LSB_ART_NoteTakingHours__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_NoteTakingSupplierLSBUFundedNMH__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_NoteTakingTotalCostLSBUFundedNMH__c
	,CAST(CONCAT(IDS.Student_ID,'.BSL.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY I.Created)) AS VARCHAR(90)) AS LSB_ART_SourceSystemId__c
	,CAST('Maximiser' AS VARCHAR(90)) AS LSB_ART_SourceSystem__c
	--,CAST(NULL AS BIT) AS LSB_ART_SpecialistTutorRecommended__c 
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_SpecialistTutorSupplier__c
	,CAST(NULL AS REAL) AS LSB_ART_SpecialistTutorSupportHours__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_SpLDScreeningOutcome__c
	,CAST(NULL AS DATE) AS LSB_ART_TotalInterimHoursUsedRequest__c
	,CAST(NULL AS REAL) AS LSB_ART_TotalInterimHoursUsed__c
	,CAST(NULL AS REAL) AS LSB_ART_TotalInterimMentoringHoursUsed__c
	,CAST(NULL AS REAL) AS LSB_ART_TutorHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_TutorInitialValue__c
	--,CAST(NULL AS BIT) AS LSB_ART_TutorRecommendedLSBUFundedNMH__c 
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupplierLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupportTotalHours__c
	--,CAST(NULL AS DECIMAL) AS LSB_ART_TutorTotalCostLSBUFundedNMH__c
	--,CAST(NULL AS VARCHAR(80)) AS Name
	--,CAST(NULL AS VARCHAR(18)) AS OwnerId
	,CAST((SELECT id FROM sfc.RecordType WHERE DeveloperName = 'LSB_ART_BSLInformation') AS VARCHAR(18)) AS RecordTypeId
	--,CAST(NULL AS DATETIME) AS SystemModstamp

 FROM Maximizer.src.Individual I
  JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON I.Id = IDS.Id
  JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
  JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON I.Id = IDLM.Id
 WHERE (
  IDLM.DDS_DSA_BSL_Interpreter_BSL_Interpreter_Recommended IS NOT NULL OR
  IDLM.DDS_DSA_BSL_Interpreter_BSL_Supplier IS NOT NULL OR
  IDLM.DDS_LSBU_funded_NMH_BSL_Interpreter_BSL_Interpreter_Recommended IS NOT NULL)
GO


