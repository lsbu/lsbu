CREATE OR ALTER VIEW [DST].[ParentApplications] AS
SELECT 
	Id, 
	LSB_APP_ExternalId__c
FROM
(SELECT 
	Id,
	LSB_APP_ExternalId__c,
	ROW_NUMBER() OVER (PARTITION BY 
	SUBSTRING(LSB_APP_ExternalID__c,0,
	CHARINDEX('.',LSB_APP_ExternalID__c,
	CHARINDEX('.',LSB_APP_ExternalID__c,
	CHARINDEX('.',LSB_APP_ExternalID__c)+1)+1)) 
	ORDER BY 
	SUBSTRING(LSB_APP_ExternalID__c,
	CHARINDEX('.',LSB_APP_ExternalID__c,
	CHARINDEX('.',LSB_APP_ExternalID__c,
	CHARINDEX('.',LSB_APP_ExternalID__c)+1)+1)+1,6) DESC) as rnk
FROM SFC.Get_ID_Application AS QLAPP 
WHERE QLAPP.hed__Application_Status__c = 'ADEF') as t1
where rnk = 1