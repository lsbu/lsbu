CREATE OR ALTER VIEW [DST].[Contact_Student] AS

SELECT DISTINCT
--null as Id
--null as IsDeleted
--null as MasterRecordId
CAST(null as varchar(18)) as AccountId,
CAST(CASE
	WHEN QLOH.QL_Surname IN ('-','*','.') THEN QL_FirstName
	WHEN QLOH.QL_Surname IS NULL THEN QL_FirstName
	ELSE QLOH.QL_Surname 
END as varchar(80)) as LastName,
CAST(CASE
	WHEN QLOH.QL_Surname IN ('-','*','.') THEN NULL
	WHEN QLOH.QL_FirstName in ('-','*','.') THEN NULL
	ELSE QLOH.QL_FirstName 
END as varchar(40)) as FirstName,
--CAST(null as varchar(40)) as Salutation,
--CAST(null as varchar(121)) as Name,
--CAST(null as varchar(255)) as OtherStreet,
--CAST(null as varchar(40)) as OtherCity,
--CAST(null as varchar(80)) as OtherState,
--CAST(null as varchar(20)) as OtherPostalCode,
--CAST(null as varchar(80)) as OtherCountry,
--CAST(null as decimal(18, 0)) as OtherLatitude,
--CAST(null as decimal(18, 0)) as OtherLongitude,
--CAST(null as varchar(40)) as OtherGeocodeAccuracy,
--CAST(null as varchar(400)) as OtherAddress,
--STUFF(CONCAT(' ' + NULLIF(QLOH.QL_Add1, ''),' ' + NULLIF(QLOH.QL_Add2, ''),' ' + NULLIF(QLOH.QL_Add3, '')),1,1,'') as MailingStreet,
CAST(null as varchar(255)) as MailingStreet,
--SUBSTRING(QLOH.QL_Add4,1,40) as MailingCity,
CAST(null as varchar(40)) as MailingCity,
--CAST(null as varchar(80)) as MailingState,
--QLOH.QL_Postcode as MailingPostalCode,
CAST(null as varchar(20)) as MailingPostalCode,
--DAC.[MailingCountry] as MailingCountry,
CAST(null as varchar(80)) as MailingCountry, 
--CAST(null as decimal(18, 0)) as MailingLatitude,
--CAST(null as decimal(18, 0)) as MailingLongitude,
--CAST(null as varchar(40)) as MailingGeocodeAccuracy,
--CAST(null as varchar(400)) as MailingAddress,
CAST(null as varchar(40)) as Phone,
CAST(null as varchar(40)) as Fax,
CAST(QLOH.QL_MobileNo as varchar(40)) as MobilePhone,
CAST(QLOH.QL_Phone as varchar(40)) as HomePhone,
CAST(null as varchar(40)) as OtherPhone,
CAST(null as varchar(40)) as AssistantPhone,
CAST(null as varchar(18)) as ReportsToId,
--QLOH.QL_PersonalEmail as Email, --PROD Email !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
case when db_name()='diprod' 
then CAST(REPLACE(REPLACE(REPLACE(QLOH.QL_PersonalEmail, char(160), ''), 'co uk', 'co.uk'), ' ', '') as varchar(80)) --Email -------------

else CAST(REPLACE(REPLACE(REPLACE(CFG.fx_HashEmail(QLOH.QL_PersonalEmail, QLOH.QL_StudentID), char(160), ''), 'co uk', 'co.uk'), ' ', '') as varchar(80)) --HashEmail -------------
end as Email,
--null as Email,
CAST(QLOH.QL_Title as varchar(128)) as Title,
CAST(null as varchar(80)) as Department, ---QLOH.QL_DepartmentName as Department, - lookup to Department ?
CAST(null as varchar(40)) as AssistantName,
CAST(null as varchar(255)) as LeadSource,
TRY_CONVERT(date, QLOH.QL_Birthdate, 103 ) as Birthdate,
CAST(null as varchar(max)) as Description, --QLOH.QL_Description source column don't exists
CAST(OWN.ID as varchar(18)) as OwnerId,--QLOH.QL_Origin as OwnerId, --no applicable 
--CAST(null as bit) as HasOptedOutOfEmail,
--CAST(null as bit) as HasOptedOutOfFax,
--CAST(null as bit) as DoNotCall,
--CAST(null as datetime) as CreatedDate,
--CAST(null as varchar(18)) as CreatedById,
--CAST(null as datetime) as LastModifiedDate,
--CAST(null as varchar(18)) as LastModifiedById,
--CAST(null as datetime) as SystemModstamp,
--CAST(null as date) as LastActivityDate,
CAST(null as datetime) as LastCURequestDate,
CAST(null as datetime) as LastCUUpdateDate,
CAST(null as datetime) as LastViewedDate,
--CAST(null as datetime) as LastReferencedDate,
--CAST(null as varchar(255)) as EmailBouncedReason,
--CAST(null as datetime) as EmailBouncedDate,
--CAST(null as bit) as IsEmailBounced,
--CAST(null as varchar(255)) as PhotoUrl,
CAST(null as varchar(20)) as Jigsaw,
--CAST(null as varchar(20)) as JigsawContactId,
--CAST(null as varchar(18)) as IndividualId,
CAST(null as varchar(18)) as Primary_Academic_Program__c,
CAST(null as varchar(18)) as Primary_Department__c,
CAST(null as varchar(18)) as Primary_Educational_Institution__c,
CAST(null as varchar(18)) as Primary_Sports_Organization__c,
case when db_name()='diprod'  
then QLOH.QL_PersonalEmail
else CAST(CFG.fx_HashEmail(QLOH.QL_PersonalEmail, QLOH.QL_StudentID) as varchar(80)) end as hed__AlternateEmail__c,
CAST(ISNULL(DCN.[Citizenship], QLOH.QL_Nationality) as varchar(255)) as hed__Citizenship__c, --QLOH.QL_Nationality as hed__Citizenship__c, --DICTIONARY need 
CAST(ISNULL(DCD.[hed__Country_of_Origin__c], QLOH.QL_Domicile) as varchar(255)) as hed__Country_of_Origin__c, --QLOH.QL_Domicile as hed__Country_of_Origin__c, --DICTIONARY need 
CAST(null as varchar(18)) as hed__Current_Address__c,
--CAST(null as bit) as hed__Deceased__c,
--CAST(null as bit) as hed__Do_Not_Contact__c,
CAST(QLOH.QL_Origin as varchar(255)) as hed__Ethnicity__c,
--CAST(null as bit) as hed__Exclude_from_Household_Formal_Greeting__c,
--CAST(null as bit) as hed__Exclude_from_Household_Informal_Greeting__c,
--CAST(null as bit) as hed__Exclude_from_Household_Name__c,
--CAST(null as bit) as hed__FERPA__c,
--CAST(null as bit) as hed__Financial_Aid_Applicant__c,
CAST((CASE --SF_PickList(Male,Female,Other)
	WHEN QLOH.QL_Gender = 'F' THEN 'Female'
	WHEN QLOH.QL_Gender = 'M' THEN 'Male'
	WHEN QLOH.QL_Gender = 'O' THEN 'Other'
END) as varchar(255)) as hed__Gender__c,
CAST(null as varchar(max)) as hed__HIPAA_Detail__c,
--CAST(null as bit) as hed__HIPAA__c,
CAST(null as varchar(max)) as hed__Military_Background__c,
--CAST(null as bit) as hed__Military_Service__c,
CAST(null as varchar(4099)) as hed__Naming_Exclusions__c,
--If QL_MobilePhone is not NULL then 'Mobile' else If QL_Phone is not NULL then 'Other'
--QLOH.QL_MobileNo,QLOH.QL_Phone,
	CAST((CASE
		WHEN QLOH.QL_MobileNo IS NOT NULL THEN 'Mobile' 
		WHEN QLOH.QL_Phone IS NOT NULL THEN 'Home'
		ELSE NULL
	END) as varchar(255)) as hed__PreferredPhone__c,
--If QL_PersonalEmail is not NULL then 'Alternate' else If QL_LSBUEmail is not NULL then 'University'
	CAST((CASE --CFG.fx_HashEmail(QLOH.QL_LSBUEmail, QLOH.QL_StudentID) 'Personal Email','University,
		WHEN QLOH.QL_PersonalEmail IS NOT NULL AND QLOH.QL_LSBUEmail IS NULL THEN 'Personal Email' --'Alternate'
		WHEN QLOH.QL_LSBUEmail IS NOT NULL AND QLOH.QL_PersonalEmail IS NULL THEN 'University'
		WHEN QLOH.QL_LSBUEmail IS NOT NULL AND QLOH.QL_PersonalEmail IS NOT NULL THEN 'University'
		ELSE NULL
	END) as varchar(255)) as hed__Preferred_Email__c, 
--null as hed__Preferred_Email__c,
CAST(null as varchar(255)) as hed__Primary_Address_Type__c,
CAST(null as varchar(18)) as hed__Primary_Household__c,
CAST(null as varchar(18)) as hed__Primary_Organization__c,
CAST(null as varchar(255)) as hed__Religion__c,
CAST(null as varchar(255)) as hed__Secondary_Address_Type__c,
CAST(null as varchar(12)) as hed__Social_Security_Number__c,
--QLOH.QL_LSBUEmail as hed__UniversityEmail__c,--Production
--QLOH.QL_LSBUEmail, QLOH.QL_StudentID,'a',
case when db_name()='diprod'  
then QLOH.QL_LSBUEmail
else CAST(CFG.fx_HashEmail(QLOH.QL_LSBUEmail, QLOH.QL_StudentID) as varchar(80)) end as hed__UniversityEmail__c,--HashEmail
CAST(null as varchar(80)) as hed__WorkEmail__c,
CAST(null as varchar(40)) as hed__WorkPhone__c,
CAST(null as varchar(1300)) as hed__Work_Address__c,
--CAST(null as bit) as hed__is_Address_Override__c,
CAST(null as varchar(18)) as Primary_Student_Organization__c,
CAST(null as varchar(255)) as hed__Dual_Citizenship__c,
CAST(null as varchar(4099)) as hed__Race__c,
CAST(null as varchar(18)) as hed__Primary_Language__c,
CAST(QLOH.QL_FamiliarName as varchar(255)) as hed__Chosen_Full_Name__c,
CAST(null as varchar(255)) as hed__Citizenship_Status__c,
CAST(null as date) as hed__Date_Deceased__c,
CAST(null as varchar(80)) as hed__Former_First_Name__c,
CAST(null as varchar(80)) as hed__Former_Last_Name__c,
CAST(null as varchar(80)) as hed__Former_Middle_Name__c,
CAST(null as varchar(80)) as hed__Mailing_County__c,
CAST(null as varchar(80)) as hed__Other_County__c,
CAST((CASE 
	WHEN QLOH.QL_PersonalEmail IS NOT NULL THEN 'Email'
	WHEN QLOH.QL_Phone IS NOT NULL THEN 'Telephone'
	ELSE 'Not Applicable'
END) as varchar(255)) as LSB_ChannelOfPreference__c,
CAST(QLOH.SrcSystem as varchar(255)) as LSB_SourceSystem__c,
CAST(QLOH.QL_StudentID as varchar(90)) as LSB_SourceSystemID__c,
CAST(QLOH.QL_StudentID as varchar(90)) as LSB_ExternalID__c,
CAST(DCSS.[LSB_CON_StudentStatus__c] as varchar(255)) as LSB_CON_StudentStatus__c,--QLOH.QL_StudentStatus as LSB_CON_StudentStatus__c,
CAST(QLOH.QL_Disabilty as varchar(255)) as LSB_CON_Disability__c,
CAST(QLOH.QL_UCASPersonalID as varchar(90)) as LSB_CON_UCASID__c,
CAST(AEDA.QLCALC_ContactRole as varchar(255)) as LSB_CON_CurrentRole__c,
CAST(QLOH.QL_DisabiltyAllowance  as varchar(255)) as LSB_CON_DisabilityAllowance__c,
CAST(QLOH.QL_DDSMarking as varchar(255)) as LSB_CON_DDSMarking__c,
CAST(QL_CareLeaver as varchar(255)) as LSB_CON_CareLeavers__c,
CAST(AEDA.QL_TypeofCourse as varchar(255)) as LSB_CON_TypeOfCourseEnrolledIn__c,
CONVERT(varchar(255),
  CASE 
    WHEN SUBSTRING(AEDA.QL_AOSPeriod,1,1) IN ('1', '2', '3', '4', '5', '6', '7', '8', '9') THEN SUBSTRING(AEDA.QL_AOSPeriod,1,1)
    ELSE NULL
  END
) AS LSB_CON_YearOfStudy__c,
CAST(
  CASE 
   WHEN QLOH.QL_OptOut_SU = 'T' THEN '1'
   WHEN QLOH.QL_OptOut_SU = 'f' THEN '0' 
   ELSE 0
  END as BIT) as 	LSB_CON_OptedOutOfTheStudentsUnion__c,
CAST((
  CASE 
	 WHEN ED.QL_StudentGraduatingFlag = 'Yes' THEN 'Graduating'
	 WHEN QLOH.QL_New_Returning_Flag = '5' THEN 'New'
	 WHEN QLOH.QL_New_Returning_Flag = '9' THEN 'Returning'
	ELSE NULL
END) as varchar(255)) as LSB_CON_Newreturningorgraduating__c, 
CAST((
 CASE 
  WHEN AEDA.QL_AcadProgNameId = 'CPD_OPEN' THEN '1'
  ELSE '0'
 END) as BIT) as LSB_CON_CPD__c, 
 CAST((
  CASE 
   WHEN AEDA.QLCALC_AffiliationRole = 'Student' then AEDA.QL_EnrollmentStatus
   WHEN AEDA.QLCALC_AffiliationRole = 'Applicant' then AEDA.QL_ApplicationStatus
   ELSE NULL
  END) as varchar(255)) as LSB_CON_ApplicationEnrolmentStatus__c,  
  CAST((
  CASE 
   WHEN EXISTS (SELECT 1 
                FROM [INT].SRC_AcademicProgram AP 
                WHERE AP.QL_AcadProgName like '%Apprenticeship%'
                 AND AP.QL_AcadProgNameId = AEDA.QL_AcadProgNameId )
   THEN '1'
   ELSE '0'
  END) as BIT)  AS LSB_CON_Apprentice__c,
  CAST(AEDA.QL_AcademicPeriod as varchar(255)) as LSB_CON_CurrentAcademicPeriod__c,
  --TRY_CONVERT(date, ED.QL_ProgramStartDate, 103 ) as 	LSB_CON_PrimaryCourseStartDate__c
  TRY_CONVERT(date, ED.QL_ProgramStartDate, 103) AS LSB_CON_CurrentCourseStartDate__c,
  TRY_CONVERT(date, ED.QL_ProgramEndDate, 103) AS LSB_CON_CurrentCourseEndDate__c,
  CAST(AEDA.QL_ApplicationStatus AS VARCHAR(255)) AS LSB_CON_ApplicationStatus__c
FROM [INT].[SRC_Student] as QLOH
JOIN [SFC].[Get_ID_USER] OWN on OWN.username like 'dataintegrationuser@lsbu.com%'
LEFT JOIN [DST].[DICT_Contact_Nationality] DCN ON DCN.[QL_Nationality] = QLOH.QL_Nationality
LEFT JOIN [DST].[DICT_Contact_Domicile] DCD ON DCD.[QL_Domicile] = QLOH.QL_Domicile 
LEFT JOIN [DST].[DICT_QL_Add5_Country] DAC ON DAC.[QL_Add5] = QLOH.QL_Add5 
LEFT JOIN [DST].[DICT_Contact_StudentStatus] DCSS ON  DCSS.[QL_StudentStatus] = QLOH.QL_StudentStatus --need mod
LEFT JOIN [INT].[SRC_ApplicationEnrolmentDetails_Affiliation] AEDA ON AEDA.QL_StudentID = QLOH.QL_StudentID AND AEDA.QLCALC_AffiliationPrimaryFlag = 1 AND AEDA.QL_AcadProgNameId IS NOT NULL AND AEDA.ChangeStatus IN ('NEW','UPDATE','NO CHANGE')
LEFT JOIN [INT].[SRC_EnrolmentDetails] ED ON ED.QL_StudentID = QLOH.QL_StudentID 
                AND ED.QL_AcadProgNameId = AEDA.QL_AcadProgNameId
                AND ED.QL_AOSPeriod = AEDA.QL_AOSPeriod
                AND ED.QL_AcademicPeriod = AEDA.QL_AcademicPeriod
                AND ED.QL_EnrollmentStatus = AEDA.QL_EnrollmentStatus
LEFT JOIN [SFC].[Get_ID_Contact] AS sgic ON sgic.LSB_ExternalID__c = QLOH.QL_StudentID

WHERE (
  QLOH.ChangeStatus IN ('NEW','UPDATE')
  OR EXISTS (Select R.LSB_ExternalID__c from DST.SF_Reject_Contact R WHERE QLOH.LSB_ExternalID = R.LSB_ExternalID__c)
  OR AEDA.QLCALC_ContactRole <> sgic.LSB_CON_CurrentRole__c
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLOH.LSB_ExternalID
				AND	  VALID.ObjectName = 'Student'
				AND	  VALID.SrcSystem = 'QL')