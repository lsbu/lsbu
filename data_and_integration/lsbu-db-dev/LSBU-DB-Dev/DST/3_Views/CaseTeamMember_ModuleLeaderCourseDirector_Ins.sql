CREATE OR ALTER   VIEW [DST].[CaseTeamMember_ModuleLeaderCourseDirector_Ins] AS
SELECT
	DISTINCT
	CAST(null as varchar(18)) AS Id,
	CAST(GIDU.Id as varchar(18)) AS MemberId,
	CAST(GIDCA.Id as varchar(18)) AS ParentId,
	CAST(CTR.Id as varchar(18)) AS TeamRoleId,
	CAST(null as varchar(18)) AS TeamTemplateId,
	CAST(null as varchar(18)) AS TeamTemplateMemberId
FROM [INT].[SRC_ModuleLeaderCourseDirector_Relationship] as STR
 JOIN SFC.Get_ID_Contact GIDC ON STR.QL_StudentID = GIDC.LSB_ExternalID__c
 JOIN [SFC].[Get_ID_User] GIDU ON STR.Staff_LSB_ExternalID = GIDU.FederationIdentifier--LSB_UER_ExternalID__c
 JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
 JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name = 'Course Director'

WHERE 
STR.QL_CourseDirector IS NOT NULL
AND
(
	STR.ChangeStatus IN ('NEW')
	AND
	NOT EXISTS (Select CONCAT(R.MemberId, R.ParentId, R.TeamRoleId) from SFC.Get_ID_CaseTeamMember R 
	WHERE CONCAT(GIDCA.Id,'#',GIDU.Id) = CONCAT(R.ParentId,'#',R.MemberId))
)
AND 
NOT EXISTS (SELECT 1 FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = STR.QL_AcadProgNameId
				AND	  VALID.ObjectName = 'AcademicProgram'
				AND	  VALID.SrcSystem = 'QL')


UNION ALL


SELECT
	DISTINCT
	CAST(null as varchar(18)) AS Id,
	CAST(GIDU.Id as varchar(18)) AS MemberId,
	CAST(GIDCA.Id as varchar(18)) AS ParentId,
	CAST(CTR.Id as varchar(18)) AS TeamRoleId,
	CAST(null as varchar(18)) AS TeamTemplateId,
	CAST(null as varchar(18)) AS TeamTemplateMemberId
FROM [INT].[SRC_ModuleLeaderCourseDirector_Relationship] as STR
 JOIN SFC.Get_ID_Contact GIDC ON STR.QL_StudentID = GIDC.LSB_ExternalID__c
 JOIN [SFC].[Get_ID_User] GIDU ON STR.Staff_LSB_ExternalID = GIDU.FederationIdentifier--LSB_UER_ExternalID__c
 JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
 JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name = 'Module Leader'

WHERE 
STR.QL_ModuleLeader IS NOT NULL
AND
(
	STR.ChangeStatus IN ('NEW')
	AND
	NOT EXISTS (Select CONCAT(R.MemberId, R.ParentId, R.TeamRoleId) from SFC.Get_ID_CaseTeamMember R 
	WHERE CONCAT(GIDCA.Id,'#',GIDU.Id) = CONCAT(R.ParentId,'#',R.MemberId))
)
AND 
NOT EXISTS (SELECT 1 FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = STR.ModuleOffering_LSB_ExternalID
				AND	  VALID.ObjectName = 'ModuleOffering'
				AND	  VALID.SrcSystem = 'QL')