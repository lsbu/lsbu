CREATE OR ALTER VIEW [DST].[Appointment_MAXSIDDIA] AS
SELECT * FROM (
SELECT
--CAST(NULL as varchar(18)) AS CreatedById
ROW_NUMBER() OVER(PARTITION BY CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC) ORDER BY APPT.DIA_DATE) as RNK
,CAST(CASE
		WHEN APPT.DIA_CRED < '2000-01-01' THEN DATEADD(YY, 200, APPT.DIA_CRED)
		ELSE APPT.DIA_CRED
	  END as datetime) AS CreatedDate
--,CAST(NULL as varchar(18)) AS LastModifiedById
,CAST(APPT.DIA_UPDD as datetime) AS LastModifiedDate
,CAST('Time, Attendee and Organiser of appointment may not be accurate, please consider these indicative' as varchar(max)) AS LSB_ANT_AppointmentNotes__c
--,CAST(NULL as varchar(255)) AS LSB_ANT_AppointmentReminderTriggerDate__c --fixed
,CAST(NULL as varchar(255)) AS LSB_ANT_AppointmentRoom__c
,CAST(CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC) as varchar(255)) AS LSB_ANT_ExternalId__c
,CAST(NULL as varchar(max)) AS LSB_ANT_FollowUpActions__c
,CAST(NULL as datetime) AS LSB_ANT_PreviousStartDate__c
,CAST(APPT.DIA_MEET as varchar(80)) AS LSB_ANT_SourceSystemId__c
,CAST('Maximiser' as varchar(80)) AS LSB_ANT_SourceSystem__c
--,CAST(NULL as varchar(255)) AS LSB_ANT_TurnOffsfalAppointmentEventSync__c
,CAST(ConCAt(TMS.TMS_NAME,' Appointment : ',APPT.DIA_NAME) as varchar(255)) AS Name
,CAST(OId.Id as varchar(18)) AS OwnerId
,CAST(NULL as varchar(255)) AS sfal__AdditionalConnectionInformation__c
,CAST(NULL as varchar(18)) AS sfal__AppointmentLocation__c
--,CAST(NULL as varchar(255)) AS sfal__AttendeeLimit__c
,CAST(NULL as varchar(max)) AS sfal__Description__c
,CAST(DATEADD(HOUR,1,APPT.DIA_DATE) as datetime) AS sfal__EndDateTime__c
--,CAST(NULL as varchar(255)) AS sfal__IsDiscoverable__c
--,CAST(NULL as varchar(255)) AS sfal__IsWebMeeting__c
,CAST('Not Available' as varchar(255)) AS sfal__Location__c
,CAST(NULL as varchar(18)) AS sfal__RelatedCase__c
,CAST(GIST.ID as varchar(18)) AS sfal__RelatedSubtopic__c
,CAST(GIT.Id as varchar(18)) AS sfal__RelatedTopic__c
,CAST(APPT.DIA_DATE as datetime) AS sfal__StartDateTime__c
,CAST(AECTS.Subtopic as varchar(255)) AS sfal__Subtopic__c
,CAST(AECTS.Topic as varchar(255)) AS sfal__Topic__c
,CAST(NULL as varchar(255)) AS sfal__Type__c
,CAST(NULL as varchar(255)) AS sfal__WebMeetingLink__c
,APPT.DIA_NAME
FROM [SID].[dbo].[DIA] APPT
LEFT JOIN SFC.Get_ID_User OId 
		ON 
		CASE
			WHEN APPT.DIA_SPTC = SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) AND OId.Id is not null AND OID.IsActive = 1 THEN 1
			WHEN SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) = 'ohallord' AND OId.Id is not null THEN 1
			ELSE 0
		END =1
		
LEFT JOIN [SID].[dbo].[SPT] SPT ON SPT.SPT_TMSC = DIA_SPTC 
LEFT JOIN [SID].[dbo].[TMS] TMS ON SPT.SPT_TMSC = TMS.TMS_CODE
INNER JOIN [SID].[dbo].[GRP] GRP ON APPT.DIA_BRQC = GRP.GRP_CODE
LEFT JOIN DST.DICT_AppointmentEnquiryCategory_TopicSubtopic AECTS ON GRP.GRP_NAME = AECTS.Category
LEFT JOIN SFC.Get_ID_Topic GIT ON GIT.name = AECTS.TOPIC  --'Mental Health & Wellbeing Support'
LEFT JOIN SFC.Get_ID_Topic GIST ON GIST.name = AECTS.Subtopic  --'Mental Health & Wellbeing Support'
WHERE APPT.DIA_SPTC IS NOT NULL
AND APPT.DIA_CCNC IS NOT NULL
AND APPT.DIA_MEET IS NOT NULL
AND GRP.GRP_NAME IS NOT NULL
AND GRP.GRP_NAME IN (
'Covid 19 Support',
'Disability & Dyslexia support',
'Disabled Student Allowance',
'EP Feedback',
'Halls Students',
'Havering Appointment',
'Havering MHWB',
'HSC Students',
'Mental Health & Wellbeing',
'MHWB Telephone Appointment',
'Pre Entry Support',
'SPLD Screening Assessment',
'Support Arrangements')) as T1
WHERE RNK = 1
AND T1.LSB_ANT_ExternalId__c NOT IN (select a.LSB_ANT_ExternalId__c from sfc.Get_ID_Appointment as a where a.LSB_ANT_ExternalId__c is not null)
GO