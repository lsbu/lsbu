CREATE OR ALTER   VIEW [DST].[Check_SF_Rejected_Rows] AS
SELECT 'QL' as SrcSystem, 'Account_EducationInstitution' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Account_EducationInstitution
union all
SELECT 'QL' as SrcSystem, 'Account_Department' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Account_Department
union all
SELECT 'QL' as SrcSystem, 'Account_AcademicProgram' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Account_AcademicProgram
union all
SELECT 'QL' as SrcSystem, 'Contact' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Contact
union all
SELECT 'QL' as SrcSystem, 'Affiliation' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Affiliation
union all
SELECT 'QL' as SrcSystem, 'ApplicationParent' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_ApplicationParent
union all
SELECT 'QL' as SrcSystem, 'Application' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Application
union all
SELECT 'QL' as SrcSystem, 'TermParent' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_TermParent
union all
SELECT 'QL' as SrcSystem, 'Term' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Term
union all
SELECT 'QL' as SrcSystem, 'ProgramEnrollment' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_ProgramEnrollment
union all
SELECT 'QL' as SrcSystem, 'Address' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Address
union all
SELECT 'QL' as SrcSystem, 'Case' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Case
union all
SELECT 'QL' as SrcSystem, 'SupportProfile' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_SupportProfile_Student
union all
SELECT 'QL' as SrcSystem, 'Affiliation_AppliedSciencesPTCD' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Affiliation_AppliedSciencesPTCD
union all
SELECT 'QL' as SrcSystem, 'Contact_AppliedSciencesPTCD' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Contact_AppliedSciencesPTCD
union all
SELECT 'QL' as SrcSystem, 'CaseTeamMember_AllStaff_Del' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Load_CaseTeamMember_AllStaff_Del
union all
SELECT 'QL' as SrcSystem, 'Load_CaseTeamMember_AllStaff_Ins' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Load_CaseTeamMember_AllStaff_Ins
union all
SELECT 'QL' as SrcSystem, 'Relationship_AppliedSciencesPTCD' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Relationship_AppliedSciencesPTCD
union all
SELECT 'QL' as SrcSystem, 'User' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_User
union all
SELECT 'QL' as SrcSystem, 'Term_Grade' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_TermGrade
union all
SELECT 'QL' as SrcSystem, 'Address_Update' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Address_Update
union all
SELECT 'QL' as SrcSystem, 'User_AppliedSciencesPTCD_Update' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_User_AppliedSciencesPTCD_Update
union all
SELECT 'QL' as SrcSystem, 'Contact_ModuleLeaderCourseDirector' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Contact_ModuleLeaderCourseDirector
union all
SELECT 'QL' as SrcSystem, 'Affiliation_ModuleLeaderCourseDirector' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Affiliation_ModuleLeaderCourseDirector
union all
SELECT 'QL' as SrcSystem, 'Contact_Student_Update' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Contact_Student_Update
union all
SELECT 'QL' as SrcSystem, 'Course_Module' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Course_Module
union all
SELECT 'QL' as SrcSystem, 'CourseOffering_ModuleOffering' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_CourseOffering_ModuleOffering
union all
SELECT 'QL' as SrcSystem, 'CourseEnrollment_ModuleEnrolmentDetails' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_CourseEnrollment_ModuleEnrolmentDetails
union all
SELECT 'QL' as SrcSystem, 'ProgramEnrollment_StudentEngagementDetails' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_ProgramEnrollment_StudentEngagementDetails
union all
SELECT 'QL' as SrcSystem, 'CourseEnrollment_ModuleComponentEnrolmentDetails' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_CourseEnrollment_ModuleComponentEnrolmentDetails
union all
SELECT 'QL' as SrcSystem, 'CourseEnrollment_ModuleSubComponentEnrolmentDetails' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_CourseEnrollment_ModuleSubComponentEnrolmentDetails
union all
SELECT 'QL' as SrcSystem, 'SF_Reject_Load_Relationship_ModuleLeaderCourseDirector' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Load_Relationship_ModuleLeaderCourseDirector
union all
SELECT 'QL' as SrcSystem, 'SF_Reject_Delete_Relationship_ModuleLeaderCourseDirector' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Delete_Relationship_ModuleLeaderCourseDirector
union all
--SELECT 'QL' as SrcSystem, 'SF_Reject_Load_CaseTeamMember_ModuleLeaderCourseDirector' as SF_Object, count(*) as RejectCount
--from DST.SF_Reject_Load_CaseTeamMember_ModuleLeaderCourseDirector
--union all
--SELECT 'QL' as SrcSystem, 'SF_Reject_Delete_CaseTeamMember_ModuleLeaderCourseDirector' as SF_Object, count(*) as RejectCount
--from DST.SF_Reject_Delete_CaseTeamMember_ModuleLeaderCourseDirector
--union all
SELECT 'QL' as SrcSystem, 'SF_Reject_User_ApplicationEnrolmentDetails_DeProv_DeAct' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_User_ApplicationEnrolmentDetails_DeProv_DeAct
union all
SELECT 'CMIS' as SrcSystem, 'Delete_Relationship_StudentTeachingStaff' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Delete_Relationship_StudentTeachingStaff
--union all
--SELECT 'CMIS' as SrcSystem, 'Delete_Contact_StudentTeachingStaff' as SF_Object, count(*) as RejectCount
--from DST.SF_Reject_Delete_Contact_StudentTeachingStaff
union all
SELECT 'CMIS' as SrcSystem, 'Contact_StudentTeachingStaff' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Contact_StudentTeachingStaff
union all
SELECT 'CMIS' as SrcSystem, 'Relationship_StudentTeachingStaff' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Relationship_StudentTeachingStaff
union all
SELECT 'Connect' as SrcSystem, 'Case_ConnectCRM' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Case_ConnectCRM
union all
SELECT 'Connect' as SrcSystem, 'Affiliation_ConnectCRM' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Affiliation_ConnectCRM
union all
SELECT 'Connect' as SrcSystem, 'Contact_ConnectCRM' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Contact_ConnectCRM
union all
SELECT 'Connect' as SrcSystem, 'Relationship_ConnectCRM' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_Relationship_ConnectCRM
union all
SELECT 'Cognos' as SrcSystem, 'ProgramEnrollment_Cog_StudentEngagementDetails' as SF_Object, count(*) as RejectCount
from DST.SF_Reject_ProgramEnrollment_Cog_StudentEngagementDetails