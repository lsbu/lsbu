CREATE OR ALTER VIEW [DST].[CaseTeamMember_ModuleLeaderCourseDirector_Del] AS
SELECT
	CAST(GIDCTM.Id as varchar(18)) AS Id
FROM [INT].[SRC_ModuleLeaderCourseDirector_Relationship] as STR
 JOIN SFC.Get_ID_Contact GIDC ON STR.QL_StudentID = GIDC.LSB_ExternalID__c
 JOIN [SFC].[Get_ID_User] GIDU ON STR.Staff_LSB_ExternalID = GIDU.FederationIdentifier--LSB_UER_ExternalID__c
 JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
 JOIN [SFC].[Get_ID_CaseTeamMember] GIDCTM ON GIDU.Id = GIDCTM.MemberId AND GIDCA.Id = GIDCTM.ParentId
 JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name = 'Course Director' AND GIDCTM.TeamRoleId = CTR.Id

WHERE 
STR.QL_CourseDirector IS NOT NULL
AND
STR.ChangeStatus IN ('DELETE')


UNION ALL

SELECT
	CAST(GIDCTM.Id as varchar(18)) AS Id
FROM [INT].[SRC_ModuleLeaderCourseDirector_Relationship] as STR
 JOIN SFC.Get_ID_Contact GIDC ON STR.QL_StudentID = GIDC.LSB_ExternalID__c
 JOIN [SFC].[Get_ID_User] GIDU ON STR.Staff_LSB_ExternalID = GIDU.FederationIdentifier--LSB_UER_ExternalID__c
 JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
 JOIN [SFC].[Get_ID_CaseTeamMember] GIDCTM ON GIDU.Id = GIDCTM.MemberId AND GIDCA.Id = GIDCTM.ParentId
  JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name = 'Module Leader' AND GIDCTM.TeamRoleId = CTR.Id

WHERE 
STR.QL_ModuleLeader IS NOT NULL
AND
STR.ChangeStatus IN ('DELETE')