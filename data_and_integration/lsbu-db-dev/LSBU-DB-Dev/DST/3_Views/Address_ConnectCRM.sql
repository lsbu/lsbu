CREATE OR ALTER VIEW  [DST].[Address_ConnectCRM] AS
SELECT 

*
FROM
(
SELECT distinct
  CAST(
		CASE
			WHEN (select DISTINCT GIA.Id
			from SFC.Get_ID_Address GIA where GIA.hed__Default_Address__c = 1 and ST.LSB_ExternalID__c = left(GIA.LSB_ADS_ExternalID__c, charindex('.', GIA.LSB_ADS_ExternalID__c)-1) ) is null then 1
			else 0
		END  as bit) as [hed__Default_Address__c],-- [bit] NOT NULL,
  CAST(null as date) as [hed__Latest_End_Date__c],-- [datetime] NULL,
  CAST(null as date) as [hed__Latest_Start_Date__c],-- [datetime] NULL,
  CAST(SM.City as varchar(255)) as [hed__MailingCity__c],-- [varchar](255) NULL,
  CAST(Country as varchar(255)) as [hed__MailingCountry__c],-- [varchar](255) NULL,
  CAST(SM.Postcode as varchar(255)) as [hed__MailingPostalCode__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__MailingState__c],-- [varchar](255) NULL,
  CAST(STUFF(CONCAT(' ' + NULLIF(SM.Address_Line_1, ''),' ' + NULLIF(SM.Address_Line_2, '')),1,1,'') as varchar(255)) as [hed__MailingStreet__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__MailingStreet2__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Parent_Account__c],-- [varchar](18) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_End_Day__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_End_Month__c],-- [varchar](255) NULL,
  CAST(null as real) as [hed__Seasonal_End_Year__c],-- [real] NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_Start_Day__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_Start_Month__c],-- [varchar](255) NULL,
  CAST(null as real) as [hed__Seasonal_Start_Year__c],-- [real] NULL,
  CAST(ST.Id as varchar(18)) as [hed__Parent_Contact__c],-- [varchar](18) NULL,
  CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(90)) as [LSB_ADS_ExternalID__c],-- [varchar](90) NULL,
  CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(90)) as [LSB_ADS_SourceSystemID__c],-- [varchar](90) NULL,
  CAST('Connect' as varchar(255)) as [LSB_ADS_SourceSystem__c], -- [varchar](255) NULL
  ROW_NUMBER() OVER (PARTITION BY SM.Connect_ID ORDER BY CSSP.RANK ASC, SM.CREATED_DATE DESC) ROWN
FROM  INT.SRC_Connect_Salesforce_Migration SM 
 JOIN  SFC.[Get_ID_Contact] ST ON (SM.E_mail = ST.hed__AlternateEmail__c )   --Changed from INNER to LEFT
 LEFT JOIN [CFG].[ConnectSourceSystemPreference] CSSP ON ST.LSB_CON_SourceSystem__c = CSSP.SourceSystem
  WHERE NOT EXISTS (SELECT 1 FROM SFC.[Get_ID_Contact] GIC WHERE SM.QL_Student_ID = GIC.LSB_ExternalID__c )
  AND (SM.Address_Line_1 is not null or SM.Address_Line_2 IS NOT NULL) AND SM.Postcode IS NOT NULL 

UNION ALL

SELECT distinct
  CAST(
		CASE
			WHEN (select DISTINCT GIA.Id
			from SFC.Get_ID_Address GIA where GIA.hed__Default_Address__c = 1 and ST.LSB_ExternalID__c = left(GIA.LSB_ADS_ExternalID__c, charindex('.', GIA.LSB_ADS_ExternalID__c)-1) ) is null then 1
			else 0
		END  as bit) as [hed__Default_Address__c],-- [bit] NOT NULL,
  CAST(null as date) as [hed__Latest_End_Date__c],-- [datetime] NULL,
  CAST(null as date) as [hed__Latest_Start_Date__c],-- [datetime] NULL,
  CAST(SM.City as varchar(255)) as [hed__MailingCity__c],-- [varchar](255) NULL,
  CAST(Country as varchar(255)) as [hed__MailingCountry__c],-- [varchar](255) NULL,
  CAST(SM.Postcode as varchar(255)) as [hed__MailingPostalCode__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__MailingState__c],-- [varchar](255) NULL,
  CAST(STUFF(CONCAT(' ' + NULLIF(SM.Address_Line_1, ''),' ' + NULLIF(SM.Address_Line_2, '')),1,1,'') as varchar(255)) as [hed__MailingStreet__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__MailingStreet2__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Parent_Account__c],-- [varchar](18) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_End_Day__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_End_Month__c],-- [varchar](255) NULL,
  CAST(null as real) as [hed__Seasonal_End_Year__c],-- [real] NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_Start_Day__c],-- [varchar](255) NULL,
  CAST(null as varchar(255)) as [hed__Seasonal_Start_Month__c],-- [varchar](255) NULL,
  CAST(null as real) as [hed__Seasonal_Start_Year__c],-- [real] NULL,
  CAST(ST.Id as varchar(18)) as [hed__Parent_Contact__c],-- [varchar](18) NULL,
  CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(90)) as [LSB_ADS_ExternalID__c],-- [varchar](90) NULL,
  CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(90)) as [LSB_ADS_SourceSystemID__c],-- [varchar](90) NULL,
  CAST('Connect' as varchar(255)) as [LSB_ADS_SourceSystem__c], -- [varchar](255) NULL
  '1' AS ROWN
 FROM INT.SRC_Connect_Salesforce_Migration SM
 JOIN  SFC.[Get_ID_Contact] ST ON SM.QL_Student_ID = ST.LSB_ExternalID__c   --Changed from INNER to LEFT
 WHERE SM.QL_Student_ID is NOT NULL
   AND (SM.Address_Line_1 is not null or SM.Address_Line_2 IS NOT NULL) AND SM.Postcode IS NOT NULL 
) C
WHERE C.ROWN = 1