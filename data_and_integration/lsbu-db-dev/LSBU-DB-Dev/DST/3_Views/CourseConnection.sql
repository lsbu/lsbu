CREATE OR ALTER VIEW [DST].[CourseConnection] AS
SELECT 	
--RunID,FullFileName,	SrcSystem,	ObjectName,	RowNo,	FileDateTime,	LSB_ExternalID,	FileRowNumber,	HashMD5,
CAST(QL_CourseConnectionId as varchar(255)) as QL_CourseConnectionId
FROM [INT].[SRC_CourseConnection] as QLC
WHERE 
( 
QLC.ChangeStatus IN ('NEW','UPDATE')
--OR EXISTS (Select LSB_ACC_ExternalID__c from DST.SF_Reject_Account_AcademicProgram WHERE QLAP.LSB_ExternalID = LSB_ACC_ExternalID__c)
)
GO