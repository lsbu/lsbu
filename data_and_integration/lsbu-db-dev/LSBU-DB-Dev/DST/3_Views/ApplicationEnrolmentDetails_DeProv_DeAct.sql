
CREATE OR ALTER  VIEW [DST].[ApplicationEnrolmentDetails_DeProv_DeAct] AS
	SELECT 
			   CAST(AEDD.SF_UserID as varchar(18)) AS Id
			   ,'0' AS IsActive										-- New Field
			  ,CAST(AEDD.QL_StudentID as varchar(18)) AS QL_StudentID
			  ,CAST(AEDD.QLCALC_ContactRole as varchar(255)) as QLCALC_ContactRole
			  ,CAST(AEDD.QL_ApplicationStatus as varchar(255)) as QL_ApplicationStatus
			  ,TRY_CONVERT(DATE, AEDD.QL_AppStatusDateAchieved, 103) as QL_AppStatusDateAchieved
			  ,CAST(AEDD.QL_EnrollmentStatus as varchar(40)) as QL_EnrolmentStatus
			  ,TRY_CONVERT(DATE, AEDD.QL_EnrolStatusDateAchieved, 103) as QL_EnrolStatusDateAchieved
			  ,CAST(QL_StudentGraduated as varchar(40)) as QL_StudentGraduated
			  ,TRY_CONVERT(DATE, AEDD.QLCALC_RowUpsertedDate, 103) AS QLCALC_RowUpsertedDate
			  ,TRY_CONVERT(DATE, AEDD.QLCALC_DeProvisioningdate, 103) AS QLCALC_DeProvisioningdate
			  ,CAST(QLCALC_DeProvisionedStatus  as varchar(40)) AS QLCALC_DeProvisionedStatus
			  ,TRY_CONVERT(DATE, AEDD.QLCALC_DeProvisionedStatusDate, 103) AS QLCALC_DeProvisionedStatusDate
	FROM [INT].[SRC_ApplicationEnrolmentDetails_DeProv_DeAct] AEDD
	WHERE 
	TRY_CONVERT(DATE, AEDD.QLCALC_DeProvisioningdate, 103) = TRY_CONVERT(DATE, GETDATE(), 103)
	AND 
	UPPER(AEDD.QLCALC_DeProvisionedStatus) = 'TO BE DONE'
  --OR EXISTS (Select 1 from DST.SF_Reject R WHERE MO.LSB_ExternalID = R.LSB_CSE_ExternalID__c)




