CREATE OR ALTER VIEW [DST].[CaseTeamMember_AppliedSciencesPTCD_Ins] AS
SELECT
 	CAST(null as varchar(18)) AS Id,
	CAST(RSP.QL_PersonalTutorId as varchar(18)) AS MemberId,
	CAST(GIDCA.Id as varchar(18)) AS ParentId,
	CAST(CTR.Id as varchar(18)) AS TeamRoleId,
	CAST(null as varchar(18)) AS TeamTemplateId,
	CAST(null as varchar(18)) AS TeamTemplateMemberId
FROM [INT].[SRC_AppliedSciencesPTCD_Relationship] RSP
  JOIN [SFC].[Get_ID_Contact] GIDC ON RSP.PTCDCSV_StudentId = GIDC.LSB_ExternalID__c
  JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
  JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name = 'Personal Tutor'
WHERE (RSP.ChangeStatus IN ('NEW')
OR EXISTS (Select CONCAT(R.MemberId, R.ParentId) from DST.SF_Reject_Load_CaseTeamMember_AppliedSciencesPTCD R WHERE CONCAT(RSP.QL_PersonalTutorId, GIDCA.Id) = CONCAT(R.MemberId, R.ParentId))
OR (NOT EXISTS (SELECT * FROM [SFC].[Get_ID_Relationship] GIR WHERE GIR.LSB_RTP_ExternalId__c = CONCAT(RSP.QL_PersonalTutorId,'.',RSP.PTCDCSV_StudentId)) AND RSP.ChangeStatus <> 'DELETE')
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
					WHERE VALID.ValidationResultFlag = 'ERROR'
					AND   VALID.LSB_ExternalID = SUBSTRING( RSP.LSB_ExternalID, CHARINDEX('.', RSP.LSB_ExternalID)+1,len(RSP.LSB_ExternalID))
					AND	  VALID.ObjectName = 'AppliedSciencesPTCD'
					AND	  VALID.SrcSystem = 'SAL')
GO