CREATE OR ALTER VIEW [DST].[LSBU_Account] AS
SELECT 
'London South Bank University' as Name,
(select Id from sfc.RecordType WHERE DeveloperName = 'Business_Organization') as RecordTypeId,
'90 London Road' as BillingStreet,
'London' as BillingCity,
'SE16LN' as BillingPostalCode,
'UK' as BillingCountry,
'+44 (0) 20 7815 6189' as Phone,
'course.enquiry@lsbu.ac.uk' as Email,
'https://www.lsbu.ac.uk/' as Website,
'LSBU' as LSB_ACC_ExternalID__c,
'LSBU' as LSB_ACC_SourceSystemID__c,
'QL' as LSB_ACC_SourceSystem__c
