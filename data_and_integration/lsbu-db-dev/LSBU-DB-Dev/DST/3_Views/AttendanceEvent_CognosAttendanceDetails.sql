CREATE OR ALTER VIEW [DST].[AttendanceEvent_CognosAttendanceDetails] AS
SELECT DISTINCT
	--CAST(NULL AS VARCHAR(18)) AS CreatedById
	--,CAST(NULL AS DATETIME) AS CreatedDate
	--CAST(NULL AS DATETIME) AS hed__Arrival_Time__c
	--,CAST(NULL AS VARCHAR(255)) AS hed__Attendance_Type__c
	CAST(GIDC.Id AS VARCHAR(18)) AS hed__Contact__c
	--,CAST(NULL AS VARCHAR(18)) AS hed__Course_Connection__c
	--,CAST(NULL AS VARCHAR(1300)) AS hed__Course_Name__c --formula
	--,CAST(NULL AS DATE) AS hed__Date__c
	--,CAST(NULL AS DATETIME) AS hed__End_Time__c
	--,CAST(NULL AS VARCHAR(MAX)) AS hed__Reason_Description__c
	--,CAST(NULL AS VARCHAR(255)) AS hed__Reason__c
	--,CAST(NULL AS DATETIME) AS hed__Start_Time__c
	--,CAST(NULL AS BIT) AS hed__Unexcused__c
	--,CAST(NULL AS VARCHAR(18)) AS Id
	--,CAST(NULL AS BIT) AS IsDeleted
	--,CAST(NULL AS DATE) AS LastActivityDate
	--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
	--,CAST(NULL AS DATETIME) AS LastModifiedDate
	--,CAST(NULL AS DATETIME) AS LastReferencedDate
	--,CAST(NULL AS DATETIME) AS LastViewedDate
	,CAST(CSAD.Cognos_ModuleLevelAttendance AS REAL) AS LSB_ATT_NoOfClassesAttended__c
	,CAST(CSAD.Cognos_CourseLevelAttendancepercent AS REAL) AS LSB_ATT_OfClassesAttended__c
	,CAST(CSAD.Cognos_TotalModuleClasses AS REAL) AS LSB_ATT_TotalNoOfClasses__c
	,CAST(CSAD.LSB_ExternalID AS VARCHAR(90)) AS LSB_ATT_ExternalID__c
	,CAST(CSAD.LSB_ExternalID AS VARCHAR(90)) AS LSB_ATT_SourceSystemID__c
	,CAST('QL' AS VARCHAR(255)) AS LSB_ATT_SourceSystem__c
	--,CAST(NULL AS VARCHAR(80)) AS Name
	--,CAST(NULL AS VARCHAR(18)) AS OwnerId
	--,CAST(NULL AS DATETIME) AS SystemModstamp
FROM [INT].[SRC_CognosAttendanceDetails] CSAD
LEFT JOIN SFC.Get_ID_Contact GIDC ON CSAD.Cognos_StudentID = GIDC.LSB_ExternalID__c
WHERE (
  CSAD.ChangeStatus IN ('NEW','UPDATE')
  --OR EXISTS (Select R.LSB_COF_ExternalID__c from DST.SF_Reject_CourseOffering_ModuleOffering R WHERE QLMO.LSB_ExternalID = R.LSB_COF_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = CSAD.LSB_ExternalID
				AND	  VALID.ObjectName = 'StudentAttendanceDetails'
				AND	  VALID.SrcSystem = 'Cognos')