CREATE OR ALTER VIEW [DST].[CaseTeamMember_AppliedSciencesPTCD_Del] AS
SELECT
 	CAST(GIDCTM.Id as varchar(18)) AS Id,
	CAST(null as varchar(18)) AS MemberId,
	CAST(null as varchar(18)) AS ParentId,
	CAST(null as varchar(18)) AS TeamRoleId,
	CAST(null as varchar(18)) AS TeamTemplateId,
	CAST(null as varchar(18)) AS TeamTemplateMemberId
FROM [INT].[SRC_AppliedSciencesPTCD_Relationship] RSP
  JOIN [SFC].[Get_ID_Contact] GIDC ON RSP.PTCDCSV_StudentId = GIDC.LSB_ExternalID__c
  JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
  JOIN [SFC].[Get_ID_CaseTeamMember] GIDCTM ON RSP.QL_PersonalTutorId = GIDCTM.MemberId AND GIDCA.Id = GIDCTM.ParentId
  JOIN [SFC].[Get_ID_CaseTeamRole] CTR ON CTR.Name = 'Personal Tutor' and GIDCTM.TeamRoleId = CTR.Id
WHERE RSP.ChangeStatus IN ('DELETE')
GO