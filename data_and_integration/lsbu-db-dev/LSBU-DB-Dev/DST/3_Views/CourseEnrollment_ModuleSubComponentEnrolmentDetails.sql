CREATE OR ALTER VIEW [DST].[CourseEnrollment_ModuleSubComponentEnrolmentDetails] AS

SELECT 
	GICE.hed__Account__c,
	GICE.LSB_CCN_SessionCode__c,
    GICE.LSB_CCN_SessionDescription__c,
    GICE.LSB_CCN_ModuleEnrolmentStatus__c,
    GICE.LSB_CCN_ModuleEnrolmentStatusdateach__c,
    GICE.LSB_CCN_CompulsoryOptional__c,
    GICE.LSB_CCN_Term__c,
    GICE.hed__Affiliation__c,
    GICE.hed__Contact__c,
    GICE.hed__Program_Enrollment__c,
    GICE.hed__Course_Offering__c,
    CAST(MCED.LSB_ExternalID AS VARCHAR(255)) AS LSB_CCN_ExternalID__c,
    CAST(MCED.LSB_ExternalID AS VARCHAR(255)) AS LSB_CCN_SourceSystemID__c,
    CAST(MCED.SrcSystem as varchar(255)) AS LSB_CCN_SourceSystem__c ,
    GICE.LSB_CCN_Semester__c,
    GICE.LSB_CCN_Resit__c,
	MCED.QL_SubComponentId AS LSB_CCN_SubComponentId__c,
	MCED.QL_SubComponentDesc AS LSB_CCN_SubComponentDescription__c,
	CONVERT(DATE,MCED.QL_SubComponentHandinDate,104) AS LSB_CCN_HandInDate__c,

	CASE 
		WHEN MCED.QL_ComponentId LIKE 'CW%' THEN 'Coursework'
		WHEN MCED.QL_ComponentId LIKE 'EX%' THEN 'Exam'
	END AS LSB_CCN_ComponentType__c,
	GICE.ID AS LSB_CCN_ParentModuleComponent__c,
	(SELECT ID FROM SFC.RecordType where DeveloperName = 'LSB_ModuleSubcomponent') AS RecordTypeId
FROM INT.SRC_ModuleSubComponentEnrolmentDetails MCED
INNER  JOIN SFC.Get_ID_CourseEnrollment GICE ON MCED.ModuleEnrolment_LSB_ExternalID = GICE.LSB_CCN_ExternalID__c
WHERE 
(
	MCED.ChangeStatus   IN ('NEW','UPDATE')
	OR  
	(NOT EXISTS (SELECT * FROM [SFC].Get_ID_CourseEnrollment GICE WHERE GICE.LSB_CCN_ExternalID__c = MCED.LSB_ExternalID) AND MCED.ChangeStatus <> 'DELETE')  -- New Condition
)