CREATE OR ALTER VIEW [DST].[ContentVersion_SIDNotes]
AS
SELECT
	CreatedDate
	,Lastmodifieddate
	,CreatedbyId						-- NewField to be mapped
	,LastmodifiedbyId				-- NewField to be mapped
	,FirstPublishLocationId
	,LSB_CVR_DocumentType__c
	,LSB_CVR_ExternalId__c
	,LSB_CVR_SourceSystemId__c
	,LSB_CVR_SourceSystem__c
	,SharingOption
	,SharingPrivacy
	,PathOnClient
	,Title
	,CONCAT('Contents: ',T1.VersionData) as VersionData
FROM
(------------------------------------------------Contact Notes-----------------------------------------------------------------------------
SELECT 
CAST(CMN.CMN_CRED as datetime) AS CreatedDate
,CAST(CMN.CMN_UPDD as datetime) AS Lastmodifieddate
,CAST(NULL as varchar(18)) AS CreatedbyId
,CAST(NULL as varchar(18)) AS LastmodifiedbyId
--,CAST('snote' AS VARCHAR(255)) AS FileExtension
--,CAST('SNOTE' AS VARCHAR(255)) AS FileTYPE
,CAST(GIC.Id as varchar(18)) FirstPublishLocationId
,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(CMM.CMM_CCNC,'.',CMN.CMN_CODE) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(CMN.CMN_CODE AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('SID' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST('P' AS VARCHAR(255)) SharingPrivacy
,CAST(CONCAT(CMM.CMM_CCNC,'.',CMN.CMN_CODE,'-SIDNotes.html') AS VARCHAR(500)) AS PathOnClient
,CAST(CONCAT(CMM.CMM_CCNC,' - ',CMN.CMN_SUBJ,' - SIDNotes') AS VARCHAR(255)) AS Title
--,CAST(
--REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CMND.CMN_NOTE, '<html>', ''), '</html>', ''), '<head>', ''), '</head>', ''), '<body>', ''), '</body>', ''), '<p>', ''), '</p>', ''), '<br>', ''), '</br>', '')
--,'&', '&amp;')
--,'''', '&#39;')
--,'"','&quot;')
--, '&amp;nbsp;', '')
--AS VARCHAR(max)) as VersionData
,CAST(CMND.CMN_NOTE AS VARCHAR(max)) as VersionData
FROM [SID_decrypted].[DBO].[CMN_Decrypted] CMND
INNER JOIN [SID].[DBO].[CMN] CMN ON CMND.CMN_CODE = CMN.CMN_CODE
INNER JOIN [SID].[DBO].[CMM] CMM ON CMN.CMN_CMMC = CMM.CMM_CODE
INNER JOIN SFC.Get_ID_Contact GIC ON GIC.LSB_ExternalID__c = CMM.CMM_CCNC
--LEFT JOIN SFC.Get_ID_User CbyId ON MSIN.Creator = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
UNION ALL

SELECT 
CAST(CMN.CMN_CRED as datetime) AS CreatedDate
,CAST(CMN.CMN_UPDD as datetime) AS Lastmodifieddate
,CAST(NULL as varchar(18)) AS CreatedbyId
,CAST(NULL as varchar(18)) AS LastmodifiedbyId
--,CAST('snote' AS VARCHAR(255)) AS FileExtension
--,CAST('SNOTE' AS VARCHAR(255)) AS FileTYPE
,CAST(GIC.Id as varchar(18)) FirstPublishLocationId
,CAST(NULL AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(CMM.CMM_CCNC,'.',CMN.CMN_CODE,'.Case') AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(CMN.CMN_CODE AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('SID' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST('P' AS VARCHAR(255)) SharingPrivacy
,CAST(CONCAT(CMM.CMM_CCNC,'.',CMN.CMN_CODE,'-SIDCaseNotes.html') AS VARCHAR(500)) AS PathOnClient
,CAST(CONCAT(CMM.CMM_CCNC,' - ',CMN.CMN_SUBJ,' - SIDNotes') AS VARCHAR(255)) AS Title
--,CAST(
--REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CMND.CMN_NOTE, '<html>', ''), '</html>', ''), '<head>', ''), '</head>', ''), '<body>', ''), '</body>', ''), '<p>', ''), '</p>', ''), '<br>', ''), '</br>', '')
--,'&', '&amp;')
--,'''', '&#39;')
--,'"','&quot;')
--, '&amp;nbsp;', '')
--AS VARCHAR(max)) as VersionData
,CAST(CMND.CMN_NOTE AS VARCHAR(max)) as VersionData
FROM [SID_decrypted].[DBO].[CMN_Decrypted] CMND
INNER JOIN [SID].[DBO].[CMN] CMN ON CMND.CMN_CODE = CMN.CMN_CODE
INNER JOIN [SID].[DBO].[CMM] CMM ON CMN.CMN_CMMC = CMM.CMM_CODE
INNER JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(CMM.CMM_CCNC,'_Case')
) AS T1
WHERE T1.LSB_CVR_ExternalId__c NOT IN (select LSB_CVR_ExternalId__c from sfc.Get_ID_ContentVersion WHERE LSB_CVR_ExternalID__c IS NOT NULL)
GO