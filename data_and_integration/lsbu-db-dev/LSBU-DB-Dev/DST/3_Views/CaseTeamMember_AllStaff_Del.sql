CREATE OR ALTER VIEW [DST].[CaseTeamMember_AllStaff_Del] AS
SELECT 
 	CAST(GICTM.Id as varchar(18))  AS Id
FROM ( select distinct StudentId ,FederationIdentifier  from [INT].[SRC_AllStaff_CaseTeamMember] 
		WHERE ChangeStatus IN ('DELETE') ) CTM
  JOIN [SFC].[Get_ID_Contact] GIDC ON CTM.StudentId = GIDC.LSB_ExternalID__c
  JOIN [SFC].[Get_ID_User] GIU ON CTM.FederationIdentifier = GIU.FederationIdentifier
  JOIN [SFC].[Get_ID_Case] GIDCA ON GIDC.Id = GIDCA.ContactId AND GIDCA.RecordTypeId = (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord') 
  JOIN [SFC].[Get_ID_CaseTeamMember] GICTM ON GIU.Id = GICTM.MemberId and GIDCA.Id =  GICTM.ParentId
