CREATE OR ALTER VIEW [DST].[Contact_Student_Update] AS
SELECT DISTINCT
SGIC.Id as Id,
SGIU.Id as LSB_CON_User__c,
SGICA.Id as LSB_CON_AdviseeCaseRecord__c
FROM [SFC].Get_ID_Contact SGIC
LEFT JOIN [SFC].Get_ID_User SGIU ON SGIU.ContactId = SGIC.Id
LEFT JOIN [SFC].Get_ID_Case SGICA ON SGICA.ContactId = SGIC.Id AND SGICA.RecordTypeId in (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
WHERE 
(SGIU.Id is not null or SGICA.Id is not null)
AND
((SGIC.LSB_CON_User__c IS NULL AND SGIU.Id IS NOT NULL)
 OR SGIU.Id != SGIC.LSB_CON_User__c
 OR (SGIC.LSB_CON_AdviseeCaseRecord__c IS NULL AND SGICA.Id IS NOT NULL)
 OR SGICA.Id != SGIC.LSB_CON_AdviseeCaseRecord__c)