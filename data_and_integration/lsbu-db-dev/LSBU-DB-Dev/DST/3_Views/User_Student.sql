CREATE OR ALTER VIEW [DST].[User_Student] AS

select [AboutMe]
,[Alias]
,[CallCenterId]
,[City]
,[CommunityNickname]
,[CompanyName]
,[ContactId]
,[Country]
,[DelegatedApproverId]
,[Department]
,[Division]
,[Email]
,[EmailEncodingKey]
,[EmployeeNumber]
,[Extension]
,[Fax]
,[FederationIdentifier]
,[FirstName]
,[GeocodeAccuracy]
,[IndividualId]
,[IsActive]
,[LanguageLocaleKey]
,[LastName]
,[Latitude]
,[LocaleSidKey]
,[Longitude]
,[LSB_UER_ExternalID__c]
,[LSB_UER_SourceSystemID__c]
,[LSB_UER_SourceSystem__c]
,[LSB_USE_InactiveReason__c]
,[LSB_USR_Linkedin__c]
,[ManagerId]
,[MobilePhone]
,[Phone]
,[PortalRole]
,[PostalCode]
,[ProfileId]
,[SenderEmail]
,[SenderName]
,[Signature]
,[State]
,[StayInTouchNote]
,[StayInTouchSignature]
,[StayInTouchSubject]
,[Street]
,[TimeZoneSidKey]
,[Title]
,[Username]
from (
SELECT
  CAST(NULL as varchar(1000)) as AboutMe,
  CAST(IIF(QLSDT.QL_PersonalEmail = GIDU.Email, GIDU.Alias, SUBSTRING(CONCAT(QLSDT.QL_FirstName, QLSDT.QL_Surname), 0, 8)) as varchar(8)) as Alias,
  CAST(NULL as varchar(18)) as CallCenterId,
  CAST(NULL as varchar(40)) as City,
  CAST(IIF(GIDU.CommunityNickname IS NOT NULL, GIDU.CommunityNickname, CONCAT(SUBSTRING(CONCAT(QLSDT.QL_FirstName, ' ', QLSDT.QL_Surname), 1, 40 - LEN(CONCAT(' ', QLSDT.QL_StudentID))), ' ', QLSDT.QL_StudentID)) as varchar(40)) as CommunityNickname,
  CAST(NULL as varchar(80)) as CompanyName,
  CAST(GIDC.Id as varchar(18)) as ContactId,
  CAST(NULL as varchar(80)) as Country,
  CAST(NULL as varchar(18)) as DelegatedApproverId,
  CAST(NULL as varchar(80)) as Department,
  CAST(NULL as varchar(80)) as Division,
  CAST(GIDC.Email as varchar(128)) as Email,
  CAST('ISO-8859-1' as varchar(40)) as EmailEncodingKey,
  CAST(NULL as varchar(20)) as EmployeeNumber,
  CAST(NULL as varchar(40)) as Extension,
  CAST(NULL as varchar(40)) as Fax,
  CAST(CASE
		WHEN QLSDT.QL_StudentUsername IS NOT NULL THEN CONCAT(lower(QLSDT.QL_StudentUsername), '@lsbu.ac.uk') 
		ELSE NULL
		END
	as varchar(512)) as FederationIdentifier, -- Change
  CAST(IIF(QLSDT.QL_PersonalEmail = GIDU.Email, GIDU.FirstName,
  CASE
  WHEN QLSDT.QL_Surname IN ('-','*','.') THEN NULL
  WHEN QLSDT.QL_FirstName in ('-','*','.') THEN NULL
  ELSE QLSDT.QL_FirstName
  END)  as varchar(40)) as FirstName,
  CAST(NULL as varchar(40)) as GeocodeAccuracy,
  CAST(NULL as varchar(18)) as IndividualId,
  CAST(IIF(DB_NAME() IN ('diprod','dipreprod'), 
  	CASE
		  WHEN GIDU.ID IS NOT NULL THEN GIDU.IsActive
		  WHEN A.[QLCALC_ContactRole] in ('Applicant') THEN 1
		  ELSE 0
	  END
  , 0) as bit) as IsActive,
  CAST('en_US' as varchar(40)) as LanguageLocaleKey,
  CAST(IIF(QLSDT.QL_PersonalEmail = GIDU.Email, GIDU.LastName,
  CASE
  WHEN QLSDT.QL_Surname IN ('-','*','.') THEN QLSDT.QL_FirstName
  WHEN QLSDT.QL_Surname IS NULL THEN QLSDT.QL_FirstName
  ELSE QLSDT.QL_Surname
  END) as varchar(80)) as LastName,
  CAST(NULL as real) as Latitude,
  CAST('en_GB' as varchar(40)) as LocaleSidKey,
  CAST(NULL as real) as Longitude,
  CAST(QLSDT.QL_StudentID as varchar(90)) as LSB_UER_ExternalID__c,
  CAST(QLSDT.QL_StudentID as varchar(90)) as LSB_UER_SourceSystemID__c,
  CAST('QL' as varchar(255)) as LSB_UER_SourceSystem__c,
  CAST(NULL as varchar(255)) as LSB_USE_InactiveReason__c,
  CAST(NULL as varchar(255)) as LSB_USR_Linkedin__c,
  CAST(NULL as varchar(18)) as ManagerId,
  CAST(NULL as varchar(40)) as MobilePhone,
  CAST(NULL as varchar(40)) as Phone,
  CAST(NULL as varchar(40)) as PortalRole,
  CAST(NULL as varchar(20)) as PostalCode,
  CAST(IIF(DB_NAME() IN ('diprod','dipreprod'), 
  	CASE
		WHEN GIDU.ID IS NOT NULL THEN GIDU.ProfileId
  		ELSE GIDP.Id
  	END, GIDP.Id) as varchar(18)) as ProfileId,
  CAST(NULL as varchar(80)) as SenderEmail,
  CAST(NULL as varchar(80)) as SenderName,
  CAST(NULL as varchar(1333)) as Signature,
  CAST(NULL as varchar(80)) as State,
  CAST(NULL as varchar(512)) as StayInTouchNote,
  CAST(NULL as varchar(512)) as StayInTouchSignature,
  CAST(NULL as varchar(80)) as StayInTouchSubject,
  CAST(NULL as varchar(255)) as Street,
  CAST('Europe/London' as varchar(40)) as TimeZoneSidKey,
  CAST(IIF(QLSDT.QL_PersonalEmail = GIDU.Email, GIDU.Title, QLSDT.QL_Title) as varchar(80)) as Title,
  CAST(IIF(GIDU.Email IS NOT NULL, GIDU.Username,
  CASE
  WHEN A.[QLCALC_ContactRole] in ('Student', 'Student - Interrupted') THEN CONCAT(QLSDT.QL_StudentUsername, '@lsbu.ac.uk', IIF(DB_NAME() = 'diprod', '', '.'+DB_NAME()))
  WHEN A.[QLCALC_ContactRole] in ('Applicant') THEN CONCAT(QLSDT.QL_PersonalEmail, IIF(DB_NAME() = 'diprod', '', '.'+DB_NAME())) end) as varchar(80)) as Username,

  ROW_NUMBER () OVER ( partition by
  ---username
  CAST(IIF(GIDU.Email IS NOT NULL, GIDU.Username,
  CASE
  WHEN A.[QLCALC_ContactRole] in ('Student', 'Student - Interrupted') THEN CONCAT(QLSDT.QL_StudentUsername, '@lsbu.ac.uk', IIF(DB_NAME() = 'diprod', '', '.'+DB_NAME()))
  WHEN A.[QLCALC_ContactRole] in ('Applicant') THEN CONCAT(QLSDT.QL_PersonalEmail, IIF(DB_NAME() = 'diprod', '', '.'+DB_NAME())) end) as varchar(80))
  ---/username
  order by (case when QL_UCASPersonalID is not null then 1 else 2 end),
  QL_UCASPersonalID,LSB_UER_ExternalID__c asc) UsernameRank
FROM [INT].[SRC_Student] as QLSDT
  INNER JOIN [SFC].[Get_ID_Contact] GIDC on QLSDT.QL_StudentID = GIDC.LSB_ExternalID__c
  INNER JOIN [INT].[SRC_ApplicationEnrolmentDetails_Affiliation] A on QLSDT.QL_StudentID = A.[QL_StudentID] AND a.QLCALC_AffiliationPrimaryFlag = 1 and a.QL_AcadProgNameId is not null
  LEFT JOIN [SFC].[Get_ID_Profile] GIDP on GIDP.Name = (CASE
  WHEN A.[QLCALC_ContactRole] in ('Student', 'Student - Interrupted') THEN 'Student'
  WHEN A.[QLCALC_ContactRole] in ('Applicant') THEN 'Offer Holder'
  END)
  LEFT JOIN [SFC].[Get_ID_User] GIDU on QLSDT.LSB_ExternalID = GIDU.LSB_UER_ExternalID__c
  --LEFT JOIN [SFC].[Get_ID_ProgramEnrollment] GIDPE on GIDC.Id = GIDPE.hed__Contact__c
WHERE
((QLSDT.QL_StudentUsername IS NOT NULL AND A.[QLCALC_ContactRole] IN ('Student', 'Student - Interrupted')) --AND A.QL_EnrollmentStatus IN ('EFE','EINTA','ESLEP') AND A.QL_AcademicPeriod in ('20/21','21/22'))
OR (QLSDT.QL_PersonalEmail IS NOT NULL AND A.[QLCALC_ContactRole] = 'Applicant' AND (A.QL_ApplicationStatus IN ('AACPT','AUF','AUI','ACF','ACI')  
OR A.[QL_EnrollmentStatus] in ('EASS')))
)
AND (
QLSDT.ChangeStatus IN ('NEW', 'UPDATE')
OR (QLSDT.ChangeStatus NOT IN ('NEW', 'UPDATE','DELETE') AND A.ChangeStatus IN ('NEW', 'UPDATE'))
OR GIDU.Email <> GIDC.Email --change
OR (GIDU.Email IS NULL AND QLSDT.ChangeStatus <> 'NEW') --change
OR EXISTS (SELECT R.LSB_UER_ExternalID__c FROM DST.SF_Reject_User R WHERE QLSDT.LSB_ExternalID = R.LSB_UER_ExternalID__c)
OR (GIDU.Id is null AND QLSDT.ChangeStatus <> 'DELETE'))
) z
WHERE z.UsernameRank = 1
AND NOT EXISTS (SELECT GIU.Id from [SFC].[Get_ID_User] GIU WHERE GIU.Username = z.Username AND GIU.LSB_UER_ExternalID__c <> z.LSB_UER_ExternalID__c)