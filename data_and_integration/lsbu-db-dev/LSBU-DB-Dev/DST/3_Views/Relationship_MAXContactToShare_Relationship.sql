CREATE OR ALTER VIEW [DST].[Relationship_MAXContactToShare_Relationship] AS
WITH IndividualDetails_cte AS (

SELECT
 qr_2.* 
FROM (
SELECT 
 qr_1.*,
 ROW_NUMBER() OVER(PARTITION BY qr_1.Student_ID ORDER BY qr_1.data_cnt DESC) rnk
FROM (
 SELECT 
  *,
  IIF(ARCHIVED_FIELDS_Assessing_LEA_Name is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Duration_of_temporary_disability is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Ed_Psych_Invoice_number is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Email_Address_2 is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_First_language is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Lockers_Locker_notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Note_Taker_initial_hours is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Note_Taker_s_ is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Dyslexia_Tutor_date_Allocated is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Hours is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Library_Support_Worker is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Mentor_date_Allocated is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_MENTOR_NOTES is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_OLD_dont_use_Pre_Entry___notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Other_Support is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Tutor_Support_Notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Recommendations_or_Comments is NULL, 0, 1)+
  IIF(Area_Of_Study_Course_Code_ is NULL, 0, 1)+
  IIF(Attend_Mode is NULL, 0, 1)+
  IIF(Confidential_Information is NULL, 0, 1)+
  IIF(Course_In_Full is NULL, 0, 1)+
  IIF(Covid_19_Fields_Coronavirus_questions_Other_coronavirus_detail__please_specify_ is NULL, 0, 1)+
  IIF(Covid_19_Fields_Covid_plan_created_notes is NULL, 0, 1)+
  IIF(Covid_19_Fields_Further_information_Anything_else_to_know_before_we_book_appointment is NULL, 0, 1)+
  IIF(Covid_19_Fields_Risk_information_Other_risk__please_specify_ is NULL, 0, 1)+
  IIF(Covid_19_Fields_Travel_Other_travel_detail__please_specify_ is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_Email is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_name is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_Phone is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Relationship_to_Student is NULL, 0, 1)+
  IIF(DDS_DSA_CLASS_Needs_Assessment_time is NULL, 0, 1)+
  IIF(DDS_DSA_CRN is NULL, 0, 1)+
  IIF(DDS_DSA_DSA_Application_notes is NULL, 0, 1)+
  IIF(DDS_DSA_NHS_Bursary_No is NULL, 0, 1)+
  IIF(DDS_Lockers_Lockers_Locker_Number is NULL, 0, 1)+
  IIF(DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_hours_agreed is NULL, 0, 1)+
  IIF(DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_total_cost is NULL, 0, 1)+
  IIF(DDS_PRE_ENTRY_Enrolment_Notes is NULL, 0, 1)+
  IIF(DDS_PRE_ENTRY_Notes is NULL, 0, 1)+
  IIF(DDS_Screening_and_Ed_Psych_Assessment_Feedback_appointment_time is NULL, 0, 1)+
  IIF(DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Assignment_Arrangement_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Examination_Arrangements_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_For_Information_notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Nature_of_Disability is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Presentation_Arrangements_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_QL_Disability_Fields_Disability_Allowance is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_QL_Disability_Fields_Disability_Type is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Teaching_Arrangements_Notes is NULL, 0, 1)+
  IIF(MH__WB_Counselling_Referrals_Corenet_ID is NULL, 0, 1)+
  IIF(MH__WB_MH__WB_Current_Risk_EP_Waiting_List_notes is NULL, 0, 1)+
  IIF(MH__WB_MHWB_Archive_Name_of_person_referring is NULL, 0, 1)+
  IIF(MH__WB_MHWB_Archive_Who_advised_student_of_MHWB_Service is NULL, 0, 1)+
  IIF(Preferred_name__if_different_ is NULL, 0, 1)+
  IIF(QL_Records_Management_Academic_Year is NULL, 0, 1)+
  IIF(QL_Records_Management_Applicant_Stage_Code is NULL, 0, 1)+
  IIF(QL_Records_Management_Applicant_Stage_Date_Achieved is NULL, 0, 1)+
  IIF(QL_Records_Management_Application_Academic_Year is NULL, 0, 1)+
  IIF(QL_Records_Management_Country_Of_Residence is NULL, 0, 1)+
  IIF(QL_Records_Management_Ethnic_Origin is NULL, 0, 1)+
  IIF(QL_Records_Management_Gender is NULL, 0, 1)+
  IIF(QL_Records_Management_Geo_Location is NULL, 0, 1)+
  IIF(QL_Records_Management_Nationality is NULL, 0, 1)+
  IIF(QL_Records_Management_Session_Code is NULL, 0, 1)+
  IIF(QL_Records_Management_Stage_Code is NULL, 0, 1)+
  IIF(School_Code is NULL, 0, 1) as data_cnt
 FROM INT.SRC_Maximizer_PIVOT_IndividualDetailString 
 WHERE Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])) qr_1
 ) qr_2  WHERE qr_2.rnk= 1
)
--mids.Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students])
SELECT 
  CAST(GIDC.Id as varchar(18)) AS hed__Contact__c
 ,CAST(0 as BIT) AS hed__Emergency_Contact__c
 ,CAST((SELECT RLC.ID FROM SFC.Get_ID_Contact RLC WHERE RLC.LSB_ExternalID__c= EXTR.LSB_ExternalID__c) as varchar(90)) AS hed__RelatedContact__c
 ,CAST('Current' as varchar(255)) AS hed__Status__c
 ,CAST(DICT_RL.Relationship1 as varchar(255)) AS hed__Type__c
 ,CAST(0 as BIT) AS LSB_RTP_ConsentWithdrawn__c -- new field
 ,CAST(CONCAT(CTE.Student_ID, '.', DICT_RL.Relationship1,'.Maximiser.R') as varchar(90)) AS LSB_RTP_ExternalId__c 
 ,CAST(
		CASE	
	WHEN CHARINDEX(';',IDSM.DDS_Consent_to_Share__DDS__Permission_Given_Method,1) != 0
	THEN SUBSTRING(IDSM.DDS_Consent_to_Share__DDS__Permission_Given_Method,1,CHARINDEX(';',IDSM.DDS_Consent_to_Share__DDS__Permission_Given_Method,1)-1)
	ELSE IDSM.DDS_Consent_to_Share__DDS__Permission_Given_Method
	END	as varchar(255)) AS LSB_RTP_PermissionGivenMethod__c 
 ,CAST(CONCAT(CTE.Student_ID, '.', DICT_RL.Relationship1,'.Maximiser.R') as varchar(90)) AS LSB_RTP_SourceSystemID__c 
 ,CAST('Maximiser' as varchar(255)) AS LSB_RTP_SourceSystem__c
 ,CONVERT(DATE, IDD.DDS_Consent_to_Share__DDS__Date_of_Agreement, 121) AS LSB_RTP_DateofAgreement__c
FROM Maximizer.src.Individual I
  JOIN IndividualDetails_cte CTE ON I.Id = CTE.Id 
  JOIN SFC.Get_ID_Contact GIDC ON CTE.Student_ID = GIDC.LSB_ExternalID__c
  JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDSM ON I.Id = IDSM.Id
  JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON I.Id = IDD.Id
  LEFT JOIN DST.DICT_MAXContactRelationship DICT_RL ON DICT_RL.DDS_Consent_to_Share__DDS__Relationship_to_Student= CTE.DDS_Consent_to_Share__DDS__Relationship_to_Student
  JOIN (SELECT 
               CAST(CR.Student_ID as varchar(18)) as Student_ID, 
               DCR.Relationship1,  
               CAST(CONCAT(CR.Student_ID, '.', DCR.Relationship1 , '.Maximiser') as varchar(90)) as LSB_ExternalID__c
             FROM Maximizer.src.ContactRelationship CR
              LEFT JOIN DST.DICT_MAXContactRelationship DCR ON CR.DDS_Consent_to_Share_DDS_Relationship_to_Student = DCR.DDS_Consent_to_Share__DDS__Relationship_to_Student
             WHERE CR.DDS_Consent_to_Share_DDS_Contact_name is not null) EXTR ON EXTR.Student_ID = CTE.Student_ID AND EXTR.Relationship1 = DICT_RL.Relationship1