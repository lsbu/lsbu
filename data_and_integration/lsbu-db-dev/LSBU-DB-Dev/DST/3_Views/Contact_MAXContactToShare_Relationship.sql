CREATE OR ALTER VIEW [DST].[Contact_MAXContactToShare_Relationship] AS
WITH IndividualDetails_cte AS (
 SELECT
 qr_2.* 
FROM (
SELECT 
 qr_1.*,
 ROW_NUMBER() OVER(PARTITION BY qr_1.Student_ID ORDER BY qr_1.data_cnt DESC) rnk
FROM (
 SELECT 
  *,
  IIF(ARCHIVED_FIELDS_Assessing_LEA_Name is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_DATA_PROTECTION_REQUESTS_DP_Notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Duration_of_temporary_disability is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_ED_PSYCH_old_UDFs_Ed_Psych_Invoice_number is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Email_Address_2 is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_First_language is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Lockers_Locker_notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Note_Taker_initial_hours is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Note_Taker_s_ is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Dyslexia_Tutor_date_Allocated is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Library_Support_Initial_Hours is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Library_Support_Worker is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Mentor_date_Allocated is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_MENTOR_NOTES is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_OLD_dont_use_Pre_Entry___notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Other_Support is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Old_fields_Tutor_Support_Notes is NULL, 0, 1)+
  IIF(ARCHIVED_FIELDS_Recommendations_or_Comments is NULL, 0, 1)+
  IIF(Area_Of_Study_Course_Code_ is NULL, 0, 1)+
  IIF(Attend_Mode is NULL, 0, 1)+
  IIF(Confidential_Information is NULL, 0, 1)+
  IIF(Course_In_Full is NULL, 0, 1)+
  IIF(Covid_19_Fields_Coronavirus_questions_Other_coronavirus_detail__please_specify_ is NULL, 0, 1)+
  IIF(Covid_19_Fields_Covid_plan_created_notes is NULL, 0, 1)+
  IIF(Covid_19_Fields_Further_information_Anything_else_to_know_before_we_book_appointment is NULL, 0, 1)+
  IIF(Covid_19_Fields_Risk_information_Other_risk__please_specify_ is NULL, 0, 1)+
  IIF(Covid_19_Fields_Travel_Other_travel_detail__please_specify_ is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_Email is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_name is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Contact_Phone is NULL, 0, 1)+
  IIF(DDS_Consent_to_Share__DDS__Relationship_to_Student is NULL, 0, 1)+
  IIF(DDS_DSA_CLASS_Needs_Assessment_time is NULL, 0, 1)+
  IIF(DDS_DSA_CRN is NULL, 0, 1)+
  IIF(DDS_DSA_DSA_Application_notes is NULL, 0, 1)+
  IIF(DDS_DSA_NHS_Bursary_No is NULL, 0, 1)+
  IIF(DDS_Lockers_Lockers_Locker_Number is NULL, 0, 1)+
  IIF(DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_hours_agreed is NULL, 0, 1)+
  IIF(DDS_LSBU_funded_NMH_Note_Taking_Support_Note_taking_total_cost is NULL, 0, 1)+
  IIF(DDS_PRE_ENTRY_Enrolment_Notes is NULL, 0, 1)+
  IIF(DDS_PRE_ENTRY_Notes is NULL, 0, 1)+
  IIF(DDS_Screening_and_Ed_Psych_Assessment_Feedback_appointment_time is NULL, 0, 1)+
  IIF(DDS_Screening_and_Ed_Psych_Assessment_Time_of_EP_Assessment is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Assignment_Arrangement_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Examination_Arrangements_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_For_Information_notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Nature_of_Disability is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Presentation_Arrangements_Notes is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_QL_Disability_Fields_Disability_Allowance is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_QL_Disability_Fields_Disability_Type is NULL, 0, 1)+
  IIF(DDS_Support_Arrangements_Teaching_Arrangements_Notes is NULL, 0, 1)+
  IIF(MH__WB_Counselling_Referrals_Corenet_ID is NULL, 0, 1)+
  IIF(MH__WB_MH__WB_Current_Risk_EP_Waiting_List_notes is NULL, 0, 1)+
  IIF(MH__WB_MHWB_Archive_Name_of_person_referring is NULL, 0, 1)+
  IIF(MH__WB_MHWB_Archive_Who_advised_student_of_MHWB_Service is NULL, 0, 1)+
  IIF(Preferred_name__if_different_ is NULL, 0, 1)+
  IIF(QL_Records_Management_Academic_Year is NULL, 0, 1)+
  IIF(QL_Records_Management_Applicant_Stage_Code is NULL, 0, 1)+
  IIF(QL_Records_Management_Applicant_Stage_Date_Achieved is NULL, 0, 1)+
  IIF(QL_Records_Management_Application_Academic_Year is NULL, 0, 1)+
  IIF(QL_Records_Management_Country_Of_Residence is NULL, 0, 1)+
  IIF(QL_Records_Management_Ethnic_Origin is NULL, 0, 1)+
  IIF(QL_Records_Management_Gender is NULL, 0, 1)+
  IIF(QL_Records_Management_Geo_Location is NULL, 0, 1)+
  IIF(QL_Records_Management_Nationality is NULL, 0, 1)+
  IIF(QL_Records_Management_Session_Code is NULL, 0, 1)+
  IIF(QL_Records_Management_Stage_Code is NULL, 0, 1)+
  IIF(School_Code is NULL, 0, 1) as data_cnt
 FROM INT.SRC_Maximizer_PIVOT_IndividualDetailString
 WHERE Student_ID NOT IN (SELECT Student_ID FROM [DST].[DICT_Invalid_Students]))  qr_1
 ) qr_2  WHERE qr_2.rnk= 1
)

SELECT DISTINCT
	--null as Id
	--null as IsDeleted
	--null as MasterRecordId
	CAST(null as varchar(18)) as AccountId,
	CAST(CR.DDS_Consent_to_Share_DDS_Contact_name as varchar(80)) as LastName,
	CAST(NULL as varchar(40)) as FirstName,
	--CAST(null as varchar(40)) as Salutation,
	--CAST(null as varchar(121)) as Name,
	--CAST(null as varchar(255)) as OtherStreet,
	--CAST(null as varchar(40)) as OtherCity,
	--CAST(null as varchar(80)) as OtherState,
	--CAST(null as varchar(20)) as OtherPostalCode,
	--CAST(null as varchar(80)) as OtherCountry,
	--CAST(null as decimal(18, 0)) as OtherLatitude,
	--CAST(null as decimal(18, 0)) as OtherLongitude,
	--CAST(null as varchar(40)) as OtherGeocodeAccuracy,
	--CAST(null as varchar(400)) as OtherAddress,
	--STUFF(CONCAT(' ' + NULLIF(QLOH.QL_Add1, ''),' ' + NULLIF(QLOH.QL_Add2, ''),' ' + NULLIF(QLOH.QL_Add3, '')),1,1,'') as MailingStreet,
	CAST(null as varchar(255)) as MailingStreet,
	--SUBSTRING(QLOH.QL_Add4,1,40) as MailingCity,
	CAST(null as varchar(40)) as MailingCity,
	--CAST(null as varchar(80)) as MailingState,
	--QLOH.QL_Postcode as MailingPostalCode,
	CAST(null as varchar(20)) as MailingPostalCode,
	--DAC.[MailingCountry] as MailingCountry,
	CAST(null as varchar(80)) as MailingCountry, 
	--CAST(null as decimal(18, 0)) as MailingLatitude,
	--CAST(null as decimal(18, 0)) as MailingLongitude,
	--CAST(null as varchar(40)) as MailingGeocodeAccuracy,
	--CAST(null as varchar(400)) as MailingAddress,
	CAST(null as varchar(40)) as Phone,
	CAST(null as varchar(40)) as Fax,
	CAST(CR.DDS_Consent_to_Share_DDS_Contact_Phone as varchar(40)) as MobilePhone,
	CAST(CR.Home as varchar(40)) as HomePhone,
	CAST(null as varchar(40)) as OtherPhone,
	CAST(null as varchar(40)) as AssistantPhone,
	CAST(null as varchar(18)) as ReportsToId,
	--QLOH.QL_PersonalEmail as Email, --PROD Email !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAST(NULL as varchar(80)) as Email,--HashEmail -------------
	--null as Email,
	CAST(NULL as varchar(128)) as Title,
	CAST(null as varchar(80)) as Department, ---QLOH.QL_DepartmentName as Department, - lookup to Department ?
	CAST(null as varchar(40)) as AssistantName,
	CAST(null as varchar(255)) as LeadSource,
	TRY_CONVERT(date, NULL, 103 ) as Birthdate,
	CAST(null as varchar(max)) as Description, --QLOH.QL_Description source column don't exists
	--CAST(null as varchar(18)) as OwnerId,--QLOH.QL_Origin as OwnerId, --no applicable 
	--CAST(null as bit) as HasOptedOutOfEmail,
	--CAST(null as bit) as HasOptedOutOfFax,
	--CAST(null as bit) as DoNotCall,
	CONVERT(DATE,IDD.DDS_Consent_to_Share__DDS__Date_of_Agreement , 121) as CreatedDate,
	--CAST(null as varchar(18)) as CreatedById,
	CONVERT(DATE,IDD.DDS_Consent_to_Share__DDS__Date_of_Agreement , 121) as LastModifiedDate,
	--CAST(null as varchar(18)) as LastModifiedById,
	--CAST(null as datetime) as SystemModstamp,
	--CAST(null as date) as LastActivityDate,
	CAST(null as datetime) as LastCURequestDate,
	CAST(null as datetime) as LastCUUpdateDate,
	CAST(null as datetime) as LastViewedDate,
	--CAST(null as datetime) as LastReferencedDate,
	--CAST(null as varchar(255)) as EmailBouncedReason,
	--CAST(null as datetime) as EmailBouncedDate,
	--CAST(null as bit) as IsEmailBounced,
	--CAST(null as varchar(255)) as PhotoUrl,
	CAST(null as varchar(20)) as Jigsaw,
	--CAST(null as varchar(20)) as JigsawContactId,
	--CAST(null as varchar(18)) as IndividualId,
	CAST(null as varchar(18)) as Primary_Academic_Program__c,
	CAST(null as varchar(18)) as Primary_Department__c,
	CAST(null as varchar(18)) as Primary_Educational_Institution__c,
	CAST(null as varchar(18)) as Primary_Sports_Organization__c,
	CAST(ISNULL(cfg.fx_HashEmail(CR.DDS_Consent_to_Share_DDS_Contact_Email, CR.Student_ID), 'emailnotavailable@dummy.com') as varchar(80)) as hed__AlternateEmail__c,
	CAST(NULL as varchar(255)) as hed__Citizenship__c, --QLOH.QL_Nationality as hed__Citizenship__c, --DICTIONARY need 
	CAST(NULL as varchar(255)) as hed__Country_of_Origin__c, --QLOH.QL_Domicile as hed__Country_of_Origin__c, --DICTIONARY need 
	CAST(null as varchar(18)) as hed__Current_Address__c,
	--CAST(null as bit) as hed__Deceased__c,
	--CAST(null as bit) as hed__Do_Not_Contact__c,
	CAST(null as varchar(255)) as hed__Ethnicity__c,
	--CAST(null as bit) as hed__Exclude_from_Household_Formal_Greeting__c,
	--CAST(null as bit) as hed__Exclude_from_Household_Informal_Greeting__c,
	--CAST(null as bit) as hed__Exclude_from_Household_Name__c,
	--CAST(null as bit) as hed__FERPA__c,
	--CAST(null as bit) as hed__Financial_Aid_Applicant__c,
	CAST(NULL as varchar(255)) as hed__Gender__c,
	CAST(null as varchar(max)) as hed__HIPAA_Detail__c,
	--CAST(null as bit) as hed__HIPAA__c,
	CAST(null as varchar(max)) as hed__Military_Background__c,
	--CAST(null as bit) as hed__Military_Service__c,
	CAST(null as varchar(4099)) as hed__Naming_Exclusions__c,
	--If QL_MobilePhone is not NULL then 'Mobile' else If QL_Phone is not NULL then 'Other'
	--QLOH.QL_MobileNo,QLOH.QL_Phone,
	CAST(CASE 
			WHEN CR.DDS_Consent_to_Share_DDS_Contact_Phone IS NOT NULL THEN 'Mobile' 
			WHEN CR.Home IS NOT NULL THEN 'Home'
			ELSE NULL 
		  END as varchar(255)) as hed__PreferredPhone__c,
	--If QL_PersonalEmail is not NULL then 'Alternate' else If QL_LSBUEmail is not NULL then 'University'
	CAST('Personal Email' as varchar(255)) as hed__Preferred_Email__c, 
	--null as hed__Preferred_Email__c,
	CAST(null as varchar(255)) as hed__Primary_Address_Type__c,
	CAST(null as varchar(18)) as hed__Primary_Household__c,
	CAST(null as varchar(18)) as hed__Primary_Organization__c,
	CAST(null as varchar(255)) as hed__Religion__c,
	CAST(null as varchar(255)) as hed__Secondary_Address_Type__c,
	CAST(null as varchar(12)) as hed__Social_Security_Number__c,
	--QLOH.QL_LSBUEmail as hed__UniversityEmail__c,--Production
	--QLOH.QL_LSBUEmail, QLOH.QL_StudentID,'a',
	CAST(NULL as varchar(80)) as hed__UniversityEmail__c,--HashEmail
	CAST(null as varchar(80)) as hed__WorkEmail__c,
	CAST(CR.Work as varchar(40)) as hed__WorkPhone__c,
	CAST(null as varchar(1300)) as hed__Work_Address__c,
	--CAST(null as bit) as hed__is_Address_Override__c,
	CAST(null as varchar(18)) as Primary_Student_Organization__c,
	CAST(null as varchar(255)) as hed__Dual_Citizenship__c,
	CAST(null as varchar(4099)) as hed__Race__c,
	CAST(null as varchar(18)) as hed__Primary_Language__c,
	CAST(NULL as varchar(255)) as hed__Chosen_Full_Name__c,
	CAST(null as varchar(255)) as hed__Citizenship_Status__c,
	CAST(null as date) as hed__Date_Deceased__c,
	CAST(null as varchar(80)) as hed__Former_First_Name__c,
	CAST(null as varchar(80)) as hed__Former_Last_Name__c,
	CAST(null as varchar(80)) as hed__Former_Middle_Name__c,
	CAST(null as varchar(80)) as hed__Mailing_County__c,
	CAST(null as varchar(80)) as hed__Other_County__c,
	CAST(NULL as varchar(255)) as LSB_ChannelOfPreference__c,
	CAST('Maximiser' as varchar(255)) as LSB_SourceSystem__c,
	CAST(CONCAT(CR.Student_ID, '.', DCR.Relationship1 , '.Maximiser') as varchar(90)) as LSB_SourceSystemID__c,
	CAST(CONCAT(CR.Student_ID, '.', DCR.Relationship1 , '.Maximiser') as varchar(90)) as LSB_ExternalID__c,
	CAST(NULL as varchar(255)) as LSB_CON_StudentStatus__c,--QLOH.QL_StudentStatus as LSB_CON_StudentStatus__c,
	CAST(NULL as varchar(255)) as LSB_CON_Disability__c,
	CAST(NULL as varchar(90)) as LSB_CON_UCASID__c,
	CAST('External to LSBU' as varchar(255)) as LSB_CON_CurrentRole__c,
	CAST(NULL as varchar(255)) as LSB_CON_DisabilityAllowance__c,
	CAST(NULL as varchar(255)) as LSB_CON_DDSMarking__c,
	CAST(NULL as varchar(255)) as LSB_CON_CareLeavers__c,
	CAST(NULL as varchar(255)) as LSB_CON_TypeOfCourseEnrolledIn__c,
	CONVERT(varchar(255), NULL) AS LSB_CON_YearOfStudy__c
FROM Maximizer.src.ContactRelationship CR
 JOIN IndividualDetails_cte CTE_IND ON CR.Student_ID = CTE_IND.Student_ID
 LEFT JOIN DST.DICT_MAXContactRelationship DCR ON CR.DDS_Consent_to_Share_DDS_Relationship_to_Student = DCR.DDS_Consent_to_Share__DDS__Relationship_to_Student
 LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailDate IDD ON CTE_IND.ID = IDD.ID
WHERE CR.DDS_Consent_to_Share_DDS_Contact_name is not null
 AND CAST(CONCAT(CR.Student_ID, '.', DCR.Relationship1 , '.Maximiser') as varchar(90)) NOT IN (select LSB_ExternalID__c from SFC.Get_ID_Contact WHERE LSB_ExternalID__c IS NOT NULL)