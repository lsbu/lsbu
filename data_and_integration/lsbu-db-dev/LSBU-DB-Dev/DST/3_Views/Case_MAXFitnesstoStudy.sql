CREATE OR ALTER VIEW [DST].[Case_MAXFitnesstoStudy] AS
SELECT
	 CAST('Maximiser' as varchar(255)) as Origin
	,CAST(NULL as varchar(18)) as OwnerId
	,CAST(RT.Id as varchar(18)) as RecordTypeId
	,CAST(GIDC.Id as varchar(18)) as ContactId
	,CAST(CbyId.Id as varchar(18)) as CreatedById
	,CAST((Select MAX(Fitness_to_Study_Initial_referral_Date_of_first_referral) from  INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD WHERE C.CaseId = CDD.CaseId) as datetime) as CreatedDate
	,CAST(CONCAT(C.CaseId, '.FitnesstoStudy') as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST('Maximiser' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(C.CaseNumber as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST('Medium' as varchar(255)) as Priority
	,CAST(LbyId.Id as varchar(18)) as LastModifiedById
	,CAST((Select MAX(Fitness_to_Study_Initial_referral_Date_of_first_referral) from  INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD WHERE C.CaseId = CDD.CaseId) as datetime) as Lastmodifieddate
	,CAST(CASE
			WHEN DATEDIFF(MM, CDD.Fitness_to_Study_Initial_referral_Date_of_first_referral, GETDATE()) < 12 THEN 'Further Actions Needed'
			ELSE 'Closed' 
		END as varchar(255)) as Status
	,CAST(Concat(GIDC.Name, ' - ', IDS.Student_ID, ' - Fitness to Study') as varchar(255)) as Subject
	,CAST(null as varchar(max)) as Description
	,CAST(
			CASE WHEN GIDC.LSB_CON_CurrentRole__c LIKE 'Student%' then 'Student'
			 WHEN GIDC.LSB_CON_CurrentRole__c = 'Graduate' then 'Student'
			ELSE GIDC.LSB_CON_CurrentRole__c
			
			END as varchar(255)) as LSB_CAS_ContactRole__c   -- New field to be mapped
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_FitnessToStudy'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN Maximizer.src.Case_CaseCompanyIndividual CCI ON C.CaseId = CCI.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON CCI.Id = IDS.Id
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON CCI.Id = IDLM.Id
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
LEFT JOIN DST.DICT_MHWB_Adviser DTA ON REPLACE(REPLACE(REPLACE(REPLACE(IDLM.MH__WB_MHWB_Adviser, 'Stephen Anderson', ''), 'Tayla Vella', ''), 'Makeba Garraway', ''), 'Ben Walford', '') = DTA.SourceValue
LEFT JOIN SFC.Get_ID_User OId ON DTA.DestinationValue = SUBSTRING(OId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND CONCAT(C.CaseId, '.FitnesstoStudy') NOT IN (select LSB_CAS_ExternalID__c from SFC.Get_ID_Case WHERE LSB_CAS_ExternalID__c IS NOT NULL)