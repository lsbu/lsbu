--View name changed to Address_Student to match naming converntion of other dst schema views
/*CREATE OR ALTER VIEW [DST].[Contact_Address] AS
SELECT DISTINCT
	--null [Id],
	--[OwnerId],
	--[IsDeleted],
null as [Name], --[varchar](80) NOT NULL,
	--[CreatedDate] [datetime] NOT NULL,
	--[CreatedById] [varchar](18) NOT NULL,
	--[LastModifiedDate] [datetime] NOT NULL,
	--[LastModifiedById] [varchar](18) NOT NULL,
	--[SystemModstamp] [datetime] NOT NULL,
	--[LastViewedDate] [datetime] NULL,
	--[LastReferencedDate] [datetime] NULL,
QLSA.QL_AddressType as [hed__Address_Type__c],-- [varchar](255) NULL,
null as [hed__Default_Address__c],-- [bit] NOT NULL,
null as [hed__Formula_MailingAddress__c],-- [varchar](1300) NULL,
null as [hed__Formula_MailingStreetAddress__c],-- [varchar](1300) NULL,
null as [hed__Geolocation__Latitude__s],-- [real] NULL,
null as [hed__Geolocation__Longitude__s],-- [real] NULL,
null as [hed__Geolocation__c],-- [varchar](255) NULL,
null as [hed__Latest_End_Date__c],-- [datetime] NULL,
null as [hed__Latest_Start_Date__c],-- [datetime] NULL,
SUBSTRING(QLSA.QL_Add4,1,40) as [hed__MailingCity__c],-- [varchar](255) NULL,
DAC.[MailingCountry] as [hed__MailingCountry__c],-- [varchar](255) NULL,
QLSA.QL_Postcode as [hed__MailingPostalCode__c],-- [varchar](255) NULL,
null as [hed__MailingState__c],-- [varchar](255) NULL,
STUFF(CONCAT(' ' + NULLIF(QLSA.QL_Add1, ''),' ' + NULLIF(QLSA.QL_Add2, ''),' ' + NULLIF(QLSA.QL_Add3, '')),1,1,'') as [hed__MailingStreet__c],-- [varchar](255) NULL,
null as [hed__MailingStreet2__c],-- [varchar](255) NULL,
null as [hed__Parent_Account__c],-- [varchar](18) NULL,
null as [hed__Seasonal_End_Day__c],-- [varchar](255) NULL,
null as [hed__Seasonal_End_Month__c],-- [varchar](255) NULL,
null as [hed__Seasonal_End_Year__c],-- [real] NULL,
null as [hed__Seasonal_Start_Day__c],-- [varchar](255) NULL,
null as [hed__Seasonal_Start_Month__c],-- [varchar](255) NULL,
null as [hed__Seasonal_Start_Year__c],-- [real] NULL,

GIDC.Id as [hed__Parent_Contact__c],-- [varchar](18) NULL,

QLSA.LSB_ExternalID as [LSB_ADS_ExternalID__c],-- [varchar](90) NULL,
QLSA.QL_AddressID as [LSB_ADS_SourceSystemID__c],-- [varchar](90) NULL,
QLSA.SrcSystem as [LSB_ADS_SourceSystem__c] -- [varchar](255) NULL

FROM [INT].[SRC_Student_Address] as QLSA
LEFT JOIN [DST].[DICT_QL_Add5_Country] DAC ON DAC.[QL_Add5] = QLSA.QL_Add5
LEFT JOIN SFC.Get_ID_Contact GIDC ON GIDC.LSB_ExternalID__c = QLSA.QL_StudentID
WHERE ( QLSA.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select R.LSB_ExternalID__c from DST.SF_Reject_Contact R WHERE QLSA.LSB_ExternalID = R.LSB_ExternalID__c)
)

GO*/


