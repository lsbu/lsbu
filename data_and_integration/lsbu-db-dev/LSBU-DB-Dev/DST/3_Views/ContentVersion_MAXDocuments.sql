CREATE OR ALTER VIEW [DST].[ContentVersion_MAXDocuments]
AS
SELECT
	CreatedDate
	,LastmodifiedDate
	,createdbyId
	,LastModifiedById
	,FirstPublishLocationId
	,LSB_CVR_DocumentType__c
	,LSB_CVR_ExternalId__c
	,LSB_CVR_SourceSystemId__c
	,LSB_CVR_SourceSystem__c
	,SharingOption
	,SharingPrivacy
	,PathOnClient
	,Title
	,VersionData
FROM
(------------------------------------------------Support Profile Attachments-----------------------------------------------------------------------------
SELECT 
CAST(ID.Date as datetime) AS CreatedDate
,CAST(ID.Date as datetime) AS LastmodifiedDate
,CAST(CbyId.Id as Varchar(18)) AS CreatedById						-- New Field
,CAST(CbyId.Id as Varchar(18)) AS LastModifiedById				-- New Field
--,CAST(
--		REPLACE(
--				CASE
--					WHEN ID.Extension IN ('.PDF 1','.PDF 2','.pdf 5') THEN '.PDF'
--					WHEN ID.Extension IN ('.rtf_tmp1443','.rtf_tmp3DFF') THEN '.rtf'
--					ELSE ID.Extension
--				END, '.',''
--				)
--	AS VARCHAR(255)) AS FileExtension
--,CAST(
--		REPLACE(
--				CASE
--					WHEN ID.Extension IN ('.PDF 1','.PDF 2','.pdf 5') THEN '.PDF'
--					WHEN ID.Extension IN ('.rtf_tmp1443','.rtf_tmp3DFF') THEN '.rtf'
--					ELSE ID.Extension
--				END, '.',''
--				)
--	AS VARCHAR(255)) AS FileTYPE
,CAST(GISP.Id as varchar(18)) FirstPublishLocationId
,CAST(DADT.SFDocumentType AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(IDS.Student_ID,'.',ID.DocumentId,'.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID,ID.DocumentId ORDER BY IDS.Student_ID,ID.DocumentId)) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(ID.DocumentId AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('Maximiser' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST(NULL AS VARCHAR(255)) SharingPrivacy
,CAST(
		CONCAT(
				ID.NAME,
				CASE
					WHEN ID.Extension IN ('.PDF 1','.PDF 2','.pdf 5') THEN '.PDF'
					WHEN ID.Extension IN ('.rtf_tmp1443','.rtf_tmp3DFF') THEN '.rtf'
					ELSE ID.Extension
				END
				) 
	AS VARCHAR(500)) AS PathOnClient
,CAST(CONCAT(CONVERT(varchar,ID.Date,20),' - ',ID.NAME) AS VARCHAR(255)) AS Title
,REPLACE(IDD.DocData, CHAR(10), '')  as VersionData
FROM [Maximizer].[src].[Individual_Document] ID
INNER JOIN [Maximizer].[src].[Individual_Document_DocData] IDD on ID.DocumentId = IDD.DocumentId
INNER JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON IDS.Id = ID.IndividualId 
LEFT JOIN SFC.Get_ID_SupportProfile GISP ON GISP.LSB_SUE_ExternalID__c = CONCAT(IDS.Student_ID,'.SP')
LEFT JOIN DST.DICT_AttachmentDocumentType DADT on  DADT.MaximiserValues = Id.Category
LEFT JOIN (SELECT OId.Username,OId.Id  FROM SFC.Get_ID_User OId
		   INNER JOIN SFC.Get_ID_Profile GIP ON GIP.Id = OId.ProfileId 
		   WHERE GIP.Name NOT IN ('Offer Holder','Student')) CbyId ON Id.Creator = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))

UNION ALL
------------------------------------------------Case Attachments-----------------------------------------------------------------------------
SELECT 
CAST(CD.Date as datetime) AS CreatedDate
,CAST(CD.Date as datetime) AS LastmodifiedDate
,CAST(CbyId.Id as Varchar(18)) AS CreatedbyId					-- New Field
,CAST(CbyId.Id as Varchar(18)) AS LastModifiedById				-- New Field
--,CAST(
--		REPLACE(
--				CASE
--					WHEN CD.Extension IN ('.PDF 1','.PDF 2','.pdf 5') THEN '.PDF'
--					WHEN CD.Extension IN ('.rtf_tmp1443','.rtf_tmp3DFF') THEN '.rtf'
--					ELSE CD.Extension
--				END, '.',''
--				)
--	AS VARCHAR(255)) AS FileExtension
--,CAST(
--		REPLACE(
--				CASE
--					WHEN CD.Extension IN ('.PDF 1','.PDF 2','.pdf 5') THEN '.PDF'
--					WHEN CD.Extension IN ('.rtf_tmp1443','.rtf_tmp3DFF') THEN '.rtf'
--					ELSE CD.Extension
--				END, '.',''
--				)
--	AS VARCHAR(255)) AS FileTYPE
,CAST(GIC.Id as varchar(18)) FirstPublishLocationId
,CAST(DADT.SFDocumentType AS VARCHAR(255)) AS LSB_CVR_DocumentType__c
,CAST(CONCAT(CD.CaseId,'.',CD.DocumentId,'.',ROW_NUMBER() OVER (PARTITION BY CD.CaseId,CD.DocumentId ORDER BY CD.CaseId,CD.DocumentId)) AS VARCHAR(255)) AS LSB_CVR_ExternalId__c
,CAST(CD.DocumentId AS VARCHAR(255))LSB_CVR_SourceSystemId__c
,CAST('Maximiser' as Varchar(255)) as LSB_CVR_SourceSystem__c
,CAST(NULL AS VARCHAR(255)) as SharingOption
,CAST(NULL AS VARCHAR(255)) SharingPrivacy
,CAST(
		CONCAT(
				CD.NAME,
				CASE
					WHEN CD.Extension IN ('.PDF 1','.PDF 2','.pdf 5') THEN '.PDF'
					WHEN CD.Extension IN ('.rtf_tmp1443','.rtf_tmp3DFF') THEN '.rtf'
					ELSE CD.Extension
				END
				) 
	AS VARCHAR(500)) AS PathOnClient
,CAST(CONCAT(CONVERT(varchar,CD.Date,20),' - ',CD.NAME) AS VARCHAR(255)) AS Title
,REPLACE(CDD.DocData, CHAR(10), '')  as VersionData
FROM [Maximizer].[src].[Case_Document] CD
INNER JOIN [Maximizer].[src].[Case_Document_DocData] CDD on CD.DocumentId = CDD.DocumentId
INNER JOIN [Maximizer].src.[Case] C ON C.CaseId = CD.CaseId
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_SourceSystemID__c = C.CaseNumber
LEFT JOIN DST.DICT_AttachmentDocumentType DADT on  DADT.MaximiserValues = CD.Category
LEFT JOIN (SELECT OId.Username,OId.Id  FROM SFC.Get_ID_User OId
		   INNER JOIN SFC.Get_ID_Profile GIP ON GIP.Id = OId.ProfileId 
		   WHERE GIP.Name NOT IN ('Offer Holder','Student')) CbyId ON Cd.Creator = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
) AS T1
WHERE T1.LSB_CVR_ExternalId__c NOT IN (select LSB_CVR_ExternalId__c from sfc.Get_ID_ContentVersion WHERE LSB_CVR_ExternalID__c IS NOT NULL)