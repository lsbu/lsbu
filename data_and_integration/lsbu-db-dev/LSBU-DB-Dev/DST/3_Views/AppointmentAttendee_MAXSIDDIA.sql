CREATE OR ALTER VIEW [DST].[AppointmentAttendee_MAXSIDDIA] AS
WITH CTE1 AS (
	SELECT * FROM (
	SELECT ROW_NUMBER() OVER(PARTITION BY LSB_AEE_SourceSystemId__c ORDER BY LSB_AEE_AppointmentDateTime__c, sfal__Appointment__c) as RNK, * FROM(
	SELECT
	--'1' AS RNK,
	--CAST(NULL as varchar(18)) AS CreatedById
	--,CAST(NULL as varchar(18)) AS LastModifiedById
	CAST(APPT.DIA_DATE as datetime) AS LSB_AEE_AppointmentDateTime__c
	--,CAST(APPT.DIA_DATE as datetime) AS LSB_AEE_AppointmentStartDate__c
	,CAST(CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC, '.Owner','.',ROW_NUMBER() OVER(PARTITION BY APPT.DIA_MEET,APPT.DIA_SPTC ORDER BY APPT.DIA_MEET,APPT.DIA_SPTC)) as varchar(80)) AS LSB_AEE_ExternalId__c -- CHANGED
	,CAST(NULL as varchar(255)) AS LSB_AEE_PausedFlowInterviewGUID__c
	,CAST(NULL as datetime) AS LSB_AEE_Reminder15MinTriggerDate__c
	,CAST('Maximiser' as varchar(80)) AS LSB_AEE_SourceSystem__c
	,CAST(CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC) as varchar(80)) AS LSB_AEE_SourceSystemId__c
	,CAST(NULL as varchar(80)) AS Name
	,CAST(NULL as varchar(18)) AS sfal__AdviseeRecord__c
	,CAST(GIA.Id as varchar(18)) AS sfal__Appointment__c
	,CAST(ISNULL(OId.Id,OId2.Id )as varchar(18)) AS sfal__Attendee__c --changed
	,CAST(NULL as varchar(255)) AS sfal__Comments__c
	,CAST('Organizer' as varchar(255)) AS sfal__Role__c
	,CAST(CASE
				WHEN APPT.DIA_ATND = 'N' THEN 'No Show'
				WHEN APPT.DIA_ATND = 'Y' THEN 'Checked In'
				ELSE 'Attending'
			END as varchar(255)
		) AS sfal__Status__c
	,CAST(NULL as varchar(255)) AS sfal__StatusComments__c
	,CAST(NULL as varchar(18)) AS sfal__StatusUpdatedBy__c
	,CAST(NULL as datetime) AS sfal__StatusUpdatedDate__c
	FROM  [SID].[dbo].[DIA] APPT
	INNER JOIN SFC.Get_ID_Appointment GIA ON CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC) = GIA.LSB_ANT_ExternalId__c AND GIA.sfal__EndDateTime__c < getdate()
	--INNER JOIN SFC.Get_ID_User OId
	--		ON 
	--		CASE
	--			WHEN APPT.DIA_SPTC = SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) AND OId.Id is not null THEN 1
	--			WHEN SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) = 'ohallord' AND OId.Id is not null THEN 1
	--			ELSE 0
	--		END =1
	--INNER JOIN SFC.Get_ID_Profile GIP ON GIP.Id = OId.ProfileId AND GIP.Name NOT IN ('Offer Holder','Student')
	---------------------------------------------------------------------------------------------------------------------------CHANGE
	LEFT JOIN (SELECT GIU.* FROM SFC.Get_ID_User GIU
			INNER JOIN SFC.Get_ID_Profile GIP ON GIP.Id = GIU.ProfileId AND GIP.Name NOT IN ('Offer Holder','Student')) OId
			ON  APPT.DIA_SPTC = SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) AND OId.Id is not null AND OID.IsActive = 1
	LEFT JOIN (SELECT GIU2.* FROM SFC.Get_ID_User GIU2
				INNER JOIN SFC.Get_ID_Profile GIP2 ON GIP2.Id = GIU2.ProfileId AND GIP2.Name NOT IN ('Offer Holder','Student')) OId2
			ON  SUBSTRING(OId2.Username,1,(CHARINDEX('@',OId2.Username)-1)) = 'ohallord' AND OId2.Id is not null 
	---------------------------------------------------------------------------------------------------------------------------/CHANGE
	LEFT JOIN [SID].[dbo].[SPT] SPT ON SPT.SPT_TMSC = DIA_SPTC 
	LEFT JOIN [SID].[dbo].[TMS] TMS ON SPT.SPT_TMSC = TMS.TMS_CODE
	INNER JOIN [SID].[dbo].[GRP] GRP ON APPT.DIA_BRQC = GRP.GRP_CODE
	WHERE APPT.DIA_SPTC IS NOT NULL
	AND APPT.DIA_CCNC IS NOT NULL
	AND APPT.DIA_MEET IS NOT NULL
	AND GRP.GRP_NAME IN ('Casework',
	'Counselling',
	'Covid 19 Support',
	'Disability & Dyslexia support',
	'Disabled Student Allowance',
	'EP Feedback',
	'Halls Students',
	'Havering Appointment',
	'Havering MHWB',
	'HSC Students',
	'Mental Health & Wellbeing',
	'MHWB Telephone Appointment',
	'Pre Entry Support',
	'Quick Query',
	'SPLD Screening Assessment',
	'Support Arrangements')) AS t3 WHERE sfal__Attendee__c IS NOT NULL

	UNION ALL

	---------------------------------Student------------------------------------
	
	SELECT ROW_NUMBER() OVER(PARTITION BY LSB_AEE_ExternalId__c ORDER BY LSB_AEE_AppointmentDateTime__c, sfal__Appointment__c) as RNK, * FROM(
	SELECT
	--CAST(NULL as varchar(18)) AS CreatedById
	--,CAST(NULL as varchar(18)) AS LastModifiedById
	CAST(APPT.DIA_DATE as datetime) AS LSB_AEE_AppointmentDateTime__c
	--,CAST(APPT.DIA_DATE as datetime) AS LSB_AEE_AppointmentStartDate__c
	,CAST(CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC, '.',APPT.DIA_CCNC, '.Student') as varchar(80)) AS LSB_AEE_ExternalId__c  --Changed
	,CAST(NULL as varchar(255)) AS LSB_AEE_PausedFlowInterviewGUID__c
	,CAST(NULL as datetime) AS LSB_AEE_Reminder15MinTriggerDate__c
	,CAST('Maximiser' as varchar(80)) AS LSB_AEE_SourceSystem__c
	,CAST(CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC, '.',APPT.DIA_CCNC) as varchar(80)) AS LSB_AEE_SourceSystemId__c
	,CAST(NULL as varchar(80)) AS Name
	,CAST(GICS.Id as varchar(18)) AS sfal__AdviseeRecord__c
	,CAST(GIA.Id as varchar(18)) AS sfal__Appointment__c
	,CAST(OId.Id as varchar(18)) AS sfal__Attendee__c
	,CAST(NULL as varchar(255)) AS sfal__Comments__c
	,CAST('Attendee' as varchar(255)) AS sfal__Role__c
	,CAST(CASE
				WHEN APPT.DIA_ATND = 'N' THEN 'No Show'
				WHEN APPT.DIA_ATND = 'Y' THEN 'Checked In'
				ELSE 'Attending'
			END as varchar(255)
		) AS sfal__Status__c
	,CAST(NULL as varchar(255)) AS sfal__StatusComments__c
	,CAST(NULL as varchar(18)) AS sfal__StatusUpdatedBy__c
	,CAST(NULL as datetime) AS sfal__StatusUpdatedDate__c
	FROM  [SID].[dbo].[DIA] APPT
	INNER JOIN SFC.Get_ID_Appointment GIA ON CONCAT(APPT.DIA_MEET,'.',APPT.DIA_SPTC) = GIA.LSB_ANT_ExternalId__c AND GIA.sfal__EndDateTime__c < getdate()
	INNER JOIN SFC.Get_ID_User OId ON DIA_CCNC = OId.LSB_UER_ExternalID__c
	LEFT JOIN SFC.Get_ID_Contact GIC ON APPT.DIA_CCNC = GIC.LSB_ExternalID__c
	LEFT JOIN SFC.Get_ID_Case GICS ON GIC.Id = GICS.ContactId AND GICS.RecordTypeId = (select top 1 Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord')
	LEFT JOIN [SID].[dbo].[SPT] SPT ON SPT.SPT_TMSC = DIA_SPTC 
	LEFT JOIN [SID].[dbo].[TMS] TMS ON SPT.SPT_TMSC = TMS.TMS_CODE
	INNER JOIN [SID].[dbo].[GRP] GRP ON APPT.DIA_BRQC = GRP.GRP_CODE
	WHERE APPT.DIA_SPTC IS NOT NULL
	AND APPT.DIA_CCNC IS NOT NULL
	AND APPT.DIA_MEET IS NOT NULL
	AND GRP.GRP_NAME IN ('Casework',
	'Counselling',
	'Covid 19 Support',
	'Disability & Dyslexia support',
	'Disabled Student Allowance',
	'EP Feedback',
	'Halls Students',
	'Havering Appointment',
	'Havering MHWB',
	'HSC Students',
	'Mental Health & Wellbeing',
	'MHWB Telephone Appointment',
	'Pre Entry Support',
	'Quick Query',
	'SPLD Screening Assessment',
	'Support Arrangements')) AS T1 
	) AS T2	WHERE T2.RNK = 1),

CTE2 AS (
	select sfal__Appointment__c from CTE1 where sfal__Appointment__c in (
	select sfal__Appointment__c
	from CTE1
	where sfal__Appointment__c is not null
	group by sfal__Appointment__c
	having COUNT(sfal__Appointment__c) < 2)
	and sfal__Role__c = 'Organizer'),
CTE3 AS (
	SELECT
	CTE1.RNK
	,CTE1.LSB_AEE_AppointmentDateTime__c
	--,CAST(APPT.DIA_DATE as datetime) AS LSB_AEE_AppointmentStartDate__c
	,REPLACE(CTE1.LSB_AEE_ExternalId__c, '.Owner',  '.TechnicalAttendee') AS LSB_AEE_ExternalId__c
	,CTE1.LSB_AEE_PausedFlowInterviewGUID__c
	,CTE1.LSB_AEE_Reminder15MinTriggerDate__c
	,CTE1.LSB_AEE_SourceSystem__c
	,CTE1.LSB_AEE_SourceSystemId__c
	,CTE1.Name
	,CTE1.sfal__AdviseeRecord__c
	,CTE1.sfal__Appointment__c
	,CTE1.sfal__Attendee__c
	,CTE1.sfal__Comments__c
	,CAST('Attendee' as varchar(255)) AS sfal__Role__c
	,CTE1.sfal__Status__c
	,CTE1.sfal__StatusComments__c
	,CTE1.sfal__StatusUpdatedBy__c
	,CTE1.sfal__StatusUpdatedDate__c
	FROM  CTE1
	INNER JOIN CTE2 ON CTE1.sfal__Appointment__c = CTE2.sfal__Appointment__c
	WHERE CTE1.sfal__Role__c = 'Organizer'
	)
SELECT * FROM CTE1 WHERE CTE1.LSB_AEE_ExternalId__c NOT IN (SELECT LSB_AEE_ExternalId__c FROM SFC.get_id_AppointmentAttendee WHERE LSB_AEE_ExternalId__c IS NOT NULL)
UNION ALL
SELECT * FROM CTE3 WHERE CTE3.LSB_AEE_ExternalId__c NOt IN (SELECT LSB_AEE_ExternalId__c FROM SFC.get_id_AppointmentAttendee WHERE LSB_AEE_ExternalId__c IS NOT NULL)
GO