CREATE OR ALTER VIEW [DST].[Affiliation_ModuleLeaderCourseDirector] AS
SELECT
	--CAST(null as varchar(1300)) AS hed__Affiliation_Type__c
	CAST(GIDC.Id as varchar(18)) AS hed__Contact__c
	,CAST(null as varchar(max)) AS hed__Description__c
	,CAST(CASE 
			WHEN MLCD.ChangeStatus = 'New' THEN null
			WHEN MLCD.ChangeStatus = 'Delete' AND GIDA.hed__Status__c = 'Current' THEN MLCD.UpdateDateTime
			WHEN MLCD.ChangeStatus = 'Delete' AND GIDA.hed__Status__c = 'Former' THEN GIDA.hed__EndDate__c
			ELSE GIDA.hed__EndDate__c
		END as date) AS hed__EndDate__c
	,CAST(GIDAC.Id as varchar(18)) AS hed__Account__c
	,CAST(CASE
			WHEN MLCD.ChangeStatus = 'New' THEN 1
			WHEN MLCD.ChangeStatus = 'Delete' THEN 0
			ELSE 0
		END as bit) AS hed__Primary__c
	,CAST('Course Director' as varchar(255)) AS hed__Role__c
	,CAST(CASE
			WHEN MLCD.ChangeStatus = 'New' THEN ISNULL(MLCD.QL_CreatedDate, MLCD.InsetDateTime)
			ELSE GIDA.hed__StartDate__c
		END as date) AS hed__StartDate__c
	,CAST(CASE
			WHEN MLCD.ChangeStatus = 'New' AND GIDA.Id IS NULL THEN 'Current'
			WHEN MLCD.ChangeStatus = 'Delete' THEN 'Former'
			ELSE GIDA.hed__Status__c
		END as varchar(255)) AS hed__Status__c
	,CAST('QL' as varchar(255)) AS LSB_AON_SourceSystem__c
	,CAST(CONCAT(MLCD.QL_AcadProgNameId,'.',MLCD.QL_CourseDirector, '@lsbu.ac.uk') as varchar(90)) AS LSB_AON_SourceSystemID__c        
	,CAST(CONCAT(MLCD.QL_AcadProgNameId,'.',MLCD.QL_CourseDirector, '@lsbu.ac.uk') as varchar(90)) AS LSB_AON_ExternalID__c        
FROM [INT].[SRC_ModuleLeaderCourseDirector_Affiliation] MLCD
JOIN [SFC].[Get_ID_Contact] GIDC ON CONCAT(MLCD.QL_CourseDirector, '@lsbu.ac.uk') = GIDC.LSB_ExternalID__c
LEFT JOIN [SFC].[Get_ID_Affiliation] GIDA ON MLCD.LSB_ExternalID = GIDA.LSB_AON_ExternalID__c
JOIN [SFC].[Get_ID_Account] GIDAC ON MLCD.QL_AcadProgNameId = GIDAC.LSB_ACC_ExternalID__c
WHERE (MLCD.ChangeStatus IN ('NEW','DELETE')
OR EXISTS (Select R.LSB_AON_ExternalID__c from DST.SF_Reject_Affiliation_ModuleLeaderCourseDirector R WHERE MLCD.LSB_ExternalID = R.LSB_AON_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   (VALID.LSB_ExternalID = MLCD.QL_AcadProgNameId OR VALID.LSB_ExternalID = CONCAT(QL_AcadProgNameId, QL_DepartmentId, QL_EducationInstitutionId))
				AND	  VALID.ObjectName = 'AcademicProgram'
				AND	  VALID.SrcSystem = 'QL')