CREATE OR ALTER VIEW [DST].[Case_MAXReportandSupport] AS
SELECT
	 CAST(NULL as varchar(18)) as AccountId
	,CAST(NULL as varchar(18)) as AssetId
	--,CAST(NULL as bit) as LSB_CAS_AvoidValidationField__c
	--,CAST(NULL as varchar(18)) as BusinessHoursId
	--,CAST(NULL as varchar(30)) as CaseNumber
	,CAST('Maximiser' as varchar(255)) as Origin
	,CAST(OId.Id as varchar(18)) as OwnerId
	--,CAST(NULL as bit) as LSB_CAS_CaseOwnerIsUser__c
	,CAST(NULL as varchar(255)) as Reason
	,CAST(RT.Id as varchar(18)) as RecordTypeId
	,CAST(NULL as varchar(1300))as LSB_CAS_CaseResponded__c
	,CAST(NULL as varchar(18)) as SourceId
	,CAST(NULL as varchar(255)) as hed__Category__c
	,CAST(CASE
			WHEN C.Status = 'Resolved' THEN 'Resolved'
		END as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(NULL as varchar(max)) as LSB_CAS_ClosureComment__c
	--,CAST(NULL as varchar(80)) as ContactEmail
	--,CAST(NULL as varchar(40)) as ContactFax
	,CAST(NULL as varchar(255)) as LSB_CAS_FirstName__c
	,CAST(NULL as varchar(255)) as LSB_CAS_LastName__c
	--,CAST(NULL as varchar(40)) as ContactMobile
	,CAST(GIDC.Id as varchar(18)) as ContactId
	--,CAST(NULL as varchar(40)) as ContactPhone
	,CAST(CbyId.Id as varchar(18)) as CreatedById
	,CAST(NULL as date) as LSB_CAS_DateOfBirth__c
	,CAST(CASE 
				WHEN C.Status = 'Resolved' then C.ModifyDate 
		END as datetime) as ClosedDate
	,CAST(C.CreateDate as datetime) as CreatedDate
	,CAST(NULL as varchar(18)) as EntitlementId
	,CAST(NULL as datetime) as SlaExitDate
	,CAST(NULL as datetime) as SlaStartDate
	--,CAST(NULL as bit) as IsEscalated
	--,CAST(NULL as bit) as LSB_CAS_ForReview__c
	,CAST(NULL as varchar(4000))as Comments
	--,CAST(NULL as bit) as LSB_CAS_InternationalEnquiry__c
	--,CAST(NULL as bit) as LSB_CAS_IsMyCase__c
	,CAST(NULL as varchar(255)) as LSB_CAS_KnowledgeArticleLink__c
	,CAST(NULL as varchar(40)) as Language
	,CAST(C.ModifyDate as datetime) as LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) as LastModifiedbyId
	,CAST(NULL as varchar(255)) as LSB_CAS_LevelOfIntendedStudy__c
	,CAST(NULL as varchar(255)) as hed__Location__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeVia__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeAbout__c
	,CAST(CONCAT(C.CaseId, '.ReportandSupport') as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST(CASE
			WHEN C.Status = 'Resolved' THEN 'Your case has been closed please contact us if your query is outstanding'
		END as varchar(max)) as LSB_CAS_Response__c
	,CAST('Maximiser' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(C.CaseNumber as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST(NULL as varchar(max)) as Description
	,CAST(NULL as datetime) as hed__Occurrence_Date__c
	,CAST(NULL as varchar(1300))as LSB_CAS_Over48Hours__c
	,CAST(NULL as varchar(18)) as ParentId
	,CAST(NULL as varchar(255)) as Priority
	--,CAST(NULL as bit) as LSB_CAS_FollowUpRequired__c
	,CAST(NULL as varchar(255)) as LSB_CAS_SLAViolation__c
	,CAST(CASE
			WHEN C.Status = 'Resolved' THEN 'Closed'
			ELSE 'Active' 
		END as varchar(255)) as Status
	,CAST(Concat('Maximiser - ', IDS.Student_ID, ' - Report and Support') as varchar(255)) as Subject
	,CAST(NULL as varchar(255)) as LSB_CAS_SubjectAreaOfInterest__c
	,CAST(NULL as varchar(255)) as LSB_CAS_Topic__c
	,CAST(NULL as varchar(255)) as Type
	,CAST(NULL as varchar(255)) as LSB_CAS_UkEuOrInternationalStudent__c
	,CAST(NULL as varchar(80)) as SuppliedCompany
	,CAST(NULL as varchar(80)) as SuppliedEmail
	--,CAST(NULL as bit) as LSB_CAS_WillProvideFeedbackViaEmail__c
	,CAST(NULL as varchar(255)) as LSB_CAS_YearOfIntendedStudy__c
	,CAST(IDS.Student_ID as varchar(255)) as sfal__StudentID__c
	,CAST(CDD.Report_and_Support_Hate_Incident_Date_of_first_referral as date) as LSB_CAS_HateIncidentDateOfFirstReferral__c
	,CAST(SVLS.Id as varchar(255)) as LSB_CAS_SVLSAdvisor__c
	,CAST(CDD.Report_and_Support_Sexual_Violence_Liaison_Support_Date_of_first_referral as date) as LSB_CAS_SVLSDateOfFirstReferral__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Alleged_perpetrator_LSBU_staff as varchar(255)) as LSB_CAS_AllegedPerpetratorLSBUStaff__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Alleged_perpetrator_LSBU_student as varchar(255)) as LSB_CAS_AllegedPerpetratorLSBUStudent__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_police as varchar(255)) as LSB_CAS_ReportedToPolice__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_Student_Complaints as varchar(255)) as LSB_CAS_ReportedToStudentComplaints__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Case_Management_Reported_to_Student_Disciplinary as varchar(255)) as LSB_CAS_ReportedToStudentDisciplinary__c
	,CAST(NULL as varchar(4099)) as LSB_CAS_IdentifiedThreatFromOthers__c
	,CAST(NULL as varchar(4099)) as LSB_CAS_IdentifiedThreatToOthers__c
	,CAST(NULL as varchar(4099)) as LSB_CAS_IdentifiedRiskToSelf__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Referral_route as varchar(4099)) as LSB_CAS_ReferralRoute__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Escalate_concerns as varchar(255)) as LSB_CAS_EscalateConcerns__c
	,CAST(CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Risk_assessment_Initial_risk_and_support_plan_complete as varchar(255)) as LSB_CAS_InitialRiskAndSupportPlanComplet__c
	,CAST(CASE
			--WHEN CDD.Report_and_Support_Hate_Incident_Date_of_first_referral is not null and CDD.Report_and_Support_Sexual_Violence_Liaison_Support_Date_of_first_referral is not null THEN 'Sexual Violence;Hate Incident'
			--WHEN CDD.Report_and_Support_Sexual_Violence_Liaison_Support_Date_of_first_referral is not null and CDD.Report_and_Support_Hate_Incident_Date_of_first_referral is null THEN 'Sexual Violence'
			--WHEN CDD.Report_and_Support_Hate_Incident_Date_of_first_referral is not null and CDD.Report_and_Support_Sexual_Violence_Liaison_Support_Date_of_first_referral is null THEN 'Hate Incident'
			WHEN C.Subject = 'Report and Support - Hate Incident' THEN 'Hate Incident'
			WHEN C.Subject =  'Sexual Violence Liaison Support' THEN 'Sexual Violence'
			ELSE NULL
	END as varchar(4099)) as LSB_CAS_ReportRelatesTo__c
FROM Maximizer.src.[Case] C
INNER JOIN Maximizer.src.Case_CaseCompanyIndividual CCI ON C.CaseId = CCI.CaseId
INNER JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON CCI.Id = IDS.Id
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_ReportandSupport'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_User OId ON C.Owner = SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) AND OId.IsActive = 1
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User SVLS ON CDLM.Report_and_Support_Sexual_Violence_Liaison_Support_Advisor = SUBSTRING(SVLS.Username,1,(CHARINDEX('@',SVLS.Username)-1))
WHERE C.Subject IN ('Report and Support - Hate Incident','Sexual Violence Liaison Support')
AND CONCAT(C.CaseId, '.ReportandSupport') NOT IN (select LSB_CAS_ExternalID__c from sfc.Get_ID_Case WHERE LSB_CAS_ExternalID__c IS NOT NULL)