CREATE OR ALTER VIEW [DST].[Address_Student_Default_Update] AS
SELECT a.Id,d.hed__Default_Address__c FROM sfc.Get_ID_Address a
JOIN dst.Address_Student d ON a.LSB_ADS_ExternalID__c = d.LSB_ADS_ExternalID__c 
WHERE a.hed__Default_Address__c <> d.hed__Default_Address__c
AND d.hed__Default_Address__c = 1
GO


