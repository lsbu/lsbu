CREATE OR ALTER VIEW [DST].[TermParent_SessionIntakeDetails] AS
SELECT  
	CAST((select Id from SFC.Get_ID_Account where Name = 'London South Bank University') as varchar(18)) AS hed__Account__c,
	CAST(NULL as date) AS hed__End_Date__c,
	CAST(QLSIDT.LSB_ExternalID as varchar(90)) AS LSB_TRM_ExternalID__c,
	CAST(NULL as real) AS hed__Grading_Period_Sequence__c,
	CAST(NULL as real) AS hed__Instructional_Days__c,
	CAST(NULL as varchar(18)) AS hed__Parent_Term__c,
	CAST(QLSIDT.SrcSystem as varchar(255)) AS LSB_TRM_SourceSystem__c,
	CAST(QLSIDT.LSB_ExternalID as varchar(90)) AS LSB_TRM_SourceSystemID__c,
	CAST(NULL as date) AS hed__Start_Date__c,
	CAST(CONCAT('Academic Period ',QLSIDT.QL_AcademicPeriod) as varchar(80)) AS Name,
	CAST('School Year' as varchar(255)) AS hed__Type__c
FROM  [INT].[SRC_SessionIntakeDetails_Term] AS QLSIDT
WHERE QLSIDT.QL_TermType = 'AcademicYear'
AND ( QLSIDT.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select R.LSB_TRM_ExternalID__c from DST.SF_Reject_TermParent R WHERE QLSIDT.LSB_ExternalID = R.LSB_TRM_ExternalID__c)
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLSIDT.LSB_ExternalID
				AND	  VALID.ObjectName = 'SessionIntakeDetails'
				AND	  VALID.SrcSystem = 'QL')
GO