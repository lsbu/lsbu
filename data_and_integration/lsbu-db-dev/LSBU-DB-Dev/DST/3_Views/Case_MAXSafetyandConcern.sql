CREATE OR ALTER VIEW [DST].[Case_MAXSafetyandConcern] AS
SELECT
	 CAST(NULL as varchar(18)) as AccountId
	,CAST(NULL as varchar(18)) as AssetId
	--,CAST(NULL as bit) as LSB_CAS_AvoidValidationField__c
	--,CAST(NULL as varchar(18)) as BusinessHoursId
	--,CAST(NULL as varchar(30)) as CaseNumber
	,CAST('Maximiser' as varchar(255)) as Origin
	,CAST(OId.Id as varchar(18)) as OwnerId								--New
	--,CAST(NULL as bit) as LSB_CAS_CaseOwnerIsUser__c
	,CAST(NULL as varchar(255)) as Reason
	,CAST(RT.Id as varchar(18)) as RecordTypeId
	,CAST(NULL as varchar(1300))as LSB_CAS_CaseResponded__c
	,CAST(NULL as varchar(18)) as SourceId
	,CAST(NULL as varchar(255)) as hed__Category__c
	,CAST(CASE
			WHEN CDD.Safety_Concern_Response_Date_left_meeting IS NOT NULL THEN 'Resolved'
		END as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(NULL as varchar(max)) as LSB_CAS_ClosureComment__c
	--,CAST(NULL as varchar(80)) as ContactEmail
	--,CAST(NULL as varchar(40)) as ContactFax
	,CAST(NULL as varchar(255)) as LSB_CAS_FirstName__c
	,CAST(NULL as varchar(255)) as LSB_CAS_LastName__c
	--,CAST(NULL as varchar(40)) as ContactMobile
	,CAST(GIDC.Id as varchar(18)) as ContactId
	--,CAST(NULL as varchar(40)) as ContactPhone
	,CAST(CbyId.Id as varchar(18)) as CreatedById						--New
	,CAST(NULL as date) as LSB_CAS_DateOfBirth__c
	,CAST(CDD.Safety_Concern_Response_Date_left_meeting as datetime) as ClosedDate
	,CAST(IIF(CDD.Safety_Concern_Response_Date_left_meeting < CDD.Safety_Concern_Response_Date_added_to_meeting, CDD.Safety_Concern_Response_Date_left_meeting, CDD.Safety_Concern_Response_Date_added_to_meeting) as datetime) as CreatedDate
	,CAST(NULL as varchar(18)) as EntitlementId
	,CAST(NULL as datetime) as SlaExitDate
	,CAST(NULL as datetime) as SlaStartDate
	--,CAST(NULL as bit) as IsEscalated
	--,CAST(NULL as bit) as LSB_CAS_ForReview__c
	,CAST(NULL as varchar(4000))as Comments
	--,CAST(NULL as bit) as LSB_CAS_InternationalEnquiry__c
	--,CAST(NULL as bit) as LSB_CAS_IsMyCase__c
	,CAST(NULL as varchar(255)) as LSB_CAS_KnowledgeArticleLink__c
	,CAST(NULL as varchar(40)) as Language
	,CAST(LbyId.Id as varchar(18)) as LastModifiedById
	,CAST(CDD.Safety_Concern_Response_Date_left_meeting as date) as LastModifiedDate
	,CAST(NULL as varchar(255)) as LSB_CAS_LevelOfIntendedStudy__c
	,CAST(NULL as varchar(255)) as hed__Location__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeVia__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeAbout__c
	,CAST(CONCAT(C.CaseId, '.SafetyConcernResponse') as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST(CASE
			WHEN CDD.Safety_Concern_Response_Date_left_meeting IS NOT NULL THEN 'Your case has been closed please contact us if your query is outstanding'
		END as varchar(max)) as LSB_CAS_Response__c
	,CAST('Maximiser' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(C.CaseNumber as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST(CASE 
				WHEN [Safety_Concern_Response_Nature_of_concern_Other_concern_detail] IS NOT NULL
				THEN CONCAT('Other Concern details -> ',[Safety_Concern_Response_Nature_of_concern_Other_concern_detail]) 
				ELSE NULL
				END 
			as varchar(max)) as Description
	,CAST(NULL as datetime) as hed__Occurrence_Date__c
	,CAST(NULL as varchar(1300))as LSB_CAS_Over48Hours__c
	,CAST(NULL as varchar(18)) as ParentId
	,CAST(NULL as varchar(255)) as Priority
	--,CAST(NULL as bit) as LSB_CAS_FollowUpRequired__c
	,CAST(NULL as varchar(255)) as LSB_CAS_SLAViolation__c
	,CAST(CASE
			WHEN CDD.Safety_Concern_Response_Date_left_meeting IS NOT NULL THEN 'Closed'
			ELSE 'Active' 
		END as varchar(255)) as Status
	,CAST(Concat(GIDC.Name, ' - ', IDS.Student_ID, ' - Safety Concern Response Case - ', CDD.Safety_Concern_Response_Date_added_to_meeting) as varchar(255)) as Subject
	,CAST(NULL as varchar(255)) as LSB_CAS_SubjectAreaOfInterest__c
	,CAST(NULL as varchar(255)) as LSB_CAS_Topic__c
	,CAST(NULL as varchar(255)) as Type
	,CAST(NULL as varchar(255)) as LSB_CAS_UkEuOrInternationalStudent__c
	,CAST(NULL as varchar(80)) as SuppliedCompany
	,CAST(NULL as varchar(80)) as SuppliedEmail
	--,CAST(NULL as bit) as LSB_CAS_WillProvideFeedbackViaEmail__c
	,CAST(NULL as varchar(255)) as LSB_CAS_YearOfIntendedStudy__c
	,CAST(IDS.Student_ID as varchar(255)) as sfal__StudentID__c
	,CAST(CASE
			WHEN CDLM.Safety_Concern_Response_Concern_raised_by = 'Exernal other' THEN 'External other'
			ELSE CDLM.Safety_Concern_Response_Concern_raised_by 
		  END as varchar(4099)) as LSB_CAS_ConcernRaisedBy__c
	,CAST(CDD.Safety_Concern_Response_Date_of_initial_concern as date) as LSB_CAS_DateOfInitialConcern__c
	,CAST(CDLM.Safety_Concern_Response_Nature_of_concern_Covid_19 as varchar(255)) as LSB_CAS_Covid19__c
	,CAST(REPLACE(REPLACE(CDLM.Safety_Concern_Response_Nature_of_concern_Covid_19_detail
		, 'Symptomatic, awaiting test result', 'Symptomatic - awaiting test result')
		, 'Symptomatic, no tests available', 'Symptomatic - no tests available') as varchar(4099)) as LSB_CAS_Covid19Detail__c
	,CAST(CDLM.Safety_Concern_Response_Nature_of_concern_Exacerbatingcontributing_factors as varchar(4099)) as LSB_CAS_ContributingFactors__c
	,CAST(REPLACE(CDLM.Safety_Concern_Response_Nature_of_concern_Exacerbatingcontributing_factors_detail, 'Historical factors (MH history, trauma, harm to others)', 'Historical factor (eg. MH history)') as varchar(255)) as LSB_CAS_ContributingFactorsDetails__c
	,CAST(CDLM.Safety_Concern_Response_Nature_of_concern_Other_concern as varchar(4099)) as LSB_CAS_OtherConcern__c
	--,CAST(CONCAT('Threat to self: ', CDLM.Safety_Concern_Response_Nature_of_concern_Threat_to_self_detail, ' ; Threat to others: ', CDLM.Safety_Concern_Response_Nature_of_concern_Threat_to_others_detail) as varchar(1000)) as LSB_CAS_ThreatDetails__c
	,CAST(IIF(CDLM.Safety_Concern_Response_Nature_of_concern_Threat_from_others = 'Yes', 1, 0) AS bit) AS LSB_CAS_ThreatFromOthers__c  --New
	,CAST(REPLACE(CDLM.Safety_Concern_Response_Nature_of_concern_Threat_from_others_detail, 'Other', 'Other (see notes)') AS varchar(255)) AS LSB_CAS_IdentifiedThreatFromOthers__c  --New
	,CAST(IIF(CDLM.Safety_Concern_Response_Nature_of_concern_Threat_to_others = 'Yes', 1, 0) AS bit) AS LSB_CAS_ThreatToOthers__c  --New
	,CAST(REPLACE(REPLACE(CDLM.Safety_Concern_Response_Nature_of_concern_Threat_to_others_detail, 'Other', 'Other (see notes)'), 'Health and safety of Other (see notes)s affected', 'Health and safety of others affected') AS varchar(255)) AS LSB_CAS_IdentifiedThreatToOthers__c  --New
	,CAST(IIF(CDLM.Safety_Concern_Response_Nature_of_concern_Threat_to_self = 'Yes', 1, 0) AS bit) AS LSB_CAS_ThreatToSelf__c  --New
	,CAST(REPLACE(REPLACE(CDLM.Safety_Concern_Response_Nature_of_concern_Threat_to_self_detail, 'Alchohol use', 'Alcohol use'), 'Other risk to self (see notes)', 'Other (see notes)') AS varchar(255)) AS LSB_CAS_IdentifiedRiskToSelf__c --New
	,CAST(CDN.Safety_Concern_Response_Number_of_students_affected as real) as LSB_CAS_NumberOfStudentsAffected__c
	,CAST(CDLM.Safety_Concern_Response_Risk_Initial_risk_or_impact_level__at_time_of_concern_ as varchar(255)) as LSB_CAS_RiskOrImpactLevel__c
	,CAST(CDLM.Safety_Concern_Response_Safeguarding_concern as varchar(255)) as LSB_CAS_SafeguardingConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(CDLM.Safety_Concern_Response_Teams_people_involved_External, 'A&amp;amp', 'A&E'), 'Exernal other', 'External other'),'A&amp;E','A&E') as varchar(4099)) as LSB_CAS_TeamsPeopleInvolvedExternal__c
	,CAST(CDLM.Safety_Concern_Response_Teams_people_involved_LSBU as varchar(4099)) as LSB_CAS_TeamsPeopleInvolvedLSBU__c
	,CAST(CASE
			WHEN CDL.Safety_Concern_Response_Student_in_halls  = 'David Bomberg' THEN 'David Bomberg House'
			WHEN CDL.Safety_Concern_Response_Student_in_halls  = 'Private halls' THEN 'Private (non-LSBU) halls,'
			ELSE CDL.Safety_Concern_Response_Student_in_halls
		  END as varchar(255)) as LSB_CAS_InHalls__c
	,CAST(IIF(CDLM.[Safety_Concern_Response_Action_plan_Action_plan_required] = 'Yes', 1, 0) as bit) as LSB_CAS_ActionPlanRequired__c  -- New
      ,CAST(REPLACE(CDLM.[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_1], 'Medium;Low', 'Medium') AS varchar(255)) as LSB_CAS_RiskLevel1__c -- New
      ,CAST(CDLM.[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_2] AS varchar(255)) as LSB_CAS_RiskLevel2__c -- New
      ,CAST(CDLM.[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_3] AS varchar(255)) as LSB_CAS_RiskLevel3__c -- New
      ,CAST(CDLM.[Safety_Concern_Response_Risk_Risk_reviews_Risk_review_4] AS varchar(255)) as LSB_CAS_RiskLevel4__c -- New
      --,CAST([Safety_Concern_Response_Risk_Risk_reviews_Risk_review_5] AS varchar(255)) as LSB_CAS_RiskLevel5__c -- New
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_SafetyConcernResponse'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailList CDL ON C.CaseId = CDL.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailNumber CDN ON C.CaseId = CDN.CaseId
LEFT JOIN Maximizer.src.Case_CaseCompanyIndividual CCI ON C.CaseId = CCI.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON CCI.Id = IDS.Id
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN SFC.Get_ID_User OId ON   C.Owner = SUBSTRING(OId.Username,1,(CHARINDEX('@',OId.Username)-1)) and OId.IsActive = 1
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject IN ('Safety Concern Response Meeting','SCRM')
AND CONCAT(C.CaseId, '.SafetyConcernResponse') NOT IN (select LSB_CAS_ExternalID__c from sfc.Get_ID_Case WHERE LSB_CAS_ExternalID__c IS NOT NULL)