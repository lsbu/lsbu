CREATE OR ALTER VIEW [DST].[CourseEnrollment_ModuleEnrolmentDetails] AS

SELECT DISTINCT 
    CAST(QLAP.Id as varchar(18)) AS hed__Account__c
   ,CAST(MED.QL_AOSPeriod AS VARCHAR(255)) AS LSB_CCN_SessionCode__c
   ,CAST(MED.QL_Term AS VARCHAR(255)) AS LSB_CCN_SessionDescription__c
   --,CAST(DICT.DST_val as VARCHAR(255)) AS LSB_CCN_ModuleEnrolmentStatus__c -- 
   ,CAST(REPLACE(MED.QL_EnrollmentStatus,'0','O') as VARCHAR(255)) AS LSB_CCN_ModuleEnrolmentStatus__c
   ,case when TRY_CONVERT(date, MED.QL_EnrolStatusDateAchieved, 103) <'19000101' then null else TRY_CONVERT(date, MED.QL_EnrolStatusDateAchieved, 103) end  AS LSB_CCN_ModuleEnrolmentStatusdateach__c
   ,CAST(CASE 
         WHEN MED.QL_Compulsory = 'T' THEN 'Compulsory'     --Switched the values
         WHEN MED.QL_Compulsory = 'F' THEN 'Optional'  
        END AS VARCHAR(255)) AS LSB_CCN_CompulsoryOptional__c
   ,CAST(GIT.Id AS VARCHAR(18)) AS LSB_CCN_Term__c
   ,CAST(GIA.Id AS VARCHAR(18)) AS hed__Affiliation__c
   ,CAST(GIDC.Id AS VARCHAR(18)) AS hed__Contact__c
   ,CAST(GIP.Id AS VARCHAR(18)) AS hed__Program_Enrollment__c
   ,CAST(GIC.Id AS VARCHAR(18)) AS hed__Course_Offering__c
   ,CAST(MED.LSB_ExternalID AS VARCHAR(255)) AS LSB_CCN_ExternalID__c 
   ,CAST(MED.LSB_ExternalID AS VARCHAR(255)) AS LSB_CCN_SourceSystemID__c
   ,CAST(MED.SrcSystem as varchar(255)) AS LSB_CCN_SourceSystem__c 
   ,CAST(MED.QL_Semester AS VARCHAR(18)) AS LSB_CCN_Semester__c
   ,CAST(CASE
          WHEN CHARINDEX('R',MED.QL_ModuleInstance) = 1 THEN 1
          ELSE 0
         END AS bit) AS LSB_CCN_Resit__c
    
    ,CAST(GRD1.QL_OverallModuleGrade AS VARCHAR(18)) AS LSB_CCN_ModuleGrade__c
    ,CAST(GRD1.QL_OverallModuleMarks AS VARCHAR(18)) AS LSB_CCN_ModuleMarks__c
  	,(SELECT ID FROM SFC.RecordType where DeveloperName = 'LSB_Module') AS RecordTypeId  -- New Field to be mapped
	  --,MED.ChangeStatus
    ,CAST(CASE GRD1.QL_OverallModuleGrade
            WHEN 'P' THEN SMO.QL_ModuleCredit ELSE NULL END AS INT) AS hed__Credits_Earned__c
    ,CAST(SGICA.Id AS VARCHAR(18)) AS LSB_PEN_AdviseeCaseRecord__c 
     
FROM [INT].[SRC_ModuleEnrolmentDetails] MED
 JOIN [SFC].[Get_ID_Contact] GIDC ON GIDC.LSB_ExternalID__c = MED.QL_StudentID
 JOIN [SFC].[Get_ID_ProgramEnrollment] GIP ON GIP.LSB_PEN_ExternalID__c = MED.ProgramEnrolment_LSB_ExternalID   -- Changed from Left to Inner 
 LEFT JOIN [SFC].[Get_ID_Case] SGICA ON SGICA.ContactId = GIDC.Id AND SGICA.RecordTypeId in (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord') 
 LEFT JOIN [SFC].[Get_ID_Account] AS QLAP ON MED.QL_AcadProgNameId = QLAP.LSB_ACC_ExternalID__c
 LEFT JOIN [SFC].[Get_ID_Affiliation] GIA ON GIA.LSB_AON_ExternalID__c = MED.Affiliation_LSB_ExternalID
 LEFT JOIN [SFC].[Get_ID_CourseOffering] GIC ON GIC.LSB_COF_ExternalID__c = MED.ModuleOffering_LSB_ExternalID
 LEFT JOIN [SFC].[Get_ID_Term] GIT ON GIT.LSB_TRM_ExternalID__c = MED.Term_LSB_ExternalID
 LEFT JOIN [INT].[SRC_ModuleOffering] SMO ON SMO.LSB_ExternalID= MED.ModuleOffering_LSB_ExternalID
--LEFT JOIN [DST].[DICT_ModuleEnrolmentStatus] DICT ON DICT.Source_val = MED.QL_EnrollmentStatus -- no need to use DICT sice API names for picklist have been updated in SF
LEFT JOIN (SELECT  
               GR1.ModuleEnrolment_LSB_ExternalID
              ,GR1.QL_OverallModuleGrade
              ,GR1.QL_OverallModuleMarks
             FROM [INT].[SRC_Grade] GR1
             WHERE QL_Attempt = (SELECT MAX(GR2.QL_Attempt) FROM [INT].[SRC_Grade] GR2 WHERE GR2.ModuleEnrolment_LSB_ExternalID = GR1.ModuleEnrolment_LSB_ExternalID)) GRD1 ON GRD1.ModuleEnrolment_LSB_ExternalID = MED.LSB_ExternalID

WHERE
(
	MED.ChangeStatus IN ('NEW','UPDATE')
	OR 
	EXISTS (Select 1 from DST.SF_Reject_CourseEnrollment_ModuleEnrolmentDetails R WHERE MED.LSB_ExternalID = R.LSB_CCN_ExternalID__c)
	OR
	(NOT EXISTS (SELECT * FROM [SFC].Get_ID_CourseEnrollment GICE WHERE GICE.LSB_CCN_ExternalID__c = MED.LSB_ExternalID) AND MED.ChangeStatus <> 'DELETE')  -- New Condition
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = MED.LSB_ExternalID
				AND	  VALID.ObjectName = 'ModuleEnrolmentDetails'
				AND	  VALID.SrcSystem = 'QL')