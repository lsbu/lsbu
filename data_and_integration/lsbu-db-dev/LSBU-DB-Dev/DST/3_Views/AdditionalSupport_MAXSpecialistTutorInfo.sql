
/****** Object:  View [DST].[AdditionalSupport_MAXSpecialistTutorInfo]    Script Date: 2021-05-12 20:48:45 ******/

CREATE OR ALTER   VIEW [DST].[AdditionalSupport_MAXSpecialistTutorInfo] AS
SELECT
	--CAST(NULL AS VARCHAR(18)) AS CreatedById
	--,CAST(NULL AS DATETIME) AS CreatedDate
	--,CAST(NULL AS VARCHAR(18)) AS Id
	--,CAST(NULL AS BIT) AS IsDeleted
	--,CAST(NULL AS DATE) AS LastActivityDate
	--,CAST(NULL AS VARCHAR(18)) AS LastModifiedById
	--,CAST(NULL AS DATETIME) AS LastModifiedDate
	--,CAST(NULL AS DATETIME) AS LastReferencedDate
	--,CAST(NULL AS DATETIME) AS LastViewedDate
	CAST(NULL AS REAL) AS LSB_ART_BSLHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS REAL) AS LSB_ART_BSLHours__c
	--,CAST(NULL AS BIT) AS LSB_ART_BSLInterpreterRecommendedLSBU__c
	--,CAST(NULL AS BIT) AS LSB_ART_BSLInterpreterRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_BSLSupplierLSBUFunded__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_BSLSupplier__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_BSLTotalCostLSBUFundedNMH__c
	,CAST(GIDC.Id AS VARCHAR(18)) AS LSB_ART_Contact__c
	,CAST(NULL AS DATETIME) AS LSB_ART_DateOfEPAssessment__c
	,CAST(NULL AS DATETIME) AS LSB_ART_DateOfSpLDScreening__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_DyslexiaTuitionType__c
	,CAST(NULL AS DATE) AS LSB_ART_DyslexiaTutorAllocated__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_DyslexiaTutorRecommended__c
	,CAST(NULL AS DATE) AS LSB_ART_EdPsychReportReceivedDate__c
	,CAST(NULL AS DATE) AS LSB_ART_EmailConvertedToFullSent__c
	,CAST(NULL AS DATE) AS LSB_ART_EmailMentoringToFullSent__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPAssessmentResult__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPAssessor__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPCost__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPFeedbackAppointmentAdviser__c
	,CAST(NULL AS DATE) AS LSB_ART_EPFeedbackAppointmentDate__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_EPRoom__c
	,CAST(CONCAT(IDS.Student_ID,'.Tutor.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY I.Created)) AS VARCHAR(90)) AS LSB_ART_ExternalId__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_FeedbackLocation__c
	--,CAST(NULL AS BIT) AS LSB_ART_LockerAgreementSigned__c
	,CAST(NULL AS DATE) AS LSB_ART_LockerAssignedDate__c
	,CAST(NULL AS DATE) AS LSB_ART_LockerLoanEndDate__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_LockerNumber__c
	,CAST(NULL AS DATE) AS LSB_ART_MentorAllocated__c
	,CAST(NULL AS REAL) AS LSB_ART_MentorHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentoringSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentoringType__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_MentorInitialValue__c
	--,CAST(NULL AS BIT) AS LSB_ART_MentorRecommendedLSBUFunded__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_MentorSupportTotalHours__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_MentorTotalCost__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_Mentor__c
	--,CAST(NULL AS BIT) AS LSB_ART_NoteTakerRecommendedLSBUFunded__c
	--,CAST(NULL AS VARCHAR(255)) AS LSB_ART_NoteTakerRecommended__c
	,CAST(NULL AS REAL) AS LSB_ART_NoteTakingHoursAgreedLSBUFunded__c
	,CAST(NULL AS REAL) AS LSB_ART_NoteTakingHours__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_NoteTakingSupplierLSBUFundedNMH__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_NoteTakingTotalCostLSBUFundedNMH__c
	,CAST(CONCAT(IDS.Student_ID,'.Tutor.',ROW_NUMBER() OVER (PARTITION BY IDS.Student_ID ORDER BY I.Created)) AS VARCHAR(90)) AS LSB_ART_SourceSystemId__c
	,CAST('Maximiser' AS VARCHAR(90)) AS LSB_ART_SourceSystem__c
	,CASE UPPER(IDLM.DDS_DSA_Specialist_Tutor_Specialist_Tutor_Recommended)
      WHEN 'NO'  THEN CAST(0 as BIT)
	  WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
     END AS LSB_ART_SpecialistTutorRecommended__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_SpecialistTutorSupplier__c -- Field does not exists in Maximizer
	,CAST(NULL AS REAL) AS LSB_ART_SpecialistTutorSupportHours__c -- Field does not exists in Maximizer
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_SpLDScreeningOutcome__c
	,CAST(NULL AS DATE) AS LSB_ART_TotalInterimHoursUsedRequest__c
	,CAST(NULL AS REAL) AS LSB_ART_TotalInterimHoursUsed__c
	,CAST(NULL AS REAL) AS LSB_ART_TotalInterimMentoringHoursUsed__c
  ,CASE 
   WHEN IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Hours_Agreed IN ('10;hours in total', 'hours in total;10') THEN '10'
   ELSE CAST(IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Hours_Agreed AS REAL) END AS LSB_ART_TutorHoursAgreedLSBUFundedNMH__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_TutorInitialValue__c
	,CASE UPPER(IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Recommended)
      WHEN 'NO'  THEN CAST(0 as BIT)
	  WHEN 'YES' THEN CAST(1 as BIT) ELSE CAST(0 as BIT) 
     END AS LSB_ART_TutorRecommendedLSBUFundedNMH__c
	,CAST( REPLACE(IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Supplier, 'Randstad,', 'Randstad') AS VARCHAR(255)) AS LSB_ART_TutorSupplierLSBUFundedNMH__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupplier__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_ART_TutorSupportTotalHours__c
	,CAST(NULL AS DECIMAL) AS LSB_ART_TutorTotalCostLSBUFundedNMH__c -- Field does not exists in Maximizer
	--,CAST(NULL AS VARCHAR(80)) AS Name
	--,CAST(NULL AS VARCHAR(18)) AS OwnerId
	,CAST((SELECT id FROM sfc.RecordType WHERE DeveloperName = 'LSB_ART_SpecialistTutorInformation') AS VARCHAR(18)) AS RecordTypeId
	--,CAST(NULL AS DATETIME) AS SystemModstamp
FROM Maximizer.src.Individual I
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON I.Id = IDS.Id
 JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
 JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON I.Id = IDLM.Id
WHERE ( 
  IDLM.DDS_DSA_Specialist_Tutor_Specialist_Tutor_Recommended IS NOT NULL OR
  IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Hours_Agreed IS NOT NULL OR
  IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Recommended IS NOT NULL OR
  IDLM.DDS_LSBU_funded_NMH_Study_Skills_Tuition__LSBU__Tutor_Supplier IS NOT NULL 
  )
GO


