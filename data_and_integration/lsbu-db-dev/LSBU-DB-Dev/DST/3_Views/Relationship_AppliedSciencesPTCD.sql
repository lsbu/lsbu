CREATE OR ALTER VIEW [DST].[Relationship_AppliedSciencesPTCD] AS
SELECT
  CAST(C.Id as varchar(18)) AS hed__Contact__c,
  CAST(null as varchar(18)) AS hed__ReciprocalRelationship__c,
  CAST(C2.Id as varchar(18)) AS hed__RelatedContact__c,
  CAST(null as varchar(1300)) AS hed__Relationship_Explanation__c,
  CAST(CASE 
		WHEN RSP.ChangeStatus = 'NEW' THEN 'Current'
		WHEN RSP.ChangeStatus = 'DELETE' THEN 'Former'
	END as nvarchar(255)) AS hed__Status__c,
  CAST('Personal Tutor' as nvarchar(255)) AS hed__Type__c,
  CAST('ASPTCDCSV' as nvarchar(255)) AS LSB_RTP_SourceSystem__c,
  CAST(CONCAT(RSP.QL_PersonalTutorId,'.',RSP.PTCDCSV_StudentId) as nvarchar(90)) AS LSB_RTP_SourceSystemID__c,
  CAST(CONCAT(RSP.QL_PersonalTutorId,'.',RSP.PTCDCSV_StudentId) as nvarchar(90)) AS LSB_RTP_ExternalID__c,
   (select id from sfc.RecordType	where name like 'Relationship'	and SobjectType='hed__Relationship__c') as RecordTypeId
FROM [INT].[SRC_AppliedSciencesPTCD_Relationship] RSP
INNER JOIN [SFC].Get_ID_Contact AS C ON RSP.PTCDCSV_PersonalTutorEmailAddress = C.LSB_ExternalID__c
INNER JOIN [SFC].Get_ID_Contact AS C2 ON RSP.PTCDCSV_StudentId = C2.LSB_ExternalID__c
WHERE (RSP.ChangeStatus IN ('NEW','UPDATE','DELETE')
OR EXISTS (Select R.LSB_RTP_ExternalID__c from DST.SF_Reject_Relationship_AppliedSciencesPTCD R WHERE RSP.LSB_ExternalID = R.LSB_RTP_ExternalID__c)
OR (NOT EXISTS (SELECT * FROM [SFC].[Get_ID_Relationship] GIR WHERE GIR.LSB_RTP_ExternalId__c = CONCAT(RSP.QL_PersonalTutorId,'.',RSP.PTCDCSV_StudentId)) AND RSP.ChangeStatus <> 'DELETE')
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
					WHERE VALID.ValidationResultFlag = 'ERROR'
					AND   VALID.LSB_ExternalID = SUBSTRING( RSP.LSB_ExternalID, CHARINDEX('.', RSP.LSB_ExternalID)+1,len(RSP.LSB_ExternalID))
					AND	  VALID.ObjectName = 'AppliedSciencesPTCD'
					AND	  VALID.SrcSystem = 'SAL')