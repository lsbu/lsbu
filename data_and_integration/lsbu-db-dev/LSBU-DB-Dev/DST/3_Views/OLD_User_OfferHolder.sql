--CREATE OR ALTER   VIEW [DST].[User_OfferHolder] AS
--SELECT DISTINCT
----null as Id
----null as IsDeleted
----null as MasterRecordId
--null as AccountId,
--CASE
--WHEN QLOH.QL_Surname IN ('-','*','.') THEN QL_FirstName
--ELSE QLOH.QL_Surname 
--END as LastName,
--CASE
--WHEN QLOH.QL_Surname IN ('-','*','.') THEN NULL
--WHEN QLOH.QL_FirstName in ('-','*','.') THEN NULL
--ELSE QLOH.QL_FirstName 
--END  as FirstName,
--null as Salutation,
--null as Name,
--null as OtherStreet,
--null as OtherCity,
--null as OtherState,
--null as OtherPostalCode,
--null as OtherCountry,
--null as OtherLatitude,
--null as OtherLongitude,
--null as OtherGeocodeAccuracy,
--null as OtherAddress,
--STUFF(CONCAT(' ' + NULLIF(QLOH.QL_Add1, ''),' ' + NULLIF(QLOH.QL_Add2, ''),' ' + NULLIF(QLOH.QL_Add3, '')),1,1,'') as MailingStreet,
--SUBSTRING(QLOH.QL_Add4,1,40) as MailingCity,
--null as MailingState,
--QLOH.QL_Postcode as MailingPostalCode,
--DAC.[MailingCountry] as MailingCountry, --QLOH.QL_Country as MailingCountry,--
--null as MailingLatitude,
--null as MailingLongitude,
--null as MailingGeocodeAccuracy,
--null as MailingAddress,
--null as Phone,
--null as Fax,
--QLOH.QL_MobileNo as MobilePhone,
--QLOH.QL_Phone as HomePhone,
--null as OtherPhone,
--null as AssistantPhone,
--null as ReportsToId,
----QLOH.QL_PersonalEmail as Email, --production Email
--CFG.fx_HashEmail(QLOH.QL_PersonalEmail, QLOH.QL_StudentID) as Email,--HashEmail
----null as Email,
--QLOH.QL_Title as Title,
--null as Department, ---QLOH.QL_DepartmentName as Department, - lookup to Department ?
--null as AssistantName,
--null as LeadSource,
--TRY_CONVERT(date, QLOH.QL_Birthdate, 103 ) as Birthdate,
--null as Description, --QLOH.QL_Description source column don't exists
--null as OwnerId,--QLOH.QL_Origin as OwnerId, --no applicable 
--null as HasOptedOutOfEmail,
--null as HasOptedOutOfFax,
--null as DoNotCall,
--null as CreatedDate,
--null as CreatedById,
--null as LastModifiedDate,
--null as LastModifiedById,
--null as SystemModstamp,
--null as LastActivityDate,
--null as LastCURequestDate,
--null as LastCUUpdateDate,
--null as LastViewedDate,
--null as LastReferencedDate,
--null as EmailBouncedReason,
--null as EmailBouncedDate,
--null as IsEmailBounced,
--null as PhotoUrl,
--null as Jigsaw,
--null as JigsawContactId,
--null as IndividualId,
--null as Primary_Academic_Program__c,
--null as Primary_Department__c,
--null as Primary_Educational_Institution__c,
--null as Primary_Sports_Organization__c,
--CFG.fx_HashEmail(QLOH.QL_PersonalEmail, QLOH.QL_StudentID) as hed__AlternateEmail__c,
--ISNULL(DCN.[Citizenship], QLOH.QL_Nationality) as hed__Citizenship__c, --QLOH.QL_Nationality as hed__Citizenship__c, --DICTIONARY need 
--ISNULL(DCD.[hed__Country_of_Origin__c], QLOH.QL_Domicile) as hed__Country_of_Origin__c, --QLOH.QL_Domicile as hed__Country_of_Origin__c, --DICTIONARY need 
--null as hed__Current_Address__c,
--null as hed__Deceased__c,
--null as hed__Do_Not_Contact__c,
--null as hed__Ethnicity__c,
--null as hed__Exclude_from_Household_Formal_Greeting__c,
--null as hed__Exclude_from_Household_Informal_Greeting__c,
--null as hed__Exclude_from_Household_Name__c,
--null as hed__FERPA__c,
--null as hed__Financial_Aid_Applicant__c,
----QLOH.QL_Gender as hed__Gender__c,
--(CASE --SF_PickList(Male,Female,Other)
--	WHEN QLOH.QL_Gender = 'F' THEN 'Female'
--	WHEN QLOH.QL_Gender = 'M' THEN 'Male'
--	WHEN QLOH.QL_Gender = 'O' THEN 'Other'
--END) as hed__Gender__c,
--null as hed__HIPAA_Detail__c,
--null as hed__HIPAA__c,
--null as hed__Military_Background__c,
--null as hed__Military_Service__c,
--null as hed__Naming_Exclusions__c,
----If QL_MobilePhone is not NULL then 'Mobile' else If QL_Phone is not NULL then 'Other'
----QLOH.QL_MobileNo,QLOH.QL_Phone,
--	(CASE
--		WHEN QLOH.QL_MobileNo IS NOT NULL THEN 'Mobile' 
--		WHEN QLOH.QL_Phone IS NOT NULL THEN 'Home'
--		ELSE NULL
--	END) as hed__PreferredPhone__c,
----If QL_PersonalEmail is not NULL then 'Alternate' else If QL_LSBUEmail is not NULL then 'University'
--	(CASE --CFG.fx_HashEmail(QLOH.QL_LSBUEmail, QLOH.QL_StudentID) 'Personal Email','University,
--		WHEN QLOH.QL_PersonalEmail is not NULL THEN 'Personal Email' --'Alternate'
--		WHEN QLOH.QL_LSBUEmail is not NULL THEN 'University'
--		ELSE NULL
--	END) as hed__Preferred_Email__c, 
----null as hed__Preferred_Email__c,
--null as hed__Primary_Address_Type__c,
--null as hed__Primary_Household__c,
--null as hed__Primary_Organization__c,
--null as hed__Religion__c,
--null as hed__Secondary_Address_Type__c,
--null as hed__Social_Security_Number__c,
----QLOH.QL_LSBUEmail as hed__UniversityEmail__c,--Production
----QLOH.QL_LSBUEmail, QLOH.QL_StudentID,'a',
--CFG.fx_HashEmail(QLOH.QL_LSBUEmail, QLOH.QL_StudentID) as hed__UniversityEmail__c,--HashEmail
--null as hed__WorkEmail__c,
--null as hed__WorkPhone__c,
--null as hed__Work_Address__c,
--null as hed__is_Address_Override__c,
--null as Primary_Student_Organization__c,
--null as hed__Dual_Citizenship__c,
--null as hed__Race__c,
--null as hed__Primary_Language__c,
--QLOH.QL_FamiliarName as hed__Chosen_Full_Name__c,
--null as hed__Citizenship_Status__c,
--null as hed__Date_Deceased__c,
--null as hed__Former_First_Name__c,
--null as hed__Former_Last_Name__c,
--null as hed__Former_Middle_Name__c,
--null as hed__Mailing_County__c,
--null as hed__Other_County__c,
--(CASE 
--	WHEN QLOH.QL_PersonalEmail IS NOT NULL THEN 'Email'
--	WHEN QLOH.QL_Phone IS NOT NULL THEN 'Telephone'
--	ELSE 'Not Applicable'
--END)as LSB_ChannelOfPreference__c,
--QLOH.SrcSystem as LSB_SourceSystem__c,
--QLOH.QL_StudentID as LSB_SourceSystemID__c,
--QLOH.QL_StudentID as LSB_ExternalID__c,
--DCSS.[LSB_CON_StudentStatus__c] as LSB_CON_StudentStatus__c,--QLOH.QL_StudentStatus as LSB_CON_StudentStatus__c,
--QLOH.QL_Disabilty as LSB_CON_Disability__c,
--QLOH.QL_UCASPersonalID as LSB_CON_UCASID__c
--,(SELECT CR.hed__Role__c FROM [DST].[Affiliation_Contact_Role] CR WHERE CR.StudentId = QLOH.QL_StudentID) as LSB_CON_CurrentRole__c
--FROM [INT].[SRC_Student] as QLOH
--LEFT JOIN [DST].[DICT_Contact_Nationality] DCN ON DCN.[QL_Nationality] = QLOH.QL_Nationality
--LEFT JOIN [DST].[DICT_Contact_Domicile] DCD ON DCD.[QL_Domicile] = QLOH.QL_Domicile 
--LEFT JOIN [DST].[DICT_QL_Add5_Country] DAC ON DAC.[QL_Add5] = QLOH.QL_Add5 
--LEFT JOIN [DST].[DICT_Contact_StudentStatus] DCSS ON  DCSS.[QL_StudentStatus] = QLOH.QL_StudentStatus --need mod
--WHERE ( QLOH.ChangeStatus IN ('NEW','UPDATE')
--OR
--EXISTS (Select R.LSB_ExternalID__c from DST.SF_Reject_Contact R WHERE QLOH.LSB_ExternalID = R.LSB_ExternalID__c)
--)

--GO


