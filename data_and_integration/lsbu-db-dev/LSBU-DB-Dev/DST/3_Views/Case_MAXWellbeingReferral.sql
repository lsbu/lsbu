CREATE OR ALTER VIEW [DST].[Case_MAXWellbeingReferral] AS
SELECT 
	AccountId
	,AssetId
	,Origin
	,Reason
	,RecordTypeId
	,LSB_CAS_CaseResponded__c
	,SourceId
	,hed__Category__c	
	,LSB_CAS_ClosedReason__c
	,LSB_CAS_ClosureComment__c
	,LSB_CAS_FirstName__c
	,LSB_CAS_LastName__c
	,ContactId
	,LSB_CAS_DateOfBirth__c
	,ClosedDate
	,CreatedDate
	,EntitlementId
	,SlaExitDate
	,SlaStartDate
	,Comments
	,LSB_CAS_KnowledgeArticleLink__c
	,Language
	,LastModifiedDate
	,LSB_CAS_LevelOfIntendedStudy__c
	,hed__Location__c
	,LSB_CAS_LSBUCanContactMeVia__c
	,LSB_CAS_LSBUCanContactMeAbout__c
	,LSB_CAS_ExternalID__c
	,LSB_CAS_Response__c
	,LSB_CAS_SourceSystem__c
	,LSB_CAS_SourceSystemID__c
	,Description
	,hed__Occurrence_Date__c
	,LSB_CAS_Over48Hours__c
	,ParentId
	,Priority
	,LSB_CAS_SLAViolation__c
	,Status
	,Subject
	,LSB_CAS_SubjectAreaOfInterest__c
	,LSB_CAS_Topic__c
	,Type
	,LSB_CAS_UkEuOrInternationalStudent__c
	,SuppliedCompany
	,SuppliedEmail
	,LSB_CAS_YearOfIntendedStudy__c
	,sfal__StudentID__c
	,LSB_CAS_ReferredTo__c
	,LSB_CAS_Referrer__c
	,LSB_CAS_CurrentPresentingConcerns__c
	,LSB_CAS_ThreatFromOthers__c
	,LSB_CAS_IdentifiedThreatFromOthers__c
	,LSB_CAS_ThreatToOthers__c
	,LSB_CAS_IdentifiedThreatToOthers__c
	,LSB_CAS_ThreatToSelf__c
	,LSB_CAS_IdentifiedRiskToSelf__c
	,LSB_CAS_OtherAppointmentMade__c
	,LSB_CAS_OutcomeMHWB__c
	,LSB_CAS_SignpostedTo__c
	,LSB_CAS_CurrentSafetyConcernsIdentified__c
	,LSB_CAS_InformationGivenToStudent__c
	,LSB_CAS_Method__c
FROM
(SELECT 
	AccountId
	,AssetId
	,Origin
	,Reason
	,RecordTypeId
	,LSB_CAS_CaseResponded__c
	,SourceId
	,hed__Category__c	
	,LSB_CAS_ClosedReason__c
	,LSB_CAS_ClosureComment__c
	,LSB_CAS_FirstName__c
	,LSB_CAS_LastName__c
	,ContactId
	,LSB_CAS_DateOfBirth__c
	,ClosedDate
	,CreatedDate
	,EntitlementId
	,SlaExitDate
	,SlaStartDate
	,Comments
	,LSB_CAS_KnowledgeArticleLink__c
	,Language
	,LastModifiedDate
	,LSB_CAS_LevelOfIntendedStudy__c
	,hed__Location__c
	,LSB_CAS_LSBUCanContactMeVia__c
	,LSB_CAS_LSBUCanContactMeAbout__c
	,REPLACE(T1.LSB_CAS_ExternalID__c, 'WellbeingReferral','WellbeingReferral.' + CAST(ROW_NUMBER() OVER(PARTITION BY T1.LSB_CAS_ExternalID__c ORDER BY T1.LSB_CAS_ExternalID__c) as varchar)) as LSB_CAS_ExternalID__c
	,LSB_CAS_Response__c
	,LSB_CAS_SourceSystem__c
	,LSB_CAS_SourceSystemID__c
	,Description
	,hed__Occurrence_Date__c
	,LSB_CAS_Over48Hours__c
	,ParentId
	,Priority
	,LSB_CAS_SLAViolation__c
	,Status
	,Subject
	,LSB_CAS_SubjectAreaOfInterest__c
	,LSB_CAS_Topic__c
	,Type
	,LSB_CAS_UkEuOrInternationalStudent__c
	,SuppliedCompany
	,SuppliedEmail
	,LSB_CAS_YearOfIntendedStudy__c
	,sfal__StudentID__c
	,LSB_CAS_ReferredTo__c
	,LSB_CAS_Referrer__c
	,LSB_CAS_CurrentPresentingConcerns__c
	,LSB_CAS_ThreatFromOthers__c
	,LSB_CAS_IdentifiedThreatFromOthers__c
	,LSB_CAS_ThreatToOthers__c
	,LSB_CAS_IdentifiedThreatToOthers__c
	, LSB_CAS_ThreatToSelf__c
	,CASE 
		WHEN T1.LSB_CAS_IdentifiedRiskToSelf__c = 'None identified' THEN NULL
		ELSE T1.LSB_CAS_IdentifiedRiskToSelf__c
	END AS LSB_CAS_IdentifiedRiskToSelf__c
	,LSB_CAS_OtherAppointmentMade__c
	,LSB_CAS_OutcomeMHWB__c
	,LSB_CAS_SignpostedTo__c
	,LSB_CAS_CurrentSafetyConcernsIdentified__c
	,LSB_CAS_InformationGivenToStudent__c
	,LSB_CAS_Method__c
FROM 
(SELECT
	 CAST(NULL as varchar(18)) as AccountId
	,CAST(NULL as varchar(18)) as AssetId
	--,CAST(NULL as bit) as LSB_CAS_AvoidValidationField__c
	--,CAST(NULL as varchar(18)) as BusinessHoursId
	--,CAST(NULL as varchar(30)) as CaseNumber
	,CAST('Maximiser' as varchar(255)) as Origin
	--,CAST(NULL as varchar(18)) as OwnerId
	--,CAST(NULL as bit) as LSB_CAS_CaseOwnerIsUser__c
	,CAST(NULL as varchar(255)) as Reason
	,CAST(RT.Id as varchar(18)) as RecordTypeId
	,CAST(NULL as varchar(1300))as LSB_CAS_CaseResponded__c
	,CAST(NULL as varchar(18)) as SourceId
	,CAST(NULL as varchar(255)) as hed__Category__c
	,CAST(CASE
			WHEN CDD.MHWB_Duty_Date_left_duty_list IS NOT NULL THEN 'Resolved'
		END as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(NULL as varchar(max)) as LSB_CAS_ClosureComment__c
	--,CAST(NULL as varchar(80)) as ContactEmail
	--,CAST(NULL as varchar(40)) as ContactFax
	,CAST(NULL as varchar(255)) as LSB_CAS_FirstName__c
	,CAST(NULL as varchar(255)) as LSB_CAS_LastName__c
	--,CAST(NULL as varchar(40)) as ContactMobile
	,CAST(GIDC.Id as varchar(18)) as ContactId
	--,CAST(NULL as varchar(40)) as ContactPhone
	--,CAST(NULL as varchar(18)) as CreatedById
	,CAST(NULL as date) as LSB_CAS_DateOfBirth__c
	,CAST(IIF(CDD.MHWB_Duty_Date_left_duty_list < C.CreateDate, C.CreateDate, CDD.MHWB_Duty_Date_left_duty_list) as datetime) as ClosedDate
	,CAST(C.CreateDate as datetime) as CreatedDate
	,CAST(NULL as varchar(18)) as EntitlementId
	,CAST(NULL as datetime) as SlaExitDate
	,CAST(NULL as datetime) as SlaStartDate
	--,CAST(NULL as bit) as IsEscalated
	--,CAST(NULL as bit) as LSB_CAS_ForReview__c
	,CAST(NULL as varchar(4000))as Comments
	--,CAST(NULL as bit) as LSB_CAS_InternationalEnquiry__c
	--,CAST(NULL as bit) as LSB_CAS_IsMyCase__c
	,CAST(NULL as varchar(255)) as LSB_CAS_KnowledgeArticleLink__c
	,CAST(NULL as varchar(40)) as Language
	,CAST(C.ModifyDate as datetime) as LastModifiedDate
	,CAST(NULL as varchar(255)) as LSB_CAS_LevelOfIntendedStudy__c
	,CAST(NULL as varchar(255)) as hed__Location__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeVia__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeAbout__c
	,CAST(CONCAT(C.CaseId, '.WellbeingReferral') as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST(CASE
			WHEN CDD.MHWB_Duty_Date_left_duty_list IS NOT NULL THEN 'Your case has been closed please contact us if your query is outstanding'
		END as varchar(max)) as LSB_CAS_Response__c
	,CAST('Maximiser' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(C.CaseNumber as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST(NULL as varchar(max)) as Description
	,CAST(NULL as datetime) as hed__Occurrence_Date__c
	,CAST(NULL as varchar(1300))as LSB_CAS_Over48Hours__c
	,CAST(NULL as varchar(18)) as ParentId
	,CAST(NULL as varchar(255)) as Priority
	--,CAST(NULL as bit) as LSB_CAS_FollowUpRequired__c
	,CAST(NULL as varchar(255)) as LSB_CAS_SLAViolation__c
	,CAST(CASE
			WHEN CDD.MHWB_Duty_Date_left_duty_list IS NOT NULL THEN 'Closed'
			ELSE 'Active' 
		END as varchar(255)) as Status
	,CAST(Concat(GIDC.Name, ' - ', IDS.Student_ID, ' - Wellbeing Referral Case') as varchar(255)) as Subject
	,CAST(NULL as varchar(255)) as LSB_CAS_SubjectAreaOfInterest__c
	,CAST(NULL as varchar(255)) as LSB_CAS_Topic__c
	,CAST(NULL as varchar(255)) as Type
	,CAST(NULL as varchar(255)) as LSB_CAS_UkEuOrInternationalStudent__c
	,CAST(NULL as varchar(80)) as SuppliedCompany
	,CAST(NULL as varchar(80)) as SuppliedEmail
	--,CAST(NULL as bit) as LSB_CAS_WillProvideFeedbackViaEmail__c
	,CAST(NULL as varchar(255)) as LSB_CAS_YearOfIntendedStudy__c
	,CAST(IDS.Student_ID as varchar(255)) as sfal__StudentID__c
	,CAST(REPLACE(REPLACE(IDLM.MH__WB_Referred_to
		, 'Healthcare provider (i.e. MH team or IAPT)', 'Healthcare provider(i.e. MH or IAPT)')
		, 'Accomodation', 'Accommodation') as varchar(4099)) AS LSB_CAS_ReferredTo__c
	,CAST(CASE
			WHEN IDLM.MH__WB_Referrer = 'LSBU professional team (Skills, LRC, Library, Security, SLC helpdesk)' THEN 'LSBU professional team'
			ELSE IDLM.MH__WB_Referrer
		  END as varchar(4099)) AS LSB_CAS_Referrer__c
	,CAST(REPLACE(REPLACE(IDLM.MH__WB_Current_presenting_concerns, 'Change in character/mood / behaviour', 'Change in character / mood / behaviour'), 'Victim of sexual assult within last 12 months', 'Victim of sexual assault within last 12 months') as varchar(4099)) AS LSB_CAS_CurrentPresentingConcerns__c
	,CAST(CASE 
			WHEN IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL
			THEN 'TRUE'
			ELSE 'FALSE'
		END  AS varchar(255)
		) AS LSB_CAS_ThreatFromOthers__c  --New
	,CAST(IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others AS varchar(255)) AS LSB_CAS_IdentifiedThreatFromOthers__c  --New
	,CAST(CASE 
			WHEN IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others IS NOT NULL
			THEN 'TRUE'
			ELSE 'FALSE'
		END  AS varchar(255)
		) AS LSB_CAS_ThreatToOthers__c  --New
	,CAST(IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others AS varchar(255)) AS LSB_CAS_IdentifiedThreatToOthers__c  --New
	,CAST(CASE 
			WHEN IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self IS NOT NULL
			THEN 'TRUE'
			ELSE 'FALSE'
		END AS varchar(255)
		) AS LSB_CAS_ThreatToSelf__c  --New
	,CAST(
	
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self
	, 'Alcohol or Substance misuse', 'Alcohol use')
	, 'Other risk behaviour', 'Other (see notes)')
	, 'Self harm', 'Self harm (no medical attention required)')
	, 'Self harm requiring medical attention', 'Serious self harm requiring medical attention')
	, 'Self neglect', 'Self-negligence')
	, 'Sexual risk taking (i.e. chemsex)', 'Sexual risk taking')
	, 'Suicidal behaviour (i.e. overdose)', 'Suicide behaviour')
	, 'Suicidal Ideation', 'Suicide thoughts')
	, 'Suicidal preparation (i.e. gathering means)', 'Suicide preparation'),'Accidental harm (i.e. falling when intoxicated)','Accidental harm')

	as varchar(4099)) AS LSB_CAS_IdentifiedRiskToSelf__c
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2020___2021_Other_appointment_made as varchar(4099)) AS LSB_CAS_OtherAppointmentMade__c
	,CAST(REPLACE(IDLM.MH__WB_MHWB_Appt_Data_Triage_2020___2021_Outcome_MHWB, 'Duty contact 24hrs', 'Duty contact 24 hrs') as varchar(4099)) AS LSB_CAS_OutcomeMHWB__c
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2020___2021_Signposted_to as varchar(4099)) AS LSB_CAS_SignpostedTo__c
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2020___2021_Current_safety_concerns_identified as varchar(4099)) AS LSB_CAS_CurrentSafetyConcernsIdentified__c   --New
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2020___2021_Information_given_to_student  as varchar(4099)) AS LSB_CAS_InformationGivenToStudent__c   --New
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2020___2021_Method  as varchar(4099)) AS LSB_CAS_Method__c   --New
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_WellbeingReferral'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN Maximizer.src.Case_CaseCompanyIndividual CCI ON C.CaseId = CCI.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON CCI.Id = IDLM.Id
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON CCI.Id = IDS.Id
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
WHERE C.Subject = 'MHWB Duty' 
OR
(MH__WB_MHWB_Appt_Data_Triage_2020___2021_Other_appointment_made IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2020___2021_Outcome_MHWB IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2020___2021_Signposted_to IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2020___2021_Current_safety_concerns_identified IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2020___2021_Information_given_to_student IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2020___2021_Method IS NOT NULL
)
UNION ALL
SELECT
	 CAST(NULL as varchar(18)) as AccountId
	,CAST(NULL as varchar(18)) as AssetId
	--,CAST(NULL as bit) as LSB_CAS_AvoidValidationField__c
	--,CAST(NULL as varchar(18)) as BusinessHoursId
	--,CAST(NULL as varchar(30)) as CaseNumber
	,CAST('Maximiser' as varchar(255)) as Origin
	--,CAST(NULL as varchar(18)) as OwnerId
	--,CAST(NULL as bit) as LSB_CAS_CaseOwnerIsUser__c
	,CAST(NULL as varchar(255)) as Reason
	,CAST(RT.Id as varchar(18)) as RecordTypeId
	,CAST(NULL as varchar(1300))as LSB_CAS_CaseResponded__c
	,CAST(NULL as varchar(18)) as SourceId
	,CAST(NULL as varchar(255)) as hed__Category__c
	,CAST(CASE
			WHEN CDD.MHWB_Duty_Date_left_duty_list IS NOT NULL THEN 'Resolved'
		END as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(NULL as varchar(max)) as LSB_CAS_ClosureComment__c
	--,CAST(NULL as varchar(80)) as ContactEmail
	--,CAST(NULL as varchar(40)) as ContactFax
	,CAST(NULL as varchar(255)) as LSB_CAS_FirstName__c
	,CAST(NULL as varchar(255)) as LSB_CAS_LastName__c
	--,CAST(NULL as varchar(40)) as ContactMobile
	,CAST(GIDC.Id as varchar(18)) as ContactId
	--,CAST(NULL as varchar(40)) as ContactPhone
	--,CAST(NULL as varchar(18)) as CreatedById
	,CAST(NULL as date) as LSB_CAS_DateOfBirth__c
	,CAST(IIF(CDD.MHWB_Duty_Date_left_duty_list < C.CreateDate, C.CreateDate, CDD.MHWB_Duty_Date_left_duty_list) as datetime) as ClosedDate
	,CAST(C.CreateDate as datetime)  as CreatedDate
	,CAST(NULL as varchar(18)) as EntitlementId
	,CAST(NULL as datetime) as SlaExitDate
	,CAST(NULL as datetime) as SlaStartDate
	--,CAST(NULL as bit) as IsEscalated
	--,CAST(NULL as bit) as LSB_CAS_ForReview__c
	,CAST(NULL as varchar(4000))as Comments
	--,CAST(NULL as bit) as LSB_CAS_InternationalEnquiry__c
	--,CAST(NULL as bit) as LSB_CAS_IsMyCase__c
	,CAST(NULL as varchar(255)) as LSB_CAS_KnowledgeArticleLink__c
	,CAST(NULL as varchar(40)) as Language
	,CAST(C.ModifyDate as dateTIME) as LastModifiedDate
	,CAST(NULL as varchar(255)) as LSB_CAS_LevelOfIntendedStudy__c
	,CAST(NULL as varchar(255)) as hed__Location__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeVia__c
	,CAST(NULL as varchar(4099))as LSB_CAS_LSBUCanContactMeAbout__c
	,CAST(CONCAT(C.CaseId, '.WellbeingReferral') as varchar(90)) as LSB_CAS_ExternalID__c
	,CAST(CASE
			WHEN CDD.MHWB_Duty_Date_left_duty_list IS NOT NULL THEN 'Your case has been closed please contact us if your query is outstanding'
		END as varchar(max)) as LSB_CAS_Response__c
	,CAST('Maximiser' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(c.CaseNumber as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST(NULL as varchar(max)) as Description
	,CAST(NULL as datetime) as hed__Occurrence_Date__c
	,CAST(NULL as varchar(1300))as LSB_CAS_Over48Hours__c
	,CAST(NULL as varchar(18)) as ParentId
	,CAST(NULL as varchar(255)) as Priority
	--,CAST(NULL as bit) as LSB_CAS_FollowUpRequired__c
	,CAST(NULL as varchar(255)) as LSB_CAS_SLAViolation__c
	,CAST(CASE
			WHEN CDD.MHWB_Duty_Date_left_duty_list IS NOT NULL THEN 'Closed'
			ELSE 'Active' 
		END as varchar(255)) as Status
	,CAST(Concat(GIDC.Name, ' - ', IDS.Student_ID, ' - Wellbeing Referral Case') as varchar(255)) as Subject
	,CAST(NULL as varchar(255)) as LSB_CAS_SubjectAreaOfInterest__c
	,CAST(NULL as varchar(255)) as LSB_CAS_Topic__c
	,CAST(NULL as varchar(255)) as Type
	,CAST(NULL as varchar(255)) as LSB_CAS_UkEuOrInternationalStudent__c
	,CAST(NULL as varchar(80)) as SuppliedCompany
	,CAST(NULL as varchar(80)) as SuppliedEmail
	--,CAST(NULL as bit) as LSB_CAS_WillProvideFeedbackViaEmail__c
	,CAST(NULL as varchar(255)) as LSB_CAS_YearOfIntendedStudy__c
	,CAST(IDS.Student_ID as varchar(255)) as sfal__StudentID__c
	,CAST(REPLACE(REPLACE(IDLM.MH__WB_Referred_to
		, 'Healthcare provider (i.e. MH team or IAPT)', 'Healthcare provider(i.e. MH or IAPT)')
		, 'Accomodation', 'Accommodation') as varchar(4099)) AS LSB_CAS_ReferredTo__c
	,CAST(CASE
			WHEN IDLM.MH__WB_Referrer = 'LSBU professional team (Skills, LRC, Library, Security, SLC helpdesk)' THEN 'LSBU professional team'
			ELSE IDLM.MH__WB_Referrer
		  END as varchar(4099)) AS LSB_CAS_Referrer__c
	,CAST(REPLACE(REPLACE(IDLM.MH__WB_Current_presenting_concerns, 'Change in character/mood / behaviour', 'Change in character / mood / behaviour'), 'Victim of sexual assult within last 12 months', 'Victim of sexual assault within last 12 months') as varchar(4099)) AS LSB_CAS_CurrentPresentingConcerns__c
	,CAST(CASE 
			WHEN IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others IS NOT NULL
			THEN 'TRUE'
			ELSE 'FALSE'
		END  AS varchar(255)
		) AS LSB_CAS_ThreatFromOthers__c  --New
	,CAST(IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_from_others AS varchar(255)) AS LSB_CAS_IdentifiedThreatFromOthers__c  --New
	,CAST(CASE 
			WHEN IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others IS NOT NULL
			THEN 'TRUE'
			ELSE 'FALSE'
		END  AS varchar(255)
		) AS LSB_CAS_ThreatToOthers__c  --New
	,CAST(IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_others AS varchar(255)) AS LSB_CAS_IdentifiedThreatToOthers__c  --New
	,CAST(CASE 
			WHEN IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self IS NOT NULL
			THEN 'TRUE'
			ELSE 'FALSE'
		END AS varchar(255)
		) AS LSB_CAS_ThreatToSelf__c  --New
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(IDLM.MH__WB_MH__WB_Current_Risk_Identified_risk_to_self
		, 'Alcohol or Substance misuse', 'Alcohol use')
		, 'Other risk behaviour', 'Other (see notes)')
		, 'Self harm', 'Self harm (no medical attention required)')
		, 'Self harm requiring medical attention', 'Serious self harm requiring medical attention')
		, 'Self neglect', 'Self-negligence')
		, 'Sexual risk taking (i.e. chemsex)', 'Sexual risk taking')
		, 'Suicidal behaviour (i.e. overdose)', 'Suicide behaviour')
		, 'Suicidal Ideation', 'Suicide thoughts')
		, 'Suicidal preparation (i.e. gathering means)', 'Suicide preparation'),'Accidental harm (i.e. falling when intoxicated)','Accidental harm') as varchar(4099)) AS LSB_CAS_IdentifiedRiskToSelf__c
	,CAST(NULL as varchar(4099)) AS LSB_CAS_OtherAppointmentMade__c						-- Not found
	,CAST(REPLACE(IDLM.MH__WB_MHWB_Appt_Data_Triage_2019___2020_Outcome_MHWB, 'Duty contact 24hrs', 'Duty contact 24 hrs') as varchar(4099)) AS LSB_CAS_OutcomeMHWB__c
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2019___2020_Signposted_to as varchar(4099)) AS LSB_CAS_SignpostedTo__c
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2019___2020_Current_safety_concerns_identified as varchar(4099)) AS LSB_CAS_CurrentSafetyConcernsIdentified__c   --New
	,CAST(Null  as varchar(4099)) AS LSB_CAS_InformationGivenToStudent__c   --New
	,CAST(IDLM.MH__WB_MHWB_Appt_Data_Triage_2019___2020_Method  as varchar(4099)) AS LSB_CAS_Method__c   --New
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_CAS_WellbeingReferral'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN Maximizer.src.Case_CaseCompanyIndividual CCI ON C.CaseId = CCI.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailListMulti IDLM ON CCI.Id = IDLM.Id
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON CCI.Id = IDS.Id
LEFT JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
WHERE C.Subject = 'MHWB Duty' 
OR
(MH__WB_MHWB_Appt_Data_Triage_2019___2020_Outcome_MHWB IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2019___2020_Signposted_to IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2019___2020_Current_safety_concerns_identified IS NOT NULL
OR MH__WB_MHWB_Appt_Data_Triage_2019___2020_Method IS NOT NULL
)
) AS T1
) AS T2
WHERE T2.LSB_CAS_ExternalID__c NOT IN (select LSB_CAS_ExternalID__c from sfc.Get_ID_Case WHERE LSB_CAS_ExternalID__c IS NOT NULL)