CREATE OR ALTER VIEW [DST].[FitnesstoStudyDetail_MAXFitnesstoStudy] AS
-------------------------------- Initial referral --------------------------------------------------
select
	RecordTypeId
	,CreatedDate
	,CreatedById
	,LastModifiedDate
	,LastModifiedById
	,LSB_FIL_FitnessToStudy__c
	,LSB_FIL_COPLetterRequested__c
	,LSB_FIL_CompanionAttended__c
	,LSB_FIL_DateCOPLetterProvided__c
	,LSB_FIL_DateOfFirstReferral__c
	,LSB_FIL_DateOfFollowUp__c
	,LSB_FIL_DateOfInformalMeeting__c
	,LSB_FIL_DateOfInterruption__c
	,LSB_FIL_DateOfInvitation__c
	,LSB_FIL_DateOfPanel__c
	,LSB_FIL_DateOfReviewPanel__c
	,LSB_FIL_DateOfReview__c
	,LSB_FIL_DateReferredToDeputyVChancellor__c
	,LSB_FIL_DateReviewRequested__c
	,LSB_FIL_DirectorOfSSEDecision__c
	,LSB_FIL_DocumentsProvided__c
	,LSB_FIL_DocumentsRequested__c
	,LSB_FIL_ExternalId__c
	,LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,LSB_FIL_ImmediateSuspension__c
	,LSB_FIL_InterruptionType__c
	,LSB_FIL_LeadPersonName__c
	,LSB_FIL_LeadPersonRole__c
	,LSB_FIL_MeetingRescheduled__c
	,LSB_FIL_NatureOfConcern__c
	,LSB_FIL_Outcome__c
	,LSB_FIL_ReasonForInterruption__c
	,LSB_FIL_Referrer__c
	,LSB_FIL_ResponseFromStudent__c
	,LSB_FIL_ReviewRequestedBy__c
	,LSB_FIL_ScheduledDateToContactAboutRetur__c
	,LSB_FIL_SignpostedServices__c
	,LSB_FIL_SourceSystemId__c
	,LSB_FIL_SourceSystem__c
	,LSB_FIL_StudentAttendanceConfirmed__c
	,LSB_FIL_StudentAttended__c
	,LSB_FIL_StudentConfirmed__c
	,LSB_FIL_StudentInformedOfReviewOutcome__c
	,LSB_FIL_StudentPlanningToReturn__c
from
(Select																								
	--CAST(NULL as varchar(80)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(0 as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(CDD.Fitness_to_Study_Initial_referral_Date_of_first_referral as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfFollowUp__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInvitation__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(0 as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(0 as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.InitialReferral.',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(CASE 
			WHEN CDLM.Fitness_to_Study_Initial_referral_Immediate_Suspension = 'No' THEN 0
			WHEN CDLM.Fitness_to_Study_Initial_referral_Immediate_Suspension = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(REPLACE(CDLM.Fitness_to_Study_Initial_referral_Nature_of_concern, 'Possible mental health condition affecting study/self/others','MHWB affecting study/self/others') as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CDLM.Fitness_to_Study_Initial_referral_Outcome_of_referral
		, 'Fitness to Study Level 1', 'FTS Level 1')
		, 'Fitness to Study Level 2', 'FTS Level 2')
		, 'Fitness to Study Level 3', 'FTS Level 3')
		, 'Fitness to Study Level 4 (Return to Study)', 'FTS Level 4 (Return to Study)')
		, 'Interruption recommended by panel', 'Interruption Recommended')
		, 'Not Fitness to Study - signposted to support', 'Not FTS - Signposted to Support')
		, 'Signposting to support', 'Not FTS - Signposted to Support') 
		, 'Informal signposting - closed', 'Informal Signposting - Closed')
		, 'Support plan agreed - no formal review date required', 'SP agreed - Formal review Not Required')
		, 'Support plan agreed/updated - review level 3 panel set', 'SP agreed/updated - Review L3 Panel Set')
		, 'Support plan agreed - level 2 review date set', 'SP agreed - L2 Review Date Set')
		, 'Interruption recommended', 'Interruption Recommended') 
		, 'Support Plan Agreed/Updated - Review Level 2 Conference Set','SP agreed/updated - Review L2 Conf Set')
		,'Support plan agreed/updated - review level 2 conference set','SP agreed/updated - Review L2 Conf Set' )as varchar(255)) AS LSB_FIL_Outcome__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(CDLM.Fitness_to_Study_Initial_referral_Referrer as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(0 as bit) AS LSB_FIL_StudentAttended__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentConfirmed__c
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_InitialReferral'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDLM.Fitness_to_Study_Initial_referral_Immediate_Suspension IS NOT NULL
OR CDLM.Fitness_to_Study_Initial_referral_Nature_of_concern IS NOT NULL
OR CDLM.Fitness_to_Study_Initial_referral_Outcome_of_referral IS NOT NULL
OR CDLM.Fitness_to_Study_Initial_referral_Referrer IS NOT NULL
OR CDD.Fitness_to_Study_Initial_referral_Date_of_first_referral IS NOT NULL
)

UNION ALL

------------------------------------------------ LSB_FIL_L1Informal------------------------------------------------------

Select																								
	--CAST(NULL as varchar(80)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(0 as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfFollowUp__c										--	FIELDZFitness to Study/Level 1 (Informal)/Date of follow up not found
	,CAST(CDD.Fitness_to_Study_Level_1__Informal__Date_of_informal_meeting as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInvitation__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(0 as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(0 as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L1Informal.',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(CDS.Fitness_to_Study_Level_1__Informal__Lead_person_name as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(CDLM.Fitness_to_Study_Level_1__Informal__Lead_person_role as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CDLM.Fitness_to_Study_Level_1__Informal__Outcome
		, 'Fitness to Study Level 1', 'FTS Level 1')
		, 'Fitness to Study Level 2', 'FTS Level 2')
		, 'Fitness to Study Level 3', 'FTS Level 3')
		, 'Fitness to Study Level 4 (Return to Study)', 'FTS Level 4 (Return to Study)')
		, 'Interruption recommended by panel', 'Interruption Recommended')
		, 'Not Fitness to Study - signposted to support', 'Not FTS - Signposted to Support')
		, 'Signposting to support', 'Not FTS - Signposted to Support') 
		, 'Informal signposting - closed', 'Informal Signposting - Closed')
		, 'Support plan agreed - no formal review date required', 'SP agreed - Formal review Not Required')
		, 'Support plan agreed/updated - review level 3 panel set', 'SP agreed/updated - Review L3 Panel Set')
		, 'Support plan agreed - level 2 review date set', 'SP agreed - L2 Review Date Set')
		, 'Interruption recommended', 'Interruption Recommended')
		, 'Support Plan Agreed/Updated - Review Level 2 Conference Set','SP agreed/updated - Review L2 Conf Set')
		,'Support plan agreed/updated - review level 2 conference set','SP agreed/updated - Review L2 Conf Set' ) as varchar(255)) AS LSB_FIL_Outcome__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(CDLM.Fitness_to_Study_Level_1__Informal__Signposted_Services as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(0 as bit) AS LSB_FIL_StudentAttended__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentConfirmed__c
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L1Informal'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailString CDS ON C.CaseId = CDS.CaseId
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDLM.Fitness_to_Study_Level_1__Informal__Lead_person_role IS NOT NULL
OR CDLM.Fitness_to_Study_Level_1__Informal__Outcome IS NOT NULL
OR CDLM.Fitness_to_Study_Level_1__Informal__Signposted_Services IS NOT NULL
OR CDS.Fitness_to_Study_Level_1__Informal__Lead_person_name IS NOT NULL
OR CDD.Fitness_to_Study_Level_1__Informal__Date_of_informal_meeting IS NOT NULL
)

UNION ALL
-----------------------------------------------------------------LSB_FIL_L2CaseConference(first)----------------------------------------------------------
Select																								
	--CAST(NULL as varchar(18)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Outcome_Date_of_follow_up as datetime) AS LSB_FIL_DateOfFollowUp__c										--	FIELDZFitness to Study/Level 1 (Informal)/Date of follow up not found
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Date_of_invitation as date) AS LSB_FIL_DateOfInvitation__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L2CaseConference(first).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST('First' as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Outcome
		, 'Fitness to Study Level 1', 'FTS Level 1')
		, 'Fitness to Study Level 2', 'FTS Level 2')
		, 'Fitness to Study Level 3', 'FTS Level 3')
		, 'Fitness to Study Level 4 (Return to Study)', 'FTS Level 4 (Return to Study)')
		, 'Interruption recommended by panel', 'Interruption Recommended')
		, 'Not Fitness to Study - signposted to support', 'Not FTS - Signposted to Support')
		, 'Signposting to support', 'Not FTS - Signposted to Support') 
		, 'Informal signposting - closed', 'Informal Signposting - Closed')
		, 'Support plan agreed - no formal review date required', 'SP agreed - Formal review Not Required')
		, 'Support plan agreed/updated - review level 3 panel set', 'SP agreed/updated - Review L3 Panel Set')
		, 'Support plan agreed - level 2 review date set', 'SP agreed - L2 Review Date Set')
		, 'Interruption recommended', 'Interruption Recommended')
		, 'Support Plan Agreed/Updated - Review Level 2 Conference Set','SP agreed/updated - Review L2 Conf Set')
		,'Support plan agreed/updated - review level 2 conference set','SP agreed/updated - Review L2 Conf Set' ) as varchar(255)) AS LSB_FIL_Outcome__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_confirmed as varchar(255)) AS LSB_FIL_StudentConfirmed__c
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L2CaseConference'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDD.Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services IS NOT NULL
OR CDD.Fitness_to_Study_Level_2__Case_Conference__Outcome_Date_of_follow_up IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Outcome IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Companion_attended IS NOT NULL
OR CDD.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Date_of_invitation IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_provided IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Documents_requested IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_attended IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Invitation_and_attendance_Student_confirmed IS NOT NULL
)

UNION ALL
----------------------------------------------------------- LSB_FIL_L2CaseConference(Review)-------------------------------------------------------
Select																								
	--CAST(NULL as varchar(18)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfFollowUp__c			
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_invitation as date) AS LSB_FIL_DateOfInvitation__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_review as datetime) AS LSB_FIL_DateOfReview__c
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L2CaseConference(review).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST('Review' as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Outcome
		, 'Fitness to Study Level 1', 'FTS Level 1')
		, 'Fitness to Study Level 2', 'FTS Level 2')
		, 'Fitness to Study Level 3', 'FTS Level 3')
		, 'Fitness to Study Level 4 (Return to Study)', 'FTS Level 4 (Return to Study)')
		, 'Interruption recommended by panel', 'Interruption Recommended')
		, 'Not Fitness to Study - signposted to support', 'Not FTS - Signposted to Support')
		, 'Signposting to support', 'Not FTS - Signposted to Support') 
		, 'Informal signposting - closed', 'Informal Signposting - Closed')
		, 'Support plan agreed - no formal review date required', 'SP agreed - Formal review Not Required')
		, 'Support plan agreed/updated - review level 3 panel set', 'SP agreed/updated - Review L3 Panel Set')
		, 'Support plan agreed - level 2 review date set', 'SP agreed - L2 Review Date Set')
		, 'Interruption recommended', 'Interruption Recommended')
		, 'Support Plan Agreed/Updated - Review Level 2 Conference Set','SP agreed/updated - Review L2 Conf Set')
		,'Support plan agreed/updated - review level 2 conference set','SP agreed/updated - Review L2 Conf Set' ) as varchar(255)) AS LSB_FIL_Outcome__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentConfirmed__c														-- FIELDZFitness to Study/Level 2 (Case Conference)/Review Case Conference/Student confirmed	 NOT FOUND							
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L2CaseConference'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDD.Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services IS NOT NULL)
AND
(CDD.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_invitation IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Companion_attended IS NOT NULL
OR CDD.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Date_of_review IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_provided IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Documents_requested IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Outcome IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Review_Case_Conference_Student_attended IS NOT NULL
)

UNION ALL
----------------------------------------------------- LSB_FIL_L2CaseConference(Second Review)-------------------------------------------------
Select																								
	--CAST(NULL as varchar(18)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Outcome_Date_of_follow_up as datetime) AS LSB_FIL_DateOfFollowUp__c			
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInvitation__c  -- FIELDZFitness to Study/Level 2 (Case Conference)/Second review case conference/Date of invitation NOT FOUND
	,CAST(CDD.Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c		-- FIELDZFitness to Study/Level 2 (Case Conference)/Second review case conference/Date of review NOT FOUND	
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END  as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L2CaseConference(second review).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST('Review' as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_Outcome__c						-- FIELDZFitness to Study/Level 2 (Case Conference)/Second review case conference/Outcome not found
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentConfirmed__c									-- FIELDZFitness to Study/Level 2 (Case Conference)/Second review case conference/Student confirmed	 NOT FOUND							
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L2CaseConference'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDD.Fitness_to_Study_Level_2__Case_Conference__Date_of_case_conference IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Outcome_Signposted_Services IS NOT NULL)
AND
(CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Companion_attended IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_provided IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Documents_requested IS NOT NULL
OR CDLM.Fitness_to_Study_Level_2__Case_Conference__Second_review_case_conference_Student_attended IS NOT NULL
)

UNION ALL
------------------------------------------------------ LSB_FIL_L3Panel(First)--------------------------------------------------
Select																								
	--CAST(NULL as varchar(18)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as varchar(255)) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(CDD.Fitness_to_Study_Level_3__Panel__Outcome_Date_of_follow_up as datetime) AS LSB_FIL_DateOfFollowUp__c			
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(CDD.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Date_of_invitation as date) AS LSB_FIL_DateOfInvitation__c   
	,CAST(CDD.Fitness_to_Study_Level_3__Panel__Date_of_Panel as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c		
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L3Panel(first).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST('First' as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CDLM.Fitness_to_Study_Level_3__Panel__Outcome_Outcome
		, 'Fitness to Study Level 1', 'FTS Level 1')
		, 'Fitness to Study Level 2', 'FTS Level 2')
		, 'Fitness to Study Level 3', 'FTS Level 3')
		, 'Fitness to Study Level 4 (Return to Study)', 'FTS Level 4 (Return to Study)')
		, 'Interruption recommended by panel', 'Interruption Recommended')
		, 'Not Fitness to Study - signposted to support', 'Not FTS - Signposted to Support')
		, 'Signposting to support', 'Not FTS - Signposted to Support') 
		, 'Informal signposting - closed', 'Informal Signposting - Closed')
		, 'Support plan agreed - no formal review date required', 'SP agreed - Formal review Not Required')
		, 'Support plan agreed/updated - review level 3 panel set', 'SP agreed/updated - Review L3 Panel Set')
		, 'Support plan agreed - level 2 review date set', 'SP agreed - L2 Review Date Set')
		, 'Interruption recommended', 'Interruption Recommended')
		, 'Support Plan Agreed/Updated - Review Level 2 Conference Set','SP agreed/updated - Review L2 Conf Set')
		,'Support plan agreed/updated - review level 2 conference set','SP agreed/updated - Review L2 Conf Set' ) as varchar(255)) AS LSB_FIL_Outcome__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(CDLM.Fitness_to_Study_Level_3__Panel__Outcome_Signposted_Services as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_confirmed as varchar(255)) AS LSB_FIL_StudentConfirmed__c													
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L3Panel'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDLM.Fitness_to_Study_Level_3__Panel__Outcome_Outcome IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Outcome_Signposted_Services IS NOT NULL
OR CDD.Fitness_to_Study_Level_3__Panel__Outcome_Date_of_follow_up IS NOT NULL
OR CDD.Fitness_to_Study_Level_3__Panel__Date_of_Panel IS NOT NULL
OR CDD.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Date_of_invitation IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Companion_attended IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_provided IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Documents_requested  IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_confirmed IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Invitation_and_attendance_Student_attended IS NOT NULL
)

UNION ALL

------------------------------------------------------ LSB_FIL_L3Panel (Review) --------------------------------------------------
Select																								
	--CAST(NULL as varchar(18)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(CDD.Fitness_to_Study_Level_3__Panel__Outcome_Date_of_follow_up as datetime) AS LSB_FIL_DateOfFollowUp__c			
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(CDD.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_invitation as date) AS LSB_FIL_DateOfInvitation__c   
	,CAST(NULL as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(CDD.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_Review_Panel as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c		
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L3Panel(review).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST('Review' as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Outcome_of_Review
		, 'Fitness to Study Level 1', 'FTS Level 1')
		, 'Fitness to Study Level 2', 'FTS Level 2')
		, 'Fitness to Study Level 3', 'FTS Level 3')
		, 'Fitness to Study Level 4 (Return to Study)', 'FTS Level 4 (Return to Study)')
		, 'Interruption recommended by panel', 'Interruption Recommended')
		, 'Not Fitness to Study - signposted to support', 'Not FTS - Signposted to Support')
		, 'Signposting to support', 'Not FTS - Signposted to Support') 
		, 'Informal signposting - closed', 'Informal Signposting - Closed')
		, 'Support plan agreed - no formal review date required', 'SP agreed - Formal review Not Required')
		, 'Support plan agreed/updated - review level 3 panel set', 'SP agreed/updated - Review L3 Panel Set')
		, 'Support plan agreed - level 2 review date set', 'SP agreed - L2 Review Date Set')
		, 'Interruption recommended', 'Interruption Recommended')
		, 'Support Plan Agreed/Updated - Review Level 2 Conference Set','SP agreed/updated - Review L2 Conf Set')
		,'Support plan agreed/updated - review level 2 conference set','SP agreed/updated - Review L2 Conf Set' ) as varchar(255)) AS LSB_FIL_Outcome__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Signposted_Services as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attedance_confirmed as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentConfirmed__c									-- FIELDZFitness to Study/Level 2 (Case Conference)/Second review case conference/Student confirmed	 NOT FOUND							
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L3Panel'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'

AND 
(Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Companion_attended IS NOT NULL
OR CDD.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_Review_Panel IS NOT NULL
OR CDD.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Date_of_invitation IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Signposted_Services IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_provided  IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Documents_requested IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Outcome_of_Review IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attedance_confirmed IS NOT NULL
OR CDLM.Fitness_to_Study_Level_3__Panel__Review_Panel_Level_3_Student_attended IS NOT NULL
)


UNION ALL

------------------------------------------------------ LSB_FIL_L4Return (underFtS) --------------------------------------------------
Select																								
	--CAST(NULL as varchar(18)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c				-- nOT AVAILABLE
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfFollowUp__c							--Not Available	
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(CDD.Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Date_of_interruption as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInvitation__c									-- Not Available
	,CAST(NULL as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c		
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L4Return(underFtS).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(CDLM.Fitness_to_Study_Level_4__Return__Interruption_type as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_Outcome__c									--Not Available
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Response_from_student = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(CDD.Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Scheduled_date_to_contact_about_return as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_confirmed as varchar(255)) AS LSB_FIL_StudentConfirmed__c															
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Interruption_under_FtS_Student_planning_to_return = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L4Return'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND CDLM.Fitness_to_Study_Level_4__Return__Interruption_type IN ('Fitness to Study Level 3 outcome','Student choice (Fitness to Study)')


UNION ALL

------------------------------------------------------ LSB_FIL_L4Return (outsideFtS) --------------------------------------------------
Select																								
	--CAST(NULL as varchar(80)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(0 as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Companion_attended = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c				-- nOT AVAILABLE
	,CAST(NULL as datetime) AS LSB_FIL_DateCOPLetterProvided__c
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfFollowUp__c							--Not Available	
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(CDD.Fitness_to_Study_Level_4__Return__Interruption_outside_FtS_Date_of_interruption as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInvitation__c									-- Not Available
	,CAST(NULL as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c		
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c
	,CAST(NULL as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_provided = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Documents_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.L4Return(outsideFtS).',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(CDLM.Fitness_to_Study_Level_4__Return__Interruption_type as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_Outcome__c									--Not Available
	,CAST(CDLM.Fitness_to_Study_Level_4__Return__Interruption_outside_FtS_Reason_for_interruption as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_attended = 'Yes' THEN 1 
			ELSE 0
		  END as bit) AS LSB_FIL_StudentAttended__c
	,CAST(CDLM.Fitness_to_Study_Level_4__Return__Return_Fitness_to_Study_Panel_Student_confirmed as varchar(255)) AS LSB_FIL_StudentConfirmed__c															
	,CAST(NULL as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_L4Return'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND CDLM.Fitness_to_Study_Level_4__Return__Interruption_type IN ('Student choice (not Fitness to Study)')

UNION ALL

------------------------------------------------------ LSB_FIL_ReviewOfDecisions --------------------------------------------------
Select																								
	--CAST(NULL as varchar(80)) AS Name
	CAST(RT.Id as varchar(18)) AS RecordTypeId
	,CAST(C.CreateDate as datetime) AS CreatedDate
	,CAST(CbyId.Id as varchar(18)) AS CreatedById
	,CAST(C.ModifyDate as datetime) AS LastModifiedDate
	,CAST(LbyId.Id as varchar(18)) AS LastModifiedById
	,CAST(GIC.Id as varchar(18)) AS LSB_FIL_FitnessToStudy__c
	,CAST(CASE
			WHEN CDLM.Fitness_to_Study_Review_of_decisions_COP_favourable_to_student_Completion_of_Procedures_letter_requested = 'Yes' THEN 1
			ELSE 0
		  END as bit) AS LSB_FIL_COPLetterRequested__c
	,CAST(0 as bit) AS LSB_FIL_CompanionAttended__c
	--,CAST(NULL as bit) AS LSB_FIL_ContactedAboutReturn__c				
	,CAST(CDD.Fitness_to_Study_Review_of_decisions_COP_unfavourable_to_student_Date_COP_letter_provided as datetime) AS LSB_FIL_DateCOPLetterProvided__c				
	,CAST(NULL as date) AS LSB_FIL_DateOfFirstReferral__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfFollowUp__c						
	,CAST(NULL as datetime) AS LSB_FIL_DateOfInformalMeeting__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInterruption__c
	,CAST(NULL as date) AS LSB_FIL_DateOfInvitation__c								
	,CAST(NULL as datetime) AS LSB_FIL_DateOfPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReviewPanel__c
	,CAST(NULL as datetime) AS LSB_FIL_DateOfReview__c		
	,CAST(NULL as datetime) AS LSB_FIL_DateReferredToDeputyVChancellor__c					-- FIELDZFitness to Study/Review of decisions/Date referred to Deputy Vice Chancellor not available
	,CAST(CDD.Fitness_to_Study_Review_of_decisions_Date_review_requested as date) AS LSB_FIL_DateReviewRequested__c
	,CAST(CDLM.Fitness_to_Study_Review_of_decisions_Director_of_SSE_decision as varchar(255)) AS LSB_FIL_DirectorOfSSEDecision__c
	,CAST(0 as bit) AS LSB_FIL_DocumentsProvided__c
	,CAST(0 as bit) AS LSB_FIL_DocumentsRequested__c
	,CAST(CONCAT(C.CaseId,'.ReviewOfDecisions.',ROW_NUMBER() OVER (PARTITION BY C.CaseId Order by C.CaseId)) as varchar(255)) AS LSB_FIL_ExternalId__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_FirstMeetingAtThisLevelOrAReview__c
	,CAST(0 as bit) AS LSB_FIL_ImmediateSuspension__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_InterruptionType__c
	,CAST(NULL as varchar(90)) AS LSB_FIL_LeadPersonName__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_LeadPersonRole__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_MeetingRescheduled__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_NatureOfConcern__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_Outcome__c									--FIELDZFitness to Study/Review of decisions/Review outcome Not Available
	,CAST(NULL as varchar(4099)) AS LSB_FIL_ReasonForInterruption__c										-- Multi Select picklist
	,CAST(NULL as varchar(4099)) AS LSB_FIL_Referrer__c      -- Multi Select picklist
	,CAST(0 as bit) AS LSB_FIL_ResponseFromStudent__c
	,CAST(CDLM.Fitness_to_Study_Review_of_decisions_Review_requested_by as varchar(255)) AS LSB_FIL_ReviewRequestedBy__c
	,CAST(NULL as date) AS LSB_FIL_ScheduledDateToContactAboutRetur__c
	,CAST(NULL as varchar(4099)) AS LSB_FIL_SignpostedServices__c                                         -- Multi Select picklist
	,CAST(C.CaseId as varchar(255)) AS LSB_FIL_SourceSystemId__c
	,CAST('Maximiser' as varchar(255)) AS LSB_FIL_SourceSystem__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentAttendanceConfirmed__c
	,CAST(0 as bit) AS LSB_FIL_StudentAttended__c
	,CAST(NULL as varchar(255)) AS LSB_FIL_StudentConfirmed__c															
	,CAST(CDD.Fitness_to_Study_Review_of_decisions_Date_student_informed_of_review_outcome as datetime) AS LSB_FIL_StudentInformedOfReviewOutcome__c
	,CAST(0 as bit) AS LSB_FIL_StudentPlanningToReturn__c
FROM Maximizer.src.[Case] C
LEFT JOIN SFC.RecordType RT ON RT.DeveloperName = 'LSB_FIL_ReviewOfDecisions'
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailDate CDD ON C.CaseId = CDD.CaseId
LEFT JOIN INT.SRC_Maximizer_PIVOT_CaseDetailListMulti CDLM ON C.CaseId = CDLM.CaseId 
LEFT JOIN SFC.Get_ID_Case GIC ON GIC.LSB_CAS_ExternalID__c = CONCAT(C.CaseId, '.FitnesstoStudy')
LEFT JOIN SFC.Get_ID_User CbyId ON C.CreatedBy = SUBSTRING(CbyId.Username,1,(CHARINDEX('@',CbyId.Username)-1))
LEFT JOIN SFC.Get_ID_User LbyId ON C.ModifiedBy = SUBSTRING(LbyId.Username,1,(CHARINDEX('@',LbyId.Username)-1))
WHERE C.Subject = 'Fitness to Study'
AND 
(CDLM.Fitness_to_Study_Review_of_decisions_COP_favourable_to_student_Completion_of_Procedures_letter_requested IS NOT NULL
OR CDLM.Fitness_to_Study_Review_of_decisions_Director_of_SSE_decision IS NOT NULL
OR CDLM.Fitness_to_Study_Review_of_decisions_Review_requested_by IS NOT NULL
OR CDD.Fitness_to_Study_Review_of_decisions_COP_unfavourable_to_student_Date_COP_letter_provided IS NOT NULL
OR CDD.Fitness_to_Study_Review_of_decisions_Date_review_requested IS NOT NULL
OR CDD.Fitness_to_Study_Review_of_decisions_Date_student_informed_of_review_outcome IS NOT NULL
)) AS T1
WHERE T1.LSB_FIL_ExternalId__c NOT IN (select LSB_FIL_ExternalId__c from sfc.Get_ID_FitnessToStudyDetail WHERE LSB_FIL_ExternalId__c IS NOT NULL)