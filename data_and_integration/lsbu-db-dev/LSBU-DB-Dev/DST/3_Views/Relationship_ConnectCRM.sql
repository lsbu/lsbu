CREATE OR ALTER VIEW [DST].[Relationship_ConnectCRM] AS
SELECT 
  CAST(GIC.ContactId as varchar(18)) AS hed__Contact__c
 ,cast( '1' as bit ) AS hed__Emergency_Contact__c
 ,CAST(GICN.ID as varchar(18)) AS hed__RelatedContact__c
 ,CAST('Current' as varchar(255)) AS hed__Status__c
 ,CAST(DCRL.DestinationValue as varchar(255)) AS hed__Type__c
 ,CAST('Connect' as varchar(255)) AS LSB_RTP_SourceSystem__c
 ,CAST(CONCAT(SM.Connect_ID,SM.Emergency_Contact_Relationship,'.Connect.R' ) as varchar(255)) AS LSB_RTP_SourceSystemID__c        
 ,CAST(CONCAT(SM.Connect_ID,SM.Emergency_Contact_Relationship,'.Connect.R' ) as varchar(255)) AS LSB_RTP_ExternalId__c 
 ,(select id from sfc.RecordType
	where name like 'Relationship'
	and SobjectType='hed__Relationship__c') as RecordTypeId

FROM INT.SRC_Connect_Salesforce_Migration SM
   JOIN [SFC].[Get_ID_Case] GIC ON GIC.LSB_CAS_ExternalID__c = CAST(CONCAT(SM.Connect_ID,'.','Connect') as varchar(18)) and gic.ContactId is not null
		JOIN [SFC].[Get_ID_Contact] GICN ON GICN.LSB_ExternalID__c = CONCAT(SM.Connect_ID,SM.Emergency_Contact_Relationship,'.Connect' )
		JOIN [SFC].[Get_ID_Contact] c ON c.Id = GIC.ContactId
   LEFT JOIN [DST].[DICT_ConnectCRMContactRelationship] DCRL ON DCRL.SourceValue = SM.Emergency_Contact_Relationship
