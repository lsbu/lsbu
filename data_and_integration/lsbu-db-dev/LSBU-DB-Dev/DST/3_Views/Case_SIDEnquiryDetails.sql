CREATE OR ALTER VIEW [DST].[Case_SIDEnquiryDetails] AS
SELECT DISTINCT 
 	 Origin
	,OwnerId
 	,RecordTypeId
	,LSB_CAS_ClosedReason__c
	,ContactId
	,CreatedDate
	,LSB_CAS_ExternalID__c
  ,LSB_CAS_Response__c
	,LSB_CAS_SourceSystem__c
	,LSB_CAS_SourceSystemID__c
	,Status
	,Subject
	,LSB_CAS_LegacyDescription__c  
  ,LSB_CAS_ContactRole__c  
FROM (
SELECT
	 ROW_NUMBER() OVER(PARTITION BY (ED.HCL_CODE+'.SID') ORDER BY OID.Username) as RNK
	,CAST('SID' as varchar(255)) as Origin
	,CAST(OId.ID as varchar(18)) as OwnerId
 	,CAST((select Id from sfc.RecordType WHERE DeveloperName = 'LSB_CAS_Enquiry') as varchar(18)) as RecordTypeId
	,CAST('Resolved' as varchar(255)) as LSB_CAS_ClosedReason__c
	,CAST(GIDC.Id as varchar(18)) as ContactId
	,CAST(ED.HCL_CRED as datetime) as CreatedDate
	,CAST(ED.HCL_CODE+'.SID' as varchar(90)) as LSB_CAS_ExternalID__c
  ,CAST('Your case has been closed please contact us if your query is outstanding' as varchar(max)) as LSB_CAS_Response__c
	,CAST('SID' as varchar(255)) as LSB_CAS_SourceSystem__c
	,CAST(ED.HCL_CODE as varchar(90)) as LSB_CAS_SourceSystemID__c
	,CAST('Closed' as varchar(255)) as Status
	,CAST(ED.HCL_TITL as varchar(255)) as Subject
	,CAST(ED.HCL_DESC as varchar(255)) as LSB_CAS_LegacyDescription__c  
  --,CAST(GIDC.LSB_CON_CurrentRole__c as varchar(255)) as LSB_CAS_ContactRole__c 
	,CAST(
			CASE WHEN GIDC.LSB_CON_CurrentRole__c LIKE 'Student%' then 'Student'
			 WHEN GIDC.LSB_CON_CurrentRole__c = 'Graduate' then 'Student'
			ELSE GIDC.LSB_CON_CurrentRole__c
			
			END as varchar(255)) as LSB_CAS_ContactRole__c  
FROM [SID].[dbo].[HCL] ED
  JOIN SFC.Get_ID_Contact GIDC ON ED.HCL_CCNC = GIDC.LSB_ExternalID__c
  LEFT JOIN SFC.Get_ID_User OID ON ED.HCL_CCN1 = SUBSTRING(OID.Username,1,(CHARINDEX('@',OID.Username)-1)) AND OID.IsActive = 1
WHERE 1=1 ) Qext WHERE RNK = 1 AND LSB_CAS_ExternalID__c NOT IN (select LSB_CAS_ExternalID__c from sfc.Get_ID_Case WHERE LSB_CAS_ExternalID__c IS NOT NULL)