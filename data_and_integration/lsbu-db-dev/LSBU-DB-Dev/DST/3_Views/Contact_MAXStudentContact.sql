CREATE OR ALTER VIEW [DST].[Contact_MAXStudentContact] AS
SELECT DISTINCT
	CAST(GIDC.Id AS VARCHAR(18)) AS Id
	,CAST(IDS.DDS_DSA_CRN AS VARCHAR(255)) AS LSB_CON_CRN__c
	,CAST(IDS.DDS_DSA_NHS_Bursary_No AS VARCHAR(255)) AS LSB_CON_NHSBursaryNo__c
	,CAST(IIF(IDL.DDS_PEEPs_Student_requires_a_PEEP___referred_to_HS = 'Yes', 1, 0) AS BIT) AS LSB_CON_PersonalEmergencyEvacuationPlan__c
	,CAST(NULL AS VARCHAR(255)) AS LSB_CON_StudentInHalls__c
FROM Maximizer.src.Individual I
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailString IDS ON I.Id = IDS.Id
JOIN SFC.Get_ID_Contact GIDC ON IDS.Student_ID = GIDC.LSB_ExternalID__c
LEFT JOIN INT.SRC_Maximizer_PIVOT_IndividualDetailList IDL ON I.Id = IDL.Id
WHERE (IDS.DDS_DSA_CRN IS NOT NULL OR IDS.DDS_DSA_NHS_Bursary_No IS NOT NULL OR IDL.DDS_PEEPs_Student_requires_a_PEEP___referred_to_HS IS NOT NULL)