CREATE OR ALTER VIEW [DST].[Relationship_ModuleLeaderCourseDirector_Del] AS
SELECT 
  CAST(R.Id as varchar(18)) AS Id
FROM [INT].[SRC_ModuleLeaderCourseDirector_Relationship] as STR
  JOIN SFC.Get_ID_Relationship r on r.LSB_RTP_ExternalId__c=STR.LSB_ExternalID

WHERE

STR.ChangeStatus IN ('DELETE')