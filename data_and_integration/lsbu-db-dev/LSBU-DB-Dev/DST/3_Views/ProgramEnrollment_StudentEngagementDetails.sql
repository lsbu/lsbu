CREATE OR ALTER VIEW [DST].[ProgramEnrollment_StudentEngagementDetails] AS

SELECT DISTINCT
  -- CAST(null AS VARCHAR(18)) AS hed__Account__c
  --,CAST(null AS DATE) AS hed__Admission_Date__c
  --,CAST(null AS VARCHAR(18)) AS hed__Affiliation__c
  --,CAST(null AS DATE) AS hed__Application_Submitted_Date__c
  --,CAST(null AS VARCHAR(255)) AS hed__Class_Standing__c
  --,CAST(null AS VARCHAR(18)) AS hed__Contact__c
  --,CAST(null AS REAL) AS hed__Credits_Attempted__c
  --,CAST(null AS REAL) AS hed__Credits_Earned__c
  --,CAST(null AS VARCHAR(255)) AS hed__Eligible_to_Enroll__c
  --,CAST(null AS VARCHAR(255)) AS hed__End_Date__c
  --,CAST(null AS VARCHAR(255)) AS hed__Enrollment_Status__c
  --,CAST(null AS REAL) AS hed__GPA__c
  --,CAST(null AS VARCHAR(255)) AS hed__Graduation_Year__c
  --,CAST(null AS VARCHAR(18)) AS hed__Program_Plan__c
  --,CAST(null AS DATE) AS hed__Start_Date__c
  --,CAST(null AS VARCHAR(18)) AS LSB_PEN_Application__c
  --,CAST(nul AS REAL) AS LSB_PEN_Attendance__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_CampusInformation__c
  --,CAST(null AS DATE) AS LSB_PEN_EnrolmentStatusChangeDate__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_EnrolmentType__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_ExternalID__c 
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_LatestEnrolmentRecord__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_ModeofStudy__c
  CAST(GIDPE.Id AS VARCHAR(18)) AS Id
  ,CAST(SED.Cognos_MoodleActivity AS REAL) AS LSB_PEN_NoMoodleLogins__c
  ,CAST(SED.Cognos_LibraryAccessActivity AS REAL) AS LSB_PEN_NoOfLIbrarySwipeIns__c
  ,CAST(SED.Cognos_TurnstileActivity AS REAL) AS LSB_PEN_NoTurnstileSwipreIns__c
  ,CAST(SED.Cognos_Submissionssum AS REAL) AS LSB_PEN_NoofSubmissions__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_SessionCode__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_SessionDescription__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_SourceSystem_ID__c
  --,CAST(null AS VARCHAR(255)) AS LSB_PEN_SourceSystem__c
  --,CAST(null AS VARCHAR(18)) AS LSB_PEN_Term__c
  --,CAST(null AS VARCHAR(18)) AS OwnerId
FROM [INT].[SRC_Student_EngagementDetails] SED
 JOIN SFC.Get_ID_ProgramEnrollment GIDPE ON SED.LSB_ExternalID = GIDPE.LSB_PEN_ExternalID__c
WHERE SED.ChangeStatus IN ('NEW','UPDATE')
 OR EXISTS (Select 1 from DST.SF_Reject_ProgramEnrollment R WHERE SED.LSB_ExternalID = R.LSB_PEN_ExternalID__c)
 AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				--AND   VALID.LSB_ExternalID = MED.LSB_ExternalID
				AND	  VALID.ObjectName = 'StudentEngagementDetails'
				AND	  VALID.SrcSystem = 'Cognos')