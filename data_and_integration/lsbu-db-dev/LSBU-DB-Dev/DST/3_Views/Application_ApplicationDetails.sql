CREATE OR ALTER VIEW [DST].[Application_ApplicationDetails] AS
--Application Child - QL_ApplicationStatus != Ã¢â‚¬ËœADEFÃ¢â‚¬â„¢
SELECT distinct
  CAST(QLOH.Id as varchar(18)) AS hed__Applicant__c,
  (CASE
    WHEN QL_ApplicationStatus = 'AUF' THEN TRY_CONVERT(datetime, QL_AppStatusDateAchieved, 103)
    ELSE TRY_CONVERT(datetime, NULL, 103)
  END) AS  hed__Applicant_Decision_Date__c, 
  TRY_CONVERT(datetime, QLOHAD.QL_ApplicationDate, 103) AS hed__Application_Date__c,
  CAST(QLOHAD.QL_ApplicationStatus as varchar(255)) as hed__Application_Status__c, --PickList(AACPT,AALT,AAPP,AARC,ACD,ACF,ACI,ACO,ADBD,ADCC,ADEC,ADEF,ADEPCO,ADEPP,ADEPUF,ADEPUO,ADUP,AINT,AINTV,ARAI,ARBD,ARBI,AREJ,AREL,AUD,AUF,AUI,AUKP,AUO,AWD,AWDX) 
  CAST(QLOHAD.QL_ApplicationType as varchar(255)) as hed__Application_Type__c, --Picklist(31,21,11,99,30,20,10)
  CAST(QLAP.Id as varchar(18)) AS hed__Applying_To__c, --Lookup to Account by QL_AcadProgNameId
  CAST(NULL as varchar(18)) AS hed__Preparer__c,
  CAST(QLTR.id as varchar(18)) as hed__Term__c,
  CAST(QLOHAD.SrcSystem as varchar(255)) AS LSB_APP_SourceSystem__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_APP_SourceSystemID__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_APP_ExternalID__c,
  CAST((
	CASE 
    WHEN SUBSTRING(QLOHAD.QL_AOSPeriod,1,1) IN ('1', '2', '3', '4', '5', '6', '7', '8', '9') THEN SUBSTRING(QLOHAD.QL_AOSPeriod,1,1)
    ELSE NULL
  END) as varchar(255)) AS LSB_APP_ClassStanding__c, --Picklist('1stYear','2stYear')
  CAST((CASE
    WHEN QLOHAD.QL_DepositStatus IS NULL OR QLOHAD.QL_DepositStatus < '3000' THEN 'Deposit Unpaid'
    ELSE 'Deposit Paid'
  END) as varchar(255)) AS LSB_APP_DepositStatus__c, --Picklist('Deposit Paid','Deposit Unpaid') 	
  CAST(QLOHAD.QL_ApplicantAgentCode as varchar(50)) AS LSB_APP_ApplicationAgentCode__c,
  CAST(QLOHAD.QL_ApplicantAgencyName as varchar(255)) AS LSB_APP_ApplicationAgencyName__c,
  CAST(QLOHAD.QL_PreviousInstituteName as varchar(255)) AS LSB_APP_PreviousInstituteName__c,
  CAST((CASE
    WHEN [CFG].[fx_PercentageTwoStringMatching](QLOHAD.QL_PreviousInstituteName, 'Lambeth College, Clapham Centre') > 90 THEN 1
    WHEN [CFG].[fx_PercentageTwoStringMatching](QLOHAD.QL_PreviousInstituteName, 'South Bank Engineering UTC') > 90 THEN 1
    WHEN [CFG].[fx_PercentageTwoStringMatching](QLOHAD.QL_PreviousInstituteName, 'University Academy of Engineering South Bank') > 90 THEN 1
    ELSE 0
  END) as bit) AS LSB_APP_GroupApplicant__c,
  CAST((CASE
    WHEN [CFG].[fx_PercentageTwoStringMatching](QLOHAD.QL_PreviousInstituteName, 'Lambeth College, Clapham Centre') > 90 THEN 'Lambeth College, Clapham Centre'
    WHEN [CFG].[fx_PercentageTwoStringMatching](QLOHAD.QL_PreviousInstituteName, 'South Bank Engineering UTC') > 90 THEN 'South Bank Engineering UTC'
    WHEN [CFG].[fx_PercentageTwoStringMatching](QLOHAD.QL_PreviousInstituteName, 'University Academy of Engineering South Bank') > 90 THEN 'University Academy of Engineering South Bank'
    ELSE NULL
  END) as varchar(255)) AS LSB_APP_GroupInstituteName__c,
  CAST(QLOHAD.QL_VisaStatus as varchar(255)) AS LSB_APP_VisaStatus__c,
  CAST(QLOHAD.QL_VisaType as varchar(255)) as LSB_APP_VisaType__c,
  CAST((CASE
    WHEN QLOHAD.QL_ApplicationSource IN ('U', 'P', 'H', 'R', 'A') THEN 'UKAPPS system'
    WHEN QLOHAD.QL_ApplicationSource = 'X' THEN 'Wozzad system'
    WHEN QLOHAD.QL_ApplicationSource = 'C' THEN 'Clearing system'
    WHEN QLOHAD.QL_ApplicationSource = 'I' THEN 'International system'
    WHEN TRY_CONVERT(INT, QLOHAD.QL_ChoiceNumber) < 10 AND QL_ChoiceNumber IS NOT NULL THEN 'UCAS'
    ELSE ''
  END) as varchar(255)) AS LSB_APP_ApplicationSource__c,
  CAST(QLOHAD.QL_CriminalConvictions as varchar(255)) as LSB_APP_CriminalConvictionSurvey__c,
  CAST(QLOHAD.QL_NominatedName as varchar(255)) as LSB_APP_NominatedPerson__c,
  CAST(QLOHAD.QL_NominatedRelationship as varchar(255)) as LSB_APP_NominatedPersonRelati__c,
  CAST(QLOHAD.QL_AttedenceType as varchar(255)) as LSB_APP_AttendanceType__c,
  CAST(QLOHAD.QL_AOSPeriod as varchar(255)) as LSB_APP_SessionCode__c,
  CAST(QLOHAD.QL_Term as varchar(255)) as LSB_APP_SessionDescription__c,
  CAST((CASE
    WHEN QLOHAD.QL_ApplicationStatus = 'AUF' AND QLOHAD.QL_CriminalConvictions= 'Completed' 
      AND ((UPPER(QLOHAD.QL_VisaType) like '%TIER%4%'
      AND UPPER(QLOHAD.QL_VisaStatus) NOT IN ('REFUSED EC', 'REFUSED LT','SPONSORSHIP WD', 'NOT PREPARED TO SPONSOR'))
        OR (NOT UPPER(QLOHAD.QL_VisaType) like '%TIER%4%') 
        OR QLOHAD.QL_VisaType IS NULL) 
    THEN 1
    ELSE 0
  END) as bit) as LSB_APP_EligibletoEnroll__c,
  CAST(QLAPP.Id as varchar(18)) as LSB_APP_DeferredApplication__C,--Lookup for Application on (QL_StudentID,QL_AcadProgNameId,QL_AOSPeriod) and application_status = 'ADEF' and link to this current application
  CAST((CASE
    WHEN TRY_CONVERT(INT, QLOHAD.QL_ChoiceNumber) < 10 AND QL_ChoiceNumber IS NOT NULL  THEN 1
    ELSE 0
  END) as bit) as LSB_APP_UCASApplicant__c, --If QL_ChoiceNumber is not null and <10 then 'TRUE' else 'FALSE'
  CAST(QLOHAD.QL_CampusInformation as varchar(255)) as LSB_APP_CampusInformation__c,
  (CASE
    WHEN QL_ApplicationStatus in ('AU0','ACO') THEN TRY_CONVERT(datetime, QL_AppStatusDateAchieved, 103)
    ELSE TRY_CONVERT(datetime, NULL, 103)
  END) AS hed__Application_Decision_Date__c,
  TRY_CONVERT(datetime, QLOHAD.QL_AppStatusDateAchieved, 103) as LSB_APP_ApplicationStatusChangeDate__c, 
  CAST(CASE
    WHEN SUBSTRING(QLOHAD.QL_QualificationCheck, CHARINDEX('Y', QLOHAD.QL_QualificationCheck, LEN(QLOHAD.QL_QualificationCheck) - 1), 1) = 'Y' THEN 'Completed'
    ELSE 'Not Yet Completed'
  END as varchar(255)) AS LSB_APP_QualificationCheck__c,
  CAST(QLOHAD.QL_DecisionStatus as varchar(255)) AS LSB_APP_DecisionStatus__c,
  TRY_CONVERT(datetime,QLOHAD.QL_DecisionDate, 103) AS LSB_APP_DecisionDate__c,
  CAST(QLOHAD.QL_DecisionNotes as varchar(1000)) AS LSB_APP_DecisionNotes__c,
  CAST(QLOHAD.QL_QualCode as varchar(255)) AS LSB_APP_QualCode__c,
  CAST((CASE
	WHEN LEFT(QLOHAD.QL_UCASStatus, 1) = '0' THEN SUBSTRING(QLOHAD.QL_UCASStatus, 2, LEN(QLOHAD.QL_UCASStatus))
	ELSE QLOHAD.QL_UCASStatus
  END) as varchar(255)) AS LSB_APP_UCASStatus__c,
  CAST(QLOHAD.QL_OfferText as varchar(1000)) AS LSB_APP_OfferText__c,
  CAST((CASE 
  	WHEN SUBSTRING(QL_AOSPeriod,2,1) = 'F' THEN 'FT' 
  	WHEN SUBSTRING(QL_AOSPeriod,2,1) = 'P' THEN 'PT'
  	WHEN SUBSTRING(QL_AOSPeriod,2,1) = 'D' THEN 'D'
  	ELSE NULL
  END) as varchar(255)) as LSB_APP_ModeOfStudy__c,
  CAST(QLOHAD.QL_FeeCode as varchar(255)) as LSB_APP_FeesCode__c,
  CAST(IIF(QLOHAD.QL_Liveathome = 'H', 1, 0) as bit) as LSB_APP_LiveAtHomeFlag__c,
  CAST(QLOHAD.QL_LateUcasApplicant as varchar(255)) as LSB_APP_LateUCASApplicant__c,
  CAST(QLOHAD.QL_DSDecisionstatus as varchar(255)) as LSB_APP_DirectSystemDecisionStatus__c,
  CAST(QLOHAD.QL_InternationalFunding as varchar(255)) as LSB_APP_InternationalFunding__c,
  TRY_CONVERT(date, QLOHAD.QL_ProgramStartDate, 103) as LSB_APP_CourseStartDate__c,
  TRY_CONVERT(date, QLOHAD.QL_ProgramEndDate, 103) as LSB_APP_CourseEndDate__c
FROM [INT].[SRC_ApplicationDetails_Application] AS QLOHAD
  LEFT OUTER JOIN [SFC].Get_ID_Contact AS QLOH ON QLOHAD.QL_StudentID = QLOH.LSB_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Account AS QLAP ON QLOHAD.QL_AcadProgNameId = QLAP.LSB_ACC_ExternalID__c
  LEFT OUTER JOIN [SFC].Get_ID_Term AS QLTR ON QLTR.LSB_TRM_ExternalID__c = QLOHAD.Term_LSB_ExternalID
  LEFT OUTER JOIN [DST].ParentApplications AS QLAPP 
  ON SUBSTRING(QLAPP.LSB_APP_ExternalID__c,0,CHARINDEX('.',QLAPP.LSB_APP_ExternalID__c,CHARINDEX('.',QLAPP.LSB_APP_ExternalID__c,CHARINDEX('.',QLAPP.LSB_APP_ExternalID__c)+1)+1) ) 
   = SUBSTRING(QLOHAD.LSB_ExternalID,0,CHARINDEX('.',QLOHAD.LSB_ExternalID,CHARINDEX('.',QLOHAD.LSB_ExternalID,CHARINDEX('.',QLOHAD.LSB_ExternalID)+1)+1) ) 
WHERE QLOHAD.QL_ApplicationStatus <> 'ADEF' --Child - QL_ApplicationStatus != Ã¢â‚¬ËœADEFÃ¢â‚¬â„¢
AND ( QLOHAD.ChangeStatus IN ('NEW','UPDATE')
OR
EXISTS (Select R.LSB_APP_ExternalID__c from DST.SF_Reject_Application R WHERE QLOHAD.LSB_ExternalID = R.LSB_APP_ExternalID__c)
OR
NOT EXISTS (SELECT GIAP.LSB_APP_ExternalID__c FROM SFC.Get_ID_Application GIAP WHERE GIAP.LSB_APP_ExternalID__c = QLOHAD.LSB_ExternalID AND QLOHAD.ChangeStatus != 'Delete' )
)
AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLOHAD.LSB_ExternalID
				AND	  VALID.ObjectName = 'ApplicationDetails'
				AND	  VALID.SrcSystem = 'QL')