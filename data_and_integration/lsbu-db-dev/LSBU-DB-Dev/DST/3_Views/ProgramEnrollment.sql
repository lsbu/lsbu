CREATE OR ALTER VIEW [DST].[ProgramEnrollment] AS
SELECT
  CAST(QLAFFI.Id as varchar(18)) AS hed__Affiliation__c,
  TRY_CONVERT(date, QLAPPI.hed__Application_Date__c, 103) AS hed__Application_Submitted_Date__c,
  CAST(CASE 
    WHEN SUBSTRING(QLOHAD.QL_AOSPeriod,1,1) IN ('1', '2', '3', '4', '5', '6', '7', '8', '9') THEN SUBSTRING(QLOHAD.QL_AOSPeriod,1,1)  -- Updated the logic
    ELSE NULL
  END as varchar(255)) AS hed__Class_Standing__c,
  CAST(QLOH.Id as varchar(18)) AS hed__Contact__c,
  TRY_CONVERT(date, QL_ProgramEndDate, 103) AS hed__End_Date__c,
  CONVERT(varchar(255), QL_EnrollmentStatus) AS hed__Enrollment_Status__c,
  CAST(QLAP.Id as varchar(18)) AS hed__Account__c,
  TRY_CONVERT(date, QLOHAD.QL_ProgramStartDate, 103) AS hed__Start_Date__c,
  CAST(QLOHAD.SrcSystem as varchar(255)) AS LSB_PEN_SourceSystem__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_PEN_SourceSystem_ID__c,
  CAST(QLOHAD.LSB_ExternalID as varchar(90)) AS LSB_PEN_ExternalID__c,
  CAST(QLAPPI.Id as varchar(18)) AS LSB_PEN_Application__c,
  CAST(QLOHAD.QL_ModeOfStudy as varchar(255)) AS LSB_PEN_ModeofStudy__c,
  CAST(QLOHAD.QL_AttedenceType as varchar(255)) AS LSB_PEN_EnrolmentType__c,
  CAST(QLOHAD.QL_CampusInformation as varchar(255)) AS LSB_PEN_CampusInformation__c,
  CAST(QLOHAD.QL_AOSPeriod as varchar(90)) AS LSB_PEN_SessionCode__c,
  CAST(QLOHAD.QL_Term as varchar(255)) AS LSB_PEN_SessionDescription__c,
  CAST((CASE 
         WHEN QLOHAD.QL_StudentGraduated = 'Yes' THEN '1'
         ELSE '0' 
        END) AS BIT) AS  LSB_PEN_Graduated__c,

  TRY_CONVERT(date, QLOHAD.QL_EnrolStatusDateAchieved, 103) AS LSB_PEN_EnrolmentStatusChangeDate__c,
  CAST(GIT.Id AS VARCHAR(18)) AS LSB_PEN_Term__c,
  CAST((CASE 
         WHEN EXISTS (SELECT 1
                      FROM INT.SRC_ApplicationEnrolmentDetails_Affiliation AEA
                      WHERE AEA.QL_StudentID = QLOHAD.QL_StudentID
                        AND AEA.QL_AcadProgNameId= QLOHAD.QL_AcadProgNameId
                        AND AEA.QL_AOSPeriod = QLOHAD.QL_AOSPeriod
                        AND AEA.QL_AcademicPeriod = QLOHAD.QL_AcademicPeriod
                        AND AEA.QLCALC_AffiliationPrimaryFlag = 1)
         THEN '1'
         ELSE '0'
        END) AS BIT) AS LSB_PEN_LatestEnrolmentRecord__c,
	CAST(null AS VARCHAR(50)) AS LSB_CCN_ModuleGrade__c,
   --CAST(GRD1.QL_OverallCourseGrade AS VARCHAR(50)) AS LSB_CCN_ModuleGrade__c,
   CAST(GRD1.QL_OverallCourseMarks AS VARCHAR(50)) AS LSB_CCN_ModuleMarks__c,
   CAST(SGICA.Id AS VARCHAR(18)) AS LSB_PEN_AdviseeCaseRecord__c
FROM [INT].[SRC_EnrolmentDetails] AS QLOHAD
 JOIN [SFC].[Get_ID_Contact] AS QLOH ON QLOHAD.QL_StudentID = QLOH.LSB_ExternalID__c					-- CHANGED THE JOIN
  LEFT JOIN [SFC].[Get_ID_Account] AS QLAP ON QLOHAD.QL_AcadProgNameId = QLAP.LSB_ACC_ExternalID__c
  LEFT JOIN [SFC].[Get_ID_Case] SGICA ON SGICA.ContactId = QLOH.Id AND SGICA.RecordTypeId in (select Id from sfc.RecordType WHERE DeveloperName = 'AdviseeRecord') 
  LEFT JOIN [SFC].[Get_ID_Affiliation] AS QLAFFI ON QLAFFI.LSB_AON_ExternalID__c = QLOHAD.Affiliation_LSB_ExternalID
  LEFT JOIN [SFC].[Get_ID_Application] AS QLAPPI ON QLAPPI.LSB_APP_ExternalID__c = QLOHAD.Application_LSB_ExternalID
  LEFT JOIN [SFC].[Get_ID_Term] GIT ON GIT.LSB_TRM_ExternalID__c = QLOHAD.Term_LSB_ExternalID
  LEFT JOIN (SELECT DISTINCT
             GR1.ProgramEnrolment_LSB_ExternalID
            ,GR1.QL_OverallCourseGrade
            ,GR1.QL_OverallCourseMarks
           FROM [INT].[SRC_Grade] GR1
           WHERE QL_Attempt = (SELECT MAX(GR2.QL_Attempt) FROM [INT].[SRC_Grade] GR2 WHERE GR2.ProgramEnrolment_LSB_ExternalID = GR1.ProgramEnrolment_LSB_ExternalID)) GRD1 ON GRD1.ProgramEnrolment_LSB_ExternalID = QLOHAD.LSB_ExternalID
WHERE QLOHAD.QL_EnrolmentStageIndicator = 'E'

  AND (
    QLOHAD.ChangeStatus IN ('NEW','UPDATE')
    OR EXISTS (SELECT R.LSB_PEN_ExternalID__c FROM DST.SF_Reject_ProgramEnrollment R WHERE QLOHAD.LSB_ExternalID = R.LSB_PEN_ExternalID__c)
  )
  AND NOT EXISTS (SELECT * FROM [RPT].[SRCValidationsSummary] VALID
				WHERE VALID.ValidationResultFlag = 'ERROR'
				AND   VALID.LSB_ExternalID = QLOHAD.LSB_ExternalID
				AND	  VALID.ObjectName = 'EnrolmentDetails'
				AND	  VALID.SrcSystem = 'QL')