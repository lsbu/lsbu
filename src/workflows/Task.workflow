<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LSB_Notification1WeekBeforeTheTaskDueDate</fullName>
        <description>Task Reminder: Send notification 1 week before the task due date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_1WeekBeforeTheDueDate</template>
    </alerts>
    <alerts>
        <fullName>LSB_Notification2DaysBeforeTheTaskDueDate</fullName>
        <description>Task Reminder: Send notification 2 days before the task due date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_2DaysBeforeTheTaskDueDate</template>
    </alerts>
    <alerts>
        <fullName>LSB_NotificationOnTheTaskDueDate</fullName>
        <description>Task Reminder: Send the task reminder notification on the task due date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_YouHaveATaskDueToday</template>
    </alerts>
    <alerts>
        <fullName>LSB_NotificationontheReminderAfterTheDueDate</fullName>
        <description>Task Reminder: Send the task reminder notification after the due date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_YouHaveAPendingOverdueTask</template>
    </alerts>
    <alerts>
        <fullName>LSB_TSK_TaskAssignedToOfferHolder</fullName>
        <description>Task Assigned to Offer Holder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_TaskAssignedToOfferHolder</template>
    </alerts>
</Workflow>
