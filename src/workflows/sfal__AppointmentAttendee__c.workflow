<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LSB_AEE_AppointmentCancelledNotifyAdvisor</fullName>
        <description>Appointment Cancelled - notify Advisor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_AEE_ApptAttendeeCancelledByStudent</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentCancelledNotifyStudent</fullName>
        <description>Appointment Cancelled - notify Student</description>
        <protected>false</protected>
        <recipients>
            <field>sfal__Attendee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_ApptAttendeeCancelForStudent</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentReminderForAdvisor</fullName>
        <description>Appointment Reminder For Advisor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_AEE_ApptAttendeeReminderForAdvisor</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentReminderForStudent1Day</fullName>
        <description>Appointment Reminder For Student 1 Day</description>
        <protected>false</protected>
        <recipients>
            <field>sfal__Attendee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_ReminderForStudents1DayBefore</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentReminderForStudent1Hour</fullName>
        <description>Appointment Reminder For Student 1 Hour</description>
        <protected>false</protected>
        <recipients>
            <field>sfal__Attendee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_ReminderForStudents1HourBefore</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentReminderForStudent1Week</fullName>
        <description>Appointment Reminder For Student 1 Week</description>
        <protected>false</protected>
        <recipients>
            <field>sfal__Attendee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_ReminderForStudents1WeekBefore</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentRescheduleNotifyAdvisor</fullName>
        <description>Appointment Reschedule - notify Advisor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_AEE_ApptAttendeeRescheduledByStudent</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_AppointmentRescheduleNotifyStudent</fullName>
        <description>Appointment Reschedule - notify Student</description>
        <protected>false</protected>
        <recipients>
            <field>sfal__Attendee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_RescheduleConfirmationForStudent</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_NewAppointmentConfirmationForStudent</fullName>
        <description>New Appointment Confirmation for Student</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_ApptAttendeeBookingConfirmation</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_NewAppointmentScheduledByAdvisor</fullName>
        <description>New Appointment Scheduled By Advisor</description>
        <protected>false</protected>
        <recipients>
            <field>sfal__Attendee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_AEE_ApptAttendeeScheduledByAdvisor</template>
    </alerts>
    <alerts>
        <fullName>LSB_AEE_NewAppointmentScheduledByStudent</fullName>
        <description>New Appointment Scheduled By Student</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_ApptAttendeeScheduledByStudent</template>
    </alerts>
</Workflow>
