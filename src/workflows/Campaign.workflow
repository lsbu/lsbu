<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LSB_CGN_CampaignRecordApproved</fullName>
        <description>Campaign: Record Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_CampaignApprovedNotification</template>
    </alerts>
    <alerts>
        <fullName>LSB_CGN_CampaignRecordRejected</fullName>
        <description>Campaign: Record Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_CampaignReturnedForRework</template>
    </alerts>
    <fieldUpdates>
        <fullName>LSB_CGN_EventApproved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Event Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_CGN_EventInApproval</fullName>
        <field>Status</field>
        <literalValue>In Approval</literalValue>
        <name>Event in Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_CGN_EventRejected</fullName>
        <field>Status</field>
        <literalValue>Returned for rework</literalValue>
        <name>Event Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
