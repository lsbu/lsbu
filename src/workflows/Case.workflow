<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LSB_CAS_CaseAlertFollowUpRequried</fullName>
        <description>Case alert: Follow Up Requried</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_ConfirmationWhenFollowUpRequired</template>
    </alerts>
    <alerts>
        <fullName>LSB_CAS_CaseAlertNotificationEnquiriesForReview</fullName>
        <description>Case Alert: Notification of Enquiries For Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>LSB_ReviewTeam</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_CaseNotificationEnquiriesForReview</template>
    </alerts>
    <alerts>
        <fullName>LSB_CAS_CaseAlertNotificationInternationalEnquiries</fullName>
        <ccEmails>global@gmail.com,</ccEmails>
        <description>Case Alert: Notification of International Enquiries</description>
        <protected>false</protected>
        <recipients>
            <recipient>LSB_InternationalTeam</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_CaseNotificationOfInternational</template>
    </alerts>
    <alerts>
        <fullName>LSB_CAS_GuestAutoResponse_WebQuery</fullName>
        <description>Guest Auto Response - WebQuery</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_FollowUpRequiredForNQLContacts</template>
    </alerts>
    <alerts>
        <fullName>LSB_CAS_OfferHolderAutoResponse_WebQuery</fullName>
        <description>Offer Holder Auto Response - WebQuery</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_FollowUpRequiredForQLContacts</template>
    </alerts>
    <alerts>
        <fullName>LSB_ConfirmationNQLNotificationCaseSubmit</fullName>
        <description>Case alert: Confirmation NQL Notification Case Submit</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_ConfirmationNQLCaseSubmit</template>
    </alerts>
    <alerts>
        <fullName>LSB_ConfirmationQLNotificationCaseSubmit</fullName>
        <description>Case alert: Confirmation QL Notification Case Submit</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_ConfirmationQLCaseSubmit</template>
    </alerts>
    <alerts>
        <fullName>LSB_ECRequestSubmissionConfirmation</fullName>
        <description>Case Alert: EC Request Submission Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_CaseECRequestSubmissionConfirmation</template>
    </alerts>
    <alerts>
        <fullName>LSB_ECStatusUpdateEmailNotification</fullName>
        <description>Case Alert: EC Status Update Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_CaseECStatusUpdateNotification</template>
    </alerts>
    <alerts>
        <fullName>LSB_EvidencePendingNotificationEmail</fullName>
        <description>Case Alert: Evidence Pending Notification Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_CaseECEvidencePendingNotification</template>
    </alerts>
    <alerts>
        <fullName>LSB_ReadyToMeetAlertContact</fullName>
        <description>Case: Ready to Meet Contact Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_ReadyToMeet</template>
    </alerts>
    <alerts>
        <fullName>LSB_ReadyToMeetAlertWebEmail</fullName>
        <description>Case: Ready to Meet Web Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_ReadyToMeet</template>
    </alerts>
    <fieldUpdates>
        <fullName>LSB_ResolutionTimeViolation</fullName>
        <field>LSB_CAS_SLAViolation__c</field>
        <literalValue>Yes</literalValue>
        <name>Resolution Time Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
