<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LSB_DM_BulkSendRecordApproved</fullName>
        <description>Bulk Send: Record Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_DM_BulkSendApprovedNotification</template>
    </alerts>
    <alerts>
        <fullName>LSB_DM_BulkSendRecordRejected</fullName>
        <description>Bulk Send: Record Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_InternalLSBUEmails/LSB_DM_BulkSendRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>LSB_BulkSendApproved</fullName>
        <field>LSB_ApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Bulk Send Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_BulkSendRejected</fullName>
        <field>LSB_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Bulk Send Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_BulkSendinApproval</fullName>
        <field>LSB_ApprovalStatus__c</field>
        <literalValue>In Approval</literalValue>
        <name>Bulk Send in Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_InApprovalQueueAssignement</fullName>
        <field>OwnerId</field>
        <lookupValue>LSB_DM_DistributedMarketingApprovers</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>In Approval Queue Assignement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
