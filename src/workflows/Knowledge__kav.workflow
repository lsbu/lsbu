<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LSB_KGE_CreateReview</fullName>
        <field>LSB_KGE_CreateReview__c</field>
        <literalValue>1</literalValue>
        <name>CreateReview</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_KGE_NotSubmittedStatus</fullName>
        <field>LSB_KGE_ApprovalStatusTech__c</field>
        <literalValue>Not submitted</literalValue>
        <name>Not submitted status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_KGE_PendingApprovalStatus</fullName>
        <field>LSB_KGE_ApprovalStatusTech__c</field>
        <literalValue>Awaiting approval</literalValue>
        <name>Pending approval status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_KGE_PublishedStatus</fullName>
        <field>LSB_KGE_ApprovalStatusTech__c</field>
        <literalValue>Approved</literalValue>
        <name>Published status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_KGE_ReturnForFurtherWorkStatus</fullName>
        <field>LSB_KGE_ApprovalStatusTech__c</field>
        <literalValue>Return for further work</literalValue>
        <name>Return for further work status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LSB_KGE_SetDate</fullName>
        <field>LSB_KGE_SubmitForApprovalDate__c</field>
        <formula>TODAY()</formula>
        <name>SetDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>LSB_KGE_Publish_ArticleAsNewVersion</fullName>
        <action>PublishAsNew</action>
        <label>Publish Article as new version</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
