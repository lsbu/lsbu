<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LSB_SendFeedbackSurvey</fullName>
        <description>Send Feedback Survey</description>
        <protected>false</protected>
        <recipients>
            <field>LSBU_SIN_RecipientEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LSB_LSBUAutoResponseEmailTemplates/LSB_FeedbackRequestEmailTemplate</template>
    </alerts>
</Workflow>
