import {api, LightningElement} from 'lwc';

import fetchListViewColumns from '@salesforce/apex/LSB_ListViewController.fetchListViewColumns';
import saveButton from '@salesforce/label/c.LSB_Save';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

export default class LsbCustomAction extends LightningElement {

    @api buttonName;
    @api objectApiName;
    @api recordId;
    @api fieldSetName;

    fields = [];
    componentReady = false;
    showModal = false;
    showSpinner = false;

    labels = {
        saveButton
    };

    handleOpenModal() {
        this.showModal = true;
    }

    handleCloseModal() {
        this.showModal = false;
    }

    handleSubmit() {
        this.showSpinner = false;
        eval("$A.get('e.force:refreshView').fire();");
        this.showModal = false;
    }

    handleShowSpinner() {
        this.showSpinner = true;
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.componentReady) {
            this.addListeners();
            return;
        }
        this.addListeners();
        fetchListViewColumns({
            objectApiName: this.objectApiName,
            fieldSetApiName: this.fieldSetName
        }).then(result => {
            let fields = [];
            result.forEach(column => {
                fields.push(column.fieldNameOrPath);
            });
            this.fields = fields;
            this.componentReady = true;
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.modal__main-container');
        if (modalContainer) {
            modalContainer.focus();
            modalContainer.addEventListener('keydown', (e) => {
                let closeButton = this.template.querySelector('.close__button');
                let saveButton = this.template.querySelector('.save__button');
                if (e.shiftKey && e.key === 'Tab' && e.target === closeButton) {
                    e.preventDefault();
                    saveButton.focus();
                } else if (!e.shiftKey && e.key === 'Tab' && e.target === saveButton) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }
}