import {LightningElement, api, track } from 'lwc';

import formFactorPropertyName from '@salesforce/client/formFactor'

import NO_RESULT_HEADER from '@salesforce/label/c.LSB_CommSelfAssessmentNoResultHeaderChart';
import NO_RESULT_MESSAGE from '@salesforce/label/c.LSB_CommSelfAssessmentNoResultErrorChart';
import CHART_TITLE from '@salesforce/label/c.LSB_CommSelfAssessmentChartTitle';
import PARAGRAPH_ONE from '@salesforce/label/c.LSB_CommSelfAssessmentChartParOne';
import PARAGRAPH_TWO from '@salesforce/label/c.LSB_CommSelfAssessmentChartParTwo';
import PARAGRAPH_THREE from '@salesforce/label/c.LSB_CommSelfAssessmentChartParThree';
import PARAGRAPH_FOUR from '@salesforce/label/c.LSB_CommSelfAssessmentChartParFour';
import PARAGRAPH_FIVE from '@salesforce/label/c.LSB_CommSelfAssessmentChartParFive';

const FORM_FACTOR_LARGE = 'Large';

export default class LsbCommSelfAssessmentPersonalDevelopment extends LightningElement {

    labels = {
        NO_RESULT_HEADER,
        NO_RESULT_MESSAGE,
        CHART_TITLE,
        PARAGRAPH_ONE,
        PARAGRAPH_TWO,
        PARAGRAPH_THREE,
        PARAGRAPH_FOUR,
        PARAGRAPH_FIVE
    }

    @track responses;
    @track chartConfigData;
    @track tableData;

    @api get responseData() {
        return this.responses;
    }

    get isMobile() {
        return !(formFactorPropertyName === FORM_FACTOR_LARGE);
    }

    set responseData(value) {
        this.responses = value;
        if (this.responses) {
            this.setChartData();
            this.setTableData();
        }
    }

    setChartData() {
        let responseData = {...this.responses};
        responseData.options = {
            responsive: false,
            elements: {
                line: {
                    borderWidth: 2
                }
            },
            scale: {
                min: 0,
                max: 5,
                ticks: {
                    max: 5,
                    min: 0,
                    stepSize: 1,
                    beginAtZero: true
                }
            },
            scales: {
                r: {
                    pointLabels: {
                        font: {
                            size: 13,
                        }
                    }
                }
            },
            plugins: {
                legend: {
                    position: 'right'
                }
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };
        responseData.type = 'radar';
        this.chartConfigData = responseData;
    }

    setTableData() {
        let initialTableData = {...this.responses};
        let tableData = [];

        for (let i = 0; i < initialTableData.data.labels.length; i ++) {
            tableData.push({
                label : initialTableData.data.labels[i],
                iconName: "mdi " + initialTableData.data.sectionData[initialTableData.data.labels[i]],
                ariaLabel: `${initialTableData.data.labels[i]}`,
                profiles: []
            });
            for (let j = 0; j < initialTableData.data.datasets.length; j ++) {
                tableData[i].profiles.push({
                    name: initialTableData.data.datasets[j].label,
                    value: initialTableData.data.datasets[j].data[i],
                    ariaLabel: `${initialTableData.data.datasets[j].label}, ${initialTableData.data.datasets[j].data[i]}`
                });
                tableData[i].ariaLabel = `${tableData[i].ariaLabel}, ${initialTableData.data.datasets[j].label}, ${initialTableData.data.datasets[j].data[i]}`;
            }
        }
        this.tableData = tableData;
    }

}