import {LightningElement, api, track, wire} from 'lwc';

import {getRecord, getFieldValue} from 'lightning/uiRecordApi';
import SMALL_PHOTO_URL from '@salesforce/schema/User.SmallPhotoUrl';

import fetchChatterFeeds from '@salesforce/apex/LSB_ChatterFeedsController.fetchChatterFeeds';
import createFeedPost from '@salesforce/apex/LSB_ChatterFeedsController.createFeedPost';
import createFeedItem from '@salesforce/apex/LSB_ChatterFeedsController.createFeedItem';

import FEED_COMMENT_OBJECT from '@salesforce/schema/FeedComment';
import FEED_COMMENT_BODY from '@salesforce/schema/FeedComment.CommentBody';
import FEED_COMMENT_FEED_ITEM_ID from '@salesforce/schema/FeedComment.FeedItemId';
import FEED_COMMENT_TYPE from '@salesforce/schema/FeedComment.CommentType';

import chatterPlaceholder from '@salesforce/label/c.LSB_ChatterPlaceholder';
import postedBy from '@salesforce/label/c.LSB_PostedBy';

import LOCALE from '@salesforce/i18n/locale';
import TIMEZONE from '@salesforce/i18n/timeZone';
import Id from '@salesforce/user/Id';

export default class LsbChatterFeedsComponent extends LightningElement {

    @api parentRecordId;
    @track chatterFeeds;

    componentReady;
    showSpinner = true;

    labels = {
        chatterPlaceholder,
        postedBy
    };

    dateTimeFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric',
        timeZone: TIMEZONE
    });

    userId = Id;

    @wire(getRecord, {recordId: '$userId', fields: [SMALL_PHOTO_URL]})
    userRecord;

    get userPhoto() {
        return getFieldValue(this.userRecord.data, SMALL_PHOTO_URL);
    }

    get chatterFeedsEmpty() {
        return typeof this.chatterFeeds === "undefined" || this.chatterFeeds.length === 0;
    }

    renderedCallback() {
        if (this.parentRecordId && !this.componentReady) {
            this.componentReady = true;
            fetchChatterFeeds({
                parentRecordId: this.parentRecordId
            }).then(result => {
                let feeds = [];
                result.forEach(feed => {
                    let feedObj = {...feed};
                    feedObj.commentBody = undefined;
                    feedObj.CreatedDate = this.dateTimeFormat.format(new Date(feedObj.CreatedDate));
                    feedObj.ariaLabel = postedBy + ' ' + feedObj.InsertedBy.FirstName + ' '
                        + feedObj.InsertedBy.LastName + ', ' + feedObj.CreatedDate + ': ' + feedObj.Body;
                    if (typeof feed.FeedComments !== "undefined") {
                        feedObj.FeedComments.forEach(feedComment => {
                            feedComment.CreatedDate = this.dateTimeFormat.format(new Date(feedComment.CreatedDate));
                            feedComment.ariaLabel = postedBy + ' ' + feedComment.InsertedBy.FirstName + ' '
                                + feedComment.InsertedBy.LastName + ', ' + feedComment.CreatedDate + ': ' + feedComment.CommentBody;
                        });
                    }
                    feeds.push(feedObj);
                });
                this.chatterFeeds = feeds;
            }).catch(error => {
                console.log('Error: ' + error);
            }).finally(() => {
                this.showSpinner = false;
            });
        }
    }

    addComment(event) {
        this.showSpinner = true;
        let feedIndex = event.currentTarget.dataset.index;
        if (typeof this.chatterFeeds[feedIndex].commentBody === "undefined") {
            return;
        }
        let recordInput = {'sobjectType': FEED_COMMENT_OBJECT.objectApiName};
        recordInput[FEED_COMMENT_BODY.fieldApiName] = this.chatterFeeds[feedIndex].commentBody;
        recordInput[FEED_COMMENT_FEED_ITEM_ID.fieldApiName] = this.chatterFeeds[feedIndex].Id;
        recordInput[FEED_COMMENT_TYPE.fieldApiName] = 'TextComment';
        createFeedPost({
            feedPost: recordInput
        }).then(result => {
            this.componentReady = false;
            this.chatterFeeds[feedIndex].commentBody = undefined;
            this.renderedCallback();
        }).catch(error => {
            console.log(error);
        }).finally(() => {
            this.showSpinner = false;
        });
    }

    onCommentInput(event) {
        let feedIndex = event.currentTarget.dataset.index;
        let value = event.target.value;
        let feeds = this.chatterFeeds;
        if (!value.replace(/\s/g, '').length) {
            feeds[feedIndex].commentBody = undefined;
        } else {
            feeds[feedIndex].commentBody = value;
        }
    }

    feedComment;

    onFeedInput(event) {
        let value = event.currentTarget.value;
        if (!value.replace(/\s/g, '').length) {
            this.feedComment = undefined;
        } else {
            this.feedComment = value;
        }
    }

    addFeed(event) {
        this.showSpinner = true;
        createFeedItem({
            feedMessage: this.feedComment,
            parentId: this.parentRecordId
        }).then(result => {
            this.componentReady = false;
            this.feedComment = undefined;
            this.renderedCallback();
        }).catch(error => {
            console.log(error);
        }).finally(() => {
            this.showSpinner = false;
        });
    }

}