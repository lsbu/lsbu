import {LightningElement} from 'lwc';

import getPreviousYearResults from '@salesforce/apex/LSB_PreviousYearResultsController.getPreviousYearResults';

import previousYearResultsHeader from '@salesforce/label/c.LSB_PreviousYearResultsHeader';
import previousYearResultsDescription from '@salesforce/label/c.LSB_PreviousYearResultsDescription';
import previousYearResultsNoGrades from '@salesforce/label/c.LSB_PreviousYearResultsNoGrades';
import myGradesHeader from '@salesforce/label/c.LSB_MyGradesHeader';
import myGradesHeader2 from '@salesforce/label/c.LSB_MyGradesHeader2';
import myGradesHeader3 from '@salesforce/label/c.LSB_MyGradesHeader3';
import myGradesParagraph1 from '@salesforce/label/c.LSB_MyGradesParagraph1';
import myGradesParagraph2 from '@salesforce/label/c.LSB_MyGradesParagraph2';
import myGradesOverallGrade from '@salesforce/label/c.LSB_MyGradesOverallGrade';
import myGradesViewResults from '@salesforce/label/c.LSB_MyGradesViewResults';
import myGradesViewResultsModal from '@salesforce/label/c.LSB_MyGradesViewResultsModal'

export default class LsbPreviousYearResults extends LightningElement {

    tabs = [];

    dataReady;
    showModal;

    labels = {
        myGradesOverallGrade,
        myGradesViewResults,
        myGradesViewResultsModal,
        myGradesHeader,
        myGradesHeader2,
        myGradesHeader3,
        myGradesParagraph1,
        myGradesParagraph2,
        previousYearResultsDescription,
        previousYearResultsHeader,
        previousYearResultsNoGrades
    }

    renderedCallback() {
        if(this.showModal) {
            this.addListeners();
        }
        if (this.dataReady) {
            return;
        }
        this.dataReady = true;
        getPreviousYearResults()
            .then(result => {
                this.tabs = result;
            })
            .catch(error => {
                console.log(error.body.message);
            });
    }

    handleShowModal(event) {
        this.showModal = true;
    }

    handleCloseModal(event) {
        this.showModal = false;
    }

    handlePress(event) {
        if (event.key === 'Enter') {
            this.handleShowModal();
        }
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.modal__main-container');
        if (modalContainer) {
            modalContainer.focus();
            modalContainer.addEventListener('keydown', (e) => {
                let closeButton = this.template.querySelector('.close__button');
                let customTabset = this.template.querySelector('.custom__tabset');
                if (e.shiftKey && e.key === 'Tab' && (e.target === closeButton || e.target === modalContainer)) {
                    e.preventDefault();
                    customTabset.focus();
                } else if (!e.shiftKey && e.target === customTabset) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }
}