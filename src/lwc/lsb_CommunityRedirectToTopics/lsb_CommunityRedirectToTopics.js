import {api, LightningElement, track} from 'lwc';

// apex
import fetchTopics from '@salesforce/apex/LSB_CommunityFeaturedTopicsController.fetchTopics';

export default class LsbCommunityRedirectToTopics extends LightningElement {

    @api variant;
    @api label;
    @api topic;
    @track dataReady = false;
    url;

    renderedCallback() {
        if (this.dataReady) {
            return;
        }

        fetchTopics()
            .then(result => {
                result.forEach((topic) => {
                    if (this.topic === topic.name) {
                        this.url = '/s/topic/' + topic.id + '/' + topic.name;
                    }
                });
                this.dataReady = true;
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

}