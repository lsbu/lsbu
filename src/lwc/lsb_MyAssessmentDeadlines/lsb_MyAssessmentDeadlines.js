import { LightningElement,api,track } from 'lwc';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import getAssessments from '@salesforce/apex/LSB_MyAssessmentDeadlines.getAssessments';

export default class LsbMyAssessmentDeadlines extends LightningElement {
    @api header;
    @api description;
    @track pastAssessments = [];
    @track upcomingAssessments = [];
    dataReady = false;

    renderedCallback() {
        if (this.dataReady) {
            return;
        }
        this.dataReady = true;
        getAssessments()
            .then(result => {
                result.upcomingAssessments.forEach((assessment) => {
                    this.upcomingAssessments.push({'componentOrSubComponentDesc' : assessment.componentOrSubComponentDesc, 'moduleName' : assessment.moduleName, 'handInDate' : assessment.handInDate ? this.formatDate(assessment.handInDate) : 'To be confirmed'});
                });
                result.pastAssessments.forEach((assessment) => {
                    this.pastAssessments.push({'componentOrSubComponentDesc' : assessment.componentOrSubComponentDesc, 'moduleName' : assessment.moduleName, 'handInDate' : assessment.handInDate ? this.formatDate(assessment.handInDate) : 'To be confirmed'});
                });
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

    formatDate(inputDate) {
        let date = new Date(inputDate);
        let year = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
        let month = new Intl.DateTimeFormat('en', { month: 'long' }).format(date);
        let day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);

        return [day, month, year].join(' ');
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }
}