import {api, LightningElement} from 'lwc';

import {loadStyle} from 'lightning/platformResourceLoader';
import communityBanners from '@salesforce/contentAssetUrl/communityBannerszip';
import communityStyles from '@salesforce/resourceUrl/communityStyles';

export default class LsbCommunityBanner extends LightningElement {

    @api bannerFileName;
    @api showCaption;
    @api captionText;
    @api captionFontSize;
    @api captionColor;
    @api showTitle;
    @api titleText;
    @api titleFontSize;
    @api titleColor;
    @api showButton;
    @api buttonLabel;
    @api buttonBackgroundColor;
    @api buttonFontColor;
    @api buttonFontSize;
    @api redirectUrl;


    connectedCallback() {
        loadStyle(this, communityStyles);
    }

    get backgroundStyle() {
        let bannerPath = this.banner;
        return `background-image:url(${bannerPath})`;
    }

    get buttonStyle() {
        let buttonFontColor = this.buttonFontColor;
        let buttonFontSize = this.buttonFontSize;
        let buttonBackgroundColor = this.buttonBackgroundColor;
        let buttonStyle = this.setTextStyles(buttonFontColor, buttonFontSize);
        buttonStyle = this.setBackgroundColor(buttonStyle, buttonBackgroundColor);
        return buttonStyle;
    }

    get captionStyle() {
        let captionColor = this.captionColor;
        let captionFontSize = this.captionFontSize;
        return this.setTextStyles(captionColor, captionFontSize);
    }

    get titleStyle() {
        let titleColor = this.titleColor;
        let titleFontSize = this.titleFontSize;
        return this.setTextStyles(titleColor, titleFontSize);
    }

    setTextStyles(color, fontSize) {
        let elementStyle = '';
        elementStyle = this.setColor(elementStyle, color);
        elementStyle = this.setFontSize(elementStyle, fontSize);
        return elementStyle;
    }

    setColor(style, colorValue) {
        if (colorValue) {
            style = style + `color: ${colorValue}; `
        }
        return style;
    }

    setFontSize(style, sizeValue) {
        if (sizeValue) {
            style = style + `font-size: ${sizeValue}; `
        }
        return style;
    }

    setBackgroundColor(style, colorValue) {
        if (colorValue) {
            style = style + `background-color: ${colorValue}; `
        }
        return style;
    }

    get banner() {
        return communityBanners + 'pathinarchive=communityBanners/' + this.bannerFileName;
    }

    handleClick() {
        if (this.redirectUrl) {
            window.location.href = this.redirectUrl;
        }
    }
}