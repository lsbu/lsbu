import {LightningElement, track, wire} from 'lwc';

import getSelfAssessmentData from '@salesforce/apex/LSB_CommSelfAssessmentController.fetchAssessmentData';
import handleSaveResponses from '@salesforce/apex/LSB_CommSelfAssessmentController.handleSaveResponses';
import {refreshApex} from "@salesforce/apex";

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import personalDevelopmentPlanUpdated from '@salesforce/label/c.LSB_PersonalDevelopmentPlanUpdated';
import getWhatYouNeed from '@salesforce/label/c.LSB_GetWhatYouNeed';
import selfAssessmentError from '@salesforce/label/c.LSB_SelfAssessmentUpdateError';
import selfAssessmentTitle from '@salesforce/label/c.LSB_CommSelfAssessmentMainTitle';
import selfAssessmentFooter from '@salesforce/label/c.LSB_CommSelfAssessmentMainFooter';
import personalDevelopmentChartTab from '@salesforce/label/c.LSB_CommSelfAssessmentChartTabName';

export default class LsbCommSelfAssessmentMain extends LightningElement {

    tabSetNames = ['Personal development plan'];
    tabSetValues = [];
    personalPlanData;
    developmentData;
    goalsData;
    showSpinner;
    myPlanData;
    @track personalDevelopmentChartData;

    selfAssessmentResponse;
    @wire(getSelfAssessmentData)
    selfAssessmentData(resp) {
        this.selfAssessmentResponse = resp;
        if (resp.data) {
            let responseData = JSON.parse(resp.data);
            this.setTabValues(responseData);
            this.developmentData = responseData.responseData['Strengths and areas for development'];
            this.personalPlanData = responseData.responseData['About me'];
            this.goalsData = responseData.responseData['Goals'];
            this.myPlanData = responseData.responseData['My Plan'];
            this.personalDevelopmentChartData = responseData.chartConfig;
        } else if (resp.error) {
            console.log('error:', resp.error)
        }
    }

    get isLoading() {
        return !this.myPlanData;
    }

    renderedCallback() {
        this.setupStyles();
    }

    labels = {
        personalDevelopmentPlanUpdated,
        getWhatYouNeed,
        selfAssessmentError,
        selfAssessmentTitle,
        personalDevelopmentChartTab,
        selfAssessmentFooter
    }

    setupStyles() {
        const style = document.createElement('style');
        style.innerText = 'c-lsb_-comm-self-assessment-main .tab_class .slds-tabs_default__link { font-weight: bold!important;}';
        this.template.querySelector('lightning-tab').appendChild(style);
    }

    setTabValues(responseData) {
        let tabSet = [...this.tabSetNames, ...responseData.picklistTabNames];
        let tabValues = {
            myPlan: tabSet[0],
            personalPlan: tabSet[1],
            developmentPlan: tabSet[2],
            goals: tabSet[3]
        };
        this.tabSetValues = tabValues;
    }

    @track modifiedRecords = [];

    handleTextChanged(event){
        let index = this.modifiedRecords.findIndex(x => x.Id === event.detail.recordId);
        if(index != -1){
            this.modifiedRecords.splice(index,1);
        }
        let record = {
            'sobjectType' : 'LSB_SAR_SelfAssessmentResponse__c',
            'Id' : event.detail.recordId,
            'LSB_SAR_ResponseValue__c' : event.detail.value,
            'LSB_SAR_ResponseShortText__c' : event.detail.value ? event.detail.value.substring(0,255) : ''
        };
        this.modifiedRecords.push(record);
        console.log(this.modifiedRecords);
    }

    handleRadioChanged(event){
        let index = this.modifiedRecords.findIndex(x => x.Id === event.detail.recordId);
        if(index != -1){
            this.modifiedRecords.splice(index,1);
        }
        let record = {
            'sobjectType' : 'LSB_SAR_SelfAssessmentResponse__c',
            'Id' : event.detail.recordId,
            'LSB_SAR_ResponseValue__c' : event.detail.answer,
            'LSB_SAR_ResponseShortText__c' : event.detail.answer ? event.detail.answer.substring(0,255) : '',
            'LSB_SAR_ChoiceValue__c' : event.detail.answer,
            'LSB_SAR_SelfAssessmentQuestionChoice__c': event.detail.choiceId
        };
        this.modifiedRecords.push(record);
        console.log(this.modifiedRecords);
    }

    handleSaveChanges(event){
        this.showSpinner = true;
        handleSaveResponses({
            responses: this.modifiedRecords
        }).then(result => {
            this.modifiedRecords = [];
            refreshApex(this.selfAssessmentResponse);
            this.showSpinner = false;
            this.template.querySelector('c-lsb_-modal')
                .openModal(
                    {
                        header: this.labels.getWhatYouNeed,
                        message: this.labels.personalDevelopmentPlanUpdated,
                        hideCancel: true,
                        isInnerHtml: true
                    });
        }).catch(error => {
            const evt = new ShowToastEvent({
                title: 'Error',
                message: this.labels.selfAssessmentError,
                variant: 'error',
            });
            this.dispatchEvent(evt);
            console.log(error);
            this.showSpinner = false;
        }).finally(() => {

        });
    }
}