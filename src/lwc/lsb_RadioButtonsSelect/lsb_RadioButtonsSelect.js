import {api, LightningElement} from 'lwc';

export default class LsbRadioButtonsSelect extends LightningElement {

    showModal;
    draftInput = false;

    @api selectedOption;
    @api recordId;
    @api fieldName;
    @api fieldLabel;
    @api options;

    handleChange(event) {
        event.detail.fieldName = this.fieldName;
        event.detail.id = this.recordId;
        this.selectedOption = event.detail.value;
        this.draftInput = true;
        this.showModal = false;
    }

    handleOpenModal(event) {
        this.showModal = true;
    }

    handleCloseModal(event) {
        this.showModal = false;
    }

    get styleClass() {
        if (this.draftInput) {
            return 'input_draft_text';
        } else {
            return 'input_text';
        }
    }
}