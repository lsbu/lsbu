import {LightningElement, wire, track} from 'lwc';

import {getRecord, getFieldValue} from 'lightning/uiRecordApi';

import redirectToSurvey from '@salesforce/apex/LSB_PostChatPageController.redirectToSurvey';
import saveTranscript from '@salesforce/apex/LSB_PostChatPageController.saveTranscriptFile';

import feedbackRequest from '@salesforce/label/c.LSB_ChatFeedbackRequest';
import feedbackButton from '@salesforce/label/c.LSB_ChatGiveFeedback';
import thankYouMessage from '@salesforce/label/c.LSB_ChatFeedbackMessage';
import defaultAdvisorName from '@salesforce/label/c.LSB_ChatDefaultAdvisorName';
import saveTranscriptButton from '@salesforce/label/c.LSB_ChatSaveTranscript';
import transcriptFileName from '@salesforce/label/c.LSB_ChatTranscriptFileName';

import USER_FIRST_NAME_FIELD from '@salesforce/schema/User.FirstName';
import {NavigationMixin} from "lightning/navigation";

export default class LsbPostChatPage extends NavigationMixin(LightningElement) {

    labels = {
        feedbackRequest,
        feedbackButton,
        thankYouMessage,
        defaultAdvisorName,
        saveTranscriptButton,
        transcriptFileName
    };

    parameters = {};

    @track
    agentId;

    chatKey;
    chatTranscript;
    transcriptFile;
    lastVisitedPage;
    inCommunity;

    @wire(getRecord, {recordId: '$agentId', fields: [USER_FIRST_NAME_FIELD]})
    agentInfo;

    connectedCallback() {
        this.parameters = this.getQueryParameters();
        let chatDetails = JSON.parse(this.parameters.chatDetails);
        this.chatKey = this.parameters.chatKey;
        this.agentId = chatDetails.agent.userId;
        this.chatTranscript = this.parameters.transcript;
        this.lastVisitedPage = this.parameters.lastVisitedPage;
        this.inCommunity = this.checkIfChatIsInCommunity();
    }

    checkIfChatIsInCommunity() {
        return this.lastVisitedPage.includes(location.host);
    }

    getFirstName() {
        return getFieldValue(this.agentInfo.data, USER_FIRST_NAME_FIELD);
    }

    getQueryParameters() {
        let params = {};
        let paramSearch = location.search.substring(1);
        if (paramSearch) {
            params = JSON.parse('{"' + paramSearch.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
                return key === "" ? value : decodeURIComponent(value)
            });
        }
        return params;
    }

    get thankYouMessage() {
        let advisorName = this.labels.defaultAdvisorName;
        if (this.agentInfo.data) {
            advisorName = this.getFirstName();
        }
        return `${this.labels.thankYouMessage} ${advisorName}`;
    }

    handleRedirect() {
        if (this.chatKey) {
            redirectToSurvey({
                chatKey: this.chatKey
            })
                .then(result => {
                    if (result) {
                        window.location.href = result;
                    }
                })
                .catch(error => {
                    this.errorMessage = error.body.message;
                    console.log('Could not open: ', this.errorMessage);
                });
        }
    }

    handleDownloadTranscript() {
        if (this.inCommunity) {
            if (!this.transcriptFile) {
                this.saveToFile(this.processTranscript(this.chatTranscript));
            } else {
                this.downloadTranscriptFromContentVersion();
            }
        } else {
            this.downloadTranscript();
        }
    }

    downloadTranscript() {
        if (this.chatTranscript) {
            let transcriptMessage = this.processTranscript(String(this.chatTranscript));
            let downloadElement = document.createElement('a');
            downloadElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(transcriptMessage);
            downloadElement.target = '_self';
            downloadElement.download = `${this.labels.transcriptFileName}.txt`;
            document.body.appendChild(downloadElement);
            downloadElement.click();
        }
    }

    downloadTranscriptFromContentVersion() {
        let fileUrl = this.getBaseUrl() + this.transcriptFile.LSB_CVR_DownloadLink__c;
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: fileUrl
            },
        }, false);
    }

    processTranscript(transcriptText) {
        return transcriptText.replaceAll('+', ' ');
    }

    saveToFile(transcriptText) {
        saveTranscript({
            chatKey: this.chatKey,
            fileName: `${this.labels.transcriptFileName}.txt`,
            fileData: transcriptText
        })
            .then(result => {
                this.transcriptFile = result;
                this.downloadTranscriptFromContentVersion();
            })
            .catch(error => {
                window.console.log(JSON.stringify(error));
            });
    }

    getBaseUrl() {
        let baseUrl = 'https://' + location.host + '/';
        return baseUrl;
    }
}