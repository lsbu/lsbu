import {api, LightningElement} from 'lwc';

export default class LsbCommSelfAssessmentNavBar extends LightningElement {


    @api sectionsList = [];

    handleClick(event) {
        console.log('event>', JSON.stringify(event.target.dataset.id));
        let sectionId = event.target.dataset.id;
        this.dispatchEvent(
            new CustomEvent('choose', {detail: sectionId})
        );
    }

    handleKeypress(event) {
        if(event.key === 'Enter') {
            this.handleClick(event);
        }
    }

    renderedCallback() {
        this.setSelectedClass();
    }

    setSelectedClass() {
        let activeSectionId = null;
        this.sectionsList.forEach((el) => {
                if (el.active) {
                    activeSectionId = el.id;
                }
            }
        )
        const activeSections = this.template.querySelectorAll(`li`);
        activeSections.forEach((el) => {
            el.classList.remove('nav__item--selected');
        });
        if (activeSectionId) {
            const activeSection = this.template.querySelector(`[data-id="${activeSectionId}"]`);
            if (activeSection) {
                this.template.querySelector(`[data-id="${activeSectionId}"]`).classList.add('nav__item--selected');
            }
        }
    }

}