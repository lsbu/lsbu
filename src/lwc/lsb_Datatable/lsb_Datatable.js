import LightningDatatable from "lightning/datatable";
import picklist from './picklist';
import lookup from './lookup';

export default class LsbDatatable extends LightningDatatable {
    static customTypes = {
        picklist: {
            template: picklist,
            typeAttributes: ['recordId','options', 'fieldName', 'fieldLabel']
        },
        lookup: {
            template: lookup
        }
    };
}