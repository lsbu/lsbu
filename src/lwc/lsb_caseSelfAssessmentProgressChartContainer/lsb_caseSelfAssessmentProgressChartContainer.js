import {LightningElement, api, wire} from 'lwc';

import getChartData from '@salesforce/apex/LSB_CaseSelfAssessmentController.getChartDataByContactId';

import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import CONTACT_ID from '@salesforce/schema/Case.ContactId';

import chartTitle from '@salesforce/label/c.LSB_CaseSelfAssessmentChartTitle';
import noDataMessage from '@salesforce/label/c.LSB_CaseSelfAssessmentNoChartData';
import genericError from '@salesforce/label/c.LSB_GenericError';

const fields = [CONTACT_ID];

export default class LsbCaseSelfAssessmentProgressChartContainer extends LightningElement {

    labels = {
        chartTitle,
        noDataMessage,
        genericError
    }

    @api recordId;
    chartData;
    error;
    isReady = false;

    @wire(getRecord, { recordId: '$recordId', fields })
    case;

    @wire(getChartData, { contactId: '$caseContactId' })
    wiredChartData({ error, data }) {
        if (data) {
            this.chartData = this.setChartData(data);
            this.error = undefined;
            this.isReady = true;
        } else if (error) {
            console.log('error>>',error)
            this.error = error;
            this.chartData = undefined;
            this.isReady = true;
        } else {
            this.isReady = true;
        }
    }

    get caseContactId() {
        return getFieldValue(this.case.data, CONTACT_ID);
    }

    setChartData(chartData) {
        let responseData = {...chartData};
        responseData.options = {
            responsive: false,
            elements: {
                line: {
                    borderWidth: 1
                }
            },
            scale: {
                min: 0,
                max: 5,
                ticks: {
                    max: 5,
                    min: 0,
                    stepSize: 1,
                    beginAtZero: true
                }
            },
            scales: {
                r: {
                    pointLabels: {
                        font: {
                            size: 13,
                        }
                    }
                }
            },
            plugins: {
                legend: {
                    position: 'right'
                }
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };
        responseData.type = 'radar';
        return responseData;
    }

}