import {LightningElement, wire, api, track} from 'lwc';
import getReports from '@salesforce/apex/LSB_MassAssignmentController.getReportsFromAssignmentFolder';
import runBatch from '@salesforce/apex/LSB_MassAssignmentController.runReportBatch';
import validateReportColumns from '@salesforce/apex/LSB_MassAssignmentController.validateReportColumns';
import fetchUsers from '@salesforce/apex/LSB_MassAssignmentController.fetchUsers';

import hasPermission from '@salesforce/customPermission/LSB_BulkTasksAndSurveys';
import hasSupportProfilePermission from '@salesforce/customPermission/LSB_BulkTaskSupportProfile';

import PERMISSION_ERROR from '@salesforce/label/c.LSB_MassAssignmentFormPermissionError';
import FIELDS_ERROR from '@salesforce/label/c.LSB_MassAssignmentFormFieldsError';
import INSUFICIENT_PRIVILEGES from '@salesforce/label/c.LSB_InsufficientPrivileges';
import SHARING_HELPTEXT from '@salesforce/label/c.LSB_MassAssignmentFormSharingHelptext';
import SUBJECT_LABEL from '@salesforce/label/c.LSB_MassAssignmentFormSubject';
import DUE_DATE_LABEL from '@salesforce/label/c.LSB_MassAssignmentFormDueDate';
import PRIORITY_LABEL from '@salesforce/label/c.LSB_MassAssignmentFormPriority';
import COMMENTS_LABEL from '@salesforce/label/c.LSB_MassAssignmentFormComments';
import ASSIGNED_BY_LABEL from '@salesforce/label/c.LSB_MassAssignmentFormAssignedBy';
import SHARING_LABEL from '@salesforce/label/c.LSB_MassAssignmentFormSharing';
import LOADING from '@salesforce/label/c.LSB_Loading';
import NO_RESULTS from '@salesforce/label/c.LSB_NoResults';
import ATTACH_TO_ADVISEE from '@salesforce/label/c.LSB_AttachToAdvisee';
import SUCCESS_TOAST_MSG from '@salesforce/label/c.LSB_MassTasksAssignmentToastMessage';
import SUCCESS_TOAST_TITLE from '@salesforce/label/c.LSB_MassTasksAssignmentToastTitle';
import ERROR_TOAST_MSG from '@salesforce/label/c.LSB_MassTasksAssignmentToastMessageError';
import ERROR_TOAST_TITLE from '@salesforce/label/c.LSB_MassTasksAssignmentToastTitleError';
import CONFIRMATION_MSG from '@salesforce/label/c.LSB_MassTasksAssignmentConfirmationMessage';
import ATTACH_TO_SUPPORT_PROFILE from '@salesforce/label/c.LSB_AttachToSupportProfile';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const DATE_ERROR_MESSAGE = 'Date cannot be in past.';

export default class LsbMassAssignmentForm extends LightningElement {
    reports;
    selectedReport;
    error;
    message;
    @track contactListSize = 0;

    subject;
    priority;
    activityDate;
    comments;
    sharingObjectType;
    reportValid = false;
    isUserPermitted = false;
    canAssignSupportProfile = false;

    ownerClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    iconFlagOwner = false;
    clearIconFlagOwner = false;
    ownersList = [];
    messageFlagOwner = false;
    ownerName;
    assignedBy;
    iconUser = 'action:user';
    ownerLoadingText = false;

    labels = {
        PERMISSION_ERROR,
        FIELDS_ERROR,
        INSUFICIENT_PRIVILEGES,
        SHARING_HELPTEXT,
        SUBJECT_LABEL,
        DUE_DATE_LABEL,
        PRIORITY_LABEL,
        COMMENTS_LABEL,
        ASSIGNED_BY_LABEL,
        SHARING_LABEL,
        LOADING,
        NO_RESULTS,
        ATTACH_TO_ADVISEE,
        ATTACH_TO_SUPPORT_PROFILE,
        SUCCESS_TOAST_MSG,
        SUCCESS_TOAST_TITLE,
        ERROR_TOAST_MSG,
        ERROR_TOAST_TITLE,
        CONFIRMATION_MSG
    };

    priorityOptions = [ // TODO: Remove hardcodes -> will be good to retrieve values from schema
        { label: 'High', value: 'High' },
        { label: 'Normal', value: 'Normal' },
        { label: 'Low', value: 'Low' },
    ];

    sharingOptions = [
        { label: this.labels.ATTACH_TO_ADVISEE, value: 'Case' },
        { label: this.labels.ATTACH_TO_SUPPORT_PROFILE, value: 'LSB_SUE_SupportProfile__c' },
    ];


    get reportNotSelected() {
        return this.reportValid == false;
    }

    renderedCallback() {
        this.isUserPermitted = hasPermission ?  true : false;

        if (this.isUserPermitted === false) {
            this.template.querySelector('c-lsb_-modal')
                .openModal(
                    {
                        header: this.labels.INSUFICIENT_PRIVILEGES,
                        message: this.labels.PERMISSION_ERROR,
                        hideFooter: true
                    });
        }

        this.setSharingOptionForUser();

        const style = document.createElement('style');
        style.innerText = '.forceDockingPanel--dockableLSB_MassAssignTasks .cuf-content { height: 379px; overflow-y:scroll }';
        if (this.template.querySelector('lightning-input')){
            this.template.querySelector('lightning-input').appendChild(style);
        }
    }

    setSharingOptionForUser() {
        this.canAssignSupportProfile = hasSupportProfilePermission ? true : false;
        if (!this.canAssignSupportProfile) {
            this.sharingObjectType = this.sharingOptions[0].value;
        }
    }

    @wire(getReports)
    wiredReports({ error, data }) {
        if (data) {
            let reportArray = [];
            for (let i = 0; i < data.length; i++)  {
                reportArray = [...reportArray, {value: data[i].Id, label: data[i].Name} ];
            }
            this.reports = reportArray;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.reports = undefined;
        }
    }

    handleChange(event) {
        this.selectedReport = event.detail.value;
        if (event.target.name === 'reportPicklist') {
            this.validateReport();
        }
    }

    handleSharingChange(event) {
        this.sharingObjectType = event.detail.value;
    }

    handlePriorityChange(event) {
        this.priority = event.detail.value;
    }

    handleSubjectChange(event) {
        this.subject = event.detail.value;
    }

    handleDescriptiontChange(event) {
        this.comments = event.detail.value;
    }

    handleActivityDateChange(event) {
        this.activityDate = event.detail.value;
        let activityDate = new Date(this.activityDate);

        if (event.target.name === 'activityDate') {
            this.validateDate(activityDate);
        }

    }

    validateDate(activityDate) {
        let activityDateField = this.template.querySelector("[data-id='activityDate']");
        let today = new Date();
        let todayDateValue = this.getDateValue(today);
        let activityDateValue = this.getDateValue(activityDate);

        if (activityDateValue < todayDateValue) {
            let errorMessage = DATE_ERROR_MESSAGE;
            activityDateField.setCustomValidity(errorMessage);
            activityDateField.reportValidity();
        } else {
            activityDateField.setCustomValidity("");
            activityDateField.reportValidity();
        }
    }

    getDateValue(initialDate) {
        const year = initialDate.getFullYear()
        const month = initialDate.getMonth()
        const day = initialDate.getDate()
        const finalDate = new Date(year, month, day);
        return finalDate;
    }

    handleSubmit() {
        const allValid = [...this.template.querySelectorAll('lightning-input, lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid) {
            if (confirm(this.labels.CONFIRMATION_MSG)) {
                this.handleRun();
            }
        }
    }

    handleCloseModal() {
        const closePopUp = new CustomEvent('closepopup');
        this.dispatchEvent(closePopUp);
    }

    handleRun() {
        let taskObject = {
            Subject: this.subject,
            Priority: this.priority,
            ActivityDate: this.activityDate,
            Description: this.comments,
            LSB_ACT_AssignedBy__c: this.assignedBy
        }


        runBatch({ reportId: this.selectedReport, objectJSON: JSON.stringify(taskObject), sharingObject: this.sharingObjectType})
            .then((result) => {
                this.message = result;
                this.showToast('success', this.labels.SUCCESS_TOAST_TITLE, this.labels.SUCCESS_TOAST_MSG);
                this.handleCloseModal();
            })
            .catch((error) => {
                this.error = error;
                this.showToast('error', this.labels.ERROR_TOAST_TITLE, this.labels.ERROR_TOAST_MSG);
                this.handleCloseModal();
            });
    }

    validateReport() {
        let reportField = this.template.querySelector("[data-id='report-picklist']");

        validateReportColumns({reportId: this.selectedReport})
            .then(() => {
                reportField.setCustomValidity("");
                reportField.reportValidity();
                this.reportValid = true;
            })
            .catch((error) => {
                let errorMessage = error.body.message;
                reportField.setCustomValidity(errorMessage);
                reportField.reportValidity();
                this.reportValid = false;
            });
    }

    searchUsersForSearch (event) {
        let ownerName = event.target.value;
        this.ownerLoadingText = true;
        fetchUsers({userName: ownerName})
            .then(result => {
                this.ownersList = result;
                this.ownerLoadingText = false;

                this.ownerClass =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
                if (ownerName.length > 0 && result.length === 0) {
                    this.messageFlagOwner = true;
                }
                else {
                    this.messageFlagOwner = false;
                }

                if (this.assignedBy) {
                    this.iconFlagOwner = false;
                    this.clearIconFlagOwner = true;
                }
                else {
                    this.iconFlagOwner = true;
                    this.clearIconFlagOwner = false;
                }
            })
            .catch(error => {
                this.showToast('error', 'Error', error);
            });
    }

    resetOwner() {
        this.ownerName = null;
        this.assignedBy = null;
        this.resetPOwnerFlags ();
    }

    resetPOwnerFlags() {
        this.iconFlagOwner = true;
        this.clearIconFlagOwner = false;
    }

    setSearchOwner(event) {
        this.assignedBy = event.currentTarget.dataset.id;
        this.ownerName = event.currentTarget.dataset.name;
        this.iconFlagOwner = false;
        this.clearIconFlagOwner = true;
        this.ownerClass =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    }

    showToast(variant, title, message) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            }),
        );
    }

    get sharingDisabled() {
        return this.reportNotSelected || !this.canAssignSupportProfile;
    }
}