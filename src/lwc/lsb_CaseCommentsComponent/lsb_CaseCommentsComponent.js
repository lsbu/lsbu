import {LightningElement, api, track, wire} from 'lwc';

import {getRecord, getFieldValue} from 'lightning/uiRecordApi';
import SMALL_PHOTO_URL from '@salesforce/schema/User.SmallPhotoUrl';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import getCaseMessages from '@salesforce/apex/LSB_CaseCommentsController.getCaseMessagesByParentId';
import createCaseComment from '@salesforce/apex/LSB_CaseCommentsController.createCaseComment';

import chatterPlaceholder from '@salesforce/label/c.LSB_ChatterPlaceholder';
import postedBy from '@salesforce/label/c.LSB_PostedBy';
import caseCommentsTitle from '@salesforce/label/c.LSB_CaseCommentsTitle';
import hideEmailsButton from '@salesforce/label/c.LSB_HideEmailsButton';
import showEmailsButton from '@salesforce/label/c.LSB_ShowEmailsButton';

import LOCALE from '@salesforce/i18n/locale';
import TIMEZONE from '@salesforce/i18n/timeZone';
import Id from '@salesforce/user/Id';

const EMAIL_REFERENCE_NUMBER = 'ref:_';
const EMAIL_REFERENCE_SYMBOL = '--------------- Original Message ---------------';

export default class LsbCaseCommentsComponent extends LightningElement {

    @api parentRecordId;
    @track caseMessages;

    componentReady;
    showSpinner = true;
    @track showEmails = false;
    hasEmails = false;

    labels = {
        chatterPlaceholder,
        postedBy,
        caseCommentsTitle,
        hideEmailsButton,
        showEmailsButton
    };

    dateTimeFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric',
        timeZone: TIMEZONE
    });

    userId = Id;
    caseComment;

    @wire(getRecord, {recordId: '$userId', fields: [SMALL_PHOTO_URL]})
    userRecord;

    get userPhoto() {
        return getFieldValue(this.userRecord.data, SMALL_PHOTO_URL);
    }

    get caseMessagesEmpty() {
        return typeof this.caseMessages === "undefined" || this.caseMessages.length === 0;
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.parentRecordId && !this.componentReady) {
            this.componentReady = true;
            getCaseMessages({
                parentRecordId: this.parentRecordId
            }).then(result => {
                this.caseMessages = this.processMessages(result);
            }).catch(error => {
                console.log('Error: ' + JSON.stringify(error));
            }).finally(() => {
                this.showSpinner = false;
            });
        }
    }

    processMessages(caseMessages) {
        if (caseMessages) {
            caseMessages.sort(function (a, b) {
                return new Date(a.createdDate) - new Date(b.createdDate)
            });
        }
        let messages = [];
        caseMessages.forEach(caseMessage => {
            caseMessage.createdDate = this.dateTimeFormat.format(new Date(caseMessage.createdDate));
            let singleMessage;
            if (caseMessage.caseComment) {
                singleMessage = this.processCaseComments(caseMessage);
            } else if (caseMessage.emailMessage) {
                this.hasEmails = true;
                singleMessage = this.processEmailMessage(caseMessage);
            }
            messages.push(singleMessage);
        });
        return messages.sort(function (a, b) {
            return new Date(a.createdDate) - new Date(b.createdDate)
        });
    }

    createCaseMessage(id, isEmail, ariaLabel, photoUrl, senderInfo, messageBody, createdDate) {
        let singleMessage = {
            id: id,
            isEmail: isEmail,
            ariaLabel: ariaLabel,
            photoUrl: photoUrl,
            senderInfo: senderInfo,
            messageBody: messageBody,
            createdDate: createdDate
        }
        return singleMessage;
    }

    processCaseComments(caseMessage) {
        let ariaLabel = caseMessage.caseComment.ariaLabel = postedBy + ' ' + caseMessage.caseComment.CreatedBy.FirstName + ' '
            + caseMessage.caseComment.CreatedBy.LastName + ', ' + caseMessage.createdDate + ': ' + caseMessage.caseComment.CommentBody;
        let senderInfo = `${caseMessage.caseComment.CreatedBy.FirstName} ${caseMessage.caseComment.CreatedBy.LastName}, ${caseMessage.createdDate}`;
        return this.createCaseMessage(caseMessage.caseComment.Id, false, ariaLabel, caseMessage.caseComment.CreatedBy.SmallPhotoUrl, senderInfo, caseMessage.caseComment.CommentBody, caseMessage.createdDate);
    }

    processEmailMessage(caseMessage) {
        let formattedBody = this.formatEmailBody(caseMessage);
        let ariaLabel = 'sent by ' + caseMessage.emailMessage.FromName + ', ' + caseMessage.createdDate + ': ' + caseMessage.emailMessage.TextBody;
        let senderInfo = `${caseMessage.emailMessage.FromName} from email ${caseMessage.emailMessage.FromAddress}, ${caseMessage.createdDate}`;
        return this.createCaseMessage(caseMessage.emailMessage.Id, true, ariaLabel, '', senderInfo, formattedBody, caseMessage.createdDate);
    }

    formatEmailBody(caseMessage) {
        let emailBodyRaw = caseMessage.emailMessage.HtmlBody;
        let formattedBody = emailBodyRaw.replace(/<img .*?>/g, "").replace(/url.*?;/g, "").replace(/style=".*?"/g, "");
        formattedBody = formattedBody.split(EMAIL_REFERENCE_NUMBER)[0].split(EMAIL_REFERENCE_SYMBOL)[0];
        return formattedBody;
    }

    onCaseCommentInput(event) {
        let value = event.currentTarget.value;
        if (!value.replace(/\s/g, '').length) {
            this.caseComment = undefined;
        } else {
            this.caseComment = value;
        }
    }

    addCaseComment(event) {
        this.showSpinner = true;
        createCaseComment({
            caseComment: this.caseComment,
            parentId: this.parentRecordId
        }).then(result => {
            this.componentReady = false;
            this.caseComment = undefined;
            eval("$A.get('e.force:refreshView').fire();");
        }).catch(error => {
            console.log(error);
        }).finally(() => {
            this.showSpinner = false;
        });
    }

    toggleShowEmail(event) {
        this.showEmails = !this.showEmails;
    }

    addCaseCommentOnEnter(event) {
        if (event.key === 'Enter' && typeof this.caseComment !== "undefined") {
            this.addCaseComment(event);
        }
    }
}