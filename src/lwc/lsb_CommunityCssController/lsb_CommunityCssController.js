/**
 * Created by elatoszek001 on 09.12.2020.
 */

import {LightningElement} from 'lwc';

export default class LsbCommunityCssController extends LightningElement {

    renderedCallback() {
        console.log('renderedCallback');
        const style = document.createElement('style');
        style.innerText = '.forcePageBlockSectionEdit .forcePageBlockItemView {\n' +
            '    border: transparent;\n' +
            '    display: none;\n' +
            '}\n';
        this.template.querySelector('lightning-input').appendChild(style);
    }

}