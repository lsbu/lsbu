import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// apex
import getStudentData from '@salesforce/apex/LSB_MeetFrontDeskFormController.fetchStudentData';
import checkInStudent from '@salesforce/apex/LSB_MeetFrontDeskFormController.createAdvisingQueueCase';

// labels
import buttonLabel from '@salesforce/label/c.LSB_SelfCheckInJoinQueueButton';
import studentIdFieldLabel from '@salesforce/label/c.LSB_SelfCheckInStudentIdFieldLabel';
import firstNameFieldLabel from '@salesforce/label/c.LSB_SelfCheckInFirstNameFieldLabel';
import lastNameFieldLabel from '@salesforce/label/c.LSB_SelfCheckInLastNameNameFieldLabel';
import emailFieldLabel from '@salesforce/label/c.LSB_SelfCheckInEmailFieldLabel';
import successCheckInMessage from '@salesforce/label/c.LSB_SelfCheckInSuccessCheckInMessage';
import successCheckInMessageAdvisor from '@salesforce/label/c.LSB_SelfCheckInSuccessCheckInMessageAdvisor';
import somethingWentWrong from '@salesforce/label/c.LSB_SelfCheckInErrorMessage';
import studentIdNotValid from '@salesforce/label/c.LSB_SelfCheckInStudentIdNotValidMessage';
import contactEmailDescription from '@salesforce/label/c.LSB_SelfCheckInContactEmailFieldDescription';

export default class LsbMeetFrontDeskForm extends LightningElement {
    @api hideButton = false;
    @api redirectUrl = '';
    @api advisorMode = false;
    @track studentId = '';
    @track firstName = '';
    @track lastName = '';
    @track contactEmail = '';

    @track labels = {
        buttonLabel,
        studentIdFieldLabel,
        firstNameFieldLabel,
        lastNameFieldLabel,
        emailFieldLabel,
        successCheckInMessage,
        successCheckInMessageAdvisor,
        somethingWentWrong,
        studentIdNotValid,
        contactEmailDescription
    };

    student;
    populateContactFields = false;
    blockSubmit = false;
    fireSubmit = false;

    fetchStudent(event) {
        this.blockSubmit = true;
        let inputValue = event.target.value;
        let studentIdField = this.template.querySelector(`[data-id="studentId"]`);

        if (inputValue == null || inputValue == undefined || inputValue == '') {
            return;
        }

        getStudentData({studentId: inputValue})
            .then(result => {
                this.student = result;
                studentIdField.setCustomValidity('');
                studentIdField.reportValidity();

                if (this.populateContactFields || this.advisorMode) {
                    if (this.firstName === '') {
                        let firstNameField = this.template.querySelector(`[data-id="firstName"]`);
                        firstNameField.value = result.FirstName;
                        firstNameField.setCustomValidity('');
                        firstNameField.reportValidity();
                    }

                    if (this.lastName === '') {
                        let lastNameField = this.template.querySelector(`[data-id="lastName"]`);
                        lastNameField.value = result.LastName;
                        lastNameField.setCustomValidity('');
                        lastNameField.reportValidity();
                    }
                }

                this.blockSubmit = false;

                if (this.fireSubmit) {
                    this.checkInAction();
                }
            }).catch(error => {
            studentIdField.setCustomValidity(this.labels.studentIdNotValid);
            studentIdField.reportValidity();

            this.blockSubmit = false;

            if (this.fireSubmit) {
                this.checkInAction();
            }
        });
    }

    handleInputChange(event) {
        let inputValue = event.target.value;
        this[event.target.name] = inputValue;

        if (inputValue != null || inputValue != undefined || inputValue != '') {
            let targetId = event.target.name;
            let inputField = this.template.querySelector(`[data-id="${targetId}"]`);
            inputField.setCustomValidity('');
            inputField.reportValidity();
        }
    }

    @api handleJoinQueueClick() {
        if (this.blockSubmit) {
            this.fireSubmit = true;
            return;
        }

        this.checkInAction();
    }

    checkInAction() {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if (allValid) {
            if (this.contactEmail == '') {
                this.contactEmail = this.student.hed__UniversityEmail__c;
            }

            checkInStudent({
                contactId: this.student.Id,
                firstName: this.firstName,
                lastName: this.lastName,
                contactEmail: this.contactEmail
            }).then(result => {
                if (result) {
                    this.showErrorMessage();
                } else {
                    this.showSuccessMessage();

                    if (this.redirectUrl != '') {
                        window.location.href = this.redirectUrl;
                    }
                }
            }).catch(error => {
                this.showErrorMessage();
            }).finally(() => {
                this.dispatchEvent(new CustomEvent('close'));
            });
        }
    }

    showSuccessMessage() {
        const evt = new ShowToastEvent({
            title: 'Success',
            message: (this.advisorMode ? this.labels.successCheckInMessageAdvisor : this.labels.successCheckInMessage),
            variant: 'success',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    showErrorMessage() {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: this.labels.somethingWentWrong,
            variant: 'error',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }
}