import {api, track, wire, LightningElement} from 'lwc';

import submitReviewRequest from '@salesforce/apex/LSB_ListViewController.submitReviewRequest';
import getEcClaimRecordTypeId from '@salesforce/apex/LSB_ListViewController.getEcClaimRecordTypeId';

import {getPicklistValues} from 'lightning/uiObjectInfoApi';
import REVIEW_REASON from '@salesforce/schema/Case.LSB_CAS_ReviewReason__c';

import saveButton from '@salesforce/label/c.LSB_Save';
import requestReview from '@salesforce/label/c.LSB_RequestReview';
import requestReviewHelpText from '@salesforce/label/c.LSB_RequestReviewHelpText';
import requestReviewSubmit from '@salesforce/label/c.LSB_RequestReviewSubmit';
import cancelButton from '@salesforce/label/c.LSB_Cancel';
import writeDetailsHere from '@salesforce/label/c.LSB_WriteDetailsHere';
import reviewReasons from '@salesforce/label/c.LSB_ReviewReasons';
import reviewComments from '@salesforce/label/c.LSB_ReviewComments';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

export default class LsbRequestReview extends LightningElement {

    @track reviewReasons = [];
    @api recordId;

    componentReady = false;
    showModal = false;
    showSpinner = false;

    labels = {
        requestReview,
        requestReviewHelpText,
        requestReviewSubmit,
        saveButton,
        cancelButton,
        writeDetailsHere,
        reviewComments,
        reviewReasons
    };

    @wire(getEcClaimRecordTypeId)
    getClaimRecordTypeId(result) {
        if (result.data) {
            this.ecClaimRecordTypeId = result.data;
        }
    }

    ecClaimRecordTypeId;

    @wire(getPicklistValues, {
        recordTypeId: '$ecClaimRecordTypeId',
        fieldApiName: REVIEW_REASON
    })
    reasonsPicklist(result) {
        if (result.data) {
            this.reviewReasons = result.data.values;
        } else if (result.error) {
            console.log(result.error);
        }
    }

    handleOpenModal() {
        this.showModal = true;
    }

    handleCloseModal() {
        this.reviewReason = undefined;
        this.reviewComment = undefined;
        this.showModal = false;
    }

    reviewReason;
    reviewComment;

    handleChange(event) {
        this.reviewReason = event.detail.value;
    }

    handleReviewComment(event) {
        this.reviewComment = event.detail.value;
    }

    handleSubmit() {
        if (typeof this.reviewReason === "undefined") {
            return;
        }
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: this.labels.requestReview,
                    message: this.labels.requestReviewSubmit
                });
    }

    handleRequestReview(event) {
        if (event.detail) {
            this.showSpinner = true;
            submitReviewRequest({
                caseId: this.recordId,
                reviewReason: this.reviewReason,
                reviewComment: this.reviewComment
            }).then(result => {
                eval("$A.get('e.force:refreshView').fire();");
                this.showSpinner = false;
            }).catch(error => {
                console.log(error);
                eval("$A.get('e.force:refreshView').fire();");
                this.showSpinner = false;
            });
        } else {
            let saveButton = this.template.querySelector('.save__button');
            saveButton.focus();
        }
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        this.componentReady = true;
        this.addListeners();
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.modal__main-container');
        if (modalContainer) {
            modalContainer.focus();
            modalContainer.addEventListener('keydown', (e) => {
                let closeButton = this.template.querySelector('.close__button');
                let saveButton = this.template.querySelector('.save__button');
                if (e.shiftKey && e.key === 'Tab' && e.target === closeButton) {
                    e.preventDefault();
                    saveButton.focus();
                } else if (!e.shiftKey && e.key === 'Tab' && e.target === saveButton) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }
}