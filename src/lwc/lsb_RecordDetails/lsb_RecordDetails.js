import {LightningElement, api} from 'lwc';

import fetchListViewColumns from '@salesforce/apex/LSB_ListViewController.fetchListViewColumns';
import fetchRecord from '@salesforce/apex/LSB_ListViewController.fetchRecord';

import LOCALE from '@salesforce/i18n/locale';
import CURRENCY from '@salesforce/i18n/currency';
import TIMEZONE from '@salesforce/i18n/timeZone';

import CASE_OBJECT from '@salesforce/schema/Case';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import informationAboutEnquiry from '@salesforce/label/c.LSB_InformationAboutEnquiry';

const CASE_STATUS_RESPONDED = 'Responded';

export default class LsbRecordDetails extends LightningElement {

    componentReady;

    @api recordId;
    @api objectApiName;
    @api fieldSetApiName;
    @api statusField;
    @api showStatusDefinitions;
    @api headerText;

    fields;
    record;

    labels = {
        informationAboutEnquiry
    }


    dateTimeFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric',
        timeZone: TIMEZONE
    });

    dateFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        timeZone: TIMEZONE
    });

    currencyFormat = new Intl.NumberFormat(LOCALE, {
        style: 'currency',
        currency: CURRENCY,
        currencyDisplay: 'symbol'
    });

    disconnectedCallback() {
        this.componentReady = false;
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.componentReady) {
            return;
        }
        fetchListViewColumns({
            objectApiName: this.objectApiName,
            fieldSetApiName: this.fieldSetApiName
        }).then(result => {
            this.fields = result;
            fetchRecord({
                objectApiName: this.objectApiName,
                recordId: this.recordId,
                fields: this.fields.map(x => x.fieldNameOrPath)
            }).then(result => {
                this.record = result;
                this.componentReady = true;
            }).catch(error => {
                console.log('Error: ' + error.body.message);
            })
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

    get recordTypeDeveloperName() {
        if (typeof this.record.RecordType !== "undefined") {
            return this.record.RecordType.DeveloperName;
        } else {
            return null;
        }
    }

    get status() {
        return this.record[this.statusField];
    }

    get statusFieldLabel() {
        return this.fields.find(x => x.fieldNameOrPath === this.statusField).label;
    }

    get recordDetails() {
        let recordDetails = [];
        this.fields.forEach(field => {
            if (field.fieldNameOrPath !== this.statusField
                && !field.hidden
            ) {
                let fieldObj = {
                    column: field,
                    label: field.label,
                    value: this.record[field.fieldNameOrPath],
                    isUrl: field.type === 'URL',
                    isRichText: field.type === 'TEXTAREA' && field.isHtmlFormatted,
                    isTextArea: field.type === 'TEXTAREA' && !field.isHtmlFormatted,
                    isOther: field.type !== 'URL' && field.type !== 'TEXTAREA'
                };
                this.setPicklistLabels(fieldObj, this.record);
                this.setLocaleOnField(fieldObj);
                recordDetails.push(fieldObj);
            }
        });
        return recordDetails;
    }

    setPicklistLabels(field, record) {
        if (field.column.type === 'PICKLIST') {
            field.displayValue = record[field.column.fieldNameOrPath + 'Label'];
        }
    }

    setLocaleOnField(field) {
        if (field.column.type === 'DATETIME' && typeof field.value !== "undefined") {
            field.value = this.dateTimeFormat.format(new Date(field.value));
        } else if (field.column.type === 'DATE' && typeof field.value !== "undefined") {
            field.value = this.dateFormat.format(new Date(field.value));
        } else if (field.column.type === 'CURRENCY' && typeof field.value !== "undefined") {
            field.value = this.currencyFormat.format(field.value);
        }
    }

    get hasWarning() {
        return this.isCaseResponded();
    }

    isCaseResponded() {
        return this.objectApiName === CASE_OBJECT.objectApiName && this.record.Status === CASE_STATUS_RESPONDED;
    }
}