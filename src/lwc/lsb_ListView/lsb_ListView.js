import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';

import fetchListViewColumns from '@salesforce/apex/LSB_ListViewController.fetchListViewColumns';
import fetchRecords from '@salesforce/apex/LSB_ListViewController.fetchRecords';
import changeTaskStatus from '@salesforce/apex/LSB_ListViewController.changeTaskStatus';

import markComplete from '@salesforce/label/c.LSB_MarkComplete';
import userAction from '@salesforce/label/c.LSB_UserAction';
import submitTask from '@salesforce/label/c.LSB_SubmitTask';
import sortBy from '@salesforce/label/c.LSB_SortBy';
import sortOptions from '@salesforce/label/c.LSB_SortOptions';
import sortAscending from '@salesforce/label/c.LSB_SortAscending';
import sortDescending from '@salesforce/label/c.LSB_SortDescending';
import closed from '@salesforce/label/c.LSB_CLOSED';
import taskOverdue from '@salesforce/label/c.LSB_TaskOverdue';
import inputRequired from '@salesforce/label/c.LSB_InputRequired';

import LOCALE from '@salesforce/i18n/locale';
import CURRENCY from '@salesforce/i18n/currency';
import TIMEZONE from '@salesforce/i18n/timeZone';

const TASK_OBJECT_API_NAME = 'Task';
const TASK_COMPLETED = 'Completed';
const CASE_OBJECT_API_NAME = 'Case';
const EC_CLAIM_RECORDTYPE = 'LSB_CAS_ECClaim';
const CASE_STATUS_CLOSED = "Closed";
const REQUEST_SUPPORTED = 'Request Supported';
const REQUEST_REJECTED = 'Request Rejected';
const REQUEST_APPROVED_AFTER_REVIEW = 'Request Approved After Review';
const REQUEST_REJECTED_AFTER_REVIEW = 'Request Rejected After Review';
const REQUEST_WITHDRAWN_BY_STUDENT = 'Request Withdrawn by Student';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

export default class Lsb_ListView extends NavigationMixin(LightningElement) {


    labels = {
        markComplete,
        userAction,
        submitTask,
        sortBy,
        sortOptions,
        sortAscending,
        sortDescending,
        closed,
        inputRequired,
        taskOverdue
    };

    componentReady = false;

    @api listViewHeader;
    @api objectApiName;
    @api fieldSetApiName;
    @api icon;
    @api mergeFirstColumnWithSecond = false;
    @api queryFilter;
    @api sortByColumn;
    @api statusFieldApiName;
    @api showStatusIndicator = false;
    @api closedStatuses;
    @api rowClickBehaviour = 'Standard';
    @api detailPage;
    @api showTaskCompleteAction = false;
    @api showCaseWithdrawAction = false;
    @api applyDifferentStyle = false;
    @api emptyTableHeader = '';
    @api emptyTableDesc = '';


    @track columns;
    @track records;
    error;

    relatedColumns;
    showSortPicklist = false;

    dateTimeFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric',
        timeZone: TIMEZONE
    });

    dateFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        timeZone: TIMEZONE
    });

    currencyFormat = new Intl.NumberFormat(LOCALE, {
        style: 'currency',
        currency: CURRENCY,
        currencyDisplay: 'symbol'
    });

    disconnectedCallback() {
        this.componentReady = false;
    }

    get styleClass() {
        return this.applyDifferentStyle ? 'support__profile' : null;
    }

    get showStaticText() {
        return this.records.length === 0 && this.emptyTableDesc != '' && this.emptyTableHeader;
    }

    get iconClass() {
        return "mdi " + this.icon;
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.componentReady) {
            this.addListeners();
            return;
        }
        fetchListViewColumns({
            objectApiName: this.objectApiName,
            fieldSetApiName: this.fieldSetApiName
        }).then(result => {
            let relationships = [];
            let relatedColumns = [];
            let columns = [];
            let index = 0;
            result.forEach(column => {
                let columnObj = {...column};
                if (this.mergeFirstColumnWithSecond && index === 1) {
                    columnObj.hidden = true;
                }
                if (columnObj.type === 'REFERENCE') {
                    columnObj.relatedColumns = [];
                    relationships.push(columnObj);
                }
                if (columnObj.fieldNameOrPath.includes('.')) {
                    let relationshipName = columnObj.fieldNameOrPath.split('.')[0];
                    let related = relationships.find(x => x.relationshipName === relationshipName);
                    if (typeof related !== "undefined") {
                        related.relatedColumns.push(columnObj);
                        relatedColumns.push(columnObj);
                    } else {
                        columns.push(columnObj);
                    }
                } else {
                    columns.push(columnObj);
                }
                index++;
            });
            this.relatedColumns = relatedColumns;
            this.columns = columns;
            this.fetchRecords();
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

    fetchRecords() {
        fetchRecords({
            objectApiName: this.objectApiName,
            queryFilter: this.queryFilter,
            fields: this.columns.map((col) => col.fieldNameOrPath).concat(this.relatedColumns.map((col) => col.fieldNameOrPath))
        }).then(result => {
            let records = [];
            let closedStatusesArray = this.closedStatuses ? this.closedStatuses.split(';') : [];
            result.forEach(record => {
                let recordObj = {};
                recordObj.id = record.Id;
                recordObj.fields = [];
                recordObj.recordTypeId = record.RecordTypeId;
                recordObj.recordTypeDeveloperName = record.RecordTypeId ? record.RecordType.DeveloperName : null;
                if (this.objectApiName === TASK_OBJECT_API_NAME) {
                    let now = new Date();
                    now.setHours(0, 0, 0, 0);
                    recordObj.isTaskCompleted = closedStatusesArray.includes(record.Status);
                    recordObj.isTaskOverdue = typeof record.ActivityDate !== "undefined" ? !closedStatusesArray.includes(record.Status)
                        && new Date(record.ActivityDate) < now : false;
                    recordObj.contentRowStyle = recordObj.isTaskOverdue ? "content__row content__row-overdue" : "content__row";
                    recordObj.isTaskOpenAndNotOverdue = !recordObj.isTaskCompleted && !recordObj.isTaskOverdue;
                } else if (this.objectApiName === CASE_OBJECT_API_NAME) {
                    recordObj.isEcCase = recordObj.recordTypeDeveloperName === EC_CLAIM_RECORDTYPE;
                    recordObj.isCaseClosed = record.Status === CASE_STATUS_CLOSED
                        || record.Status === REQUEST_SUPPORTED
                        || record.Status === REQUEST_REJECTED
                        || record.Status === REQUEST_APPROVED_AFTER_REVIEW
                        || record.Status === REQUEST_REJECTED_AFTER_REVIEW
                        || record.Status === REQUEST_WITHDRAWN_BY_STUDENT;
                    recordObj.isCaseResponded = record.Status === 'Responded';
                    recordObj.contentRowStyle = recordObj.isCaseResponded ? "content__row content__row-input-required" : "content__row";
                } else {
                    recordObj.contentRowStyle = "content__row";
                }
                recordObj.hasWarning = recordObj.isTaskOverdue || recordObj.isCaseResponded;
                let index = 0;
                this.columns.forEach(column => {
                    let field = {};
                    field.column = column;
                    field.fieldApiName = column.fieldNameOrPath;
                    field.isStatus = field.fieldApiName === this.statusFieldApiName;
                    this.setAdditionalTypes(field, column);
                    this.makeAdditionalChangesToField(index, field, record, column, closedStatusesArray);
                    this.setLocaleOnField(field);
                    this.setAriaLabel(field);
                    this.setPicklistLabels(field, record);
                    recordObj.fields.push(field);
                    index++;
                });
                records.push(recordObj);
            });
            this.records = records;
            if (typeof this.sortByColumn !== "undefined") {
                let sortUp = !this.sortByColumn.startsWith('-');
                let columnName = this.sortByColumn.replace('-', '');
                let columnIndex = this.columns.map(x => x.fieldNameOrPath).indexOf(columnName);
                if (typeof columnIndex !== "undefined") {
                    this.sort(columnIndex, sortUp);
                }
            }
            this.componentReady = true;
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

    setPicklistLabels(field, record) {
        if (field.column.type === 'PICKLIST') {
            field.displayValue = record[field.column.fieldNameOrPath + 'Label'];
        }
    }

    makeAdditionalChangesToField(index, field, record, column, closedStatusesArray) {
        if (this.mergeFirstColumnWithSecond && index === 0) {
            field.bottomText = record[this.columns[1].fieldNameOrPath];
        }
        if (field.column.type === 'REFERENCE') {
            let relatedRecord = record[field.column.relationshipName];
            field.isReference = true;
            field.values = [];
            if (relatedRecord) {
                field.column.relatedColumns.forEach(column => {
                    let fieldObj = {};
                    fieldObj.value = relatedRecord[column.fieldNameOrPath.split('.')[1]];
                    fieldObj.column = column;
                    this.setAdditionalTypes(fieldObj, column);
                    this.setLocaleOnField(fieldObj);
                    this.setAriaLabel(fieldObj);
                    field.values.push(fieldObj);
                });
            }
        }
        if (field.fieldApiName.includes('.')) {
            let relationshipField = field.fieldApiName.split('.');
            if (typeof record[relationshipField[0]] !== "undefined") {
                field.value = record[relationshipField[0]][relationshipField[1]];
            }
        } else {
            field.value = record[column.fieldNameOrPath];
        }
        if (field.isStatus) {
            field.isClosed = closedStatusesArray.includes(field.value);
        }
    }

    setAriaLabel(field) {
        if (field.column.type === 'REFERENCE') {
            field.ariaLabel = field.column.label + ': ';
            field.values.forEach(value => {
                if (!value.isPhoto) {
                    field.ariaLabel += value.ariaLabel + ', ';
                }
            });
        } else if (typeof field.displayValue !== "undefined") {
            field.ariaLabel = field.column.label + ': ' + field.displayValue + ',';
        } else {
            field.ariaLabel = field.column.label + ': ' + field.value + ', ';
        }
    }

    setAdditionalTypes(field, column) {
        field.isUrl = column.type === 'URL' && !column.fieldNameOrPath.endsWith('PhotoUrl');
        field.isPhoto = column.type === 'URL' && column.fieldNameOrPath.endsWith('PhotoUrl');
        field.isNormal = !field.isPhoto && !field.isUrl && !field.isStatus;
    }

    setLocaleOnField(field) {
        if (field.column.type === 'DATETIME' && typeof field.value !== "undefined") {
            field.displayValue = this.dateTimeFormat.format(new Date(field.value));
        } else if (field.column.type === 'DATE' && typeof field.value !== "undefined") {
            field.displayValue = this.dateFormat.format(new Date(field.value));
        } else if (field.column.type === 'CURRENCY' && typeof field.value !== "undefined") {
            field.displayValue = this.currencyFormat.format(field.value);
        }
    }

    sortColumn(event) {
        let columnIndex = event.currentTarget.dataset.index;
        this.sort(columnIndex);
    }

    onSortBy(event) {
        let columnIndex = event.currentTarget.dataset.index;
        let sortUp = event.currentTarget.dataset.sortUp === 'true';
        this.sort(columnIndex, sortUp);
        this.showSortPicklist = false;
    }

    sort(columnIndex, sortUp) {
        for (let i = 0; i < this.columns.length; i++) {
            if (i === Number(columnIndex)) {
                this.columns[i].sortBy = true;
                this.columns[i].sortUp = typeof sortUp === "undefined" ? !this.columns[i].sortUp : sortUp;
                if (this.columns[i].sortUp) {
                    this.records.sort((a, b) => typeof a.fields[i].value === "undefined" ? -1 : (a.fields[i].value > b.fields[i].value) ? -1 : ((b.fields[i].value > a.fields[i].value) ? 1 : 0));
                } else {
                    this.records.sort((a, b) => typeof b.fields[i].value === "undefined" ? -1 : (a.fields[i].value > b.fields[i].value) ? 1 : ((b.fields[i].value > a.fields[i].value) ? -1 : 0));
                }
                if (this.objectApiName == TASK_OBJECT_API_NAME) {
                    this.records.sort((a, b) => a.isTaskCompleted ? 1 : b.isTaskCompleted ? -1 : 0);
                }
            } else {
                this.columns[i].sortBy = false;
                this.columns[i].sortUp = false;
            }
        }
    }

    get tabIndex() {
        if (this.rowClickBehaviour === 'No click') {
            return -1;
        } else {
            0;
        }
    }

    handleRowClick(event) {
        let recordId = event.currentTarget.dataset.index;
        if (this.rowClickBehaviour === 'Standard') {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: recordId,
                    objectApiName: this.objectApiName,
                    actionName: 'view'
                }
            });
        }
        if (this.rowClickBehaviour === 'Custom') {
            window.location.href = '/s/' + this.detailPage + '/?recordId=' + recordId;
        }
    }

    record;

    handleCompleteTask(event) {
        this.record = this.records.find(x => x.id === event.currentTarget.dataset.index);
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: this.labels.userAction,
                    message: this.labels.submitTask
                });
    }

    handleTaskSubmit(event) {
        if (event.detail) {
            changeTaskStatus({
                recordId: this.record.id,
                newStatus: TASK_COMPLETED
            }).then(result => {
                let field = this.record.fields.find(x => x.fieldApiName === 'Status');
                field.value = TASK_COMPLETED;
                field.isClosed = true;
                this.record.isTaskCompleted = true;
                this.record.isTaskOverdue = false;
                this.record.isTaskOpenAndNotOverdue = false;
            }).catch(error => {
                console.log(error);
            });
        }
    }

    openSortPicklist(event) {
        this.showSortPicklist = true;
    }

    closeSortPicklist(event) {
        this.showSortPicklist = false;
    }

    get isTask() {
        return this.objectApiName === TASK_OBJECT_API_NAME;
    }

    get isCase() {
        return this.objectApiName === CASE_OBJECT_API_NAME;
    }

    get isOther() {
        return !this.isCase && !this.isTask;
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.custom-modal__container');
        if (modalContainer) {
            modalContainer.focus();
            modalContainer.addEventListener('keydown', (e) => {
                let sortOptions = this.template.querySelectorAll('.sort__option');
                let closeButton = this.template.querySelector('.custom-modal__closebutton');
                let lastElement = sortOptions[sortOptions.length - 1];
                if (e.shiftKey && e.key === 'Tab' && (e.target === modalContainer || e.target === closeButton)) {
                    e.preventDefault();
                    lastElement.focus();
                } else if (!e.shiftKey && e.target === lastElement) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }
}