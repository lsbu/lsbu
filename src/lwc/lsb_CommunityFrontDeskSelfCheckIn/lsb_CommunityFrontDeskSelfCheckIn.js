import {LightningElement, track, wire, api} from 'lwc';

import qrcode from './qrcode.js'

// apex
import fetchTotalInQueue from '@salesforce/apex/LSB_MeetFrontDeskFormController.fetchTotalInQueue';

// labels
import joinQueueButtonLabel from '@salesforce/label/c.LSB_SelfCheckInJoinQueueButton';
import meetButtonLabel from '@salesforce/label/c.LSB_SelfCheckInMeetWithFrontDeskButtonLabel';
import cancelButtonLabel from '@salesforce/label/c.LSB_SelfCheckInCancelButtonLabel';
import closeButtonLabel from '@salesforce/label/c.LSB_SelfCheckInCloseButtonLabel';
import modalBoxHeader from '@salesforce/label/c.LSB_SelfCheckInModalBoxHeader';
import qrCodeDescription from '@salesforce/label/c.LSB_SelfCheckInQRCodeDescription';
import totalTimeLabel from '@salesforce/label/c.LSB_SelfCheckInTotalTime';
import totalInQueueLabel from '@salesforce/label/c.LSB_SelfCheckInTotalInQueue';

export default class LsbCommunityFrontDeskSelfCheckIn extends LightningElement {
    @api minutesPerStudent = 5;
    @api advisorMode = false;
    @track isModalOpen = false;
    @track totalInQueue = 0;
    isSubmitting = false;

    @track labels = {
        joinQueueButtonLabel,
        cancelButtonLabel,
        closeButtonLabel,
        meetButtonLabel,
        modalBoxHeader,
        qrCodeDescription,
        totalInQueueLabel,
        totalTimeLabel
    };

    get totalTotalInQueueText() {
        return totalInQueueLabel.replace('{0}', this.totalInQueue);
    }

    get totalTimeInMinText() {
        return totalTimeLabel.replace('{0}', (this.totalInQueue * this.minutesPerStudent));
    }

    connectedCallback() {
        this._interval = setInterval(() => {
            fetchTotalInQueue({}).then(result => {
                this.totalInQueue = result;
            }).catch(error => {
                this.totalInQueue = undefined;
            });
            if ( this.totalInQueue === 60000 ) {
                clearInterval(this._interval);
            }
        }, 60000); // every minute

    }

    renderedCallback() {
        if (!this.advisorMode) {
            const qrCodeGenerated = new qrcode(0, 'H');
            let strForGenearationOfQRCode  = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;
            qrCodeGenerated.addData(strForGenearationOfQRCode);
            qrCodeGenerated.make();
            let element = this.template.querySelector(".qrcode");
            element.innerHTML = qrCodeGenerated.createSvgTag({});
        }

        fetchTotalInQueue({}).then(result => {
            this.totalInQueue = result;
        }).catch(error => {
            console.log('Error: ' + error.body.message);
            this.totalInQueue = undefined;
        });
    }

    openModal() {
        this.isModalOpen = true;
        this.isSubmitting = false;
    }

    closeModal() {
        this.isModalOpen = false;
    }

    submitDetails() {
        if (this.isSubmitting) {
            return;
        }

        this.isSubmitting = true;
        this.template.querySelector("c-lsb_-meet-front-desk-form").handleJoinQueueClick();
        this.totalInQueue++;
    }
}