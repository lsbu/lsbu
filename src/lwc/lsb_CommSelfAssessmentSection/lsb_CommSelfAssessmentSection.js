import {LightningElement, api} from 'lwc';

export default class LsbCommSelfAssessmentSection extends LightningElement {

    @api
    sectionItems;
    @api isMobile;

    get sectionItemsData() {
        let _sectionItemsData = [];
        this.sectionItems.forEach(sar => {
            let _sar = {...sar};
            if (sar.LSB_SAR_SelfAssessmentQuestion__r.SelfAssessmentQuestion_Choices__r) {
                let choices = [];
                sar.LSB_SAR_SelfAssessmentQuestion__r.SelfAssessmentQuestion_Choices__r.records.forEach(choice => {
                    choices.push({
                        label: choice.LSB_SAC_SurveyQuestionChoiceFullName__c,
                        value: choice.Id
                    })
                });
                _sar.choices = choices;
            }
            _sectionItemsData.push(_sar);
        })
        return _sectionItemsData;
    }
}