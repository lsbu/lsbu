import {LightningElement, api, wire} from 'lwc';
import {NavigationMixin} from "lightning/navigation";

import findOutMore from '@salesforce/label/c.LSB_FindOutMore';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityTiles from '@salesforce/contentAssetUrl/communityTileszip';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import {subscribe, APPLICATION_SCOPE, MessageContext} from 'lightning/messageService';
import LSB_HAS_ADVISORS_MESSAGE from '@salesforce/messageChannel/LSB_HasAdvisors__c';

export default class LsbTile extends NavigationMixin(LightningElement) {

    @api header;
    @api description;
    @api bannerUrl;
    @api buttonUrl;
    @api icon;
    @api showBanner;
    @api isDetailTile;
    @api hideIfNoAdvisors = false;

    hideComponent = false;
    subscription = null;
    @wire(MessageContext)
    messageContext;


    labels = {
        findOutMore
    };

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
        this.subscription = subscribe(
            this.messageContext,
            LSB_HAS_ADVISORS_MESSAGE,
            (message) => {
                this.hideComponent = this.hideIfNoAdvisors && !message.hasAdvisors;
            },
            {scope: APPLICATION_SCOPE});
    }

    renderedCallback() {
        let descriptionElement = this.template.querySelector('.tile__description');
        if (descriptionElement) {
            descriptionElement.innerHTML = this.description;
        }
    }

    handleButtonClick(event) {
        if (this.buttonUrl) {
            window.location.href = '/s/' + this.buttonUrl;
        }
    }

    get banner() {
        return communityTiles + 'pathinarchive=images/' + this.bannerUrl;
    }

    get iconClass() {
        return "mdi " + this.icon;
    }

    get tileStyleClass() {
        if (this.isDetailTile) {
            return "tile tile__detail";
        } else {
            return "tile";
        }
    }

    get tileDescription() {
        return this.labels.findOutMore + ' about ' + this.header;
    }

}