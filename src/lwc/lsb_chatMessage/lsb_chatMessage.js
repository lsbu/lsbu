import BaseChatMessage from 'lightningsnapin/baseChatMessage';
import { track } from 'lwc';
import getArticle from '@salesforce/apex/LSB_ChatMessageController.getKnowledgeDataById';
import formFactorPropertyName from '@salesforce/client/formFactor'

import noArticleMessage from '@salesforce/label/c.LSB_ChatNoArticleMessage';
import viewArticleMessage from '@salesforce/label/c.LSB_ChatViewArticleMessage';
import readMoreMessage from '@salesforce/label/c.LSB_ChatReadMoreMessage';
import infoAboutArticlePrefix from '@salesforce/label/c.LSB_ChatInfoAboutArticlePrefix';

const DEFAULT_MESSAGE_PREFIX = 'PLAIN_TEXT';
const RICHTEXT_MESSAGE_PREFIX = 'RICH_TEXT';
const ARTICLE_MESSAGE_PREFIX = 'ARTICLE';
const FORM_FACTOR_LARGE = 'Large';
const AGENT_USER = 'agent';
const SUPPORTED_MESSAGE_PREFIX = [DEFAULT_MESSAGE_PREFIX, RICHTEXT_MESSAGE_PREFIX, ARTICLE_MESSAGE_PREFIX];

/**
 * Displays a chat message using the inherited api messageContent and is styled based on the inherited api userType and messageContent api objects passed in from BaseChatMessage.
 */
export default class ChatMessageDefaultUI extends BaseChatMessage {

    messageType = DEFAULT_MESSAGE_PREFIX;
    searchFieldApiName;
    @track content = '';
    communityUrl;
    article;
    error;

    labels = {
        noArticleMessage,
        viewArticleMessage,
        readMoreMessage,
        infoAboutArticlePrefix
    };

    connectedCallback() {
        if (!this.isAgent) {
            return;
        }
        this.retrievePrefixAndSearchField();

        const contentValue = (this.messageContent.value.split(this.messageType + ':').length === 1 && this.isPlainText) ? this.messageContent.value : this.messageContent.value.split(this.messageType + '[' + this.searchFieldApiName + ']:')[1];

        if (this.isPlainText) {
            this.content = contentValue;
        } else {
            this.findArticle(contentValue);
        }
    }

    retrievePrefixAndSearchField() {
        let prefixInfo = this.messageContent.value.split(':')[0];
        if (prefixInfo.includes(ARTICLE_MESSAGE_PREFIX)) {
            let matches = prefixInfo.match(/\[(.*?)\]/);
            if (matches) {
                let messageTypePrefix = prefixInfo.split(matches[0])[0];
                let searchField = matches[1];
                this.searchFieldApiName = searchField;
                this.messageType = messageTypePrefix;
                let messageTypePrefixPosition = SUPPORTED_MESSAGE_PREFIX.indexOf(messageTypePrefix);
                if (messageTypePrefixPosition > -1) {
                    this.messageType = SUPPORTED_MESSAGE_PREFIX[messageTypePrefixPosition];
                }
            }
        }
    }

    formatRichText(contentValue) {
        return contentValue
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '\"')
            .replace(/<a href='mailto:.*?' target='_blank'>(.*?)<\/a>/g, '$1')
            .replace(/<a href='/g, '')
            .replace(/' target='_blank'.*?<\/a>( +)/g, '$1')
            .replace(/' target='_blank'.*?<\/a>.*?<\/a>/g, '');
    }

    findArticle(contentValue) {
        let articleId = contentValue.trim();
        let fieldName = this.searchFieldApiName;
        getArticle({knowledgeId: articleId, fieldApiName: fieldName})
            .then(result => {
                this.content = this.formatRichText(result.content);
                this.article = result;
                this.communityUrl = result.communityUrl;
            })
            .catch(error => {
                this.error = error;
                console.log('Error>>', JSON.stringify(error));
            });
    }

    fallback(event) {
        event.target.onerror = null;
        event.target.style.display = 'none';
        event.target.style.height = 0;
    }

    get isAgent() {
        return this.userType === AGENT_USER;
    }

    get isPlainText() {
        return this.messageType === DEFAULT_MESSAGE_PREFIX;
    }

    get isArticleAnswer() {
        return this.messageType === ARTICLE_MESSAGE_PREFIX;
    }

    handleClick() {
        let url = this.getUrl();
        if (url) {
            let win;
            if (formFactorPropertyName === FORM_FACTOR_LARGE) {
                win = window.open(url, '_blank');
            } else {
                win = window.location.assign(url);
            }
            win.focus();
        }
    }

    getUrl() {
        if (this.communityUrl) {
            return this.communityUrl + '/s/article/' + this.article.urlName;
        }
    }
}