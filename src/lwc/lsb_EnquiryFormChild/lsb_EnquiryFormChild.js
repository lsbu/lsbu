/**
 * Created by ociszek001 on 09.02.2021.
 */

import {LightningElement, track, api, wire} from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import CASE_OBJECT from '@salesforce/schema/Case';
import CONTACT_ID from '@salesforce/schema/User.ContactId';
import CONTACT_ROLE from '@salesforce/schema/User.Contact.LSB_CON_CurrentRole__c';
import EMAIL from '@salesforce/schema/User.Contact.Email';
import USER_ID from "@salesforce/user/Id";
import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import button from '@salesforce/label/c.LSB_SubmitAndAttachFiles';
import helpText from '@salesforce/label/c.LSB_ReportIncidentHelpText';
import caseWasCreated from '@salesforce/label/c.LSB_CaseWasCreated';
import requestId from '@salesforce/label/c.LSB_RequestId';
import weGetBackToYouSoon from '@salesforce/label/c.LSB_WeGetBackToYouSoon';
import uploadFileMessage from '@salesforce/label/c.LSB_UploadFileMessage';
import myAccount from '@salesforce/label/c.LSB_MyAccount';
import enquiryRecordTypeName from '@salesforce/label/c.LSB_EnquiryRecordTypeName';

const INCIDENT_TOPIC = 'Report an Incident (Student Disciplinary)';

export default class Lsb_EnquiryForm extends NavigationMixin(LightningElement) {

    @track labels = {
        button, caseWasCreated, weGetBackToYouSoon,
        uploadFileMessage, myAccount, helpText,
        enquiryRecordTypeName, requestId};
    @track isStudentDisciplinary;
    @track isSuccessful;
    @api myRecordId;
    @api title;
    @wire(CurrentPageReference)
    pageRef;
    isSubmitting = false;
    caseNumber = '';
    @wire(getRecord, { recordId: USER_ID, fields: [CONTACT_ID, CONTACT_ROLE, EMAIL] })
    user;
    @wire(getObjectInfo, { objectApiName: CASE_OBJECT })
    caseObjectInfo;

    get contactId() {
        return getFieldValue(this.user.data, CONTACT_ID);
    }

    get contactRole(){
        return getFieldValue(this.user.data, CONTACT_ROLE);
    }
    get email() {
        return getFieldValue(this.user.data, EMAIL);
    }

    get enquiryRecordTypeId() {
        if (this.caseObjectInfo.data) {
            const recordTypeInfo = this.caseObjectInfo.data.recordTypeInfos;
            return Object.keys(recordTypeInfo).find(rti => recordTypeInfo[rti].name === this.labels.enquiryRecordTypeName);
        }
    }

    handleChange(event) {
        if (event.target.value === INCIDENT_TOPIC) {
            return this.isStudentDisciplinary = true;
        }
        return this.isStudentDisciplinary = false;
    }

    itemsChange(event) {
        const description = event.target.value;
        if(description.length > 3) {
            var objDescription = {
                descriptionValue: description
            };
            const descriptionChange = new CustomEvent('descriptionChange', {detail: objDescription});
            this.dispatchEvent(descriptionChange);
        }
    }

    handleSubmit(event) {
        this.isSubmitting = true;
        event.preventDefault();
        const fields = event.detail.fields;
        fields.ContactId = this.contactId;
        fields.Origin = this.labels.myAccount;
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    handleSuccess(event) {
        this.myRecordId = event.detail.id;
        this.caseNumber = event.detail.fields.CaseNumber.value;
        this.isSubmitting = false;
        this.isSuccessful = true;
    }

    handleClick(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.myRecordId,
                objectApiName: 'Case',
                actionName: 'view'
            }
        });
    }

    handleError(event) {
        this.isSubmitting = false;
    }

}