import {LightningElement, api} from 'lwc';

import {loadStyle} from 'lightning/platformResourceLoader';
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';

export default class LsbCommSelfAssessmentPersonalDevelopmentTable extends LightningElement {

    @api tableData

    renderedCallback() {
        const allProfiles = this.template.querySelectorAll(`[data-id='Current profile']`);
        allProfiles.forEach((el) => {
            el.classList.add('current__profile');
        });
    }

    connectedCallback() {
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }
}