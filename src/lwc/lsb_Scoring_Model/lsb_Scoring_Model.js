import { LightningElement,api } from 'lwc';

import checkIfContactIsPreApplicant from '@salesforce/apex/LSB_ScoringModel.checkIfContactIsPreApplicant';

export default class RecordIdExampleLWC extends LightningElement {
    @api recordId;
    dataReady = false;
    score;
    percentage;

    renderedCallback() {
        if (this.dataReady) {
            return;
        }

        this.dataReady = true;
        checkIfContactIsPreApplicant({contactId: this.recordId})
            .then(result => {
                this.score = result.score;
                this.percentage = result.percentage;
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }
}