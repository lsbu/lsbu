import {LightningElement, wire} from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import {loadStyle} from 'lightning/platformResourceLoader';

import headerLogo from '@salesforce/contentAssetUrl/LSBU_CommunityHeaderBgNew';

import currentUserId from '@salesforce/user/Id';

import communityStyles from '@salesforce/resourceUrl/communityStyles';

// labels
import salutation from '@salesforce/label/c.LSB_CommunityHeaderWelcomeSalulation';
import message from '@salesforce/label/c.LSB_CommunityHeaderWelcomeContentMessage';

import FIRST_NAME_FIELD from '@salesforce/schema/User.Contact.FirstName';

export default class LsbCommunityHeader extends LightningElement {

    labels = {
        salutation,
        message
    };

    @wire(getRecord, { recordId: currentUserId, fields: [FIRST_NAME_FIELD] })
    currentUser;

    get firstName() {
        return getFieldValue(this.currentUser.data, FIRST_NAME_FIELD);
    }

    get backgroundStyle() {
        return `background-image:url(${headerLogo})`;
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
    }
}