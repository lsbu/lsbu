import {LightningElement, track, api} from 'lwc';

import ok from '@salesforce/label/c.LSB_OK';
import cancel from '@salesforce/label/c.LSB_Cancel';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

export default class LsbModal extends LightningElement {

    labels = {
        ok,
        cancel
    };

    @track isModalOpen = false;
    @track modalMessage = [];
    @track modalHeader;
    @track hideFooter;
    @track hideCancel;
    @track isInnerHtml;
    submitLabel;
    cancelLabel;

    @api
    openModal({header, message, hideFooter, hideCancel, isInnerHtml, submitLabel, cancelLabel}) {
        this.modalHeader = header;
        this.hideFooter = hideFooter;
        this.hideCancel = hideCancel;
        this.isInnerHtml = isInnerHtml;
        this.modalMessage = [];
        this.submitLabel = submitLabel;
        this.cancelLabel = cancelLabel;
        if (typeof message !== "undefined") {
            if (Array.isArray(message)) {
                message.forEach(msg => {
                    this.modalMessage.push(msg);
                })
            } else {
                this.modalMessage.push(message);
            }
        }
        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
        const selectedEvent = new CustomEvent('submitmodal', {detail: false});
        this.dispatchEvent(selectedEvent);
    }

    submitDetails() {
        this.isModalOpen = false;
        const selectedEvent = new CustomEvent('submitmodal', {detail: true});
        this.dispatchEvent(selectedEvent);
    }

    renderedCallback() {
        this.addListeners();
        let htmlContent = this.template.querySelector('.html__content');
        if (htmlContent) {
            htmlContent.innerHTML = this.modalMessage[0];
        }
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.modal__main-container');
        if (modalContainer) {
            modalContainer.focus();
            modalContainer.addEventListener('keydown', (e) => {
                let closeButton = this.template.querySelector('.close__button');
                let okButton = this.template.querySelector('.ok__button');
                if (e.shiftKey && e.key === 'Tab' && e.target === closeButton) {
                    e.preventDefault();
                    okButton.focus();
                } else if (!e.shiftKey && e.key === 'Tab' && e.target === okButton) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
    }

    get submitLabelText() {
        if (this.submitLabel) {
            return this.submitLabel;
        }
        return this.labels.ok
    }

    get cancelLabelText() {
        if (this.cancelLabel) {
            return this.cancelLabel;
        }
        return this.labels.cancel
    }
}