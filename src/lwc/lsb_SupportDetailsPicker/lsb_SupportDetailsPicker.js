import {api, LightningElement, wire, track} from 'lwc';
import {ShowToastEvent} from "lightning/platformShowToastEvent";
import {getObjectInfo} from "lightning/uiObjectInfoApi";
import {refreshApex} from '@salesforce/apex';

// schema
import SUPPORT_MASTER_NAME_FIELD from '@salesforce/schema/LSB_SUM_SupportArrangementMaster__c.Name';
import SUPPORT_MASTER_CATEGORY_FIELD
    from '@salesforce/schema/LSB_SUM_SupportArrangementMaster__c.LSB_SUM_SupportArrangementCategory__c';
import DUTY_LIST_PRIORITY_LEVEL
    from '@salesforce/schema/LSB_SPD_SupportProfileDetails__c.LSB_SPD_DutyListPriorityLevel__c';

// apex
import fetchColumnData from '@salesforce/apex/LSB_SupportDetailsPickerController.fetchColumnData';
import fetchSupportMasters from '@salesforce/apex/LSB_SupportDetailsPickerController.fetchSupportMasters';
import fetchSupportDetails from '@salesforce/apex/LSB_SupportDetailsPickerController.fetchSupportDetails';
import saveDetails from '@salesforce/apex/LSB_SupportDetailsPickerController.saveDetails';

// labels
import ERROR from '@salesforce/label/c.LSB_SupportDetailsErrorMsgHeader';
import SUCCESS from '@salesforce/label/c.LSB_SupportDetailsSuccessMsgHeader';
import ERROR_MSG from '@salesforce/label/c.LSB_SupportDetailsErrorMsg';
import NEW_CREATED_MSG from '@salesforce/label/c.LSB_SupportDetailsCreatedMsg';
import CHANGES_SAVED_MSG from '@salesforce/label/c.LSB_SupportDetailsSavedMsg';
import HEADER from '@salesforce/label/c.LSB_SupportDetailsHeader';
import NEW_TABLE_HEADER from '@salesforce/label/c.LSB_SupportDetailsNewTableHeader';
import EXISTING_TABLE_HEADER from '@salesforce/label/c.LSB_SupportDetailsExistingTableHeader';
import CREATE_NEW_BTN from '@salesforce/label/c.LSB_SupportDetailsCreateNewButton';
import SAVE_BTN from '@salesforce/label/c.LSB_SupportDetailsSaveButton';
import SAVE_NEW from '@salesforce/label/c.LSB_SupportDetailsSaveNewButton';
import PREVIOUS_BTN from '@salesforce/label/c.LSB_SupportDetailsPreviousButton';
import NEXT_BTN from '@salesforce/label/c.LSB_SupportDetailsNextButton';
import NO_MASTER_RECORDS from '@salesforce/label/c.LSB_SupportDetailsNoMasterRecordsToShow';
import NO_NEW_RECORDS from '@salesforce/label/c.LSB_SupportDetailsNoNewRecordsToShow';
import NO_EXISTING_RECORDS from '@salesforce/label/c.LSB_SupportDetailsNoExistingRecordsToShow';
import NAME_FILTER from '@salesforce/label/c.LSB_SupportDetailsNameFilter';
import CATEGORY_FILTER from '@salesforce/label/c.LSB_SupportDetailsCategoryFilter';
import OPTION_IS_FILTER from '@salesforce/label/c.LSB_SupportDetailsIsFilter';
import OPTION_STARTS_WITH_FILTER from '@salesforce/label/c.LSB_SupportDetailsStartsWithFilter';
import OPTION_CONTAINS_FILTER from '@salesforce/label/c.LSB_SupportDetailsContainsFilter';
import HEADER_MHWB from '@salesforce/label/c.LSB_HeaderDetailsMHWB';
import HEADER_DDS from '@salesforce/label/c.LSB_HeaderDetailsDDS';
import HEADER_SA from '@salesforce/label/c.LSB_HeaderDetailsSA';

// constants
const SUPPORT_MASTER = 'LSB_SUM_SupportArrangementMaster__c';
const SUPPORT_PROFILE = 'LSB_SUE_SupportProfile__c';
const SUPPORT_ARRANGEMENT = 'LSB_SUT_SupportArrangement__c';
const SUPPORT_PROFILE_DETAILS = 'LSB_SPD_SupportProfileDetails__c';
const SUPPORT_ARRANGEMENT_DETAILS = 'LSB_SUD_SupportArrangementDetail__c';

const SUPPORT_MASTER_FIELD_SET_API_NAME = 'LSB_SupportMasterFieldsForPicker';

const SUPPORT_PROFLE_DETAIL_DDS_RECORD_TYPE = 'Disability and Dyslexia Support';
const SUPPORT_PROFLE_DETAIL_MHWB_RECORD_TYPE = 'MHWB Concerns';

const FIELD_TYPE_TEXT = 'text';
const FIELD_TYPE_PICKLIST = 'picklist';

const DETAILS_LIST_VIEW = 0;
const ADD_DETAIL_PAGE = 1;
const NEW_DETAILS_EDIT_PAGE = 2;

const SUPPORT_ARRANGMENT_DETAILS_MASTER_RECORD_TYPE = 'Master';
const DATA_CONFIGURATION = [
    {
        objectApiName: SUPPORT_PROFILE,
        headerLabel: HEADER_MHWB,
        detailsObjectApiName: SUPPORT_PROFILE_DETAILS,
        recordType: SUPPORT_PROFLE_DETAIL_MHWB_RECORD_TYPE,
        detailsFieldSetApiName: 'LSB_SupportProfileDetailsMHWB',
        categoryFieldApiName: 'LSB_SPD_SupportProfileDetails__c.LSB_SPD_SupportProfileCategory__c',
        readOnlyColumnsApiNames: 'Name',
        supportMasterFieldSetApiName: SUPPORT_MASTER_FIELD_SET_API_NAME,
        keyField: 'LSB_SPD_SupportMaster__c',
        recordTypeToLookup: 'SupportProfileMHWB__c',
        activationFlag: 'LSB_SPD_Active__c',
        startDate: 'LSB_SPD_StartDate__c',
        endDate: 'LSB_SPD_EndDate__c',
        dutyListPrioritiesOptions: [
            {label: 'Empty', value: ''},
            {label: 'Red', value: 'Red'},
            {label: 'Amber', value: 'Amber'},
            {label: 'Green', value: 'Green'}
        ]
    },
    {
        objectApiName: SUPPORT_PROFILE,
        headerLabel: HEADER_DDS,
        detailsObjectApiName: SUPPORT_PROFILE_DETAILS,
        recordType: SUPPORT_PROFLE_DETAIL_DDS_RECORD_TYPE,
        detailsFieldSetApiName: 'LSB_SupportProfileDetailsDDS',
        categoryFieldApiName: 'LSB_SPD_SupportProfileDetails__c.LSB_SPD_SupportProfileCategory__c',
        readOnlyColumnsApiNames: 'Name',
        supportMasterFieldSetApiName: SUPPORT_MASTER_FIELD_SET_API_NAME,
        keyField: 'LSB_SPD_SupportMaster__c',
        recordTypeToLookup: 'SupportProfileDDS__c',
        activationFlag: 'LSB_SPD_Active__c',
        startDate: 'LSB_SPD_StartDate__c',
        endDate: 'LSB_SPD_EndDate__c'
    },
    {
        objectApiName: SUPPORT_ARRANGEMENT,
        headerLabel: HEADER_SA,
        detailsObjectApiName: SUPPORT_ARRANGEMENT_DETAILS,
        recordType: SUPPORT_ARRANGMENT_DETAILS_MASTER_RECORD_TYPE,
        detailsFieldSetApiName: 'LSB_SupportArrangementDetailsFields',
        categoryFieldApiName: 'LSB_SUD_SupportArrangementDetail__c.LSB_SUD_SupportCategory__c',
        supportMasterFieldSetApiName: SUPPORT_MASTER_FIELD_SET_API_NAME,
        keyField: 'LSB_SUD_SupportArrangementMaster__c',
        readOnlyColumnsApiNames: 'Name',
        activationFlag: 'LSB_SUD_Active__c',
        startDate: 'LSB_SUD_StartDate__c',
        endDate: 'LSB_SUD_EndDate__c'
    }
];

export default class LsbSupportDetailsPicker extends LightningElement {

    // data model attributes
    @api objectApiName;
    @api recordId;
    @api recordType;
    @track supportProfileDetailsInfo;

    dataConfiguration;

    currentPage = DETAILS_LIST_VIEW;

    label = {
        ERROR,
        SUCCESS,
        ERROR_MSG,
        NEW_CREATED_MSG,
        CHANGES_SAVED_MSG,
        HEADER,
        NEW_TABLE_HEADER,
        EXISTING_TABLE_HEADER,
        CREATE_NEW_BTN,
        SAVE_BTN,
        SAVE_NEW,
        PREVIOUS_BTN,
        NEXT_BTN,
        NO_MASTER_RECORDS,
        NO_NEW_RECORDS,
        NO_EXISTING_RECORDS,
        NAME_FILTER,
        CATEGORY_FILTER,
        OPTION_IS_FILTER,
        OPTION_STARTS_WITH_FILTER,
        OPTION_CONTAINS_FILTER
    };

    // details attributes
    existingSupportDetailsColumns;
    @track existingSupportDetailsRows = [];
    @track newSupportDetailsRows = [];
    newSupportDetailsColumns;

    // support masters attributes
    supportMastersColumns;
    allSupportMasters = [];
    @track supportMastersRows = [];
    @track noSupportMasterAvailable;
    @track selectedSupportMastersRows = [];
    @track preselectedMasterRows = [];

    // filters attributes
    filtersConfiguration = [
        {
            fieldApiName: SUPPORT_MASTER_NAME_FIELD.fieldApiName,
            fieldLabel: this.label.NAME_FILTER,
            inputType: FIELD_TYPE_TEXT,
            filterOptions: [
                {label: this.label.OPTION_IS_FILTER, value: this.label.OPTION_IS_FILTER},
                {label: this.label.OPTION_STARTS_WITH_FILTER, value: this.label.OPTION_STARTS_WITH_FILTER},
                {label: this.label.OPTION_CONTAINS_FILTER, value: this.label.OPTION_CONTAINS_FILTER}
            ],
            fieldValues: []
        },
        {
            fieldApiName: SUPPORT_MASTER_CATEGORY_FIELD.fieldApiName,
            fieldLabel: this.label.CATEGORY_FILTER,
            inputType: FIELD_TYPE_PICKLIST,
            filterOptions: [{label: this.label.OPTION_IS_FILTER, value: this.label.OPTION_IS_FILTER}],
            fieldValues: []
        }
    ];

    filterTypes = [
        {label: this.label.NAME_FILTER, value: SUPPORT_MASTER_NAME_FIELD.fieldApiName},
        {label: this.label.CATEGORY_FILTER, value: SUPPORT_MASTER_CATEGORY_FIELD.fieldApiName}
    ];

    @track newFilter;
    @track newFilterTypeInput = false;
    @track newFilterTypeCombobox = false;

    @track activeFilters = [];

    connectedCallback() {
        if (this.objectApiName === SUPPORT_PROFILE) {
            this.dataConfiguration = DATA_CONFIGURATION.filter(config => config.objectApiName === this.objectApiName).find(recordType => recordType.recordType === this.recordType);
        } else {
            this.dataConfiguration = DATA_CONFIGURATION.find(config => config.objectApiName === this.objectApiName);
        }
        this.loadMasterColumns();
        this.loadDetailColumns();
    }

    loadMasterColumns() {
        fetchColumnData({objectApiName: SUPPORT_MASTER, fieldSetName: SUPPORT_MASTER_FIELD_SET_API_NAME})
            .then(result => {
                let fetchedColumnData = [];
                result.forEach((col) => {
                    let column = {...col};
                    fetchedColumnData.push(column);
                });
                this.supportMastersColumns = fetchedColumnData;
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.ERROR,
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }

    loadDetailColumns() {
        fetchColumnData({
            objectApiName: this.dataConfiguration.detailsObjectApiName,
            fieldSetName: this.dataConfiguration.detailsFieldSetApiName
        })
            .then(result => {
                let fetchedColumnDataNew = [];
                let fetchedColumnDataExisting = [];
                result.forEach((col) => {
                    let columnNew = {...col};
                    let columnExisting = {...col};
                    if (!this.dataConfiguration.readOnlyColumnsApiNames.includes(col.fieldName)) {
                        columnNew.editable = true;
                        columnExisting.editable = true;
                    }
                    if (col.type === FIELD_TYPE_PICKLIST
                        && col.fieldName === DUTY_LIST_PRIORITY_LEVEL.fieldApiName) {
                        columnExisting.typeAttributes = {
                            recordId: {
                                fieldName: 'Id'
                            },
                            fieldName: 'LSB_SPD_DutyListPriorityLevel__c',
                            fieldLabel: 'Duty List Priority Level',
                            options: this.dataConfiguration.dutyListPrioritiesOptions
                        };
                        columnNew.typeAttributes = {
                            recordId: {
                                fieldName: this.dataConfiguration.keyField
                            },
                            fieldName: 'LSB_SPD_DutyListPriorityLevel__c',
                            fieldLabel: 'Duty List Priority Level',
                            options: this.dataConfiguration.dutyListPrioritiesOptions
                        };
                    }
                    fetchedColumnDataNew.push(columnNew);
                    fetchedColumnDataExisting.push(columnExisting);
                });
                this.newSupportDetailsColumns = fetchedColumnDataNew;
                this.existingSupportDetailsColumns = fetchedColumnDataExisting;
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.ERROR,
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }

    @wire(getObjectInfo, {objectApiName: SUPPORT_PROFILE_DETAILS})
    supportProfileDetailsInfo;

    supportProfileDetailsRecordTypeId(recordTypeName) {
        const recordTypeIds = this.supportProfileDetailsInfo.data.recordTypeInfos;
        return Object.keys(recordTypeIds).find(recordTypeId => recordTypeIds[recordTypeId].name === recordTypeName);
    }

    supportDetails;

    @wire(fetchSupportDetails, {
        objectApiName: '$dataConfiguration.detailsObjectApiName',
        fieldSetApiName: '$dataConfiguration.detailsFieldSetApiName',
        parentObjectApiName: '$objectApiName',
        supportProfileDetailRecordType: '$dataConfiguration.recordType',
        parentId: '$recordId'
    })
    supportDetailsWired(response) {
        this.supportDetails = response;
        if (response.data) {
            let rows = [];
            response.data.forEach(row => {
                rows.push({...row});
            });
            this.existingSupportDetailsRows = rows;
        } else if (response.error) {
            this.error = response.error;
        }
    }

    supportMasters;

    @wire(fetchSupportMasters, {
        detailsObjectApiName: '$dataConfiguration.detailsObjectApiName',
        supportId: '$recordId',
        masterFieldSetApiName: '$dataConfiguration.supportMasterFieldSetApiName',
        recordType: '$dataConfiguration.recordType'
    })
    supportMastersWired(response) {
        this.supportMasters = response;
        const dataCategories = new Set();
        let categoryOptions = [];
        if (response.data) {
            let rows = [];
            response.data.forEach(row => {
                rows.push({...row});
                dataCategories.add(row.LSB_SUM_SupportArrangementCategory__c);
            });
            this.supportMastersRows = rows;
            this.allSupportMasters = rows;
            this.noSupportMasterAvailable = rows.length == 0;

            // category filter configuration
            dataCategories.forEach(category => {
                categoryOptions.push({
                    label: category,
                    value: category
                });
            });
            let categoryFilterConfig = this.filtersConfiguration.find(config => config.fieldApiName === SUPPORT_MASTER_CATEGORY_FIELD.fieldApiName);
            let categoryFilterConfigIndex = this.filtersConfiguration.indexOf(categoryFilterConfig);
            categoryFilterConfig.fieldValues = categoryOptions;
            this.filtersConfiguration[categoryFilterConfigIndex] = categoryFilterConfig;

        } else if (response.error) {
            this.error = response.error;
        }
    }

    // navigation
    goToDetailsListPage() {
        this.preselectedMasterRows = [];
        this.newSupportDetailsRows = [];
        this.currentPage = DETAILS_LIST_VIEW;
    }

    goToAddNewDetailPage() {
        let selection = [];
        this.selectedSupportMastersRows.forEach(supportMaster => {
            selection.push(supportMaster['Id']);
        });
        this.currentPage = ADD_DETAIL_PAGE;
        this.preselectedMasterRows = selection;
    }

    goToEditNewDetailsPage() {
        this.currentPage = NEW_DETAILS_EDIT_PAGE;
    }

    goToDetailsListView() {
        this.currentPage = DETAILS_LIST_VIEW;
    }

    closeModal() {
        const closePopUp = new CustomEvent('close');
        this.dispatchEvent(closePopUp);
    }

    get addNewDetail() {
        return (this.currentPage === ADD_DETAIL_PAGE);
    }

    get detailsListView() {
        return (this.currentPage === DETAILS_LIST_VIEW);
    }

    get editNewDetailsPage() {
        return (this.currentPage === NEW_DETAILS_EDIT_PAGE);
    }

    // filters for Support Master records
    createNewFilter() {
        this.newFilter = {};
    }

    handleFilterTypeSelect(event) {
        const filterType = event.detail.value;
        let filterConfiguration = this.filtersConfiguration.find(config => config.fieldApiName === filterType);
        this.newFilterTypeCombobox = filterConfiguration.inputType === FIELD_TYPE_PICKLIST;
        this.newFilterTypeInput = !this.newFilterTypeCombobox;
        this.newFilter.type = filterConfiguration.inputType;
        this.newFilter.options = filterConfiguration.fieldValues;
        this.newFilter.field = filterConfiguration.fieldApiName;
        this.newFilter.label = filterConfiguration.fieldLabel;
        this.newFilter.filterOptions = filterConfiguration.filterOptions;
        this.newFilter.oneFilterOption = this.newFilter.filterOptions.length == 1;
        this.newFilter.selectedOption = this.newFilter.filterOptions[0].value;
    }

    handleFilterValueChange(event) {
        this.newFilter.value = event.detail.value;
    }

    handleFilterOptionChange(event) {
        this.newFilter.selectedOption = event.detail.value;
    }

    addFilter() {
        this.activeFilters.push({
            label: this.newFilter.label + ' ' + this.newFilter.selectedOption + ' ' + this.newFilter.value,
            field: this.newFilter.field,
            option: this.newFilter.selectedOption,
            value: this.newFilter.value
        });

        this.newFilter = null;
        this.newFilterTypeInput = false;
        this.newFilterTypeCombobox = false;
        this.filterMasterRecords();
    }

    removeFilter(event) {
        const selectedLabel = event.target.label;
        let removedFilter = this.activeFilters.find(filter => filter.label === selectedLabel);
        let removedFilterIndex = this.activeFilters.indexOf(removedFilter);
        this.activeFilters.splice(removedFilterIndex, 1);

        this.filterMasterRecords();
    }

    filterMasterRecords() {
        let supportMastersFilteredList = [];

        this.allSupportMasters.forEach(record => {
            if (this.activeFiltersRecordValidation(record)) {
                supportMastersFilteredList.push(record);
            }
        });

        this.supportMastersRows = supportMastersFilteredList;
        this.noSupportMasterAvailable = supportMastersFilteredList.length == 0;
    }

    activeFiltersRecordValidation(supportMasterRecord) {
        let result = true;

        this.activeFilters.forEach(filter => {
            let fieldValue = supportMasterRecord[filter.field].toLowerCase();
            let filterValue = filter.value.toLowerCase();

            if ((filter.option === this.label.OPTION_IS_FILTER && fieldValue !== filterValue) ||
                (filter.option === this.label.OPTION_STARTS_WITH_FILTER && !fieldValue.startsWith(filterValue)) ||
                (filter.option === this.label.OPTION_CONTAINS_FILTER && !fieldValue.includes(filterValue))) {
                result = false;
            }
        });

        return result;
    }

    // Support Master records selection
    handleMasterRowSelection(event) {
        this.selectedSupportMastersRows = event.detail.selectedRows;
    }

    handleExistingCellChange(event) {
        let today = new Date();
        let drafts = event.detail.draftValues;
        let draftsArray = Object.getOwnPropertyNames(drafts[0]);
        let fieldName = draftsArray[0];
        let fieldValue = drafts[0][fieldName];
        let idValue = drafts[0].Id;
        this.existingSupportDetailsRows.find(item => item.Id === idValue)[fieldName] = fieldValue;
        if (fieldName === this.dataConfiguration.activationFlag) {
            if (fieldValue) {
                this.existingSupportDetailsRows.find(item => item.Id === idValue)[this.dataConfiguration.endDate] = undefined;
            } else {
                this.existingSupportDetailsRows.find(item => item.Id === idValue)[this.dataConfiguration.endDate] = today;
            }
        }
    }

    handleNewCellChange(event) {
        let today = new Date();
        let drafts = event.detail.draftValues;
        let draftsArray = Object.getOwnPropertyNames(drafts[0]);
        let fieldName = draftsArray[0];
        let fieldValue = drafts[0][fieldName];
        if (this.dataConfiguration.detailsObjectApiName === SUPPORT_PROFILE_DETAILS) {
            let idValue = drafts[0].LSB_SPD_SupportMaster__c;
            this.newSupportDetailsRows.find(item => item.LSB_SPD_SupportMaster__c === idValue)[fieldName] = fieldValue;
            if (fieldName === this.dataConfiguration.activationFlag) {
                if (fieldValue) {
                    this.newSupportDetailsRows.find(item => item.LSB_SPD_SupportMaster__c === idValue)[this.dataConfiguration.endDate] = undefined;
                } else {
                    this.newSupportDetailsRows.find(item => item.LSB_SPD_SupportMaster__c === idValue)[this.dataConfiguration.endDate] = today;
                }
            }
        } else if (this.dataConfiguration.detailsObjectApiName === SUPPORT_ARRANGEMENT_DETAILS) {
            let idValue = drafts[0].LSB_SUD_SupportArrangementMaster__c;
            this.newSupportDetailsRows.find(item => item.LSB_SUD_SupportArrangementMaster__c === idValue)[fieldName] = fieldValue;
            if (fieldName === this.dataConfiguration.activationFlag) {
                if (fieldValue) {
                    this.newSupportDetailsRows.find(item => item.LSB_SUD_SupportArrangementMaster__c === idValue)[this.dataConfiguration.endDate] = undefined;
                } else {
                    this.newSupportDetailsRows.find(item => item.LSB_SUD_SupportArrangementMaster__c === idValue)[this.dataConfiguration.endDate] = today;
                }
            }
        }
    }

    get newItemsData() {
        let today = new Date();
        let newItems = [];
        this.selectedSupportMastersRows.forEach(selectedParent => {
            if (this.dataConfiguration.detailsObjectApiName === SUPPORT_PROFILE_DETAILS) {
                let recordTypeName = selectedParent.LSB_SUM_ObjectUsedInRecordType__c;
                let lookupField = this.dataConfiguration.recordTypeToLookup;

                let detailRecord = {
                    sobjectType: this.dataConfiguration.detailsObjectApiName,
                    SupportProfileMaster__c: this.recordId,
                    LSB_SPD_SupportMaster__c: selectedParent.Id,
                    LSB_SPD_SupportMaster__r: selectedParent,
                    LSB_SPD_SupportProfileCategory__c: selectedParent.LSB_SUM_SupportArrangementCategory__c,
                    RecordTypeId: this.supportProfileDetailsRecordTypeId(recordTypeName),
                    LSB_SPD_Active__c: true,
                    LSB_SPD_StartDate__c: today
                };

                detailRecord[lookupField] = this.recordId;
                newItems.push(detailRecord);
            } else if (this.dataConfiguration.detailsObjectApiName === SUPPORT_ARRANGEMENT_DETAILS) {
                newItems.push(
                    {
                        sobjectType: this.dataConfiguration.detailsObjectApiName,
                        LSB_SUD_SupportArrangementMaster__c: selectedParent.Id,
                        LSB_SUD_SupportArrangementMaster__r: selectedParent,
                        LSB_SUD_SupportCategory__c: selectedParent.LSB_SUM_SupportArrangementCategory__c,
                        LSB_SUD_Active__c: true,
                        LSB_SUD_StartDate__c: today,
                        LSB_SUD_SupportArrangementPlan__c: this.recordId
                    });
            }
        });
        this.newSupportDetailsRows = newItems;
        return newItems;
    }

    handlePicklistChange(event) {
        if (event.detail.id && event.detail.fieldName) {
            this.existingSupportDetailsRows.find(x => x.Id === event.detail.id)[event.detail.fieldName] = event.detail.value;
        }
    }

    handleNewPicklistChange(event) {
        if (event.detail.id && event.detail.fieldName) {
            this.newSupportDetailsRows.find(x => x[this.dataConfiguration.keyField] === event.detail.id)[event.detail.fieldName] = event.detail.value;
        }
    }

    // save actions
    saveNewDetails() {
        saveDetails({supportDetails: this.newSupportDetailsRows, parentId: this.recordId})
            .then(result => {
                this.dispatchEvent(new ShowToastEvent({
                    title: this.label.SUCCESS,
                    message: this.label.NEW_CREATED_MSG,
                    variant: 'success'
                }),);
                refreshApex(this.supportMasters);
                refreshApex(this.supportDetails);
                this.goToDetailsListPage();
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    title: this.label.ERROR,
                    message: this.label.ERROR_MSG + ': ' + error.message,
                    variant: 'error'
                }),);
                console.log(error);
                console.log('error: ' + error.message);
            })
    }

    draftValues = [];

    handleSave() {
        saveDetails({supportDetails: this.existingSupportDetailsRows, parentId: this.recordId})
            .then(result => {
                this.dispatchEvent(new ShowToastEvent({
                    title: this.label.SUCCESS,
                    message: this.label.CHANGES_SAVED_MSG,
                    variant: 'success'
                }),);
                this.draftValues = [];
                refreshApex(this.supportMasters);
                refreshApex(this.supportDetails);
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    title: this.label.ERROR,
                    message: this.label.ERROR_MSG + ': ' + error.message,
                    variant: 'error'
                }),);
                console.log(error);
                console.log('error: ' + error.message);
            })
    }
}