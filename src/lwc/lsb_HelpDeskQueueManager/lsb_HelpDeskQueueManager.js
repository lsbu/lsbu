import {LightningElement, track} from 'lwc';

// labels
import componentHeader from '@salesforce/label/c.LSB_HelpDeskQueueManagerHeader';

export default class LsbHelpDeskQueueManager extends LightningElement {

    @track labels = {
        componentHeader
    }

}