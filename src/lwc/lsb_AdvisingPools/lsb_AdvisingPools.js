import {LightningElement, track, api} from 'lwc';

import fetchAdvisingPools from '@salesforce/apex/LSB_AdvisingPoolsController.fetchAdvisingPools';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

//labels
import findOutMore from '@salesforce/label/c.LSB_FindOutMore';

export default class LsbAdvisingPools extends LightningElement {

    @api header;
    @api description;
    @track advisingPools = [];
    componentReady = false;
    defaultIcon = 'mdi mdi-account-multiple';

    labels = {
        findOutMore
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.componentReady) {
            return;
        } else {
            fetchAdvisingPools()
                .then(result => {
                    result.forEach((act) => {
                        this.advisingPools.push({
                            'Id': act.Id,
                            'LSB_Url__c': act.LSB_Url__c,
                            'Name': act.Name,
                            'sfal__Description__c': act.sfal__Description__c,
                            'LSB_Icon__c': act.LSB_Icon__c,
                            'iconClass': act.LSB_Icon__c ? "mdi " + act.LSB_Icon__c : this.defaultIcon
                        });
                    });
                    this.componentReady = true;
                })
                .catch(error => {
                    console.log('Error: ' + error.body.message);
                });
        }
    }
}