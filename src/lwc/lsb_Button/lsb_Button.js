import {LightningElement, api} from 'lwc';

export default class LsbButton extends LightningElement {

    @api variant;
    @api label;
    @api redirectUrl;

    renderedCallback() {
        this.setupStyles();
    }

    setupStyles() {
        const style = document.createElement('style');
        style.innerText = 'c-lsb_-button .slds-button_outline-brand { width: 100%;\n' +
            '    margin-top: 5px;\n' +
            '    margin-bottom: 15px; padding: 7px 7px;\n' +
            '    font-size: 13px;\n' +
            '    font-family: Arial, Helvetica, sans-serif;\n' +
            '    border-radius: 4px;\n' +
            '    border-color: #D7425E;\n' +
            '    color: #D7425E;\n' +
            '    font-weight: 700;} \n' +
            'c-lsb_-button .slds-button_brand { width: 100%;\n' +
            '    margin-top: 5px;\n' +
            '    margin-bottom: 15px; padding: 7px 12px;\n' +
            '    font-size: 13px;\n' +
            '    font-family: Arial, Helvetica, sans-serif;\n' +
            '    font-weight: 700;} \n';
        this.template.querySelector('lightning-button').appendChild(style);
    }

    handleClick() {
        if (this.redirectUrl) {
            window.location.href = this.redirectUrl;
        }
    }
}