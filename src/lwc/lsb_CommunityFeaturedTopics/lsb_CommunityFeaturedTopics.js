import { LightningElement, api, track } from 'lwc'

// labels
import browseArticlesLink from '@salesforce/label/c.LSB_CommunityFeaturedTopicsBrowseArticlesLink';

// apex
import fetchTopics from '@salesforce/apex/LSB_CommunityFeaturedTopicsController.fetchTopics';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

const DEFAULT_SEPARATOR = ';';

export default class LWC_CommunityFeaturedTopics extends LightningElement {

    @api topicNamesString;
    @api separator;
    @track topics = [];
    @track dataReady = false;

    labels = {
        browseArticlesLink
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.dataReady) {
            return;
        }

        this.dataReady = true;
        fetchTopics()
            .then(result => {
                this.filterTopics(result);
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

    filterTopics(result) {
        let topicNames = this.getTopicNamesToDisplay();
        if (topicNames) {
            topicNames.forEach((topicName) => {
                result.forEach((topic) => {
                    if (topicName === topic.name) {
                        topic.title = topic.name.replaceAll("amp;", "");
                        topic.url = '/s/topic/' + topic.id + '/' + topic.title;
                        topic.linkAriaLabel = 'Browse ' + topic.title + ' articles';
                        this.topics.push(topic);
                    }
                });
            });
        }
    }

    getTopicNamesToDisplay() {
        let separator = this.separator.trim();
        if (!separator) {
            separator = DEFAULT_SEPARATOR;
        }
        return this.topicNamesString.split(separator);
    }

    redirectToArticles(event) {
        // Stop the event's default behavior.
        // Stop the event from bubbling up in the DOM.
        event.preventDefault();
        event.stopPropagation();

        window.location.href = event.currentTarget.dataset.url;
    }

    redirectToArticlesFromLink(event) {
        // Stop the event's default behavior.
        // Stop the event from bubbling up in the DOM.
        event.preventDefault();
        event.stopPropagation();

        window.location.href = event.target.href;
    }
}