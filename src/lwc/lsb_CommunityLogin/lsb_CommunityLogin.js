import {LightningElement} from 'lwc';

import usernameFieldLabel from '@salesforce/label/c.LSB_CommLoginUsernameField';
import passwordFieldLabel from '@salesforce/label/c.LSB_SelfRegPasswordField';
import loginButtonLabel from '@salesforce/label/c.LSB_CommLoginButtonLabel';
import usernameMissingMessage from '@salesforce/label/c.LSB_CommLoginUsernameMissing';
import passwordMissingMessage from '@salesforce/label/c.LSB_CommLoginPasswordMissing';

import loginToCommunity from '@salesforce/apex/LSB_CommunityLoginController.login';

export default class LsbCommunityLogin extends LightningElement {

    labels = {
        usernameFieldLabel,
        passwordFieldLabel,
        loginButtonLabel,
        usernameMissingMessage,
        passwordMissingMessage
    };
    errorMessage = '';
    username = '';
    password = '';
    startUrl = '';

    renderedCallback() {
        this.setupLandingURL();
        this.setupStyles();
    }

    setupLandingURL() {
        let urlString = window.location.href;
        let landingUrl = urlString.substring(0, urlString.indexOf('Page1'));
        this.landingUrl = landingUrl;
    }

    setupStyles() {
        const style = document.createElement('style');
        style.innerText = 'c-lsb_-community-login .slds-form-element input { padding: 7px 12px; font-size: 13px; } \n ' +
            'c-lsb_-community-login .slds-has-error input { border-color: #D7425E !important;} \n ' +
            'c-lsb_-community-login input:focus { border-color: #D7425E !important; } \n ' +
            'c-lsb_-community-login .slds-form-element__help { color: #D7425E !important; margin-top: 5px; font-size: 13px}' +
            //'.siteforceSldsOneColLayout { border: 1px solid #d4d4d4 !important; }\n'
            '.sfdc_input_container[c-lsb_communitylogin_lsb_communitylogin]{ padding: 9px; }\n' +
            //+ 'c-lsb_-community-login .slds-button_icon-bare { display: none }\n'*/
            '' + 'c-lsb_-community-login .slds-button_brand { width: 100%;\n' +
            '    margin-top: 5px;\n' +
            '    margin-bottom: 15px; padding: 7px 7px;\n' +
            '    font-size: 13px;\n' +
            '    font-family: Arial, Helvetica, sans-serif;\n' +
            '    border-radius: 4px;\n' +
            '    border-color: #D7425E;\n' +
            '    color: white; \n' +
            '    font-weight: 700; \n' +
            '   transition: all .2s;} \n   '
            + 'c-lsb_-community-login .slds-button_brand:hover { background-color: #B62E5F !important; border-color: #B62E5F; color:white}';
        this.template.querySelector('lightning-input').appendChild(style);
    }


    handleLogin() {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid) {
            loginToCommunity({
                username: this.username,
                password: this.password,
                startURL: this.landingUrl
            })
                .then(result => {
                    if (result) {
                        if (result.resultUrl) {
                            let loginResult = result.resultUrl;
                            window.location.href = loginResult;
                        } else if (result.loginMessage) {
                            this.errorMessage = result.loginMessage;
                            this.password = '';
                        }
                    }
                })
                .catch(error => {
                    this.errorMessage = error.body.message;
                    this.password = '';
                });
        }
    }

    handleChange(event) {
        this[event.target.name] = event.target.value;
    }

    handleLoginOnKeyPress(event) {
        if (event.which === 13) {
            this.handleLogin();
        }
    }

}