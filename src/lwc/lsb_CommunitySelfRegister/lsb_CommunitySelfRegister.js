import { LightningElement, api, track, wire } from 'lwc';

// apex
import fetchInputFields from '@salesforce/apex/LSB_CommunitySelfRegisterController.fetchFields'
import validateUniqueId from '@salesforce/apex/LSB_CommunitySelfRegisterController.validateEmailAddress';
import createCommunityUser from '@salesforce/apex/LSB_UserHelper.createCommunityUser';

// labels
import signUpButtonLabel from '@salesforce/label/c.LSB_SelfRegSignUpBtn';
import signUpErrorMessage from '@salesforce/label/c.LSB_SelfRegNoContactMessage';
import inputFieldMissingMessage from '@salesforce/label/c.LSB_SelfRegInputFieldMissingMessage';

// schema
import USER_OBJECT from '@salesforce/schema/Account';

export default class Lsb_CommunitySelfRegisterController extends LightningElement {
    @track dataReady = false; // it is used to indicate if all data were fetched.

    @track inputFields = [];
    @track inputValues = {};

    @track password = '';

    @track errorMessage = '';
    @track showError = false;

    @track labels = {
        signUpButtonLabel,
        signUpErrorMessage,
        inputFieldMissingMessage
    };

    setup() {
        this.dataReady = true;
        fetchInputFields()
            .then(result => {
                let fetchedInputFields = [];
                result.forEach((field) => {
                    var missingMessage = this.labels.inputFieldMissingMessage.replace('{0}', field.fieldLabel);
                    console.log('fieldLabel: ' + field.fieldLabel + ' message ' + missingMessage);
                    //field.missingMessage = missingMessage;
                    this.inputValues[field.fieldAPIName] = null;
                });
                this.inputFields = result;
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
        this.inputValues = { apiName : USER_OBJECT.objectApiName };
    }

    renderedCallback() {
        if (!this.dataReady) {
            this.setup();
        }

        const style = document.createElement('style');
        style.innerText = 'c-lsb_-community-self-register .slds-input { border: 1px solid #d4d4d4 !important; padding: 7px 12px; font-size: 13px; }\n'
            + '.sfdc_input_container[c-lsb_CommunitySelfRegister_lsb_CommunitySelfRegister]{ padding: 9px; }\n'
            + 'c-lsb_-community-self-register .slds-button_icon-bare { display: none }\n'
            + 'c-lsb_-community-self-register .slds-button_brand { width: 100%;\n' +
            '    margin-top: 5px;\n' +
            '    margin-bottom: 15px; padding: 7px 12px;\n' +
            '    font-size: 13px;\n' +
            '    font-family: Arial, Helvetica, sans-serif;\n' +
            '    font-weight: 700;} \n' + 
            'c-lsb_-community-self-register .slds-has-error input { border-color: #D7425E !important;} \n ' +
            'c-lsb_-community-self-register input:focus { border-color: #D7425E !important; } \n ' +
            'c-lsb_-community-self-register .slds-form-element__help { color: #D7425E !important; margin-top: 5px;}';
        this.template.querySelector('lightning-input').appendChild(style);
    }

    handleInputChange(event){
        this.inputValues[event.target.name] = event.target.value;
    }

    validateUniqueId(event) {
        let inputValue = event.target.value;
        let inputEmail = this.template.querySelector(".uniqueId");

        if (inputValue == null || inputValue == undefined || inputValue == '') {
            return;
        }

        validateUniqueId({ emailAddress: inputValue })
            .then(result => {
                this.errorMessage = result;
            }).catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

    signUpToCommunity(event) {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid) {
            if (this.errorMessage != '') {
                this.showError = true;
            } else {
                this.showError = false;
                createCommunityUser({
                    newUser : this.inputValues,
                    password : this.password
                }).then(result => {
                    if (result != null) {
                        window.location.href = result;
                    } else {
                        this.errorMessage = this.labels.signUpErrorMessage;
                        this.showError = true;
                    }
                }).catch(error => {
                    console.log('Error: ' + error.body.message);
                });
            }
        }
    }

    handleLoginOnKeyPress(event) {
        if (event.which === 13) {
            this.signUpToCommunity(event);
        }
    }
}