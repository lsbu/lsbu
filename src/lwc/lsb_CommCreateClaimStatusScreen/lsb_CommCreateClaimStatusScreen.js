import {LightningElement, api} from 'lwc';
import {loadStyle} from "lightning/platformResourceLoader";
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';

export default class LsbCommCreateClaimStatusScreen extends LightningElement {

    @api firstParagraph;
    @api secondParagraph;
    @api thirdParagraph;
    @api caseId;
    @api caseNumber;
    @api iconClassName;

    connectedCallback() {
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    get iconClass() {
        return `mdi ${this.iconClassName}`;
    }
}