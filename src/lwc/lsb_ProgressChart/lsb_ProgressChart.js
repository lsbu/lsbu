import {LightningElement, api} from 'lwc';

import chartjs from '@salesforce/resourceUrl/chartJsLib';
import {loadScript} from 'lightning/platformResourceLoader';
import {ShowToastEvent} from "lightning/platformShowToastEvent";

export default class LsbProgressChart extends LightningElement {

    @api chartData;

    @api get chartConfig() {
        return this.chartData;
    }

    set chartConfig(value) {
        this.chartData = value;
        this.setupChart();
    }


    setupChart() {
        Promise.all([loadScript(this, chartjs)])
            .then(() => {
                let chartData = JSON.parse(JSON.stringify(this.chartData));
                if (this.chart instanceof Chart) {
                    this.chart.data = chartData.data;
                    this.chart.update();
                } else {
                    const ctx = this.template.querySelector('canvas.radarChart').getContext('2d');
                    this.chart = new window.Chart(ctx, JSON.parse(JSON.stringify(chartData)));
                }
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading Chart',
                        message: error.message,
                        variant: 'error',
                    })
                );
            });
    }
}