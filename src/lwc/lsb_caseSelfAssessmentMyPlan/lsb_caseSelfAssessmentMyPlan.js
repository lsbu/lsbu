import {LightningElement, api, wire} from 'lwc';

import getMyPlanData from '@salesforce/apex/LSB_CaseSelfAssessmentController.getSelfAssessmentPlan';

import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import CONTACT_ID from '@salesforce/schema/Case.ContactId';

import noDataMessage from '@salesforce/label/c.LSB_CaseSelfAssessmentNoChartData';
import genericError from '@salesforce/label/c.LSB_GenericError';

const fields = [CONTACT_ID];

export default class LsbCaseSelfAssessmentMyPlan extends LightningElement {

    labels = {
        noDataMessage,
        genericError
    }

    @api recordId;

    responsesInSections;
    showResponses = false;
    isReady = false;

    @wire(getRecord, { recordId: '$recordId', fields })
    case;

    @wire(getMyPlanData, { contactId: '$caseContactId' })
    wiredMyPlanData({ error, data }) {
        if (data) {
            this.setTabData(data);
            this.error = undefined;
            this.isReady = true;
        } else if (error) {
            console.log('error>>',error)
            this.error = error;
            this.sections = undefined;
            this.isReady = true;
        } else {
            this.isReady = true;
        }
    }

    get caseContactId() {
        return getFieldValue(this.case.data, CONTACT_ID);
    }

    setTabData(value) {
        let responsesInSections = [];
        if (value) {
            value.forEach(item => {
                responsesInSections.push({
                    id: item.id,
                    sectionName: item.title,
                    responses: item.sectionItems
                })
            });
        }
        this.responsesInSections = responsesInSections;
    }
}