import {LightningElement, api, wire, track} from 'lwc';
import fetchPicklistValues from '@salesforce/apex/LSB_Utils.fetchPicklistEntryWrappers';
import fetchStatusDefinitions from '@salesforce/apex/LSB_Utils.fetchStatusDefinitions';

import yourStatus from '@salesforce/label/c.LSB_YourStatus';
import checkStatusDefinitions from '@salesforce/label/c.LSB_CheckStatusDefinitions';
import inputRequired from '@salesforce/label/c.LSB_InputRequired';
import responded from '@salesforce/label/c.LSB_CaseStatusResponded';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import CASE_OBJECT from '@salesforce/schema/Case';

const REQUEST_PENDING = 'Request Pending';
const REQUEST_BEING_REVIEWED = 'Request Being Reviewed';
const REQUEST_SUPPORTED = 'Request Supported';
const REQUEST_REJECTED = 'Request Rejected';
const REQUEST_APPROVED_AFTER_REVIEW = 'Request Approved After Review';
const REQUEST_REJECTED_AFTER_REVIEW = 'Request Rejected After Review';
const REQUEST_WITHDRAWN_BY_STUDENT = 'Request Withdrawn by Student';
const BEING_REVIEWED = 'Being Reviewed';
const CLOSED = 'Closed';

export default class Lsb_ProgressIndicator extends LightningElement {

    @api objectApiName;
    @api fieldApiName;
    @api recordTypeDeveloperName;
    @api hasWarning;
    @api currentValue;
    @api showLabels = false;
    @api showStatusDefinitions;
    statusDefinitions;

    @track options;
    componentReady = false;

    labels = {
        checkStatusDefinitions,
        yourStatus,
        inputRequired,
        responded
    };

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
        this.generateProgressBarOptions();
    }

    generateProgressBarOptions() {
        fetchPicklistValues({
            objectApiName: this.objectApiName,
            recordTypeDeveloperName: this.recordTypeDeveloperName,
            picklistApiName: this.fieldApiName
        }).then(result => {
            let options = [];
            let currentPassed = false;
            let currentValue = this.currentValue;
            let currentValue2;
            //logic for EC Claim status grouping
            if (currentValue === REQUEST_PENDING
                || currentValue === REQUEST_BEING_REVIEWED) {
                currentValue2 = currentValue;
                currentValue = BEING_REVIEWED
            } else if (currentValue === REQUEST_SUPPORTED
                || currentValue === REQUEST_REJECTED
                || currentValue === REQUEST_APPROVED_AFTER_REVIEW
                || currentValue === REQUEST_REJECTED_AFTER_REVIEW
                || currentValue === REQUEST_WITHDRAWN_BY_STUDENT) {
                currentValue2 = currentValue;
                currentValue = CLOSED;
            }
            result.forEach(value => {
                if (value.value === currentValue) {
                    currentPassed = true;
                    if (currentValue2) {
                        options.push({
                            label: currentValue2,
                            value: value.value,
                            markerStyleClass: "slds-button slds-button_icon slds-progress__marker slds-progress__marker_icon",
                            stepStyleClass: "slds-progress__item is-active",
                            isActive: true,
                            isRejected: currentValue2 === REQUEST_REJECTED || currentValue2 === REQUEST_REJECTED_AFTER_REVIEW
                        });
                    } else {
                        options.push({
                            label: this.objectApiName === CASE_OBJECT.objectApiName && value.value === this.labels.responded ? this.labels.inputRequired : value.label,
                            value: value.value,
                            markerStyleClass: "slds-button slds-button_icon slds-progress__marker slds-progress__marker_icon",
                            stepStyleClass: "slds-progress__item is-active",
                            isActive: true,
                            isRejected: currentValue2 === REQUEST_REJECTED || currentValue2 === REQUEST_REJECTED_AFTER_REVIEW
                        });
                    }
                } else if (currentPassed) {
                    options.push({
                        label: this.objectApiName === CASE_OBJECT.objectApiName && value.value === this.labels.responded ? this.labels.inputRequired : value.label,
                        value: value.value,
                        markerStyleClass: "slds-button slds-progress__marker",
                        stepStyleClass: "slds-progress__item"
                    });
                } else {
                    options.push({
                        label: this.objectApiName === CASE_OBJECT.objectApiName && value.value === this.labels.responded ? this.labels.inputRequired : value.label,
                        value: value.value,
                        markerStyleClass: "slds-button slds-button_icon slds-button_icon slds-progress__marker slds-progress__marker_icon",
                        stepStyleClass: "slds-progress__item is-active",
                        isCompleted: true
                    });
                }
            });
            this.options = options;
            this.componentReady = true;
        })
    }

    @wire(fetchStatusDefinitions, {
        objectApiName: '$objectApiName',
        recordTypeDeveloperName: '$recordTypeDeveloperName',
        picklistApiName: '$fieldApiName'
    })
    fetchStatusDefinitions(response) {
        let data = response.data;
        let error = response.error;
        if (data) {
            this.statusDefinitions = data;
        } else if (error) {
            console.log(error);
        }
    }

    get progressListStyleClass() {
        if (this.hasWarning) {
            return "slds-progress__list slds-has_warning";
        } else {
            return "slds-progress__list";
        }
    }

    get ariaLabelForCurrentValue() {
        return yourStatus + ' ' + this.currentValue;
    }

    handleCheckStatusDefinitions(event) {
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: checkStatusDefinitions,
                    message: this.statusDefinitions.split(/\r?\n/),
                    hideFooter: true
                });
    }
}