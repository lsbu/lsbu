import {LightningElement, api } from 'lwc';
import {loadStyle} from "lightning/platformResourceLoader";
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';

export default class LsbCommSelfAssessmentNoResult extends LightningElement {

    @api headerText;
    @api messageText;

    connectedCallback() {
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    get iconClass() {
        return "mdi " + "mdi-text-box-remove-outline";
    }

}