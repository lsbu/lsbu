import { api, LightningElement } from 'lwc';

import getTeamMembers from '@salesforce/apex/LSB_CaseTeamRelatedListController.getTeamMembers';
import deleteTeamMember from '@salesforce/apex/LSB_CaseTeamRelatedListController.deleteTeamMember';
import deleteTeamTemplate from '@salesforce/apex/LSB_CaseTeamRelatedListController.deleteTeamTemplate';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import mainTitle from '@salesforce/label/c.LSB_CaseTeamComponentTitle';
import addMember from '@salesforce/label/c.LSB_CaseTeamAddMemberButton';
import addTeam from '@salesforce/label/c.LSB_CaseTeamAddTeamButton';
import columnTeamMember from '@salesforce/label/c.LSB_CaseTeamColumnTeamMember';
import columnMemberRole from '@salesforce/label/c.LSB_CaseTeamColumnMemberRole';
import columnCaseAccess from '@salesforce/label/c.LSB_CaseTeamColumnCaseAccess';
import columnVisibleInCSP from '@salesforce/label/c.LSB_CaseTeamColumnVisibleInCSP';
import deleteTeamModalTitle from '@salesforce/label/c.LSB_CaseTeamDeleteTeamModalTitle';
import deleteTeamModalMessage from '@salesforce/label/c.LSB_CaseTeamDeleteTeamModalMessage';
import deleteMemberModalTitle from '@salesforce/label/c.LSB_CaseTeamDeleteMemberModalTitle';
import deleteMemberModalMessage from '@salesforce/label/c.LSB_CaseTeamDeleteMemberModalMessage';
import deleteButton from '@salesforce/label/c.LSB_CaseTeamDeleteButton';
import cancelButton from '@salesforce/label/c.LSB_CaseTeamCancelButton';
import deleteTeamToast from '@salesforce/label/c.LSB_CaseTeamDeleteTeamToast';
import deleteMemberToast from '@salesforce/label/c.LSB_CaseTeamDeleteMemberToast';

const actions = [
    { label: 'Delete', name: 'delete' },
];

const DELAY = 3000;

export default class LsbCaseTeamRelatedList extends NavigationMixin(LightningElement) {

    labels = {
        mainTitle,
        addMember,
        addTeam,
        columnTeamMember,
        columnMemberRole,
        columnCaseAccess,
        columnVisibleInCSP,
        deleteTeamModalTitle,
        deleteTeamModalMessage,
        deleteMemberModalTitle,
        deleteMemberModalMessage,
        deleteButton,
        cancelButton,
        deleteTeamToast,
        deleteMemberToast
    };

    @api recordId;
    data = [];
    actions = actions;
    title;
    isReady = false;

    connectedCallback() {
        this.loadTeamMembers();
    }

    loadTeamMembers() {
        let caseId = this.recordId;
        if (caseId) {
        getTeamMembers({ caseId: caseId })
            .then(result => {
                this.data = result;
                this.isReady = true;
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
                this.isReady = true;
            });
        }
    }

    handleRowAction(event) {
        const actionName = event.target.value;
        const rowId = event.currentTarget.dataset.id;
        let isTeam = event.currentTarget.dataset.name;
        if (isTeam == 'false') {
            isTeam = false;
        } else {
            isTeam = true;
        }
        let payload = {
            rowId: rowId,
            isTeam: isTeam
        }
        switch (actionName) {
            case 'delete':
                this.showConfirmationModal(payload);
                break;
            default:
        }
    }

    showConfirmationModal(payload) {
        let modalHeader;
        let modalMessage;
        if (payload.isTeam == true) {
            modalHeader = this.labels.deleteTeamModalTitle;
            modalMessage = this.labels.deleteTeamModalMessage;
        } else {
            modalHeader = this.labels.deleteMemberModalTitle;
            modalMessage = this.labels.deleteMemberModalMessage;
        }
        this.template.querySelector('c-lsb_confirmation-modal')
            .openModal(
                {
                    header: modalHeader,
                    message: modalMessage,
                    hideCancel: false,
                    submitLabel: this.labels.deleteButton,
                    cancelLabel: this.cancelButton,
                    payload: payload
                });
    }

    openAddMemberModal() {
        this.template.querySelector('c-lsb_add-case-team-member-modal')
            .openModal(
                {
                    caseId: this.recordId,
                    teamMembers: this.data
                });
    }

    openAddTeamModal() {
        this.template.querySelector('c-lsb_add-case-predefined-team-modal')
            .openModal(
                {
                    caseId: this.recordId,
                    teamMembers: this.data
                });
    }

    handleRowClick(event) {
        const memberId = event.currentTarget.dataset.id;
        if (memberId) {
            this.navigateToRecordViewPage(memberId);
        }
    }

    handleSaveEvent(event) {
        if (event.detail) {
            this.loadTeamMembers();
        } else {
            window.clearTimeout(this.delayTimeout);
            this.delayTimeout = setTimeout(() => {
                this.loadTeamMembers();
            }, DELAY);
        }
    }

    navigateToRecordViewPage(memberId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: memberId,
                actionName: 'view'
            }
        });
    }

    deleteRow(event) {
        if(event.detail) {
            let eventData = event.detail;
            if (eventData.isTeam === false) {
                this.deleteMember(eventData.rowId);
            } else if (eventData.isTeam) {
                this.deleteTeam(eventData.rowId)
            }
        }
    }

    deleteTeam(rowId) {
        deleteTeamTemplate({templateId: rowId})
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.labels.deleteTeamToast,
                        variant: 'success',
                    }),
                );
                this.loadTeamMembers();
            })
            .catch(error => {
                console.log('Error: ', JSON.stringify(error))
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error',
                        message: error.message,
                        variant: 'error',
                    }),
                );
                this.loadTeamMembers();
            });
    }

    deleteMember(rowId) {
        deleteTeamMember({memberId: rowId})
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.labels.deleteMemberToast,
                        variant: 'success',
                    }),
                );
                this.loadTeamMembers();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error',
                        message: error.message,
                        variant: 'error',
                    }),
                );
                this.loadTeamMembers();
            });
    }

    get isDataEmpty() {
        return this.dataSize < 1;
    }

    get dataSize() {
        return this.data.length;
    }

    get cardTitle() {
        let cardTitle = this.labels.mainTitle;
        let dataSize = this.dataSize;
        return `${cardTitle} (${dataSize})`;
    }
}