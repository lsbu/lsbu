import {LightningElement} from 'lwc';

export default class LsbSpinner extends LightningElement {

    alternativeText = 'Loading';

}