import {LightningElement} from 'lwc';
import {loadStyle} from 'lightning/platformResourceLoader';
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';

import PLAN_TITLE from '@salesforce/label/c.LSB_CommSelfAssessmentPlanTitle';

export default class LsbCommSelfAssessmentTile extends LightningElement {


    labels = {
        PLAN_TITLE
    }

    connectedCallback() {
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    get iconClass() {
        return "mdi " + "mdi-bell";
    }
}