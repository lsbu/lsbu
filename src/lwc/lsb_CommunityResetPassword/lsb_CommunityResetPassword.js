import {LightningElement, track} from 'lwc';

// apex
import findUser from '@salesforce/apex/LSB_CommunityResetPasswordController.findUser';
import resetPassword from '@salesforce/apex/LSB_CommunityResetPasswordController.resetPassword';

// labels
import resetPasswordButtonLabel from '@salesforce/label/c.LSB_ResetPassButtonLabel';
import emailLabel from '@salesforce/label/c.LSB_ResetPassEmailFieldLabel';
import userDoesntExistMessage from '@salesforce/label/c.LSB_ResetPassUserDoesntExistMessage';
import somethingGoesWrongMessage from '@salesforce/label/c.LSB_ResetPassSomethingGoesWrongMessage';


export default class LSB_CommunityResetPasswordController extends LightningElement {

    @track email = '';
    @track userId = '';
    @track errorMessage = '';

    @track labels = {
        resetPasswordButtonLabel,
        emailLabel,
        userDoesntExistMessage,
        somethingGoesWrongMessage
    };

    renderedCallback() {
        const style = document.createElement('style');
        style.innerText = 'c-lsb_-community-reset-password .slds-form-element input { padding: 7px 12px; font-size: 13px; } \n' +
        'c-lsb_-community-reset-password .slds-input { border: 1px solid #d4d4d4 !important; }\n'
            + '.sfdc_input_container[c-lsb_CommunityResetPassword_lsb_CommunityResetPassword]{ padding: 9px; }\n'
            + 'c-lsb_-community-reset-password .slds-button_icon-bare { display: none }\n'
            + 'c-lsb_-community-reset-password .slds-button_brand { width: 100%;\n' +
            '    margin-top: 5px;\n' +
            '    margin-bottom: 7px; padding: 7px 12px;\n' +
            '    font-size: 13px;\n' +
            '    font-family: Arial, Helvetica, sans-serif;\n' +
            '    font-weight: 700;}' + 
            'c-lsb_-community-reset-password  .slds-button_outline-brand { width: 100%;\n' +
            '    margin-top: 5px;\n' +
            '    margin-bottom: 15px; padding: 7px 7px;\n' +
            '    font-size: 13px;\n' +
            '    font-family: Arial, Helvetica, sans-serif;\n' +
            '    border-radius: 4px;\n' +
            '    border-color: #D7425E;\n' +
            '    color: #D7425E;\n' +
            '    font-weight: 700;} \n' + 
            'c-lsb_-community-reset-password .slds-has-error input { border-color: #D7425E !important;} \n ' +
            'c-lsb_-community-reset-password .error-message.slds-text-color_error {margin-bottom: 16px !important;} \n ' +
            'c-lsb_-community-reset-password input:focus { border-color: #D7425E !important; } \n ' +
            'c-lsb_-community-reset-password .slds-form-element__help { color: #D7425E !important; margin-top: 5px; font-size: 13px}' + 
            'c-lsb_-community-reset-password .slds-rich-text-editor__output {color: #D7425E !important; margin-top: 5px; font-size: 13px;} \n ' + 
            'c-lsb_-community-reset-password .slds-rich-text-editor__output a {color: #D7425E !important;}';

        this.template.querySelector('lightning-input').appendChild(style);
    }

    validateEmail(event) {
        let inputValue = event.target.value;

        if (inputValue == null || inputValue == undefined || inputValue == '') {
            return;
        }

        findUser({ emailAddress: inputValue })
            .then(result => {
                if (result == undefined || result == null) {
                    this.errorMessage = this.labels.userDoesntExistMessage;
                } else {
                    this.userId = result;
                    this.errorMessage = '';
                }
            }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

    handleEmailChange(event) {
        this[event.target.name] = event.target.value;
    }

    resetPassword(event) {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid) {
            if (this.errorMessage == '') {
                if (this.userId == undefined || this.userId == null || this.userId == '') {
                    findUser({ emailAddress: this.email })
                        .then(result => {
                            if (result == undefined || result == null) {
                                this.errorMessage = this.labels.userDoesntExistMessage;
                            } else {
                                this.errorMessage = '';
                                this.userId = result;
                                this.doResetPass(result);
                            }
                        }).catch(error => {
                        console.log('Error: ' + error.body.message);
                    });
                } else {
                    this.doResetPass(this.userId);
                }
            }
        }
    }

    doResetPass(userId) {
        resetPassword({ userId: userId })
            .then(result => {
                if (result == '') {
                    window.location.href = './CheckPasswordResetEmail';
                } else {
                    this.errorMessage = this.labels.somethingGoesWrongMessage;
                }
            }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

}