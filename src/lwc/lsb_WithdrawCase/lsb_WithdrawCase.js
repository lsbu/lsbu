import {api, track, LightningElement} from 'lwc';

import saveButton from '@salesforce/label/c.LSB_Save';
import withdrawCaseLabel from '@salesforce/label/c.LSB_WithdrawCase';
import closedReason from '@salesforce/label/c.LSB_ClosedReason';
import confirmWithdrawCase from '@salesforce/label/c.LSB_ConfirmWithdrawCase';
import withdrawalComment from '@salesforce/label/c.LSB_WithdrawalComment';
import withdrawalReason from '@salesforce/label/c.LSB_WithdrawalReason';
import isEcClaim from '@salesforce/apex/LSB_ListViewController.isEcClaim';

import withdrawCase from '@salesforce/apex/LSB_ListViewController.withdrawCase';
import withdrawClaim from '@salesforce/apex/LSB_ListViewController.withdrawClaim';
import communityResources from '@salesforce/resourceUrl/communityResources';
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

export default class LsbWithdrawCase extends LightningElement {

    @track closedReasons = [];
    @api recordId;

    componentReady = false;
    showModal = false;
    showSpinner = false;

    ecClaim;

    labels = {
        saveButton,
        withdrawCaseLabel,
        closedReason,
        withdrawalComment,
        withdrawalReason
    };

    handleOpenModal() {
        if (typeof this.ecClaim === "undefined") {
            this.checkForEcClaimRecordType();
        } else {
            this.showModal = true;
        }
    }

    checkForEcClaimRecordType() {
        isEcClaim({
            caseId: this.recordId
        }).then(result => {
            this.ecClaim = result;
            if (!result) {
                fetch(communityResources)
                    .then((response) => response.json())
                    .then((data) => {
                        this.closedReasons = data.communityClosedReasons.default;
                    });
            } else {
                fetch(communityResources)
                    .then((response) => response.json())
                    .then((data) => {
                        this.closedReasons = data.communityClosedReasons.ecClaim;
                    });
            }
            this.showModal = true;
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        })
    }

    handleCloseModal() {
        this.closedReason = undefined;
        this.showModal = false;
    }

    closedReason;
    otherReason;

    handleChange(event) {
        this.closedReason = event.detail.value;
        this.otherReason = undefined;
    }

    handleOtherReason(event) {
        this.otherReason = event.detail.value;
    }

    get isOther() {
        return this.ecClaim && this.closedReason === 'Other';
    }

    handleSubmit() {
        if (typeof this.closedReason === "undefined") {
            return;
        }
        if (this.isOther && !this.otherReason) {
            return;
        }
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: withdrawCaseLabel,
                    message: confirmWithdrawCase
                });
    }

    handleCaseWithdraw(event) {
        if (event.detail) {
            if (this.ecClaim) {
                this.showSpinner = true;
                withdrawClaim({
                    caseId: this.recordId,
                    withdrawalReason: this.closedReason,
                    withdrawalComment: this.otherReason
                }).then(result => {
                    eval("$A.get('e.force:refreshView').fire();");
                    this.showSpinner = false;
                }).catch(error => {
                    console.log(error);
                    eval("$A.get('e.force:refreshView').fire();");
                    this.showSpinner = false;
                });
            } else {
                this.showSpinner = true;
                withdrawCase({
                    recordId: this.recordId,
                    closedReason: this.closedReason
                }).then(result => {
                    eval("$A.get('e.force:refreshView').fire();");
                    this.showSpinner = false;
                }).catch(error => {
                    console.log(error);
                    eval("$A.get('e.force:refreshView').fire();");
                    this.showSpinner = false;
                });
            }
        } else {
            let saveButton = this.template.querySelector('.save__button');
            saveButton.focus();
        }
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        this.componentReady = true;
        this.addListeners();
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.modal__main-container');
        if (modalContainer) {
            modalContainer.focus();
            modalContainer.addEventListener('keydown', (e) => {
                let closeButton = this.template.querySelector('.close__button');
                let saveButton = this.template.querySelector('.save__button');
                if (e.shiftKey && e.key === 'Tab' && e.target === closeButton) {
                    e.preventDefault();
                    saveButton.focus();
                } else if (!e.shiftKey && e.key === 'Tab' && e.target === saveButton) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }
}