import {LightningElement, api, track} from 'lwc';

import previousYearResultsModule from '@salesforce/label/c.LSB_PreviousYearResultsModule';
import previousYearResultsModuleOutcome from '@salesforce/label/c.LSB_PreviousYearResultsModuleOutcome';
import previousYearResultsMarks from '@salesforce/label/c.LSB_PreviousYearResultsMarks';
import previousYearResultsSemester from '@salesforce/label/c.LSB_PreviousYearResultsSemester';
import previousYearResultsAttempt from '@salesforce/label/c.LSB_PreviousYearResultsAttempt';
import previousYearResultsResit from '@salesforce/label/c.LSB_PreviousYearResultsResit';
import previousYearResultsModuleCredits from '@salesforce/label/c.LSB_PreviousYearResultsModuleCredits';
import previousYearResultsModuleFail from '@salesforce/label/c.LSB_PreviousYearResultsModuleFail';
import previousYearResultsModulePass from '@salesforce/label/c.LSB_PreviousYearResultsModulePass';


export default class LsbPreviousYearResultsTab extends LightningElement {
    @track
    _previousYearResults;

    @api
    get previousYearResults() {
        return this._previousYearResults;
    }

    set previousYearResults(value) {
        this._previousYearResults = JSON.parse(JSON.stringify(value));
    }


    labels = {
        previousYearResultsModule,
        previousYearResultsModuleOutcome,
        previousYearResultsMarks,
        previousYearResultsSemester,
        previousYearResultsAttempt,
        previousYearResultsResit,
        previousYearResultsModuleCredits,
        previousYearResultsModuleFail,
        previousYearResultsModulePass
    }

    toggleExpand(event) {
        let module = this._previousYearResults[event.currentTarget.dataset.index];
        module.isExpanded = !module.isExpanded;
        if (module.outcome) {
            module.styleClass = module.isExpanded ? 'module__expanded' : 'module__collapsed';
        } else {
            module.styleClass = module.isExpanded ? 'module__expanded module__failed' : 'module__collapsed module__failed';
        }
    }

}