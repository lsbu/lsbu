import {api, LightningElement, track, wire} from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import CASE_RECORD from '@salesforce/schema/Case';
import CONTACT_ID from "@salesforce/schema/User.ContactId";
import SUBMIT_5_WORKING_DAYS_FIELD from '@salesforce/schema/Case.LSB_CAS_SubmitWithin5WorkingDays__c';
import REASON_FOR_EC_FIELD from '@salesforce/schema/Case.LSB_CAS_ReasonForEC__c';
import ABLE_TO_SIT_EXAM_FIELD from '@salesforce/schema/Case.LSB_CAS_AreYouAbleToAttendTheExam__c';
import USER_ID from '@salesforce/user/Id';

import getConfigData from '@salesforce/apex/LSB_CommCreateClaimController.getConfigData';
import getModuleData from '@salesforce/apex/LSB_CommCreateClaimController.getModuleData';
import getECScreenConfig from '@salesforce/apex/LSB_CommCreateClaimController.getECScreenAssessmentConfig';
import saveCaseRecord from '@salesforce/apex/LSB_CommCreateClaimController.saveClaimCase';

import confirmationModalTitle from '@salesforce/label/c.LSB_ECConfirmationModalTitle';
import confirmationModalMessage from '@salesforce/label/c.LSB_ECConfirmationModalMessage';
import confirmationModalYes from '@salesforce/label/c.LSB_ECConfirmationModalYes';
import confirmationModalNo from '@salesforce/label/c.LSB_ECConfirmationModalNo';
import otherCheckboxLabel from '@salesforce/label/c.LSB_ECOtherCheckboxLabel';
import detailsPlaceholder from '@salesforce/label/c.LSB_ECDetailsPlaceholder';
import detailsLabel from '@salesforce/label/c.LSB_ECDetailsLabel';
import declarationError from '@salesforce/label/c.LSB_ECDeclarationMissingError';
import confirmationCheckboxLabel from '@salesforce/label/c.LSB_ECConfirmationCheckboxLabel';
import modalTitle from '@salesforce/label/c.LSB_ECModalTitle';
import backButton from '@salesforce/label/c.LSB_ECModalBackButton';
import submitButton from '@salesforce/label/c.LSB_ECModalSubmitButton';
import okButton from '@salesforce/label/c.LSB_ECModalOkButton';
import requestTypeFieldLabel from '@salesforce/label/c.LSB_ECRequestTypeFieldLabel';
import nextButtonLabel from '@salesforce/label/c.LSB_ECModalNextButton';
import selectModulePlaceholder from '@salesforce/label/c.LSB_ECModuleFieldPlaceholder';
import selectModuleLabel from '@salesforce/label/c.LSB_ECModuleFieldLabel';
import assessmentFieldLabel from '@salesforce/label/c.LSB_ECAssessmentFieldLabel';
import assessmentFieldPlaceholder from '@salesforce/label/c.LSB_ECAssessmentFieldPlaceholder';
import submit5WorkingDaysFieldLabel from '@salesforce/label/c.LSB_ECAbleSubmit5WorkingDaysLabel';
import attendExamFieldLabel from '@salesforce/label/c.LSB_ECAbleToAttendExamFieldLabel';
import reasonFieldLabel from '@salesforce/label/c.LSB_ECReasonFieldLabel';
import regulationsLinkValue from '@salesforce/label/c.LSB_ECRegulationsLinkValue';
import regulationsLinkLabel from '@salesforce/label/c.LSB_ECRegulationsLinkLabel';
import regulationsFieldLabel from '@salesforce/label/c.LSB_ECRegulationsConfirmationLabel';
import moduleFieldError from '@salesforce/label/c.LSB_ECModuleFieldError';
import assessmentFieldError from '@salesforce/label/c.LSB_ECAssessmentFieldError';
import noArrangementsError from '@salesforce/label/c.LSB_ECNoArrangementsError';
import reasonFieldError from '@salesforce/label/c.LSB_ECReasonFieldError';
import selectAnswerError from '@salesforce/label/c.LSB_ECSelectAnswerFieldError';
import detailsFieldError from '@salesforce/label/c.LSB_ECDetailsFieldError';
import confirmationOtherFieldError from '@salesforce/label/c.LSB_ECConfirmationOtherFieldError';

const MAX_SCREEN_NO = 4;
const LATE_SUBMISSION_REQUEST = 'Late submission request';
const EC_REQUEST = 'Extenuating circumstances claim';
const DDS_LATE_REQUEST = 'DDS late submission request';
const REASON_OTHER = 'Other';

const STUDENT_CONTACT_ROLE = 'Student';
const EC_LATE_SUBMISSION_REQUEST = 'EC / Late submission request';
export default class LsbCommCreateClaimModal extends LightningElement {

    labels = {
        confirmationModalTitle,
        confirmationModalMessage,
        confirmationModalYes,
        confirmationModalNo,
        otherCheckboxLabel,
        detailsPlaceholder,
        detailsLabel,
        declarationError,
        confirmationCheckboxLabel,
        modalTitle,
        backButton,
        submitButton,
        okButton,
        requestTypeFieldLabel,
        nextButtonLabel,
        selectModulePlaceholder,
        selectModuleLabel,
        assessmentFieldLabel,
        assessmentFieldPlaceholder,
        submit5WorkingDaysFieldLabel,
        attendExamFieldLabel,
        reasonFieldLabel,
        regulationsLinkValue,
        regulationsLinkLabel,
        regulationsFieldLabel,
        moduleFieldError,
        assessmentFieldError,
        noArrangementsError,
        reasonFieldError,
        selectAnswerError,
        detailsFieldError,
        confirmationOtherFieldError
    }

    @track recordTypeId;
    hasActiveSupportArrangements = false;
    progress = 5;
    screenNo = 1;
    @track isModalOpen = false;
    @track isReady = false;

    selectedRequestType;
    selectedModule;
    selectedModuleId;
    selectedAssessment;
    selectedAssessmentId;
    reason = '';
    understandAcademicRegulations = false;
    informationProvidedCorrect = false;
    submitWithin5Days;
    ableToSitExam;
    confirmReasonOther = false;
    details;
    @track createdCaseId;

    ecScreenConfigData;
    @track confirmationScreenConfigData;

    picklistData = [];
    requestTypeData = [];
    moduleData = [];

    moduleOptions = [];
    assessmentOptions = [];
    submitWithin5DaysOptions= [];
    reasonOptions = [];
    sitExamOptions = [];
    error;

    @wire(getPicklistValuesByRecordType, {
        objectApiName: CASE_RECORD,
        recordTypeId: '$recordTypeId'
    })
    wiredPicklistValues({ error, data }) {
        if (data) {
            this.picklistData = data.picklistFieldValues;
            this.error = undefined;
        } else {
            this.error = error;
            this.treeModel = undefined;
        }
    }

    @wire(getRecord, { recordId: USER_ID, fields: [CONTACT_ID] })
    currentUser;

    get contactId() {
        return getFieldValue(this.currentUser.data, CONTACT_ID);
    }

    get progressBarValue() {
        return `width:${this.progress}%`;
    }

    get progressBarValueText() {
        return `Progress:${this.progress}%`;
    }

    get showFirstScreen() {
        return this.screenNo == 1;
    }

    get requestTypeNotSelected() {
        return !this.selectedRequestType;
    }

    get showSecondScreen() {
        if (this.selectedRequestType && this.screenNo == 2) {
            return true;
        }
        return false;
    }

    get showThirdScreen() {
        return this.screenNo == 3 && this.isECRequest;
    }

    get showLastScreen() {
        return this.screenNo == MAX_SCREEN_NO && this.createdCaseId != null;
    }

    get isLateRequest() {
        return this.selectedRequestType.value == LATE_SUBMISSION_REQUEST;
    }

    get isECRequest() {
        return this.selectedRequestType.value == EC_REQUEST;
    }

    get isFirstScreen() {
        return this.screenNo == 1;
    }

    get isLastScreen() {
        return this.screenNo == MAX_SCREEN_NO;
    }

    get selectedValues() {
        return this.reason.join(',');
    }

    get hasOtherValue() {
        if (this.reason) {
            let reasonsList = this.reason.split(';');
            return reasonsList.includes(REASON_OTHER);
        }
        return false;
    }

    get moduleNotSelected() {
        return this.selectedModule == undefined;
    }

    get understandRegulationLabel() {
        let linkValue = this.labels.regulationsLinkValue;
        let linklabel = this.labels.regulationsLinkLabel;
        let linkTag = `<a href="${linkValue}" target="blank">${linklabel}</a>`;
        return `${this.labels.regulationsFieldLabel} ${linkTag}`;
    }

    preparePicklistValues(picklistConfig) {
        this.reasonOptions = this.setPicklistValuesFromConfig(REASON_FOR_EC_FIELD.fieldApiName);
        this.submitWithin5DaysOptions = this.setPicklistValuesFromConfig(SUBMIT_5_WORKING_DAYS_FIELD.fieldApiName, picklistConfig.LSB_AbleToSubmit5WorkingDaysOptions__c);
        this.sitExamOptions = this.setPicklistValuesFromConfig(ABLE_TO_SIT_EXAM_FIELD.fieldApiName, picklistConfig.LSB_AbleToAttendExamOptions__c);
    }

    setPicklistValuesFromConfig(fieldApiName, picklistConfig) {
        let picklistValues = [];
        let allPicklistValues = this.picklistData[fieldApiName].values;
        if (picklistConfig) {
            let picklistConfigList = picklistConfig.split(';');
            for (let i = 0; i < picklistConfigList.length; i ++) {
                for (let j = 0; j < allPicklistValues.length; j ++) {
                    if (allPicklistValues[j].value === picklistConfigList[i]) {
                        picklistValues.push(allPicklistValues[j]);
                    }
                }
            }
        } else  {
            picklistValues = allPicklistValues;
        }
        return picklistValues;
    }

    @api
    openModal() {
        this.isModalOpen = true;
        this.loadConfigData();
    }

    loadConfigData() {
        this.isReady = false;
        getConfigData({ contactId: this.contactId})
            .then(result => {
                let configData = JSON.parse(result);
                this.recordTypeId = configData.claimRecordTypeId;
                this.requestTypeData = configData.configData;
                this.hasActiveSupportArrangements = configData.hasActiveSupportArrangements;
            })
            .catch(error => {
                console.log('error>', error)
            })
            .finally( () => {
                this.isReady = true;
            });
    }

    loadModuleData() {
        this.isReady = false;
        getModuleData({ contactId: this.contactId, requestType: this.selectedRequestType.value })
            .then(result => {
                this.setModuleOptions(result);
            })
            .catch(error => {
                console.log('error>', error)
                this.isReady = true;
            })
            .finally( () => {
            });
    }

    setModuleOptions(resultArray) {
        let modules = [];
        this.moduleData = resultArray;
        resultArray.forEach(el => {
            modules.push({
                label: el.name,
                value: el.id
            })
        });
        this.moduleOptions = modules;
        let currentScreen = this.screenNo;
        let nextScreen = currentScreen + 1;
        this.setNextScreen(currentScreen, nextScreen);
    }

    handleCloseModal() {
        this.isModalOpen = false;
        this.resetModalData();
    }

    resetModalData() {
        this.progress = 5;
        this.screenNo = 1;
        this.selectedRequestType = undefined;
        this.requestTypeData = [];
        this.createdCaseId = undefined;
        this.details = undefined;
        this.resetSecondScreenFields();
        this.resetThirdScreenFields();
    }

    resetSecondScreenFields() {
        this.selectedModule = undefined;
        this.selectedModuleId = undefined;
        this.selectedAssessment = undefined;
        this.selectedAssessmentId = undefined;
        this.moduleData = [];
        this.moduleOptions = [];
        this.assessmentOptions = [];
    }

    resetThirdScreenFields() {
        this.reason = '';
        this.understandAcademicRegulations = false;
        this.informationProvidedCorrect = false;
        this.submitWithin5Days = undefined;
        this.ableToSitExam = undefined;
        this.confirmReasonOther = false;
        this.submitWithin5DaysOptions= [];
        this.reasonOptions = [];
        this.sitExamOptions = [];
        this.ecScreenConfigData = undefined;
    }

    handleClickNext() {
        if (this.isFirstScreen) {
            if (this.firstScreenValid()) {
                this.loadModuleData();
            }
        } else {
            if (this.secondScreenValid()) {
                this.loadECScreenConfigData();
            }
        }
    }

    loadECScreenConfigData() {
        this.isReady = false;
        let payload = this.createScreenConfigParameters();
        getECScreenConfig(payload)
            .then((result) => {
                this.ecScreenConfigData = result;
                this.preparePicklistValues(result)
                let currentScreen = this.screenNo;
                let nextScreen = currentScreen + 1;
                this.setNextScreen(currentScreen, nextScreen);
            })
            .catch((error) => {
                console.log('Error: ', error)
                this.showToast('error', error.body.message)
                this.isReady = true;
            });
    }

    createScreenConfigParameters() {
        if (this.selectedAssessment && this.selectedAssessment.isRetrospective) {
            return {
                isRetrospective : true,
                isExam : false,
                isResit : false
            }
        } else {
            return {
                isRetrospective : this.selectedAssessment.isRetrospective,
                isExam : this.selectedAssessment.isExam,
                isResit : this.selectedAssessment.isResit
            }
        }
    }

    isAllValid() {
        const allValid = [...this.template.querySelectorAll('lightning-checkbox-group, lightning-input, lightning-textarea, lightning-combobox, lightning-radio-group')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        return allValid;
    }

    firstScreenValid() {
        if (this.isRequestTypeValid()) {
            if (this.selectedRequestType.value == DDS_LATE_REQUEST && !this.hasActiveSupportArrangements) {
                this.showToast('error', this.labels.noArrangementsError);
                return false;
            }
            return true;
        }
        return false;
    }

    secondScreenValid() {
        this.validateFieldConditionallyByIdAndValue(
            'moduleSelect',
            this.selectedModule,
            true,
            this.labels.moduleFieldError
        );

        this.validateFieldConditionallyByIdAndValue(
            'assessmentSelect',
            this.selectedAssessment,
            true,
            this.labels.assessmentFieldError
        );

        return this.isAllValid();
    }

    validateThirdScreen() {
        this.validateFieldConditionallyByIdAndValue(
            'reasonCheckbox',
            this.reason,
            true,
            this.labels.reasonFieldError
        );

        this.validateFieldConditionallyByIdAndValue(
            'submitRadioGroup',
            this.submitWithin5Days,
            this.ecScreenConfigData.LSB_ShowAbleSubmit5WorkingDays__c,
            this.labels.selectAnswerError
        );

        this.validateFieldConditionallyByIdAndValue(
            'sitExamRadioGroup',
            this.ableToSitExam,
            this.ecScreenConfigData.LSB_ShowAbleToAttendExam__c,
            this.labels.selectAnswerError
        );

        this.validateFieldConditionallyByIdAndValue(
            'detailsText',
            this.details,
            this.ecScreenConfigData.LSB_DetailsMandatory__c,
            this.labels.detailsFieldError
        );

        this.validateFieldConditionallyByIdAndValue(
            'otherConfirmation',
            this.confirmReasonOther,
            this.hasOtherValue,
            this.labels.confirmationOtherFieldError
        );

        this.validateFieldConditionallyByIdAndValue(
            'confirmationCorrectCheckbox',
            this.informationProvidedCorrect,
            true,
            this.labels.declarationError
        );

        return this.validateUnderstandRegulations() && this.isAllValid();
    }

    validateFieldConditionallyByIdAndValue(dataId, fieldValue, condition, message) {
        let field = this.template.querySelector(`[data-id='${dataId}']`);
        if (condition) {
            if (!fieldValue) {
                field.setCustomValidity(message);
                field.reportValidity();
                return false;
            } else {
                field.setCustomValidity("");
                field.reportValidity();
            }
        }
    }

    validateUnderstandRegulations() {
        if (!this.understandAcademicRegulations) {
            this.template.querySelector(`div.academic-regulation__confirmation`).classList.add('slds-has-error');
            this.template.querySelector(`div.academic-regulation__confirmation__error`).classList.remove('slds-hide');
            return false;
        } else {
            this.template.querySelector(`div.academic-regulation__confirmation`).classList.remove('slds-has-error');
            this.template.querySelector(`div.academic-regulation__confirmation__error`).classList.add('slds-hide');
            return true;
        }
    }

    isRequestTypeValid() {
        if (!this.selectedRequestType) {
            this.template.querySelector(`div.request-types__input`).classList.add('slds-has-error');
            this.template.querySelector(`div.request-types__error`).classList.remove('slds-hide');
            return false;
        } else {
            this.template.querySelector(`div.request-types__input`).classList.remove('slds-has-error');
            this.template.querySelector(`div.request-types__error`).classList.add('slds-hide');
            return true;
        }
    }

    handleClickBack() {
        if (!this.isFirstScreen && !this.isLastScreen) {
            let currentScreen = this.screenNo;
            let nextScreen = currentScreen - 1;
            this.setNextScreen(currentScreen, nextScreen);
        }
    }

    handleSubmit() {
        if (this.screenNo == 2) {
            if(this.secondScreenValid()) {
                this.showConfirmationModal();
            }
        } else {
            if (this.validateThirdScreen()) {
                this.showConfirmationModal();
            }
        }
    }

    saveCaseRecord(event) {
        if(event.detail) {
            this.isReady = false;
            let caseSerialized = JSON.stringify(this.createClaimCaseData());
            saveCaseRecord({caseData: caseSerialized})
                .then((result) => {
                    this.confirmationScreenConfigData = result;
                    this.createdCaseId = result.caseId;
                    let currentScreen = this.screenNo;
                    this.setNextScreen(currentScreen, MAX_SCREEN_NO)
                })
                .catch((error) => {
                    console.log('Error: ', error);
                })
                .finally(() => {
                    this.isReady = true;
                });
        }
    }

    createClaimCaseData() {
        let caseToCreate = {
            RecordTypeId: this.recordTypeId,
            LSB_CAS_RequestType__c: this.selectedRequestType.value,
            LSB_CAS_RelatedModule__c: this.selectedModule.id,
            LSB_CAS_RelatedAssessment__c: this.selectedAssessment.value,
            LSB_CAS_SubmitWithin5WorkingDays__c: this.submitWithin5Days,
            LSB_CAS_ReasonForEC__c: this.reason,
            LSB_CAS_ConfirmECWhenReasonIsOther__c: this.confirmReasonOther,
            LSB_CAS_UnderstandTheAcademicRegulations__c: this.understandAcademicRegulations,
            LSB_CAS_InformationProvidedIsCorrect__c: this.informationProvidedCorrect,
            LSB_CAS_AreYouAbleToAttendTheExam__c: this.ableToSitExam,
            LSB_CAS_ContactRole__c: STUDENT_CONTACT_ROLE,
            LSB_CAS_Topic__c: EC_LATE_SUBMISSION_REQUEST,
            LSB_CAS_NatureOfEnquiry__c: EC_LATE_SUBMISSION_REQUEST,
            Description: this.details,
            ContactId: this.contactId
        }
        return caseToCreate;
    }

    setNextScreen(currentScreen, nextScreen) {
        this.screenNo = nextScreen;
        this.setProgressBarValue(nextScreen, MAX_SCREEN_NO);
        if (!this.isFirstScreen && !this.isLastScreen) {
            this.template.querySelector(`button.modal__back`).classList.add('modal__back--active');
        } else {
            this.template.querySelector(`button.modal__back`).classList.remove('modal__back--active');
        }
        this.isReady= true;
    }

    setProgressBarValue(nextScreen, maxScreenNo) {
        let progress = Math.round(nextScreen / maxScreenNo * 100);
        this.progress = progress;
    }

    handleOtherConfirmationChange(event) {
        this.confirmReasonOther = event.target.checked;
        this.validateFieldConditionallyByIdAndValue(
            'otherConfirmation',
            this.confirmReasonOther,
            this.hasOtherValue,
            this.labels.confirmationOtherFieldError
        );
    }

    handleReasonChange(event) {
        this.reason = event.detail.value.join(';');
        if (!this.hasOtherValue) {
            this.confirmReasonOther = false;
        }

        this.validateFieldConditionallyByIdAndValue(
            'reasonCheckbox',
            this.reason,
            true,
            this.labels.reasonFieldError
        );
    }

    handleDescriptionChange(event) {
        let descriptionValue = event.target.value;
        this.details = descriptionValue;
        this.validateFieldConditionallyByIdAndValue(
            'detailsText',
            this.details,
            this.ecScreenConfigData.LSB_DetailsMandatory__c,
            this.labels.detailsFieldError
        );
    }

    handleRequestTypeChange(event) {
        let requestValue = event.target.value;
        let selectedRequestType = {};
        this.requestTypeData.forEach((el) => {
            el.active = false;
            if (el.value === requestValue) {
                el.active = true;
                selectedRequestType = el;
            }
        });
        if (this.selectedRequestType) {
            this.resetSecondScreenFields();
            this.resetThirdScreenFields();
        }
        this.selectedRequestType = selectedRequestType;
        this.isRequestTypeValid();
    }

    handleModuleChange(event) {
        let moduleId = event.target.value;
        let selectedModule = this.moduleData.find(el => el.id === moduleId);
        this.selectedModule = selectedModule;
        this.selectedModuleId = moduleId;
        this.assessmentOptions = selectedModule.assessments;
        this.resetThirdScreenFields();
        this.secondScreenValid();this.validateFieldConditionallyByIdAndValue(
            'moduleSelect',
            this.selectedModule,
            true,
            this.labels.moduleFieldError
        );
    }

    handleAssessmentChange(event) {
        let assessmentId = event.target.value;
        let selectedAssessment = this.assessmentOptions.find(el => el.value === assessmentId);
        this.selectedAssessment = selectedAssessment;
        this.selectedAssessmentId = assessmentId;
        this.resetThirdScreenFields();
        this.validateFieldConditionallyByIdAndValue(
            'assessmentSelect',
            this.selectedAssessment,
            true,
            this.labels.assessmentFieldError
        );
    }

    handleWorkingDaysChange(event) {
        let submitWorkingDays = event.target.value;
        this.submitWithin5Days = submitWorkingDays;
        this.validateFieldConditionallyByIdAndValue(
            'submitRadioGroup',
            this.submitWithin5Days,
            this.ecScreenConfigData.LSB_ShowAbleSubmit5WorkingDays__c,
            this.labels.selectAnswerError
        );
    }

    handleSitExamChange(event) {
        let ableToSitExam = event.target.value;
        this.ableToSitExam = ableToSitExam;
        this.validateFieldConditionallyByIdAndValue(
            'sitExamRadioGroup',
            this.ableToSitExam,
            this.ecScreenConfigData.LSB_ShowAbleToAttendExam__c,
            this.labels.selectAnswerError
        );
    }

    handleUnderstandRegulationsChange(event) {
        this.understandAcademicRegulations = event.target.checked;
        this.validateUnderstandRegulations()
    }

    handleInformationProvidedCheckboxChange(event) {
        this.informationProvidedCorrect = event.target.checked;
        this.validateFieldConditionallyByIdAndValue(
            'confirmationCorrectCheckbox',
            this.informationProvidedCorrect,
            true,
            this.labels.declarationError
        );
    }

    showConfirmationModal() {
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: this.labels.confirmationModalTitle,
                    message: this.labels.confirmationModalMessage,
                    hideCancel: false,
                    isInnerHtml: true,
                    submitLabel: this.labels.confirmationModalYes,
                    cancelLabel: this.labels.confirmationModalNo
                });
    }

    showToast(variant, message) {
        const evt = new ShowToastEvent({
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    renderedCallback() {
        this.componentReady = true;
        this.addListeners();
    }

    addListeners() {
        let modalContainer = this.template.querySelector('.modal__section');
        if (modalContainer) {
            modalContainer.addEventListener('keydown', (e) => {
                let closeButton = this.template.querySelector('.close__button');
                let saveButton = this.template.querySelector('.save__button');
                if (e.shiftKey && e.key === 'Tab' && e.target === closeButton) {
                    e.preventDefault();
                    saveButton.focus();
                } else if (!e.shiftKey && e.key === 'Tab' && e.target === saveButton) {
                    e.preventDefault();
                    closeButton.focus();
                }
            });
        }
    }
}