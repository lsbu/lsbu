import {LightningElement, track} from 'lwc';

// apex
import retrieveActionsFromMetadata from '@salesforce/apex/LSB_CommunityMyActions.retrieveActionsFromMetadata'

//labels
import findOutMore from '@salesforce/label/c.LSB_FindOutMore';

import {loadStyle} from "lightning/platformResourceLoader";
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';

export default class LsbCommunityMyActions extends LightningElement {

    @track dataReady = false;
    @track actions = [];

    labels = {
        findOutMore
    }

    renderedCallback() {
        if (this.dataReady) {
            return;
        }

        retrieveActionsFromMetadata()
            .then(result => {
                result.forEach((act) => {
                    this.actions.push({'LSB_Number__c' : act.LSB_Number__c, 'LSB_Link__c' : act.LSB_Link__c, 'LSB_Title__c' : act.LSB_Title__c, 'LSB_Description__c' : act.LSB_Description__c, 'LSB_Icon__c' : act.LSB_Icon__c ,'iconClass' : "mdi " + act.LSB_Icon__c});
                });
                this.dataReady = true;
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

    redirectToAction(event) {
        // Stop the event's default behavior.
        // Stop the event from bubbling up in the DOM.
        event.preventDefault();
        event.stopPropagation();

        window.location.href = event.currentTarget.dataset.url;
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    handlePress(event) {
        if (event.key === 'Enter') {
            this.redirectToAction(event);
        }
    }
}