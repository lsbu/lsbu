import {LightningElement, api} from 'lwc';

import fetchListViewColumns from '@salesforce/apex/LSB_ListViewController.fetchListViewColumns';
import fetchRecord from '@salesforce/apex/LSB_ListViewController.fetchRecord';
import changeTaskStatus from '@salesforce/apex/LSB_ListViewController.changeTaskStatus';

import markComplete from '@salesforce/label/c.LSB_MarkComplete';
import markInProgress from '@salesforce/label/c.LSB_MarkInProgress';
import userAction from '@salesforce/label/c.LSB_UserAction';
import submitTask from '@salesforce/label/c.LSB_SubmitTask';
import inProgressTask from '@salesforce/label/c.LSB_InProgressTask';
import addComplaintDetails from '@salesforce/label/c.LSB_AddComplaintDetails';
import moreOptions from '@salesforce/label/c.LSB_MoreOptions';

import LOCALE from '@salesforce/i18n/locale';
import CURRENCY from '@salesforce/i18n/currency';
import TIMEZONE from '@salesforce/i18n/timeZone';

import TASK_OBJECT from '@salesforce/schema/Task';
import CASE_OBJECT from '@salesforce/schema/Case';
import TASK_STATUS from '@salesforce/schema/Task.Status';
import CASE_STATUS from '@salesforce/schema/Case.Status';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

const TASK_COMPLETED = 'Completed';
const TASK_NEW = 'New';
const CASE_CLOSED = 'Closed';
const TASK_IN_PROGRESS = 'In Progress';

const REQUEST_SUPPORTED = 'Request Supported';
const REQUEST_REJECTED = 'Request Rejected';
const REQUEST_APPROVED_AFTER_REVIEW = 'Request Approved After Review';
const REQUEST_REJECTED_AFTER_REVIEW = 'Request Rejected After Review';
const REQUEST_WITHDRAWN_BY_STUDENT = 'Request Withdrawn by Student';

export default class LsbRecordBanner extends LightningElement {

    labels = {
        markComplete,
        markInProgress,
        userAction,
        submitTask,
        addComplaintDetails,
        moreOptions,
        inProgressTask
    };

    componentReady;

    @api recordId;
    @api objectApiName;
    @api fieldSetApiName;
    @api headerField;
    @api showAddComplaintDetails;
    @api showWithdrawCase;

    showRequestReview;

    fields;
    record;

    dateTimeFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric',
        timeZone: TIMEZONE
    });

    dateFormat = new Intl.DateTimeFormat(LOCALE, {
        year: 'numeric', month: 'short', day: 'numeric',
        timeZone: TIMEZONE
    });

    currencyFormat = new Intl.NumberFormat(LOCALE, {
        style: 'currency',
        currency: CURRENCY,
        currencyDisplay: 'symbol'
    });

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    disconnectedCallback() {
        this.componentReady = false;
    }

    renderedCallback() {
        if (this.componentReady) {
            return;
        }
        fetchListViewColumns({
            objectApiName: this.objectApiName,
            fieldSetApiName: this.fieldSetApiName
        }).then(result => {
            this.fields = result;
            fetchRecord({
                objectApiName: this.objectApiName,
                recordId: this.recordId,
                fields: this.fields.map(x => x.fieldNameOrPath)
            }).then(result => {
                this.showRequestReview = this.objectApiName === 'Case'
                    && result.RecordType.DeveloperName === 'LSB_CAS_ECClaim'
                    && result.Status === REQUEST_REJECTED;
                this.record = result;
                this.componentReady = true;
            }).catch(error => {
                console.log('Error: ' + error.body.message);
            })
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

    get recordDetails() {
        let recordDetails = [];
        this.fields.forEach(field => {
            if (!field.hidden && field.fieldNameOrPath !== this.headerField && field.fieldNameOrPath !== 'Status') {
                let fieldObj = {
                    column: field,
                    label: field.label,
                    value: this.record[field.fieldNameOrPath]
                };
                this.setLocaleOnField(fieldObj);
                recordDetails.push(fieldObj);
            }
        });
        return recordDetails;
    }

    get header() {
        return this.record[this.headerField];
    }

    setLocaleOnField(field) {
        if (field.column.type === 'DATETIME' && typeof field.value !== "undefined") {
            field.value = this.dateTimeFormat.format(new Date(field.value));
        } else if (field.column.type === 'DATE' && typeof field.value !== "undefined") {
            field.value = this.dateFormat.format(new Date(field.value));
        } else if (field.column.type === 'CURRENCY' && typeof field.value !== "undefined") {
            field.value = this.currencyFormat.format(field.value);
        }
    }

    get isTask() {
        return this.objectApiName === TASK_OBJECT.objectApiName;
    }

    get isCase() {
        return this.objectApiName === CASE_OBJECT.objectApiName;
    }

    get isCompleted() {
        return this.record[TASK_STATUS.fieldApiName] === TASK_COMPLETED;
    }

    get isNew() {
        return this.record[TASK_STATUS.fieldApiName] === TASK_NEW;
    }

    get isCaseClosed() {
        return this.record[CASE_STATUS.fieldApiName] === CASE_CLOSED ||
            this.record[CASE_STATUS.fieldApiName] === REQUEST_SUPPORTED ||
            this.record[CASE_STATUS.fieldApiName] === REQUEST_REJECTED ||
            this.record[CASE_STATUS.fieldApiName] === REQUEST_APPROVED_AFTER_REVIEW ||
            this.record[CASE_STATUS.fieldApiName] === REQUEST_REJECTED_AFTER_REVIEW ||
            this.record[CASE_STATUS.fieldApiName] === REQUEST_WITHDRAWN_BY_STUDENT;
    }

    get assignedByEmail() {
        if (typeof this.record.LSB_ACT_AssignedBy__r !== "undefined") {
            return 'mailto:' + this.record.LSB_ACT_AssignedByEmail__c;
        } else {
            return '';
        }
    }

    get assignedByName() {
        if (typeof this.record.LSB_ACT_AssignedBy__r !== "undefined") {
            return this.record.LSB_ACT_AssignedBy__r.FirstName + ' ' + this.record.LSB_ACT_AssignedBy__r.LastName;
        } else {
            return '';
        }
    }

    get assignedByLabel() {
        return this.fields.find(x => x.fieldNameOrPath === 'LSB_ACT_AssignedBy__c').label;
    }

    handleCompleteTask(event) {
        this.markAsComplete = true;
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: this.labels.userAction,
                    message: this.labels.submitTask
                });
    }

    handleInProgressTask(event) {
        this.markAsComplete = false;
        this.template.querySelector('c-lsb_-modal')
            .openModal(
                {
                    header: this.labels.userAction,
                    message: this.labels.inProgressTask
                });
    }

    markAsComplete = true;

    handleTaskSubmit(event) {
        if (event.detail) {
            changeTaskStatus({
                recordId: this.record.Id,
                newStatus: this.markAsComplete ? TASK_COMPLETED : TASK_IN_PROGRESS
            }).then(result => {
                eval("$A.get('e.force:refreshView').fire();");
            }).catch(error => {
                console.log(error);
            });
        }
    }

    showActions = false;

    openActions(event) {
        this.showActions = true;
    }

    closeActions(event) {
        this.showActions = false;
    }

    get showMobileActionsButton() {
        return (this.isTask && (this.isNew || !this.isCompleted))
            || this.showRequestReview
            || (this.isCase && this.showWithdrawCase && !this.isCaseClosed);
    }
}