import {LightningElement, api, wire} from 'lwc';

import fetchKnowledgeArticleVersions from '@salesforce/apex/LSB_CaseArticlesController.fetchKnowledgeArticleVersions';

import attachedArticles from '@salesforce/label/c.LSB_AttachedArticles';
import readAnswer from '@salesforce/label/c.LSB_ReadAnswer';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

export default class LsbCaseArticles extends LightningElement {

    showSpinner = true;

    @api caseId;
    knowledgeArticles;

    labels = {
        attachedArticles,
        readAnswer
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    @wire(fetchKnowledgeArticleVersions, {caseId: '$caseId'})
    fetchedKnowledgeArticleVersions(result) {
        if (result.data) {
            let knowledgeArticles = [];
            result.data.forEach(kav => {
                let ka = {...kav}
                ka.url = '/s/article/' + kav.Id;
                knowledgeArticles.push(ka);
            })
            this.knowledgeArticles = knowledgeArticles;
            this.showSpinner = false;
        }
        if (result.error) {
            console.log(result.error);
            this.showSpinner = false;
        }
    }

    get emptyList() {
        return typeof this.knowledgeArticles === 'undefined' || this.knowledgeArticles.length === 0;
    }
}