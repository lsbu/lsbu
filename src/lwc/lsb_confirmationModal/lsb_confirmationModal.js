import {LightningElement, track, api} from 'lwc';

import ok from '@salesforce/label/c.LSB_OK';
import cancel from '@salesforce/label/c.LSB_Cancel';

export default class LsbConfirmationModal extends LightningElement {

    labels = {
        ok,
        cancel
    };

    @track isModalOpen = false;
    @track modalMessage = [];
    @track modalHeader;
    @track hideFooter;
    @track hideCancel;
    @track payload;

    submitLabel;
    cancelLabel;

    @api
    openModal({header, message, hideFooter, hideCancel, submitLabel, cancelLabel, payload}) {
        this.modalHeader = header;
        this.hideFooter = hideFooter;
        this.hideCancel = hideCancel;
        this.modalMessage = [];
        this.submitLabel = submitLabel;
        this.cancelLabel = cancelLabel;
        this.payload = payload;
        console.log('message >>>',message)
        if (typeof message !== "undefined") {
            if (Array.isArray(message)) {
                message.forEach(msg => {
                    this.modalMessage.push(msg);
                })
            } else {
                this.modalMessage.push(message);
            }
        }

        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
        const selectedEvent = new CustomEvent('submitmodal', {detail: false});
        this.dispatchEvent(selectedEvent);
    }

    submitDetails() {
        this.isModalOpen = false;
        let payload = true;
        if (this.payload) {
            payload = this.payload;
        }
        const selectedEvent = new CustomEvent('submitmodal', {detail: payload});
        this.dispatchEvent(selectedEvent);
    }

    get submitLabelText() {
        if (this.submitLabel) {
            return this.submitLabel;
        }
        return this.labels.ok
    }

    get cancelLabelText() {
        if (this.cancelLabel) {
            return this.cancelLabel;
        }
        return this.labels.cancel
    }
}