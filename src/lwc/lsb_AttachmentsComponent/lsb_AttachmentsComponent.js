import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';

import fetchContentVersions from '@salesforce/apex/LSB_AttachmentsController.getContentVersions';
import deleteAttachment from '@salesforce/apex/LSB_AttachmentsController.deleteAttachment';

import remove from '@salesforce/label/c.LSB_Remove';
import noAttachments from '@salesforce/label/c.LSB_NoAttachments';
import uploadFile from '@salesforce/label/c.LSB_UploadFile';
import attachmentsTitle from '@salesforce/label/c.LSB_Attachments';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';
import userId from '@salesforce/user/Id';

export default class LsbAttachmentsComponent extends NavigationMixin(LightningElement) {

    @api parentRecordId;
    @api acceptedFormats;
    @api noAttachmentsMessage;
    @api allowMultiple;
    @track contentVersions;

    componentReady;
    showSpinner = true;

    labels = {
        remove,
        noAttachments,
        uploadFile,
        attachmentsTitle
    };

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.parentRecordId && !this.componentReady) {
            this.componentReady = true;
            fetchContentVersions({
                parentRecordId: this.parentRecordId
            }).then(result => {
                this.contentVersions = result;
                this.contentVersions.forEach(contentVersion => {
                    contentVersion.Filename = contentVersion.Title + '.' + contentVersion.FileExtension;
                    contentVersion.fileUrl = this.getBaseUrl() + contentVersion.LSB_CVR_DownloadLink__c;
                    contentVersion.isDeletable = userId === contentVersion.CreatedById;
                })
            }).catch(error => {
                console.log('Error: ' + error);
            }).finally(() => {
                this.showSpinner = false;
            });
        }
    }

    deleteAttachment(event) {
        this.showSpinner = true;
        let attachmentIndex = event.currentTarget.dataset.index;
        deleteAttachment({
            documentId: this.contentVersions[attachmentIndex].ContentDocumentId
        }).then(result => {
            this.componentReady = false;
            this.renderedCallback();
        }).catch(error => {
            console.log(error);
        }).finally(() => {
            this.showSpinner = false;
        });
    }

    get attachmentsEmpty() {
        return typeof this.contentVersions === "undefined" || this.contentVersions.length === 0;
    }

    handleUploadFinished(event) {
        this.componentReady = false;
        this.renderedCallback();
    }

    get acceptedFormatsArray() {
        return this.acceptedFormats.split(',');
    }

    handleDownloadDocument(event) {
        let fileUrl = event.currentTarget.dataset.fileurl;
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: fileUrl
            },
        }, false);
    }

    getBaseUrl() {
        let baseUrl = 'https://' + location.host + '/';
        return baseUrl;
    }

}