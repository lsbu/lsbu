import { LightningElement,api } from 'lwc';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import getMyModulesNames from '@salesforce/apex/LSB_MyModules.getMyModulesNames';

export default class LsbMyModules extends LightningElement {
    @api header;
    @api description;
    dataReady = false;
    moduleNames;

    renderedCallback() {
        if (this.dataReady) {
            return;
        }
        this.dataReady = true;
        getMyModulesNames()
            .then(result => {
                this.moduleNames = result;
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }
}