import {LightningElement, track, api} from 'lwc';

import fetchCaseTeamMembers from '@salesforce/apex/LSB_CommunityMySuccessTeamController.fetchCaseTeamMembers';

import showMoreButtonLabel from '@salesforce/label/c.LSB_ShowMoreButtonLabel';
import showLessButtonLabel from '@salesforce/label/c.LSB_ShowLessButtonLabel';

const threeDots = '...';

export default class LsbCommunityMySuccessTeam extends LightningElement {

    @api maxStringLength = 50;

    @track members = [];
    @track dataReady;

    labels = {
        showLessButtonLabel,
        showMoreButtonLabel
    };

    renderedCallback() {
        if (this.dataReady) {
            return;
        }

        this.dataReady = true;
        fetchCaseTeamMembers()
            .then(result => {
                this.members = [];
                result.forEach((member) => {
                    member.email = 'mailto: ' + member.email;
                    if (member.aboutme && member.aboutme.length > this.maxStringLength) {
                        member.toggleAboutme = false;
                        member.fullAboutme = member.aboutme;
                        member.aboutme = this.shortenString(member.fullAboutme, this.maxStringLength) + threeDots;
                        member.showButton = true;
                    }
                    this.members.push(member);
                });
            })
            .catch(error => {
                console.log('Error: ' + error.body.message);
            });
    }

    handleToggleAboutme(event) {
        let member = this.members[event.target.dataset.index];
        member.toggleAboutme = !member.toggleAboutme;
        if (member.toggleAboutme) {
            member.aboutme = member.fullAboutme;
        } else {
            member.aboutme = this.shortenString(member.fullAboutme, this.maxStringLength) + threeDots;
        }
    }

    shortenString(text, max) {
        return text && text.length > max ? text.slice(0, max).split(' ').slice(0, -1).join(' ') : text
    }
}