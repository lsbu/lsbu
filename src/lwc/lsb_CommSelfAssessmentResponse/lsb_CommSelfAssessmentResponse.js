import {LightningElement, api, track} from 'lwc';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';

import FIND_OUT_MORE from '@salesforce/label/c.LSB_FindOutMore';
import {loadStyle} from "lightning/platformResourceLoader";

export default class LsbCommSelfAssessmentResponse extends LightningElement {

    labels = {
        FIND_OUT_MORE
    };

    @api assessmentResponse;
    @api isQuestionnaire = false;
    @api isMobile;

    arrowRight = 'mdi-chevron-right';
    arrowDown = 'mdi-chevron-down';

    showDescription;
    bodyClass = this.showDescription ? "show" : "hide";
    icon = this.showDescription ? this.arrowDown : this.arrowRight;



    connectedCallback() {
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (typeof this.showDescription === 'undefined') {
            if (this.isMobile) {
                this.showDescription = false;
            } else {
                this.showDescription = true;
            }
        }
    }

    handleClick(event) {
        let url = this.assessmentResponse.LSB_SAR_KnowledgeFullUrl__c;
        window.open(url, '_blank');
    }

    get iconClass() {
        return "mdi " + this.icon;
    }

    handleHeaderClick() {
        this.showDescription = !this.showDescription;
        this.icon = this.showDescription ? this.arrowDown : this.arrowRight;
    }

    get hidingClass() {
        this.bodyClass = this.showDescription ? "show" : "hide";
        return "response__body__" + this.bodyClass;
    }

    handleTextInputChange(event) {
        this.dispatchEvent(
            new CustomEvent('textchanged', {
                bubbles: true,
                composed: true,
                detail: {
                    recordId: event.currentTarget.dataset.index,
                    value: event.currentTarget.value
                }
            })
        );
    }

    handleRadioInputChange(event) {
        this.dispatchEvent(
            new CustomEvent('radiochanged', {
                bubbles: true,
                composed: true,
                detail: {
                    recordId: event.currentTarget.dataset.index,
                    choiceId: event.currentTarget.value,
                    answer: event.currentTarget.options.find(x => x.value === event.currentTarget.value).label
                }
            })
        );
    }
}