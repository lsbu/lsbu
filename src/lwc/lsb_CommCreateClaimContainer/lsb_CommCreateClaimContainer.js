import {LightningElement} from 'lwc';

import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';
import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';

import componentHeader from '@salesforce/label/c.LSB_ECContainerTitle';
import componentParagraph from '@salesforce/label/c.LSB_ECContainerParagraph';
import listItem1 from '@salesforce/label/c.LSB_ECContainerListItem1';
import listItem2 from '@salesforce/label/c.LSB_ECContainerListItem2';
import listItem3 from '@salesforce/label/c.LSB_ECContainerListItem3';
import submitRequest from '@salesforce/label/c.LSB_ECContainerSubmitRequest';

export default class LsbCommCreateClaimContainer extends LightningElement {

    labels = {
        componentHeader,
        componentParagraph,
        listItem1,
        listItem2,
        listItem3,
        submitRequest
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    handleClick() {
        this.template.querySelector('c-lsb_-comm-create-claim-modal')
            .openModal();
    }

    handlePress(event) {
        if (event.key === 'Enter') {
            this.handleClick();
        }
    }
}