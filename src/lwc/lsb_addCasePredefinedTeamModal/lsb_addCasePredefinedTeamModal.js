import {api, LightningElement} from 'lwc';

import fetchTeams from '@salesforce/apex/LSB_AddCasePredefinedTeamController.fetchTeams';
import addCaseTeam from '@salesforce/apex/LSB_AddCasePredefinedTeamController.addCaseTeam';
import {ShowToastEvent} from "lightning/platformShowToastEvent";

import ERROR_TOAST_TITLE from '@salesforce/label/c.LSB_GenericError';
import addTeamModalTitle from '@salesforce/label/c.LSB_CaseTeamAddTeamModalTitle';
import addTeamModalToast from '@salesforce/label/c.LSB_CaseTeamAddTeamModalToast';
import addTeamModalButtonSave from '@salesforce/label/c.LSB_CaseTeamAddTeamModalSave';
import addTeamModalButtonCancel from '@salesforce/label/c.LSB_CaseTeamAddTeamModalCancel';

const DUPLICATE_MESSAGE = 'The selected team is already added to the team.'

export default class LsbAddCasePredefinedTeamModal extends LightningElement {

    @api caseId;
    isModalOpen = false;
    errorMessage = '';
    _selectedTeam = '';
    _availableTeams = [];
    existingTeamMembers;

    labels = {
        ERROR_TOAST_TITLE,
        addTeamModalTitle,
        addTeamModalToast,
        addTeamModalButtonSave,
        addTeamModalButtonCancel
    };

    get selectedTeam() {
        return this._selectedTeam === '' ? this._availableTeams[0].value : this._selectedTeam;
    }

    renderedCallback() {
        if (this._availableTeams.length !== 0) {
            return;
        }

        fetchTeams()
             .then(result => {
                 result.forEach((team) => {
                     this._availableTeams.push({
                         label: team.Name,
                         value: team.Id
                     });
                 });
             })
             .catch(error => {
             });
    }

    handleChange(event) {
        this._selectedTeam = event.detail.value;
        this.errorMessage = '';
    }

    handleSubmit() {
        if (!this.teamMemberExist()) {
        addCaseTeam({ caseId : this.caseId, teamId : this.selectedTeam })
            .then(result => {
                if (result === '') {
                    this.showToast('success', this.labels.addTeamModalToast);
                    const selectedEvent = new CustomEvent('savemember', {detail: true});
                    this.dispatchEvent(selectedEvent);
                    this.closeModal();
                } else {
                    this.errorMessage = result;
                }
            })
            .catch(error => {
                this.showToast('error', this.labels.ERROR_TOAST_TITLE);
                this.closeModal();
            });
        } else {
            this.errorMessage = DUPLICATE_MESSAGE;
        }
    }

    teamMemberExist() {
        let exists = false;
        this.existingTeamMembers.forEach((member) => {
            if (this.selectedTeam === member.memberId) {

                exists = true;
            }
        });
        return exists;
    }

    @api
    openModal({ caseId, teamMembers}) {
        this.caseId = caseId;
        this.isModalOpen = true;
        this.existingTeamMembers = teamMembers;
    }

    closeModal() {
        this.errorMessage = '';
        this._selectedTeam = '';
        this.isModalOpen = false;
    }

    showToast(variant, title, message) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            }),
        );
    }
}