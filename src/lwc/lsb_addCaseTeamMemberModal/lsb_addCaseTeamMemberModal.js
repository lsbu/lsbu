import {api, LightningElement, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import fetchUsers from '@salesforce/apex/LSB_AddCaseTeamMemberController.fetchUsers';
import addCaseTeamMember from '@salesforce/apex/LSB_AddCaseTeamMemberController.addCaseTeamMember';
import fetchCaseRoles from '@salesforce/apex/LSB_AddCaseTeamMemberController.getCaseTeamRoles';
import checkIfRoleExists from '@salesforce/apex/LSB_AddCaseTeamMemberController.checkIfConfigExist';

import errorToastTitle from '@salesforce/label/c.LSB_GenericError';
import LOADING from '@salesforce/label/c.LSB_Loading';
import NO_RESULTS from '@salesforce/label/c.LSB_NoResults';
import modalTitle from '@salesforce/label/c.LSB_CaseTeamAddTeamMemberModalTitle';
import rolesMultipicklist from '@salesforce/label/c.LSB_CaseTeamSelectRolesMultipicklist';
import roleLabelInput from '@salesforce/label/c.LSB_CaseTeamCustomLabelInput';
import submitButton from '@salesforce/label/c.LSB_CaseTeamAddMemberModalButtonSubmit';
import savedMemberToastTitle from '@salesforce/label/c.LSB_CaseTeamToastTitleSavedMember';
import pendingMemberToastMessage from '@salesforce/label/c.LSB_CaseTeamToastMessagePendingMember';
import pendingMemberToastTitle from '@salesforce/label/c.LSB_CaseTeamToastTitlePendingMember';
import caseRoleInputLabel from '@salesforce/label/c.LSB_CaseTeamRoleInputLabel';

const SEARCH_DELAY = 350;
const SEPARATOR_CHART = '/'

export default class LsbAddCaseTeamMemberModal extends LightningElement {

    @api caseId;
    baseCaseTeamRoles;
    labels = {
        errorToastTitle,
        LOADING,
        NO_RESULTS,
        modalTitle,
        rolesMultipicklist,
        roleLabelInput,
        submitButton,
        savedMemberToastTitle,
        pendingMemberToastTitle,
        pendingMemberToastMessage,
        caseRoleInputLabel
    };
    caseTeamMembers = [];
    teamMemberName;
    teamMemberId;
    @track _activeTeamRoles = [];
    teamRolesOptions = [];
    @track _selectedTeamRoles = [];
    isModalOpen = false;
    ownerClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    iconFlagOwner = false;
    clearIconFlagOwner = false;
    ownersList = [];
    messageFlagOwner = false;
    iconUser = 'action:user';
    ownerLoadingText = false;
    showRoleInput = false;
    caseTeamRole;
    isRoleExist = false;
    isReady = false;

    get min() {
        return 1;
    }

    get max() {
        return 5;
    }

    get disabled() {
        return this.teamMemberId == null;
    }

    get activeTeamRoles() {
        return this._activeTeamRoles;
    }

    renderedCallback() {
        this.validateOnRendering();
    }

    @api
    openModal({caseId, teamMembers}) {
        this.caseId = caseId;
        this.caseTeamMembers = teamMembers;
        this.isModalOpen = true;
        if (!this.baseCaseTeamRoles) {
            this.loadRoles();
        } else {
            this.isReady = true;
        }
    }

    closeModal() {
        this.resetModal();
        this.isModalOpen = false;
    }

    resetModal() {
        this.caseTeamMembers = [];
        this.teamMemberName = null;
        this.teamMemberId = null;
        this._activeTeamRoles = [];
        this.teamRolesOptions = [];
        this._selectedTeamRoles = [];
        this.isModalOpen = false;
        this.iconFlagOwner = false;
        this.clearIconFlagOwner = false;
        this.ownersList = [];
        this.messageFlagOwner = false;
        this.ownerLoadingText = false;
        this.showRoleInput = false;
        this.caseTeamRole = null;
        this.isRoleExist = false;
        this.isReady = false;
        this.baseCaseTeamRoles = null;
    }

    loadRoles() {
        fetchCaseRoles()
            .then((result) => {
            if (result) {
                this.baseCaseTeamRoles = result;
                this.setTeamRolesOptions();
            } else {
                this.isReady = true;
            }
        }).catch((error) => {
            console.log('error:', JSON.stringify(error))
            this.isReady = true;
        });
    }

    setTeamRolesOptions() {
        const items = [];
        this.baseCaseTeamRoles.forEach((role) => {
            items.push({
                label: role.label,
                value: role.name,
            });
        });

        this.teamRolesOptions.push(...items);
        this.isReady = true;
    }

    validateOnRendering() {
        if (this.caseTeamRole) {
            let field = this.template.querySelector(`[data-id="roleInput"]`);
            this.validateRoleInputField(this.caseTeamRole.label, field);
        }
    }

    handleSearch(event) {
        let teamMemberName = event.target.value;
        if (teamMemberName.length > 1) {
            window.clearTimeout(this.delayTimeout);
            this.delayTimeout = setTimeout(() => {
                this.searchUsers(teamMemberName);
            }, SEARCH_DELAY);
        }
    }

    searchUsers(teamMemberName) {
        this.ownerLoadingText = true;
        fetchUsers({userName: teamMemberName})
            .then(result => {
                this.ownersList = result;
                this.ownerLoadingText = false;

                this.ownerClass = result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
                if (teamMemberName.length > 0 && result.length === 0) {
                    this.messageFlagOwner = true;
                } else {
                    this.messageFlagOwner = false;
                }

                if (this.teamMemberId) {
                    this.iconFlagOwner = false;
                    this.clearIconFlagOwner = true;
                } else {
                    this.iconFlagOwner = true;
                    this.clearIconFlagOwner = false;
                }
            })
            .catch(error => {
                console.log('error >>>', JSON.stringify(error))
                this.showToast('error', 'Error', error);
            });
    }

    resetOwner() {
        this.teamMemberName = null;
        this.teamMemberId = null;
        this.resetPOwnerFlags ();
    }

    resetPOwnerFlags() {
        this.iconFlagOwner = true;
        this.clearIconFlagOwner = false;
    }

    setSearchOwner(event) {
        this.teamMemberId = event.currentTarget.dataset.id;
        this.teamMemberName = event.currentTarget.dataset.name;
        this.caseTeamMembers.forEach((member) => {
            if (member.memberId == this.teamMemberId && member.roleUniqueName != null) {
                let currentRoles = [];
                if (member.roleUniqueName.includes(SEPARATOR_CHART)) {
                    currentRoles = member.roleUniqueName.split(SEPARATOR_CHART);
                } else {
                    currentRoles.push(member.roleUniqueName);
                }
                this.baseCaseTeamRoles.forEach((role) => {
                    if (currentRoles.indexOf(role.name) != -1) {
                        this._activeTeamRoles.push(role.name);
                    }
                });
            }
        });
        this.iconFlagOwner = false;
        this.clearIconFlagOwner = true;
        this.ownerClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    }

    handleChangeSelectRoles(event) {
        this._selectedTeamRoles = event.detail.value;
        this.caseTeamRole = null;
        this.showRoleInput = false;
        this.caseTeamRole = this.setRoleObject();
        this.checkIfRoleConfigExist();

    }

    checkIfRoleConfigExist() {
        if(this.caseTeamRole.name) {
            checkIfRoleExists({roleName: this.caseTeamRole.name})
                .then((result) => {
                    this.isRoleExist = result;
                }).catch((error) => {
                console.log('error:', JSON.stringify(error))
            });
        }
    }

    handleSubmit() {
        if (this.isRoleValid) {
            this.saveTeamMember(JSON.stringify(this.caseTeamRole));
        } else {
            this.showRoleInput = true;
            let field = this.template.querySelector(`[data-id="roleInput"]`);
            this.validateRoleInputField(this.caseTeamRole.label, field);
        }
    }

    saveTeamMember(roleObject) {
        addCaseTeamMember({
            caseId: this.caseId,
            memberId: this.teamMemberId,
            roleJSON: roleObject
        }).then((result) => {
            if (result) {
                this.showToast('success', this.labels.savedMemberToastTitle);
            } else {
                this.showToast('success', this.labels.pendingMemberToastTitle, this.labels.pendingMemberToastMessage);
            }
            const selectedEvent = new CustomEvent('savemember', {detail: result});
            this.dispatchEvent(selectedEvent);
            this.closeModal();
        }).catch((error) => {
            console.log('error:', JSON.stringify(error))
            this.showToast('error', this.labels.errorToastTitle);
            this.closeModal();
        });
    }

    setRoleObject() {
        let roleLabel = '';
        let roleName = '';
        if (this.baseCaseTeamRoles.length > 1) {
            this.baseCaseTeamRoles.forEach((role) => {
                if (this._selectedTeamRoles.indexOf(role.name) != -1) {
                    roleLabel += role.label + ` ${SEPARATOR_CHART} `;
                    roleName += role.name + SEPARATOR_CHART;
                }
            });
        } else {
            roleLabel = this.baseCaseTeamRoles[0].label;
            roleName = this.baseCaseTeamRoles[0].name;
        }
        if (this._selectedTeamRoles.length > 0) {
            roleLabel = roleLabel.slice(0, -2);
            roleName = roleName.slice(0, -1);
        }
        roleLabel = roleLabel.trim();
        roleName = roleName.trim();
        let roleObject = {
            label : roleLabel,
            name : roleName
        }
        return roleObject;
    }

    get isRoleValid() {
        if (this.isRoleExist) {
            return true;
        } else {
            if (this.caseTeamRole) {
                if (this.caseTeamRole.label.length < 80) {
                    return true;
                }
            }
            return false;
        }
    }

    get showRoleInput() {
        return this.showRoleInput;
    }

    onRoleInputChange(event) {
        let inputValue = event.target.value;
        let field = this.template.querySelector(`[data-id="roleInput"]`);
        this.caseTeamRole.label = inputValue;
        this.validateRoleInputField(inputValue, field);
    }

    validateRoleInputField(inputValue, field) {
        if (!(inputValue.length < 80)) {
            field.setCustomValidity(this.labels.caseRoleInputLabel);
            field.reportValidity();
            return false;
        } else {
            field.setCustomValidity("");
            field.reportValidity();
        }
    }

    get caseTeamRoleLabel() {
        if (this.caseTeamRole) {
            return this.caseTeamRole.label;
        }
        return '';
    }

    showToast(variant, title, message) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            }),
        );
    }
}