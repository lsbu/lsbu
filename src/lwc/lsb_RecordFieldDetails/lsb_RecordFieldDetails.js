/**
 * Created by ociszek001 on 04.03.2021.
 */

import {LightningElement, api} from 'lwc';
import fetchRecord from '@salesforce/apex/LSB_RecordFieldDetailsController.fetchRecord';

export default class LsbRecordFieldDetails extends LightningElement {
    componentReady;
    @api objectApiName;
    @api contactFieldApiName;
    @api fieldSetApiName;
    @api statusField;
    @api recordId;

    disconnectedCallback() {
        this.componentReady = false;
    }

    renderedCallback() {
        if (this.componentReady) {
            return;
        }
        fetchRecord({objectApiName: this.objectApiName, contactFieldApiName: this.contactFieldApiName}).then(result => {
            if (result !== null) {
                this.recordId = result;
                this.componentReady = true;
            }
        }).catch(error => {
            console.log('Error: ' + error.body.message);
        });
    }

}