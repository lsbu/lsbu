/**
 * Created by pslowinski001 on 15.06.2021.
 */

import {api, LightningElement} from 'lwc';
import communityBanners from '@salesforce/contentAssetUrl/communityTileszip';

export default class LsbCommunityImageContainer extends LightningElement {

    @api bannerUrl;
    @api width;
    @api height;

    get banner() {
        return communityBanners + 'pathinarchive=images/' + this.bannerUrl;
    }

    get imageStyle() {
        return `width:${this.width}px;`;
    }
}