import {LightningElement, track, api, wire} from 'lwc';

import fetchAdvisors from '@salesforce/apex/LSB_AdvisorBiosController.fetchAdvisors';

import materialDesignIcons from '@salesforce/resourceUrl/materialDesignIcons';
import communityStyles from '@salesforce/resourceUrl/communityStyles';
import {loadStyle} from 'lightning/platformResourceLoader';

import {publish, MessageContext} from 'lightning/messageService';
import LSB_HAS_ADVISORS_MESSAGE from '@salesforce/messageChannel/LSB_HasAdvisors__c';

import modules from '@salesforce/label/c.LSB_Modules';

export default class LsbAdvisorBios extends LightningElement {

    @api advisorsFrom;
    @api groupName;
    @api header;
    @api description;

    @track advisors = [];
    componentReady;
    @wire(MessageContext) messageContext;

    isTeachingStaff;
    showContent;

    labels = {
        modules
    }

    connectedCallback() {
        loadStyle(this, communityStyles);
        loadStyle(this, materialDesignIcons + '/materialDesignIcons/css/materialdesignicons.css');
    }

    renderedCallback() {
        if (this.componentReady) {
            return;
        }
        this.componentReady = true;
        this.isTeachingStaff = this.advisorsFrom === 'Teaching Staff';
        fetchAdvisors({
            advisorsFrom: this.advisorsFrom,
            groupName: this.groupName
        })
            .then(result => {
                this.advisors = [];
                result.forEach((advisor) => {
                    let advisorObj = {...advisor};
                    advisorObj.styleClass = 'advisor advisor__expanded';
                    this.advisors.push(advisorObj);
                });
                const message = {
                    hasAdvisors: !(this.advisors.length === 0)
                };
                publish(this.messageContext, LSB_HAS_ADVISORS_MESSAGE, message);
                this.showContent = typeof this.description !== "undefined" || this.advisors.length;
            })
            .catch(error => {
                console.log(error);
            });
    }

    toggleExpand(event) {
        let advisor = this.advisors[event.currentTarget.dataset.index];
        advisor.isExpanded = !advisor.isExpanded;
        advisor.styleClass = advisor.isExpanded ? 'advisor advisor__expanded' : 'advisor advisor__collapsed';
    }

}