trigger LSB_SupportArrangementTrigger on LSB_SUT_SupportArrangement__c (after insert) {
    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.new, Trigger.old, Schema.SObjectType.LSB_SUT_SupportArrangement__c);
}