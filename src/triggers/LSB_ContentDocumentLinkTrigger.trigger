trigger LSB_ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.new, Trigger.old, Schema.SObjectType.ContentDocumentLink);
}