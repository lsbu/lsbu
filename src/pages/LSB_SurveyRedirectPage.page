<apex:page id="LSB_SurveyRedirectPage" controller="LSB_SurveyRedirectController" lightningStylesheets="true"
           showHeader="false" sidebar="false" docType="html-5.0">

    <style>

        html, body {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            -webkit-appearance: none;
            height: 100%;
        }

        .dialogState {
            display: flex;
            height: 100%;
            padding: 0 20px;
        }

        .dialogState__form {
            display: flex;
            flex-direction: column;
            width: 100%;
        }

        .dialogState__text {
            align-items: center;
            display: flex;
            flex-direction: column;
            justify-content: center;
            height: 100%;
        }

        .dialogState__buttons {
            display: flex;
            flex-direction: column;
            margin-top: auto;
            width: 100%;
        }

        .dialogState__button {
            background: none !important;
            background-color: #901F82 !important;
            border: none !important;
            color: white !important;
            font-weight: bold !important;
            height: 44px;
            margin: 0 0 8px 0 !important;
        }

        .dialogState__button:hover {
            filter:brightness(1.15);
        }

        .dialogState__button:focus {
            box-shadow: none !important;
        }

        .dialogState__button:focus-visible {
            background-color: #F3E62C !important;
            color: #000000 !important;
        }

        .dialogState__header {
            color: #323232;
            font-family: Arial, Helvetica, sans-serif !important;
            font-size: 16px !important;
            font-weight: bold !important;
            letter-spacing: 0.13px;
            line-height: 24px;
            margin-bottom: 16px !important;
            text-align: center;
        }

        .dialogState__descp {
            color: #646464;
            font-family: Arial, Helvetica, sans-serif !important;;
            font-size: 13px !important;;
            letter-spacing: 0.1px;
            line-height: 22px;
            text-align: center;
        }

    </style>

    <apex:slds id="slds">
        <div class="dialogState">
        <apex:form styleClass="dialogState__form" id="postForm">
            <apex:actionFunction action="{!getTranscriptFromFile}" name="callSaveTranscriptFile" rerender="postForm">
            </apex:actionFunction>
                <div class="dialogState__text" id="createDoctorFormGrid">
                    <h3 class="dialogState__header" id="dialogText">
                        {!ThankYouMessage}
                    </h3>
                    <p class="dialogState__descp" id="dialogText">
                            <apex:outputText rendered="{!showFeedback}" value="{!$Label.LSB_ChatFeedbackRequest}"/>
                    </p>
                </div>
                <div class="dialogState__buttons">
                    <apex:commandButton styleClass="dialogState__button"
                                        action="{!redirectToSurvey}"
                                        value="{!$Label.LSB_ChatGiveFeedback}" status="pageStatus"
                                        rerender="postForm, dialogText" id="searchButton" rendered="{!showFeedback}"/>
                    <apex:commandButton styleClass="dialogState__button"
                                        onClick="handleDownloadTranscript({!chatTranscript}, {!isCommunity}); return false;"
                                        value="{!$Label.LSB_ChatSaveTranscript}" status="pageStatus"
                                        rerender="postForm, dialogText" id="saveTranscriptButton"/>
                </div>
        </apex:form>
        </div>
    </apex:slds>

    <script type="text/javascript">

        function handleDownloadTranscript(transcript, isCommunity) {
            if (isCommunity) {
                callSaveTranscriptFile();
            } else {
                downloadTranscriptInJS(transcript);
            }
        }

        function downloadTranscriptInJS(transcript) {
            if (transcript) {
                let transcriptMessage = transcript;
                let downloadElement = document.createElement('a');
                downloadElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(transcriptMessage);
                downloadElement.target = '_self';
                downloadElement.download = 'transcript.txt';
                document.body.appendChild(downloadElement);
                downloadElement.click();
                document.body.removeChild(downloadElement);
            }
        }

    </script>

</apex:page>