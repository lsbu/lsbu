<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Retrospective</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExamOptions__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDaysOptions__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_DetailsFieldDescription__c</field>
        <value xsi:type="xsd:string">We are unable to accept ECs claims submitted after a deadline has passed, unless there is a valid reason which prevented you from being able to notify us before the deadline. Please explain here why you were unable to submit your ECs claim before your deadline.</value>
    </values>
    <values>
        <field>LSB_DetailsMandatory__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">This means that you will need to submit documentary evidence to support your ECs claim, which will be reviewed by the ECs Team. In addition to providing evidence to support the ECs claim, you are also required to provide an explanation as to why you did not notify the university about your ECs before the deadline passed. Please be aware that unless you offer a valid reason for late submission of this claim, your claim will not be supported.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_ScreenTitle__c</field>
        <value xsi:type="xsd:string">You have selected an assessment with a deadline that has already passed.</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_ShowAbleSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_ShowAbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_SingleSelectFieldDescription__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
