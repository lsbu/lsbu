<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lsbu Website</label>
    <protected>false</protected>
    <values>
        <field>LSB_WebsiteUrl__c</field>
        <value xsi:type="xsd:string">www.lsbu.ac.uk/</value>
    </values>
</CustomMetadata>
