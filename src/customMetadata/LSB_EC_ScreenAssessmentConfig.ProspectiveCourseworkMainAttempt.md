<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prospective Coursework Main Attempt</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExamOptions__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDaysOptions__c</field>
        <value xsi:type="xsd:string">Yes;No, I need to submit his coursework in the resit period</value>
    </values>
    <values>
        <field>LSB_DetailsFieldDescription__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_DetailsMandatory__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">This means that you will be able to choose whether you want to submit your coursework within 5 working days of the deadline for an uncapped mark, or whether you think you will need to submit during the resit period for an uncapped mark. Please note that you are only able to select one of these options.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_ScreenTitle__c</field>
        <value xsi:type="xsd:string">You have selected a coursework assessment, and the deadline for this has not yet passed.</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_ShowAbleSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_ShowAbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_SingleSelectFieldDescription__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
