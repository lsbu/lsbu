<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Program Enrolment</label>
    <protected>false</protected>
    <values>
        <field>LSB_FlowsOn__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_ValidationsOn__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_WorkflowsOn__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
