<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Late request</label>
    <protected>false</protected>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">Late submission request</value>
    </values>
    <values>
        <field>LSB_RequestTypeDescription__c</field>
        <value xsi:type="xsd:string">You think you are going to be unable to meet a coursework deadline, but do not have a valid extenuating circumstance. In this situation, notifying us in advance will mean you can submit within 5 working days, and your mark will be capped less than if you simply submit late without notifying us. Late submissions which are not notified in advance will be capped at a pass mark.</value>
    </values>
    <values>
        <field>LSB_RequestTypeLabel__c</field>
        <value xsi:type="xsd:string">I need to request additional time to submit an upcoming piece of coursework, but do not have a valid extenuating circumstance</value>
    </values>
    <values>
        <field>LSB_SignpostingFirstPart__c</field>
        <value xsi:type="xsd:string">If you think you are going to miss your coursework submission deadline, you can notify us here before the deadline passes. By doing this, you will be able to submit your work up to 5 working days past the deadline, and your mark will be reduced by 5% for each working day past the deadline. Importantly, this reduction may be far less than if you do not submit this notification. Work submitted late without submission of this form will be capped at a pass mark.</value>
    </values>
    <values>
        <field>LSB_SignpostingSecondPart__c</field>
        <value xsi:type="xsd:string">While it is important, when there are not any factors outside of your control impacting on your studies, that you adhere to deadlines, we also think it is important that you feel you can let us know about difficulties, even if these are to do with your own time management, or if you are finding your course more difficult than you expected. This is the reason why, when you tell us that you are going to miss a deadline, the penalties for this are less severe than if you do not.</value>
    </values>
    <values>
        <field>LSB_SortOrder__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
