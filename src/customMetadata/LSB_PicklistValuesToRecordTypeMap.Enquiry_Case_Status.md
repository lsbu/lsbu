<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Enquiry Case Status</label>
    <protected>false</protected>
    <values>
        <field>AvailablePicklistValues__c</field>
        <value xsi:type="xsd:string">Received
Being Reviewed
Responded
Closed</value>
    </values>
    <values>
        <field>ObjectAPIName__c</field>
        <value xsi:type="xsd:string">Case</value>
    </values>
    <values>
        <field>PicklistAPIName__c</field>
        <value xsi:type="xsd:string">Status</value>
    </values>
    <values>
        <field>RecordTypeDeveloperName__c</field>
        <value xsi:type="xsd:string">LSB_CAS_Enquiry</value>
    </values>
    <values>
        <field>StatusDefinitions__c</field>
        <value xsi:type="xsd:string">Received
Your enquiry has been received by LSBU and a member of the team will get back soon. We aim to respond within 2 working days although at peak times it may take slightly longer.

Being Reviewed
A member of the LSBU Team is currently reviewing your enquiry and will be in touch soon. 

Input Required
A member of the LSBU Team has responded to your enquiry, this will be direct to your My Account and also via email. If the enquiry is &apos;Input Required&apos; it usually means that they have requested more information and/or have tried to contact you to discuss further.

Closed
Your enquiry has been resolved and we have closed it. If you would like to discuss further you can re-open the enquiry by simply replying to the last email from us.</value>
    </values>
</CustomMetadata>
