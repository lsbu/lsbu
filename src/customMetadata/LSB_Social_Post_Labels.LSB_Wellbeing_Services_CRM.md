<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mental Health and Wellbeing</label>
    <protected>false</protected>
    <values>
        <field>LSB_Current_Role__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_Picklist_Value__c</field>
        <value xsi:type="xsd:string">Wellbeing Services</value>
    </values>
    <values>
        <field>LSB_Social_Studio_Label__c</field>
        <value xsi:type="xsd:string">Mental Health and Wellbeing</value>
    </values>
</CustomMetadata>
