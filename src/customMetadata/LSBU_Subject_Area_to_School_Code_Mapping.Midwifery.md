<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Midwifery</label>
    <protected>false</protected>
    <values>
        <field>SchoolCode__c</field>
        <value xsi:type="xsd:string">HAN</value>
    </values>
</CustomMetadata>
