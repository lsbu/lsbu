<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SECTION - Careers, employability</label>
    <protected>false</protected>
    <values>
        <field>LSB_BackgroundColor__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_BorderColor__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_Filled__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IconName__c</field>
        <value xsi:type="xsd:string">mdi-briefcase-clock</value>
    </values>
    <values>
        <field>LSB_IsSectionConfig__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_PointBackgroundColor__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_PointBorderColor__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_PointHoverBackgroundColor__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_PointHoverBorderColor__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_SectionLabelName__c</field>
        <value xsi:type="xsd:string">Careers, employability and enterprise</value>
    </values>
    <values>
        <field>LSB_SortOrder__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
