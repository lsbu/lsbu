<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>VALID_TYPE_FOR_SUPPORT_ARRANGEMENT_SHARE</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:type="xsd:string">Separated by ;</value>
    </values>
    <values>
        <field>LSB_Value__c</field>
        <value xsi:type="xsd:string">Personal Tutor;Course Director;Module Leader;Supervisor;Academic Integrity Coordinator;Head of Division;DESE;Course Teaching Staff</value>
    </values>
</CustomMetadata>
