<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Late Coursework Prospective Supported</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">Your late submission request has been approved. We understand it may not be possible for you to submit your assessment as per the existing deadline, however, please aim to submit this as soon as possible and within 5 working days of the existing assessment deadline to avoid being penalised.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsThirdOrMoreSubmission__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_RejectionReason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_RequestStatus__c</field>
        <value xsi:type="xsd:string">Request Supported</value>
    </values>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">Late submission request</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:type="xsd:string">Please note that your mark will be reduced by 5% for each working day past the deadline until you reach your 5 working day extension limit, after which it will be capped at pass mark.</value>
    </values>
    <values>
        <field>LSB_ThirdParagraph__c</field>
        <value xsi:type="xsd:string">If your current situation has an impact on your wellbeing, please reach out to the Mental Health and Wellbeing team for further support.</value>
    </values>
</CustomMetadata>
