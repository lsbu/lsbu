<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Get what you need</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:type="xsd:string">Access your personal development plan, tailor-made for you based on what you tell us.  You can also find your personal development plan responses to come back to and update at any time.</value>
    </values>
    <values>
        <field>LSB_Icon__c</field>
        <value xsi:type="xsd:string">mdi-shield-star</value>
    </values>
    <values>
        <field>LSB_Is_Visible_To_Offer_Holder__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_Is_Visible_To_Student__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_Link__c</field>
        <value xsi:type="xsd:string">/s/self-assessment</value>
    </values>
    <values>
        <field>LSB_Number__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>LSB_Title__c</field>
        <value xsi:type="xsd:string">Get what you need: personal development</value>
    </values>
</CustomMetadata>
