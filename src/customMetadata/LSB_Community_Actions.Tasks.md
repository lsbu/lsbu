<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Tasks</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:type="xsd:string">Access all the tasks assigned to you by staff and from the Get what you need: personal development plan survey.</value>
    </values>
    <values>
        <field>LSB_Icon__c</field>
        <value xsi:type="xsd:string">mdi-format-list-checks</value>
    </values>
    <values>
        <field>LSB_Is_Visible_To_Offer_Holder__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_Is_Visible_To_Student__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_Link__c</field>
        <value xsi:type="xsd:string">/s/my-tasks</value>
    </values>
    <values>
        <field>LSB_Number__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>LSB_Title__c</field>
        <value xsi:type="xsd:string">Tasks</value>
    </values>
</CustomMetadata>
