<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Your Team</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:type="xsd:string">Find a list of the teams and advisors that you can book an appointment with, including how they can support you.</value>
    </values>
    <values>
        <field>LSB_Icon__c</field>
        <value xsi:type="xsd:string">mdi-school</value>
    </values>
    <values>
        <field>LSB_Is_Visible_To_Offer_Holder__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_Is_Visible_To_Student__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_Link__c</field>
        <value xsi:type="xsd:string">/s/my-support/your-team</value>
    </values>
    <values>
        <field>LSB_Number__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>LSB_Title__c</field>
        <value xsi:type="xsd:string">Your team</value>
    </values>
</CustomMetadata>
