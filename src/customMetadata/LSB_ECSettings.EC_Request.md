<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EC Request</label>
    <protected>false</protected>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">Extenuating circumstances claim</value>
    </values>
    <values>
        <field>LSB_RequestTypeDescription__c</field>
        <value xsi:type="xsd:string">You are experiencing an unforeseen problem, which was outside of your control and is impacting on your studies. You can use this form to submit an extenuating circumstances claim.</value>
    </values>
    <values>
        <field>LSB_RequestTypeLabel__c</field>
        <value xsi:type="xsd:string">I have an extenuating circumstance which has affected an assessment</value>
    </values>
    <values>
        <field>LSB_SignpostingFirstPart__c</field>
        <value xsi:type="xsd:string">Extenuating Circumstances (ECs) are any type of unforeseen situation, which you could not have avoided, that impacts on your studies. When you submit an ECs claim, if you submit before your deadline, we will automatically approve your claim without requesting any supporting evidence. If you submit three or more ECs claims in a 4 week period, or if you submit an ECs claim after your deadline has passed, you will then be asked to submit evidence to support your claim, which will be manually reviewed by our ECs Team.</value>
    </values>
    <values>
        <field>LSB_SignpostingSecondPart__c</field>
        <value xsi:type="xsd:string">A supported ECs claim offers different types of support, depending on the type of assessment, and whether your claim relates to a main or a resit assessment. This is explained in the form when you select the relevant assessment, but the type of support may include: being allowed to submit within 5 working days for an uncapped mark, or being allowed to submit your work or sit your exam during the resit period without your mark being capped.</value>
    </values>
    <values>
        <field>LSB_SortOrder__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
