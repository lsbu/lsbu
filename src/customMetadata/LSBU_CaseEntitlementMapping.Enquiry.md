<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Enquiry</label>
    <protected>false</protected>
    <values>
        <field>LSB_CaseRecordType__c</field>
        <value xsi:type="xsd:string">LSB_CAS_Enquiry</value>
    </values>
    <values>
        <field>LSB_EntitlementName__c</field>
        <value xsi:type="xsd:string">LSBU Entitlement</value>
    </values>
</CustomMetadata>
