<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DDS</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">Your request has been approved. Please submit your assessment within 5 working days of the deadline.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsThirdOrMoreSubmission__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_RejectionReason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_RequestStatus__c</field>
        <value xsi:type="xsd:string">Request Supported</value>
    </values>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">DDS late submission request</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:type="xsd:string">We would like to ensure that your DDS support arrangements enable you to meet deadlines most of the time, and so if you feel that it would be helpful to review your support arrangements, please visit MyAccount</value>
    </values>
    <values>
        <field>LSB_ThirdParagraph__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
