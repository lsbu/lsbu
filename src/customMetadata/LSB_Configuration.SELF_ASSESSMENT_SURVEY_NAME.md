<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SELF ASSESSMENT SURVEY NAME</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:type="xsd:string">Denotes Developer Name of the Self Assessment Survey. Responses from this survey will be creating/updating Self Assessment Response records  (see LEAP-5218)</value>
    </values>
    <values>
        <field>LSB_Value__c</field>
        <value xsi:type="xsd:string">self_assessment_survey</value>
    </values>
</CustomMetadata>
