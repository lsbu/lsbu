<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prospective Exam Main Attempt</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExamOptions__c</field>
        <value xsi:type="xsd:string">Yes;No, I need to attempt the exam in the resit period</value>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDaysOptions__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_DetailsFieldDescription__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_DetailsMandatory__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">This means that you will have the option of sitting the exam on the current advertised date, or sitting the exam during the resit period for an uncapped mark.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_ScreenTitle__c</field>
        <value xsi:type="xsd:string">You have selected an exam, and the deadline for this has not yet passed.</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:type="xsd:string">If you are not sure whether you should attempt the exam now, we would encourage you to speak to your personal tutor to discuss what is best for you.</value>
    </values>
    <values>
        <field>LSB_ShowAbleSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_ShowAbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_SingleSelectFieldDescription__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
