<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EC Exam Prosp No Resit Supported</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">Your extenuating circumstances claim has been approved. As you have indicated that you will not be to sit the exam during the resit period, you may not able to complete this module in the current academic year. The final decision on your mark will be made by your Exam Board.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsThirdOrMoreSubmission__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_RejectionReason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_RequestStatus__c</field>
        <value xsi:type="xsd:string">Request Supported</value>
    </values>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">Extenuating circumstances claim</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_ThirdParagraph__c</field>
        <value xsi:type="xsd:string">If your current situation has an impact on your wellbeing, please reach out to the Mental Health and Wellbeing team for further support.</value>
    </values>
</CustomMetadata>
