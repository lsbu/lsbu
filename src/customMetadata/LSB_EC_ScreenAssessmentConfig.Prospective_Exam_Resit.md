<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prospective Exam Resit</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExamOptions__c</field>
        <value xsi:type="xsd:string">Yes;No</value>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDaysOptions__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_DetailsFieldDescription__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_DetailsMandatory__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">Because this is a resit attempt, you are strongly encouraged to attempt to sit the exam.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_ScreenTitle__c</field>
        <value xsi:type="xsd:string">You have selected an exam, and the deadline for this has not yet passed.</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_ShowAbleSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_ShowAbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_SingleSelectFieldDescription__c</field>
        <value xsi:type="xsd:string">If you are unable to sit your exam, you can still submit your ECs claim, but you may not be able to complete this module in the current academic year. This decision will be made by your Exam Board.</value>
    </values>
</CustomMetadata>
