<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GMT</label>
    <protected>false</protected>
    <values>
        <field>LSB_TZM_TimeZoneAbbreviation__c</field>
        <value xsi:type="xsd:string">GMT</value>
    </values>
    <values>
        <field>LSB_TZM_TimeZoneOffset__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
</CustomMetadata>
