<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>StrengthsAndAreasTaskDescription</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_Value__c</field>
        <value xsi:type="xsd:string">Please go to your ‘‘Get what you need: personal development plan’ section located under your My Support tab to review and update your Strengths and areas for development section.</value>
    </values>
</CustomMetadata>
