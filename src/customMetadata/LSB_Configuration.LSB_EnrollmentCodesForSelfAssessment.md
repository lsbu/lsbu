<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Enrollment Codes For Self Assessment</label>
    <protected>false</protected>
    <values>
        <field>LSB_Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_Value__c</field>
        <value xsi:type="xsd:string">AACPT,AUF,AUI,ACF,ACI</value>
    </values>
</CustomMetadata>
