<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EC Coursework Retrospective</label>
    <protected>false</protected>
    <values>
        <field>LSB_AbleToAttendExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_AbleToSubmit5WorkingDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_FirstParagraph__c</field>
        <value xsi:type="xsd:string">Your claim has been received by the extenuating circumstances team who will be in touch shortly to further progress your claim. Please make sure you attach any evidence to support your claim using the &quot;add files&quot; button below.</value>
    </values>
    <values>
        <field>LSB_IsExam__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsResit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_IsRetrospective__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LSB_IsThirdOrMoreSubmission__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LSB_RejectionReason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_RequestStatus__c</field>
        <value xsi:type="xsd:string">Request Pending</value>
    </values>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">Extenuating circumstances claim</value>
    </values>
    <values>
        <field>LSB_SecondParagraph__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>LSB_ThirdParagraph__c</field>
        <value xsi:type="xsd:string">If your current situation has an impact on your wellbeing, please reach out to the Mental Health and Wellbeing team for further support.</value>
    </values>
</CustomMetadata>
