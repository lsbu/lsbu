<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EC Claim Case</label>
    <protected>false</protected>
    <values>
        <field>AvailablePicklistValues__c</field>
        <value xsi:type="xsd:string">Received
Being Reviewed
Closed</value>
    </values>
    <values>
        <field>ObjectAPIName__c</field>
        <value xsi:type="xsd:string">Case</value>
    </values>
    <values>
        <field>PicklistAPIName__c</field>
        <value xsi:type="xsd:string">Status</value>
    </values>
    <values>
        <field>RecordTypeDeveloperName__c</field>
        <value xsi:type="xsd:string">LSB_CAS_ECClaim</value>
    </values>
    <values>
        <field>StatusDefinitions__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
