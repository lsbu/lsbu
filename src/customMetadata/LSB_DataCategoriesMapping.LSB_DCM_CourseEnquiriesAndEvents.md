<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Course Enquiries and Events</label>
    <protected>false</protected>
    <values>
        <field>LSB_DataCategoryApiName__c</field>
        <value xsi:type="xsd:string">Course</value>
    </values>
    <values>
        <field>LSB_TopicValuesApiName__c</field>
        <value xsi:type="xsd:string">CourseEnquiriesAndEvents</value>
    </values>
</CustomMetadata>
