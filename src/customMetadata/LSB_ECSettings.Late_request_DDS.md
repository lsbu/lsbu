<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Late request DDS</label>
    <protected>false</protected>
    <values>
        <field>LSB_RequestTypeApiName__c</field>
        <value xsi:type="xsd:string">DDS late submission request</value>
    </values>
    <values>
        <field>LSB_RequestTypeDescription__c</field>
        <value xsi:type="xsd:string">You have DDS support arrangements which allow you to submit your work up to 5 working days late for an uncapped mark. Submission of the form will automatically approve this for you.</value>
    </values>
    <values>
        <field>LSB_RequestTypeLabel__c</field>
        <value xsi:type="xsd:string">I need to request the opportunity to submit my work late for an uncapped mark as part of my DDS support arrangements</value>
    </values>
    <values>
        <field>LSB_SignpostingFirstPart__c</field>
        <value xsi:type="xsd:string">Your DDS support arrangements provide you with the option of submitting coursework up to 5 working days after the deadline for an uncapped mark. These requests will be automatically approved on submission of this form.</value>
    </values>
    <values>
        <field>LSB_SignpostingSecondPart__c</field>
        <value xsi:type="xsd:string">While you are able to submit these requests for any coursework, we do aim to provide other forms of support to help you submit by the deadline most of the time. If you feel that the wider support we have put in place is not helping you to meet deadlines, we would encourage you to speak with a DDS advisor to review your support arrangements.</value>
    </values>
    <values>
        <field>LSB_SortOrder__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
