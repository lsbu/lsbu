@isTest
private class LSB_ScoringModelTest {

    private static final String PROSPECT_EMAIL_WITH_OPT_OUT_CONSENTS = 'wilhelm.grimm@lsbu.test.com';

    @TestSetup
    private static void createdData() {
        hed__Affl_Mappings__c affMapping = new hed__Affl_Mappings__c();
        affMapping.hed__Account_Record_Type__c = 'Course';
        affMapping.hed__Auto_Program_Enrollment_Role__c ='Student';
        affMapping.hed__Auto_Program_Enrollment_Status__c = 'Current';
        affMapping.hed__Primary_Affl_Field__c = 'Primary Course';
        affMapping.Name ='Academic Program';
        insert affMapping;

        Account a = new Account(Name='Test Account Name');
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('University_Department').getRecordTypeId();
        insert a;

        Contact prospect = new Contact(
                FirstName = 'Wilhelm',
                LastName = 'Grimm',
                Primary_Academic_Program__c = a.Id,
                hed__AlternateEmail__c = PROSPECT_EMAIL_WITH_OPT_OUT_CONSENTS,
                MailingCity = 'London',
                MailingCountry = 'UK',
                MailingStreet = 'Street 1',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234TEST');
        insert prospect;

        hed__Affiliation__c aff = new hed__Affiliation__c();
        aff.hed__Contact__c = prospect.Id;
        aff.hed__Account__c = a.Id;
        aff.hed__Role__c='Prospect';
        aff.hed__Status__c='Current';
        insert aff;
    }

    @IsTest
    private static void testCheckIfContactIsPreApplicant() {
        List<Contact> contacts = [SELECT Id, hed__AlternateEmail__c FROM Contact WHERE hed__AlternateEmail__c = : PROSPECT_EMAIL_WITH_OPT_OUT_CONSENTS];

        System.assert(!contacts.isEmpty());

        Test.startTest();
        LSB_ScoringModel.ContactWrapper wrapper = LSB_ScoringModel.checkIfContactIsPreApplicant(contacts.get(0).Id);
        Test.stopTest();

        System.assertNotEquals(null, wrapper);
        System.assertNotEquals(null, wrapper.score);
        System.assertNotEquals(null, wrapper.percentage);
    }
}