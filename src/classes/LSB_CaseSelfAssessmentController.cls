public with sharing class LSB_CaseSelfAssessmentController {

    @AuraEnabled(Cacheable = true)
    public static LSB_CommSelfAssessmentHelper.ChartConfig getChartDataByContactId(String contactId) {
        return LSB_CommSelfAssessmentHelper.getChartResponseData(contactId);
    }

    @AuraEnabled(Cacheable = true)
    public static List<LSB_CommSelfAssessmentHelper.SectionData> getSelfAssessmentPlan(String contactId) {
        List<LSB_SAR_SelfAssessmentResponse__c> responses = LSB_CommSelfAssessmentHelper.getResponsesForContact(contactId);
        Map<String, List<LSB_CommSelfAssessmentHelper.SectionData>> category2SectionData = LSB_CommSelfAssessmentHelper.filterResponsesByCategory(responses);
        List<LSB_CommSelfAssessmentHelper.SectionData> planData = category2SectionData.get(LSB_Constants.MY_PLAN_TAB);
        if (planData.isEmpty()) {
            return null;
        }
        return planData;
    }

}