/**
 * Created by kgromek002 on 24.08.2021.
 */

global with sharing class LSB_DMTeamSendingComponentController implements mcdm_15.BulkSendPlugin {
    global static Map<String, Map<String, String>> getBulkSendPluginData(List<mcdm_15.JourneyApproval> approvals, Map<String, String> pluginData) {
        Map<String, Map<String, String>> IdsToCustomData = new Map<String, Map<String, String>>();


        if (pluginData.size() > 0) {
            for (mcdm_15.JourneyApproval approval : approvals) {
                IdsToCustomData.put(approval.objectId, pluginData);
            }
        }

        return IdsToCustomData;
    }

    @AuraEnabled
    public static List<	LSB_DMTeams__c> getGroupMembers() {
        List<LSB_DMTeams__c> groupMembers = [
                SELECT Name,LSB_DM_Email__c FROM LSB_DMTeams__c
        ];

        return groupMembers;
    }
}