/**
 * Created by ociszek001 on 16.10.2020.
 */
@isTest
private class LSB_EmailHeaderControllerTest {

    @TestSetup
    static void loadData() {
        User usr = TestDataClass.insertUser('Advisor');
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(usr.Id,'Header','headerTitle/jpg',
                TestDataClass.headerDeveloperName, 'Header Title'));
    }

    @isTest
    static void testHeaderTitleCorrect() {
        LSB_EmailHeaderController ehc;
        List<User> uList = [
                SELECT Id
                FROM User
                WHERE Username = :TestDataClass.userName
        ];
        System.assertNotEquals(null,uList, 'User list is empty');
        System.assertEquals(1, uList.size());
        List<Document> cd = [
                SELECT Id, DeveloperName
                FROM Document
                WHERE DeveloperName = :TestDataClass.headerDeveloperName
        ];
        System.assertNotEquals(null, cd, 'Document list is empty');
        System.assertEquals(1, cd.size());

        System.runAs(uList.get(0)) {
            Test.startTest();
            ehc = new LSB_EmailHeaderController();
            ehc.setHeaderTitle(cd.get(0).DeveloperName);
            Test.stopTest();
        }
        System.assertEquals(TestDataClass.headerDeveloperName, ehc.headerTitle);
        System.assertEquals(true, ehc.imageUrl.contains(cd.get(0).Id));
        System.assertEquals(TestDataClass.headerDeveloperName, ehc.getHeaderTitle());
    }

    @isTest
    static void testHeaderTitleIncorrect() {
        LSB_EmailHeaderController ehc;
        List<User> uList = [
                SELECT Id
                FROM User
                WHERE Username = :TestDataClass.userName
        ];
        System.assertNotEquals(null,uList, 'User list is empty');
        System.assertEquals(1, uList.size());
        System.runAs(uList.get(0)) {
            Test.startTest();
            ehc = new LSB_EmailHeaderController();
            ehc.setHeaderTitle(TestDataClass.incorrectHeaderDeveloperName);
            Test.stopTest();
        }
        System.assertEquals(null, ehc.imageUrl);
    }
}