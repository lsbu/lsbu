@isTest
private class LSB_SuccessPlanTemplateHelperTest {

    private static final String ADVISOR_USERNAME = 'academic.advisor@lsbu.test.com';
    private static final String MANAGER_USERNAME = 'advisor.manager@lsbu.test.com';

    @testSetup
    static void createTestData() {
        insert new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = 'LSB_TriggerDispatcher',
                hed__Load_Order__c = 1,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = 'sfal__SuccessPlanTemplate__c'
        );

        insertUsers();
    }

    @future
    private static void insertUsers() {
        UserRole advisorRole = [SELECT Id, DeveloperName, Name FROM UserRole WHERE Name LIKE '[%]%' LIMIT 1];
        Profile advisorProfile = [SELECT Id, Name FROM Profile WHERE Name LIKE 'Academic Advisor' LIMIT 1];

        User manager = new User(
                UserName = MANAGER_USERNAME,
                FirstName = '',
                LastName = 'Manager',
                Email = 'manager@lsbu.test.com',
                ProfileId = advisorProfile.Id,
                UserRoleId = advisorRole.Id,
                Alias = 'mgr',
                TimeZoneSidKey = UserInfo.getTimezone().getID(),
                LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
                EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
                LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert manager;

        insert new User(
                UserName = ADVISOR_USERNAME,
                FirstName = 'Advisor',
                LastName = 'Academic',
                Email = 'academic.advisor@lsbu.test.com',
                ManagerId = manager.Id,
                ProfileId = advisorProfile.Id,
                UserRoleId = advisorRole.Id,
                Alias = 'advis',
                TimeZoneSidKey = UserInfo.getTimezone().getID(),
                LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
                EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
                LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );
    }

    @isTest
    private static void testInsertPrivateSuccessPlanTemplate() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            sfal__SuccessPlanTemplate__c template = new sfal__SuccessPlanTemplate__c(Name = 'Plan Template 3', LSB_Type__c = '0');

            Test.startTest();

            insert template;

            Test.stopTest();

            List<sfal__SuccessPlanTemplate__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlanTemplate__Share
                    WHERE ParentId = :template.Id AND
                    RowCause = :Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            System.assertEquals(1, shares.size());
            System.assertEquals('Edit', shares.get(0).AccessLevel);
            System.assertEquals([SELECT ID FROM User WHERE UserName = :MANAGER_USERNAME].Id, shares.get(0).UserOrGroupId);
        }
    }

    @isTest
    private static void testInsertOpenSuccessPlanTemplate() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            sfal__SuccessPlanTemplate__c template = new sfal__SuccessPlanTemplate__c(Name = 'Plan Template 3', LSB_Type__c = '2');

            Test.startTest();

            insert template;

            Test.stopTest();

            List<sfal__SuccessPlanTemplate__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlanTemplate__Share
                    WHERE ParentId = :template.Id AND
                    RowCause = :Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            for (sfal__SuccessPlanTemplate__Share share : shares) {
                System.assertEquals('Edit', share.AccessLevel);
                System.assertEquals('00G', String.valueOf(share.UserOrGroupId).substring(0, 3));
            }
        }
    }

    @isTest
    private static void testChangeOpenSuccessPlanTemplateToPrivate() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            sfal__SuccessPlanTemplate__c template = new sfal__SuccessPlanTemplate__c(Name = 'Plan Template', LSB_Type__c = '2');
            insert template;

            template.LSB_Type__c = '0';

            Test.startTest();

            update template;

            Test.stopTest();

            List<sfal__SuccessPlanTemplate__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlanTemplate__Share
                    WHERE ParentId = :template.Id AND
                    RowCause = :Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            System.assertEquals(1, shares.size());
            System.assertEquals('Edit', shares.get(0).AccessLevel);
            System.assertEquals([SELECT ID FROM User WHERE UserName = :MANAGER_USERNAME].Id, shares.get(0).UserOrGroupId);
        }
    }

    @isTest
    private static void testChangePrivateSuccessPlanTemplateToOpen() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            sfal__SuccessPlanTemplate__c template = new sfal__SuccessPlanTemplate__c(Name = 'Plan Template', LSB_Type__c = '0');
            insert template;

            template.LSB_Type__c = '2';

            Test.startTest();

            update template;

            Test.stopTest();

            List<sfal__SuccessPlanTemplate__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlanTemplate__Share
                    WHERE ParentId = :template.Id AND
                    RowCause = :Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            for (sfal__SuccessPlanTemplate__Share share : shares) {
                System.assertEquals('Edit', share.AccessLevel);
                System.assertEquals('00G', String.valueOf(share.UserOrGroupId).substring(0, 3));
            }
        }
    }

}