/**
 * Created by ociszek001 on 13.10.2020.
 */

@IsTest
public with sharing class TestDataClass {
    public static final String headerDeveloperName = 'headerTitle';
    public static final String userName = 'devkama23052021@test.com';
    public static final String OH_userName = 'devkama23052019@test.org';
    public static final String incorrectHeaderDeveloperName = 'incHeaderTitle';
    public static final String footerDeveloperName = 'footerTitle';
    public static final String incorrectFooterDeveloperName = 'incFooterTitle';
    public static final String socialImageDeveloperName = 'facebook_logo';
    public static final String communityUserEmail = 'john.snowman12345@pwc.com';
    public static final String studentProfile = 'Student';
    public static final String objectApiName = 'LSB_SUE_SupportProfile__c';
    public static final String contactFieldApiName = 'LSB_SUE_Contact__c';
    public static final String universityEmail = 'snowman@university.pl';

    public static void insertDocument(DocumentWrapper documentWrapper){
        Document documentObj = new Document();
        documentObj.Body = Blob.valueOf(documentWrapper.body);
        documentObj.ContentType = documentWrapper.contentType;
        documentObj.DeveloperName = documentWrapper.developerName;
        documentObj.IsPublic = true;
        documentObj.Name = documentWrapper.name;
        documentObj.FolderId = documentWrapper.folderId;
        insert documentObj;

    }
    public static User insertUser(String profileName) {
        List<Profile> pList = [SELECT Id FROM Profile WHERE Name =: profileName LIMIT 1];
        User u = new User (Alias = 'newUser', Email = 'newuser@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_GB', ProfileId = pList.get(0).Id,
                        TimeZoneSidKey = 'Europe/London', UserName = userName);
        insert u;
        return u;
    }
    public static User insertUser() {
        List<Profile> pList = [SELECT Id FROM Profile WHERE Name =: 'Advisor' LIMIT 1];
        User u = new User (Alias = 'newUser', Email = 'newuser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_GB', ProfileId = pList.get(0).Id,
                TimeZoneSidKey = 'Europe/London', UserName = userName);
        insert u;
        return u;
    }
    public static void createOfferHolder(){
        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact contact = new Contact
                (FirstName = 'John', LastName = 'Snowman', hed__AlternateEmail__c = TestDataClass.communityUserEmail, LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_APPLICANT, Email = TestDataClass.communityUserEmail, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = a.Id);

        insert contact;
        List<Profile> profileId = [Select Id From Profile Where Name=: LSB_Constants.OFFER_HOLDER_PROFILE_NAME];
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            User offerHolderUser = new User(
                    Alias = 'jsnow',
                    Email = contact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = contact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = profileId.get(0).Id,
                    ContactId = contact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = OH_userName);
            insert offerHolderUser;
        }
    }

    public static void createStudent(){
        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact contact = new Contact
               (FirstName = 'John', LastName = 'Snowman', hed__AlternateEmail__c = TestDataClass.communityUserEmail, LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_STUDENT, Email = TestDataClass.communityUserEmail, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = a.Id);

        insert contact;
        List<Profile> profileId = [Select Id From Profile Where Name=: LSB_Constants.STUDENT_PROFILE_NAME];
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            User studentUser = new User(
                    Alias = 'jsnow',
                    Email = contact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = contact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = profileId.get(0).Id,
                    ContactId = contact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = userName);
            insert studentUser;
        }
    }
    public static LSB_SUE_SupportProfile__c insertSupportProfile(Id contactId){
        LSB_SUE_SupportProfile__c sp = new LSB_SUE_SupportProfile__c();
        sp.LSB_SUE_Contact__c = contactId;
        sp.LSB_SUE_Status__c = 'Update Pending';
        insert sp;
        return sp;

    }

    public static void insertDataUsePurpose(){
        List<DataUsePurpose> dataUsePurposes = new List<DataUsePurpose>();
        DataUsePurpose dupExplicit = new DataUsePurpose(Name ='Explicit interest', LSB_DAP_IsDefault__c = true, LSB_Type__c = LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST);
        DataUsePurpose dupLegitimate = new DataUsePurpose(Name ='Legitimate interest', LSB_DAP_IsDefault__c = true, LSB_Type__c = LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST);
        dataUsePurposes.add(dupExplicit);
        dataUsePurposes.add(dupLegitimate);
        insert dataUsePurposes;

    }

    public static Contact insertContact(){
        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact contact = new Contact
                (FirstName = 'John',
                        LastName = 'Snowman',
                        hed__AlternateEmail__c = TestDataClass.communityUserEmail,
                        hed__UniversityEmail__c = TestDataClass.universityEmail,
                        LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_APPLICANT,
                        Email = TestDataClass.communityUserEmail, LSB_CON_SourceSystem__c = 'QL',
                        LSB_ChannelOfPreference__c = 'Email',
                        LSB_ExternalID__c = '1234QWERTY', AccountId = a.Id);

        insert contact;

        hed__Address__c address1 = new hed__Address__c(
                hed__MailingCity__c = 'city1',
                hed__MailingCountry__c = 'uk',
                hed__Parent_Contact__c = contact.Id);

        hed__Address__c address2 = new hed__Address__c(
                hed__MailingCity__c = 'city2',
                hed__MailingCountry__c = 'uk',
                hed__Parent_Contact__c = contact.Id
        );

        insert new List<hed__Address__c> { address1, address2 };

        return contact;
    }

    public static Case createCase(Id recordTypeId, String email, String origin) {
        Case newCase = new Case();

        newCase.Description = '';
        newCase.SuppliedEmail = email;
        newCase.RecordTypeId = recordTypeId;
        newCase.Origin = origin;

        return newCase;
    }

    public static void insertTriggerHandlerRecord(String objectName) {
        insertTriggerHandlerRecord(objectName, 'LSB_TriggerDispatcher');
    }

    public static void insertTriggerHandlerRecord(String objectName, String className) {
        insert new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = className,
                hed__Load_Order__c = 1,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = objectName
        );
    }

    public static hed__Trigger_Handler__c createTriggerHandlerRecord(String objectName) {
        return new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = 'LSB_TriggerDispatcher',
                hed__Load_Order__c = 1,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = objectName
        );
    }

    public with sharing class DocumentWrapper{
        public Id folderId {get; set;}
        public String body {get; set;}
        public String contentType {get; set;}
        public String developerName {get; set;}
        public String name {get; set;}

       public DocumentWrapper(Id folderId, String body, String contentType, String developerName, String name ){
           this.folderId=folderId;
           this.body=body;
           this.contentType=contentType;
           this.developerName=developerName;
           this.name=name;
       }
    }
}