@IsTest
class LSB_AdvisingPoolsControllerTest {

    @TestSetup
    static void createTestData() {
        sfal__AdvisingPool__c advisingPool = new sfal__AdvisingPool__c(
            Name = 'Test Advising Pool',
            sfal__Description__c = 'Test Advising Pool'
        );

        insert advisingPool;
    }

    @IsTest
    static void fetchAdvisingPoolsTest() {
        Test.startTest();
        List<sfal__AdvisingPool__c> advisingPools = LSB_AdvisingPoolsController.fetchAdvisingPools();
        Test.stopTest();
        System.assertEquals(1, advisingPools.size());
    }

}