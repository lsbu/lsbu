public with sharing class LSB_ReportHelper {

    @TestVisible private static Reports.ReportMetadata TEST_REPORT_METADATA;
    private static final String CONTACT_FILTER_FIELD_CONFIG_METADATA_NAME = 'LSB_ReportFilterFieldName';
    private static final String MASS_ASSIGNMENT_FOLDER_NAME_CONFIG_METADATA_NAME = 'LSB_MassAssignmentReportFolderName';
    private static final String FILTER_OPERATOR_GREATER_THAN = 'greaterThan';
    private static final String TABULAR_REPORT_KEY = 'T!T';
    private static final String CONTACT_OBJECT_PREFIX = '003';
    private static final String TABULAR_REPORT_FORMAT = 'Tabular';
    private static final String CONTACT_OBJECT_NAME = LSB_Constants.CONTACT_OBJECT;
    private static final Integer ID_18_DIGIT_NUMBER = 18;

    private String contactFilterFieldName;
    private String contactFilterColumn;
    private String massAssignmentReportFolderName;

    public LSB_ReportHelper() {
        this.contactFilterFieldName = LSB_Utils.getConfigFromMetadataByDeveloperName(CONTACT_FILTER_FIELD_CONFIG_METADATA_NAME);
        this.contactFilterColumn = CONTACT_OBJECT_NAME + '.' + this.contactFilterFieldName;
        this.massAssignmentReportFolderName = LSB_Utils.getConfigFromMetadataByDeveloperName(MASS_ASSIGNMENT_FOLDER_NAME_CONFIG_METADATA_NAME);
    }

    public List<Report> getTabularReportsFromAssignmentFolder() {
        List<Report> reports = [
            SELECT
                Description,
                DeveloperName,
                FolderName,
                Format,
                Id,
                Name
            FROM Report
            WHERE FolderName = :this.massAssignmentReportFolderName
                AND Format = :TABULAR_REPORT_FORMAT
        ];
        return reports;
    }

    public Set<String> runReportAndGetContactIds(String reportId) {
        Set<String> contactIds = new Set<String>();
        ReportResultWrapper reportInitialRunWrapper = runTabularReportInitially(reportId);

        if (reportInitialRunWrapper.allRows && !Test.isRunningTest()) {
            return reportInitialRunWrapper.contactIds;
        }

        Boolean allRows = false;
        String filterValue;
        Reports.ReportMetadata reportMetadata = reportInitialRunWrapper.reportMetadata;
        Integer filterSize = reportMetadata.getReportFilters().size();
        Integer filterColumnIndex = checkFilterColumnIndex(reportMetadata, this.contactFilterFieldName);

        do {
            Reports.SortColumn sortColumnContactNumber = new Reports.SortColumn(this.contactFilterColumn, Reports.ColumnSortOrder.ASCENDING);
            reportMetadata.setSortBy(new List<Reports.SortColumn>{
                sortColumnContactNumber
            });

            if (filterValue != null) {
                setReportFilters(filterValue, reportMetadata, filterSize);
            }

            Reports.reportResults results = Reports.ReportManager.runReport(reportId, reportMetadata, true);
            allRows = results.getAllData();

            Reports.ReportFactWithDetails factDetails = (Reports.ReportFactWithDetails) results.getFactMap().get(TABULAR_REPORT_KEY);
            List<Reports.ReportDetailRow> rows = factDetails.getRows();

            for (Integer i = 0; i < rows.size(); i++) {
                for (Reports.ReportDataCell dataCell : rows[i].getDataCells()) {
                    if (dataCell.getValue() != null && dataCell.getValue().toString().startsWith(CONTACT_OBJECT_PREFIX) && dataCell.getValue().toString().length() == ID_18_DIGIT_NUMBER) {
                        contactIds.add(dataCell.getValue().toString());
                    }
                }
                if (i == rows.size() - 1) {
                    filterValue = rows[i].getDataCells()[filterColumnIndex].getValue().toString();
                }
            }

        } while (allRows != true);

        return contactIds;
    }

    private ReportResultWrapper runTabularReportInitially(String reportId) {
        Set<String> contactIds = new Set<String>();
        Reports.reportResults results = Test.isRunningTest() ? Reports.ReportManager.runReport(reportId, TEST_REPORT_METADATA, true) : Reports.ReportManager.runReport(reportId, true);

        Reports.ReportFactWithDetails factDetails =
            (Reports.ReportFactWithDetails) results.getFactMap().get(TABULAR_REPORT_KEY);
        List<Reports.ReportDetailRow> rows = factDetails.getRows();

        Integer filterColumnIndex = checkFilterColumnIndex(results.getReportMetadata(), this.contactFilterFieldName);
        Boolean hasContactFilterColumn = filterColumnIndex > -1;

        for (Reports.ReportDetailRow reportRow : rows) {
            for (Reports.ReportDataCell dataCell : reportRow.getDataCells()) {
                if (dataCell.getValue() != null && dataCell.getValue().toString().startsWith(CONTACT_OBJECT_PREFIX) && dataCell.getValue().toString().length() == ID_18_DIGIT_NUMBER) {
                    contactIds.add(dataCell.getValue().toString());
                }
            }
        }

        return new ReportResultWrapper(results.getReportMetadata(), contactIds, results.getAllData(), hasContactFilterColumn );
    }

    private Integer checkFilterColumnIndex(Reports.ReportMetadata reportMetadata, String filterFieldName) {
        Integer filterColumnIndex = -1;
        for (Integer i = 0; i < reportMetadata.getDetailColumns().size(); i++) {
            if (reportMetadata.getDetailColumns()[i].contains(filterFieldName)) {
                filterColumnIndex = i;
            }
        }
        return filterColumnIndex;
    }

    private void setReportFilters(String filterValue, Reports.ReportMetadata reportMetadata, Integer filterSize) {
        if (reportMetadata.getReportFilters().size() > filterSize) {
            Reports.ReportFilter filter = reportMetadata.getReportFilters()[filterSize];
            filter.setValue(filterValue);
        } else {
            Reports.ReportFilter reportFilter = new Reports.ReportFilter();
            reportFilter.setColumn(this.contactFilterColumn);
            reportFilter.setFilterType(Reports.ReportFilterType.fieldValue);
            reportFilter.setOperator(FILTER_OPERATOR_GREATER_THAN);
            reportFilter.setValue(filterValue);
            List<Reports.ReportFilter> allFilters = new List<Reports.ReportFilter>{
                reportFilter
            };
            reportMetadata.setReportFilters(allFilters);
        }
    }

    public void validateReportColumns(String reportId) {
        LSB_ReportHelper.ReportResultWrapper reportInitialRunWrapper = runTabularReportInitially(reportId);

        if (reportInitialRunWrapper.contactIds.isEmpty()) {
            throw new AuraHandledException(Label.LSB_MassAssignmentFormContactError);
        } else if (!reportInitialRunWrapper.hasFilterColumn) {
            throw new AuraHandledException(Label.LSB_MassAssignmentFormColumnsError);
        }
    }

    public class ReportResultWrapper {

        public Reports.ReportMetadata reportMetadata { get; set; }
        Set<String> contactIds { get; set; }
        Boolean allRows { get; set; }
        Boolean hasFilterColumn { get; set; }

        public ReportResultWrapper(Reports.ReportMetadata reportMetadata, Set<String> contactIds, Boolean allRows, Boolean hasFilterColumn) {
            this.reportMetadata = reportMetadata;
            this.contactIds = contactIds;
            this.allRows = allRows;
            this.hasFilterColumn = hasFilterColumn;
        }
    }
}