@IsTest
class LSB_MassAssignmentBatchTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman1234@pwc.com';
    private static final String SAMPLE_REPORT_MASS_TASKS_ASSIGNMENT = 'Sample Report: Mass Tasks Assignment';

    static void testData() {
        Account testAccount = new Account(Name='Test Account Name');
        insert testAccount;
        Contact contact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                Email = COMMUNITY_USER_EMAIL, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY', AccountId = testAccount.Id);
        insert contact;
        LSB_UserHelper.createCommunityUser(
                new User(
                        FirstName = 'John',
                        LastName = 'Snow',
                        Email = COMMUNITY_USER_EMAIL
                ),
                'KMDyzfik#lMtTXN6@dl6uU'
        );
        RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'AdviseeRecord' LIMIT 1];
        Case testCase = new Case(Subject = 'Test Case', RecordTypeId = rt.Id, ContactId = contact.Id, LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_STUDENT);
        insert testCase;
    }

    @IsTest(SeeAllData=true)
    static void runReportBatchTest() {
        testData();
        String reportId = '';
        for (Report rpt : LSB_MassAssignmentController.getReportsFromAssignmentFolder()) {
            if (rpt.Name == SAMPLE_REPORT_MASS_TASKS_ASSIGNMENT) {
                reportId = rpt.Id;
            }
        }
        System.assertNotEquals(null, reportId);
        Contact testContact = [SELECT Id FROM Contact WHERE LSB_ExternalID__c = '1234QWERTY' LIMIT 1];
        Reports.ReportMetadata reportMetadata =
            Reports.ReportManager.describeReport(reportId).getReportMetadata();
        List<Reports.ReportFilter> filters = new List<Reports.ReportFilter>();
        Reports.ReportFilter newFilter = new Reports.ReportFilter();
        newFilter.setColumn('CONTACT_ID');
        newFilter.setOperator('equals');
        newFilter.setValue(testContact.Id);
        filters.add(newFilter);
        reportMetadata.setReportFilters(filters);

        LSB_ReportHelper.TEST_REPORT_METADATA = reportMetadata;


        Task testTask = new Task(
            Subject = 'Test Task Creation 123123',
            Priority = 'Low',
            Description = 'test desc',
            LSB_ACT_AssignedBy__c = UserInfo.getUserId()
        );

        Test.startTest();
        String jobId = LSB_MassAssignmentController.runReportBatch(reportId, JSON.serialize(testTask), LSB_Constants.CASE_OBJECT);
        Test.stopTest();

        List<Task> tasks = [
            SELECT Id
            FROM Task
            WHERE Subject = 'Test Task Creation 123123'
        ];
        LSB_MAL_MassAssignmentLog__c massAssignmentLog = [
            SELECT Id, LSB_MAL_Errors__c, LSB_MAL_Completed__c, LSB_MAL_Failed__c
            FROM LSB_MAL_MassAssignmentLog__c
            WHERE LSB_MAL_JobId__c LIKE :jobId + '%'
            LIMIT 1
        ];

        System.assertEquals(null, massAssignmentLog.LSB_MAL_Errors__c);
        System.assertEquals(1, massAssignmentLog.LSB_MAL_Completed__c);
        System.assertEquals(0, massAssignmentLog.LSB_MAL_Failed__c);
        System.assertEquals(1, tasks.size());
    }

}