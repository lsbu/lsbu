@IsTest
private class LSB_MyAssessmentDeadlinesTest {

    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';

    @TestSetup
    private static void setup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Account accountRecord = new Account(Name = 'Administrative Test Account');
            insert accountRecord;

            Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
            insert studentContact;

            Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];

            User studentUser = new User(
                    Alias = 'jsnow',
                    Email = studentContact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = studentContact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = studentProfile.Id,
                    ContactId = studentContact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = STUDENT_USERNAME);

            insert studentUser;

            hed__Term__c courseTerm = new hed__Term__c(
                    hed__Start_Date__c = Date.today() - 2,
                    hed__End_Date__c = Date.today() + 2,
                    hed__Account__c = accountRecord.Id
            );

            insert courseTerm;

            hed__Course__c course = new hed__Course__c(
                    Name = 'Test',
                    hed__Account__c = accountRecord.Id
            );

            insert course;

            hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
                    Name = 'Test Course',
                    hed__Course__c = course.Id,
                    hed__Term__c = courseTerm.Id
            );

            insert courseOffering;

            hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
                    LSB_PEN_LatestEnrolmentRecord__c = true,
                    hed__Enrollment_Status__c = 'EFE',
                    hed__Contact__c = studentContact.Id
            );

            insert programEnrollment;

            Id moduleEnrollmentRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'LSB_Module' AND SobjectType = 'hed__Course_Enrollment__c'].Id;

            hed__Course_Enrollment__c courseEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = courseOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = moduleEnrollmentRecordTypeId
            );

            insert courseEnrollment;

            Id componentEnrollmentRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME AND SobjectType = 'hed__Course_Enrollment__c'].Id;

            hed__Course_Enrollment__c componentEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = courseOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = componentEnrollmentRecordTypeId,
                    LSB_CCN_ParentModuleComponent__c = courseEnrollment.Id,
                    LSB_CCN_HandInDate__c = Date.today()
            );

            insert componentEnrollment;

            hed__Course_Enrollment__c pastComponentEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = courseOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = componentEnrollmentRecordTypeId,
                    LSB_CCN_ParentModuleComponent__c = courseEnrollment.Id,
                    LSB_CCN_HandInDate__c = Date.today()-2
            );

            insert pastComponentEnrollment;

            hed__Course_Offering__c subComponentOffering = new hed__Course_Offering__c(
                    Name = 'Test Subcomponent',
                    hed__Course__c = course.Id,
                    hed__Term__c = courseTerm.Id
            );

            insert subComponentOffering;

            hed__Course_Enrollment__c componentSecEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = courseOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = componentEnrollmentRecordTypeId,
                    LSB_CCN_ParentModuleComponent__c = courseEnrollment.Id
            );

            insert componentSecEnrollment;

            Id subComponentEnrollmentRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_DEVELOPER_NAME AND SobjectType = 'hed__Course_Enrollment__c'].Id;

            hed__Course_Enrollment__c subComponentEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = subComponentOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = subComponentEnrollmentRecordTypeId,
                    LSB_CCN_ParentModuleComponent__c = componentSecEnrollment.Id,
                    LSB_CCN_HandInDate__c = Date.today()
            );

            insert subComponentEnrollment;

            hed__Course_Enrollment__c pastSubComponentEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = subComponentOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = subComponentEnrollmentRecordTypeId,
                    LSB_CCN_ParentModuleComponent__c = componentSecEnrollment.Id,
                    LSB_CCN_HandInDate__c = Date.today() - 1
            );

            insert pastSubComponentEnrollment;

            hed__Course_Enrollment__c subComponentEnrollmentWithOutDate = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = subComponentOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = subComponentEnrollmentRecordTypeId,
                    LSB_CCN_ParentModuleComponent__c = componentSecEnrollment.Id
            );

            insert subComponentEnrollmentWithOutDate;
        }
    }

    @IsTest
    private static void getAssessmentsTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];

        System.runAs(studentUser) {
            Test.startTest();
            LSB_MyAssessmentDeadlines.AssessmentListsWrapper assessmentListsWrapper = LSB_MyAssessmentDeadlines.getAssessments();
            Test.stopTest();

            System.assert(assessmentListsWrapper != null);
            System.assertEquals(3, assessmentListsWrapper.upcomingAssessments.size());
            System.assertEquals(2, assessmentListsWrapper.pastAssessments.size());
        }
    }
}