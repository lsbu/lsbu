public class LSB_StrengthsAndAreasTaskBatch implements Database.Batchable<SObject>, Schedulable {
    public static final String TASK_SUBJECT = LSB_Configuration__mdt.getInstance('LSB_StrengthsAndAreasTaskSubject').LSB_Value__c;
    private static final String TASK_SUBTYPE = LSB_Configuration__mdt.getInstance('LSB_StrengthsAndAreasTaskSubtype').LSB_Value__c;
    private static final String TASK_DESCRIPTION = LSB_Configuration__mdt.getInstance('LSB_StrengthsAndAreasTaskDescription').LSB_Value__c;

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query = '';
        query += 'SELECT Id, hed__Contact__c, hed__Contact__r.LSB_CON_User__c';
        query += ' FROM hed__Program_Enrollment__c';
        query += ' WHERE LSB_PEN_StrengthsAndAreasTaskToBeCreated__c = true ';
        query += ' LIMIT 8000';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<hed__Program_Enrollment__c> scope) {
        LSB_TaskHelper.insertTasksForProgramEnrollments(
            scope,
            TASK_SUBJECT,
            TASK_DESCRIPTION,
            TASK_SUBTYPE,
            'Medium',
            Date.today().addDays(28)
        );
    }

    public void finish(Database.BatchableContext BC)
    {
    }

    public void execute(SchedulableContext sc)
    {
        LSB_StrengthsAndAreasTaskBatch schedulableBatch = new LSB_StrengthsAndAreasTaskBatch();
        Database.executeBatch(schedulableBatch);
    }
}