global with sharing class LSB_MassAssignmentBatch implements Database.Batchable<sObject> {

    private final String ADVISEE_CASE_QUERY = 'SELECT Id, LSB_CON_User__c, (SELECT Id, OwnerId, RecordTypeId, ContactId FROM Cases WHERE RecordTypeId = :adviseeCaseRecordTypeId) FROM Contact WHERE Id IN :contactIds';
    private final String SUPPORT_PROFILE_QUERY = 'SELECT Id, LSB_CON_User__c, (SELECT Id FROM Support_Profiles__r) FROM Contact WHERE Id IN :contactIds';
    private LSB_MassAssignmentBatchType batchType;
    private Set<String> contactIds;
    private Id adviseeCaseRecordTypeId;
    private LSB_TaskHelper taskHelper;
    private SObject objectToCreateData;

    public LSB_MassAssignmentBatch(Set<String> contactIds, LSB_MassAssignmentBatchType batchType, SObject objectToCreateData) {
        this.batchType = batchType;
        this.contactIds = contactIds;
        this.adviseeCaseRecordTypeId = LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME);
        this.objectToCreateData = objectToCreateData;
        this.taskHelper = new LSB_TaskHelper();
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (this.batchType == LSB_MassAssignmentBatchType.ADVISEE) {
            return Database.getQueryLocator(ADVISEE_CASE_QUERY);
        } else {
            return Database.getQueryLocator(SUPPORT_PROFILE_QUERY);
        }
    }

    global void execute(Database.BatchableContext BC, List<SObject> objects) {
        this.taskHelper.createTaskFromObjectList(BC, (Task)this.objectToCreateData, (List<Contact>)objects, this.batchType);
    }

    global void finish(Database.BatchableContext BC) {

    }
}