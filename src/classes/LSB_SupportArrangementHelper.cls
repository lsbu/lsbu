public without sharing class LSB_SupportArrangementHelper {

    public static void createSupportArrangementSharingOnSupportArrangementInsert(
        Map<Id, LSB_SUT_SupportArrangement__c> newSupportArrangementsMap
    ) {
        Set<Id> parentContactIds = new Set<Id>();
        for (LSB_SUT_SupportArrangement__c sa : newSupportArrangementsMap.values()) {
            parentContactIds.add(sa.LSB_SUT_Contact__c);
        }

        if (parentContactIds.isEmpty()) {
            return;
        }

        Map<Id, Set<Id>> parentContact2RelatedContactUsers = fetchContact2RelatedContactUsersIdsFromRelationship(parentContactIds);

        if (parentContact2RelatedContactUsers.isEmpty()) {
            return;
        }

        List<LSB_SUT_SupportArrangement__Share> supportArrangementShares = new List<LSB_SUT_SupportArrangement__Share>();
        for (LSB_SUT_SupportArrangement__c sa : newSupportArrangementsMap.values()) {
            for (Id relatedContactUserId : parentContact2RelatedContactUsers.get(sa.LSB_SUT_Contact__c)) {
                if (String.isBlank(relatedContactUserId)) {
                    continue;
                }
                supportArrangementShares.add(new LSB_SUT_SupportArrangement__Share(
                    UserOrGroupId = relatedContactUserId,
                    ParentID = sa.Id,
                    AccessLevel = 'Read',
                    RowCause = Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
                ));
            }
        }

        insert supportArrangementShares;
    }

    private static Map<Id, Set<Id>> fetchContact2RelatedContactUsersIdsFromRelationship(Set<Id> contactIds) {
        List<String> validRelationshipTypes = LSB_RelationshipHelper.fetchValidTypesForSupportArrangementSharing();
        Map<Id, Set<Id>> parentContact2RelatedContactUsers = new Map<Id, Set<Id>>();
        for (hed__Relationship__c rel : [
            SELECT Id, hed__RelatedContact__r.LSB_CON_User__c, hed__Contact__c
            FROM hed__Relationship__c
            WHERE hed__Contact__c IN : contactIds
                AND hed__Type__c IN :validRelationshipTypes
                AND hed__Status__c = :LSB_RelationshipHelper.RELATIONSHIP_CURRENT_STATUS
        ]) {
            if (parentContact2RelatedContactUsers.containsKey(rel.hed__Contact__c)) {
                parentContact2RelatedContactUsers.get(rel.hed__Contact__c).add(rel.hed__RelatedContact__r.LSB_CON_User__c);
            } else {
                parentContact2RelatedContactUsers.put(
                    rel.hed__Contact__c,
                    new Set<Id>{rel.hed__RelatedContact__r.LSB_CON_User__c}
                );
            }
        }
        return parentContact2RelatedContactUsers;
    }

}