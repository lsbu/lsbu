public class LSB_CaseTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;

    public LSB_CaseTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
    }

    public void beforeInsert(List<SObject> newList) {
        LSB_CaseHelper.fillContactData((List<Case>) newList);
        LSB_CaseHelper.assignEntitlement((List<Case>) newList);
        LSB_CaseHelper.fillWebEmail((List<Case>) newList);
        LSB_CaseHelper.fillDescription((List<Case>) newList);
        LSB_CaseHelper.processNewComplaintsCases((List<Case>) newList);
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_CaseHelper.fillWebEmail((List<Case>) newItems.values());
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
        LSB_CaseHelper.triggerCaseAssignmentRules((List<Case>) newItems.values());
        LSB_CaseHelper.handleContactDataUpdate((List<Case>) newItems.values());
        LSB_CaseHelper.handleAddressesUpdate((List<Case>) newItems.values());
        LSB_CaseHelper.updateConsents((Map<Id, Case>) newItems);
        LSB_CaseHelper.updateRelatedAssessments((List<Case>) newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_CaseHelper.fillQueueIdTechnicalFieldOnRelatedChangeTranscripts(
            (List<Case>) newItems.values(),
            (Map<Id, Case>) oldItems);
        LSB_CaseHelper.updateRelatedAssessments((List<Case>) newItems.values(), (Map<Id, Case>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> newItems) {}
}