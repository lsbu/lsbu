public without sharing class LSB_CommunitySelfRegisterController {

    public static final String SELFREG_USERNAME_DOMAIN;

    public static final String SELFREG_PROFILE_NAME;

    public static final String SELFREG_USER_LANGUAGE;

    public static final String SELFREG_USER_LOCALE;

    public static final String SELFREG_USER_ENCODING;

    public static final String SELFREG_SUCCESS_PAGE_NAME;

    public static final String SELFREG_INVALID_STATUS_CODES;

    private static Map<String, String> fieldType2InputType = new Map<String, String>{
            'BOOLEAN' => 'checkbox',
            'DATE' => 'date',
            'DATETIME' => 'datetime',
            'EMAIL' => 'email',
            'INTEGER' => 'number',
            'LONG' => 'number',
            'DOUBLE' => 'number',
            'PERCENT' => 'number',
            'PHONE' => 'tel',
            'URL' => 'url'
    };

    private static String defaultInputType = 'text';

    private static Map<String, String> fieldType2IconName = new Map<String, String>();

    private static String defaultIconName = 'utility:user';

    private static Map<String, String> validateResults = new Map<String, String>();

    static {
        Map<String, String> selfRegistrationConfigurationSettings = new Map<String, String>();

        for (LSB_Configuration__mdt configRecord : [SELECT DeveloperName, LSB_Value__c, LSB_Description__c FROM LSB_Configuration__mdt WHERE DeveloperName LIKE 'SELFREG_%']) {
            selfRegistrationConfigurationSettings.put(configRecord.DeveloperName, configRecord.LSB_Value__c);
        }

        SELFREG_USERNAME_DOMAIN = selfRegistrationConfigurationSettings.containsKey('SELFREG_USERNAME_DOMAIN') ?
                selfRegistrationConfigurationSettings.get('SELFREG_USERNAME_DOMAIN') : 'lsbu.com';

        SELFREG_PROFILE_NAME = selfRegistrationConfigurationSettings.containsKey('SELFREG_PROFILE_NAME') ?
                selfRegistrationConfigurationSettings.get('SELFREG_PROFILE_NAME') : 'Offer Holder';

        SELFREG_USER_LANGUAGE = selfRegistrationConfigurationSettings.containsKey('SELFREG_USER_LANGUAGE') ?
                selfRegistrationConfigurationSettings.get('SELFREG_USER_LANGUAGE') : 'en_US';

        SELFREG_USER_LOCALE = selfRegistrationConfigurationSettings.containsKey('SELFREG_USER_LOCALE') ?
                selfRegistrationConfigurationSettings.get('SELFREG_USER_LOCALE') : 'en_GB';

        SELFREG_USER_ENCODING = selfRegistrationConfigurationSettings.containsKey('SELFREG_USER_ENCODING') ?
                selfRegistrationConfigurationSettings.get('SELFREG_USER_ENCODING') : 'UTF-8';

        SELFREG_SUCCESS_PAGE_NAME = selfRegistrationConfigurationSettings.containsKey('SELFREG_SUCCESS_PAGE_NAME') ?
                selfRegistrationConfigurationSettings.get('SELFREG_SUCCESS_PAGE_NAME') : '/s/SuccessfulRegistration';

        SELFREG_INVALID_STATUS_CODES = selfRegistrationConfigurationSettings.containsKey('SELFREG_INVALID_STATUS_CODES') ?
            selfRegistrationConfigurationSettings.get('SELFREG_INVALID_STATUS_CODES') : '';

        fieldType2IconName.put('BOOLEAN', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_CHECKBOX_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_CHECKBOX_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('DATE', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_DATE_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_DATE_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('DATETIME', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_DATETIME_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_DATETIME_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('EMAIL', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_EMAIL_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_EMAIL_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('INTEGER', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_NUMBER_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_NUMBER_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('LONG', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_NUMBER_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_NUMBER_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('DOUBLE', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_NUMBER_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_NUMBER_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('PERCENT', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_PERCENT_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_PERCENT_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('PHONE', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_PHONE_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_PHONE_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('URL', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_URL_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_URL_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('TEXT', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_TEXT_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_TEXT_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('PASSWORD', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_PASS_FIELD_ICON') ?
                    selfRegistrationConfigurationSettings.get('SELFREG_PASS_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('PICKLIST', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_PICKLIST_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_PICKLIST_FIELD_ICON') : defaultIconName
        ));

        fieldType2IconName.put('MULTIPICKLIST', (
                selfRegistrationConfigurationSettings.containsKey('SELFREG_PICKLIST_FIELD_ICON') ?
                        selfRegistrationConfigurationSettings.get('SELFREG_PICKLIST_FIELD_ICON') : defaultIconName
        ));

        validateResults.put('NO_CONTACT', Label.LSB_SelfRegNoContactMessage);
        validateResults.put('QL_CONTACT_EXISTS', '');
        validateResults.put('USER_EXISTS', Label.LSB_SelfRegUserExistsMessage);
        validateResults.put('NOT_QL_CONTACT', Label.LSB_SelfRegNotQLContactMessage);
        validateResults.put('ALREADY_STUDENT', Label.LSB_AlreadyAStudentMessage);
    }

    @AuraEnabled(cacheable=true)
    public static List<LSB_FieldWrapper> fetchFields() {
        List<LSB_FieldWrapper> fields = new List<LSB_FieldWrapper>();

        for (Schema.FieldSetMember member : SObjectType.User.FieldSets.LSB_SelfRegistrationFields.getFields()) {
            fields.add(new LSB_FieldWrapper(member));
        }

        return fields;
    }

    @AuraEnabled(cacheable=true)
    public static String validatePassword(String password) {
        String result = '';

        try {
            Site.validatePassword(new User(Username = 'testuser1234@' + SELFREG_USERNAME_DOMAIN), password, password);
        } catch(Exception ex) {
            result = ex.getMessage();
        }

        return result;
    }

    @AuraEnabled(cacheable=true)
    public static String validateEmailAddress(String emailAddress) {
        List<Contact> contacts = [
            SELECT Id, LSB_CON_SourceSystem__c, LSB_CON_ApplicationEnrolmentStatus__c
            FROM Contact
            WHERE Email = :emailAddress
        ];
        List<String> invalidStatusCodes = SELFREG_INVALID_STATUS_CODES.split(';');
        Boolean noContact = contacts.size() == 0;
        String result = '';

        if (!noContact) {
            Id qlContactId = null;

            for (Contact record : contacts) {
                if (invalidStatusCodes.contains(record.LSB_CON_ApplicationEnrolmentStatus__c)) {
                    return validateResults.get('NO_CONTACT');
                }
                if (record.LSB_CON_SourceSystem__c != null &&
                        record.LSB_CON_SourceSystem__c.equals(LSB_Constants.CONTACT_SOURCE_SYSTEM_QL)) {
                    qlContactId = record.Id;
                }
            }

            if (qlContactId != null) {
                List<User> usersListForContactId = [SELECT Id, Profile.Name FROM User WHERE ContactId = :qlContactId];
                Boolean userExists = usersListForContactId.size() != 0;
                Boolean isStudent = false;
                for (User u : usersListForContactId) {
                    if (u.Profile.Name == LSB_Constants.STUDENT_PROFILE_NAME) {
                        isStudent = true; 
                    }
                }
                if (userExists) {
                    result = validateResults.get('USER_EXISTS');
                    if (isStudent){
                        result = validateResults.get('ALREADY_STUDENT');
                    }
                } else {
                    result = validateResults.get('QL_CONTACT_EXISTS');
                }
            } else {
                result = validateResults.get('NOT_QL_CONTACT');
            }
        } else {
            result = validateResults.get('NO_CONTACT');
        }

        return result;
    }

    public class LSB_FieldWrapper {

        @AuraEnabled
        public String iconName { get; private set; }

        @AuraEnabled
        public String fieldLabel { get; private set; }

        @AuraEnabled
        public String fieldType { get; private set; }

        @AuraEnabled
        public String fieldAPIName { get; private set; }

        @AuraEnabled
        public String inputType { get; private set; }

        @AuraEnabled
        public Boolean uniqueIdField { get; private set; }

        @AuraEnabled
        public Object inputValue { get; private set; }

        @AuraEnabled
        public List<LSB_PicklistValueWrapper> picklistValues { get; private set; }

        @AuraEnabled
        public Boolean isPicklist { get; private set; }

        @AuraEnabled
        public String missingMessage { get; private set; }

        LSB_FieldWrapper(Schema.FieldSetMember member) {
            this.fieldLabel = member.getLabel();
            this.fieldType = member.getType().name();
            this.fieldAPIName = member.getSObjectField().getDescribe().getName();
            this.iconName = fieldType2IconName.containsKey(this.fieldType) ?
                    fieldType2IconName.get(this.fieldType) :
                    defaultIconName;
            this.picklistValues = new List<LSB_PicklistValueWrapper>();
            this.uniqueIdField = this.fieldAPIName.equalsIgnoreCase('Email');
            this.inputType = fieldType2InputType.containsKey(this.fieldType) ?
                    fieldType2InputType.get(this.fieldType) :
                    defaultInputType;
            this.missingMessage = String.format(
                    System.Label.LSB_SelfRegInputFieldMissingMessage,
                    new String[] { fieldLabel.toLowerCase() }
            );

            if (this.fieldType.equals('PICKLIST') || this.fieldType.equals('MULTIPICKLIST')) {
                this.isPicklist = true;
                for (Schema.PicklistEntry entry : member.getSObjectField().getDescribe().getPicklistValues()) {
                    this.picklistValues.add(new LSB_PicklistValueWrapper(entry.getLabel(), entry.getValue()));
                }
            }
        }
    }

    public class LSB_PicklistValueWrapper {

        @AuraEnabled
        public String label { get; private set; }

        @AuraEnabled
        public String value { get; private set; }

        LSB_PicklistValueWrapper(String picklistLabel, String picklistValue) {
            this.label = picklistLabel;
            this.value = picklistValue;
        }
    }

}