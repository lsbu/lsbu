/**
 * Created by ociszek001 on 16.10.2020.
 */
@isTest
private class LSB_EmailFooterControllerTest {
    @TestSetup
    static void loadData() {
        User usr = TestDataClass.insertUser('Advisor');
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(usr.Id,'Footer','footerTitle/jpg',
                TestDataClass.footerDeveloperName, 'Footer Title'));
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(usr.Id,'facebook_logo','facebook_logo/jpg',
                TestDataClass.socialImageDeveloperName, 'facebook logo'));
    }
    @isTest
    static void testFooterTitleCorrect() {
        LSB_EmailFooterController efc;
        List<User> uList = [
                SELECT Id
                FROM User
                WHERE Username = :TestDataClass.userName
        ];
        System.assertNotEquals(null,uList, 'User list is empty');
        System.assertEquals(1, uList.size());
        List<Document> cd = [
                SELECT Id, DeveloperName
                FROM Document
                WHERE DeveloperName = :TestDataClass.footerDeveloperName
        ];
        System.assertNotEquals(null, cd, 'Document list is empty');
        System.assertEquals(1, cd.size());
        System.runAs(uList.get(0)) {
            Test.startTest();
            efc = new LSB_EmailFooterController();
            efc.setFooterTitle(cd.get(0).DeveloperName);
            Test.stopTest();
            System.assertEquals(TestDataClass.footerDeveloperName, efc.footerTitle);
            System.assertEquals(true, efc.imageUrl.contains(cd.get(0).Id));
            System.assertEquals(TestDataClass.footerDeveloperName, efc.getFooterTitle());
        }
    }

    @isTest
    static void testFooterTitleIncorrect() {
        LSB_EmailFooterController efc;
        List<User> uList = [
                SELECT Id
                FROM User
                WHERE Username = :TestDataClass.userName
        ];
        System.assertEquals(1, uList.size());
        System.runAs(uList.get(0)) {
            Test.startTest();
            efc = new LSB_EmailFooterController();
            efc.setFooterTitle(TestDataClass.incorrectFooterDeveloperName);
            Test.stopTest();
        }
        System.assertEquals(null, efc.imageUrl);
    }
}