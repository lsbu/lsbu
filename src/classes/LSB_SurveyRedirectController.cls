public without sharing class LSB_SurveyRedirectController {

    private static final String CHAT_KEY_PARAM_NAME = LSB_Constants.CHAT_KEY_PARAM_NAME;
    private static final String CHAT_DETAILS_PARAM_NAME = LSB_Constants.CHAT_DETAILS_PARAM_NAME;
    private static final String CHAT_TRANSCRIPT_PARAM_NAME = LSB_Constants.CHAT_TRANSCRIPT_PARAM_NAME;
    private static final String CHAT_LAST_VISITED_PAGE_PARAM_NAME = LSB_Constants.CHAT_LAST_VISITED_PAGE_PARAM_NAME;
    private static final String DEFAULT_ADVISOR_AGENT_NAME = System.Label.LSB_ChatDefaultAdvisorName;
    private String chatKey { get; set; }
    private String chatDetails { get; set; }
    private String agentId { get; set; }
    public String agentName { get; set; }
    public Boolean isCommunity { get; set; }
    public Boolean showFeedback { get; set; }
    public String chatTranscript { get; set; }
    public String lastVisitedUrl { get; set; }
    public String downloadUrl { get; set; }

    public LSB_SurveyRedirectController() {
        this.chatKey = getUrlParameterByName(CHAT_KEY_PARAM_NAME);
        this.chatDetails = getUrlParameterByName(CHAT_DETAILS_PARAM_NAME);
        this.chatDetails = getUrlParameterByName(CHAT_DETAILS_PARAM_NAME);
        this.chatTranscript = getUrlParameterByName(CHAT_TRANSCRIPT_PARAM_NAME);
        this.lastVisitedUrl = getUrlParameterByName(CHAT_LAST_VISITED_PAGE_PARAM_NAME);
        this.agentName = getAgentDetails(chatDetails);
        this.isCommunity = checkIfCommunity();
        this.showFeedback = validateUrlToShowFeedback();
    }

    private String getUrlParameterByName(String paramName) {
        return ApexPages.currentPage().getParameters().get(paramName);
    }

    private Boolean validateUrlToShowFeedback() {
        List<LSB_ChatSurveyExcludedWebsites__mdt> websites = [SELECT Id, LSB_WebsiteUrl__c FROM LSB_ChatSurveyExcludedWebsites__mdt];
        for (LSB_ChatSurveyExcludedWebsites__mdt website : websites) {
            if (String.isNotBlank(website.LSB_WebsiteUrl__c)) {
                if (this.lastVisitedUrl != null && this.lastVisitedUrl.contains(website.LSB_WebsiteUrl__c)) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean checkIfCommunity() {
        LSB_CommunityUrl__c communityUrlSettings = LSB_CommunityUrl__c.getInstance();
        if (communityUrlSettings.LSB_CommunityURL__c != null) {
            String communityUrl = communityUrlSettings.LSB_CommunityURL__c;
            if (this.lastVisitedUrl != null && this.lastVisitedUrl.contains(communityUrl)) {
                return true;
            }
        }
        return false;
    }

    private String getAgentDetails(String chatDetailsJson) {
        if (chatDetailsJson != null || String.isNotBlank(chatDetailsJson)) {
            Root rootObject = (Root) JSON.deserialize(chatDetailsJson, Root.class);
            if (rootObject != null) {
                this.agentId = rootObject.agent.userId;
                List<User> agentUsers = [SELECT FirstName FROM User WHERE Id = :this.agentId];
                if (!agentUsers.isEmpty()) {
                    return agentUsers[0].FirstName;
                }
            }
        }
        return DEFAULT_ADVISOR_AGENT_NAME;
    }

    /**
    * @description Method used to create SurveyInvitation record basing on chatKey param.
    *
    * @param 
    *
    * @return PageReference of InvitationLink url or null.
    */
    public PageReference redirectToSurvey() {
        String url = LSB_SurveyHelper.getRelatedSurveyInvitationLink(this.chatKey);
        if (url != null) {
            PageReference surveyURL = new PageReference(url);
            return surveyURL;
        }
        return null;
    }

    public PageReference getTranscriptFromFile() {
        if (String.isBlank(this.downloadUrl)) {
            String fileName = System.Label.LSB_ChatTranscriptFileName + LSB_Constants.TEXT_FILE_EXTENSION_NAME;
            ContentVersion transcriptFile = saveTranscriptFile(this.chatKey, fileName, this.chatTranscript);
            String fileUrl = transcriptFile.LSB_CVR_DownloadLink__c;
            LSB_CommunityUrl__c communityUrlSettings = LSB_CommunityUrl__c.getInstance();
            String communityUrl = communityUrlSettings.LSB_CommunityURL__c;
            this.downloadUrl = communityUrl + '/' + fileUrl;
        }

        PageReference pageRef = new PageReference(this.downloadUrl);
        pageRef.setRedirect(false);
        return pageRef;
    }

    public ContentVersion saveTranscriptFile(String chatKey, String fileName, String fileData) {
        LiveChatTranscript chatTranscript = LSB_SurveyHelper.getLiveChatTranscript(chatKey);
        if (chatTranscript != null) {
            Id parentId = chatTranscript.CaseId;
            Blob blobData = Blob.valueOf(fileData);
            if (parentId != null) {
                return LSB_AttachmentsHelper.saveFile(parentId, fileName, blobData);
            }
        }
        return null;
    }

    public String getThankYouMessage() {
        return String.format(System.Label.LSB_ChatFeedbackMessage, new String[] { String.valueOf(this.agentName) });
    }

    public class Agent {
        public String userId { get; set; }
        public String agentName { get; set; }
    }

    public class Root {
        public Agent agent { get; set; }
    }
}