public with sharing class LSB_CommunityMyActions {

    @AuraEnabled
    public static List<LSB_Community_Actions__mdt> retrieveActionsFromMetadata() {
        List<LSB_Community_Actions__mdt> communityActions = new List<LSB_Community_Actions__mdt>();

        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, ProfileId, Profile.Name FROM User WHERE Id = :userId];

        if (u.Profile.Name.equals(LSB_Constants.OFFER_HOLDER_PROFILE_NAME)) {
            communityActions = (List<LSB_Community_Actions__mdt>) queryToFetchCommunityActionsWithBooleanParameter('LSB_Is_Visible_To_Offer_Holder__c');
        } else if (u.Profile.Name.equals(LSB_Constants.STUDENT_PROFILE_NAME)) {
            communityActions = (List<LSB_Community_Actions__mdt>) queryToFetchCommunityActionsWithBooleanParameter('LSB_Is_Visible_To_Student__c');
        }

        return communityActions;
    }

    private static List<SObject> queryToFetchCommunityActionsWithBooleanParameter (String booleanParameter) {
        String query = 'SELECT Id, LSB_Title__c, DeveloperName, LSB_Description__c, LSB_Link__c, LSB_Number__c, LSB_Icon__c\n' +
                'FROM LSB_Community_Actions__mdt ' +
                'WHERE ' + booleanParameter + ' = true\n' +
                'ORDER BY LSB_Number__c ASC';
        return Database.query(query);
    }
}