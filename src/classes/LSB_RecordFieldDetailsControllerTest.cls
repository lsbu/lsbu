/**
 * Created by ociszek001 on 04.03.2021.
 */

@IsTest
private class LSB_RecordFieldDetailsControllerTest {
    @TestSetup
    static void loadData() {
        TestDataClass.createStudent();
        List<User> userList = [SELECT Id, ContactId FROM User WHERE Email = :TestDataClass.communityUserEmail];
        System.assertEquals(1, userList.size());
        TestDataClass.insertSupportProfile(userList.get(0).ContactId);

    }
    @IsTest
    static void fetchRecordTest() {
        List<User> userList = [
                SELECT Id, FirstName, Email, ContactId
                FROM User
                WHERE Email = :TestDataClass.communityUserEmail
        ];
        System.assertEquals(1, userList.size());

        List<LSB_SUE_SupportProfile__c> sp = [Select Id FROM LSB_SUE_SupportProfile__c WHERE LSB_SUE_Contact__c=: userList.get(0).ContactId];
        System.assertEquals(1,sp.size());

        String recordId;
        System.runAs(userList.get(0)) {
            Test.startTest();
            recordId = LSB_RecordFieldDetailsController.fetchRecord(TestDataClass.objectApiName, TestDataClass.contactFieldApiName);
            Test.stopTest();
        }
        System.assertEquals(false, String.isEmpty(recordId));
        System.assertEquals(sp.get(0).Id,recordId);
    }

    @IsTest
    static void fetchRecordNegativeTest() {
        List<User> userList = [
                SELECT Id, FirstName, Email, ContactId
                FROM User
                WHERE Email = :TestDataClass.communityUserEmail
        ];
        System.assertEquals(1, userList.size());

        List<LSB_SUE_SupportProfile__c> sp = [SELECT Id FROM LSB_SUE_SupportProfile__c WHERE LSB_SUE_Contact__c = :userList.get(0).ContactId];
        delete sp;

        String recordId;
        System.runAs(userList.get(0)) {
            Test.startTest();
            recordId = LSB_RecordFieldDetailsController.fetchRecord(TestDataClass.objectApiName, TestDataClass.contactFieldApiName);
            Test.stopTest();
        }
        System.assertEquals(true, String.isEmpty(recordId));
    }
}