/**
 * @descritpion Class contains methods that test Reset Password component.
 *
 * @date 17.09.2020.
 */

@isTest
private class LSB_CommunityResetPasswordTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman1234@pwc.com';

    @testSetup
    private static void createTestData() {
        insert new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = 'LSB_TriggerDispatcher',
                hed__Load_Order__c = 2,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = 'Case'
        );
        
        Account a = new Account(Name='Test Account Name');
        insert a;

        List<Contact> contacts = new List<Contact> {
                new Contact(FirstName = 'Wilhelm', LastName = 'Grimm', hed__AlternateEmail__c = 'will.grimm@gmail.com', Email = 'will.grimm@gmail.com', Birthdate = Date.newInstance(1987, 1, 1), MobilePhone = '+48 123 123 123', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWER', AccountId = a.Id),
                new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = COMMUNITY_USER_EMAIL, Email = COMMUNITY_USER_EMAIL, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = a.Id)
        };
            
        insert contacts;

        LSB_UserHelper.createCommunityUser(
                new User(
                    FirstName = 'John',
                    LastName = 'Snow',
                    Email = COMMUNITY_USER_EMAIL
                ),
                'KMDyzfik#lMtTXN6@dl6uU'
        );
    }

    @isTest
    private static void testFindUser_RightTest() {
        String emailAddress = COMMUNITY_USER_EMAIL;

        Test.startTest();
        String message = LSB_CommunityResetPasswordController.findUser(emailAddress);
        Test.stopTest();

        System.assertNotEquals('', message);
    }

    @isTest
    private static void testFindUser_InvalidTest() {
        String emailAddress = 'will.grimm@gmail.com';

        Test.startTest();
        String message = LSB_CommunityResetPasswordController.findUser(emailAddress);
        Test.stopTest();

        System.assertEquals(null, message);
    }

    @isTest
    private static void testResetPassword_RightTest() {
        User communityUser = [SELECT Id FROM User WHERE Email = :COMMUNITY_USER_EMAIL];

        Test.startTest();
        String message = LSB_CommunityResetPasswordController.resetPassword(communityUser.Id);
        Test.stopTest();

        System.assertEquals('', message);
    }

    @isTest
    private static void testResetPassword_InvalidTest() {

        Test.startTest();
        String message = LSB_CommunityResetPasswordController.resetPassword('');
        Test.stopTest();

        System.assertNotEquals('', message);
    }
}