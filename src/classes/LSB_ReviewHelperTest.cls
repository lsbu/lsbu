@IsTest
public with sharing class LSB_ReviewHelperTest {

    private static String DATE_FOR_REVIEW_3_MONTHS = LSB_Constants.THREE_MONTHS_FOR_REVIEW;
    private static String REVIEW_STATUS_NO_LONGER_REQUIRED = LSB_Constants.REVIEW_STATUS_NO_LONGER_REQUIRED;
    private static final String REVIEW_STATUS_NEW = LSB_Constants.REVIEW_STATUS_NEW;
    private static final String REVIEW_STATUS_DONE = LSB_Constants.REVIEW_STATUS_DONE;

    @TestSetup
    static void createTestData() {
        insert new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = 'LSB_TriggerDispatcher',
                hed__Load_Order__c = 1,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = 'LSB_REW_Review__c'
        );
        Knowledge__kav testArticle = new Knowledge__kav();
        testArticle.Title = 'Unit Test Review Helper Apex class';
        testArticle.UrlName = 'Unit-Test-Review-Helper-Apex-Class';
        testArticle.LSB_KGE_DateForReview__c = DATE_FOR_REVIEW_3_MONTHS;
        testArticle.LSB_KGE_FaqCategory2__c = 'EnrolmentAndInduction';
        testArticle.IsVisibleInPkb = true;
        testArticle.LSB_KGE_ChatbotResponse__c = 'Test';
        testArticle.LSB_KGE_ChatbotResponseLong__c = 'Test';

        insert testArticle;

        LSB_REW_Review__c testReview = new LSB_REW_Review__c();
        testReview.LSB_REW_KnowledgeArticle__c = testArticle.Id;
        testReview.LSB_REW_ReviewStatus__c = REVIEW_STATUS_NEW;
        insert testReview;
    }

    @IsTest
    private static void shouldMarkExistingReviewsAsNoLongerRequiredWhenCreatedNewReview() {
        Knowledge__kav testArticle = [SELECT Id, KnowledgeArticleId, LSB_KGE_DateForReview__c FROM Knowledge__kav][0];

        Test.startTest();
            LSB_REW_Review__c testReview = new LSB_REW_Review__c();
            testReview.LSB_REW_KnowledgeArticle__c = testArticle.Id;
            testReview.LSB_REW_ReviewStatus__c = REVIEW_STATUS_NEW;
            insert testReview;
        Test.stopTest();

        List<LSB_REW_Review__c> reviews = [SELECT Id, LSB_REW_KnowledgeArticle__c, LSB_REW_KnowledgeArticle__r.KnowledgeArticleId, LSB_REW_ReviewStatus__c
        FROM LSB_REW_Review__c ORDER BY CreatedDate ASC];

        System.assertEquals(2, reviews.size());
        System.assertEquals(REVIEW_STATUS_NO_LONGER_REQUIRED, reviews[0].LSB_REW_ReviewStatus__c);
    }

    @IsTest
    private static void shouldCreateNewReviewWithProperValuesWhenOldIsDone() {
        Knowledge__kav testArticle = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav][0];

        LSB_REW_Review__c oldReview = [SELECT Id, LSB_REW_KnowledgeArticle__c, LSB_REW_KnowledgeArticle__r.KnowledgeArticleId, LSB_REW_ReviewStatus__c
        FROM LSB_REW_Review__c][0];

        Test.startTest();
            oldReview.LSB_REW_ReviewStatus__c = REVIEW_STATUS_DONE;
            update oldReview;
        Test.stopTest();

        List<LSB_REW_Review__c> newReviews = [SELECT Id, LSB_REW_ReviewDueDate__c, LSB_REW_KnowledgeArticle__c, LSB_REW_KnowledgeArticle__r.KnowledgeArticleId, LSB_REW_ReviewStatus__c
        FROM LSB_REW_Review__c WHERE LSB_REW_ReviewStatus__c =: REVIEW_STATUS_NEW];

        System.assertEquals(1, newReviews.size());
        System.assertEquals(testArticle.Id, newReviews[0].LSB_REW_KnowledgeArticle__c);
        System.assertEquals(Date.today().addMonths(3), newReviews[0].LSB_REW_ReviewDueDate__c);
    }
}