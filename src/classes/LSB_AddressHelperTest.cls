@IsTest
private class LSB_AddressHelperTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman1234567@pwc.test12345.com';

    @testSetup
    static void createTestData() {
        TestDataClass.insertTriggerHandlerRecord('Address__c');
        TestDataClass.insertTriggerHandlerRecord('Address__c', 'ADDR_Addresses_TDTM');
        TestDataClass.insertDataUsePurpose();

        Account a = new Account(Name = 'Test Account Name');
        insert a;

        Contact student = new Contact(
                FirstName = 'John',
                LastName = 'Snowman',
                hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                hed__Preferred_Email__c = LSB_Constants.CONTACT_PREFERRED_EMAIL_PERSONAL,
                Email = COMMUNITY_USER_EMAIL,
                LSB_CON_SourceSystem__c = 'QL',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY',
                LSB_CON_CurrentRole__c = 'Student',
                AccountId = a.Id);

        insert student;

        insertAddresses(student.Id);
    }

    @future
    static void insertAddresses(Id studentId) {
        hed__Address__c address1 = new hed__Address__c(
                hed__MailingCity__c = 'city1',
                hed__MailingCountry__c = 'uk',
                hed__Parent_Contact__c = studentId);

        hed__Address__c address2 = new hed__Address__c(
                hed__MailingCity__c = 'city2',
                hed__MailingCountry__c = 'uk',
                hed__Parent_Contact__c = studentId
        );

        insert new List<hed__Address__c> { address1, address2 };
    }

    @isTest
    static void testContactPointAddressInsert() {
        Contact student = [
                SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName,
                    (SELECT Id, hed__Address_Type__c, hed__Default_Address__c, hed__MailingCity__c, hed__MailingCountry__c,
                            hed__MailingPostalCode__c, hed__MailingState__c, hed__MailingStreet__c
                    FROM hed__Addresses__r),
                    (SELECT Id, AddressType, City, Country, ParentId, PostalCode, State, Street, IsDefault, IsPrimary,
                            LSB_COA_Address__c, LSB_COA_ExternalId__c
                    FROM Contact_Point_Addresses__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        System.assertEquals(2, student.hed__Addresses__r.size());
        System.assertEquals(2, student.Contact_Point_Addresses__r.size());

        Map<Id, hed__Address__c> addressesByIds = new Map<Id, hed__Address__c>(student.hed__Addresses__r);

        for (ContactPointAddress address : student.Contact_Point_Addresses__r) {
            hed__Address__c originalAddress = addressesByIds.get(address.LSB_COA_Address__c);

            System.assertEquals(originalAddress.hed__Address_Type__c, address.AddressType);
            System.assertEquals(originalAddress.hed__MailingCity__c, address.City);
            System.assertEquals(originalAddress.hed__MailingCountry__c, address.Country);
            System.assertEquals(originalAddress.hed__MailingPostalCode__c, address.PostalCode);
            System.assertEquals(originalAddress.hed__MailingState__c, address.State);
            System.assertEquals(originalAddress.hed__Default_Address__c, address.IsDefault);
            System.assertEquals(originalAddress.hed__Default_Address__c, address.IsPrimary);
            System.assertEquals(student.IndividualId, address.ParentId);
        }
    }

    @isTest
    static void testContactPointAddressUpdate() {
        Contact student = [
                SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName,
                (SELECT Id, hed__Address_Type__c, hed__Default_Address__c, hed__MailingCity__c, hed__MailingCountry__c,
                        hed__MailingPostalCode__c, hed__MailingState__c, hed__MailingStreet__c
                FROM hed__Addresses__r),
                (SELECT Id, AddressType, City, Country, ParentId, PostalCode, State, Street, IsDefault, IsPrimary,
                        LSB_COA_Address__c, LSB_COA_ExternalId__c
                FROM Contact_Point_Addresses__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        Test.startTest();

        for (hed__Address__c address : student.hed__Addresses__r) {
            address.hed__Address_Type__c = 'Work';
            address.hed__MailingCity__c = 'City 123';
            address.hed__MailingCountry__c = 'USA';
            address.hed__MailingPostalCode__c = '123456';
            address.hed__MailingState__c = 'State 123';
        }

        update student.hed__Addresses__r;

        Test.stopTest();

        student = [
                SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName,
                (SELECT Id, hed__Address_Type__c, hed__Default_Address__c, hed__MailingCity__c, hed__MailingCountry__c,
                        hed__MailingPostalCode__c, hed__MailingState__c, hed__MailingStreet__c
                FROM hed__Addresses__r),
                (SELECT Id, AddressType, City, Country, ParentId, PostalCode, State, Street, IsDefault, IsPrimary,
                        LSB_COA_Address__c, LSB_COA_ExternalId__c
                FROM Contact_Point_Addresses__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        System.assertEquals(2, student.hed__Addresses__r.size());
        System.assertEquals(2, student.Contact_Point_Addresses__r.size());

        Map<Id, hed__Address__c> addressesByIds = new Map<Id, hed__Address__c>(student.hed__Addresses__r);

        for (ContactPointAddress address : student.Contact_Point_Addresses__r) {
            hed__Address__c originalAddress = addressesByIds.get(address.LSB_COA_Address__c);

            System.assertEquals(originalAddress.hed__Address_Type__c, address.AddressType);
            System.assertEquals(originalAddress.hed__MailingCity__c, address.City);
            System.assertEquals(originalAddress.hed__MailingCountry__c, address.Country);
            System.assertEquals(originalAddress.hed__MailingPostalCode__c, address.PostalCode);
            System.assertEquals(originalAddress.hed__MailingState__c, address.State);
            System.assertEquals(originalAddress.hed__Default_Address__c, address.IsDefault);
            System.assertEquals(originalAddress.hed__Default_Address__c, address.IsPrimary);
            System.assertEquals(student.IndividualId, address.ParentId);
        }
    }

    @isTest
    static void testContactPointConsentsInsert() {
        List<ContactPointAddress> addresses = [
                SELECT Id, AddressType, City, Country, ParentId, PostalCode, State, Street, IsDefault, IsPrimary,
                        LSB_COA_Address__c, LSB_COA_ExternalId__c,
                    (SELECT Id, LSB_ExternalId__c, PrivacyConsentStatus FROM ContactPointConsents)
                FROM ContactPointAddress
        ];

        for (ContactPointAddress address : addresses) {
            System.assertEquals(2, address.ContactPointConsents.size());

            for (ContactPointConsent consent : address.ContactPointConsents) {
                System.assert(consent.LSB_ExternalId__c.startsWith(address.LSB_COA_ExternalId__c));
                System.assert(
                    consent.PrivacyConsentStatus == LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN ||
                    consent.PrivacyConsentStatus == LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_NOTSEEN
                );
            }
        }
    }

    @isTest
    static void testContactPointsDeletion() {
        Contact student = [
                SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName,
                (SELECT Id, hed__Address_Type__c, hed__Default_Address__c, hed__MailingCity__c, hed__MailingCountry__c,
                        hed__MailingPostalCode__c, hed__MailingState__c, hed__MailingStreet__c
                FROM hed__Addresses__r),
                (SELECT Id, AddressType, City, Country, ParentId, PostalCode, State, Street, IsDefault, IsPrimary,
                        LSB_COA_Address__c, LSB_COA_ExternalId__c
                FROM Contact_Point_Addresses__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        Test.startTest();

        delete student.hed__Addresses__r;

        Test.stopTest();

        student = [
                SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName,
                (SELECT Id, hed__Address_Type__c, hed__Default_Address__c, hed__MailingCity__c, hed__MailingCountry__c,
                        hed__MailingPostalCode__c, hed__MailingState__c, hed__MailingStreet__c
                FROM hed__Addresses__r),
                (SELECT Id, AddressType, City, Country, ParentId, PostalCode, State, Street, IsDefault, IsPrimary,
                        LSB_COA_Address__c, LSB_COA_ExternalId__c
                FROM Contact_Point_Addresses__r),
                (SELECT Id FROM Contact_Point_Consents__r WHERE CaptureContactPointType = :LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        System.assertEquals(0, student.hed__Addresses__r.size());
        System.assertEquals(0, student.Contact_Point_Addresses__r.size());
        System.assertEquals(0, student.Contact_Point_Consents__r.size());
    }

    @isTest
    static void testSetDefaultAddressType() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = null);
        insert address;

        List<hed__Address__c> resultAddresses = [SELECT Id, hed__Address_Type__c FROM hed__Address__c WHERE ID = :address.Id];

        System.assertEquals(1, resultAddresses.size());
        System.assertEquals('Home', resultAddresses.get(0).hed__Address_Type__c);
    }

    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceInsertPostalCode() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        LSB_PostalCodes__c postalCode = new LSB_PostalCodes__c();
        postalCode.Postcode__c = 'AL100BQ';
        postalCode.Name = 'AL100BQ';
        insert postalCode;

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__MailingPostalCode__c = 'AL100BQ',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals(true, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);
        System.assertEquals('Not London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
    }

    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceInsertPrefix() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__MailingPostalCode__c = 'E12345',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);
    }

    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceUpdatePostalCode() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        LSB_PostalCodes__c postalCode = new LSB_PostalCodes__c();
        postalCode.Postcode__c = 'AL100BQ';
        postalCode.Name = 'AL100BQ';
        insert postalCode;

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        address.hed__MailingPostalCode__c = 'AL100BQ';
        update address;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals(true, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);
        System.assertEquals('Not London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
    }

    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceUpdatePrefix() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        address.hed__MailingPostalCode__c = 'E12345';
        update address;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);
    }

    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceBlank() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        address.hed__MailingPostalCode__c = null;
        update address;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals(null, result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);
    }

    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceSetDefaultToFalse() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        address.hed__MailingPostalCode__c = 'E12345';
        update address;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        address.hed__Default_Address__c = false;
        update address;

        List<Contact> resultAfterSetDefaultToFalse = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, resultAfterSetDefaultToFalse.size());
        System.assertEquals(null, resultAfterSetDefaultToFalse.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, resultAfterSetDefaultToFalse.get(0).LSB_CON_PostcodeDeprivationFlag__c);

    }


    @IsTest
    static void testUpdatePostalCodeDeprivationFlagAndAreaOfPermanentResidenceTwoAddresses() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c addressLondon = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert addressLondon;

        addressLondon.hed__MailingPostalCode__c = 'E12345';
        update addressLondon;

        List<Contact> result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        LSB_PostalCodes__c postalCode = new LSB_PostalCodes__c();
        postalCode.Postcode__c = 'AL100BQ';
        postalCode.Name = 'AL100BQ';
        insert postalCode;

        hed__Address__c addressNotLondon = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__MailingPostalCode__c = 'AL100BQ');
        insert addressNotLondon;

        addressNotLondon.hed__Default_Address__c = true;
        update addressNotLondon;

        List<Contact> resultAfterSetDefault = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, resultAfterSetDefault.size());
        System.assertEquals('Not London', resultAfterSetDefault.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(true, resultAfterSetDefault.get(0).LSB_CON_PostcodeDeprivationFlag__c);
    }

    @IsTest
    static void testCleanPostalCodeDeprivationFlagAndAreaOfPermanentResidenceAfterDelete() {

        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c address = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert address;

        address.hed__MailingPostalCode__c = 'E12345';
        update address;

        List<Contact> resultAfterUpdate = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, resultAfterUpdate.size());
        System.assertEquals('London', resultAfterUpdate.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, resultAfterUpdate.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        delete address;

        List<Contact> resultAfterDelete = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, resultAfterDelete.size());
        System.assertEquals(null, resultAfterDelete.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, resultAfterDelete.get(0).LSB_CON_PostcodeDeprivationFlag__c);
    }

    @IsTest
    static void testMultipleAddresses() {
        Contact student = [
                SELECT Id
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        hed__Address__c addressPermanent = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true);
        insert addressPermanent;

        addressPermanent.hed__MailingPostalCode__c = 'E12345';
        update addressPermanent;

        List<Contact> result = new List<Contact>();
        result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        LSB_PostalCodes__c postalCode = new LSB_PostalCodes__c();
        postalCode.Postcode__c = 'AL100BQ';
        postalCode.Name = 'AL100BQ';
        insert postalCode;

        hed__Address__c addressPermanentSecond = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true,
                hed__MailingPostalCode__c = 'AL100BQ');
        insert addressPermanentSecond;

        result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('Not London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(true, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        hed__Address__c addressHome = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = 'Work',
                hed__Default_Address__c = true,
                hed__MailingPostalCode__c = 'AL100BQ');
        insert addressHome;

        result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals(null, result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        hed__Address__c addressPermanentThird = new hed__Address__c(
                hed__MailingCity__c = 'Test',
                hed__MailingCountry__c = 'UK',
                hed__Parent_Contact__c = student.Id,
                hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                hed__Default_Address__c = true,
                hed__MailingPostalCode__c = 'E12222');
        insert addressPermanentThird;

        result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        delete addressPermanentSecond;

        result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals('London', result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);

        delete addressPermanentThird;

        result = [SELECT Id, LSB_CON_PostcodeDeprivationFlag__c, LSB_CON_AreaOfPermanentResidence__c FROM Contact WHERE ID = :student.Id];

        System.assertEquals(1, result.size());
        System.assertEquals(null, result.get(0).LSB_CON_AreaOfPermanentResidence__c);
        System.assertEquals(false, result.get(0).LSB_CON_PostcodeDeprivationFlag__c);
    }
}