@IsTest
private class LSB_ListViewControllerTest {

    private static final String TEST_ACCOUNT_123456 = 'Test Account 123456';

    @TestSetup
    static void testData() {
        Account account = new Account(Name = TEST_ACCOUNT_123456);
        insert account;
        Contact contact = new Contact(FirstName = 'Test Contact', LastName = 'Test Contact', AccountId = account.Id, LSB_CON_CurrentRole__c = 'Prospect', LSB_CON_SourceSystem__c = 'QL');
        insert contact;
        RecordType enquiryRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = :LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME LIMIT 1];
        Case newCase = new Case(RecordTypeId = enquiryRecordType.Id, ContactId = contact.Id, Status = 'New', Subject = 'Test case', LSB_CAS_ContactRole__c = 'Prospect');
        insert newCase;

        Contact studentContact = new Contact(FirstName = 'Test Student', LastName = 'Test Student', AccountId = account.Id, LSB_CON_CurrentRole__c = 'Student', LSB_CON_SourceSystem__c = 'QL');
        insert studentContact;
        RecordType ecClaimRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = :LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME LIMIT 1];
        Case newEcClaim = new Case(RecordTypeId = ecClaimRecordType.Id,
            ContactId = studentContact.Id,
            Status = 'Request Pending',
            Subject = 'Test case',
            LSB_CAS_NatureOfEnquiry__c = 'EC / Late submission request',
            LSB_CAS_Topic__c = 'EC / Late submission request',
            LSB_CAS_ContactRole__c = 'Student'
        );
        insert newEcClaim;
    }

    @IsTest
    static void fetchRecordsTest() {
        List<Account> accounts = new List<Account>();
        Test.startTest();
        accounts.addAll((List<Account>) LSB_ListViewController.fetchRecords('Account', new List<String>{
            'Name'
        }, 'WHERE Name =\'' + TEST_ACCOUNT_123456 + '\''));
        Test.stopTest();
        System.assertEquals(1, accounts.size());
        System.assertEquals(TEST_ACCOUNT_123456, accounts[0].Name);
    }

    @IsTest
    static void fetchListViewColumnsTest() {
        Schema.FieldSet caseFieldSet = SObjectType.Case.FieldSets.LSB_CAS_CommunityCaseListView;
        Test.startTest();
        List<LSB_ListViewController.ListViewColumn> listViewColumns = LSB_ListViewController.fetchListViewColumns(Case.getSObjectType().getDescribe().getName(), caseFieldSet.getName());
        Test.stopTest();
        System.assertEquals(caseFieldSet.getFields().size(), listViewColumns.size());
        for (Integer i = 0; i < caseFieldSet.getFields().size(); i++) {
            LSB_ListViewController.ListViewColumn column = listViewColumns[i];
            FieldSetMember fieldSetMember = caseFieldSet.getFields()[i];
            System.assertEquals(fieldSetMember.getSObjectField().getDescribe().getLabel(), column.label);
            System.assertEquals(fieldSetMember.getSObjectField().getDescribe().getName(), column.fieldNameOrPath);
            System.assertEquals(fieldSetMember.getSObjectField().getDescribe().isSortable(), column.sortable);
            System.assertEquals(fieldSetMember.getSObjectField().getDescribe().isAccessible(), !column.hidden);
            System.assertEquals(String.valueOf(fieldSetMember.getSObjectField().getDescribe().getType()), column.type);
        }
    }

    @IsTest
    static void fetchListViewColumnsNoObjectTest() {
        String errorMsg;
        Test.startTest();
        try {
            List<LSB_ListViewController.ListViewColumn> listViewColumns = LSB_ListViewController.fetchListViewColumns('Some Non-exisitng object', 'some string');
        } catch (AuraHandledException ex) {
            System.debug(ex);
            errorMsg = ex.getMessage();
        }
        Test.stopTest();
        System.assertEquals('Object Not Found: Some Non-exisitng object', errorMsg);
    }

    @IsTest
    static void fetchListViewColumnsNoFieldSetTest() {
        String errorMsg;
        Test.startTest();
        try {
            List<LSB_ListViewController.ListViewColumn> listViewColumns = LSB_ListViewController.fetchListViewColumns('Case', 'some string');
        } catch (AuraHandledException ex) {
            errorMsg = ex.getMessage();
        }
        Test.stopTest();
        System.assertEquals('Field Set Not Found: some string', errorMsg);
    }

    @IsTest
    static void fetchPicklistValuesTest() {
        LSB_PicklistValuesToRecordTypeMap__mdt caseStatusEnquiry = [
            SELECT AvailablePicklistValues__c
            FROM LSB_PicklistValuesToRecordTypeMap__mdt
            WHERE RecordTypeDeveloperName__c = :LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME
                AND ObjectAPIName__c = 'Case'
                AND PicklistAPIName__c = 'Status'
            LIMIT 1
        ];
        Set<String> availableValues = new Set<String>(caseStatusEnquiry.AvailablePicklistValues__c.replace('\r\n', '\n').split('\n'));
        Test.startTest();
        List<LSB_Utils.PicklistEntryWrapper> picklistEntryWrappers = LSB_Utils.fetchPicklistEntryWrappers('Case', 'Status', LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME);
        Test.stopTest();
        System.assertEquals(false, picklistEntryWrappers.isEmpty());
        for (LSB_Utils.PicklistEntryWrapper plw : picklistEntryWrappers) {
            System.assertEquals(true, availableValues.contains(plw.value));
        }
    }

    @IsTest
    static void withdrawCaseTest() {
        Case caseToWithdraw = [
            SELECT Id
            FROM Case
            WHERE RecordType.DeveloperName = :LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME
            LIMIT 1
        ];
        Test.startTest();
        Case caseForAssertion = LSB_ListViewController.withdrawCase(caseToWithdraw.Id, 'Other - Withdrawn');
        Test.stopTest();
        System.assertEquals('Closed', caseForAssertion.Status);
    }

    @IsTest
    static void withdrawClaimTest() {
        Case caseToWithdraw = [
            SELECT Id
            FROM Case
            WHERE RecordType.DeveloperName = :LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME
            LIMIT 1
        ];
        Test.startTest();
        Case caseForAssertion = LSB_ListViewController.withdrawClaim(caseToWithdraw.Id, 'Other', 'Other Comments');
        Test.stopTest();
        System.assertEquals('Request Withdrawn by Student', caseForAssertion.Status);
    }

    @IsTest
    static void isEcClaimTest() {
        Case ecClaim = [
            SELECT Id
            FROM Case
            WHERE RecordType.DeveloperName = :LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME
            LIMIT 1
        ];
        Test.startTest();
        Boolean isEcClaim = LSB_ListViewController.isEcClaim(ecClaim.Id);
        Test.stopTest();
        System.assertEquals(true, isEcClaim);
    }

}