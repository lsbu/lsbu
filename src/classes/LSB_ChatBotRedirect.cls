public with sharing class LSB_ChatBotRedirect {

    public class UserInput {
        @InvocableVariable(Required=true)
        public String userInput;
        @InvocableVariable(Required=true)
        public Id caseId;
    }

    @InvocableMethod(label='Get Knowledge Articles')
    public static List<List<Knowledge__kav>> getKnowledgeArticles(List<UserInput> userInputs) {
        if (userInputs == null || userInputs.isEmpty() || userInputs[0].userInput.length() < 3) {
            return new List<List<Knowledge__kav>>();
        }
        String userInput = userInputs[0].userInput;
        Id caseId = userInputs[0].caseId;
        List<List<SObject>> searchResults = [
            FIND :userInput
            IN ALL FIELDS
                RETURNING Knowledge__kav (Id, KnowledgeArticleId, VersionNumber, Title, Summary, LSB_KGE_ChatbotResponse__c, LSB_KGE_ChatbotResponseLong__c, LSB_KGE_NoneAboveFlag__c
                WHERE PublishStatus = 'Online'
                LIMIT 3)
        ];
        List<LSB_AR_ArticleRating__c> articleRatings = new List<LSB_AR_ArticleRating__c>();
        List<Knowledge__kav> knowledgeArticles = new List<Knowledge__kav>();
        knowledgeArticles.addAll((List<Knowledge__kav>) searchResults[0]);

        if (!knowledgeArticles.isEmpty()) {
            List<CaseArticle> caseArticlesToInsert = new List<CaseArticle>();
            Set<Id> knowledgeVersionIds = new Set<Id>();
            for (CaseArticle car : LSB_CaseHelper.getRelatedCaseArticles(new Set<Id>{
                caseId
            })) {
                knowledgeVersionIds.add(car.KnowledgeArticleVersionId);
            }
            for (Knowledge__kav kav : knowledgeArticles) {
                if (!knowledgeVersionIds.contains(kav.Id)) {
                    caseArticlesToInsert.add(
                        new CaseArticle(
                            ArticleVersionNumber = kav.VersionNumber,
                            CaseId = caseId,
                            KnowledgeArticleId = kav.KnowledgeArticleId
                        )
                    );
                }
                articleRatings.add(
                    new LSB_AR_ArticleRating__c(
                        LSB_AR_Case__c = caseId,
                        LSB_AR_KnowledgeQuestion__c = userInput.length() > 255 ? userInput.substring(0, 255) : userInput,
                        LSB_AR_Knowledge__c = kav.Id
                    )
                );
            }
            knowledgeArticles.add(new Knowledge__kav(
                Title = Label.LSB_NoneAboveButton,
                LSB_KGE_NoneAboveFlag__c = true
            ));
            LSB_CaseHelper.insertArticleRatings(articleRatings);
            LSB_CaseHelper.insertCaseArticles(caseArticlesToInsert);
        }

        return new List<List<Knowledge__kav>>{
            knowledgeArticles
        };
    }
}