public with sharing class LSB_AdvisingPoolsController {

    /**
     * @description fetch Advising Pools
     *
     * @return list of Advising Pools
     */
    @AuraEnabled(Cacheable=true)
    public static List<sfal__AdvisingPool__c> fetchAdvisingPools() {
        return [SELECT Id, Name, sfal__Description__c, LSB_Url__c, LSB_Icon__c FROM sfal__AdvisingPool__c];
    }

}