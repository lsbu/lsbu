public with sharing class LSB_ContactPointConsentTriggerHandler implements ITriggerHandler{

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;

    public LSB_ContactPointConsentTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
    }

    public void beforeInsert(List<SObject> newList) {
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_ContactHelper.processUpdatedContactPointConsents((Map<Id, ContactPointConsent>) newItems, (Map<Id, ContactPointConsent>) oldItems);
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {
    }

    public void afterInsert(Map<Id, SObject> newItems) {
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
    }

    public void afterUndelete(Map<Id, SObject> newItems) {
    }
}