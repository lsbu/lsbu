public without sharing class LSB_IndividualHelper {

    public static void updateContactPointConsents(Map<Id, Individual> newIndividuals, Map<Id, Individual> oldIndividuals) {

        List<ContactPointConsent> contactPointConsentsToUpdate = new List<ContactPointConsent>();
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, Name FROM Contact WHERE IndividualId IN:newIndividuals.keySet()]);
        List<ContactPointConsent> contactPointConsents = [
                SELECT
                        PrivacyConsentStatus,
                        EffectiveFrom,
                        LSB_COC_Contact__r.IndividualId,
                        DataUsePurpose.LSB_Type__c
                FROM ContactPointConsent
                WHERE LSB_COC_Contact__c IN:contactMap.keySet()];


        for (ContactPointConsent cpc : contactPointConsents) {
            Boolean hasOptedOutSolicitIsChanged = newIndividuals.get(cpc.LSB_COC_Contact__r.IndividualId).HasOptedOutSolicit != oldIndividuals.get(cpc.LSB_COC_Contact__r.IndividualId).HasOptedOutSolicit;
            Boolean hasOptedOutProcessingIsChanged = newIndividuals.get(cpc.LSB_COC_Contact__r.IndividualId).HasOptedOutProcessing != oldIndividuals.get(cpc.LSB_COC_Contact__r.IndividualId).HasOptedOutProcessing;

            if ((hasOptedOutProcessingIsChanged && newIndividuals.get(cpc.LSB_COC_Contact__r.IndividualId).HasOptedOutProcessing) ||
                (hasOptedOutSolicitIsChanged && newIndividuals.get(cpc.LSB_COC_Contact__r.IndividualId).HasOptedOutSolicit) &&
                 cpc.DataUsePurpose != null &&
                 cpc.DataUsePurpose.LSB_Type__c.equals(LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST)) {
                cpc.PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT;
                cpc.EffectiveFrom = System.now();
                contactPointConsentsToUpdate.add(cpc);
            }
        }

        update contactPointConsentsToUpdate;
    }
}