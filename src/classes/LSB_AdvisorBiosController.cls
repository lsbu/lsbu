public without sharing class LSB_AdvisorBiosController {

    private static final String SUCCESS_TEAM = 'Success Team';
    private static final String PUBLIC_GROUP = 'Public Group';
    private static final String ADVISING_POOL = 'Advising Pool';
    private static final String TEACHING_STAFF = 'Teaching Staff';
    private static final String TEACHING_STAFF_LECTURER = 'Teaching Staff (Lecturer)';
    private static final String COURSE_TUTOR = 'Course Tutor';
    private static final String GENERIC_PROFILE_PIC = '/profilephoto/005/M';

    /**
     * @description fetch Advisors
     *
     * @param advisorsFrom - mode,
     * Success Team - select advisors from Success Team,
     * Public Group - select members of public group,
     * Advising Pool - select members of advising pool
     * Teaching Staff - select Teaching Staff for the student
     *
     * @param groupName advising pool/public group name/relationships
     *
     * @return
     */
    @AuraEnabled(Cacheable=true)
    public static List<AdvisorWrapper> fetchAdvisors(String advisorsFrom, String groupName) {
        if (String.isBlank(advisorsFrom) || !SUCCESS_TEAM.equals(advisorsFrom) && String.isBlank(groupName)) {
            return new List<AdvisorWrapper>();
        } else if (SUCCESS_TEAM.equals(advisorsFrom)) {
            return fetchAdvisorsFromSuccessTeam();
        } else if (PUBLIC_GROUP.equals(advisorsFrom)) {
            return fetchAdvisorsFromPublicGroup(groupName);
        } else if (ADVISING_POOL.equals(advisorsFrom)) {
            return fetchAdvisorsFromAdvisingPool(groupName);
        } else if (TEACHING_STAFF.equals(advisorsFrom)) {
            return fetchTeachingStaff(groupName);
        } else {
            return new List<AdvisorWrapper>();
        }
    }

    private static List<AdvisorWrapper> fetchTeachingStaff(String relationshipTypes) {
        List<AdvisorWrapper> advisorWrappers = new List<AdvisorWrapper>();

        Id contactId = getContactId();

        Map<Id, hed__Course_Enrollment__c> courseEnrollmentMap = new Map<Id, hed__Course_Enrollment__c>();

        for (hed__Course_Enrollment__c courseEnrollment : [
            SELECT Id,
                hed__Course_Offering__c,
                LSB_CCN_ModuleInstanceName__c
            FROM hed__Course_Enrollment__c
            WHERE hed__Contact__c = :contactId
                AND LSB_CCN_LatestEnrolmentRecord__c = TRUE
                AND LSB_CCN_ModuleEnrolmentStatus__c = 'EFE'
                AND hed__Program_Enrollment__r.hed__Enrollment_Status__c = 'EFE'
                AND RecordType.DeveloperName = 'LSB_Module'
        ]) {
            courseEnrollmentMap.put(courseEnrollment.hed__Course_Offering__c, courseEnrollment);
        }

        for (Contact contact : [
            SELECT Id,
                FirstName,
                LastName,
                Description,
                LSB_CON_User__r.FirstName,
                LSB_CON_User__r.LastName,
                LSB_CON_User__r.AboutMe,
                LSB_CON_User__r.MediumPhotoUrl, (
                SELECT Id,
                    hed__Type__c,
                    LSB_RTP_Module__c
                FROM hed__Relationships1__r
                WHERE hed__Type__c IN :relationshipTypes.split(';')
                    AND hed__Contact__c = :contactId
                    AND hed__Status__c = 'Current'
                ORDER BY hed__Type__c DESC
            )
            FROM Contact
            WHERE LSB_CON_CurrentRole__c = 'Staff'
                AND Id IN (
                    SELECT hed__RelatedContact__c
                    FROM hed__Relationship__c
                    WHERE hed__Contact__c = :contactId
                        AND hed__Type__c IN :relationshipTypes.split(';')
                        AND hed__Status__c = 'Current'
                )
            ORDER BY Id
        ]) {
            Set<String> alreadyExists = new Set<String>();
            for (hed__Relationship__c relationship : contact.hed__Relationships1__r) {
                if (!alreadyExists.contains(relationship.hed__Type__c)) {
                    String relationshipType = relationship.hed__Type__c == COURSE_TUTOR ? TEACHING_STAFF_LECTURER : relationship.hed__Type__c;
                    if(contact.LSB_CON_User__c == null){
                        advisorWrappers.add(new AdvisorWrapper(contact, relationshipType, new List<String>{
                        }));
                    } else {
                        advisorWrappers.add(new AdvisorWrapper(contact.LSB_CON_User__r, relationshipType, new List<String>{
                        }));
                    }
                    alreadyExists.add(relationship.hed__Type__c);
                }
                if (relationship.LSB_RTP_Module__c != null && courseEnrollmentMap.containsKey(relationship.LSB_RTP_Module__c)) {
                    advisorWrappers.get(advisorWrappers.size() - 1).modules.add(courseEnrollmentMap.get(relationship.LSB_RTP_Module__c).LSB_CCN_ModuleInstanceName__c);
                }
            }
        }

        return advisorWrappers;
    }

    private static List<AdvisorWrapper> fetchAdvisorsFromSuccessTeam() {
        List<AdvisorWrapper> advisorWrappers = new List<AdvisorWrapper>();

        Id contactId = getContactId();

        for (CaseTeamMember caseTeamMember : [
            SELECT Id, TeamRole.Name,
                TYPEOF Member
                    WHEN User THEN
                    Firstname,
                    Lastname,
                    MediumPhotoUrl,
                    AboutMe
                END
            FROM CaseTeamMember
            WHERE Parent.ContactId = :contactId
                AND Parent.RecordType.DeveloperName = :LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME
        ]) {
            advisorWrappers.add(new AdvisorWrapper((User) caseTeamMember.Member, caseTeamMember.TeamRole.Name, new List<String>()));
        }

        return advisorWrappers;
    }

    private static Id getContactId() {
        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, ContactId FROM User WHERE Id = :userId];
        Id contactId = u.ContactId;
        return contactId;
    }

    private static List<AdvisorWrapper> fetchAdvisorsFromAdvisingPool(String advisingPoolName) {
        List<AdvisorWrapper> advisorWrappers = new List<AdvisorWrapper>();

        List<sfal__AdvisingPool__c> advisingPools = new List<sfal__AdvisingPool__c>(
        [
            SELECT Id, sfal__CaseTeamName__c
            FROM sfal__AdvisingPool__c
            WHERE Name = :advisingPoolName
            LIMIT 1
        ]
        );

        if (advisingPools.isEmpty()) {
            return advisorWrappers;
        }

        List<CaseTeamTemplateMember> caseTeamTemplateMembers = new List<CaseTeamTemplateMember>(
        [
            SELECT MemberId, TeamRoleId
            FROM CaseTeamTemplateMember
            WHERE TeamTemplateId IN (
                SELECT Id
                FROM CaseTeamTemplate
                WHERE Name = :advisingPools[0].sfal__CaseTeamName__c
            )
        ]
        );

        if (caseTeamTemplateMembers.isEmpty()) {
            return advisorWrappers;
        }

        Map<Id, CaseTeamRole> caseTeamRoleMap = new Map<Id, CaseTeamRole>();
        Map<Id, User> membersMap = new Map<Id, User>();

        for (CaseTeamTemplateMember caseTeamTemplateMember : caseTeamTemplateMembers) {
            caseTeamRoleMap.put(caseTeamTemplateMember.TeamRoleId, null);
            membersMap.put(caseTeamTemplateMember.MemberId, null);
        }

        caseTeamRoleMap.putAll([SELECT Id, Name FROM CaseTeamRole WHERE Id IN :caseTeamRoleMap.keySet()]);
        membersMap.putAll([SELECT Id, FirstName, LastName, AboutMe, MediumPhotoUrl FROM User WHERE Id IN :membersMap.keySet()]);

        for (CaseTeamTemplateMember caseTeamTemplateMember : caseTeamTemplateMembers) {
            if (membersMap.containsKey(caseTeamTemplateMember.MemberId) && caseTeamRoleMap.containsKey(caseTeamTemplateMember.TeamRoleId)) {
                advisorWrappers.add(new AdvisorWrapper((User) membersMap.get(caseTeamTemplateMember.MemberId), caseTeamRoleMap.get(caseTeamTemplateMember.TeamRoleId).Name, new List<String>()));
            }
        }
        return advisorWrappers;
    }

    private static List<AdvisorWrapper> fetchAdvisorsFromPublicGroup(String publicGroupDeveloperName) {
        List<AdvisorWrapper> advisorWrappers = new List<AdvisorWrapper>();
        List<Group> publicGroup = new List<Group>([SELECT Name FROM Group WHERE DeveloperName = :publicGroupDeveloperName LIMIT 1]);
        if (publicGroup.isEmpty()) {
            return advisorWrappers;
        }
        for (User usr : [
            SELECT FirstName,
                LastName,
                MediumPhotoUrl,
                AboutMe
            FROM User
            WHERE Id IN (
                SELECT UserOrGroupId
                FROM GroupMember
                WHERE Group.DeveloperName = :publicGroupDeveloperName
            )
        ]) {
            advisorWrappers.add(new AdvisorWrapper(usr, String.format(Label.LSB_PublicGroupAdvisor, new List<String>{
                publicGroup[0].Name
            }), new List<String>()));
        }
        return advisorWrappers;
    }

    public class AdvisorWrapper {
        @AuraEnabled
        public String id;
        @AuraEnabled
        public String firstname;
        @AuraEnabled
        public String lastname;
        @AuraEnabled
        public String aboutme;
        @AuraEnabled
        public String role;
        @AuraEnabled
        public String photourl;
        @AuraEnabled
        public List<String> modules;

        public AdvisorWrapper(SObject sobjectRecord, String roleName, List<String> modules) {
            this.id = sobjectRecord.Id;
            this.photourl = sobjectRecord instanceof User ? (String) sobjectRecord.get('MediumPhotoUrl') : GENERIC_PROFILE_PIC;
            this.firstname = (String) sobjectRecord.get('FirstName');
            this.lastname = (String) sobjectRecord.get('LastName');
            this.aboutme = sobjectRecord instanceof User ? (String) sobjectRecord.get('AboutMe') : (String) sobjectRecord.get('Description');
            this.role = roleName;
            this.modules = new List<String>(modules);
        }
    }

}