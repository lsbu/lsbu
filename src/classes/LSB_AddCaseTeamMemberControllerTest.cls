@IsTest
public with sharing class LSB_AddCaseTeamMemberControllerTest {

    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';
    private static final String ADVISOR_USERNAME = 'mr.advisor@lsbu.com';
    private static final String ADVISOR_FIRST_NAME = 'John';

    @TestSetup
    private static void testSetup() {
        Profile advisorProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.ADVISOR_PROFILE_NAME];

        User advisorUser = new User(
                Alias = 'jadvisor',
                Email = ADVISOR_USERNAME,
                EmailEncodingKey = 'UTF-8',
                FirstName = ADVISOR_FIRST_NAME,
                LastName = 'Advisor',
                AboutMe = 'Bla bla bla',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_GB',
                ProfileId = advisorProfile.Id,
                TimeZoneSidKey = 'Europe/London',
                UserName = ADVISOR_USERNAME);

        insert advisorUser;

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Account accountRecord = new Account(Name = 'Administrative Test Account');
            insert accountRecord;

            Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
            insert studentContact;

            Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];

            User studentUser = new User(
                    Alias = 'jsnow',
                    Email = studentContact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = studentContact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = studentProfile.Id,
                    ContactId = studentContact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = STUDENT_USERNAME);

            insert studentUser;

            Case studentCase = new Case (
                    ContactId = studentContact.Id,
                    RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId(),
                    Origin = LSB_Constants.CASE_ORIGIN_WEB,
                    Status = 'New'
            );

            insert studentCase;

            CaseTeamRole caseTeamRole = [SELECT Id FROM CaseTeamRole LIMIT 1];

            CaseTeamMember caseTeamMember = new CaseTeamMember(
                    MemberId = advisorUser.Id,
                    TeamRoleId = caseTeamRole.Id,
                    ParentId = studentCase.Id
            );

            insert caseTeamMember;
        }
    }

    @IsTest
    private static void shouldReturnBaseRoles() {
        List<LSB_CaseTeamHelper.TeamRoleWrapper> result = LSB_AddCaseTeamMemberController.getCaseTeamRoles();
        System.assert(!result.isEmpty());
    }

    @IsTest
    private static void testSaveNewTeamMember() {
        Case adviseeCase = [SELECT Id FROM Case LIMIT 1];
        User teamMember = [SELECt Id, Username FROM User WHERE Username =: ADVISOR_USERNAME LIMIT 1];
        LSB_BasicCaseTeamRoles__mdt basicRole = [SELECT DeveloperName, Label, LSB_SortOrder__c
        FROM LSB_BasicCaseTeamRoles__mdt LIMIT 1];

        LSB_CaseTeamHelper.TeamRoleWrapper roleWrapper = new LSB_CaseTeamHelper.TeamRoleWrapper(basicRole.Label,
                basicRole.DeveloperName, basicRole.LSB_SortOrder__c);
        String serializedRole = JSON.serialize(roleWrapper);

        Boolean result = LSB_AddCaseTeamMemberController.addCaseTeamMember(adviseeCase.Id, teamMember.Id, serializedRole);

        System.assert(!result);
    }

    @IsTest
    private static void shouldCheckIfConfigExists() {
        LSB_BasicCaseTeamRoles__mdt basicRole = [SELECT DeveloperName, Label, LSB_SortOrder__c
        FROM LSB_BasicCaseTeamRoles__mdt LIMIT 1];

        Boolean result = LSB_AddCaseTeamMemberController.checkIfConfigExist(basicRole.DeveloperName);

        System.assert(!result);
    }

    @IsTest
    private static void shouldFindUsers() {
        List<User> result = LSB_AddCaseTeamMemberController.fetchUsers(ADVISOR_FIRST_NAME);

        System.assert(!result.isEmpty());
    }
}