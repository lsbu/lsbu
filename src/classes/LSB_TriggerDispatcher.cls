global class LSB_TriggerDispatcher extends hed.TDTM_Runnable {

    global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newList, List<SObject> oldList,
        hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
        hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
        LSB_TriggerHandlerFactory triggerHandlerFactory = new LSB_TriggerHandlerFactory();
        ITriggerHandler handler = triggerHandlerFactory.createHandler(objResult, dmlWrapper);
        if (triggerAction == hed.TDTM_Runnable.Action.BeforeInsert) {
            handler.beforeInsert(newList);
        } else if (triggerAction == hed.TDTM_Runnable.Action.BeforeUpdate) {
            handler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
        } else if (triggerAction == hed.TDTM_Runnable.Action.BeforeDelete) {
            handler.beforeDelete(Trigger.oldMap);
        } else if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert) {
            handler.afterInsert(Trigger.newMap);
        } else if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate) {
            handler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        } else if (triggerAction == hed.TDTM_Runnable.Action.AfterDelete) {
            handler.afterDelete(Trigger.oldMap);
        } else if (triggerAction == hed.TDTM_Runnable.Action.AfterUndelete) {
            handler.afterUndelete(Trigger.newMap);
        }
        return dmlWrapper;
    }

    private class LSB_TriggerHandlerFactory {

        private ITriggerHandler createHandler(Schema.DescribeSObjectResult objResult, hed.TDTM_Runnable.dmlWrapper dmlWrapper) {

            if (objResult.getName() == 'Case') {
                return new LSB_CaseTriggerHandler(dmlWrapper);
            } if (objResult.getName() == 'Contact') {
                return new LSB_ContactTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'LSB_REW_Review__c') {
                return new LSB_ReviewTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'sfal__SuccessPlan__c') {
                return new LSB_SuccessPlanTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'sfal__SuccessPlanTemplate__c') {
                return new LSB_SuccessPlanTemplateTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'Task') {
                return new LSB_TaskTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'Individual') {
                return new LSB_IndividualTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'ContactPointConsent') {
                return new LSB_ContactPointConsentTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'hed__Address__c') {
                return new LSB_AddressTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'EmailMessage') {
                return new LSB_EmailMessageTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'hed__Relationship__c') {
                return new LSB_RelationshipTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'LSB_SUT_SupportArrangement__c') {
                return new LSB_SupportArrangementTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'ContentDocument') {
                return new LSB_ContentDocumentTriggerHandler(dmlWrapper);
            } else if (objResult.getName() == 'ContentDocumentLink') {
                return new LSB_ContentDocumentLinkTriggerHandler(dmlWrapper);
            }

            return null;
        }
    }
}