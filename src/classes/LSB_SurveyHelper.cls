public without sharing class LSB_SurveyHelper {

    private static final String CHAT_SURVEY_NAME_MTD = LSB_Constants.CHAT_SURVEY_NAME_MTD;
    private static final String COMMUNITY_NAME_MTD = LSB_Constants.COMMUNITY_NAME_MTD;
    private static final String SURVEY_OBJECT = LSB_Constants.SURVEY_OBJECT;
    private static final String NETWORK_OBJECT = LSB_Constants.NETWORK_OBJECT;
    private static final String METADATA_SELF_ASSESSMENT_SURVEY_NAME = 'SELF_ASSESSMENT_SURVEY_NAME';

    /**
    * @description Method used to create SurveyInvitation record basing on chatKey param.
    *
    * @param chatKey param from LSB_SurveyRedirectPage VF page.
    *
    * @return InvitationLink url or null.
    */
    public static String getRelatedSurveyInvitationLink(String chatKey) {
        LiveChatTranscript transcript = getLiveChatTranscript(chatKey);
        if (transcript != null) {
            Survey chatSurvey = (Survey) LSB_Utils.getRecordByObjectNameAndRecordNameStoredInMetadataConfig(SURVEY_OBJECT, CHAT_SURVEY_NAME_MTD);
            if (chatSurvey != null) {
                SurveyInvitation newSurveyInvitation = createSurveyInvitation(chatSurvey);
                SurveyInvitation surveyInvitation = getSurveyInvitationById(newSurveyInvitation.Id);
                createSurveySubject(surveyInvitation, transcript, chatSurvey);
                if (surveyInvitation != null) {
                    return surveyInvitation.InvitationLink;
                }
            }
        }
        return null;
    }

    public static LiveChatTranscript getLiveChatTranscript(String chatKey) {
        List<LiveChatTranscript> transcripts = [
                SELECT Id, ChatKey, ContactId, CaseId, Body
                FROM LiveChatTranscript
                WHERE ChatKey =: chatKey
        ];
        if (transcripts.isEmpty()) {
            return null;
        }
        return transcripts[0];
    }

    /**
     * @description Method is used in flow to create initial responses for Self Assessment Survey.
     * @param responseIds response Ids
     */
    @InvocableMethod(label='Create SA Responses' description='It creates Self Assessment Response records' category='SurveyResponse')
    public static void createEmptySelfAssessmentResponses(List<Id> responseIds) {
        String selfAssessmentSurveyName = [SELECT DeveloperName, LSB_Value__c, LSB_Description__c
        FROM LSB_Configuration__mdt
        WHERE DeveloperName = :METADATA_SELF_ASSESSMENT_SURVEY_NAME].LSB_Value__c;

        Survey selfAssessmentSurvey = [SELECT Id, ActiveVersionId FROM Survey WHERE DeveloperName = :selfAssessmentSurveyName];

        Set<Id> contactIds = new Set<Id>();

        Map<Id, SurveyResponse> surveyResponses =  new Map<Id, SurveyResponse>([
                SELECT Id, Status, InvitationId, Invitation.ContactId, SubmitterId, SurveyId
                FROM SurveyResponse
                WHERE Id IN :responseIds]);

        for (SurveyResponse surveyResponse : surveyResponses.values()) {
            if (surveyResponse.SurveyId == selfAssessmentSurvey.Id) {
                contactIds.add(surveyResponse.SubmitterId);
            }
        }

        if (contactIds.isEmpty()) {
            return;
        }

        Id surveyActiveVersionId = selfAssessmentSurvey.ActiveVersionId;

        Map<String, LSB_SAC_SelfAssessmentQuesstionChoice__c> choiceDevName2SelfAssessmentChoice = getChoiceDevName2SelfAssessmentChoiceMap(surveyActiveVersionId);
        Map<SurveyResponse, List<SurveyQuestionResponse>> response2QuestionResponses = getResponse2QuestionResponsesMap(responseIds, surveyResponses);
        Map<Id, LSB_SAQ_SelfAssessmentQuestion__c> questionId2SelfAssessmentQuestion = getQuestionId2SelfAssessmentQuestionMap(selfAssessmentSurvey.Id);
        Map<Id, Id> contactId2AdviseeCaseRecordId = getContactId2AdviseeCaseRecordIdByContactsIds(contactIds);
        Map<Id, LSB_SAR_SelfAssessmentResponse__c> questionId2SelfAssessmentResponse = new Map<Id, LSB_SAR_SelfAssessmentResponse__c>();
        for (SurveyResponse surveyResponse : response2QuestionResponses.keySet()) {
            for (SurveyQuestionResponse questionResponse : response2QuestionResponses.get(surveyResponse)) {
                LSB_SAQ_SelfAssessmentQuestion__c question = questionId2SelfAssessmentQuestion.get(questionResponse.QuestionId);
                if (question == null) {
                    break;
                }
                LSB_SAC_SelfAssessmentQuesstionChoice__c selfAssessQuestionChoice = choiceDevName2SelfAssessmentChoice.get(questionResponse.QuestionChoice.DeveloperName);

                questionId2SelfAssessmentResponse.put(question.Id, createSelfAssessmentQuestionResponse(surveyResponse.SubmitterId, questionResponse, question,
                        contactId2AdviseeCaseRecordId.get(surveyResponse.SubmitterId), selfAssessQuestionChoice));
            }
        }

        for (SurveyResponse surveyResponse : response2QuestionResponses.keySet()) {
            for (LSB_SAQ_SelfAssessmentQuestion__c selfAssessQuestion : questionId2SelfAssessmentQuestion.values()) {
                if (!questionId2SelfAssessmentResponse.containsKey(selfAssessQuestion.Id)) {
                    questionId2SelfAssessmentResponse.put(selfAssessQuestion.Id, createEmptySelfAssessmentResponse(
                            surveyResponse.SubmitterId, selfAssessQuestion, contactId2AdviseeCaseRecordId.get(surveyResponse.SubmitterId)
                    ));
                }
            }
        }

        if (!questionId2SelfAssessmentResponse.values().isEmpty()) {
            insert questionId2SelfAssessmentResponse.values();
        }
    }

    private static LSB_SAR_SelfAssessmentResponse__c createEmptySelfAssessmentResponse(Id contactId, LSB_SAQ_SelfAssessmentQuestion__c question, Id adviseeCaseRecordId) {
        return new LSB_SAR_SelfAssessmentResponse__c(
                LSB_SAR_Contact__c = contactId,
                LSB_SAR_SelfAssessmentQuestion__c = question.Id,
                LSB_SAR_QuestionDeveloperName__c = question.LSB_SAQ_SurveyQuestionDeveloperName__c,
                LSB_SAR_QuestionName__c = question.Name,
                LSB_CON_AdviseeCaseRecord__c = adviseeCaseRecordId,
                LSB_SAR_IsInitialResponse__c = true
        );
    }

    private static LSB_SAR_SelfAssessmentResponse__c createSelfAssessmentQuestionResponse(Id contactId, SurveyQuestionResponse questionResponse,
            LSB_SAQ_SelfAssessmentQuestion__c question, Id adviseeCaseRecordId, LSB_SAC_SelfAssessmentQuesstionChoice__c selfAssessQuestionChoice) {

        LSB_SAR_SelfAssessmentResponse__c response = createEmptySelfAssessmentResponse(contactId, question, adviseeCaseRecordId);
        if (response != null) {
            response.LSB_SAR_ChoiceValue__c = questionResponse.ChoiceValue;
            response.LSB_SAR_DataType__c = questionResponse.Datatype;
            response.LSB_SAR_DateTimeValue__c = questionResponse.DateTimeValue;
            response.LSB_SAR_DateValue__c = questionResponse.DateValue;
            response.LSB_SAR_IsTrueOrFalse__c = questionResponse.IsTrueOrFalse;
            response.LSB_SAR_NumericValue__c = questionResponse.NumberValue;
            response.LSB_SAR_Rank__c = questionResponse.Rank;
            response.LSB_SAR_ResponseShortText__c = questionResponse.ResponseShortText;
            response.LSB_SAR_ResponseValue__c = questionResponse.ResponseValue;
            response.LSB_SAR_SurveyChoiceId__c = questionResponse.QuestionChoiceId;
            response.LSB_SAR_SurveyQuestionId__c = questionResponse.QuestionId;
            if (selfAssessQuestionChoice != null) {
                response.LSB_SAR_SelfAssessmentQuestionChoice__c = selfAssessQuestionChoice.Id;
                response.LSB_SAR_KnowledgePublicUrl__c = selfAssessQuestionChoice.LSB_SAC_KnowledgePublicUrl__c;
                response.LSB_SAR_QuestionChoiceAnswerExplanation__c = selfAssessQuestionChoice.LSB_SAC_QuestionChoiceAnswerExplanation__c;
                response.LSB_SAR_QuestionChoiceAnswer__c = selfAssessQuestionChoice.LSB_SAC_QuestionChoiceAnswer__c;
            }

        }
        return response;
    }

    private static Map<String, LSB_SAC_SelfAssessmentQuesstionChoice__c> getChoiceDevName2SelfAssessmentChoiceMap(Id surveyActiveVersionId) {
        Map<String, LSB_SAC_SelfAssessmentQuesstionChoice__c> choiceDevName2SelfAssessmentChoice = new Map<String, LSB_SAC_SelfAssessmentQuesstionChoice__c>();
        List<LSB_SAC_SelfAssessmentQuesstionChoice__c> selfAssessmentChoices = [SELECT ID, LSB_SAC_SurveyQuestionChoiceDeveloperNam__c, LSB_SAC_SelfAssessmentQuestion__c,
                LSB_SAC_KnowledgePublicUrl__c, LSB_SAC_QuestionChoiceAnswer__c, LSB_SAC_QuestionChoiceAnswerExplanation__c
        FROM LSB_SAC_SelfAssessmentQuesstionChoice__c
        WHERE LSB_SAC_SurveyActiveVersionId__c = :surveyActiveVersionId];
        for (LSB_SAC_SelfAssessmentQuesstionChoice__c selfAssessmentChoice : selfAssessmentChoices) {
            if (selfAssessmentChoice.LSB_SAC_SurveyQuestionChoiceDeveloperNam__c != null) {
                choiceDevName2SelfAssessmentChoice.put(selfAssessmentChoice.LSB_SAC_SurveyQuestionChoiceDeveloperNam__c, selfAssessmentChoice);
            }
        }
        return choiceDevName2SelfAssessmentChoice;
    }

    private static Map<SurveyResponse, List<SurveyQuestionResponse>> getResponse2QuestionResponsesMap(List<Id> responseIds, Map<Id, SurveyResponse> surveyResponses) {
        Map<SurveyResponse, List<SurveyQuestionResponse>> response2QuestionResponses = new Map<SurveyResponse, List<SurveyQuestionResponse>>();
        List<SurveyQuestionResponse> surveyQuestionResponses = [ SELECT Id, ResponseId, QuestionId, QuestionChoice.DeveloperName,
                ChoiceValue, Datatype, DateTimeValue, DateValue, IsTrueOrFalse, NumberValue, Rank, ResponseShortText, ResponseValue, QuestionChoiceId
        FROM SurveyQuestionResponse
        WHERE ResponseId IN : responseIds];

        for (SurveyQuestionResponse questionResponse : surveyQuestionResponses) {
            if (response2QuestionResponses.containsKey(surveyResponses.get(questionResponse.ResponseId))) {
                response2QuestionResponses.get(surveyResponses.get(questionResponse.ResponseId)).add(questionResponse);
            } else {
                response2QuestionResponses.put(surveyResponses.get(questionResponse.ResponseId), new List<SurveyQuestionResponse> {questionResponse});
            }
        }
        return response2QuestionResponses;
    }

    private static Map<Id, LSB_SAQ_SelfAssessmentQuestion__c> getQuestionId2SelfAssessmentQuestionMap(Id surveyId) {
        Map<Id, LSB_SAQ_SelfAssessmentQuestion__c> questionId2SelfAssessmentQuestion = new Map<Id, LSB_SAQ_SelfAssessmentQuestion__c>();
        List<LSB_SAQ_SelfAssessmentQuestion__c> surveyQuestions = [
                SELECT Id, Name, LSB_SAQ_ActiveSurveyVersionId__c, LSB_SAQ_Category__c, LSB_SAQ_Section__c, LSB_SAQ_SelfAssessment__c,
                        LSB_SAQ_SortOrder__c, LSB_SAQ_SurveyQuestionDeveloperName__c, LSB_SAQ_SurveyQuestionFullQuestion__c,
                        LSB_SAQ_SurveyQuestionId__c, LSB_SAQ_SurveyQuestionType__c
                FROM LSB_SAQ_SelfAssessmentQuestion__c
                WHERE IsDeleted = false
                AND LSB_SAQ_IsDeprecated__c = false
                AND LSB_SAQ_SelfAssessment__r.LSB_SAS_SurveyId__c = :surveyId
        ];
        for (LSB_SAQ_SelfAssessmentQuestion__c selfAssessmentQuestion : surveyQuestions) {
            questionId2SelfAssessmentQuestion.put(selfAssessmentQuestion.LSB_SAQ_SurveyQuestionId__c, selfAssessmentQuestion);
        }
        return questionId2SelfAssessmentQuestion;
    }

    private static Map<Id, Id> getContactId2AdviseeCaseRecordIdByContactsIds(Set<Id> contactIds) {
        Map<Id, Id> getContactId2AdviseeCaseRecordId = new Map<Id, Id>();
        List<Contact> relatedContacts = [SELECT Id, LSB_CON_AdviseeCaseRecord__c FROM Contact WHERE Id IN: contactIds];
        for (Contact contact : relatedContacts) {
            getContactId2AdviseeCaseRecordId.put(contact.Id, contact.LSB_CON_AdviseeCaseRecord__c);
        }
        return getContactId2AdviseeCaseRecordId;
    }

    @TestVisible
    private static SurveyInvitation getSurveyInvitationById(Id invitationId) {
        List<SurveyInvitation> invitations = [
                SELECT Id, InvitationLink, Name
                FROM SurveyInvitation
                WHERE Id = :invitationId
        ];
        if (invitations.isEmpty()) {
            return null;
        }
        return invitations[0];
    }

    @TestVisible
    private static SurveyInvitation createSurveyInvitation(Survey survey) {
        SurveyInvitation invitation = new SurveyInvitation(
                Name = getSurveyNameFromCustomLabel(survey.Name),
                SurveyId = survey.Id,
                CommunityId = LSB_Utils.getRecordByObjectNameAndRecordNameStoredInMetadataConfig(NETWORK_OBJECT, COMMUNITY_NAME_MTD).Id,
                OptionsAllowGuestUserResponse = true
        );
        insert invitation;
        return invitation;
    }

    private static void createSurveySubject(SurveyInvitation surveyInvitation, LiveChatTranscript transcript, Survey survey) {
        SurveySubject surveySubject = new SurveySubject();
        surveySubject.SubjectId = transcript.CaseId;
        surveySubject.Name = getSurveyNameFromCustomLabel(survey.Name);
        surveySubject.ParentId = surveyInvitation.Id;

        insert surveySubject;
    }

    @TestVisible
    private static String getSurveyNameFromCustomLabel(String surveyName) {
        return String.format(System.Label.LSB_ChatSurveyInvitationName, new String[] { String.valueOf(surveyName) });
    }
}