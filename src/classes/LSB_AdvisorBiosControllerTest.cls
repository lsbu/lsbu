@IsTest
class LSB_AdvisorBiosControllerTest {

    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';
    private static final String ADVISOR_USERNAME = 'mr.advisor@lsbu.com';
    private static final String TEST_ADVISING_POOL = 'Test Advising Pool';
    private static final String ADVISING_POOL = 'Advising Pool';
    private static final String TEST_TOPIC = 'Test Topic';
    private static final String PUBLIC_GROUP = 'Public Group';
    private static final String TEACHING_STAFF = 'Teaching Staff';
    private static final String TEST_PUBLIC_GROUP = 'TestPublicGroup';
    private static final String ALL_INTERNAL_USERS = 'All Internal Users';

    @TestSetup
    private static void loadData() {
        Profile advisorProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.ADVISOR_PROFILE_NAME];

        User advisorUser = new User(
            Alias = 'jadvisor',
            Email = ADVISOR_USERNAME,
            EmailEncodingKey = 'UTF-8',
            FirstName = 'John',
            LastName = 'Advisor',
            AboutMe = 'Bla bla bla',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_GB',
            ProfileId = advisorProfile.Id,
            TimeZoneSidKey = 'Europe/London',
            UserName = ADVISOR_USERNAME);

        insert advisorUser;

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Account accountRecord = new Account(Name = 'Administrative Test Account');
            insert accountRecord;

            Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
            insert studentContact;

            Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];

            User studentUser = new User(
                Alias = 'jsnow',
                Email = studentContact.Email,
                EmailEncodingKey = 'UTF-8',
                LastName = studentContact.LastName,
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_GB',
                ProfileId = studentProfile.Id,
                ContactId = studentContact.Id,
                TimeZoneSidKey = 'Europe/London',
                UserName = STUDENT_USERNAME);

            insert studentUser;

            Case studentCase = new Case (
                ContactId = studentContact.Id,
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId(),
                Origin = LSB_Constants.CASE_ORIGIN_WEB,
                Status = 'New'
            );

            insert studentCase;

            CaseTeamRole caseTeamRole = [SELECT Id FROM CaseTeamRole LIMIT 1];

            CaseTeamMember caseTeamMember = new CaseTeamMember(
                MemberId = advisorUser.Id,
                TeamRoleId = caseTeamRole.Id,
                ParentId = studentCase.Id
            );

            insert caseTeamMember;

            CaseTeamTemplate predefinedCaseTeamTemplate = new CaseTeamTemplate(
                Name = TEST_ADVISING_POOL
            );

            insert predefinedCaseTeamTemplate;

            CaseTeamTemplateMember predefinedCaseTeamTemplateMember = new CaseTeamTemplateMember(
                MemberId = advisorUser.Id,
                TeamRoleId = caseTeamRole.Id,
                TeamTemplateId = predefinedCaseTeamTemplate.Id
            );

            insert predefinedCaseTeamTemplateMember;

            sfal__AdvisingPool__c advisingPool = new sfal__AdvisingPool__c(Name = TEST_ADVISING_POOL, sfal__CaseTeamName__c = TEST_ADVISING_POOL);

            insert advisingPool;

            sfal__Topic__c topic = new sfal__Topic__c(Name = TEST_TOPIC, LSB_TIC_AppointmentSharingSetting__c = ALL_INTERNAL_USERS);

            insert topic;

            sfal__QueueTopicSetting__c queueTopicSetting = new sfal__QueueTopicSetting__c(sfal__AdvisingPool__c = advisingPool.Id, sfal__Topic__c = topic.Id);

            insert queueTopicSetting;

            Group publicGroup = new Group(DeveloperName = TEST_PUBLIC_GROUP, Name = TEST_PUBLIC_GROUP);

            insert publicGroup;

            GroupMember groupMember = new GroupMember(UserOrGroupId = advisorUser.Id, GroupId = publicGroup.Id);

            insert groupMember;

            Contact advisorContact = new Contact(
                FirstName = advisorUser.FirstName,
                LastName = advisorUser.LastName,
                Email = advisorUser.Email,
                LSB_CON_User__c = advisorUser.Id,
                LSB_CON_CurrentRole__c = 'Staff'
            );

            insert advisorContact;

            Account department = new Account(
                Name = 'Example Department',
                RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'University_Department' AND SobjectType = 'Account'].Id
            );

            insert department;

            hed__Term__c courseTerm = new hed__Term__c(
                hed__Start_Date__c = Date.today() - 2,
                hed__End_Date__c = Date.today() + 2,
                hed__Account__c = department.Id
            );

            insert courseTerm;

            hed__Course__c course = new hed__Course__c(
                Name = 'Test Course',
                hed__Account__c = department.Id
            );

            insert course;

            hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
                Name = 'Test Course',
                hed__Course__c = course.Id,
                hed__Term__c = courseTerm.Id,
                hed__Faculty__c = advisorContact.Id
            );

            insert courseOffering;

            hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
                LSB_PEN_LatestEnrolmentRecord__c = true,
                hed__Enrollment_Status__c = 'EFE'
            );

            insert programEnrollment;

            Id moduleEnrollmentRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'LSB_Module' AND SobjectType = 'hed__Course_Enrollment__c'].Id;

            hed__Course_Enrollment__c courseEnrollment = new hed__Course_Enrollment__c(
                hed__Contact__c = studentContact.Id,
                hed__Course_Offering__c = courseOffering.Id,
                hed__Program_Enrollment__c = programEnrollment.Id,
                LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                RecordTypeId = moduleEnrollmentRecordTypeId
            );

            insert courseEnrollment;

            hed__Relationship__c advisorStudentRelationship = new hed__Relationship__c(
                hed__Contact__c = studentContact.Id,
                hed__RelatedContact__c = advisorContact.Id,
                hed__Type__c = 'Advisor',
                LSB_RTP_PermissionGivenMethod__c = 'Verbal',
                LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
                LSB_RTP_Module__c = courseOffering.Id
            );

            insert advisorStudentRelationship;
        }
    }

    @IsTest
    private static void fetchTeachingStaffTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];
        User advisorUser = [SELECT Id, FirstName, LastName, AboutMe FROM User WHERE Username = :ADVISOR_USERNAME];
        System.runAs(studentUser) {
            Test.startTest();
            List<LSB_AdvisorBiosController.AdvisorWrapper> advisorWrappers = LSB_AdvisorBiosController.fetchAdvisors(TEACHING_STAFF, 'Advisor');
            Test.stopTest();
            System.assertEquals(1, advisorWrappers.size());
            System.assertEquals(advisorUser.Id, advisorWrappers[0].id);
            System.assertEquals(advisorUser.FirstName, advisorWrappers[0].firstname);
            System.assertEquals(advisorUser.LastName, advisorWrappers[0].lastname);
            System.assertEquals(advisorUser.AboutMe, advisorWrappers[0].aboutme);
            System.assertEquals(1, advisorWrappers[0].modules.size());
            System.assertEquals('Test Course', advisorWrappers[0].modules[0]);
            System.assertNotEquals(null, advisorWrappers[0].role);
            System.assertNotEquals(null, advisorWrappers[0].photourl);
        }
    }

    @IsTest
    private static void fetchAdvisorsFromSuccessTeamTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];
        User advisorUser = [SELECT Id, FirstName, LastName, AboutMe FROM User WHERE Username = :ADVISOR_USERNAME];
        System.runAs(studentUser) {
            Test.startTest();
            List<LSB_AdvisorBiosController.AdvisorWrapper> advisorWrappers = LSB_AdvisorBiosController.fetchAdvisors('Success Team', null);
            Test.stopTest();
            System.assertEquals(1, advisorWrappers.size());
            System.assertEquals(advisorUser.Id, advisorWrappers[0].id);
            System.assertEquals(advisorUser.FirstName, advisorWrappers[0].firstname);
            System.assertEquals(advisorUser.LastName, advisorWrappers[0].lastname);
            System.assertEquals(advisorUser.AboutMe, advisorWrappers[0].aboutme);
            System.assertNotEquals(null, advisorWrappers[0].role);
            System.assertNotEquals(null, advisorWrappers[0].photourl);
        }
    }

    @IsTest
    private static void fetchAdvisorsFromAdvisingPool() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];
        User advisorUser = [SELECT Id, FirstName, LastName, AboutMe FROM User WHERE Username = :ADVISOR_USERNAME];
        System.runAs(studentUser) {
            Test.startTest();
            List<LSB_AdvisorBiosController.AdvisorWrapper> advisorWrappers = LSB_AdvisorBiosController.fetchAdvisors(ADVISING_POOL, TEST_ADVISING_POOL);
            Test.stopTest();
            System.assertEquals(1, advisorWrappers.size());
            System.assertEquals(advisorUser.Id, advisorWrappers[0].id);
            System.assertEquals(advisorUser.FirstName, advisorWrappers[0].firstname);
            System.assertEquals(advisorUser.LastName, advisorWrappers[0].lastname);
            System.assertEquals(advisorUser.AboutMe, advisorWrappers[0].aboutme);
            System.assertNotEquals(null, advisorWrappers[0].photourl);
        }
    }

    @IsTest
    private static void fetchAdvisorsFromPublicGroupTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];
        User advisorUser = [SELECT Id, FirstName, LastName, AboutMe FROM User WHERE Username = :ADVISOR_USERNAME];
        Group publicGroup = [SELECT Id, Name FROM Group WHERE DeveloperName = :TEST_PUBLIC_GROUP LIMIT 1];
        System.runAs(studentUser) {
            Test.startTest();
            List<LSB_AdvisorBiosController.AdvisorWrapper> advisorWrappers = LSB_AdvisorBiosController.fetchAdvisors(PUBLIC_GROUP, TEST_PUBLIC_GROUP);
            Test.stopTest();
            System.assertEquals(1, advisorWrappers.size());
            System.assertEquals(advisorUser.Id, advisorWrappers[0].id);
            System.assertEquals(advisorUser.FirstName, advisorWrappers[0].firstname);
            System.assertEquals(advisorUser.LastName, advisorWrappers[0].lastname);
            System.assertEquals(advisorUser.AboutMe, advisorWrappers[0].aboutme);
            System.assertEquals(String.format(Label.LSB_PublicGroupAdvisor, new List<String>{
                publicGroup.Name
            }), advisorWrappers[0].role);
            System.assertNotEquals(null, advisorWrappers[0].photourl);
        }
    }

    @IsTest
    private static void emptyStringsTest() {
        Test.startTest();
        Boolean exceptionOccured = false;
        try {
            LSB_AdvisorBiosController.fetchAdvisors(null, null);
            LSB_AdvisorBiosController.fetchAdvisors(ADVISING_POOL, null);
            LSB_AdvisorBiosController.fetchAdvisors(PUBLIC_GROUP, null);
        } catch (Exception ex) {
            exceptionOccured = true;
        }
        Test.stopTest();
        System.assertEquals(false, exceptionOccured);
    }
}