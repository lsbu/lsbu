public without sharing class LSB_RelationshipHelper {
    public static final String RELATIONSHIP_CURRENT_STATUS = 'Current';
    public static final String RELATIONSHIP_FORMER_STATUS = 'Former';

    public static void createSupportArrangementSharingOnRelationshipCreationAndEdit(
        Map<Id, hed__Relationship__c> oldRelationshipsMap,
        Map<Id, hed__Relationship__c> newRelationshipsMap
    ) {
        Set<Id> studentContactIds = new Set<Id>();
        Set<Id> relatedContactIds = new Set<Id>();
        List<String> validRelationshipTypes = fetchValidTypesForSupportArrangementSharing();

        for (hed__Relationship__c rel : newRelationshipsMap.values()) {
            if (!validRelationshipTypes.contains(rel.hed__Type__c)) {
                continue;
            }
            if ((oldRelationshipsMap == null && rel.hed__Status__c == RELATIONSHIP_CURRENT_STATUS) || (
                    oldRelationshipsMap != null &&
                    rel.hed__Status__c == RELATIONSHIP_CURRENT_STATUS &&
                    oldRelationshipsMap.get(rel.Id).hed__Status__c == RELATIONSHIP_FORMER_STATUS
                ) || (
                    oldRelationshipsMap != null &&
                    rel.hed__Type__c != oldRelationshipsMap.get(rel.Id).hed__Type__c
                )
            ) {
                studentContactIds.add(rel.hed__Contact__c);
                relatedContactIds.add(rel.hed__RelatedContact__c);
            }
        }

        if (studentContactIds.isEmpty() || relatedContactIds.isEmpty()) {
            return;
        }

        Map<Id, List<LSB_SUT_SupportArrangement__c>> contactId2SupportArrangements =
            fetchSupportArrangementsForContact(studentContactIds);

        Map<Id, Id> relatedContactId2UserId = fetchContactToUserRelationByContactIds(relatedContactIds);

        List<LSB_SUT_SupportArrangement__Share> supportArrangementShares = new List<LSB_SUT_SupportArrangement__Share>();
        for (hed__Relationship__c rel : newRelationshipsMap.values()) {
            if (contactId2SupportArrangements.containsKey(rel.hed__Contact__c)) {
                for (LSB_SUT_SupportArrangement__c sa : contactId2SupportArrangements.get(rel.hed__Contact__c)) {
                    if (String.isBlank(relatedContactId2UserId.get(rel.hed__RelatedContact__c))) {
                        continue;
                    }
                    supportArrangementShares.add(new LSB_SUT_SupportArrangement__Share(
                        UserOrGroupId = relatedContactId2UserId.get(rel.hed__RelatedContact__c),
                        ParentID = sa.Id,
                        AccessLevel = 'Read',
                        RowCause = Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
                    ));
                }
            }
        }

        insert supportArrangementShares;
    }

    public static void deleteSupportArrangementSharingOnRelationshipDeletionAndEdit(
        Map<Id, hed__Relationship__c> oldRelationshipsMap,
        Map<Id, hed__Relationship__c> newRelationshipsMap
    ) {
        Set<Id> studentContactIds = new Set<Id>();
        Set<Id> relatedContactIds = new Set<Id>();
        List<hed__Relationship__c> inactiveRelationships = new List<hed__Relationship__c>();
        Map<Id, LSB_SUT_SupportArrangement__Share> sharesToDelete = new Map<Id, LSB_SUT_SupportArrangement__Share>();
        List<String> validRelationshipTypes = fetchValidTypesForSupportArrangementSharing();

        for (hed__Relationship__c oldRel : oldRelationshipsMap.values()) {
            if (newRelationshipsMap == null || (
                    oldRel.hed__Status__c == RELATIONSHIP_CURRENT_STATUS &&
                    newRelationshipsMap.get(oldRel.Id).hed__Status__c == RELATIONSHIP_FORMER_STATUS
                ) || (
                    oldRel.hed__Type__c != newRelationshipsMap.get(oldRel.Id).hed__Type__c &&
                    !validRelationshipTypes.contains(newRelationshipsMap.get(oldRel.Id).hed__Type__c)
                )
            ) {
                studentContactIds.add(oldRel.hed__Contact__c);
                relatedContactIds.add(oldRel.hed__RelatedContact__c);
                inactiveRelationships.add(oldRel);
            }
        }

        if (studentContactIds.isEmpty() || relatedContactIds.isEmpty()) {
            return;
        }

        Map<Id, List<LSB_SUT_SupportArrangement__c>> contactId2SupportArrangements =
            fetchSupportArrangementsForContact(studentContactIds);

        if (contactId2SupportArrangements.isEmpty()) {
            return;
        }

        Map<Id, List<LSB_SUT_SupportArrangement__Share>> contactId2SupportArrangementShare =
            fetchSharesForSupportArrangementsForContactId(contactId2SupportArrangements.values());

        if (contactId2SupportArrangementShare.isEmpty()) {
            return;
        }

        Map<Id, Id> relatedContactId2UserId = fetchContactToUserRelationByContactIds(relatedContactIds);

        for (hed__Relationship__c rel : inactiveRelationships) {
            if (contactId2SupportArrangementShare.containsKey(rel.hed__Contact__c)) {
                for (LSB_SUT_SupportArrangement__Share sas : contactId2SupportArrangementShare.get(rel.hed__Contact__c)) {
                    if (sas.UserOrGroupId == relatedContactId2UserId.get(rel.hed__RelatedContact__c)) {
                        sharesToDelete.put(sas.Id, sas);
                    }
                }
            }
        }

        delete sharesToDelete.values();
    }

    private static Map<Id, List<LSB_SUT_SupportArrangement__c>> fetchSupportArrangementsForContact(Set<Id> contactIds) {
            Map<Id, List<LSB_SUT_SupportArrangement__c>> contactId2SupportArrangements
                = new Map<Id, List<LSB_SUT_SupportArrangement__c>>();

        for (LSB_SUT_SupportArrangement__c sa : [
            SELECT Id, LSB_SUT_Contact__c
            FROM LSB_SUT_SupportArrangement__c
            WHERE LSB_SUT_Contact__c IN :contactIds
        ]) {
            if (contactId2SupportArrangements.containsKey(sa.LSB_SUT_Contact__c)) {
                contactId2SupportArrangements.get(sa.LSB_SUT_Contact__c).add(sa);
            } else {
                contactId2SupportArrangements.put(sa.LSB_SUT_Contact__c, new List<LSB_SUT_SupportArrangement__c>{sa});
            }
        }

        return contactId2SupportArrangements;
    }

    public static Map<Id, Id> fetchContactToUserRelationByContactIds(Set<Id> contactIds) {
        Map<Id, Id> contactId2UserId = new Map<Id, Id>();

        for (Contact c : [
            SELECT LSB_CON_User__c
            FROM Contact
            WHERE LSB_CON_ActiveUser__c = true
                AND LSB_CON_User__r.UserType = 'Standard'
                AND Id IN :contactIds
        ]) {
            contactId2UserId.put(c.Id, c.LSB_CON_User__c);
        }

        return contactId2UserId;
    }

    private static Map<Id, List<LSB_SUT_SupportArrangement__Share>> fetchSharesForSupportArrangementsForContactId(
        List<List<LSB_SUT_SupportArrangement__c>> supportArrangements
    ) {
        Map<Id, LSB_SUT_SupportArrangement__c> id2supportArrangement = new Map<Id, LSB_SUT_SupportArrangement__c>();
        Map<Id, List<LSB_SUT_SupportArrangement__Share>> contactId2SupportArrangementShare = new Map<Id, List<LSB_SUT_SupportArrangement__Share>>();

        for (List<LSB_SUT_SupportArrangement__c> saList : supportArrangements) {
            for (LSB_SUT_SupportArrangement__c sa : saList) {
                id2supportArrangement.put(sa.Id, sa);
            }
        }

        List<LSB_SUT_SupportArrangement__Share> supportArrangementShares = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE ParentId IN :id2supportArrangement.keySet()
                AND RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
        ];

        for (LSB_SUT_SupportArrangement__Share sas : supportArrangementShares) {
            if (contactId2SupportArrangementShare.containsKey(id2supportArrangement.get(sas.ParentId).LSB_SUT_Contact__c)) {
                contactId2SupportArrangementShare.get(id2supportArrangement.get(sas.ParentId).LSB_SUT_Contact__c).add(sas);
            } else {
                contactId2SupportArrangementShare.put(
                    id2supportArrangement.get(sas.ParentId).LSB_SUT_Contact__c,
                    new List<LSB_SUT_SupportArrangement__Share>{sas}
                );
            }
        }

        return contactId2SupportArrangementShare;
    }

    public static List<String> fetchValidTypesForSupportArrangementSharing() {
        List<String> validTypes = new List<String>();
        String typesConfig = LSB_Configuration__mdt.getInstance('VALID_TYPE_FOR_SUPPORT_ARRANGEMENT_SHARE').LSB_Value__c;
        validTypes = typesConfig.split(';');

        return validTypes;
    }
}