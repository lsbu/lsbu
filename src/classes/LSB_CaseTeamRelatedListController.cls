public with sharing class LSB_CaseTeamRelatedListController {

    @auraEnabled
    public static List<LSB_CaseTeamHelper.TeamMember> getTeamMembers(String caseId) {
        return LSB_CaseTeamHelper.getTeamMembers(caseId);

    }

    @auraEnabled
    public static void deleteTeamMember(String memberId) {
        LSB_CaseTeamHelper.deleteTeamMember(memberId);
    }

    @auraEnabled
    public static void deleteTeamTemplate(String templateId) {
        LSB_CaseTeamHelper.deleteCaseTeamTemplateRecord(templateId);
    }


}