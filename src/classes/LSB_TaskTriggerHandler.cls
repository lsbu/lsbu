public with sharing class LSB_TaskTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;
    private LSB_TaskHelper taskHelper;

    public LSB_TaskTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
        this.taskHelper = new LSB_TaskHelper();
    }

    public void beforeInsert(List<SObject> newList) {
        taskHelper.fillData((List<Task>) newList);
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {}

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> newItems) {}

}