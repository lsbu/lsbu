public without sharing class LSB_ECClaimService {

    private static final String MULTIPLE_CONFIG_RECORD_ERROR = 'Multiple config metadata records found. Please contact your Administrator.';
    private static final String YES_VALUE = 'Yes';

    public ClaimSaveResultWrapper createNewClaim(Case claimCase, hed__Course_Enrollment__c assessment) {
        Boolean isThirdOrMoreRequest = false;
        if (claimCase.LSB_CAS_RequestType__c == LSB_Constants.CLAIM_REQUEST_TYPE_DDS ) {
            return createDDSOrLateRequestTypeClaim(claimCase, isThirdOrMoreRequest);
        } else if (claimCase.LSB_CAS_RequestType__c == LSB_Constants.CLAIM_REQUEST_TYPE_LATE) {
            isThirdOrMoreRequest = checkIfItsThirdOrMoreCreateRequestAttempt(claimCase);
            return createDDSOrLateRequestTypeClaim(claimCase, isThirdOrMoreRequest);
        } else {
            return createECRequestTypeClaim(claimCase, assessment, isThirdOrMoreRequest);
        }
    }

    private ClaimSaveResultWrapper createDDSOrLateRequestTypeClaim(Case claimCase, Boolean isThirdOrMoreRequest) {
        LSB_EC_ResultConfig__mdt configRecord = getResultConfigByRequestTypeAndComponentTypeAndIsThirdSubmissionFlag(claimCase.LSB_CAS_RequestType__c,
                isThirdOrMoreRequest, false, false);
        if (configRecord != null) {
            Case createdCase = createClaimCase(claimCase, configRecord);
            return new ClaimSaveResultWrapper(createdCase, configRecord);
        }

        return null;
    }

    private ClaimSaveResultWrapper createECRequestTypeClaim(Case claimCase, hed__Course_Enrollment__c assessment, Boolean isThirdOrMoreRequest) {
        Boolean isRetrospective = false;
        if (assessment.LSB_CCN_HandInDate__c < Date.today()) {
            isRetrospective = true;
            isThirdOrMoreRequest = false;
        } else {
            isThirdOrMoreRequest = checkIfItsThirdOrMoreCreateRequestAttempt(claimCase);
        }

        Boolean isExam = false;
        if (assessment.LSB_CCN_ComponentType__c == LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM) {
            isExam = true;
        }

        Boolean submitWithin5Days = false;
        if (claimCase.LSB_CAS_SubmitWithin5WorkingDays__c == YES_VALUE) {
            submitWithin5Days = true;
        }

        Boolean ableToAttendExam = false;
        if (claimCase.LSB_CAS_AreYouAbleToAttendTheExam__c == YES_VALUE) {
            ableToAttendExam = true;
        }

        LSB_EC_ResultConfig__mdt configRecord = getResultConfigByByAllParameters(claimCase.LSB_CAS_RequestType__c,
                isThirdOrMoreRequest, isExam, isRetrospective, submitWithin5Days, ableToAttendExam, assessment.LSB_CCN_Resit__c);
        if (configRecord != null) {
            Case createdCase = createClaimCase(claimCase, configRecord);
            return new ClaimSaveResultWrapper(createdCase, configRecord);
        }

        return null;
    }


    private Case createClaimCase(Case claimCase, LSB_EC_ResultConfig__mdt configRecord) {
        claimCase.Status = configRecord.LSB_RequestStatus__c;
        claimCase.LSB_CAS_RejectionReason__c = configRecord.LSB_RejectionReason__c;
        claimCase.LSB_CAS_AutoRejected__c = LSB_Constants.CASE_REJECTION_REASON_AUTO == claimCase.LSB_CAS_RejectionReason__c;
        claimCase.LSB_CAS_Guidance__c = '';
        if(String.isNotBlank(configRecord.LSB_FirstParagraph__c)){
            claimCase.LSB_CAS_Guidance__c += '<p>' + configRecord.LSB_FirstParagraph__c + '</p>';
        }
        if(String.isNotBlank(configRecord.LSB_SecondParagraph__c)){
            claimCase.LSB_CAS_Guidance__c += '<p>' + configRecord.LSB_SecondParagraph__c + '</p>';
        }
        if(String.isNotBlank(configRecord.LSB_ThirdParagraph__c)){
            claimCase.LSB_CAS_Guidance__c += '<p>' + configRecord.LSB_ThirdParagraph__c + '</p>';
        }
        Id createCaseId = LSB_CaseHelper.saveAndReturnCaseId(claimCase);
        if (createCaseId != null) {
            return LSB_CaseHelper.getCaseById(createCaseId)[0];
        }
        return null;
    }

    private LSB_EC_ResultConfig__mdt getResultConfigByByAllParameters(String requestType, Boolean isThirdOrMoreRequest,
            Boolean isExam, Boolean retrospective, Boolean submitWithin5Days, Boolean ableToAttendExam, Boolean isResit) {
        List<LSB_EC_ResultConfig__mdt> configRecords = [SELECT LSB_RequestTypeApiName__c, LSB_AbleToSubmit5WorkingDays__c, LSB_IsRetrospective__c,
                LSB_RequestStatus__c, LSB_IsThirdOrMoreSubmission__c, LSB_FirstParagraph__c, LSB_SecondParagraph__c, LSB_ThirdParagraph__c, LSB_RejectionReason__c
        FROM LSB_EC_ResultConfig__mdt
        WHERE LSB_RequestTypeApiName__c =: requestType
        AND LSB_IsThirdOrMoreSubmission__c =: isThirdOrMoreRequest
        AND LSB_IsExam__c =: isExam
        AND LSB_AbleToSubmit5WorkingDays__c =: submitWithin5Days
        AND LSB_AbleToAttendExam__c =: ableToAttendExam
        AND LSB_IsResit__c =: isResit
        AND LSB_IsRetrospective__c =: retrospective];
        if (!configRecords.isEmpty()) {
            if (configRecords.size() > 1) {
                throw new AuraHandledException(MULTIPLE_CONFIG_RECORD_ERROR);
            }
            return configRecords[0];
        }
        return null;
    }

    private LSB_EC_ResultConfig__mdt getResultConfigByRequestTypeAndComponentTypeAndIsThirdSubmissionFlag(String requestType, Boolean isThirdOrMoreRequest,
            Boolean isExam, Boolean retrospective) {
        List<LSB_EC_ResultConfig__mdt> configRecords = [SELECT LSB_RequestTypeApiName__c, LSB_AbleToSubmit5WorkingDays__c, LSB_IsRetrospective__c,
                LSB_RequestStatus__c, LSB_IsThirdOrMoreSubmission__c, LSB_FirstParagraph__c, LSB_SecondParagraph__c, LSB_ThirdParagraph__c, LSB_RejectionReason__c
        FROM LSB_EC_ResultConfig__mdt
        WHERE LSB_RequestTypeApiName__c =: requestType
        AND LSB_IsThirdOrMoreSubmission__c =: isThirdOrMoreRequest
        AND LSB_IsExam__c =: isExam
        AND LSB_IsRetrospective__c =: retrospective];
        if (!configRecords.isEmpty()) {
            if (configRecords.size() > 1) {
                throw new AuraHandledException(MULTIPLE_CONFIG_RECORD_ERROR);
            }
            return configRecords[0];
        }
        return null;
    }

    public Boolean checkIfItsThirdOrMoreCreateRequestAttempt(Case claimCase) {
        List<Case> existingClaimCases = LSB_CaseHelper.getCasesByContactIdAndRecordTypeAndRequestType(claimCase.ContactId, claimCase.RecordTypeId, claimCase.LSB_CAS_RequestType__c);
        if (existingClaimCases.size() >= 2) {
            return true;
        }
        return false;
    }

    public class ClaimSaveResultWrapper {
        @AuraEnabled public String caseId { get; set; }
        @AuraEnabled public String caseNumber { get; set; }
        @AuraEnabled public String iconClass { get; set; }
        @AuraEnabled public String firstParagraph { get; set; }
        @AuraEnabled public String secondParagraph { get; set; }
        @AuraEnabled public String thirdParagraph { get; set; }
        @AuraEnabled public String status { get; set; }

        public ClaimSaveResultWrapper(Case claimCase, LSB_EC_ResultConfig__mdt configRecord) {
            this.caseId = claimCase.Id;
            this.caseNumber = claimCase.CaseNumber;
            this.iconClass = setIconName(configRecord.LSB_RequestStatus__c);
            this.firstParagraph = configRecord.LSB_FirstParagraph__c;
            this.secondParagraph = configRecord.LSB_SecondParagraph__c;
            this.thirdParagraph = configRecord.LSB_ThirdParagraph__c;
            this.status = configRecord.LSB_RequestStatus__c;
        }

        private String setIconName(String caseStatus) {
            if (caseStatus == LSB_Constants.CASE_STATUS_REQUEST_SUPPORTED) {
                return System.Label.LSB_ECRequestSupportedIconName;
            } else if (caseStatus == LSB_Constants.CASE_STATUS_REQUEST_PENDING) {
                return System.Label.LSB_ECRequestPendingIconName;
            } else {
                return System.Label.LSB_ECRequestRejectedIconName;
            }
        }
    }
}