@isTest(SeeAllData=true)
private class LSB_CommunityFeaturedTopicsCtrlTest {

    private static final String OFFER_HOLDER_USERNAME = 'mr.offer.holder@lsbu.com';

    private static void loadData() {
        Account accountRecord = new Account(Name = 'Administrative Test Account');
        insert accountRecord;

        Contact offerHolderContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
        insert offerHolderContact;

        Profile offerHolderProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.OFFER_HOLDER_PROFILE_NAME];
        User offerHolderUser = new User(
                Alias = 'jsnow',
                Email = offerHolderContact.Email,
                EmailEncodingKey = 'UTF-8',
                LastName = offerHolderContact.LastName,
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_GB',
                ProfileId = offerHolderProfile.Id,
                ContactId = offerHolderContact.Id,
                TimeZoneSidKey = 'Europe/London',
                UserName = OFFER_HOLDER_USERNAME);
        insert offerHolderUser;
    }

    @isTest(SeeAllData=true)
    private static void fetchTopicsTest() {
        loadData();
        User offerHolderUser = [SELECT Id FROM User WHERE Username = :OFFER_HOLDER_USERNAME];

        System.runAs(offerHolderUser) {
            List<ConnectApi.Topic> featuredTopics = LSB_CommunityFeaturedTopicsController.fetchTopics();

            if (!featuredTopics.isEmpty()) {
                System.assertNotEquals(null, featuredTopics.get(0).name);
                System.assertNotEquals(null, featuredTopics.get(0).id);
            }
        }
    }

    @isTest(SeeAllData=true)
    private static void getAllContentTopicsTest() {
        loadData();
        User offerHolderUser = [SELECT Id FROM User WHERE Username = :OFFER_HOLDER_USERNAME];

        System.runAs(offerHolderUser) {
            List<ConnectApi.Topic> featuredTopics = LSB_CommunityFeaturedTopicsController.getAllContentTopics();

            if (!featuredTopics.isEmpty()) {
                System.assertNotEquals(null, featuredTopics.get(0).name);
                System.assertNotEquals(null, featuredTopics.get(0).id);
            }
        }
    }

    @isTest(SeeAllData=true)
    private static void fetchTopicIdByArticleIdTest() {
        loadData();
        User offerHolderUser = [SELECT Id FROM User WHERE Username = :OFFER_HOLDER_USERNAME];

        System.runAs(offerHolderUser) {
            TopicAssignment topicAssignment = [
                SELECT TopicId, EntityId FROM TopicAssignment
                WHERE TopicId != null AND EntityId != NULL AND EntityKeyPrefix = :LSB_TopicHelper.keyPrefixOfKnowledge
                LIMIT 1];

            System.assertNotEquals(null, topicAssignment);
            Id topicId = LSB_CommunityFeaturedTopicsController.fetchTopicIdByEntityId(topicAssignment.EntityId);

            System.assertNotEquals(null, topicId);
        }
    }

    @isTest
    private static void fetchTopicIdByArticleIdNullTest() {
        loadData();
        User offerHolderUser = [SELECT Id FROM User WHERE Username = :OFFER_HOLDER_USERNAME];

        System.runAs(offerHolderUser) {
            Id topicId = LSB_CommunityFeaturedTopicsController.fetchTopicIdByEntityId(null);

            System.assertEquals(null, topicId);
        }
    }
}