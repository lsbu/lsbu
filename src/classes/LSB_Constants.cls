/**
 * @descritpion Class contains constants.
 *
 * @date 04.09.2020.
 */
public class LSB_Constants {

    // Community
    public static final String COMMUNITY_NAME = 'LSBU';
    public static final String LOGIN_STATUS_INVALID_PASSWORD = 'Invalid Password';
    public static final String LOGIN_STATUS_SUCCESS = 'Success';

    // Chat
    public static final String CHAT_SURVEY_NAME_MTD = 'CHAT_SURVEY_NAME';
    public static final String COMMUNITY_NAME_MTD = 'COMMUNITY_NAME';
    public static final String CHAT_KEY_PARAM_NAME = 'chatKey';
    public static final String CHAT_DETAILS_PARAM_NAME = 'chatDetails';
    public static final String CHAT_TRANSCRIPT_PARAM_NAME = 'transcript';
    public static final String CHAT_LAST_VISITED_PAGE_PARAM_NAME = 'lastVisitedPage';
    public static final String TEXT_FILE_EXTENSION_NAME = '.txt';

    // Profiles
    public static final String OFFER_HOLDER_PROFILE_NAME = 'Offer Holder';
    public static final String STUDENT_PROFILE_NAME = 'Student';
    public static final String ADVISOR_PROFILE_NAME = 'Advisor';

    // Global picklist values
    public static final String CONTACT_ROLE_STUDENT = 'Student';
    public static final String CONTACT_ROLE_APPLICANT = 'Applicant';
    public static final String CONTACT_ROLE_PROSPECT = 'Prospect';
    public static final String NATURE_OF_ENQUIRY_STUDENT = 'Student General Enquiry';
    public static final String NATURE_OF_ENQUIRY_APPLICANT = 'Offer Holder General Enquiry';
    public static final String NATURE_OF_ENQUIRY_PROSPECT = 'Prospect General Enquiry';


    // Objects
    public static final String SURVEY_OBJECT = 'Survey';
    public static final String NETWORK_OBJECT = 'Network';
    public static final String CASE_OBJECT = 'Case';
    public static final String CONTACT_OBJECT = 'Contact';

    // Case
    public static final String CASE_STATUS_RECEIVED = 'Received';
    public static final String CASE_STATUS_QUEUED = 'Queued';
    public static final String CASE_STATUS_CLOSED = 'Closed';
    public static final String CASE_STATUS_REQUEST_SUPPORTED = 'Request Supported';
    public static final String CASE_STATUS_REQUEST_REJECTED = 'Request Rejected';
    public static final String CASE_STATUS_REQUEST_PENDING = 'Request Pending';
    public static final String CASE_STATUS_REQUEST_BEING_REVIEWED = 'Request Being Reviewed';
    public static final String CASE_ORIGIN_WEB = 'Web';
    public static final String CASE_ORIGIN_EMAIL = 'Email';
    public static final String CASE_ORIGIN_SLC_HELP_DESK = 'SLC Help Desk';
    public static final String CASE_ORIGIN_GECKO_FORMS = 'Gecko Forms';
    public static final String CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME = 'LSB_CAS_Enquiry';
    public static final String CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME = 'LSB_CAS_ECClaim';
    public static final String CASE_EC_CLAIM_REQUEST_WITHDRAWN_BY_STUDENT = 'Request Withdrawn by Student';
    public static final String CASE_PROSPECT_CAPTURE_FORM_RECORD_TYPE_DEVELOPER_NAME = 'LSB_CAS_ProspectCaptureForm';
    public static final String CASE_SUPPLIED_EMAIL_FROM_MARKETING_PART = '@reply.comms.lsbu.ac.uk';
    public static final String CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME = 'AdviseeRecord';
    public static final String CASE_ADVISING_QUEUE_RECORD_TYPE_DEVELOPER_NAME = 'AdvisingQueue';
    public static final String CASE_CONTACT_ROLE_API_NAME = Schema.SObjectType.Case.fields.LSB_CAS_ContactRole__c.getName();
    public static final String CASE_NATURE_OF_ENQUIRY_API_NAME = Schema.SObjectType.Case.fields.LSB_CAS_NatureOfEnquiry__c.getName();
    public static final String CASE_TOPIC_API_NAME = Schema.SObjectType.Case.fields.LSB_CAS_Topic__c.getName();
    public static final String CASE_SUBJECT_API_NAME = Schema.SObjectType.Case.fields.Subject.getName();
    public static final String CASE_TOPIC_COMPLAINT = 'Complaint';
    public static final String CASE_TOPIC_TEACHING_AND_TIMETABLE = 'Timetable';
    public static final String CASE_NATURE_OF_ENQUIRY_COMPLAINT = 'Complaint';
    public static final String CASE_STAGE_OF_COMPLAINT_0 = '0';
    public static final String CASE_REJECTION_REASON_AUTO = 'REJ_AUTO';
    public static final String CASE_DECLARED_ROLE_LABEL = Schema.SObjectType.Case.fields.LSB_CAS_DeclaredRole__c.getLabel();
    public static final String CASE_SUBJECT_LABEL = Schema.SObjectType.Case.fields.Subject.getLabel();
    public static final String CASE_TOPIC_LABEL = Schema.SObjectType.Case.fields.LSB_CAS_Topic__c.getLabel();

    // Contact
    public static final String CONTACT_ROLE_API_NAME = Schema.SObjectType.Contact.fields.LSB_CON_CurrentRole__c.getName();
    public static final String CONTACT_FIRST_NAME_LABEL = Schema.SObjectType.Contact.fields.FirstName.getLabel();
    public static final String CONTACT_LAST_NAME_LABEL = Schema.SObjectType.Contact.fields.LastName.getLabel();
    public static final String CONTACT_EMAIL_LABEL = Schema.SObjectType.Contact.fields.Email.getLabel();


    public static final String CONTACT_SOURCE_SYSTEM_QL = 'QL';
    public static final String CONTACT_PREFERRED_PHONE_MOBILE = 'Mobile';
    public static final String CONTACT_PREFERRED_HOME_PHONE = 'Home';
    public static final String CONTACT_PREFERRED_EMAIL_PERSONAL = 'Personal Email';
    public static final String CONTACT_PREFERRED_EMAIL_UNIVERSITY = 'University';

    //Contact Point Consent
    public static final String CONTACT_POINT_CONSENT_NAME_LEGITIMATE_INTEREST = 'Email channel - legitimate interest';
    public static final String CONTACT_POINT_CONSENT_NAME_EXPLICIT_INTEREST = 'Email channel - explicit interest';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_EMAIL = 'Email';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_PHONE = 'Phone';
    public static final String CONTACT_POINT_CONSENT_PHONE_CALL = ' (Call)';
    public static final String CONTACT_POINT_CONSENT_PHONE_SMS = ' (SMS)';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS = 'MailingAddress';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS_LABEL = 'Mailing Address';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN = 'OptIn';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_NOTSEEN = 'NotSeen';
    public static final String CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT = 'OptOut';

    public static final String DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST = 'Explicit interest';
    public static final String DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST = 'Legitimate interest';

    // Review
    public static final String THREE_MONTHS_FOR_REVIEW = '3 Months';
    public static final String SIX_MONTHS_FOR_REVIEW = '6 Months';
    public static final String TWELVE_MONTHS_FOR_REVIEW = '12 Months';
    public static final String REVIEW_STATUS_NEW = 'New';
    public static final String REVIEW_STATUS_DONE = 'Done';
    public static final String REVIEW_STATUS_NO_LONGER_REQUIRED = 'No Longer Required';

    // Topics
    public static final String TOPIC_SLC_HELP_DESC_NAME = 'Student Life Centre Help Desk';

    // Custom Metadata
    public static final String DEFAULT_CONTACT_NAME_METADATA_NAME = 'DEFAULT_CONTACT_NAME';
    public static final String PRECHAT_NATURE_OF_ENQUIRY_TO_SKIP_METADATA_NAME = 'PRECHAT_NATURE_OF_ENQUIRY_TO_SKIP';
    public static final String PRECHAT_TOPIC_TO_SKIP_METADATA_NAME = 'PRECHAT_TOPIC_TO_SKIP';
    public static final String PRECHAT_ALLOWED_ROLES = 'PRECHAT_ALLOWED_ROLES';
    public static final String PRECHAT_STUDENT_ALLOWED_TOPICS = 'PRECHAT_STUDENT_ALLOWED_TOPICS';
    public static final String PRECHAT_APPLICANT_ALLOWED_TOPICS = 'PRECHAT_APPLICANT_ALLOWED_TOPICS';
    public static final String PRECHAT_PROSPECT_ALLOWED_TOPICS = 'PRECHAT_PROSPECT_ALLOWED_TOPICS';

    // Queue
    public static final String QUEUE_API_NAME_STUDENT_COMPLAINTS = 'LSB_StudentComplaints';

    // Task
    public static final String TASK_STATUS_COMPLETED = 'Completed';

    // Survey
    public static final String SURVEY_RESPONSE_STATUS_STARTED = 'Started';

    // Course Enrollment
    public static final String COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME = 'LSB_Module';
    public static final String COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME = 'LSB_ModuleComponent';
    public static final String COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_DEVELOPER_NAME = 'LSB_ModuleSubcomponent';
    public static final String ENROLLMENT_STATUS_FULLY_ENROLLED = 'EFE';
    public static final String ENROLLMENT_STATUS_OUTSTANDING_ENROL_REQS = 'EOER';
    public static final String ENROLLMENT_COMPONENT_TYPE_COURSEWORK = 'Coursework';
    public static final String ENROLLMENT_COMPONENT_TYPE_EXAM = 'Exam';
    public static final String CLAIM_REQUEST_TYPE_EC = 'Extenuating circumstances claim';
    public static final String CLAIM_REQUEST_TYPE_DDS = 'DDS late submission request';
    public static final String CLAIM_REQUEST_TYPE_LATE = 'Late submission request';

    //Self Assessment
    public static final String CATEGORY_PERSONAL_DEVELOPMENT = 'Strengths and areas for development';
    public static final String MY_PLAN_TAB = 'My Plan';

    //Address
    public static final String ADDRESS_TYPE_PERMANENT = 'Permanent';
}