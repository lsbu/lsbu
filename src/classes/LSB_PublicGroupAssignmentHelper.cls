/**
 * Created by ociszek001 on 10.12.2020.
 */

global class LSB_PublicGroupAssignmentHelper {

    @InvocableMethod
    public static void addUserToGroup(List<UserGroup> userGroupList) {
        List<Group> groupList = [
                SELECT Id,DeveloperName
                FROM Group
                WHERE DeveloperName = :userGroupList.get(0).groupName
                LIMIT 1
        ];
        List<GroupMember> groupMemberList = new List<GroupMember>();
        try {
            for (UserGroup ug : userGroupList) {
                GroupMember gm = new GroupMember(GroupId = groupList.get(0).Id, UserOrGroupId = ug.userId);
                groupMemberList.add(gm);
            }
            insert groupMemberList;
        }
        catch(Exception ex){
            System.debug(LoggingLevel.ERROR,ex.getMessage());
            throw ex;
        }
    }


    global class UserGroup {
        @InvocableVariable (label='UserId' description='Insert User Id' required=true)
        global Id userId;
        @InvocableVariable (label='Group DeveloperName' description='Insert DeveloperName of public group' required=true)
        global String groupName;
    }

}