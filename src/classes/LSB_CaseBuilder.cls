public without sharing class LSB_CaseBuilder {

    private String recordTypeId;
    private String requestType;
    private String relatedModule;
    private String relatedAssessment;
    private String submitWithin5WorkingDays;
    private String ableToSitTheExam;
    private String status;

    public LSB_CaseBuilder withRecordType(String recordTypeDeveloperName) {
        this.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();
        return this;
    }

    public LSB_CaseBuilder withRequestType(String requestType) {
        this.requestType = requestType;
        return this;
    }

    public LSB_CaseBuilder withSubmitWithin5WorkingDays(String submitWithin5WorkingDays) {
        this.submitWithin5WorkingDays = submitWithin5WorkingDays;
        return this;
    }

    public LSB_CaseBuilder withAbleToSitTheExam(String ableToSitTheExam) {
        this.ableToSitTheExam = ableToSitTheExam;
        return this;
    }

    public LSB_CaseBuilder withRelatedModule(String moduleId) {
        this.relatedModule = moduleId;
        return this;
    }

    public LSB_CaseBuilder withRelatedAssessment(String assessmentId) {
        this.relatedAssessment = assessmentId;
        return this;
    }

    public LSB_CaseBuilder withStatus(String status) {
        this.status = status;
        return this;
    }

    public Case create() {
        return new Case(
                LSB_CAS_RequestType__c= this.requestType,
                RecordTypeId = this.recordTypeId,
                LSB_CAS_RelatedModule__c = this.relatedModule,
                LSB_CAS_RelatedAssessment__c = this.relatedAssessment,
                LSB_CAS_SubmitWithin5WorkingDays__c = this.submitWithin5WorkingDays,
                LSB_CAS_AreYouAbleToAttendTheExam__c = this.ableToSitTheExam,
                Status = this.status
        );
    }

    public Case createAndSave() {
        Case newCase = this.create();
        insert newCase;
        return newCase;
    }
}