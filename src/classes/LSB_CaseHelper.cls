/**
 * @description Class contains helper methods that operates on the Case records.
 */
public without sharing class LSB_CaseHelper {

    private static Set<Id> processedCasesIds = new Set<Id>();
    private static String DEFAULT_CONTACT_NAME_METADATA_NAME = LSB_Constants.DEFAULT_CONTACT_NAME_METADATA_NAME;
    private static String CASE_ORIGIN_EMAIL = LSB_Constants.CASE_ORIGIN_EMAIL;
    private static String CASE_STATUS_RECEIVED = LSB_Constants.CASE_STATUS_RECEIVED;
    private static String CASE_TOPIC_COMPLAINT = LSB_Constants.CASE_TOPIC_COMPLAINT;
    private static String CASE_NATURE_OF_ENQUIRY_COMPLAINT = LSB_Constants.CASE_NATURE_OF_ENQUIRY_COMPLAINT;
    private static String CASE_STAGE_OF_COMPLAINT_0 = LSB_Constants.CASE_STAGE_OF_COMPLAINT_0;
    private static String DEFAULT_CONTACT_NAME;
    private static String QUEUE_STUDENT_COMPLAINTS_ID;
    private static Set<Id> existingContactIds = new Set<Id>();

    public static final Map<String, Id> CASE_RECORD_TYPE_DEVELOPER_NAME_TO_ID;
    public static final Map<Id, String> CASE_RECORD_TYPE_ID_TO_DEVELOPER_NAME;

    private static Map<String, String> CASE_FIELD_API_NAME_TO_CONTACT_FIELD_API_NAME = new Map<String, String>{
            'LSB_CAS_FirstName__c' => 'FirstName',
            'LSB_CAS_LastName__c' => 'LastName',
            'SuppliedEmail' => 'hed__AlternateEmail__c',
            'LSB_CAS_DateOfBirth__c' => 'Birthdate',
            'LSB_CAS_LSBUStudentId__c' => 'LSB_ExternalID__c',
            'LSB_CAS_UCASNo__c' => 'LSB_CON_UCASID__c',
            'LSB_CAS_Nationality__c' => 'hed__Citizenship__c',
            'LSB_CAS_CountryOfResidence__c' => 'hed__Country_of_Origin__c',
            'LSB_CAS_Disability__c' => 'LSB_CON_Disability__c',
            'LSB_CAS_Ethnicity__c' => 'hed__Ethnicity__c',
            'LSB_CAS_MobilePhone__c' => 'MobilePhone',
            'LSB_CAS_Title__c' => 'Title',
            'LSB_CAS_Gender__c' => 'hed__Gender__c'
    };

    static {
        Map<String, Id> caseRecordTypeDevName2Id = new Map<String, Id>();
        Map<Id, String> caseRecordTypeId2DeveloperName = new Map<Id,String>();
        Map<String, Schema.RecordTypeInfo> recordTypesByDeveloperName = Case.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName();

        for (String recordTypeDeveloperName : recordTypesByDeveloperName.keySet()) {
            caseRecordTypeDevName2Id.put(recordTypeDeveloperName, recordTypesByDeveloperName.get(recordTypeDeveloperName).getRecordTypeId());
            caseRecordTypeId2DeveloperName.put(recordTypesByDeveloperName.get(recordTypeDeveloperName).getRecordTypeId(), recordTypeDeveloperName);
        }

        CASE_RECORD_TYPE_DEVELOPER_NAME_TO_ID = caseRecordTypeDevName2Id;
        CASE_RECORD_TYPE_ID_TO_DEVELOPER_NAME = caseRecordTypeId2DeveloperName;
    }

    /**
     * @description Method updates ContactId field based on the contact data.
     */
    public static void fillContactData(List<Case> newCases) {
        Set<String> contactEmails = new Set<String>();
        List<LSB_ContactSelector.ContactData> cData = new List<LSB_ContactSelector.ContactData>();
        for (Case caseRecord : newCases) {
            if (caseRecord.Origin == LSB_Constants.CASE_ORIGIN_WEB) { // Case comes from Web Chat
                return;
            } else if (caseRecord.Origin == CASE_ORIGIN_EMAIL) { //Cases fom Email to Case
                retrieveDefaultContactNameFromMetadata();
                caseRecord.LSB_CAS_LastName__c = DEFAULT_CONTACT_NAME;
            }

            if (caseRecord.ContactId == null && caseRecord.SuppliedEmail != null) {
                contactEmails.add(convertWebEmail(caseRecord.SuppliedEmail).replaceAll('\\.', '%'));
            } else {
                existingContactIds.add(caseRecord.ContactId);
            }
        }

        Map<String, Contact> email2contact = LSB_ContactSelector.fetchContactsByEmails(contactEmails);

        for (Case caseRecord : newCases) {
            if (caseRecord.SuppliedEmail != null && email2contact.containsKey(convertWebEmail(caseRecord.SuppliedEmail))) {
                caseRecord.ContactId = email2contact.get(convertWebEmail(caseRecord.SuppliedEmail)).Id;
                existingContactIds.add(caseRecord.ContactId);
            } else if (caseRecord.ContactId == null && caseRecord.SuppliedEmail != null && caseRecord.LSB_CAS_LastName__c != null) {
                cData.add(createContactDataFromCase(caseRecord));
            }
        }

        if (cData.isEmpty()) {
            return;
        }

        Map<String, Contact> contactDataId2contact = LSB_ContactSelector.fetchContactsByContactData(cData);
        Map<String, Contact> contactsToCreate = new Map<String, Contact>();

        for (Case caseRecord : newCases) {
            if (caseRecord.ContactId == null && caseRecord.SuppliedEmail != null) {
                LSB_ContactSelector.ContactData data = createContactDataFromCase(caseRecord);
                if (contactDataId2contact.containsKey(data.id)) {
                    caseRecord.ContactId = contactDataId2contact.get(data.id).Id;
                    existingContactIds.add(caseRecord.ContactId);
                } else {
                    contactsToCreate.put(caseRecord.SuppliedEmail, data.returnContactRecord());
                }
            }
        }

        try {
            insert contactsToCreate.values();

            for (Case caseRecord : newCases) {
                if (caseRecord.ContactId == null && caseRecord.SuppliedEmail != null && contactsToCreate.containsKey(caseRecord.SuppliedEmail)) {
                    caseRecord.ContactId = contactsToCreate.get(caseRecord.SuppliedEmail).Id;
                }
            }
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Exception message: ' + ex.getMessage());
        }
    }

    public static void handleContactDataUpdate(List<Case> newCases) {
        Map<Id, Case> contactId2Case = new Map<Id, Case>();

        for (Case caseRecord : newCases) {
            if (caseRecord.RecordTypeId.equals(CASE_RECORD_TYPE_DEVELOPER_NAME_TO_ID.get(LSB_Constants.CASE_PROSPECT_CAPTURE_FORM_RECORD_TYPE_DEVELOPER_NAME)) && caseRecord.ContactId != null) {
                contactId2Case.put(caseRecord.ContactId, caseRecord);
            }
        }

        if (contactId2Case.isEmpty()) {
            return;
        }

        try {
            List<Contact> contactsToUpdate = new List<Contact>();
            String prospectQuery = 'SELECT ';

            for (String prospectField : CASE_FIELD_API_NAME_TO_CONTACT_FIELD_API_NAME.values()) {
                prospectQuery += prospectField + ', ';
            }

            Set<Id> contactIds = contactId2Case.keySet();

            prospectQuery += ' Id FROM Contact WHERE LSB_CON_CurrentRole__c = \'' +
                    LSB_Constants.CONTACT_ROLE_PROSPECT + '\' AND Id IN :contactIds';

            for (Contact prospect : Database.query(prospectQuery)) {
                Case prospectCaptureForm = contactId2Case.get(prospect.Id);
                Boolean isDataUpdate = false;

                for (String caseField : CASE_FIELD_API_NAME_TO_CONTACT_FIELD_API_NAME.keySet()) {
                    String contactField = CASE_FIELD_API_NAME_TO_CONTACT_FIELD_API_NAME.get(caseField);

                    if (prospectCaptureForm.get(caseField) != null && prospect.get(contactField) != prospectCaptureForm.get(caseField)) {
                        isDataUpdate = true;

                        if (contactField.equals('MobilePhone') && prospectCaptureForm.LSB_CAS_InternationalDialCode__c != null) {
                            prospect.put(contactField, prospectCaptureForm.LSB_CAS_InternationalDialCode__c + ' ' + prospectCaptureForm.get(caseField));
                        } else {
                            prospect.put(contactField, prospectCaptureForm.get(caseField));
                        }
                    }
                }

                if (isDataUpdate) {
                    contactsToUpdate.add(prospect);
                }
            }

            if (!contactsToUpdate.isEmpty()) {
                update contactsToUpdate;
            }
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Exception message: ' + ex.getMessage());
        }
    }

    public static void handleAddressesUpdate(List<Case> newCases) {
        Map<Id, Case> contactId2Case = new Map<Id, Case>();
        Set<Id> contactIdsToProcess = new Set<Id>();

        for (Case caseRecord : newCases) {
            if (caseRecord.RecordTypeId.equals(CASE_RECORD_TYPE_DEVELOPER_NAME_TO_ID.get(LSB_Constants.CASE_PROSPECT_CAPTURE_FORM_RECORD_TYPE_DEVELOPER_NAME)) &&
                caseRecord.ContactId != null && caseRecord.LSB_CAS_AddressLine1__c != null && caseRecord.LSB_CAS_Postcode__c != null) {
                contactId2Case.put(caseRecord.ContactId, caseRecord);
                contactIdsToProcess.add(caseRecord.ContactId);
            }
        }

        if (contactId2Case.isEmpty()) {
            return;
        }

        List<hed__Address__c> addresses = [
                SELECT Id, hed__Parent_Contact__c, hed__MailingStreet__c, hed__MailingPostalCode__c,hed__MailingCity__c,
                        hed__MailingCountry__c, hed__MailingState__c, hed__MailingStreet2__c
                FROM hed__Address__c
                WHERE hed__Parent_Contact__c IN :contactId2Case.keySet() AND
                    hed__Parent_Contact__r.LSB_CON_CurrentRole__c != :LSB_Constants.CONTACT_ROLE_APPLICANT AND
                    hed__Parent_Contact__r.LSB_CON_ContactRole__c != :LSB_Constants.CONTACT_ROLE_STUDENT];

        Map<Id, hed__Address__c> addressesToUpdate = new Map<Id, hed__Address__c>();

        for (hed__Address__c address : addresses) {
            if (contactId2Case.get(address.hed__Parent_Contact__c) != null && address.hed__MailingStreet__c != null && address.hed__MailingPostalCode__c != null &&
                    address.hed__MailingStreet__c.equalsIgnoreCase(contactId2Case.get(address.hed__Parent_Contact__c).LSB_CAS_AddressLine1__c)  &&
                    address.hed__MailingPostalCode__c.equalsIgnoreCase(contactId2Case.get(address.hed__Parent_Contact__c).LSB_CAS_Postcode__c)) {

                Case prospectCaptureForm = contactId2Case.get(address.hed__Parent_Contact__c);
                Boolean isDefaultAddress = prospectCaptureForm.LSB_CAS_PostOptIn__c;
                hed__Address__c addressToUpdate = new hed__Address__c(Id = address.Id);

                if (isDefaultAddress) {
                    addressToUpdate.hed__Default_Address__c = isDefaultAddress;
                }

                addressToUpdate.hed__MailingCity__c = String.isBlank(addressToUpdate.hed__MailingCity__c) ? contactId2Case.get(address.hed__Parent_Contact__c).LSB_CAS_City__c : addressToUpdate.hed__MailingCity__c;
                addressToUpdate.hed__MailingCountry__c = String.isBlank(addressToUpdate.hed__MailingCountry__c) ? contactId2Case.get(address.hed__Parent_Contact__c).LSB_CAS_Country__c : addressToUpdate.hed__MailingCountry__c;
                addressToUpdate.hed__MailingStreet2__c = String.isBlank(addressToUpdate.hed__MailingStreet2__c) ? contactId2Case.get(address.hed__Parent_Contact__c).LSB_CAS_AddressLine2__c : addressToUpdate.hed__MailingStreet2__c;
                addressesToUpdate.put(address.Id, addressToUpdate);
                contactIdsToProcess.remove(address.hed__Parent_Contact__c);
            }
        }

        List<ContactPointConsent> consentsToUpdate = new List<ContactPointConsent>();
        String explicitInterest = '%' + LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST;

        for (ContactPointAddress contactPoint : [SELECT Id, LSB_COA_Address__c, LSB_COA_Contact__c,
                (SELECT Id, PrivacyConsentStatus FROM ContactPointConsents WHERE LSB_ExternalID__c LIKE :explicitInterest)
            FROM ContactPointAddress WHERE LSB_COA_Address__c IN :addressesToUpdate.keySet()]) {
            Case prospectCaptureForm = contactId2Case.get(contactPoint.LSB_COA_Contact__c);
            Boolean isDefaultAddress = prospectCaptureForm.LSB_CAS_PostOptIn__c;
            
            for (ContactPointConsent consent : contactPoint.ContactPointConsents) {
                if ((isDefaultAddress && !consent.PrivacyConsentStatus.equals(LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN)) ||
                        (!isDefaultAddress && !consent.PrivacyConsentStatus.equals(LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT))) {
                    consent.PrivacyConsentStatus = isDefaultAddress ?
                            LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN :
                            LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT;
                    consentsToUpdate.add(consent);
                }
            }
        }

        List<Contact> contactsToUpdate = new List<Contact>();
        List<hed__Address__c> addressesToInsert = new List<hed__Address__c>();

        for (Contact contactRecord : [SELECT Id FROM Contact WHERE Id IN :contactIdsToProcess AND
            LSB_CON_CurrentRole__c != :LSB_Constants.CONTACT_ROLE_APPLICANT AND
            LSB_CON_ContactRole__c != :LSB_Constants.CONTACT_ROLE_STUDENT]) {

            Id contactId = contactRecord.Id;
            Case prospectCaptureForm = contactId2Case.get(contactId);
            Boolean isDefaultAddress = prospectCaptureForm.LSB_CAS_PostOptIn__c;

            addressesToInsert.add(new hed__Address__c(
                    hed__Parent_Contact__c = contactId,
                    hed__Default_Address__c = isDefaultAddress,
                    hed__Address_Type__c = LSB_Constants.ADDRESS_TYPE_PERMANENT,
                    hed__MailingStreet__c = prospectCaptureForm.LSB_CAS_AddressLine1__c,
                    hed__MailingPostalCode__c = prospectCaptureForm.LSB_CAS_Postcode__c,
                    hed__MailingCity__c = prospectCaptureForm.LSB_CAS_City__c,
                    hed__MailingCountry__c = prospectCaptureForm.LSB_CAS_Country__c,
                    hed__MailingStreet2__c = prospectCaptureForm.LSB_CAS_AddressLine2__c,
                    LSB_ADS_ExplicitInterestConsentStatus__c = (prospectCaptureForm.LSB_CAS_PostOptIn__c ?
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN :
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT)
            ));

            if (isDefaultAddress) {
                contactsToUpdate.add(new Contact(
                        Id = contactId,
                        MailingStreet = prospectCaptureForm.LSB_CAS_AddressLine1__c,
                        MailingPostalCode = prospectCaptureForm.LSB_CAS_Postcode__c,
                        MailingCity = prospectCaptureForm.LSB_CAS_City__c,
                        MailingCountry = prospectCaptureForm.LSB_CAS_Country__c
                ));
            }
        }

        try {
            List<hed__Address__c> addressesToUpsert = new List<hed__Address__c>();
            addressesToUpsert.addAll(addressesToInsert);
            addressesToUpsert.addAll(addressesToUpdate.values());

            if (!addressesToUpsert.isEmpty()) {
                upsert addressesToUpsert;
            }

            if (!contactsToUpdate.isEmpty()) {
                update contactsToUpdate;
            }

            if (!consentsToUpdate.isEmpty()) {
                update consentsToUpdate;
            }
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Exception message: ' + ex.getMessage());
        }
    }

    /**
     * @description Method assigns Entitlement to the Case.
     */
    public static void assignEntitlement(List<Case> newCases) {
        Map<String, Id> entitlementName2Id = new Map<String, Id>();
        for (Entitlement e : [
                SELECT Id, Name
                FROM Entitlement
                WHERE Account.RecordType.DeveloperName = 'LSB_ACC_Technical']
        ) {
            entitlementName2Id.put(e.Name, e.Id);
        }

        if (entitlementName2Id.keySet().isEmpty()) {
            return;
        }

        Map<String, String> caseRecordType2EntitlementName = new Map<String, String>();
        for (LSBU_CaseEntitlementMapping__mdt mapping : LSBU_CaseEntitlementMapping__mdt.getAll().values()) {
            caseRecordType2EntitlementName.put(mapping.LSB_CaseRecordType__c, mapping.LSB_EntitlementName__c);
        }
        if (caseRecordType2EntitlementName.keySet().isEmpty()) {
            return;
        }

        for (Case caseRecord : newCases) {
            String entitlementName = caseRecordType2EntitlementName.get(CASE_RECORD_TYPE_ID_TO_DEVELOPER_NAME.get(caseRecord.RecordTypeId));
            if (String.isNotBlank(entitlementName)) {
                caseRecord.EntitlementId = entitlementName2Id.get(entitlementName);
            }
        }
    }

    /**
     * @description Method fill Web Email field with value taken from the Contact.
     */
    public static void fillWebEmail(List<Case> newCases) {
        Set<Id> contactIds = new Set<Id>();

        for (Case caseRecord : newCases) {
            if (caseRecord.ContactId != null && String.isBlank(caseRecord.SuppliedEmail) || String.isBlank(caseRecord.LSB_CAS_ContactRole__c)) {
                contactIds.add(caseRecord.ContactId);
            }
        }

        if (contactIds.isEmpty()) {
            return;
        }

        Map<Id, Contact> fetchedContactsByIds = new Map<Id, Contact>([SELECT Id, Email, LSB_CON_ContactRole__c FROM Contact WHERE Id IN :contactIds]);

        for (Case caseRecord : newCases) {
            if (caseRecord.ContactId != null && String.isBlank(caseRecord.SuppliedEmail)) {
                caseRecord.SuppliedEmail = fetchedContactsByIds.get(caseRecord.ContactId).Email;
            }

            if (caseRecord.ContactId != null && String.isBlank(caseRecord.LSB_CAS_ContactRole__c)) {
                caseRecord.LSB_CAS_ContactRole__c = fetchedContactsByIds.get(caseRecord.ContactId).LSB_CON_ContactRole__c;
            }
        }
    }

    /**
     * @description Method fill Description field based on the Subject.
     */
    public static void fillDescription(List<Case> newCases) {
        for (Case caseRecord : newCases) {
            if (String.isBlank(caseRecord.Description) && String.isNotBlank(caseRecord.Subject)) {
                caseRecord.Description = caseRecord.Subject;
            }
        }
    }

    /**
     * @description Method using to update contact's consents
     */
    public static void updateConsents(Map<Id, Case> newCases) {
        List<ContactPointConsent> consentsToUpdate = new List<ContactPointConsent>();
        Set<String> consentExternalIdsForExistingContacts = new Set<String>();
        Map<Id, Id> existingContactId2CaseId = new Map<Id, Id>();
        Set<String> consentExternalIdsForNewContacts = new Set<String>();
        Map<Id, Id> newContactId2CaseId = new Map<Id, Id>();

        for (Case newCase : newCases.values()) {
            if (newCase.RecordTypeId.equals(CASE_RECORD_TYPE_DEVELOPER_NAME_TO_ID.get(LSB_Constants.CASE_PROSPECT_CAPTURE_FORM_RECORD_TYPE_DEVELOPER_NAME)) &&
                    String.isNotBlank(newCase.ContactId) && existingContactIds.contains(newCase.ContactId)) {
                consentExternalIdsForExistingContacts.add(newCase.ContactId + '%' + '#' + LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST);
                existingContactId2CaseId.put(newCase.ContactId, newCase.Id);
            } else if (newCase.RecordTypeId.equals(CASE_RECORD_TYPE_DEVELOPER_NAME_TO_ID.get(LSB_Constants.CASE_PROSPECT_CAPTURE_FORM_RECORD_TYPE_DEVELOPER_NAME)) &&
                    String.isNotBlank(newCase.ContactId) && String.isNotBlank(newCase.SuppliedEmail) && !existingContactIds.contains(newCase.ContactId)) {
                consentExternalIdsForNewContacts.add(newCase.ContactId + '%' + '#' + LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST);
                newContactId2CaseId.put(newCase.ContactId, newCase.Id);
            }
        }

        List<ContactPointConsent> consentsToUpdateForExistingContacts = updateContactPointConsents(consentExternalIdsForExistingContacts, existingContactId2CaseId, newCases, true);
        List<ContactPointConsent> consentsToUpdateForNewContacts = updateContactPointConsents(consentExternalIdsForNewContacts, newContactId2CaseId, newCases, false);

        consentsToUpdate.addAll(consentsToUpdateForExistingContacts);
        consentsToUpdate.addAll(consentsToUpdateForNewContacts);

        if (!consentsToUpdate.isEmpty()) {
            update consentsToUpdate;
        }
    }

    private static List<ContactPointConsent> updateContactPointConsents(Set<String> consentExternalIds, Map<Id, Id> contactId2CaseId, Map<Id, Case> newCases, Boolean skipMailingAddress) {
        List<ContactPointConsent> consentsToUpdate = new List<ContactPointConsent>();

        for (ContactPointConsent consent : [
                SELECT Id, CaptureContactPointType, PrivacyConsentStatus, LSB_COC_Subtype__c, LSB_COC_Contact__c,
                        LSB_COC_Contact__r.LSB_CON_ContactRole__c
                FROM ContactPointConsent
                WHERE LSB_ExternalId__c LIKE :consentExternalIds]) {

            if (skipMailingAddress && consent.CaptureContactPointType == LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS) {
                continue;
            }

            if (!String.isNotBlank(consent.LSB_COC_Contact__r.LSB_CON_ContactRole__c) &&
                    !consent.LSB_COC_Contact__r.LSB_CON_ContactRole__c.equals(LSB_Constants.CONTACT_ROLE_PROSPECT)) {
                continue;
            }

            Case newCase = newCases.get(contactId2CaseId.get(consent.LSB_COC_Contact__c));
            Boolean consentValue = (Boolean) newCase.get(LSB_ContactPointHelper.getCaseConsentFieldAPI(consent));
            String consentStatus = LSB_ContactPointHelper.convertBooleanToPrivacyStatus(consentValue);

            if (!consent.PrivacyConsentStatus.equals(consentStatus)) {
                consent.PrivacyConsentStatus = consentStatus;
                consent.CaptureDate = Datetime.now();
            } else {
                if (consentValue) {
                    consent.EffectiveFrom = Datetime.now();
                } else {
                    consent.EffectiveTo = Datetime.now();
                }
            }

            consentsToUpdate.add(consent);
        }

        return consentsToUpdate;
    }

    /**
     * @description Method adds useDefaultRule flag to all updated Cases. Was done to trigger case assignment rules for every Case
     * update.
     */
    public static void triggerCaseAssignmentRules(List<Case> newCases) {
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.useDefaultRule = true;
        List<Case> casesToUpdate = new List<Case>();
        Id adviseeCaseRecordTypeId = LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME);

        for (Case c : newCases) {
            if (processedCasesIds.contains(c.Id) || c.Origin == LSB_Constants.CASE_ORIGIN_EMAIL || c.RecordTypeId == adviseeCaseRecordTypeId) {
                continue;
            }

            Case cs = new Case(Id = c.Id);
            cs.setOptions(dmlOpts);
            casesToUpdate.add(cs);
            processedCasesIds.add(c.Id);
        }

        update casesToUpdate;
    }

    public static void processNewComplaintsCases(List<Case> newCases) {
        retrieveComplaintsQueueId();
        Map<Id, Case> contactId2Complaints = new Map<Id, Case>();
        for (Case caseRecord : newCases) {
            Id enquiryRecordTypeId = LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME);
            if (caseRecord.OwnerId != null && caseRecord.RecordTypeId == enquiryRecordTypeId &&
                    caseRecord.OwnerId == QUEUE_STUDENT_COMPLAINTS_ID && caseRecord.ContactId != null) {
                contactId2Complaints.put(caseRecord.ContactId, caseRecord);
            }
        }
        assignValuesToStudentComplaintsCaseFields(contactId2Complaints);
    }

    public static void fillQueueIdTechnicalFieldOnRelatedChangeTranscripts(List<Case> newCases, Map<Id, Case> oldCases) {
        Map<Id, Id> caseIdToQueueId = new Map<Id, Id>();

        for (Case newRecord : newCases) {
            if (newRecord.OwnerId != oldCases.get(newRecord.Id).OwnerId &&
                    String.valueOf(newRecord.OwnerId).startsWith('00G')) {
                caseIdToQueueId.put(newRecord.Id, newRecord.OwnerId);
            }
        }

        List<LiveChatTranscript> chatTranscripts = new List<LiveChatTranscript>();

        for (LiveChatTranscript transcriptRecord : [SELECT Id, CaseId, Case.OwnerId, LSB_QueueId__c FROM LiveChatTranscript WHERE CaseId IN :caseIdToQueueId.keySet()]) {
            if (transcriptRecord.LSB_QueueId__c == transcriptRecord.Case.OwnerId) {
                continue;
            }

            transcriptRecord.LSB_QueueId__c = transcriptRecord.Case.OwnerId;
            chatTranscripts.add(transcriptRecord);
        }

        if (!chatTranscripts.isEmpty()) {
            update chatTranscripts;
        }
    }

    private static void assignValuesToStudentComplaintsCaseFields(Map<Id, Case> contactId2Complaints) {
        List<Contact> contacts = [SELECT ID, LSB_CON_ContactRole__c FROM Contact WHERE Id IN : contactId2Complaints.keySet()];
        for (Contact contact : contacts) {
            if (contact.LSB_CON_ContactRole__c == LSB_Constants.CONTACT_ROLE_STUDENT) {
                Case complaintToUpdate = contactId2Complaints.get(contact.Id);
                complaintToUpdate.LSB_CAS_NatureOfEnquiry__c = CASE_NATURE_OF_ENQUIRY_COMPLAINT;
                complaintToUpdate.LSB_CAS_Topic__c = CASE_TOPIC_COMPLAINT;
                complaintToUpdate.LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_STUDENT;
                complaintToUpdate.Status = CASE_STATUS_RECEIVED;
                complaintToUpdate.LSB_CAS_StageOfComplaint__c = CASE_STAGE_OF_COMPLAINT_0;
            }
        }
    }

    private static String convertWebEmail(String webEmail) {
        if (webEmail.contains(LSB_Constants.CASE_SUPPLIED_EMAIL_FROM_MARKETING_PART)) {
            webEmail = webEmail.substringBefore(LSB_Constants.CASE_SUPPLIED_EMAIL_FROM_MARKETING_PART);
        } else {
            webEmail = webEmail.replaceAll('@', '\\.');
        }

        return webEmail;
    }

    public static String retrieveDefaultContactNameFromMetadata() {
        if (DEFAULT_CONTACT_NAME == null) {
            List<LSB_Configuration__mdt> configRecords = [
                    SELECT DeveloperName, LSB_Value__c, LSB_Description__c
                    FROM LSB_Configuration__mdt
                    WHERE DeveloperName = :DEFAULT_CONTACT_NAME_METADATA_NAME
            ];
            if (configRecords.isEmpty()) {
                DEFAULT_CONTACT_NAME = 'Unknown';
                return DEFAULT_CONTACT_NAME;
            }
            DEFAULT_CONTACT_NAME = configRecords[0].LSB_Value__c;
        }
        return DEFAULT_CONTACT_NAME;
    }

    private static void retrieveComplaintsQueueId() {
        if (QUEUE_STUDENT_COMPLAINTS_ID == null) {
            List<Group> groups = [SELECT Id FROM Group WHERE DeveloperName =: LSB_Constants.QUEUE_API_NAME_STUDENT_COMPLAINTS];
            if (groups.isEmpty()) {
                return;
            }
            QUEUE_STUDENT_COMPLAINTS_ID = groups[0].Id;
        }
    }

    private static LSB_ContactSelector.ContactData createContactDataFromCase(Case caseRecord) {
        LSB_ContactSelector.ContactData contactData = new LSB_ContactSelector.ContactData(
                caseRecord.LSB_CAS_FirstName__c,
                caseRecord.LSB_CAS_LastName__c,
                caseRecord.LSB_CAS_MobilePhone__c,
                caseRecord.SuppliedEmail);
        return contactData;
    }

    public static List<CaseArticle> getRelatedCaseArticles(Set<Id> caseIds) {
        return [
                SELECT KnowledgeArticleVersionId
                FROM CaseArticle
                WHERE CaseId = :caseIds
        ];
    }

    public static List<Case> getCasesByContactIdAndRecordTypeAndRequestType(Id contactId, Id recordTypeId, String requestType) {
        return [SELECT Id, CaseNumber
        FROM Case
        WHERE ContactId =: contactId AND RecordTypeId =: recordTypeId AND LSB_CAS_RequestType__c =: requestType];
    }

    public static List<Case> getCaseById(Id caseId) {
        return [SELECT Id, CaseNumber
        FROM Case
        WHERE Id =: caseId];
    }

    public static void insertCaseArticles(List<CaseArticle> caseArticles) {
        insert caseArticles;
    }

    public static void insertArticleRatings(List<LSB_AR_ArticleRating__c> articleRatings) {
        insert articleRatings;
    }

    public static Id saveAndReturnCaseId(Case caseToCreate) {
        insert caseToCreate;
        return caseToCreate.Id;
    }

    public static void updateRelatedAssessments(List<Case> newCases) {
        updateRelatedAssessments(newCases, null);
    }

    public static void updateRelatedAssessments(List<Case> newCases, Map<Id, Case> oldCasesMap) {
        Id ecClaimRecordTypeId = LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME);
        Map<Id, hed__Course_Enrollment__c> courseEnrollments = new Map<Id, hed__Course_Enrollment__c>();
        for (Case newCase : newCases) {
            if (newCase.LSB_CAS_RelatedAssessment__c != null
                && newCase.RecordTypeId == ecClaimRecordTypeId
                && (oldCasesMap == null || newCase.Status != oldCasesMap.get(newCase.Id).Status)
                ) {
                courseEnrollments.put(newCase.LSB_CAS_RelatedAssessment__c,
                    new hed__Course_Enrollment__c(
                        Id = newCase.LSB_CAS_RelatedAssessment__c,
                        LSB_CCN_EcLateSubmission__c = true,
                        LSB_CCN_EcLateSubmissionStatus__c = newCase.Status,
                        LSB_CCN_RequestType__c = newCase.LSB_CAS_RequestType__c,
                        LSB_CCN_LatestEcClaim__c = newCase.Id
                    )
                );
            }
        }
        update courseEnrollments.values();
    }
}