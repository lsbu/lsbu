public without sharing class LSB_CaseActivitiesHelper {

    public static List<LSB_CaseActivitiesHelper.MessageWrapper> getCaseMessagesByParentId(Id parentRecordId) {
        List<LSB_CaseActivitiesHelper.MessageWrapper> messages = LSB_CaseActivitiesHelper.getMessagesFromChatterFeeds(fetchCaseComments(parentRecordId));
        List<EmailMessage> emailHistory = getEmailHistory(parentRecordId);
        List<EmailMessage> filteredEmails = filterCurrentUserContactEmailMessages(emailHistory);
        messages.addAll(getMessagesFromEmailHistory(filteredEmails));
        return messages;
    }

    private static List<LSB_CaseActivitiesHelper.MessageWrapper> getMessagesFromChatterFeeds(List<CaseComment> caseComments) {
        List<LSB_CaseActivitiesHelper.MessageWrapper> messageWrappers = new List<LSB_CaseActivitiesHelper.MessageWrapper>();
        if (!caseComments.isEmpty()) {
            for (CaseComment caseComment : caseComments) {
                messageWrappers.add(new MessageWrapper(caseComment));
            }
        }
        return messageWrappers;
    }

    private static List<CaseComment> fetchCaseComments(Id parentRecordId) {
        return [
                SELECT Id,
                        CreatedDate,
                        TYPEOF CreatedBy
                                WHEN User THEN
                                FirstName,
                                LastName,
                                SmallPhotoUrl
                        END,
                        CommentBody
                FROM CaseComment
                WHERE ParentId = :parentRecordId
                AND IsPublished = true
                ORDER BY CreatedDate ASC
        ];
    }

    private static List<EmailMessage> getEmailHistory(Id parentRecordId) {
        List<EmailMessage> emails = new List<EmailMessage>();

        for (Case singleCase : [
                SELECT Id, (
                        SELECT
                                ActivityId,
                                CreatedDate,
                                FromAddress,
                                FromName,
                                HtmlBody,
                                Id,
                                Incoming,
                                Subject,
                                SystemModstamp,
                                TextBody,
                                ToAddress
                        FROM EmailMessages
                )
                FROM Case
                WHERE Id = :parentRecordId
        ]) {
            for (EmailMessage emailMessage : singleCase.EmailMessages) {
                emails.add(emailMessage);
            }
        }
        return emails;
    }

    private static List<LSB_CaseActivitiesHelper.MessageWrapper> getMessagesFromEmailHistory(List<EmailMessage> emails) {
        List<LSB_CaseActivitiesHelper.MessageWrapper> messageWrappers = new List<LSB_CaseActivitiesHelper.MessageWrapper>();
        if (!emails.isEmpty()) {
            for (EmailMessage emailMessage : emails) {
                messageWrappers.add(new MessageWrapper(emailMessage));
            }
        }
        return messageWrappers;
    }

    @TestVisible
    private static List<EmailMessage> filterCurrentUserContactEmailMessages(List<EmailMessage> emailHistory) {
        List<EmailMessage> filteredEmail = new List<EmailMessage>();
        Contact currentUserContact = getCurrentUserContact();
        if (currentUserContact != null && String.isNotBlank(currentUserContact.Email)) {
            for( EmailMessage singleEmail : emailHistory) {
                if (singleEmail.ToAddress == currentUserContact.Email || singleEmail.FromAddress == currentUserContact.Email) {
                    if (singleEmail.FromAddress == currentUserContact.Email) {
                        singleEmail.FromName = currentUserContact.Name;
                    }
                    filteredEmail.add(singleEmail);
                }
            }
        }
        return filteredEmail;
    }

    private static Contact getCurrentUserContact() {
        List<Contact> contacts = [
                SELECT Id, Name, Email
                FROM Contact
                WHERE Id IN (
                        SELECT ContactId
                        FROM User
                        WHERE Id = :UserInfo.getUserId()
                )
        ];
        if (!contacts.isEmpty()) {
            return contacts[0];
        }
        return null;
    }

    public class MessageWrapper {
        @AuraEnabled public Datetime createdDate;
        @AuraEnabled public EmailMessage emailMessage;
        @AuraEnabled public CaseComment caseComment;

        public MessageWrapper(EmailMessage emailMessage) {
            this.createdDate = emailMessage.CreatedDate;
            this.emailMessage = emailMessage;
        }

        public MessageWrapper(CaseComment caseComment) {
            this.createdDate = caseComment.CreatedDate;
            this.caseComment = caseComment;
        }

    }
}