@IsTest
public with sharing class LSB_CommCreateClaimControllerTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman720275@pwc.com';
    private static final String COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME = LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME;

    @TestSetup
    private static void setup() {
        Account testAccount = new Account(Name='Test Account Name');
        insert testAccount;
        Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                Email = COMMUNITY_USER_EMAIL, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY', AccountId = testAccount.Id, LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_STUDENT);
        insert studentContact;
        LSB_UserHelper.createCommunityUser(
                new User(
                        FirstName = 'John',
                        LastName = 'Snow',
                        Email = COMMUNITY_USER_EMAIL
                ),
                'KMDyzfik#lMtTXN6@dl6uU'
        );

            hed__Term__c courseTerm = new hed__Term__c(
                    hed__Start_Date__c = Date.today() - 2,
                    hed__End_Date__c = Date.today() + 2,
                    hed__Account__c = testAccount.Id
            );

            insert courseTerm;

            hed__Course__c course = new hed__Course__c(
                    Name = 'Test',
                    hed__Account__c = testAccount.Id
            );

            insert course;

            hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
                    Name = 'Test Course',
                    hed__Course__c = course.Id,
                    hed__Term__c = courseTerm.Id
            );

            insert courseOffering;

            hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
                    LSB_PEN_LatestEnrolmentRecord__c = true,
                    hed__Enrollment_Status__c = 'EFE',
                    hed__Contact__c = studentContact.Id
            );

            insert programEnrollment;

            hed__Course_Enrollment__c module = new LSB_CourseEnrollmentBuilder()
                    .withRecordType(COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME)
                    .withProgramEnrollment(programEnrollment.Id)
                    .withModuleEnrollmentStatus(LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED)
                    .withCourseOffering(courseOffering.Id)
                    .createAndSave();
    }

    @IsTest
    private static void shouldReturnFutureCourseworkAssessmentsForDDSRequestType() {
        Id studentContactId = [SELECT Id FROM Contact WHERE Email =: COMMUNITY_USER_EMAIL].Id;
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        List<hed__Course_Enrollment__c> assessments = new List<hed__Course_Enrollment__c>();
        hed__Course_Enrollment__c pastCoursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(-10))
                .create();
        assessments.add(pastCoursework);
        hed__Course_Enrollment__c futureCoursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .create();
        assessments.add(futureCoursework);
        hed__Course_Enrollment__c futureExam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .create();
        assessments.add(futureExam);
        insert assessments;

        Test.startTest();
        List<LSB_CourseEnrollmentHelper.ModuleWrapper> result = LSB_CommCreateClaimController.getModuleData(studentContactId, LSB_Constants.CLAIM_REQUEST_TYPE_DDS);
        Test.stopTest();

        System.assertEquals(1, result.size());
        List<LSB_CourseEnrollmentHelper.AssessmentWrapper> resultAssessments = result[0].assessments;
        System.assertEquals(1, resultAssessments.size());
        System.assertEquals(false, resultAssessments[0].isExam);
        System.assertEquals(false, resultAssessments[0].isRetrospective);
    }

    @IsTest
    private static void shouldReturnFutureCourseworkAssessmentsForLateRequestType() {
        Id studentContactId = [SELECT Id FROM Contact WHERE Email =: COMMUNITY_USER_EMAIL].Id;
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        List<hed__Course_Enrollment__c> assessments = new List<hed__Course_Enrollment__c>();
        hed__Course_Enrollment__c pastCoursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(-10))
                .create();
        assessments.add(pastCoursework);
        hed__Course_Enrollment__c futureCoursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .create();
        assessments.add(futureCoursework);
        hed__Course_Enrollment__c futureExam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .create();
        assessments.add(futureExam);
        insert assessments;

        Test.startTest();
        List<LSB_CourseEnrollmentHelper.ModuleWrapper> result = LSB_CommCreateClaimController.getModuleData(studentContactId, LSB_Constants.CLAIM_REQUEST_TYPE_LATE);
        Test.stopTest();

        System.assertEquals(1, result.size());
        List<LSB_CourseEnrollmentHelper.AssessmentWrapper> resultAssessments = result[0].assessments;
        System.assertEquals(1, resultAssessments.size());
        System.assertEquals(false, resultAssessments[0].isExam);
        System.assertEquals(false, resultAssessments[0].isRetrospective);
    }

    @IsTest
    private static void shouldReturnFutureAndPastExamAndCourseworkAssessmentsForECRequestType() {
        Id studentContactId = [SELECT Id FROM Contact WHERE Email =: COMMUNITY_USER_EMAIL].Id;
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        List<hed__Course_Enrollment__c> assessments = new List<hed__Course_Enrollment__c>();
        hed__Course_Enrollment__c pastCoursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(-10))
                .create();
        assessments.add(pastCoursework);
        hed__Course_Enrollment__c futureCoursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .create();
        assessments.add(futureCoursework);
        hed__Course_Enrollment__c futureExam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .create();
        assessments.add(futureExam);
        hed__Course_Enrollment__c pastExam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(-3))
                .create();
        assessments.add(pastExam);
        insert assessments;

        Test.startTest();
        List<LSB_CourseEnrollmentHelper.ModuleWrapper> result = LSB_CommCreateClaimController.getModuleData(studentContactId, LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        Test.stopTest();

        System.assertEquals(1, result.size());
        List<LSB_CourseEnrollmentHelper.AssessmentWrapper> resultAssessments = result[0].assessments;
        System.assertEquals(4, resultAssessments.size());
    }

    @IsTest
    private static void shouldReturnOnlySubcomponentAssessmentsWhenExist() {
        Id studentContactId = [SELECT Id FROM Contact WHERE Email =: COMMUNITY_USER_EMAIL].Id;
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c componentAssessment = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .createAndSave();
        hed__Course_Enrollment__c subcomponentAssessment = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(componentAssessment.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .createAndSave();

        Test.startTest();
        List<LSB_CourseEnrollmentHelper.ModuleWrapper> result = LSB_CommCreateClaimController.getModuleData(studentContactId, LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        Test.stopTest();

        System.assertEquals(1, result.size());
        List<LSB_CourseEnrollmentHelper.AssessmentWrapper> resultAssessments = result[0].assessments;
        System.assertEquals(1, resultAssessments.size());
        System.assertEquals(subcomponentAssessment.Id, resultAssessments[0].value);
    }

    @IsTest
    private static void testGetAssignmentConfig() {
        LSB_EC_ScreenAssessmentConfig__mdt result = LSB_CommCreateClaimController.getECScreenAssessmentConfig(false, true, true);

        System.assertNotEquals(null, result);
    }

    @IsTest
    private static void testGetConfigData() {
        Id studentContactId = [SELECT Id FROM Contact WHERE Email =: COMMUNITY_USER_EMAIL].Id;

        String result = LSB_CommCreateClaimController.getConfigData(studentContactId);

        System.assert(String.isNotBlank(result));
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultWhenPassingSerializedECClaimCase() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .createAndSave();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withRelatedAssessment(coursework.Id)
                .withSubmitWithin5WorkingDays('Yes')
                .withRelatedModule(testModule.Id)
                .create();

        String caseJson = JSON.serialize(claimCase);

        LSB_ECClaimService.ClaimSaveResultWrapper result = LSB_CommCreateClaimController.saveClaimCase(caseJson);

        System.assert(result != null);
        System.assertEquals(LSB_Constants.CASE_STATUS_REQUEST_SUPPORTED, result.status);
    }
}