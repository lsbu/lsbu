/**
 * Created by ociszek001 on 10.12.2020.
 */

@IsTest
private class LSB_PublicGroupAssignmentHelperTest {

    @TestSetup
    static void loadData() {
        TestDataClass.createOfferHolder();
        TestDataClass.insertUser('System Administrator');
    }

    @IsTest
    static void addUserToGroupTest() {
        List<Group> groupList = [
                SELECT Id, DeveloperName
                FROM Group
                WHERE DeveloperName = :System.Label.LSB_CustomerPortalAdviseesGroupName
        ];
        Id groupId = groupList.get(0).Id;
        System.assertEquals(1, groupList.size());

        List<User> userList = [
                SELECT Id, FirstName, Email
                FROM User
                WHERE Email = :TestDataClass.communityUserEmail
        ];
        Id userId = userList.get(0).Id;
        System.assertEquals(1, userList.size());

        List<GroupMember> groupMemberList = [
                SELECT Id, UserOrGroupId
                FROM GroupMember
                WHERE GroupId = :groupId AND UserOrGroupId = :userId
        ];
        System.assertEquals(0, groupMemberList.size());

        List<GroupMember> groupMemberListAfterUpdate = new List<GroupMember>();
        List<User> adminList = [SELECT Id, Username FROM User WHERE Username = :TestDataClass.userName];
        System.runAs(adminList.get(0)) {
            Test.startTest();
            List<Profile> pList = [SELECT Id FROM Profile WHERE Name = :TestDataClass.studentProfile];
            System.assertEquals(1, pList.size());

            userList.get(0).ProfileId = pList.get(0).Id;
            update userList;

            groupMemberListAfterUpdate = [
                    SELECT Id, UserOrGroupId
                    FROM GroupMember
                    WHERE GroupId = :groupId AND UserOrGroupId = :userId
            ];
            Test.stopTest();
        }
        System.assertEquals(1, groupMemberListAfterUpdate.size());
    }
}