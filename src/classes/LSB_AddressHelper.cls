public without sharing class LSB_AddressHelper {

    private static final String HOME_ADDRESS_TYPE = 'Home';
    private static final String LONDON_AREA_OF_PERMANENT_RESIDENCE = 'London';
    private static final String NON_LONDON_AREA_OF_PERMANENT_RESIDENCE = 'Not London';

    public static void setDefaultAddressType(List<hed__Address__c> newAddresses) {
        for (hed__Address__c addr : newAddresses) {
            if (addr.hed__Address_Type__c == null) {
                addr.hed__Address_Type__c = HOME_ADDRESS_TYPE;
            }
        }
    }

    public static Map<String, ContactPointAddress> syncWithContactPoints(Map<Id, hed__Address__c> newAddresses, Map<Id, hed__Address__c> oldAddresses) {
        Map<String, ContactPointAddress> newContactPointAddresses = new Map<String, ContactPointAddress>();

        for (hed__Address__c addr : newAddresses.values()) {
            String externalId = LSB_ContactPointHelper.getContactPointExternalId(new Contact(Id = addr.hed__Parent_Contact__c), addr.Id);
            Boolean isDefaultUpdated = oldAddresses.isEmpty() || oldAddresses.get(addr.Id).hed__Default_Address__c != addr.hed__Default_Address__c;

            newContactPointAddresses.put(
                    externalId,
                LSB_ContactPointHelper.createContactPointAddress(addr, isDefaultUpdated, oldAddresses.isEmpty(), externalId));
        }

        upsert newContactPointAddresses.values() LSB_COA_ExternalId__c;
        return newContactPointAddresses;
    }

    public static void createContactPointConsents(Map<Id, hed__Address__c> newAddresses, Map<String, ContactPointAddress> newContactPointAddresses) {
        List<ContactPointConsent> newContactPointConsents = new List<ContactPointConsent>();

        for (hed__Address__c addr : newAddresses.values()) {
            String externalId = LSB_ContactPointHelper.getContactPointExternalId(new Contact(Id = addr.hed__Parent_Contact__c), addr.Id);
            newContactPointConsents.add(LSB_ContactPointHelper.createContactPointConsent(
                    externalId+ '#' + LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST,
                    LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST,
                    LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN,
                    new Contact(Id = addr.hed__Parent_Contact__c, LSB_CON_SourceSystem__c = addr.LSB_ADS_SourceSystem__c),
                    newContactPointAddresses.get(externalId).Id,
                    LSB_ContactPointHelper.DEFAULT_LEGITIMATE_INTEREST_DATA_USE_PURPOSE_ID));

            newContactPointConsents.add(LSB_ContactPointHelper.createContactPointConsent(
                    externalId + '#' + LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST,
                    LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST,
                    addr.LSB_ADS_ExplicitInterestConsentStatus__c,
                    new Contact(Id = addr.hed__Parent_Contact__c, LSB_CON_SourceSystem__c = addr.LSB_ADS_SourceSystem__c),
                    newContactPointAddresses.get(externalId).Id,
                    LSB_ContactPointHelper.DEFAULT_EXPLICIT_INTEREST_DATA_USE_PURPOSE_ID));
        }

        insert newContactPointConsents;
    }

    public static void deleteContactPoints(Map<Id, hed__Address__c> oldAddresses) {
        Set<String> externalIds = new Set<String>();

        for (hed__Address__c addr : oldAddresses.values()) {
            externalIds.add(LSB_ContactPointHelper.getContactPointExternalId(new Contact(Id = addr.hed__Parent_Contact__c), addr.Id) + '%');
        }

        delete [SELECT Id FROM ContactPointConsent WHERE LSB_ExternalId__c LIKE :externalIds];
        delete [SELECT Id FROM ContactPointAddress WHERE LSB_COA_ExternalId__c LIKE :externalIds];
    }

    public static void updatePostalCodeDeprivationFlagAndAreaOfPermanentResidence(List<hed__Address__c> newAddresses, Map<Id, hed__Address__c> oldAddresses) {
        List<hed__Address__c> permanentAddresses = new List<hed__Address__c>();
        Map<Id, Id> addressByContactToClean = new Map<Id, Id>();
        for (hed__Address__c newAddress : newAddresses) {
            if (newAddress.hed__Address_Type__c == LSB_Constants.ADDRESS_TYPE_PERMANENT && newAddress.hed__Default_Address__c == true && newAddress.hed__Parent_Contact__c != null) {
                permanentAddresses.add(newAddress);
            } else if (Trigger.isUpdate && oldAddresses.get(newAddress.Id).hed__Address_Type__c == LSB_Constants.ADDRESS_TYPE_PERMANENT && oldAddresses.get(newAddress.Id).hed__Default_Address__c == true && newAddress.hed__Parent_Contact__c != null
                    && (oldAddresses.get(newAddress.Id).hed__Address_Type__c != newAddress.hed__Address_Type__c || oldAddresses.get(newAddress.Id).hed__Default_Address__c != newAddress.hed__Default_Address__c)){
                addressByContactToClean.put(newAddress.hed__Parent_Contact__c, newAddress.Id);
            }
        }

        Map<Id, Contact> contactsToUpdateById = new Map<Id, Contact>();
        if (!addressByContactToClean.keySet().isEmpty()) {
            List<hed__Address__c> existingDefaultPermanentAddresses = new List<hed__Address__c>();
            for (hed__Address__c address : [SELECT Id, hed__Parent_Contact__c FROM hed__Address__c WHERE hed__Parent_Contact__c =: addressByContactToClean.keySet() AND hed__Default_Address__c = true AND hed__Address_Type__c =: LSB_Constants.ADDRESS_TYPE_PERMANENT]) {
                if (address.Id != addressByContactToClean.get(address.hed__Parent_Contact__c)){
                    existingDefaultPermanentAddresses.add(address);
                }
            }

            if (existingDefaultPermanentAddresses.isEmpty()) {
                for (Id contactsToCleanId : addressByContactToClean.keySet()) {
                    Contact contact = getContactWithBlankAreaOfPermanentResidenceAndFalsePostCodeFlag(contactsToCleanId);
                    contactsToUpdateById.put(contact.Id, contact);
                }
            }
        }

        if (!permanentAddresses.isEmpty()) {
            List<String> postalCodes = new List<String>();
            for (LSB_PostalCodes__c postalCode : [SELECT Postcode__c FROM LSB_PostalCodes__c]) {
                if (postalCode.Postcode__c != null) {
                    postalCodes.add(postalCode.Postcode__c);
                }
            }
            List<String> postcodesPrefixes = new List<String>();
            for (LSB_PostcodesDatabase__mdt postcodePrefix : [SELECT DeveloperName FROM LSB_PostcodesDatabase__mdt]) {
                if (postcodePrefix.DeveloperName != null) {
                    postcodesPrefixes.add(postcodePrefix.DeveloperName);
                }
            }

            for (hed__Address__c permanentAddress : permanentAddresses) {
                Contact contact = new Contact(Id = permanentAddress.hed__Parent_Contact__c);
                contact.LSB_CON_PostcodeDeprivationFlag__c = false;
                contact.LSB_CON_AreaOfPermanentResidence__c = NON_LONDON_AREA_OF_PERMANENT_RESIDENCE;
                if (!postalCodes.isEmpty() && postalCodes.contains(permanentAddress.hed__MailingPostalCode__c)) {
                    contact.LSB_CON_PostcodeDeprivationFlag__c = true;
                }
                if (permanentAddress.hed__MailingPostalCode__c == null) {
                    contact.LSB_CON_AreaOfPermanentResidence__c = null;
                } else {
                    for (String postcodePrefix : postcodesPrefixes) {
                        if (permanentAddress.hed__MailingPostalCode__c.startsWith(postcodePrefix)) {
                            contact.LSB_CON_AreaOfPermanentResidence__c = LONDON_AREA_OF_PERMANENT_RESIDENCE;
                        }
                    }
                }
                contactsToUpdateById.put(contact.Id, contact);
            }
        }

        if (!contactsToUpdateById.values().isEmpty()) {
            update contactsToUpdateById.values();
        }
    }

    public static void cleanPostalCodeDeprivationFlagAndAreaOfPermanentResidence(List<hed__Address__c> oldAddresses) {
        List<hed__Address__c> permanentAddresses = new List<hed__Address__c>();
        for (hed__Address__c oldAddress : oldAddresses) {
            if (oldAddress.hed__Address_Type__c == LSB_Constants.ADDRESS_TYPE_PERMANENT && oldAddress.hed__Default_Address__c == true && oldAddress.hed__Parent_Contact__c != null) {
                permanentAddresses.add(oldAddress);
            }
        }

        if (permanentAddresses.isEmpty()) {
            return;
        }

        List<Contact> contactsToUpdate = new List<Contact>();
        for (hed__Address__c permanentAddress : permanentAddresses) {
            Contact contact = getContactWithBlankAreaOfPermanentResidenceAndFalsePostCodeFlag(permanentAddress.hed__Parent_Contact__c);
            contactsToUpdate.add(contact);
        }

        update contactsToUpdate;
    }

    private static Contact getContactWithBlankAreaOfPermanentResidenceAndFalsePostCodeFlag(Id contactId) {
        Contact contact = new Contact(Id = contactId);
        contact.LSB_CON_PostcodeDeprivationFlag__c = false;
        contact.LSB_CON_AreaOfPermanentResidence__c = null;
        return Contact;
    }
}