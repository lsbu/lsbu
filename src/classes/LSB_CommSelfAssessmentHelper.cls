public without sharing class LSB_CommSelfAssessmentHelper {

    private static final String CATEGORY_PERSONAL_DEVELOPMENT = LSB_Constants.CATEGORY_PERSONAL_DEVELOPMENT;
    private static final String MY_PLAN_TAB = LSB_Constants.MY_PLAN_TAB;
    private static final String CONFIG_NAME_INITIAL_RESPONSE = 'LSB_InitialResponse';
    private static final String CONFIG_NAME_CURRENT_RESPONSE = 'LSB_CurrentResponse';
    private static final String SECTION_ID_CHARACTER = '#';

    public static SelfAssessmentWrapper getAssessmentWrapperData() {
        Set<String> picklistTabNames = LSB_CommSelfAssessmentHelper.getTabNamesFromPicklistValues();

        Id contactId = getContactIdByCommunityUser();
        List<LSB_SAR_SelfAssessmentResponse__c> responses = getResponsesForContact(contactId);

        Map<String, List<SectionData>> category2SectionData = filterResponsesByCategory(responses);

        LSB_CommSelfAssessmentHelper.SelfAssessmentWrapper selfAssessmentWrapper = new LSB_CommSelfAssessmentHelper.SelfAssessmentWrapper();
        selfAssessmentWrapper.picklistTabNames = picklistTabNames;
        selfAssessmentWrapper.responseData = category2SectionData;
        selfAssessmentWrapper.chartConfig = getChartResponseData(contactId);
        return selfAssessmentWrapper;
    }

    public static Map<String, List<SectionData>> filterResponsesByCategory(List<LSB_SAR_SelfAssessmentResponse__c> responses) {
        Map<String, List<SectionData>> category2SectionData = new Map<String, List<LSB_CommSelfAssessmentHelper.SectionData>>();

        for(LSB_SAR_SelfAssessmentResponse__c sar : responses){
            category2SectionData.put(sar.LSB_SAR_Category__c, new List<SectionData>());
        }
        category2SectionData.put(MY_PLAN_TAB, new List<SectionData>());

        for(LSB_SAR_SelfAssessmentResponse__c sar : responses){
            List<SectionData> sectionData = category2SectionData.get(sar.LSB_SAR_Category__c);
            List<SectionData> myPlanSectionData = category2SectionData.get(MY_PLAN_TAB);
            Boolean sectionExists = false;
            for(SectionData sd : sectionData){
                if(isTheSameSection(sd.id, sar.LSB_SAR_Section__c)){
                    sd.sectionItems.add(sar);
                    sectionExists = true;
                    break;
                }
            }
            if(!sectionExists){
                sectionData.add(
                    new SectionData(transformSectionId(sar.LSB_SAR_Section__c), getSectionLabelFromPicklistValues(sar.LSB_SAR_Section__c), new List<LSB_SAR_SelfAssessmentResponse__c>{
                        sar
                    })
                );
            }

            if(!sectionData.isEmpty()){
                sectionData[0].active = true;
            }

            if (String.isNotBlank(sar.LSB_SAR_KnowledgePublicUrl__c)) {
                Boolean myPlanSectionExists = false;
                for(SectionData sd : myPlanSectionData){
                    if(isTheSameSection(sd.id, sar.LSB_SAR_Section__c)){
                        sd.sectionItems.add(sar);
                        myPlanSectionExists = true;
                        break;
                    }
                }

                if(!myPlanSectionExists) {
                    myPlanSectionData.add(
                        new SectionData(transformSectionId(sar.LSB_SAR_Section__c), getSectionLabelFromPicklistValues(sar.LSB_SAR_Section__c), new List<LSB_SAR_SelfAssessmentResponse__c>{
                            sar
                        })
                    );
                }
            }
        }

        return category2SectionData;
    }

    private static String transformSectionId(String sectionName) {
        if (sectionName != null) {
            return sectionName.replaceAll('(\\s+)', '') + SECTION_ID_CHARACTER;
        }
        return '';
    }

    private static Boolean isTheSameSection(String sectionId, String sectionName) {
        String transformedSection = transformSectionId(sectionName);
        return sectionId == transformedSection;
    }

    public static Set<String> getTabNamesFromPicklistValues() {
        Set<String> picklistTabNames = new Set<String>();
        Schema.DescribeFieldResult fieldResult = LSB_SAQ_SelfAssessmentQuestion__c.LSB_SAQ_Category__c.getDescribe();
        for (Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()) {
            picklistTabNames.add(pickListVal.getLabel());
        }

        return picklistTabNames;
    }

    private static String getSectionLabelFromPicklistValues(String picklistValue) {
        Map<String, String> picklistLabelByValue = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = LSB_SAQ_SelfAssessmentQuestion__c.LSB_SAQ_Section__c.getDescribe();
        for (Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()) {
            picklistLabelByValue.put(pickListVal.getValue(), pickListVal.getLabel());
        }

        return picklistLabelByValue.get(picklistValue);
    }

    private static Id getContactIdByCommunityUser() {
        Id currentUserId = UserInfo.getUserId();
        List<User> users = [SELECT Id, ContactId FROM User WHERE Id = :currentUserId];
        if (!users.isEmpty()) {
            return users[0].ContactId;
        }
        return null;
    }

    public static ChartConfig getChartResponseData(Id contactId) {
    List<LSB_SAR_SelfAssessmentResponse__c> responses = getAllResponsesForContactByCategory(contactId, CATEGORY_PERSONAL_DEVELOPMENT);
    List<LSB_SAR_SelfAssessmentResponse__c> initialResponses = new List<LSB_SAR_SelfAssessmentResponse__c>();
    List<LSB_SAR_SelfAssessmentResponse__c> currentResponses = new List<LSB_SAR_SelfAssessmentResponse__c>();
        Boolean hasCurrentResponses = false;

        for (LSB_SAR_SelfAssessmentResponse__c response : responses) {
            if (response.LSB_SAR_IsCurrent__c) {
                currentResponses.add(response);
            }
            if (response.LSB_SAR_IsInitialResponse__c) {
                initialResponses.add(response);
            }
            if (response.LSB_SAR_IsCurrent__c && !response.LSB_SAR_IsInitialResponse__c) {
                hasCurrentResponses = true;
            }
        }

        Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> initialResponseMap = getSection2ResponsesMap(initialResponses);
        Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> currentResponseMap = getSection2ResponsesMap(currentResponses);
        List<LSB_ChartConfig__mdt> chartConfigRecords = [SELECT Id, DeveloperName, Label,
                LSB_BackgroundColor__c, LSB_BorderColor__c, LSB_Filled__c, LSB_PointBackgroundColor__c,
                LSB_PointBorderColor__c, LSB_PointHoverBackgroundColor__c, LSB_PointHoverBorderColor__c,
                LSB_IconName__c, LSB_IsSectionConfig__c, LSB_SectionLabelName__c, LSB_SortOrder__c
        FROM LSB_ChartConfig__mdt
        ORDER BY LSB_SortOrder__c ASC];

        ChartConfig chartConfig = createChartConfig(initialResponseMap, currentResponseMap, chartConfigRecords, hasCurrentResponses);

        return chartConfig;
    }

    private static ChartConfig createChartConfig(Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> initialResponseMap,
            Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> currentResponseMap, List<LSB_ChartConfig__mdt> chartConfigRecords, Boolean hasCurrentResponses) {

        if (initialResponseMap.isEmpty()) {
            return null;
        }

        List<LSB_CommSelfAssessmentHelper.ChartDataset> datasets = new List<LSB_CommSelfAssessmentHelper.ChartDataset>();

        Map<String, String> sectionLabel2Icon = getLabel2IconNameMap(chartConfigRecords);


        for (LSB_ChartConfig__mdt configRecord : chartConfigRecords) {
            if (configRecord.DeveloperName == CONFIG_NAME_INITIAL_RESPONSE) {
                datasets.add(new ChartDataset(configRecord, initialResponseMap, sectionLabel2Icon.keySet()));
            } else if (configRecord.DeveloperName == CONFIG_NAME_CURRENT_RESPONSE && hasCurrentResponses) {
                datasets.add(new ChartDataset(configRecord, currentResponseMap, sectionLabel2Icon.keySet()));
            }
        }

        ChartConfig chartConfig = new ChartConfig();
        ChartData chartData = new ChartData();
        chartData.labels = new List<String> (sectionLabel2Icon.keySet());
        chartData.datasets = datasets;
        chartData.sectionData = sectionLabel2Icon;
        chartConfig.data = chartData;

        return chartConfig;
    }

    private static Map<String, String> getLabel2IconNameMap(List<LSB_ChartConfig__mdt> chartConfigRecords) {
        Map<String, String> sectionLabel2Icon = new Map<String, String>();
        for (LSB_ChartConfig__mdt configRecord : chartConfigRecords) {
            if (configRecord.LSB_IsSectionConfig__c) {
                sectionLabel2Icon.put(configRecord.LSB_SectionLabelName__c, configRecord.LSB_IconName__c);
            }
        }

        return sectionLabel2Icon;
    }

    private static Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> getSection2ResponsesMap(List<LSB_SAR_SelfAssessmentResponse__c> responses) {
        Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> section2Responses = new Map<String, List<LSB_SAR_SelfAssessmentResponse__c>>();
        Map<String, String> picklistLabelByValue = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = LSB_SAQ_SelfAssessmentQuestion__c.LSB_SAQ_Section__c.getDescribe();
        for (Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()) {
            picklistLabelByValue.put(pickListVal.getValue(), pickListVal.getLabel());
        }

        for (LSB_SAR_SelfAssessmentResponse__c response : responses) {

            if (section2Responses.containsKey(picklistLabelByValue.get(response.LSB_SAR_Section__c))) {
                section2Responses.get(picklistLabelByValue.get(response.LSB_SAR_Section__c)).add(response);
            } else {
                section2Responses.put(picklistLabelByValue.get(response.LSB_SAR_Section__c), new List<LSB_SAR_SelfAssessmentResponse__c> {response});
            }
        }
        return section2Responses;
    }

    private static List<LSB_SAR_SelfAssessmentResponse__c> getAllResponsesForContactByCategory(Id contactId, String categoryName) {
        List<LSB_SAR_SelfAssessmentResponse__c> responses = [
                SELECT Id,
                        LSB_SAR_SelfAssessmentQuestion__r.LSB_SAQ_SurveyQuestionFullQuestion__c,
                        LSB_SAR_SelfAssessmentQuestion__r.LSB_SAQ_IncludeInPersonalDevChart__c,
                        LSB_SAR_ResponseValue__c,
                        LSB_SAR_ChoiceValue__c,
                        LSB_SAR_SelfAssessmentQuestionChoice__c,
                        LSB_SAR_Section__c,
                        LSB_SAR_Category__c,
                        LSB_SAR_QuestionName__c,
                        LSB_SAR_QuestionChoiceAnswerExplanation__c,
                        LSB_SAR_KnowledgePublicUrl__c,
                        LSB_SAR_QuestionChoiceAnswer__c,
                        LSB_SAR_IsCurrent__c,
                        LSB_SAR_ChoiceNumericalRating__c,
                        LSB_SAR_IsInitialResponse__c,
                        LSB_SAR_SurveyQuestionId__c
                FROM LSB_SAR_SelfAssessmentResponse__c
                WHERE LSB_SAR_Contact__c =: contactId
                AND LSB_SAR_Category__c =: categoryName
                AND LSB_SAR_SelfAssessmentQuestion__r.LSB_SAQ_IncludeInPersonalDevChart__c = TRUE
                ORDER BY LSB_SAR_Category__c, LSB_SAR_Section__c, LSB_SAR_SelfAssessmentQuestion__r.LSB_SAQ_SortOrder__c
        ];

        return responses;
    }

    public static List<LSB_SAR_SelfAssessmentResponse__c> getResponsesForContact(Id contactId) {
        List<LSB_SAR_SelfAssessmentResponse__c> responses = [
            SELECT Id,
                LSB_SAR_SelfAssessmentQuestion__r.LSB_SAQ_SurveyQuestionFullQuestion__c,
                LSB_SAR_ResponseValue__c,
                LSB_SAR_ChoiceValue__c,
                LSB_SAR_SelfAssessmentQuestionChoice__c,
                LSB_SAR_Section__c,
                LSB_SAR_Category__c,
                LSB_SAR_QuestionName__c,
                LSB_SAR_QuestionChoiceAnswerExplanation__c,
                LSB_SAR_KnowledgePublicUrl__c,
                LSB_SAR_KnowledgeFullUrl__c,
                LSB_SAR_QuestionChoiceAnswer__c,
                LSB_SAR_IsCurrent__c
            FROM LSB_SAR_SelfAssessmentResponse__c
            WHERE LSB_SAR_Contact__c = :contactId
                AND LSB_SAR_IsCurrent__c = TRUE
                AND LSB_SAR_EndDate__c = NULL
                AND LSB_SAR_Category__c != NULL
            ORDER BY LSB_SAR_Category__c, LSB_SAR_Section__c, LSB_SAR_SelfAssessmentQuestion__r.LSB_SAQ_SortOrder__c
        ];

        Map<Id, LSB_SAQ_SelfAssessmentQuestion__c> selfAssessmentQuestionMap = new Map<Id, LSB_SAQ_SelfAssessmentQuestion__c>();

        for(LSB_SAR_SelfAssessmentResponse__c sar : responses){
            selfAssessmentQuestionMap.put(sar.LSB_SAR_SelfAssessmentQuestion__c, null);
        }

        selfAssessmentQuestionMap = new Map<Id, LSB_SAQ_SelfAssessmentQuestion__c>([
            SELECT Id,
                LSB_SAQ_SurveyQuestionFullQuestion__c,
            (
                SELECT Id, LSB_SAC_SurveyQuestionChoiceFullName__c
                FROM SelfAssessmentQuestion_Choices__r
                ORDER BY LSB_SAC_SortOrder__c ASC
            )
            FROM LSB_SAQ_SelfAssessmentQuestion__c
            WHERE Id IN :selfAssessmentQuestionMap.keySet()
        ]);

        for(LSB_SAR_SelfAssessmentResponse__c sar : responses){
            sar.LSB_SAR_SelfAssessmentQuestion__r = selfAssessmentQuestionMap.get(sar.LSB_SAR_SelfAssessmentQuestion__c);
        }

        return responses;
    }

    public class SelfAssessmentWrapper {
        @AuraEnabled public Set<String> picklistTabNames { get; set; }
        @AuraEnabled public Map<String, List<SectionData>> responseData { get; set; }
        @AuraEnabled public ChartConfig chartConfig { get; set; }


    }

    public class SectionData {
        @AuraEnabled public String id { get; set; }
        @AuraEnabled public String title { get; set; }
        @AuraEnabled public Boolean active { get; set; }
        @AuraEnabled public List<LSB_SAR_SelfAssessmentResponse__c> sectionItems { get; set; }

        public SectionData(String sectionId, String label, List<LSB_SAR_SelfAssessmentResponse__c> responses){
            this.id = sectionId;
            this.title = label;
            this.sectionItems = responses;
            this.active = false;
        }
    }

    public class ChartConfig {
        @AuraEnabled public String type { get; set; }
        @AuraEnabled public ChartData data { get; set; }
    }

    public class ChartData {
        @AuraEnabled public List<String> labels { get; set; }
        @AuraEnabled public Map<String, String> sectionData { get; set; }
        @AuraEnabled public List<ChartDataset> datasets { get; set; }
    }

    public class ChartDataset {
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public Boolean fill { get; set; }
        @AuraEnabled public String backgroundColor { get; set; }
        @AuraEnabled public String borderColor { get; set; }
        @AuraEnabled public String pointBackgroundColor { get; set; }
        @AuraEnabled public String pointBorderColor { get; set; }
        @AuraEnabled public String pointHoverBackgroundColor { get; set; }
        @AuraEnabled public String pointHoverBorderColor { get; set; }
        @AuraEnabled public List<Double> data { get; set; }

        public ChartDataset(LSB_ChartConfig__mdt chartConfig, Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> responseMap,  Set<String> sectionLabels) {
            this.label = chartConfig.Label;
            this.fill = chartConfig.LSB_Filled__c;
            this.backgroundColor = chartConfig.LSB_BackgroundColor__c;
            this.borderColor = chartConfig.LSB_BorderColor__c;
            this.pointBackgroundColor = chartConfig.LSB_PointBackgroundColor__c;
            this.pointBorderColor = chartConfig.LSB_PointBorderColor__c;
            this.pointHoverBackgroundColor = chartConfig.LSB_PointHoverBackgroundColor__c;
            this.pointHoverBorderColor = chartConfig.Label;
            this.data = calculateChartValues(responseMap, sectionLabels);
        }


        private List<Double> calculateChartValues(Map<String, List<LSB_SAR_SelfAssessmentResponse__c>> responseMap, Set<String> sectionLabels) {
            List<Double> ratingValues = new List<Double>();
            for (String section : sectionLabels) {
                if (responseMap.containsKey(section)) {
                    ratingValues.add(calculateSectionMediumRating(responseMap.get(section)));
                } else {
                    ratingValues.add(0);
                }
            }
            return ratingValues;
        }

        private Double calculateSectionMediumRating(List<LSB_SAR_SelfAssessmentResponse__c> responses) {
            Decimal sum = 0;
            for (LSB_SAR_SelfAssessmentResponse__c response : responses) {
                sum += response.LSB_SAR_ChoiceNumericalRating__c;
            }
            Decimal result = sum / responses.size();
            return result.setScale(2);
        }
    }
}