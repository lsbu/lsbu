@IsTest
public with sharing class LSB_CommunityMySuccessTeamControllerTest {

    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';
    private static final String ADVISOR_USERNAME = 'mr.advisor@lsbu.com';

    @TestSetup
    private static void loadData() {
        Account accountRecord = new Account(Name = 'Administrative Test Account');
        insert accountRecord;

        Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
        insert studentContact;

        Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];

        User studentUser = new User(
            Alias = 'jsnow',
            Email = studentContact.Email,
            EmailEncodingKey = 'UTF-8',
            LastName = studentContact.LastName,
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_GB',
            ProfileId = studentProfile.Id,
            ContactId = studentContact.Id,
            TimeZoneSidKey = 'Europe/London',
            UserName = STUDENT_USERNAME);
        insert studentUser;

        Profile advisorProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.ADVISOR_PROFILE_NAME];

        User advisorUser = new User(
            Alias = 'jadvisor',
            Email = ADVISOR_USERNAME,
            EmailEncodingKey = 'UTF-8',
            FirstName = 'John',
            LastName = 'Advisor',
            AboutMe = 'Bla bla bla',
            LSB_USR_Linkedin__c = 'http://linkedin.com',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_GB',
            ProfileId = advisorProfile.Id,
            TimeZoneSidKey = 'Europe/London',
            UserName = ADVISOR_USERNAME);
        insert advisorUser;

        Case studentCase = new Case (
            ContactId = studentContact.Id,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId(),
            Origin = LSB_Constants.CASE_ORIGIN_WEB,
            Status = 'New'
        );

        insert studentCase;

        CaseTeamRole caseTeamRole = [SELECT Id FROM CaseTeamRole LIMIT 1];

        CaseTeamMember caseTeamMember = new CaseTeamMember(
            MemberId = advisorUser.Id,
            TeamRoleId = caseTeamRole.Id,
            ParentId = studentCase.Id
        );

        insert caseTeamMember;
    }

    @IsTest
    private static void fetchCaseTeamMembersTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];
        User advisorUser = [SELECT Id, FirstName, LastName, Email, AboutMe, LSB_USR_Linkedin__c FROM User WHERE Username = :ADVISOR_USERNAME];
        System.runAs(studentUser) {
            Test.startTest();
            List<LSB_CommunityMySuccessTeamController.MemberWrapper> memberWrappers = LSB_CommunityMySuccessTeamController.fetchCaseTeamMembers();
            Test.stopTest();
            System.assertEquals(1, memberWrappers.size());
            System.assertEquals(advisorUser.Id, memberWrappers[0].id);
            System.assertEquals(advisorUser.FirstName, memberWrappers[0].firstname);
            System.assertEquals(advisorUser.LastName, memberWrappers[0].lastname);
            System.assertEquals(advisorUser.Email, memberWrappers[0].email);
            System.assertEquals(advisorUser.LSB_USR_Linkedin__c, memberWrappers[0].linkedinurl);
            System.assertEquals(advisorUser.AboutMe, memberWrappers[0].aboutme);
            System.assertNotEquals(null, memberWrappers[0].role);
            System.assertNotEquals(null, memberWrappers[0].photourl);
        }
    }

}