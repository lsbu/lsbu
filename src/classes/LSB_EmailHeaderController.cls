public without sharing class LSB_EmailHeaderController {
        public String headerTitle;
        public String imageUrl { get; set; }

        public void setHeaderTitle(String header) {
            if (this.headerTitle == null) {
                this.headerTitle = header;
                List<Document> docList = [
                        SELECT Id, Name, SystemModstamp
                        FROM Document
                        WHERE DeveloperName = :headerTitle
                        LIMIT 1
                ];
                if (!docList.isEmpty()) {
                    imageUrl = URL.getSalesforceBaseUrl().getProtocol() + '://' + System.URL.getSalesforceBaseUrl().getHost().
                            remove('-api').replace('visual', 'content') + '/servlet/servlet.ImageServer?id=' + docList.get(0).Id + '&oid=' + UserInfo.getOrganizationId();
                }
            }
        }
        public String getHeaderTitle() {
            return headerTitle;
        }
}