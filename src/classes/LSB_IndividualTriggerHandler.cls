/**
 * Created by ociszek001 on 25.03.2021.
 */

public class LSB_IndividualTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;
    private LSB_IndividualHelper individualHelper;

    public LSB_IndividualTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
        this.individualHelper = new LSB_IndividualHelper();
    }

    public void beforeInsert(List<SObject> newList) {
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {
    }

    public void afterInsert(Map<Id, SObject> newItems) {

    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_IndividualHelper.updateContactPointConsents((Map<Id, Individual>) newItems, (Map<Id, Individual>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
    }

    public void afterUndelete(Map<Id, SObject> newItems) {
    }
}
