public class LSB_SupportDetailsPickerController {

    static final Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();

    static final Set<String> excludedPicklists = new Set<String>{
        'LSB_SUM_SupportArrangementCategory__c',
        'LSB_SPD_SupportProfileCategory__c',
        'LSB_SUD_SupportCategory__c'
    };

    public static final String MHWB_CONCERNS = 'MHWB Concerns';
    public static final String DISABILITY_AND_DYSLEXIA_SUPPORT = 'Disability and Dyslexia Support';

    private static Map<String, String> fieldType2DatatableType = new Map<String, String>{
        'BOOLEAN' => 'boolean',
        'REFERENCE' => 'lookup',
        'STRING' => 'text',
        'DATE' => 'date',
        'PICKLIST' => 'picklist'
    };


    public class ColumnData {
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String type;
        @AuraEnabled
        public Boolean editable;
        @AuraEnabled
        public Boolean wrapText;

        public ColumnData(String fieldName, String label, String type, Boolean editable, Boolean wrapText) {
            this.fieldName = fieldName;
            this.label = label;
            this.type = type;
            this.editable = editable;
            this.wrapText = wrapText;
        }
    }

    @AuraEnabled
    public static List<String> getFieldNamesFromFieldSets(String objectApiName, String fieldSetName) {
        Set<String> fieldNames = new Set<String>();

        FieldSet fieldSet = Schema.getGlobalDescribe().get(objectApiName).getDescribe().fieldSets.getMap().get(fieldSetName);

        for (FieldSetMember fsm : fieldSet.getFields()) {
            fieldNames.add(fsm.SObjectField.getDescribe().name);
        }

        return new List<String>(fieldNames);
    }

    @AuraEnabled
    public static List<ColumnData> fetchColumnData(String objectApiName, String fieldSetName) {
        List<String> columns = getFieldNamesFromFieldSets(objectApiName, fieldSetName);
        Map<String, SObjectField> objectFieldMap = Schema.getGlobalDescribe().get(objectApiName).getDescribe().fields.getMap();

        List<ColumnData> columnData = new List<ColumnData>();

        for (String column : columns) {
            DescribeFieldResult fieldResult = objectFieldMap.get(column).getDescribe();
            if (fieldResult == null
                || !fieldResult.isAccessible()
                ) {
                continue;
            }
            String fieldType = String.valueOf(fieldResult.type);
            String fieldApiName = fieldResult.name;
            Boolean wrapText = false;
            if (fieldType == 'REFERENCE') {
                fieldApiName = fieldResult.name.replace('__c', '__r');
            }
            if (fieldType == 'PICKLIST' && !excludedPicklists.contains(fieldResult.name)) {
                wrapText = true;
            }
            if (fieldType == 'PICKLIST' && excludedPicklists.contains(fieldResult.name)) {
                fieldType = 'text';
            }
            if (fieldType2DatatableType.containsKey(fieldType)) {
                fieldType = fieldType2DatatableType.get(fieldType);
            }
            columnData.add(new ColumnData(fieldApiName, fieldResult.label, fieldType, false, wrapText));
        }
        return columnData;
    }

    @AuraEnabled(cacheable=true)
    public static List<LSB_SUM_SupportArrangementMaster__c> fetchSupportMasters(String detailsObjectApiName,
        String supportId, String masterFieldSetApiName, String recordType) {

        if (String.isBlank(detailsObjectApiName)
            || String.isBlank(supportId) || String.isBlank(masterFieldSetApiName)) {
            return new List<LSB_SUM_SupportArrangementMaster__c>();
        }

        Set<String> masterFields = new Set<String>();
        Schema.FieldSet fieldSetObj = Schema.sObjectType.LSB_SUM_SupportArrangementMaster__c.FieldSets.getMap().get(masterFieldSetApiName);
        for (FieldSetMember fsm : fieldSetObj.getFields()) {
            masterFields.add(fsm.getFieldPath());
        }
        masterFields.add('LSB_SUM_ObjectUsedInRecordType__c');
        String queryFilter = '';

        if (detailsObjectApiName == 'LSB_SPD_SupportProfileDetails__c') {
            queryFilter += ' WHERE Id NOT IN (\n' +
                '                    SELECT LSB_SPD_SupportMaster__c\n' +
                '                    FROM LSB_SPD_SupportProfileDetails__c\n' +
                '                    WHERE LSB_SPD_Active__c = TRUE' +
                '                    AND(SupportProfileDDS__c = :supportId\n' +
                '                    OR SupportProfileMHWB__c = :supportId)\n' +
                '                ) AND Object_Used_In__c = \'Support Profile\' ' +
                'AND LSB_SUM_ObjectUsedInRecordType__c = :recordType';
        } else if (detailsObjectApiName == 'LSB_SUD_SupportArrangementDetail__c') {
            queryFilter += ' WHERE Id NOT IN (\n' +
                '                    SELECT LSB_SUD_SupportArrangementMaster__c\n' +
                '                    FROM LSB_SUD_SupportArrangementDetail__c\n' +
                '                    WHERE LSB_SUD_SupportArrangementPlan__c = :supportId\n' +
                '                    AND LSB_SUD_Active__c = TRUE' +
                '                ) AND Object_Used_In__c = \'Support Arrangement\' ' +
                'AND LSB_SUM_ObjectUsedInRecordType__c = :recordType';
        }
        queryFilter += ' ORDER BY Name';

        String query = String.format('SELECT {0} FROM {1} {2}', new List<String>{
            String.join(new List<String>(masterFields), ','), 'LSB_SUM_SupportArrangementMaster__c', queryFilter
        });

        return (List<LSB_SUM_SupportArrangementMaster__c>) Database.query(query);
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchSupportDetails(String objectApiName, String fieldSetApiName, String parentObjectApiName,
        String parentId, String supportProfileDetailRecordType) {
        if (String.isBlank(objectApiName) || String.isBlank(fieldSetApiName) || String.isBlank(parentId)) {
            return new List<SObject>();
        }

        List<String> detailFields = new List<String>();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);

        for (FieldSetMember fsm : fieldSetObj.getFields()) {
            detailFields.add(fsm.getFieldPath());
        }

        String queryFilter = '';
        if (parentObjectApiName == LSB_SUE_SupportProfile__c.SObjectType.getDescribe().getName()) {
            detailFields.add('LSB_SPD_SupportMaster__r.Name');
            if (supportProfileDetailRecordType == MHWB_CONCERNS) {
                queryFilter += ' WHERE SupportProfileMHWB__c = :parentId';
            } else if (supportProfileDetailRecordType == DISABILITY_AND_DYSLEXIA_SUPPORT) {
                queryFilter += ' WHERE SupportProfileDDS__c = :parentId';
            }
            queryFilter += ' AND LSB_SPD_Active__c = TRUE';
        } else if (parentObjectApiName == LSB_SUT_SupportArrangement__c.SObjectType.getDescribe().getName()) {
            detailFields.add('LSB_SUD_SupportArrangementMaster__r.Name');
            queryFilter += ' WHERE LSB_SUD_SupportArrangementPlan__c =:parentId';
            queryFilter += ' AND LSB_SUD_Active__c = TRUE';
        }

        queryFilter += ' ORDER BY Name';

        String query = String.format('SELECT {0} FROM {1} {2}', new List<String>{
            String.join(new List<String>(detailFields), ','), objectApiName, queryFilter
        });

        return Database.query(query);
    }

    @AuraEnabled
    public static void saveDetails(List<SObject> supportDetails, Id parentId) {
        if (parentId.getSobjectType() == LSB_SUE_SupportProfile__c.getSObjectType()) {
            LSB_SUE_SupportProfile__c supportProfile = [
                SELECT Id, LSB_SUE_Contact__c
                FROM LSB_SUE_SupportProfile__c
                WHERE Id = :parentId
                LIMIT 1
            ];
            for (SObject record : supportDetails) {
                record.put(LSB_SPD_SupportProfileDetails__c.LSB_SPD_Contact__c.getDescribe().getName(), supportProfile.LSB_SUE_Contact__c);
            }
        } else if (parentId.getSobjectType() == LSB_SUT_SupportArrangement__c.getSObjectType()) {
            LSB_SUT_SupportArrangement__c supportArrangement = [
                SELECT Id, LSB_SUT_Contact__c
                FROM LSB_SUT_SupportArrangement__c
                WHERE Id = :parentId
                LIMIT 1
            ];
            for (SObject record : supportDetails) {
                record.put(LSB_SUD_SupportArrangementDetail__c.LSB_SUD_Contact__c.getDescribe().getName(), supportArrangement.LSB_SUT_Contact__c);
            }
        }
        upsert supportDetails;
    }
}