public with sharing class LSB_AddCasePredefinedTeamController {

    @AuraEnabled(Cacheable=true)
    public static List<CaseTeamTemplate> fetchTeams() {
        return LSB_CaseTeamHelper.getCaseTeams();
    }

    @AuraEnabled
    public static String addCaseTeam(Id caseId, Id teamId) {
        return LSB_CaseTeamHelper.createCaseTeam(caseId, teamId);
    }

}