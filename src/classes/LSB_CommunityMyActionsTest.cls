@IsTest
private class LSB_CommunityMyActionsTest {

    private static final String OFFER_HOLDER_USERNAME = 'mr.offer.holder@lsbu.com';
    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';

    @testSetup
    private static void loadData() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Account accountRecord = new Account(Name = 'Administrative Test Account');
            insert accountRecord;

            Contact offerHolderContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
            insert offerHolderContact;

            Profile offerHolderProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.OFFER_HOLDER_PROFILE_NAME];
            User offerHolderUser = new User(
                    Alias = 'jsnow',
                    Email = offerHolderContact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = offerHolderContact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = offerHolderProfile.Id,
                    ContactId = offerHolderContact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = OFFER_HOLDER_USERNAME);
            insert offerHolderUser;

            Account accountStudentRecord = new Account(Name = 'Administrative Student Account');
            insert accountStudentRecord;

            Contact studentContact = new Contact(FirstName = 'John', LastName = 'Student', hed__AlternateEmail__c = 'john.student@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1255QWERTY', AccountId = accountStudentRecord.Id);
            insert studentContact;

            Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];
            User studentUser = new User(
                    Alias = 'jstud',
                    Email = studentContact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = studentContact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = studentProfile.Id,
                    ContactId = studentContact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = STUDENT_USERNAME);
            insert studentUser;
        }
    }

    @IsTest
    private static void retrieveActionsFromMetadataStudentTest() {
        User communityUser = [SELECT Id FROM User WHERE UserName = :STUDENT_USERNAME];

        List<LSB_Community_Actions__mdt> result;

        System.runAs(communityUser) {
            Test.startTest();
             result = LSB_CommunityMyActions.retrieveActionsFromMetadata();
            Test.stopTest();
        }

        System.assert(result != null);
        System.assert(!result.isEmpty());
        System.assertEquals(7, result.size());
    }

    @IsTest
    private static void retrieveActionsFromMetadataOfferHolderTest() {
        User communityUser = [SELECT Id FROM User WHERE UserName = :OFFER_HOLDER_USERNAME];

        List<LSB_Community_Actions__mdt> result;

        System.runAs(communityUser) {
            Test.startTest();
            result = LSB_CommunityMyActions.retrieveActionsFromMetadata();
            Test.stopTest();
        }

        System.assert(result != null);
        System.assert(!result.isEmpty());
        System.assertEquals(6, result.size());
    }
}