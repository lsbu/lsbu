public class LSB_ContactTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;

    public LSB_ContactTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
    }

    public void beforeInsert(List<SObject> newList) {}

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
      LSB_ContactHelper.createIndividuals((List<Contact>) newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_ContactHelper.updateContactPoints((Map<Id, Contact>) newItems, (Map<Id, Contact>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
        LSB_ContactHelper.deleteIndividuals((Map<Id, Contact>) oldItems);
    }

    public void afterUndelete(Map<Id, SObject> newItems) {}
}