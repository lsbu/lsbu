/**
 * @descritpion Class contains methods that are using by Reset Password component.
 *
 * @date 17.09.2020.
 */

public without sharing class LSB_CommunityResetPasswordController {

    /**
     * @description Method using to find community user based on Email address.
     *
     * @param emailAddress email of community user.
     *
     * @return Id of user or null if no user found.
     */
    @AuraEnabled
    public static Id findUser(String emailAddress) {
        List<Contact> contacts = [SELECT Id, LSB_CON_SourceSystem__c FROM Contact WHERE Email = :emailAddress];
        Boolean noContact = contacts.size() == 0;
        String result = '';

        if (!noContact) {
            Id qlContactId = null;

            for (Contact record : contacts) {
                if (record.LSB_CON_SourceSystem__c != null &&
                        record.LSB_CON_SourceSystem__c.equals(LSB_Constants.CONTACT_SOURCE_SYSTEM_QL)) {
                    qlContactId = record.Id;
                }
            }

            if (qlContactId != null) {
                User communityUser = [SELECT Id FROM User WHERE ContactId = :qlContactId LIMIT 1];
                return communityUser.Id;
            }
        }

        return null;
    }

    /**
     * @description Method using to reset user password.
     *
     * @param userId Id of user to reset password.
     *
     * @return Error message or empty string if success.
     */
    @AuraEnabled
    public static String resetPassword(String userId) {
        return LSB_UserHelper.resetPassword(userId);
    }
}