public without sharing class LSB_SuccessPlanTemplateTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;
    private LSB_SuccessPlanTemplateHelper successPlanTemplateHelper;

    public LSB_SuccessPlanTemplateTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
        this.successPlanTemplateHelper = new LSB_SuccessPlanTemplateHelper();
    }

    public void beforeInsert(List<SObject> newList) {
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
        successPlanTemplateHelper.applyDefaultTeam((List<sfal__SuccessPlanTemplate__c>) newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        successPlanTemplateHelper.handleTypeChange(
                (List<sfal__SuccessPlanTemplate__c>) newItems.values(),
                (Map<Id, sfal__SuccessPlanTemplate__c>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> newItems) {}

}