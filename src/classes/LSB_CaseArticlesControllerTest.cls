@IsTest
without sharing class LSB_CaseArticlesControllerTest {

    @TestSetup
    static void testData() {
        TestDataClass.createStudent();

        Knowledge__kav knowledgeKav = new Knowledge__kav(
            UrlName = 'TestPassword123',
            Title = 'Test',
            Summary = 'Test',
            IsVisibleInPkb = true,
            LSB_KGE_FaqCategory2__c = 'Other',
            LSB_KGE_ChatbotResponse__c = 'Test',
            LSB_KGE_ChatbotResponseLong__c = 'Test');
        insert knowledgeKav;

        knowledge__kav knowledgeKav2 = [
            SELECT Id, Title, KnowledgeArticleId, VersionNumber
            FROM knowledge__kav
            WHERE Id = :knowledgeKav.Id
        ];

        KbManagement.PublishingService.publishArticle(knowledgeKav2.KnowledgeArticleId, true);

        Contact studentContact = [SELECT Id FROM Contact WHERE LSB_CON_ContactRole__c = :LSB_Constants.CONTACT_ROLE_STUDENT LIMIT 1];

        Case testCase = new Case(Subject = 'Test Case', ContactId = studentContact.Id, LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_STUDENT);
        insert testCase;

        CaseArticle caseArticle = new CaseArticle(
            CaseId = testCase.Id,
            KnowledgeArticleId = knowledgeKav2.KnowledgeArticleId
        );

        insert caseArticle;
    }

    @IsTest
    static void fetchKnowledgeArticleVersionsTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        List<Knowledge__kav> knowledgeKavs = LSB_CaseArticlesController.fetchKnowledgeArticleVersions(testCase.Id);
        Test.stopTest();
        System.assertEquals(1, knowledgeKavs.size());
    }

}