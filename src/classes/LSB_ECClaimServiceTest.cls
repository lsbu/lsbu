@IsTest
public with sharing class LSB_ECClaimServiceTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman2383624@pwc.com';
    private static final String YES_VALUE = 'Yes';
    private static final String NO_VALUE = 'No';
    private static final String CLAIM_STATUS_REQUEST_SUPPORTED = LSB_Constants.CASE_STATUS_REQUEST_SUPPORTED;
    private static final String CLAIM_STATUS_REQUEST_REJECTED = LSB_Constants.CASE_STATUS_REQUEST_REJECTED;
    private static final String CLAIM_STATUS_REQUEST_PENDING = LSB_Constants.CASE_STATUS_REQUEST_PENDING;

    @TestSetup
    private static void setup() {
        Account testAccount = new Account(Name='Test Account Name');
        insert testAccount;
        Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                Email = COMMUNITY_USER_EMAIL, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY', AccountId = testAccount.Id, LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_STUDENT);
        insert studentContact;
        LSB_UserHelper.createCommunityUser(
                new User(
                        FirstName = 'John',
                        LastName = 'Snow',
                        Email = COMMUNITY_USER_EMAIL
                ),
                'KMDyzfik#lMtTXN6@dl6uU'
        );

        hed__Term__c courseTerm = new hed__Term__c(
                hed__Start_Date__c = Date.today() - 2,
                hed__End_Date__c = Date.today() + 2,
                hed__Account__c = testAccount.Id
        );

        insert courseTerm;

        hed__Course__c course = new hed__Course__c(
                Name = 'Test',
                hed__Account__c = testAccount.Id
        );

        insert course;

        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
                Name = 'Test Course',
                hed__Course__c = course.Id,
                hed__Term__c = courseTerm.Id
        );

        insert courseOffering;

        hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
                LSB_PEN_LatestEnrolmentRecord__c = true,
                hed__Enrollment_Status__c = 'EFE',
                hed__Contact__c = studentContact.Id
        );

        insert programEnrollment;

        hed__Course_Enrollment__c module = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME)
                .withProgramEnrollment(programEnrollment.Id)
                .withModuleEnrollmentStatus(LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED)
                .withCourseOffering(courseOffering.Id)
                .createAndSave();
    }

    @IsTest
    private static void shouldReturnDDSResultRegardlessOFPreviousDDSCases() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_DDS);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_DDS)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
        System.assert(result.caseNumber != null);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForLateSubmissionRequestType() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_LATE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestRejectedResultForLateSubmissionRequestTypeWhenAttemptIsThirdOrMore() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_LATE);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_LATE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_REJECTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeRetrospectiveExamRegardlessToPreviousCases() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(-10))
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeRetrospectiveCourseworkRegardlessToPreviousCases() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(-10))
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveCourseworkAbleToSubmitMain() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveCourseworkAbleToSubmitMainWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveCourseworkAbleToSubmitResit() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveCourseworkAbleToSubmitResitWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveCourseworkNotAbleToSubmitMain() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveCourseworkNotAbleToSubmitMainWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveCourseworkNotAbleToSubmitResit() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveCourseworkNotAbleToSubmitResitWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withSubmitWithin5WorkingDays(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, coursework);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveExamAbleToAttemptMain() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveExamAbleToAttemptMainWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveExamNotAbleToAttemptMain() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveExamNotAbleToAttemptWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(false)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveExamAbleToAttemptResit() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveExamAbleToAttemptResitWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(YES_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeProspectiveExamNotAbleToAttemptResit() {
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    @IsTest
    private static void shouldReturnRequestPendingResultForECRequestTypeProspectiveExamNotAbleToAttemptResitWhenThirdOrMoreAttempt() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_EC);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_PENDING, result.status);
    }

    @IsTest
    private static void shouldReturnRequestSupportedResultForECRequestTypeWhenPreviousCasesHaveDifferentRequestType() {
        createPreviousClaimCases(LSB_Constants.CLAIM_REQUEST_TYPE_LATE);
        hed__Course_Enrollment__c testModule = [SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleEnrolmentStatus__c
        FROM hed__Course_Enrollment__c WHERE RecordType.DeveloperName =: LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME][0];
        hed__Course_Enrollment__c exam = new LSB_CourseEnrollmentBuilder()
                .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
                .withParentModuleComponent(testModule.Id)
                .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM)
                .withHandInDate(Date.today().addDays(10))
                .withIsResit(true)
                .create();
        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
                .withAbleToSitTheExam(NO_VALUE)
                .create();


        LSB_ECClaimService service = new LSB_ECClaimService();
        LSB_ECClaimService.ClaimSaveResultWrapper result = service.createNewClaim(claimCase, exam);

        System.assert(result != null);
        System.assertEquals(CLAIM_STATUS_REQUEST_SUPPORTED, result.status);
    }

    private static void createPreviousClaimCases(String requestType) {
        Case claimCase1 = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(requestType)
                .create();
        Case claimCase2 = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
                .withRequestType(requestType)
                .create();
        List<Case> previousCases = new List<Case> {claimCase1, claimCase2};
        insert previousCases;
    }
}