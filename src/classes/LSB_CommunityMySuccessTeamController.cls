public without sharing class LSB_CommunityMySuccessTeamController {

    @AuraEnabled
    public static List<LSB_CommunityMySuccessTeamController.MemberWrapper> fetchCaseTeamMembers() {
        List<MemberWrapper> memberWrappers = new List<MemberWrapper>();
        Id profileId = UserInfo.getProfileId();
        String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

        if (profileName != LSB_Constants.STUDENT_PROFILE_NAME) {
            return memberWrappers;
        }

        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, ContactId FROM User WHERE Id = :userId];
        Id contactId = u.ContactId;

        for (CaseTeamMember caseTeamMember : [
            SELECT Id, TeamRole.Name,
                TYPEOF Member
                    WHEN User THEN
                    Firstname,
                    Lastname,
                    Email,
                    MediumPhotoUrl,
                    LSB_USR_Linkedin__c,
                    AboutMe
                END
            FROM CaseTeamMember
            WHERE Parent.ContactId = :contactId
                AND Parent.RecordType.DeveloperName = :LSB_Constants.CASE_ADVISEE_RECORD_TYPE_DEVELOPER_NAME
        ]) {
            memberWrappers.add(new MemberWrapper(caseTeamMember));
        }

        return memberWrappers;
    }

    public class MemberWrapper {
        @AuraEnabled
        public String id;

        @AuraEnabled
        public String firstname;

        @AuraEnabled
        public String lastname;

        @AuraEnabled
        public String email;

        @AuraEnabled
        public String linkedinurl;

        @AuraEnabled
        public String aboutme;

        @AuraEnabled
        public String role;

        @AuraEnabled
        public String photourl;

        public MemberWrapper(CaseTeamMember caseTeamMember) {
            User member = (User) caseTeamMember.Member;
            this.id = member.id;
            this.firstname = member.FirstName;
            this.lastname = member.LastName;
            this.email = member.Email;
            this.linkedinurl = member.LSB_USR_Linkedin__c;
            this.aboutme = member.AboutMe;
            this.role = caseTeamMember.TeamRole.Name;
            this.photourl = member.MediumPhotoUrl;
        }
    }

}