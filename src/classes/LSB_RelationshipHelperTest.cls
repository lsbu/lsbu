@IsTest
private class LSB_RelationshipHelperTest {

    private static final String ADVISOR_USER_EMAIL = 'Advisor12345@pwc.test.com';
    private static final String STUDENT_USER_EMAIL = 'Student12345@pwc.test.com';
    private static final String STUDENT_USER_PROFILE = 'Student';
    private static final String ADVISOR_USER_PROFILE = 'Advisor';

    @testSetup
    private static void createdData() {
        insert new List<hed__Trigger_Handler__c>{
            TestDataClass.createTriggerHandlerRecord('Relationship__c')
        };

        Account a = new Account(Name = 'Test Account Name Student');
        insert a;

        Contact student = new Contact(
            FirstName = 'John',
            LastName = 'Student',
            hed__AlternateEmail__c = STUDENT_USER_EMAIL,
            Email = STUDENT_USER_EMAIL,
            LSB_CON_SourceSystem__c = 'QL',
            LSB_ChannelOfPreference__c = 'Email',
            LSB_ExternalID__c = 'QWERTY1234',
            AccountId = a.Id);

        insert student;

        insertUser(student.Id, STUDENT_USER_PROFILE);

        Account aa = new Account(Name = 'Test Account Name Advisor');
        insert aa;

        User advUsr = new User(
            UserName = 'advisor_12345@pwc.test.com',
            FirstName = 'John',
            LastName = 'Advisor',
            Email = +'advisor12345@pwc.test.com',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Advisor' LIMIT 1].Id,
            Alias = 'Adv',
            TimeZoneSidKey = UserInfo.getTimezone().getID(),
            LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
            EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
            LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert advUsr;

        Contact advisor = new Contact(
            FirstName = 'John',
            LastName = 'Advisor',
            hed__AlternateEmail__c = ADVISOR_USER_EMAIL,
            Email = ADVISOR_USER_EMAIL,
            LSB_CON_SourceSystem__c = 'QL',
            LSB_ChannelOfPreference__c = 'Email',
            LSB_ExternalID__c = '1234QWERTY',
            AccountId = a.Id,
            LSB_CON_User__c = advUsr.Id);

        insert advisor;
    }

    @future
    private static void insertUser(Id contactId, String profileName) {
        User usr = new User(
            ContactId = contactId,
            UserName = profileName+'12345@pwc.test.com',
            FirstName = 'John',
            LastName = profileName,
            Email = profileName+'12345@pwc.test.com',
            ProfileId = [SELECT Id FROM Profile WHERE Name = :profileName LIMIT 1].Id,
            Alias = profileName,
            TimeZoneSidKey = UserInfo.getTimezone().getID(),
            LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
            EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
            LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert usr;
    }

    @IsTest
    static void testSupportRelationshipInsert() {
        Contact student = [SELECT Id FROM Contact WHERE Email = :STUDENT_USER_EMAIL];
        Contact advisorContact = [SELECT Id FROM Contact WHERE Email = :ADVISOR_USER_EMAIL];
        User advisor = [SELECT Id FROM User WHERE Username = 'advisor_12345@pwc.test.com'];

        LSB_SUT_SupportArrangement__c supportArrangement = new LSB_SUT_SupportArrangement__c(
            LSB_SUT_Contact__c = student.Id,
            Name = 'Test'
        );
        insert supportArrangement;

        hed__Relationship__c rel = new hed__Relationship__c(
            hed__Contact__c = student.Id,
            hed__RelatedContact__c = advisorContact.Id,
            hed__Type__c = 'Personal Tutor',
            LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
            LSB_RTP_PermissionGivenMethod__c = 'Email'
        );

        Test.startTest();
        insert rel;
        Test.stopTest();

        List<LSB_SUT_SupportArrangement__Share> shareRecord = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
        ];

        System.assertEquals(supportArrangement.Id, shareRecord[0].ParentId);
        System.assertEquals(advisor.Id, shareRecord[0].UserOrGroupId);
    }

    @IsTest
    static void testSupportRelationshipInsertWrongStatus() {
        Contact student = [SELECT Id FROM Contact WHERE Email = :STUDENT_USER_EMAIL];
        Contact advisorContact = [SELECT Id FROM Contact WHERE Email = :ADVISOR_USER_EMAIL];
        User advisor = [SELECT Id FROM User WHERE Username = 'advisor_12345@pwc.test.com'];

        LSB_SUT_SupportArrangement__c supportArrangement = new LSB_SUT_SupportArrangement__c(
            LSB_SUT_Contact__c = student.Id,
            Name = 'Test'
        );
        insert supportArrangement;

        hed__Relationship__c rel = new hed__Relationship__c(
            hed__Contact__c = student.Id,
            hed__RelatedContact__c = advisorContact.Id,
            hed__Type__c = 'Personal Tutor',
            LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
            LSB_RTP_PermissionGivenMethod__c = 'Email',
            hed__Status__c = LSB_RelationshipHelper.RELATIONSHIP_FORMER_STATUS
        );

        Test.startTest();
        insert rel;
        Test.stopTest();

        List<LSB_SUT_SupportArrangement__Share> shareRecord = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
            AND ParentId = :supportArrangement.Id
        ];

        System.assertEquals(0, shareRecord.size());
    }

    @IsTest
    static void testSupportRelationshipInsertWrongType() {
        Contact student = [SELECT Id FROM Contact WHERE Email = :STUDENT_USER_EMAIL];
        Contact advisorContact = [SELECT Id FROM Contact WHERE Email = :ADVISOR_USER_EMAIL];
        User advisor = [SELECT Id FROM User WHERE Username = 'advisor_12345@pwc.test.com'];

        LSB_SUT_SupportArrangement__c supportArrangement = new LSB_SUT_SupportArrangement__c(
            LSB_SUT_Contact__c = student.Id,
            Name = 'Test'
        );
        insert supportArrangement;

        hed__Relationship__c rel = new hed__Relationship__c(
            hed__Contact__c = student.Id,
            hed__RelatedContact__c = advisorContact.Id,
            hed__Type__c = 'xxx',
            LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
            LSB_RTP_PermissionGivenMethod__c = 'Email'
        );

        Test.startTest();
        insert rel;
        Test.stopTest();

        List<LSB_SUT_SupportArrangement__Share> shareRecord = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
            AND ParentId = :supportArrangement.Id
        ];

        System.assertEquals(0, shareRecord.size());
    }

    @IsTest
    static void testSupportRelationshipUpdateStatusToInactive() {
        Contact student = [SELECT Id FROM Contact WHERE Email = :STUDENT_USER_EMAIL];
        Contact advisorContact = [SELECT Id FROM Contact WHERE Email = :ADVISOR_USER_EMAIL];
        User advisor = [SELECT Id FROM User WHERE Username = 'advisor_12345@pwc.test.com'];

        LSB_SUT_SupportArrangement__c supportArrangement = new LSB_SUT_SupportArrangement__c(
            LSB_SUT_Contact__c = student.Id,
            Name = 'Test'
        );
        insert supportArrangement;

        hed__Relationship__c rel = new hed__Relationship__c(
            hed__Contact__c = student.Id,
            hed__RelatedContact__c = advisorContact.Id,
            hed__Type__c = 'Personal Tutor',
            LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
            LSB_RTP_PermissionGivenMethod__c = 'Email'
        );

        insert rel;

        List<LSB_SUT_SupportArrangement__Share> shareRecord = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
        ];

        System.assertEquals(supportArrangement.Id, shareRecord[0].ParentId);
        System.assertEquals(advisor.Id, shareRecord[0].UserOrGroupId);

        rel.hed__Status__c = LSB_RelationshipHelper.RELATIONSHIP_FORMER_STATUS;
        Test.startTest();
        update rel;
        Test.stopTest();

        List<LSB_SUT_SupportArrangement__Share> shareRecordAfterUpdate = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
            AND ParentId = :supportArrangement.Id
        ];

        System.assertEquals(0, shareRecordAfterUpdate.size());
    }

    @IsTest
    static void testSupportRelationshipDeletion() {
        Contact student = [SELECT Id FROM Contact WHERE Email = :STUDENT_USER_EMAIL];
        Contact advisorContact = [SELECT Id FROM Contact WHERE Email = :ADVISOR_USER_EMAIL];
        User advisor = [SELECT Id FROM User WHERE Username = 'advisor_12345@pwc.test.com'];

        LSB_SUT_SupportArrangement__c supportArrangement = new LSB_SUT_SupportArrangement__c(
            LSB_SUT_Contact__c = student.Id,
            Name = 'Test'
        );
        insert supportArrangement;

        hed__Relationship__c rel = new hed__Relationship__c(
            hed__Contact__c = student.Id,
            hed__RelatedContact__c = advisorContact.Id,
            hed__Type__c = 'Personal Tutor',
            LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
            LSB_RTP_PermissionGivenMethod__c = 'Email'
        );

        insert rel;

        List<LSB_SUT_SupportArrangement__Share> shareRecord = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
        ];

        System.assertEquals(supportArrangement.Id, shareRecord[0].ParentId);
        System.assertEquals(advisor.Id, shareRecord[0].UserOrGroupId);

        Test.startTest();
        delete rel;
        Test.stopTest();

        List<LSB_SUT_SupportArrangement__Share> shareRecordAfterUpdate = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
            AND ParentId = :supportArrangement.Id
        ];

        System.assertEquals(0, shareRecordAfterUpdate.size());
    }

    @IsTest
    static void testSupportRelationshipUpdateStatusToActive() {
        Contact student = [SELECT Id FROM Contact WHERE Email = :STUDENT_USER_EMAIL];
        Contact advisorContact = [SELECT Id FROM Contact WHERE Email = :ADVISOR_USER_EMAIL];
        User advisor = [SELECT Id FROM User WHERE Username = 'advisor_12345@pwc.test.com'];

        LSB_SUT_SupportArrangement__c supportArrangement = new LSB_SUT_SupportArrangement__c(
            LSB_SUT_Contact__c = student.Id,
            Name = 'Test'
        );
        insert supportArrangement;

        hed__Relationship__c rel = new hed__Relationship__c(
            hed__Contact__c = student.Id,
            hed__RelatedContact__c = advisorContact.Id,
            hed__Type__c = 'Personal Tutor',
            LSB_RTP_YouCanDiscuss__c = 'Disability and Dyslexia Support',
            LSB_RTP_PermissionGivenMethod__c = 'Email',
            hed__Status__c = LSB_RelationshipHelper.RELATIONSHIP_FORMER_STATUS
        );

        insert rel;

        List<LSB_SUT_SupportArrangement__Share> shareRecordAfterUpdate = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
            AND ParentId = :supportArrangement.Id
        ];

        System.assertEquals(0, shareRecordAfterUpdate.size());

        rel.hed__Status__c = LSB_RelationshipHelper.RELATIONSHIP_CURRENT_STATUS;
        Test.startTest();
        update rel;
        Test.stopTest();

        List<LSB_SUT_SupportArrangement__Share> shareRecord = [
            SELECT ParentId, UserOrGroupId
            FROM LSB_SUT_SupportArrangement__Share
            WHERE RowCause = :Schema.LSB_SUT_SupportArrangement__Share.RowCause.Manual
        ];

        System.assertEquals(supportArrangement.Id, shareRecord[0].ParentId);
        System.assertEquals(advisor.Id, shareRecord[0].UserOrGroupId);
    }
}