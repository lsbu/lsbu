@isTest
public with sharing class LSB_CaseActivitiesHelperTest {

    private static final String COMMUNITY_USER_EMAIL = 'h77fh9487r3rf@pwc.test.com';
    private static final String COMMUNITY_USER_FIRST_NAME = 'Pankaj';
    private static final String COMMUNITY_USER_LAST_NAME = 'Kleks';

    @TestSetup
    static void setup() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        Contact contact = new Contact(AccountId = account.Id, FirstName = 'Test', LastName = 'Contact', LSB_CON_CurrentRole__c = 'Prospect');
        insert contact;

        Case testCase = new Case(Subject = 'Test Case', ContactId = contact.Id, LSB_CAS_ContactRole__c = 'Prospect');
        insert testCase;

        CaseComment testCaseComment = new CaseComment(
                ParentId = testCase.Id,
                CommentBody = 'Test 123',
                IsPublished = true
        );

        insert testCaseComment;

        Account householdAccount = new Account(Name='Test Household Account Name');
        insert householdAccount;

        Contact student = new Contact(
                FirstName = 'Pankaj',
                LastName = 'Kleks',
                hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                Email = COMMUNITY_USER_EMAIL,
                LSB_CON_SourceSystem__c = 'QL',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY',
                AccountId = householdAccount.Id);

        insert student;

        insertUser(student.Id);
    }

    @IsTest
    private static void shouldReturnMessageWrapperFromCaseComment() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];

        Test.startTest();
        List<LSB_CaseActivitiesHelper.MessageWrapper> caseComments = LSB_CaseActivitiesHelper.getCaseMessagesByParentId(testCase.Id);
        Test.stopTest();

        System.assertEquals(1, caseComments.size());

    }

    @IsTest
    private static void shouldReturnMessageWrappersFromCaseEmailsAndCaseComments() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        new EmailMessageBuilder().withFromAddress(COMMUNITY_USER_EMAIL).withParentId(testCase.Id).createAndSave();
        new EmailMessageBuilder().withToAddress(COMMUNITY_USER_EMAIL).withParentId(testCase.Id).createAndSave();

        User student = [SELECT Id FROM User WHERE UserName = :COMMUNITY_USER_EMAIL];
        System.runAs(student) {
            List<LSB_CaseActivitiesHelper.MessageWrapper> caseComments = LSB_CaseActivitiesHelper.getCaseMessagesByParentId(testCase.Id);

            System.assertEquals(3, caseComments.size());
        }

    }

    @IsTest
    private static void shouldFilterEmailsByUsersEmailAddress() {
        List<EmailMessage> testMessages = new List<EmailMessage>();
        EmailMessage email1 = new EmailMessageBuilder().withFromAddress(COMMUNITY_USER_EMAIL).createAndSave();
        EmailMessage email2 = new EmailMessageBuilder().withFromAddress('test@random.com').createAndSave();
        EmailMessage email3 = new EmailMessageBuilder().withToAddress(COMMUNITY_USER_EMAIL).createAndSave();
        EmailMessage email4 = new EmailMessageBuilder().withToAddress('test@random.com').createAndSave();
        testMessages.add(email1);
        testMessages.add(email2);
        testMessages.add(email3);
        testMessages.add(email4);

        User student = [SELECT Id FROM User WHERE UserName = :COMMUNITY_USER_EMAIL];

        System.runAs(student) {
            List<EmailMessage> result = LSB_CaseActivitiesHelper.filterCurrentUserContactEmailMessages(testMessages);
            
            System.assertEquals(2, result.size());
        }
    }

    @future
    private static void insertUser(Id contactId) {
        User studentUser = new User(
                ContactId = contactId,
                UserName = COMMUNITY_USER_EMAIL,
                FirstName = COMMUNITY_USER_FIRST_NAME,
                LastName = COMMUNITY_USER_LAST_NAME,
                Email = COMMUNITY_USER_EMAIL,
                ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Student%' LIMIT 1].Id,
                Alias = 'snow',
                TimeZoneSidKey = UserInfo.getTimezone().getID(),
                LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
                EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
                LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert studentUser;
    }

    public class EmailMessageBuilder {
        private String fromAddress;
        private String toAddress;
        private String subject;
        private String htmlBody;
        private Id parentId;
        private Boolean incoming;

        public EmailMessageBuilder() {
            this.fromAddress = 'test.from@xyz.org';
            this.toAddress = 'test.to@xyz.org';
            this.subject = 'Test email';
            this.htmlBody = 'Test email body';
            this.incoming = true;
        }

        public EmailMessageBuilder withFromAddress(String fromAddress) {
            this.fromAddress = fromAddress;
            return this;
        }

        public EmailMessageBuilder withToAddress(String toAddress) {
            this.toAddress = toAddress;
            return this;
        }

        public EmailMessageBuilder withParentId(String parentId) {
            this.parentId = parentId;
            return this;
        }

        public EmailMessage create() {
            return new EmailMessage(
                    ToAddress= this.toAddress,
                    FromAddress = this.fromAddress,
                    Subject = this.subject,
                    HtmlBody = this.htmlBody,
                    ParentId = this.parentId,
                    Incoming = this.incoming
            );
        }

        public EmailMessage createAndSave() {
            EmailMessage emailMessage = this.create();
            insert emailMessage;
            return emailMessage;
        }
    }
}