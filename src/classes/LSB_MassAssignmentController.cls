public with sharing class LSB_MassAssignmentController {

    private static LSB_ReportHelper reportHelper = new LSB_ReportHelper();

    @AuraEnabled(Cacheable = true)
    public static List<Report> getReportsFromAssignmentFolder() {
        return reportHelper.getTabularReportsFromAssignmentFolder();
    }

    @AuraEnabled
    public static String runReportBatch(String reportId, String objectJSON, String sharingObject) {
        Set<String> contactIds = reportHelper.runReportAndGetContactIds(reportId);
        SObject taskObject = (SObject) JSON.deserialize(objectJSON, Task.class);

        LSB_MassAssignmentBatch assignmentBatch;
        if (sharingObject == LSB_Constants.CASE_OBJECT) {
            assignmentBatch = new LSB_MassAssignmentBatch(contactIds, LSB_MassAssignmentBatchType.ADVISEE, taskObject);
        } else {
            assignmentBatch = new LSB_MassAssignmentBatch(contactIds, LSB_MassAssignmentBatchType.SUPPORT_PROFILE, taskObject);
        }

        String jobId = Database.executeBatch(assignmentBatch, 200);

        if (Test.isRunningTest()) {
            return jobId;
        }

        return 'Contacts size: ' + contactIds.size();
    }

    @AuraEnabled(Cacheable = true)
    public static void validateReportColumns(String reportId) {
        reportHelper.validateReportColumns(reportId);
    }

    @AuraEnabled(Cacheable = true)
    public static List<User> fetchUsers(String userName) {
        String searchString = '%' + userName + '%';
        return [
            SELECT Id, Name
            FROM User
            WHERE Name LIKE :searchString
                AND UserType = 'Standard'
                AND IsActive = true
            LIMIT 15
        ];
    }

}