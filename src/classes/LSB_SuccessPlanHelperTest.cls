@isTest
private class LSB_SuccessPlanHelperTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman12345@pwc.test.com';
    private static final String ADVISOR_USERNAME = 'academic.advisor@lsbu.test.com';
    private static final String MANAGER_USERNAME = 'advisor.manager@lsbu.test.com';

    @testSetup
    static void createTestData() {
        insert new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = 'LSB_TriggerDispatcher',
                hed__Load_Order__c = 1,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = 'sfal__SuccessPlan__c'
        );

        insertUsers();
    }

    @future
    private static void insertUsers() {
        UserRole advisorRole = [SELECT Id, DeveloperName, Name FROM UserRole WHERE Name LIKE '[%]%' LIMIT 1];
        Profile advisorProfile = [SELECT Id, Name FROM Profile WHERE Name LIKE 'Academic Advisor' LIMIT 1];

        User manager = new User(
                UserName = MANAGER_USERNAME,
                FirstName = '',
                LastName = 'Manager',
                Email = 'manager@lsbu.test.com',
                ProfileId = advisorProfile.Id,
                UserRoleId = advisorRole.Id,
                Alias = 'mgr',
                TimeZoneSidKey = UserInfo.getTimezone().getID(),
                LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
                EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
                LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert manager;

        insert new User(
                UserName = ADVISOR_USERNAME,
                FirstName = 'Advisor',
                LastName = 'Academic',
                Email = 'academic.advisor@lsbu.test.com',
                ManagerId = manager.Id,
                ProfileId = advisorProfile.Id,
                UserRoleId = advisorRole.Id,
                Alias = 'advis',
                TimeZoneSidKey = UserInfo.getTimezone().getID(),
                LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
                EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
                LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );
    }

    @isTest
    private static void testInsertPrivateSuccessPlan() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];


        System.runAs(advisor) {
            Account a = new Account(Name='Test Account Name');
            insert a;

            Contact student = new Contact(
                    FirstName = 'John',
                    LastName = 'Snowman',
                    hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                    Email = COMMUNITY_USER_EMAIL,
                    LSB_CON_SourceSystem__c = 'QL',
                    LSB_ChannelOfPreference__c = 'Email',
                    LSB_ExternalID__c = '1234QWERTY',
                    AccountId = a.Id);

            insert student;

            sfal__SuccessPlan__c plan = new sfal__SuccessPlan__c(Name = 'Plan 1', LSB_Type__c = '0', sfal__Advisee__c = student.Id);

            Test.startTest();

            insert plan;

            Test.stopTest();

            List<sfal__SuccessPlan__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlan__Share
                    WHERE ParentId = :plan.Id AND
                    RowCause = :Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            System.assertEquals(1, shares.size());
            System.assertEquals('Edit', shares.get(0).AccessLevel);
            System.assertEquals([SELECT ID FROM User WHERE UserName = :MANAGER_USERNAME].Id, shares.get(0).UserOrGroupId);
        }
    }

    @isTest
    private static void testInsertOpenSuccessPlan() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            Account a = new Account(Name='Test Account Name');
            insert a;

            Contact student = new Contact(
                    FirstName = 'John',
                    LastName = 'Snowman',
                    hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                    Email = COMMUNITY_USER_EMAIL,
                    LSB_CON_SourceSystem__c = 'QL',
                    LSB_ChannelOfPreference__c = 'Email',
                    LSB_ExternalID__c = '1234QWERTY',
                    AccountId = a.Id);

            insert student;
            sfal__SuccessPlan__c plan = new sfal__SuccessPlan__c(Name = 'Plan 2', LSB_Type__c = '1', sfal__Advisee__c = student.Id);

            Test.startTest();

            insert plan;

            Test.stopTest();

            List<sfal__SuccessPlan__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlan__Share
                    WHERE ParentId = :plan.Id AND
                    RowCause = :Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            for (sfal__SuccessPlan__Share share : shares) {
                System.assertEquals('Edit', share.AccessLevel);
                System.assertEquals('00G', String.valueOf(share.UserOrGroupId).substring(0, 3));
            }
        }
    }

    @isTest
    private static void testChangeOpenSuccessPlanToPrivate() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            Account a = new Account(Name='Test Account Name');
            insert a;

            Contact student = new Contact(
                    FirstName = 'John',
                    LastName = 'Snowman',
                    hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                    Email = COMMUNITY_USER_EMAIL,
                    LSB_CON_SourceSystem__c = 'QL',
                    LSB_ChannelOfPreference__c = 'Email',
                    LSB_ExternalID__c = '1234QWERTY',
                    AccountId = a.Id);

            insert student;
            sfal__SuccessPlan__c plan = new sfal__SuccessPlan__c(Name = 'Plan 3', LSB_Type__c = '2', sfal__Advisee__c = student.Id);
            insert plan;

            plan.LSB_Type__c = '0';

            Test.startTest();

            update plan;

            Test.stopTest();

            List<sfal__SuccessPlan__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlan__Share
                    WHERE ParentId = :plan.Id AND
                    RowCause = :Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            System.assertEquals(1, shares.size());
            System.assertEquals('Edit', shares.get(0).AccessLevel);
            System.assertEquals([SELECT ID FROM User WHERE UserName = :MANAGER_USERNAME].Id, shares.get(0).UserOrGroupId);
        }
    }

    @isTest
    private static void testChangePrivateSuccessPlanToOpen() {
        User advisor = [SELECT Id FROM User WHERE UserName = :ADVISOR_USERNAME];

        System.runAs(advisor) {
            Account a = new Account(Name='Test Account Name');
            insert a;

            Contact student = new Contact(
                    FirstName = 'John',
                    LastName = 'Snowman',
                    hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                    Email = COMMUNITY_USER_EMAIL,
                    LSB_CON_SourceSystem__c = 'QL',
                    LSB_ChannelOfPreference__c = 'Email',
                    LSB_ExternalID__c = '1234QWERTY',
                    AccountId = a.Id);

            insert student;
            sfal__SuccessPlan__c plan = new sfal__SuccessPlan__c(Name = 'Plan 4', LSB_Type__c = '0', sfal__Advisee__c = student.Id);
            insert plan;

            plan.LSB_Type__c = '2';

            Test.startTest();

            update plan;

            Test.stopTest();

            List<sfal__SuccessPlan__Share> shares = [
                    SELECT Id, AccessLevel, UserOrGroupId
                    FROM sfal__SuccessPlan__Share
                    WHERE ParentId = :plan.Id AND
                    RowCause = :Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c
            ];

            for (sfal__SuccessPlan__Share share : shares) {
                System.assertEquals('Edit', share.AccessLevel);
                System.assertEquals('00G', String.valueOf(share.UserOrGroupId).substring(0, 3));
            }
        }
    }

}