/**
 * Created by kgromek002 on 23.08.2021.
 */

global class LSB_DMBulkSendCustomSubjectController implements mcdm_15.BulkSendPlugin {
    global static Map<String, Map<String, String>> getBulkSendPluginData(List<mcdm_15.JourneyApproval> approvals, Map<String, String> pluginData) {
        Map<String, Map<String, String>> IdsToCustomData = new Map<String, Map<String, String>>();
        if (pluginData.size() > 0) {
            for (mcdm_15.JourneyApproval approval : approvals) {
                IdsToCustomData.put(approval.objectId, pluginData);
            }
        }

        return IdsToCustomData;
    }
}