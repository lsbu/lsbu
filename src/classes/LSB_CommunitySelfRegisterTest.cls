/**
 * @descritpion Class contains methods that test Self Registration component.
 *
 * @date 11.09.2020.
 */

@isTest
public class LSB_CommunitySelfRegisterTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman12345@pwc.com';

    @testSetup
    static void createTestData() {
        Account a = new Account(Name='Test Account Name');
        insert a;

        List<Contact> contacts = new List<Contact> {
                new Contact(FirstName = 'Wilhelm', LastName = 'Grimm', Email = 'will.grimm@gmail.com', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWER'),
                new Contact(FirstName = 'John', LastName = 'Snowman', hed__AlternateEmail__c = COMMUNITY_USER_EMAIL, Email = COMMUNITY_USER_EMAIL, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = a.Id)
        };

        insert contacts;
    }

    @isTest
    public static void testCommunitySelfRegisterConfiguration() {
        Test.startTest();
        List<LSB_CommunitySelfRegisterController.LSB_FieldWrapper> inputFields = LSB_CommunitySelfRegisterController.fetchFields();
        Boolean isEmail = false;

        for (LSB_CommunitySelfRegisterController.LSB_FieldWrapper inputField : inputFields) {
            if (inputField.fieldAPIName.endsWithIgnoreCase('Email')) {
                isEmail = true;
            }
        }
        Test.stopTest();

        System.assert(isEmail);
    }

    @isTest
    public static void testCommunitySelfRegisterWithQLContactValidation() {
        String emailAddress = COMMUNITY_USER_EMAIL;

        Test.startTest();
        String message = LSB_CommunitySelfRegisterController.validateEmailAddress(emailAddress);
        Test.stopTest();

        System.assertEquals('', message);
    }

    @isTest
    public static void testCommunitySelfRegisterWithoutContactValidation() {
        String emailAddress = 'john.snow2@gmail.com';

        Test.startTest();
        String message = LSB_CommunitySelfRegisterController.validateEmailAddress(emailAddress);
        Test.stopTest();

        System.assertNotEquals('', message);
    }

    @isTest
    public static void testCommunitySelfRegisterWithQLContactCreation() {
        String emailAddress = COMMUNITY_USER_EMAIL;

        Test.startTest();
        String urlAddress = LSB_UserHelper.createCommunityUser(
                new User(
                        FirstName = 'John',
                        LastName = 'Snowman',
                        Email = emailAddress
                ),
                'KMDyzfik#lMtTXN6@dl6uU'
        );
        Test.stopTest();

        System.assertNotEquals('', urlAddress);
    }
}