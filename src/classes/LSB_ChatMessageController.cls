public with sharing class LSB_ChatMessageController {

    @AuraEnabled
    public static ArticleWrapper getKnowledgeDataById(String knowledgeId, String fieldApiName) {
        Knowledge__kav article = getArticleById(knowledgeId, fieldApiName);
        if (article != null) {
            String communityUrl = getCommunityUrl();
            return new ArticleWrapper(communityUrl, article, fieldApiName);
        }
        return null;
    }

    private static Knowledge__kav getArticleById(String knowledgeId, String fieldApiName) {
        String query = 'SELECT Id, UrlName, ' + fieldApiName + ' FROM Knowledge__kav WHERE Id = :knowledgeId';
        List<Knowledge__kav> articles = Database.query(query);
        if (!articles.isEmpty()) {
            return articles[0];
        }
        return null;
    }

    private static String getCommunityUrl() {
        LSB_CommunityUrl__c communitySettings = LSB_CommunityUrl__c.getOrgDefaults();
        if (communitySettings != null) {
            return communitySettings.LSB_CommunityURL__c;
        }
        return null;
    }

    public class ArticleWrapper {
        @AuraEnabled public String id { get; set; }
        @AuraEnabled public String communityUrl { get; set; }
        @AuraEnabled public String content { get; set; }
        @AuraEnabled public String urlName { get; set; }

        public ArticleWrapper(String url, Knowledge__kav article, String fieldApiName) {
            this.communityUrl = url;
            this.id = article.Id;
            this.content = (String)article.get(fieldApiName);
            this.urlName = article.UrlName;
        }
    }
}