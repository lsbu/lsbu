public without sharing class LSB_ContactHelper {

    /**
     * @description Method is using to create and assign Individual records to all newly created Contacts.
     * @param newContacts List of new contact records.
     */
    public static void createIndividuals(List<Contact> newContacts) {
        try {
            Map<Id, Individual> contactId2Individual = new Map<Id, Individual>();
            List<Contact> contacts = new List<Contact>();

            for (Contact newContact : newContacts) {
                contactId2Individual.put(newContact.Id, createIndividual(newContact));
            }

            insert contactId2Individual.values();

            for (Contact newContact : newContacts) {
                contacts.add(
                    new Contact(
                        Id = newContact.Id,
                        IndividualId = contactId2Individual.get(newContact.Id).Id
                    )
                );
            }

            update contacts;
        } catch(Exception ex) {
            System.debug('Problem with creating Individuals.\nError Message: ' + ex.getMessage() +
                    '\nStack Trace: ' + ex.getStackTraceString());
        }
    }

    /**
     * @description Method is using to delete Individual records that are associated with deleted Contacts.
     * @param deletedContacts List of deleted contact records.
     */
    public static void deleteIndividuals(Map<Id, Contact> deletedContacts) {
        try {
            List<Individual> individuals = new List<Individual>();
            Set<String> externalIds = new Set<String>();

            for (Contact deletedContact : deletedContacts.values()) {
                externalIds.add(deletedContact.Id + '#%');
                individuals.add(new Individual(Id = deletedContact.IndividualId));
            }

            List<sObject> recordsToDelete = new List<sObject>();

            recordsToDelete.addAll([SELECT Id FROM ContactPointConsent WHERE LSB_ExternalId__c LIKE :externalIds]);
            recordsToDelete.addAll([SELECT Id FROM ContactPointPhone WHERE LSB_ExternalId__c LIKE :externalIds]);
            recordsToDelete.addAll([SELECT Id FROM ContactPointEmail WHERE LSB_ExternalId__c LIKE :externalIds]);
            recordsToDelete.addAll(individuals);

            delete recordsToDelete;
        } catch (Exception ex) {
            System.debug('Problem with deleting Individuals.\nError Message: ' + ex.getMessage() +
                    '\nStack Trace: ' + ex.getStackTraceString());
        }
    }

    /**
     * @description Method is using to create and remove Contact Point Email, Contact Point Phone and
     * Contact Point Consent records based on the Contact data.
     * @param newContacts map of new Contacts.
     * @param oldContacts map of old Contacts.
     */
    public static void updateContactPoints(Map<Id, Contact> newContacts, Map<Id, Contact> oldContacts) {
        Set<String> newContactPointExternalIds = new Set<String>();
        List<ContactPointPhone> newContactPointPhones = new List<ContactPointPhone>();
        List<ContactPointEmail> newContactPointEmails = new List<ContactPointEmail>();
        Set<String> contactPointConsentsExternalIdsToRemove = new Set<String>();
        Set<String> contactPointPhonesExternalIdsToRemove = new Set<String>();
        Set<String> contactPointEmailsExternalIdsToRemove = new Set<String>();
        Set<Id> contactIdsToStopNotifications = new Set<Id>();
        Set<Id> contactIdsToRevokeNotifications = new Set<Id>();

        List<DataUsePurpose> defaultDataUsePurposes = [SELECT Id, LSB_Type__c FROM DataUsePurpose WHERE LSB_DAP_IsDefault__c = true];
        Id defaultLegitimateInterestDataUsePurposeId = null;
        Id defaultExplicitInterestDataUsePurposeId = null;

        for (DataUsePurpose record : defaultDataUsePurposes) {
            if (record.LSB_Type__c == LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST) {
                defaultExplicitInterestDataUsePurposeId = record.Id;
            } else if (record.LSB_Type__c == LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST) {
                defaultLegitimateInterestDataUsePurposeId = record.Id;
            }
        }

        for (Contact newContact : newContacts.values()) {
            Contact oldContact = oldContacts.get(newContact.Id);
            Boolean isInsert = (oldContact.IndividualId == null && newContact.IndividualId != null);
            Boolean isPrimaryPhoneUpdated = (oldContact.hed__PreferredPhone__c != newContact.hed__PreferredPhone__c);
            Boolean isPrimaryEmailUpdated = (oldContact.hed__Preferred_Email__c != newContact.hed__Preferred_Email__c);

            if ((isInsert && newContact.HomePhone != null) || (newContact.HomePhone != null && (newContact.HomePhone != oldContact.HomePhone || isPrimaryPhoneUpdated))) {
                newContactPointPhones.add(LSB_ContactPointHelper.createContactPointPhone(newContact.HomePhone, newContact, LSB_Constants.CONTACT_PREFERRED_HOME_PHONE));

                if (isInsert || oldContact.HomePhone == null) {
                    newContactPointExternalIds.add(LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_HOME_PHONE) + LSB_Constants.CONTACT_POINT_CONSENT_PHONE_CALL);
                }
            } else if (newContact.HomePhone == null && oldContact.HomePhone != null) {
                String externalId = LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_HOME_PHONE);
                contactPointPhonesExternalIdsToRemove.add(externalId);
                contactPointConsentsExternalIdsToRemove.add(externalId + '%');
            }

            if ((isInsert && newContact.MobilePhone != null) || (newContact.MobilePhone != null && (newContact.MobilePhone != oldContact.MobilePhone || isPrimaryPhoneUpdated))) {
                newContactPointPhones.add(LSB_ContactPointHelper.createContactPointPhone(newContact.MobilePhone, newContact, LSB_Constants.CONTACT_PREFERRED_PHONE_MOBILE));

                if (isInsert || oldContact.MobilePhone == null) {
                    newContactPointExternalIds.add(LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_PHONE_MOBILE) + LSB_Constants.CONTACT_POINT_CONSENT_PHONE_CALL);
                    newContactPointExternalIds.add(LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_PHONE_MOBILE) + LSB_Constants.CONTACT_POINT_CONSENT_PHONE_SMS);
                }
            } else if (newContact.MobilePhone == null && oldContact.MobilePhone != null) {
                String externalId = LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_PHONE_MOBILE);
                contactPointPhonesExternalIdsToRemove.add(externalId);
                contactPointConsentsExternalIdsToRemove.add(externalId + '%');
            }

            if ((isInsert && newContact.hed__AlternateEmail__c != null) ||
                    (newContact.hed__AlternateEmail__c != null && (newContact.hed__AlternateEmail__c != oldContact.hed__AlternateEmail__c || isPrimaryEmailUpdated))) {
                newContactPointEmails.add(LSB_ContactPointHelper.createContactPointEmail(newContact.hed__AlternateEmail__c, newContact, LSB_Constants.CONTACT_PREFERRED_EMAIL_PERSONAL));

                if (isInsert || oldContact.hed__AlternateEmail__c == null) {
                    newContactPointExternalIds.add(LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_EMAIL_PERSONAL));
                }
            } else if (newContact.hed__AlternateEmail__c == null && oldContact.hed__AlternateEmail__c != null) {
                String externalId = LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_EMAIL_PERSONAL);
                contactPointEmailsExternalIdsToRemove.add(externalId);
                contactPointConsentsExternalIdsToRemove.add(externalId + '%');
            }

            if ((isInsert && newContact.hed__UniversityEmail__c != null) ||
                    (newContact.hed__UniversityEmail__c != null && (newContact.hed__UniversityEmail__c != oldContact.hed__UniversityEmail__c || isPrimaryEmailUpdated))) {
                newContactPointEmails.add(LSB_ContactPointHelper.createContactPointEmail(newContact.hed__UniversityEmail__c, newContact, LSB_Constants.CONTACT_PREFERRED_EMAIL_UNIVERSITY));

                if (isInsert || oldContact.hed__UniversityEmail__c == null) {
                    newContactPointExternalIds.add(LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_EMAIL_UNIVERSITY));
                }
            } else if (newContact.hed__UniversityEmail__c == null && oldContact.hed__UniversityEmail__c != null) {
                String externalId = LSB_ContactPointHelper.getContactPointExternalId(newContact, LSB_Constants.CONTACT_PREFERRED_EMAIL_UNIVERSITY);
                contactPointEmailsExternalIdsToRemove.add(externalId);
                contactPointConsentsExternalIdsToRemove.add(externalId + '%');
            }

            if (!isInsert && newContact.LSB_CON_StopEmailNotifications__c && !oldContact.LSB_CON_StopEmailNotifications__c) {
                contactIdsToStopNotifications.add(newContact.Id);
            } else if (!isInsert && !newContact.LSB_CON_StopEmailNotifications__c && oldContact.LSB_CON_StopEmailNotifications__c) {
                contactIdsToRevokeNotifications.add(newContact.Id);
            }
        }

        // upsert ContactPointPhones and ContactPointEmails
        if (!newContactPointPhones.isEmpty()) {
            upsert newContactPointPhones LSB_ExternalId__c;
        }

        if (!newContactPointEmails.isEmpty()) {
            upsert newContactPointEmails LSB_ExternalId__c;
        }

        Map<String, sObject> contactPoints = new Map<String, sObject>();

        for (ContactPointPhone contactPhone : newContactPointPhones) {
            contactPoints.put(contactPhone.LSB_ExternalId__c, contactPhone);
        }

        for (ContactPointEmail contactEmail : newContactPointEmails) {
            contactPoints.put(contactEmail.LSB_ExternalId__c, contactEmail);
        }

        // upsert ContactPointConsents
        List<ContactPointConsent> consents = new List<ContactPointConsent>();

        for (String externalId : newContactPointExternalIds) {
            Contact newContact = newContacts.get(externalId.substringBefore('#'));
            String key = externalId.remove(LSB_Constants.CONTACT_POINT_CONSENT_PHONE_CALL).remove(LSB_Constants.CONTACT_POINT_CONSENT_PHONE_SMS);

            if (defaultLegitimateInterestDataUsePurposeId != null) {
                consents.add(LSB_ContactPointHelper.createContactPointConsent(
                        externalId + '#' + LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST,
                        LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST,
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN,
                        newContact,
                        contactPoints.get(key).Id,
                        defaultLegitimateInterestDataUsePurposeId)
                );
            }

            if (defaultExplicitInterestDataUsePurposeId != null) {
                consents.add(LSB_ContactPointHelper.createContactPointConsent(
                        externalId + '#' + LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST,
                        LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST,
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_NOTSEEN,
                        newContact,
                        contactPoints.get(key).Id,
                        defaultExplicitInterestDataUsePurposeId)
                );
            }
        }

        for (ContactPointConsent consent : [
                SELECT Id, LSB_COC_Contact__c, LSB_COC_Contact__r.Individual.HasOptedOutProcessing,
                        LSB_COC_Contact__r.Individual.HasOptedOutSolicit, DataUsePurpose.LSB_Type__c
                FROM ContactPointConsent
                WHERE LSB_COC_Contact__c IN :contactIdsToStopNotifications OR LSB_COC_Contact__c IN :contactIdsToRevokeNotifications]) {
            if (contactIdsToStopNotifications.contains(consent.LSB_COC_Contact__c)) {
                consent.PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT;
                consent.EffectiveTo = System.now();
                consents.add(consent);
            }

            if (contactIdsToRevokeNotifications.contains(consent.LSB_COC_Contact__c)) {
                if (consent.DataUsePurpose != null &&
                    consent.DataUsePurpose.LSB_Type__c.equals(LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST) ||
                    consent.LSB_COC_Contact__r.Individual.HasOptedOutProcessing) {
                    continue;
                }

                consent.PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN;
                consent.EffectiveFrom = System.now();
                consent.EffectiveTo = null;
                consents.add(consent);
            }
        }

        if (!consents.isEmpty()) {
            upsert consents;
        }

        // delete ContactPointPhones, ContactPointEmails and ContactPointConsents
        List<sObject> contactPointsToRemove = new List<sObject>();
        contactPointsToRemove.addAll([
                SELECT Id
                FROM ContactPointConsent
                WHERE LSB_ExternalId__c LIKE :contactPointConsentsExternalIdsToRemove]);
        contactPointsToRemove.addAll([SELECT Id FROM ContactPointPhone WHERE LSB_ExternalId__c IN :contactPointPhonesExternalIdsToRemove]);
        contactPointsToRemove.addAll([SELECT Id FROM ContactPointEmail WHERE LSB_ExternalId__c IN :contactPointEmailsExternalIdsToRemove]);

        if (!contactPointsToRemove.isEmpty()) {
            delete contactPointsToRemove;
        }
    }

    private static Individual createIndividual(Contact contactRecord) {
        return new Individual(
            FirstName = contactRecord.FirstName,
            LastName = contactRecord.LastName,
            BirthDate = contactRecord.BirthDate,
            LSB_Contact__c = contactRecord.Id
        );
    }

    public static void processUpdatedContactPointConsents(Map<Id, ContactPointConsent> newContactPoints, Map<Id, ContactPointConsent> oldContactPoints) {
        for (ContactPointConsent newContactPoint : newContactPoints.values()) {
            if (newContactPoint.PrivacyConsentStatus != oldContactPoints.get(newContactPoint.Id).PrivacyConsentStatus &&
                    newContactPoint.PrivacyConsentStatus == LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN) {
                newContactPoint.EffectiveFrom = Datetime.now();
                newContactPoint.EffectiveTo = null;
            } else if (newContactPoint.PrivacyConsentStatus != oldContactPoints.get(newContactPoint.Id).PrivacyConsentStatus &&
                    newContactPoint.PrivacyConsentStatus == LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT) {
                newContactPoint.EffectiveTo = Datetime.now();
            }
        }
    }
}