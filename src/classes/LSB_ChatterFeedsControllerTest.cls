@IsTest
class LSB_ChatterFeedsControllerTest {

    @TestSetup
    static void testData() {

        Account account = new Account(Name = 'Test Account');
        insert account;

        Contact contact = new Contact(AccountId = account.Id, FirstName = 'Test', LastName = 'Contact', LSB_CON_CurrentRole__c = 'Prospect');
        insert contact;

        Case testCase = new Case(Subject = 'Test Case', ContactId = contact.Id, LSB_CAS_ContactRole__c = 'Prospect');
        insert testCase;

        FeedItem feed = new FeedItem (
            ParentId = testCase.Id,
            Type = 'TextPost',
            Body = 'Hello',
            Visibility = 'AllUsers'
        );

        insert feed;
    }

    @IsTest
    static void fetchChatterFeedsTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        List<FeedItem> feedItems = LSB_ChatterFeedsController.fetchChatterFeeds(testCase.Id);
        Test.stopTest();
        System.assertEquals(1, feedItems.size());
    }

    @IsTest
    static void createFeedPostTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        List<FeedItem> feedItems = LSB_ChatterFeedsController.fetchChatterFeeds(testCase.Id);
        Test.startTest();
        FeedComment feedComment = new FeedComment(
            FeedItemId = feedItems[0].Id,
            CommentBody = 'Test',
            CommentType = 'TextComment'
        );
        LSB_ChatterFeedsController.createFeedPost(feedComment);
        Test.stopTest();
        List<FeedItem> feedItems2 = LSB_ChatterFeedsController.fetchChatterFeeds(testCase.Id);
        System.assertEquals(1, feedItems2[0].FeedComments.size());
    }

    @IsTest
    static void createFeedItemTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        LSB_ChatterFeedsController.createFeedItem(testCase.Id, 'Test Message');
        Test.stopTest();
        List<FeedItem> feedItems2 = LSB_ChatterFeedsController.fetchChatterFeeds(testCase.Id);
        System.assertEquals(2, feedItems2.size());
    }

}