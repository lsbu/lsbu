public with sharing class LSB_CommunityLoginController {

    /**
    * @description Method is using for login to Community.
    * It is used lsb_CommunityLogin lwc component and invokes LSB_UserHelper.loginToCommunity method.
    *
    * @param username Username.
    * @param password Password that will be used for login.
    * @param startURL URL where user will be redirected after logging in.
    *
    * @return LoginResult wrapper with start URL or error message.
    */
    @AuraEnabled
    public static LSB_UserHelper.LoginResult login(String username, String password, String startURL) {
        return LSB_UserHelper.loginToCommunity(username, password, startURL);
    }
}