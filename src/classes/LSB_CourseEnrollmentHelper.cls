public without sharing class LSB_CourseEnrollmentHelper {

    private static final String ENROLLMENT_STATUS_FULLY_ENROLLED = LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED;
    private static final String ENROLLMENT_COMPONENT_TYPE_COURSEWORK = LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK;
    private static final String ENROLLMENT_COMPONENT_TYPE_EXAM = LSB_Constants.ENROLLMENT_COMPONENT_TYPE_EXAM;
    private static final String CLAIM_REQUEST_TYPE_EC = LSB_Constants.CLAIM_REQUEST_TYPE_EC;

    public List<LSB_CourseEnrollmentHelper.ModuleWrapper> getModulesWithAssessments(String contactId, String requestType) {
        Id componentRecordTypeId = hed__Course_Enrollment__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName()
                .get(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId();
        Id subcomponentRecordTypeId = hed__Course_Enrollment__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName()
                .get(LSB_Constants.COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId();

        Map<Id, hed__Course_Enrollment__c> id2Module = getModulesFromProgramEnrollmentByContactId(contactId);
        Map<Id, hed__Course_Enrollment__c> id2Component = new Map<Id, hed__Course_Enrollment__c>();
        Map<Id, List<hed__Course_Enrollment__c>> moduleId2Components = new Map<Id, List<hed__Course_Enrollment__c>>();
        Map<Id, hed__Course_Enrollment__c> id2Subcomponent = new Map<Id, hed__Course_Enrollment__c>();
        Map<Id, List<hed__Course_Enrollment__c>> componentId2subcomponents = new Map<Id, List<hed__Course_Enrollment__c>>();
        id2Component = getIdToAssessmentByParent(id2Module.keySet(), componentRecordTypeId);
        moduleId2Components = getParentId2ChildrenMap(id2Component);
        id2Subcomponent = getIdToAssessmentByParent(id2Component.keySet(), subcomponentRecordTypeId);
        componentId2subcomponents = getParentId2ChildrenMap(id2Subcomponent);

        return createModuleWrappers(moduleId2Components, id2Module, componentId2subcomponents, requestType);
    }

    private List<LSB_CourseEnrollmentHelper.ModuleWrapper> createModuleWrappers(Map<Id, List<hed__Course_Enrollment__c>> moduleId2Components,
            Map<Id, hed__Course_Enrollment__c> id2Module, Map<Id, List<hed__Course_Enrollment__c>> componentId2subcomponents, String requestType) {

        List<LSB_CourseEnrollmentHelper.ModuleWrapper> moduleWrappers = new List<LSB_CourseEnrollmentHelper.ModuleWrapper>();

        for (Id moduleId : moduleId2Components.keySet()) {
            hed__Course_Enrollment__c module = id2Module.get(moduleId);
            List<hed__Course_Enrollment__c> assessments = new List<hed__Course_Enrollment__c>();
            for (hed__Course_Enrollment__c component : moduleId2Components.get(moduleId)) {
                if (componentId2subcomponents.containsKey(component.Id) && !(componentId2subcomponents.get(component.Id).isEmpty())) {
                    assessments.addAll(componentId2subcomponents.get(component.Id));
                } else {
                    assessments.add(component);
                }
            }
            if (!(requestType == CLAIM_REQUEST_TYPE_EC)) {
                assessments = filterFutureCourseworkAssessments(assessments);
            }
            if (!assessments.isEmpty()) {
                moduleWrappers.add(new ModuleWrapper(module, assessments));
            }
        }

        return moduleWrappers;
    }

    private  List<hed__Course_Enrollment__c> filterFutureCourseworkAssessments( List<hed__Course_Enrollment__c> assessments) {
        List<hed__Course_Enrollment__c> futureAssessments = new List<hed__Course_Enrollment__c>();
        for (hed__Course_Enrollment__c assessment : assessments) {
            if (assessment.LSB_CCN_HandInDate__c != null && assessment.LSB_CCN_HandInDate__c >= Date.today()
                    && assessment.LSB_CCN_ComponentType__c == ENROLLMENT_COMPONENT_TYPE_COURSEWORK) {
                futureAssessments.add(assessment);
            }
        }
        return futureAssessments;
    }

    public Map<Id, hed__Course_Enrollment__c> getModulesFromProgramEnrollmentByContactId(String contactId) {
        Map<Id, hed__Course_Enrollment__c> id2Module = new Map<Id, hed__Course_Enrollment__c>( [
                SELECT ID, hed__Program_Enrollment__c, LSB_CCN_ModuleInstanceName__c, LSB_CCN_ModuleEnrolmentStatus__c, Name
                FROM hed__Course_Enrollment__c
                WHERE LSB_CCN_ModuleEnrolmentStatus__c =: ENROLLMENT_STATUS_FULLY_ENROLLED AND hed__Program_Enrollment__c IN
                (
                        SELECT Id FROM hed__Program_Enrollment__c
                        WHERE LSB_PEN_LatestEnrolmentRecord__c = TRUE
                        AND hed__Enrollment_Status__c =: ENROLLMENT_STATUS_FULLY_ENROLLED
                        AND hed__Contact__c = : contactId
                )
        ]);

        return id2Module;
    }

    private Map<Id, hed__Course_Enrollment__c> getIdToAssessmentByParent(Set<Id> parentIds, Id recordTypeId) {
        Map<Id, hed__Course_Enrollment__c> id2Assessment = new Map<Id, hed__Course_Enrollment__c>( [
                SELECT Id, Name, LSB_CCN_ComponentDescription__c, LSB_CCN_SubComponentDescription__c, LSB_CCN_ParentModuleComponent__c,
                        RecordTypeId, LSB_CCN_SubComponentId__c, LSB_CCN_HandInDate__c, LSB_CCN_Resit__c, LSB_CCN_ComponentType__c
                FROM hed__Course_Enrollment__c
                WHERE LSB_CCN_ParentModuleComponent__c IN : parentIds AND RecordTypeId =: recordTypeId
        ]);

        return id2Assessment;
    }

    private Map<Id, List<hed__Course_Enrollment__c>> getParentId2ChildrenMap(Map<Id, hed__Course_Enrollment__c> id2Component) {
        Map<Id, List<hed__Course_Enrollment__c>> moduleId2Components = new Map<Id, List<hed__Course_Enrollment__c>>();

        for (hed__Course_Enrollment__c component : id2Component.values()) {
            if (moduleId2Components.containsKey(component.LSB_CCN_ParentModuleComponent__c)) {
                moduleId2Components.get(component.LSB_CCN_ParentModuleComponent__c).add(component);
            } else {
                moduleId2Components.put(component.LSB_CCN_ParentModuleComponent__c, new List<hed__Course_Enrollment__c> {component});
            }
        }

        return moduleId2Components;
    }

    public class ModuleWrapper {
        @AuraEnabled public String id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public List<LSB_CourseEnrollmentHelper.AssessmentWrapper> assessments { get; set; }

        public ModuleWrapper(hed__Course_Enrollment__c module, List<hed__Course_Enrollment__c> assessments) {
            this.id = module.Id;
            this.name = module.LSB_CCN_ModuleInstanceName__c;
            this.assessments = setAssessments(assessments);
        }

        public List<AssessmentWrapper> setAssessments(List<hed__Course_Enrollment__c> assessments) {
            List<AssessmentWrapper> assessmentWrappers = new List<AssessmentWrapper>();
            for (hed__Course_Enrollment__c assessment : assessments) {
                assessmentWrappers.add(new AssessmentWrapper(assessment));
            }
            return  assessmentWrappers;
        }
    }

    public List<hed__Course_Enrollment__c> getCourseEnrollmentById(Id courseEnrolmentId) {
        List<hed__Course_Enrollment__c> enrollments = [SELECT Id, Name, LSB_CCN_ComponentDescription__c, LSB_CCN_SubComponentDescription__c, LSB_CCN_ParentModuleComponent__c,
                RecordTypeId, LSB_CCN_SubComponentId__c, LSB_CCN_HandInDate__c, LSB_CCN_Resit__c, LSB_CCN_ComponentType__c
        FROM hed__Course_Enrollment__c WHERE Id =: courseEnrolmentId];
        return enrollments;
    }

    public class AssessmentWrapper {
        @AuraEnabled public String value { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public Boolean isRetrospective { get; set; }
        @AuraEnabled public Boolean isResit { get; set; }
        @AuraEnabled public Boolean isExam { get; set; }

        public AssessmentWrapper(hed__Course_Enrollment__c assessment) {
            this.value = assessment.Id;
            this.label = setAssessmentName(assessment);
            this.isRetrospective = assessment.LSB_CCN_HandInDate__c < Date.today();
            this.isResit = assessment.LSB_CCN_Resit__c;
            this.isExam = assessment.LSB_CCN_ComponentType__c == ENROLLMENT_COMPONENT_TYPE_EXAM;
        }

        private String setAssessmentName(hed__Course_Enrollment__c assessment) {
            if (!String.isBlank(assessment.LSB_CCN_ComponentDescription__c)) {
                return assessment.LSB_CCN_ComponentDescription__c;
            } else {
                return assessment.LSB_CCN_SubComponentDescription__c;
            }
        }
    }
}