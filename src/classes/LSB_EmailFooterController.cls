public without sharing class LSB_EmailFooterController {
    public String footerTitle;
    public Map<String, String> socialMediaMap { get; set; }
    public String imageUrl { get; set; }
    public String socialImageUrl { get; set; }

    public LSB_EmailFooterController() {
        socialMediaMap = new Map<String, String>();
        for (Document doc : [
                SELECT DeveloperName,Id, Name, SystemModstamp
                FROM Document
                WHERE DeveloperName IN('facebook_logo', 'instagram_logo', 'twitter_logo')
        ]) {
            socialImageUrl = URL.getSalesforceBaseUrl().getProtocol() + '://' + System.URL.getSalesforceBaseUrl().getHost().
                    remove('-api').replace('visual', 'content') + '/servlet/servlet.ImageServer?id=' + doc.Id
                    + '&oid=' + UserInfo.getOrganizationId();
            socialMediaMap.put(doc.DeveloperName, socialImageUrl);
        }
    }

    public void setFooterTitle(String footer) {
        if (this.footerTitle == null) {
            this.footerTitle = footer;
            List<Document> docList = [
                    SELECT Id, Name, SystemModstamp
                    FROM Document
                    WHERE DeveloperName = :footerTitle
                    LIMIT 1
            ];
            if (!docList.isEmpty()) {
                imageUrl = URL.getSalesforceBaseUrl().getProtocol() + '://' + System.URL.getSalesforceBaseUrl().
                        getHost().remove('-api').replace('visual', 'content') + '/servlet/servlet.ImageServer?id='
                        + docList.get(0).Id + '&oid=' + UserInfo.getOrganizationId();
            }
        }
    }
    public String getFooterTitle() {
        return footerTitle;
    }
}