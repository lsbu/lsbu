public with sharing class LSB_ReviewHelper {

    private static final String THREE_MONTHS_FOR_REVIEW = LSB_Constants.THREE_MONTHS_FOR_REVIEW;
    private static final String SIX_MONTHS_FOR_REVIEW = LSB_Constants.SIX_MONTHS_FOR_REVIEW;
    private static final String TWELVE_MONTHS_FOR_REVIEW = LSB_Constants.TWELVE_MONTHS_FOR_REVIEW;
    private static final String REVIEW_STATUS_NEW = LSB_Constants.REVIEW_STATUS_NEW;
    private static final String REVIEW_STATUS_NO_LONGER_REQUIRED = LSB_Constants.REVIEW_STATUS_NO_LONGER_REQUIRED;

    public void processNewReviews(List<LSB_REW_Review__c> newReviews) {
        List<LSB_REW_Review__c> closedReviews = markExistingReviewsAsNoLongerRequired(newReviews);

        update closedReviews;
    }

    public void processUpdatedReviews(Map<Id, LSB_REW_Review__c> newReviews, Map<Id, LSB_REW_Review__c> oldReviews) {
        Map<Id, LSB_REW_Review__c> doneReviews = filterDoneReviews(newReviews, oldReviews);
        processDoneReviews(doneReviews);
    }

    private List<LSB_REW_Review__c> markExistingReviewsAsNoLongerRequired(List<LSB_REW_Review__c> newReviews) {
        Set<Id> knowledgeArticleVersionIds = new Set<Id>();
        for (LSB_REW_Review__c review : newReviews) {
            knowledgeArticleVersionIds.add(review.LSB_REW_KnowledgeArticle__c);
        }
        Set<Id> knowledgeArticleIds = getKnowledgeArticleIdsByKnowledgeArticleVersionId(knowledgeArticleVersionIds);
        List<LSB_REW_Review__c> reviews = getReviewsByKnowledgeArticleIds(knowledgeArticleIds, newReviews);

        List<LSB_REW_Review__c> closedReviews = setReviewsStatusAsClosed(reviews);
        return closedReviews;
    }

    private void processDoneReviews(Map<Id, LSB_REW_Review__c> doneReviews) {
        List<LSB_REW_Review__c> reviews2Create = new List<LSB_REW_Review__c>();
        Map<LSB_REW_Review__c, Knowledge__kav> review2KnowledgeArticleVersion = getReview2KnowledgeArticleVersion(doneReviews.values());
        for (LSB_REW_Review__c doneReview : doneReviews.values()) {
            LSB_REW_Review__c review2Create = createNewReview(review2KnowledgeArticleVersion.get(doneReview));
            reviews2Create.add(review2Create);
        }
        insert reviews2Create;
    }

    private LSB_REW_Review__c createNewReview(Knowledge__kav articleVersion) {
        LSB_REW_Review__c newReview = new LSB_REW_Review__c(
                LSB_REW_KnowledgeArticle__c = articleVersion.Id,
                LSB_REW_ReviewStatus__c = REVIEW_STATUS_NEW,
                LSB_REW_ReviewDueDate__c = setReviewDueDate(articleVersion)
        );
        return newReview;
    }

    private Date setReviewDueDate(Knowledge__kav articleVersion) {
        if (articleVersion.LSB_KGE_DateForReview__c == null) {
            return null;
        } else if (articleVersion.LSB_KGE_DateForReview__c == THREE_MONTHS_FOR_REVIEW) {
            return Date.today().addMonths(3);
        } else if (articleVersion.LSB_KGE_DateForReview__c == SIX_MONTHS_FOR_REVIEW) {
            return Date.today().addMonths(6);
        } else if (articleVersion.LSB_KGE_DateForReview__c == TWELVE_MONTHS_FOR_REVIEW) {
            return Date.today().addMonths(12);
        } else {
            return Date.today().addMonths(24);
        }
    }

    private Map<LSB_REW_Review__c, Knowledge__kav> getReview2KnowledgeArticleVersion(List<LSB_REW_Review__c> doneReviews) {
        Map<LSB_REW_Review__c, Knowledge__kav> review2KnowledgeArticleVersion = new Map<LSB_REW_Review__c, Knowledge__kav>();
        Set<Id> articleIds = new Set<Id>();
        for (LSB_REW_Review__c doneReview : doneReviews) {
            articleIds.add(doneReview.LSB_REW_KnowledgeArticle__c);
        }
        List<Knowledge__kav> articleVersions = [SELECT Id, LSB_KGE_DateForReview__c FROM Knowledge__kav WHERE Id IN :articleIds];
        for (LSB_REW_Review__c doneReview : doneReviews) {
            for (Knowledge__kav article : articleVersions) {
                if (doneReview.LSB_REW_KnowledgeArticle__c == article.Id) {
                    review2KnowledgeArticleVersion.put(doneReview, article);
                }
            }
        }
        return review2KnowledgeArticleVersion;
    }

    private Map<Id, LSB_REW_Review__c> filterDoneReviews(Map<Id, LSB_REW_Review__c> newReviews, Map<Id, LSB_REW_Review__c> oldReviews) {
        Map<Id, LSB_REW_Review__c> doneReviews = new Map<Id, LSB_REW_Review__c>();
        for (Id newReviewId : newReviews.keySet()) {
            if (newReviews.get(newReviewId).LSB_REW_ReviewStatus__c != oldReviews.get(newReviewId).LSB_REW_ReviewStatus__c && newReviews.get(newReviewId).LSB_REW_ReviewStatus__c == 'Done') {
                doneReviews.put(newReviewId, newReviews.get(newReviewId));
            }
        }
        return doneReviews;
    }

    private List<LSB_REW_Review__c> setReviewsStatusAsClosed(List<LSB_REW_Review__c> knowledgeReviews) {
        List<LSB_REW_Review__c> closedReviews = new List<LSB_REW_Review__c>();
        for (LSB_REW_Review__c singleReview : knowledgeReviews) {
            singleReview.LSB_REW_ReviewStatus__c = REVIEW_STATUS_NO_LONGER_REQUIRED;
            closedReviews.add(singleReview);
        }
        return closedReviews;
    }

    private List<LSB_REW_Review__c> getReviewsByKnowledgeArticleIds(Set<Id> knowledgeArticleIds, List<LSB_REW_Review__c> newReviews) {
        Set<Id> newReviewIds = new Set<Id>();
        for (LSB_REW_Review__c newReview : newReviews) {
            newReviewIds.add(newReview.Id);
        }
        List<LSB_REW_Review__c> reviews = [
                SELECT Id, LSB_REW_KnowledgeArticle__c, LSB_REW_KnowledgeArticle__r.KnowledgeArticleId, LSB_REW_ReviewStatus__c
                FROM LSB_REW_Review__c
                WHERE LSB_REW_KnowledgeArticle__r.KnowledgeArticleId IN :knowledgeArticleIds AND Id NOT IN :newReviewIds AND LSB_REW_ReviewStatus__c IN ('New', 'Pending')
        ];
        return reviews;
    }

    private Set<Id> getKnowledgeArticleIdsByKnowledgeArticleVersionId(Set<Id> knowledgeArticleVersionIds) {
        List<Knowledge__kav> knowledgeArticleVersions = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id IN :knowledgeArticleVersionIds];
        Set<Id> knowledgeArticleIds = new Set<Id>();
        for (Knowledge__kav knowledgeVersion : knowledgeArticleVersions) {
            knowledgeArticleIds.add(knowledgeVersion.KnowledgeArticleId);
        }
        return knowledgeArticleIds;
    }
}