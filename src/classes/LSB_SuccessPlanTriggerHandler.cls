public without sharing class LSB_SuccessPlanTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;
    private LSB_SuccessPlanHelper successPlanHelper;

    public LSB_SuccessPlanTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
        this.successPlanHelper = new LSB_SuccessPlanHelper();
    }

    public void beforeInsert(List<SObject> newList) {
        successPlanHelper.copyValuesFromTemplate((List<sfal__SuccessPlan__c>) newList);
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
        successPlanHelper.applyDefaultTeam((List<sfal__SuccessPlan__c>) newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        successPlanHelper.handleTypeChange(
                (List<sfal__SuccessPlan__c>) newItems.values(),
                (Map<Id, sfal__SuccessPlan__c>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> newItems) {}

}