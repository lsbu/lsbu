@IsTest
public with sharing class LSB_DevelopmentPlanTaskBatchTest {
    private static final String STUDENT_USER_EMAIL = 'student_test@test.test.pl';

    @TestSetup
    public static void prepareData() {

        Account acc = new Account(Name = 'Test Account Name Student');
        insert acc;

        User studentUser = new User(
            UserName = 'advisor_12345@pwc.test.com',
            FirstName = 'John',
            LastName = 'Advisor',
            Email = +'advisor12345@pwc.test.com',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Advisor' LIMIT 1].Id,
            Alias = 'Adv',
            TimeZoneSidKey = UserInfo.getTimezone().getID(),
            LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
            EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
            LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert studentUser;

        Contact student = new Contact(
            FirstName = 'John',
            LastName = 'Student',
            hed__AlternateEmail__c = STUDENT_USER_EMAIL,
            Email = STUDENT_USER_EMAIL,
            LSB_CON_SourceSystem__c = 'QL',
            LSB_ChannelOfPreference__c = 'Email',
            LSB_ExternalID__c = 'QWERTY1234',
            AccountId = acc.Id,
            LSB_CON_User__c = studentUser.Id,
            LSB_CON_CurrentRole__c = 'Student'
        );

        insert student;

        hed__Term__c term = new hed__Term__c(
            hed__Account__c = acc.Id,
            Name = 'Test Term',
            hed__Start_Date__c = Date.today()
        );
        insert term;

        hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
            LSB_PEN_Term__c = term.Id,
            hed__Account__c = acc.Id,
            hed__Contact__c = student.Id,
            hed__Class_Standing__c = '2',
            LSB_PEN_LatestEnrolmentRecord__c = true,
            hed__Enrollment_Status__c = 'EFE'
        );

        insert programEnrollment;

    }

    @IsTest
    public static void testTaskCreation() {
        hed__Term__c t = [SELECT Id, hed__Start_Date__c FROM hed__Term__c];
        Contact c = [SELECT Id, LSB_CON_ContactRole__c, LSB_CON_User__c FROM Contact];
        hed__Program_Enrollment__c pe = [SELECT Id FROM hed__Program_Enrollment__c];

        Test.startTest();
        LSB_DevelopmentPlanTaskBatch schedulableBatch = new LSB_DevelopmentPlanTaskBatch();
        Database.executeBatch(schedulableBatch);
        Test.stopTest();

        Task tsk = [SELECT OwnerId, LSB_ACT_SourceProgramEnrollment__c, Subject FROM Task];

        System.assertEquals(c.LSB_CON_User__c, tsk.OwnerId);
        System.assertEquals(pe.Id, tsk.LSB_ACT_SourceProgramEnrollment__c);
        System.assertEquals(LSB_DevelopmentPlanTaskBatch.TASK_SUBJECT, tsk.Subject);
    }

    @IsTest
    public static void testNoTaskCreation() {
        hed__Term__c t = [SELECT Id, hed__Start_Date__c FROM hed__Term__c];
        Contact c = [SELECT Id, LSB_CON_ContactRole__c, LSB_CON_User__c FROM Contact];
        hed__Program_Enrollment__c pe = [SELECT Id FROM hed__Program_Enrollment__c];

        t.hed__Start_Date__c = Date.today().addDays(2);
        update t;

        Test.startTest();
        LSB_DevelopmentPlanTaskBatch schedulableBatch = new LSB_DevelopmentPlanTaskBatch();
        Database.executeBatch(schedulableBatch);
        Test.stopTest();

        List<Task> taskList = [SELECT OwnerId, LSB_ACT_SourceProgramEnrollment__c, Subject FROM Task];

        System.assert(taskList.isEmpty());
    }

    @IsTest
    public static void testNoTaskCreationFirstYear() {
        hed__Term__c t = [SELECT Id, hed__Start_Date__c FROM hed__Term__c];
        Contact c = [SELECT Id, LSB_CON_ContactRole__c, LSB_CON_User__c FROM Contact];
        hed__Program_Enrollment__c pe = [SELECT Id FROM hed__Program_Enrollment__c];

        pe.hed__Class_Standing__c = '1';
        update pe;

        Test.startTest();
        LSB_DevelopmentPlanTaskBatch schedulableBatch = new LSB_DevelopmentPlanTaskBatch();
        Database.executeBatch(schedulableBatch);
        Test.stopTest();

        List<Task> taskList = [SELECT OwnerId, LSB_ACT_SourceProgramEnrollment__c, Subject FROM Task];

        System.assert(taskList.isEmpty());
    }
}