public with sharing class LSB_PreChatFormController {

    @auraEnabled
    public static LSB_PreChatFormController.LSB_PreChatConfiguration getConfiguration() {
        Map<String, List<String>> contactRoleToNatureOfEnquiry = LSB_Utils.getDependentOptions(
                'Case', LSB_Constants.CASE_CONTACT_ROLE_API_NAME, LSB_Constants.CASE_NATURE_OF_ENQUIRY_API_NAME, true);
        Map<String, List<String>> natureOfEnquiryToTopicDependencies = LSB_Utils.getDependentOptions(
                'Case', LSB_Constants.CASE_NATURE_OF_ENQUIRY_API_NAME, LSB_Constants.CASE_TOPIC_API_NAME, true);

        Map<String, Schema.SObjectField> caseFieldMap = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap();
        Map<String, Schema.SObjectField> contactFieldMap = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();

        Set<String> topicsToExclude = new Set<String>();
        Set<String> allowedApplicantTypes = new Set<String>();
        Set<String> natureOfEnquiryToExclude = new Set<String>();

        // LEVEL 1 -> Contact Role
        String profileName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        String contactRole =
                profileName == LSB_Constants.OFFER_HOLDER_PROFILE_NAME ?
                        LSB_Constants.CONTACT_ROLE_APPLICANT :
                        (profileName == LSB_Constants.STUDENT_PROFILE_NAME ?
                                LSB_Constants.CONTACT_ROLE_STUDENT :
                                LSB_Constants.CONTACT_ROLE_PROSPECT);


        for (LSB_Configuration__mdt configRecord : [SELECT DeveloperName, LSB_Value__c FROM LSB_Configuration__mdt
                WHERE DeveloperName = :LSB_Constants.PRECHAT_TOPIC_TO_SKIP_METADATA_NAME OR
                    DeveloperName = :LSB_Constants.PRECHAT_NATURE_OF_ENQUIRY_TO_SKIP_METADATA_NAME
                    OR DeveloperName = :LSB_Constants.PRECHAT_ALLOWED_ROLES]) {
            if (configRecord.DeveloperName == LSB_Constants.PRECHAT_TOPIC_TO_SKIP_METADATA_NAME) {
                topicsToExclude = new Set<String>(configRecord.LSB_Value__c.split(';'));
            }

            if (configRecord.DeveloperName == LSB_Constants.PRECHAT_NATURE_OF_ENQUIRY_TO_SKIP_METADATA_NAME) {
                natureOfEnquiryToExclude = new Set<String>(configRecord.LSB_Value__c.split(';'));
            }

            if (configRecord.DeveloperName == LSB_Constants.PRECHAT_ALLOWED_ROLES) {
                allowedApplicantTypes = new Set<String>(configRecord.LSB_Value__c.split(';'));
            }
        }

        if (contactRole == LSB_Constants.CONTACT_ROLE_APPLICANT) {
            topicsToExclude.add(LSB_Constants.CASE_TOPIC_TEACHING_AND_TIMETABLE);
        }


        DescribeFieldResult caseContactRoleField = caseFieldMap.get(LSB_Constants.CASE_CONTACT_ROLE_API_NAME).getDescribe();
        DescribeFieldResult natureOfEnquiryRoleField = caseFieldMap.get(LSB_Constants.CASE_NATURE_OF_ENQUIRY_API_NAME).getDescribe();
        DescribeFieldResult topicRoleField = caseFieldMap.get(LSB_Constants.CASE_TOPIC_API_NAME).getDescribe();
        DescribeFieldResult subjectField = caseFieldMap.get(LSB_Constants.CASE_SUBJECT_API_NAME).getDescribe();
        DescribeFieldResult userRoleField = contactFieldMap.get(LSB_Constants.CONTACT_ROLE_API_NAME).getDescribe();

        LSB_PreChatConfiguration configuration = new LSB_PreChatConfiguration();
        configuration.contactRoleFieldLabel = caseContactRoleField.getLabel();
        configuration.natureOfEnquiryFieldLabel = natureOfEnquiryRoleField.getLabel();
        configuration.topicFieldLabel = topicRoleField.getLabel();
        configuration.subjectFieldLabel = subjectField.getLabel();
        configuration.picklistConfiguration = new LSB_PicklistWrapper();
        configuration.availableTopicsForGuest = new List<LSB_PicklistWrapper>();
        configuration.availableRolesWithLabels = new List<LSB_PicklistWrapper>();
        configuration.contactCaseRoleApiName = Schema.SObjectType.Case.fields.LSB_CAS_ContactRole__c.getName();
        configuration.tellUsAboutYourselfName = Schema.SObjectType.Case.fields.LSB_CAS_DeclaredRole__c.getName();
        configuration.tellUsAboutYourselfLabel = Schema.SObjectType.Case.fields.LSB_CAS_DeclaredRole__c.getLabel();
        configuration.emailLabel = LSB_Constants.CONTACT_EMAIL_LABEL;
        configuration.insideCommunity = contactRole != LSB_Constants.CONTACT_ROLE_PROSPECT;
        configuration.contactRoleValuesMap = new Map<String, String>();
        configuration.sObjectsLabelsMap = new Map<String, Set<String>>();

        configuration.contactRoleValuesMap.put(LSB_Constants.CONTACT_ROLE_PROSPECT, LSB_Constants.NATURE_OF_ENQUIRY_PROSPECT);
        configuration.contactRoleValuesMap.put(LSB_Constants.CONTACT_ROLE_APPLICANT, LSB_Constants.NATURE_OF_ENQUIRY_APPLICANT);
        configuration.contactRoleValuesMap.put(LSB_Constants.CONTACT_ROLE_STUDENT, LSB_Constants.NATURE_OF_ENQUIRY_STUDENT);
        configuration.studentContactRoleConstant = LSB_Constants.CONTACT_ROLE_STUDENT;

        configuration.sObjectsLabelsMap.put('EmailOnly', new Set<String>{LSB_Constants.CASE_DECLARED_ROLE_LABEL, LSB_Constants.CONTACT_EMAIL_LABEL});
        configuration.sObjectsLabelsMap.put('FullView', new Set<String>{LSB_Constants.CASE_DECLARED_ROLE_LABEL,
            LSB_Constants.CONTACT_EMAIL_LABEL, LSB_Constants.CONTACT_FIRST_NAME_LABEL, LSB_Constants.CONTACT_LAST_NAME_LABEL,
            LSB_Constants.CASE_SUBJECT_LABEL, LSB_Constants.CASE_TOPIC_LABEL});
        configuration.sObjectsLabelsMap.put('UserFound', new Set<String>{LSB_Constants.CASE_DECLARED_ROLE_LABEL,
            LSB_Constants.CONTACT_EMAIL_LABEL, LSB_Constants.CASE_SUBJECT_LABEL, LSB_Constants.CASE_TOPIC_LABEL});

        for (Schema.PicklistEntry picklistEntry : caseContactRoleField.getPicklistValues()) {
            if (picklistEntry.getValue().equals(contactRole)) {
                configuration.picklistConfiguration.label = picklistEntry.getLabel();
                configuration.picklistConfiguration.value = picklistEntry.getValue();
                configuration.picklistConfiguration.dependentValues = new List<LSB_PicklistWrapper>();
                break;
            }
        }

        // LEVEL 2 -> Nature of Enquiry
        Set<String> availableNatureOfEnquiryValues = new Set<String>(
                contactRoleToNatureOfEnquiry.get(configuration.picklistConfiguration.value));

        if (!configuration.insideCommunity) {
            availableNatureOfEnquiryValues = new Set<String>();
            for (String val : configuration.contactRoleValuesMap.values()) {
                availableNatureOfEnquiryValues.add(val);
            }
        }

        for (Schema.PicklistEntry picklistEntry : natureOfEnquiryRoleField.getPicklistValues()) {
            if (availableNatureOfEnquiryValues.contains(picklistEntry.getValue()) &&
                !natureOfEnquiryToExclude.contains(picklistEntry.getLabel())) {
                LSB_PicklistWrapper picklistValueWrapper = new LSB_PicklistWrapper();
                picklistValueWrapper.label = picklistEntry.getLabel();
                picklistValueWrapper.value = picklistEntry.getValue();
                picklistValueWrapper.dependentValues = new List<LSB_PicklistWrapper>();

                // LEVEL 3 -> Topics
                Set<String> availableTopicValues = new Set<String>(natureOfEnquiryToTopicDependencies.get(picklistValueWrapper.value));
                availableTopicValues.removeAll(topicsToExclude);
                for (Schema.PicklistEntry picklistEntry2 : topicRoleField.getPicklistValues()) {
                    if (availableTopicValues.contains(picklistEntry2.getValue())) {
                        LSB_PicklistWrapper picklistValueWrapper2 = new LSB_PicklistWrapper();
                        picklistValueWrapper2.label = picklistEntry2.getLabel();
                        picklistValueWrapper2.value = picklistEntry2.getValue();
                        picklistValueWrapper2.dependentValues = new List<LSB_PicklistWrapper>();
                        configuration.availableTopicsForGuest.add(picklistValueWrapper2);
                        picklistValueWrapper.dependentValues.add(picklistValueWrapper2);
                    }
                }
                configuration.picklistConfiguration.dependentValues.add(picklistValueWrapper);
            }
        }

        for (Schema.PicklistEntry picklistEntry : userRoleField.getPicklistValues()) {
            if (allowedApplicantTypes.contains(picklistEntry.getLabel())) {
                LSB_PicklistWrapper picklistValueWrapper = new LSB_PicklistWrapper();
                switch on picklistEntry.getLabel() {
                    when 'Applicant' {
                        picklistValueWrapper.label = Label.LSB_PreChatApplicant;
                    }
                    when 'Prospect' {
                        picklistValueWrapper.label = Label.LSB_PreChatProspect;
                    }
                    when 'Student' {
                        picklistValueWrapper.label = Label.LSB_PreChatStudent;
                    }
                }
                picklistValueWrapper.value = picklistEntry.getValue();
                configuration.availableRolesWithLabels.add(picklistValueWrapper);
            }
        }
        return configuration;
    }

    @auraEnabled
    public static Contact validateUser(String emailValue) {
        try {
            emailValue = emailValue.toLowerCase();
            Map<String, Contact> email2contact = LSB_ContactSelector.fetchContactsByEmails(new Set<String>{emailValue});
            emailValue = emailValue.replaceAll('@', '\\.');
            Contact contact = email2contact.get(emailValue);
            //Some existing Contacts might not have First Name
            //condition below is workaround for mandatory name requirement in form
            if (String.isEmpty(contact.FirstName)) {
                contact.FirstName = LSB_CaseHelper.retrieveDefaultContactNameFromMetadata();
            }
            return contact;
        } catch (Exception e) {
            return null;
        }
    }

    public class LSB_PreChatConfiguration {

        @auraEnabled
        public Boolean insideCommunity { get; set; }

        @auraEnabled
        public String contactRoleFieldLabel { get; set; }

        @auraEnabled
        public String natureOfEnquiryFieldLabel { get; set; }

        @auraEnabled
        public String topicFieldLabel { get; set; }

        @auraEnabled
        public String subjectFieldLabel { get; set; }

        @auraEnabled
        public String contactCaseRoleApiName { get; set; }

        @auraEnabled
        public String tellUsAboutYourselfLabel { get; set; }

        @auraEnabled
        public String tellUsAboutYourselfName { get; set; }

        @auraEnabled
        public String studentContactRoleConstant { get; set; }

        @auraEnabled
        public String emailLabel { get; set; }

        @auraEnabled
        public Map<String, String> contactRoleValuesMap { get; set; }

        @auraEnabled
        public Map<String, Set<String>> sObjectsLabelsMap { get; set; }

        @auraEnabled
        public LSB_PicklistWrapper picklistConfiguration { get; set; }

        @auraEnabled
        public List<LSB_PicklistWrapper> availableTopicsForGuest { get; set; }

        @auraEnabled
        public List<LSB_PicklistWrapper> availableRolesWithLabels { get; set; }
    }

    public class LSB_PicklistWrapper {

        @auraEnabled
        public String label { get; set; }

        @auraEnabled
        public String value { get; set; }

        @auraEnabled
        public List<LSB_PicklistWrapper> dependentValues { get; set; }
    }
}