@IsTest
public with sharing class LSB_PostChatPageControllerTest {

    private static final String TEST_FILE_NAME = 'testFileNameLsbu.txt';
    private static final String TEST_TRANSCRIPT_MESSAGE = 'This is test transcript message. Have a good day!';

    @TestSetup
    private static void setup() {
        Account householdAccount = new Account(Name = 'Test Household');
        insert householdAccount;

        Contact testContact = new Contact(AccountId = householdAccount.Id, LastName = 'Test', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = 'Test123');
        insert testContact;

        Id enquiryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('LSB_CAS_Enquiry').getRecordTypeId();

        Case testCase = new Case(Status = 'Received', ContactId = testContact.Id, RecordTypeId = enquiryRecordTypeId, LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT);
        insert testCase;

        LiveChatVisitor tesVisitor = new LiveChatVisitor();
        insert tesVisitor;

        LiveChatTranscript transcript = new LiveChatTranscript(CaseId = testCase.Id, LiveChatVisitorId = tesVisitor.Id, Body = '<p>Test</p>');
        insert transcript;
    }

    @IsTest
    private static void shouldReturnNullWhenNoParentI() {
        ContentVersion result = LSB_PostChatPageController.saveTranscriptFile('xyz', TEST_FILE_NAME, TEST_TRANSCRIPT_MESSAGE);

        System.assertEquals(null, result);
    }

    @IsTest
    private static void shouldReturnInsertedFile() {
        LiveChatTranscript chatTranscript = [SELECT ID, ChatKey FROM LiveChatTranscript];

        ContentVersion result = LSB_PostChatPageController.saveTranscriptFile(chatTranscript.ChatKey, TEST_FILE_NAME, TEST_TRANSCRIPT_MESSAGE);

        System.assertNotEquals(null, result);
        System.assertEquals(TEST_FILE_NAME, result.Title);
    }

    @IsTest
    private static void shouldReturnNullWhenTryingToGetSurveyLink() {
        LiveChatTranscript chatTranscript = [SELECT ID, ChatKey FROM LiveChatTranscript];

        String result = LSB_PostChatPageController.redirectToSurvey(chatTranscript.ChatKey);

        System.assertEquals(null, result);
    }
}