public without sharing class LSB_CaseTeamHelper {

    private static final String GENERIC_PROFILE_PIC = '/profilephoto/005/M';
    private static final String ERROR_MESSAGE_DELETE = 'Could not delete record: ';
    private static final String ROLE_NAME_TEAM = 'Team';
    private static final String ACCESS_LEVEL_EDIT = 'Edit';
    private static final String ACCESS_LEVEL_NONE = '-';

    public static Boolean addCaseTeamMember(Id caseId, Id memberId, String roleJSON) {
        TeamRoleWrapper roleWrapper = (TeamRoleWrapper)JSON.deserialize(roleJSON, TeamRoleWrapper.class);
        String roleName = roleWrapper.label;

        List<LSB_CRC_CaseTeamRoleConfiguration__c> roleConfigRecords = getRoleConfigRecordsByName(roleWrapper.name);
            if (!roleConfigRecords.isEmpty()) {
                String roleId = roleConfigRecords[0].LSB_CaseTeamRoleId__c;
                addTeamMember(caseId, memberId, roleId);
                return true;

            } else {
                String roleId = '';
                List<CaseTeamRole> roles = [SELECT Id FROM CaseTeamRole WHERE Name = :roleName LIMIT 1];
                if (!roles.isEmpty()) {
                    roleId = roles[0].Id;
                } else {
                    CaseTeamRole newRole = createNewConfigRole(roleName);
                    roleId = newRole.Id;
                }
                createRoleConfigRecord(roleWrapper.label, roleWrapper.name, roleId);
                insertCaseTeamMemberInFuture(caseId, memberId, roleId);
            }
            return false;
    }

    public static List<LSB_CRC_CaseTeamRoleConfiguration__c> getRoleConfigRecordsByName(String roleName) {
        return [SELECT Name, LSB_CaseTeamRoleId__c, LSB_FullLabel__c
        FROM LSB_CRC_CaseTeamRoleConfiguration__c
        WHERE Name =: roleName];
    }

    @future
    private static void createRoleConfigRecord(String roleLabel, String roleName, Id roleId) {
        LSB_CRC_CaseTeamRoleConfiguration__c roleConfig = new LSB_CRC_CaseTeamRoleConfiguration__c(
                Name = roleName,
                LSB_FullLabel__c = roleLabel,
                LSB_CaseTeamRoleId__c = roleId
        );
        insert roleConfig;
    }

    private static void addTeamMember(Id caseId, Id memberId, Id roleId) {
        List<CaseTeamMember> teamMembers = [SELECT Id, ParentId, MemberId, TeamRoleId FROM CaseTeamMember WHERE
        MemberId =: memberId AND ParentId =: caseId LIMIT 1
        ];
        if (!teamMembers.isEmpty()) {
            teamMembers[0].TeamRoleId = roleId;
            update teamMembers[0];
        } else {
            insert new CaseTeamMember(ParentId = caseId, MemberId = memberId, TeamRoleId = roleId);
        }

    }

    private static CaseTeamRole createNewConfigRole(String roleName) {
        CaseTeamRole newRole = new CaseTeamRole(Name = roleName, AccessLevel = ACCESS_LEVEL_EDIT, PreferencesVisibleInCSP = true);
        insert newRole;
        return newRole;
    }

    public static List<LSB_CaseTeamHelper.TeamMember> getTeamMembers(String caseId) {
        List<LSB_CaseTeamHelper.TeamMember> members = new List<LSB_CaseTeamHelper.TeamMember>();
        List<CaseTeamMember> teamMembers = getCaseTeamMemberRecords(caseId);
        List<CaseTeamTemplateRecord> teams = getCaseTeamTemplateRecords(caseId);
        Map<String, String> roleId2ConfigRecordName = getRoleId2ConfigRecordNames(teamMembers);
        Map<String, String> accessValue2AccessLabel = getAccessMap();
        for (CaseTeamMember caseTeamMember : teamMembers) {
            members.add(new TeamMember(false, caseTeamMember.Id, caseTeamMember.TeamRole.Name, accessValue2AccessLabel.get(caseTeamMember.TeamRole.AccessLevel),
                    caseTeamMember.TeamRole.PreferencesVisibleInCSP, (User)caseTeamMember.Member, roleId2ConfigRecordName.get(caseTeamMember.TeamRoleId)));
        }

        for (CaseTeamTemplateRecord templateRecord : teams) {
            members.add(new TeamMember(true, templateRecord.Id, ROLE_NAME_TEAM, ACCESS_LEVEL_NONE, true, templateRecord.TeamTemplateId, templateRecord.TeamTemplate.Name, GENERIC_PROFILE_PIC));
        }

        return members;
    }

    private static Map<String, String> getRoleId2ConfigRecordNames(List<CaseTeamMember> teamMembers) {
        Map<String, String> roleId2ConfigRecordName = new Map<String, String>();
        Set<Id> roleIds = new Set<Id>();

        for (CaseTeamMember member : teamMembers) {
            roleIds.add(member.TeamRoleId);
        }
        List<LSB_CRC_CaseTeamRoleConfiguration__c> configRecords = [
                SELECT LSB_CaseTeamRoleId__c, Name, LSB_FullLabel__c
                FROM LSB_CRC_CaseTeamRoleConfiguration__c
                WHERE LSB_CaseTeamRoleId__c IN :roleIds
        ];


        for (LSB_CRC_CaseTeamRoleConfiguration__c config : configRecords) {
            roleId2ConfigRecordName.put(config.LSB_CaseTeamRoleId__c, config.Name);
        }
        return roleId2ConfigRecordName;
    }

    private static Map<String, String> getAccessMap() {
        Map<String, String> accessValue2AccessLabel = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = CaseTeamRole.AccessLevel.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for ( Schema.PicklistEntry pickListVal : picklistEntries) {
            accessValue2AccessLabel.put(pickListVal.value, pickListVal.label);
        }
        return accessValue2AccessLabel;
    }

    private static List<CaseTeamMember> getCaseTeamMemberRecords(String caseId) {
        return [SELECT Id, TeamRoleId, TeamRole.Name, TeamRole.AccessLevel, TeamRole.PreferencesVisibleInCSP,
            TYPEOF Member
            WHEN User THEN
                Id,
                Name,
                Email,
                MediumPhotoUrl,
                LSB_USR_Linkedin__c,
                AboutMe
            END
        FROM CaseTeamMember
        WHERE ParentId =: caseId
        AND TeamTemplateMemberId = null];
    }

    private static List<CaseTeamTemplateRecord> getCaseTeamTemplateRecords(String caseId) {
        return [SELECT Id, ParentId, TeamTemplate.Name, TeamTemplateId
                FROM CaseTeamTemplateRecord
                WHERE ParentId =: caseId];
    }


    public static void deleteTeamMember(String memberId) {
        CaseTeamMember caseTeamMember = [SELECT Id FROM CaseTeamMember WHERE Id =: memberId LIMIT 1];
        try {
            delete caseTeamMember;
        } catch (Exception e) {
            throw new AuraHandledException(ERROR_MESSAGE_DELETE + e.getMessage());
        }
    }

    public static void deleteCaseTeamTemplateRecord(String teamId) {
        CaseTeamTemplateRecord teamTemplate = [SELECT Id FROM CaseTeamTemplateRecord WHERE Id =: teamId LIMIT 1];
        try {
            delete teamTemplate;
        } catch (Exception e) {
            throw new AuraHandledException(ERROR_MESSAGE_DELETE + e.getMessage());
        }
    }

    public static List<TeamRoleWrapper> getBasicCaseTeamRoles() {
        List<LSB_BasicCaseTeamRoles__mdt> basicRoles = [SELECT DeveloperName, Label, LSB_SortOrder__c
        FROM LSB_BasicCaseTeamRoles__mdt
        ORDER BY LSB_SortOrder__c ASC];

        List<TeamRoleWrapper> roleWrappers = new List<LSB_CaseTeamHelper.TeamRoleWrapper>();
        for (LSB_BasicCaseTeamRoles__mdt basicRole : basicRoles) {
            roleWrappers.add(new TeamRoleWrapper(basicRole.Label, basicRole.DeveloperName, basicRole.LSB_SortOrder__c));
        }

        return roleWrappers;
    }

    public static List<CaseTeamTemplate> getCaseTeams() {
        return [SELECT Id, Name, Description FROM CaseTeamTemplate];
    }

    public static String createCaseTeam(Id caseId, Id teamId) {
        try {
            insert new CaseTeamTemplateRecord(ParentId = caseId, TeamTemplateId = teamId);
            return '';
        } catch(Exception ex) {
            return ex.getMessage();
        }
    }

    @future
    private static void insertCaseTeamMemberInFuture(Id caseId, Id memberId, Id roleId) {
        addTeamMember(caseId, memberId, roleId);
    }

    public class TeamRoleWrapper {
        @auraEnabled public String label { get; set; }
        @auraEnabled public String name { get; set; }
        @auraEnabled public Decimal order { get; set; }

        public TeamRoleWrapper(String label, String name, Decimal sortOrder) {
            this.label = label;
            this.name = name;
            this.order = sortOrder;
        }
    }

    public class TeamMember {
        @auraEnabled public Boolean isTeam { get; set; }
        @auraEnabled public String id { get; set; }
        @auraEnabled public String roleName { get; set; }
        @auraEnabled public String roleUniqueName { get; set; }
        @auraEnabled public String accessLevel { get; set; }
        @auraEnabled public Boolean visibleInCSP { get; set; }
        @auraEnabled public String memberName { get; set; }
        @auraEnabled public String memberId { get; set; }
        @auraEnabled public String memberPhotoUrl { get; set; }

        public TeamMember(Boolean isTeam, String id, String roleName, String accessLevel, Boolean visibleInCSP, String templateId, String memberName, String memberPhotoUrl) {
            this.isTeam = isTeam;
            this.id = id;
            this.roleName = roleName;
            this.accessLevel = accessLevel;
            this.visibleInCSP = visibleInCSP;
            this.memberId = templateId;
            this.memberName = memberName;
            this.memberPhotoUrl = memberPhotoUrl;
        }

        public TeamMember(Boolean isTeam, String id, String roleName, String accessLevel, Boolean visibleInCSP, User memberUser, String roleUniqueName) {
            this.isTeam = isTeam;
            this.id = id;
            this.roleName = roleName;
            this.accessLevel = accessLevel;
            this.visibleInCSP = visibleInCSP;
            this.memberId = memberUser.Id;
            this.memberName = memberUser.Name;
            this.memberPhotoUrl = memberUser.MediumPhotoUrl;
            this.roleUniqueName = roleUniqueName;
        }
    }
}