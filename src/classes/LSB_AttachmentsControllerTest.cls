@IsTest
class LSB_AttachmentsControllerTest {

    @TestSetup
    static void testData() {
        Task task = new Task(Subject = 'Test task');
        insert task;

        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );

        insert contentVersionInsert;

        ContentDocument contentDocument = [SELECT Id FROM ContentDocument LIMIT 1];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = task.Id;
        contentDocumentLink.ContentDocumentId = contentDocument.Id;
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;
    }

    @IsTest
    static void getContentVersionsTest() {
        Task task = [SELECT Id FROM Task LIMIT 1];
        Test.startTest();
        List<ContentVersion> contentVersions = LSB_AttachmentsController.getContentVersions(task.Id);
        Test.stopTest();
        System.assertEquals(1, contentVersions.size());
    }

    @IsTest
    static void deleteAttachmentTest() {
        Task task = [SELECT Id FROM Task LIMIT 1];
        ContentDocument contentDocument = [SELECT Id FROM ContentDocument LIMIT 1];
        Test.startTest();
        LSB_AttachmentsController.deleteAttachment(contentDocument.Id);
        List<ContentVersion> contentVersions = LSB_AttachmentsController.getContentVersions(task.Id);
        Test.stopTest();
        System.assertEquals(0, contentVersions.size());
    }

}