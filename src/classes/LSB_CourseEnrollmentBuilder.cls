public without sharing class LSB_CourseEnrollmentBuilder {

    private String recordTypeId;
    private String componentType;
    private String moduleEnrollmentStatus;
    private String programEnrollment;
    private String parentModuleComponent;
    private String courseOffering;
    private Date handInDate;
    private Boolean isResit;

    public LSB_CourseEnrollmentBuilder() {
        this.isResit = false;
        this.moduleEnrollmentStatus = LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED;
    }

    public LSB_CourseEnrollmentBuilder withRecordType(String recordTypeDeveloperName) {
        this.recordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();
        return this;
    }

    public LSB_CourseEnrollmentBuilder withComponentType(String componentType) {
        this.componentType = componentType;
        return this;
    }

    public LSB_CourseEnrollmentBuilder withHandInDate(Date handInDate) {
        this.handInDate = handInDate;
        return this;
    }

    public LSB_CourseEnrollmentBuilder withIsResit(Boolean isResit) {
        this.isResit = isResit;
        return this;
    }

    public LSB_CourseEnrollmentBuilder withModuleEnrollmentStatus(String moduleEnrollmentStatus) {
        this.moduleEnrollmentStatus = moduleEnrollmentStatus;
        return this;
    }

    public LSB_CourseEnrollmentBuilder withProgramEnrollment(String programEnrollmentId) {
        this.programEnrollment = programEnrollmentId;
        return this;
    }

    public LSB_CourseEnrollmentBuilder withParentModuleComponent(String parentModuleComponentId) {
        this.parentModuleComponent = parentModuleComponentId;
        return this;
    }

    public LSB_CourseEnrollmentBuilder withCourseOffering(String courseOffering) {
        this.courseOffering = courseOffering;
        return this;
    }

    public hed__Course_Enrollment__c create() {
        return new hed__Course_Enrollment__c(
                RecordTypeId = this.recordTypeId,
                LSB_CCN_HandInDate__c = this.handInDate,
                LSB_CCN_Resit__c = this.isResit,
                LSB_CCN_ModuleEnrolmentStatus__c = this.moduleEnrollmentStatus,
                hed__Program_Enrollment__c = this.programEnrollment,
                LSB_CCN_ParentModuleComponent__c = this.parentModuleComponent,
                hed__Course_Offering__c = this.courseOffering,
                LSB_CCN_ComponentType__c = this.componentType
        );
    }

    public hed__Course_Enrollment__c createAndSave() {
        hed__Course_Enrollment__c newCourseEnrollment = this.create();
        insert newCourseEnrollment;
        return newCourseEnrollment;
    }
}