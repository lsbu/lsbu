public without sharing class LSB_ChatterFeedsController {

    /**
     * @description get feed items for parent record
     *
     * @param parentRecordId
     *
     * @return
     */
    @AuraEnabled
    public static List<FeedItem> fetchChatterFeeds(Id parentRecordId) {
        return [
            SELECT Id,
                CreatedDate,
                TYPEOF InsertedBy
                    WHEN User THEN
                    FirstName,
                    LastName,
                    SmallPhotoUrl
                END,
                Body, (
                SELECT Id,
                    CreatedDate,
                    InsertedBy.FirstName,
                    InsertedBy.LastName,
                    InsertedBy.SmallPhotoUrl,
                    CommentBody
                FROM FeedComments
            )
            FROM FeedItem
            WHERE ParentId = :parentRecordId
                AND Type = 'TextPost'
                AND Visibility = 'AllUsers'
        ];
    }

    /**
     * @description insert feed post
     *
     * @param feedPost
     */
    @AuraEnabled
    public static void createFeedPost(FeedComment feedPost) {
        insert feedPost;
    }

    /**
     * @ create Feed Item on parent record
     *
     * @param parentId
     * @param feedMessage
     */
    @AuraEnabled
    public static void createFeedItem(Id parentId, String feedMessage) {
        FeedItem feedItem = new FeedItem(Body = feedMessage,
            ParentId = parentId,
            Type = 'TextPost',
            Visibility = 'AllUsers');
        insert feedItem;
    }

}