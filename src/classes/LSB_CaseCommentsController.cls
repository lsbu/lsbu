public with sharing class LSB_CaseCommentsController {

    private static final String ACTIVITY_TYPE_EMAIL = 'Email';
    private static final String ACTIVITY_STATUS_COMPLETED = 'Completed';

    /**
     * @description fetch Case Comments
     *
     * @param parentRecordId - parent Case Id
     *
     * @return list of Case MessageWrapper
     */

    @AuraEnabled
    public static List<LSB_CaseActivitiesHelper.MessageWrapper> getCaseMessagesByParentId(Id parentRecordId) {
        List<LSB_CaseActivitiesHelper.MessageWrapper> messages = LSB_CaseActivitiesHelper.getCaseMessagesByParentId(parentRecordId);
        return messages;
    }

    /**
     * @description create Case Comment
     * @param parentId parent Case Id
     * @param caseComment comment content
     */
    @AuraEnabled
    public static void createCaseComment(Id parentId, String caseComment) {
        CaseComment newCaseComment = new CaseComment(
            CommentBody = caseComment,
            ParentId = parentId,
            IsPublished = true
        );
        insert newCaseComment;
    }
}