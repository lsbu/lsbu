public with sharing class LSB_AttachmentsController {

    /**
     * @description get content versions
     *
     * @param parentRecordId - parent record Id
     *
     * @return list of content versions
     */
    @AuraEnabled
    public static List<ContentVersion> getContentVersions(Id parentRecordId) {
        return LSB_AttachmentsHelper.getContentVersions(parentRecordId);
    }

    /**
     * @description delete attachments
     *
     * @param documentId - id of attachment
     */
    @AuraEnabled
    public static void deleteAttachment(Id documentId) {
        LSB_AttachmentsHelper.deleteAttachment(documentId);
    }
}