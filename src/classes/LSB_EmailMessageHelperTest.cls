@IsTest
class LSB_EmailMessageHelperTest {

    private static final String JOHN_SNOW2_GMAIL_COM = 'john.snow2@gmail.com';
    private static final String TEST_JOHN_SNOW = 'Test John Snow';

    @TestSetup
    static void testData() {
        insert new hed__Hierarchy_Settings__c(
            hed__Admin_Account_Naming_Format__c = '{!LastName} Administrative Account'
        );

        insert new hed__Trigger_Handler__c(
            hed__Active__c = true,
            hed__Class__c = 'LSB_TriggerDispatcher',
            hed__Load_Order__c = 1,
            hed__Trigger_Action__c = 'AfterInsert',
            hed__Object__c = 'EmailMessage'
        );

        RecordType administrativeRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Administrative' AND SobjectType = 'Account' LIMIT 1];

        List<Account> accounts = new List<Account>{
            new Account(
                Name = 'Unknown Administrative Account',
                RecordTypeId = administrativeRecordType.Id
            )
        };

        insert accounts;

        List<Contact> contacts = new List<Contact>{
            new Contact(
                FirstName = '',
                LastName = 'Unknown',
                hed__AlternateEmail__c = JOHN_SNOW2_GMAIL_COM,
                Email = JOHN_SNOW2_GMAIL_COM,
                Birthdate = Date.newInstance(1987, 1, 1),
                MobilePhone = '+48 123 123 123',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWER',
                LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT,
                AccountId = accounts[0].Id
            )
        };

        insert contacts;

        Id recordTypeId = LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME);

        List<Case> casesToInsert = new List<Case>{
            new Case(
                Subject = 'Test Case',
                Origin = LSB_Constants.CASE_ORIGIN_EMAIL,
                LSB_CAS_FirstName__c = 'John',
                LSB_CAS_LastName__c = 'Snow',
                LSB_CAS_MobilePhone__c = '+48 123 123 123',
                SuppliedEmail = JOHN_SNOW2_GMAIL_COM,
                RecordTypeId = recordTypeId,
                ContactId = contacts[0].Id,
                LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT,
                AccountId = accounts[0].Id
            )
        };

        insert casesToInsert;
    }

    @IsTest
    static void updateCaseContactLastNameTest() {
        Case caseRecord = [SELECT Id FROM Case WHERE SuppliedEmail = :JOHN_SNOW2_GMAIL_COM LIMIT 1];
        Test.startTest();
        insert new EmailMessage(
            FromAddress = JOHN_SNOW2_GMAIL_COM,
            FromName = TEST_JOHN_SNOW,
            Incoming = true,
            ParentId = caseRecord.Id
        );
        Test.stopTest();
        Contact testContact = [SELECT Id, LastName FROM Contact LIMIT 1];
        System.assertEquals(TEST_JOHN_SNOW, testContact.LastName);
    }

}