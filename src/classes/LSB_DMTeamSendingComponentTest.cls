/**
 * Created by kgromek002 on 24.08.2021.
 */

@IsTest
class LSB_DMTeamSendingComponentTest {

    @IsTest
    static void insertTestData(){

        LSB_DMTeams__c team = new LSB_DMTeams__c(
                Name = 'Test Team',
                LSB_DM_Email__c = 'teamtest@teamtest.com'
        );

        insert team;

        Test.startTest();
        List<LSB_DMTeams__c> groupMembers = LSB_DMTeamSendingComponentController.getGroupMembers();
        Test.stopTest();

        system.assert(!groupMembers.isEmpty());

    }


}