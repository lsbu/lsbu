/**
 * @description Controller class for lsb_MeetFrontDeskForm and lsb_CommunityFrontDeskSelfCheckIn lightning web components.
 */
public without sharing class LSB_MeetFrontDeskFormController {

    /**
     * @description Method is using to fetch student data based on given student id. It is using to validate student id.
     * @param studentId Id of the student.
     * @return Student data.
     */
    @auraEnabled(Cacheable=true)
    public static Contact fetchStudentData(String studentId) {
        return [SELECT Id, FirstName, LastName, hed__UniversityEmail__c FROM Contact WHERE LSB_ExternalID__c = :studentId];
    }

    /**
     * @description Method is using to retrieve number of cases in queue.
     * @return Number of cases in queue.
     */
    @auraEnabled
    public static Integer fetchTotalInQueue() {
        return [
                SELECT count()
                FROM Case
                WHERE
                RecordType.DeveloperName = :LSB_Constants.CASE_ADVISING_QUEUE_RECORD_TYPE_DEVELOPER_NAME AND
                Status = :LSB_Constants.CASE_STATUS_QUEUED];
    }

    /**
     * @description Method is using to create the Advising Queue Case.
     * @param contactId Id of the Contact record.
     * @param firstName value for the Contact First Name field.
     * @param lastName value for the Contact Last Name field.
     * @param contactEmail value for the Web Email field which is using for email notifications.
     * @return true if no error occurred, otherwise false.
     */
    @auraEnabled
    public static Boolean createAdvisingQueueCase(Id contactId, String firstName, String lastName, String contactEmail) {
        Boolean hasError = false;

        try {
            RecordType advisingQueueRT = [
                    SELECT Id
                    FROM RecordType
                    WHERE DeveloperName = :LSB_Constants.CASE_ADVISING_QUEUE_RECORD_TYPE_DEVELOPER_NAME];

            sfal__Topic__c advisingQueueTopic = [
                    SELECT Id
                    FROM sfal__Topic__c
                    WHERE Name = :LSB_Constants.TOPIC_SLC_HELP_DESC_NAME];

            Contact student = [SELECT FirstName, LastName, LSB_ExternalID__c FROM Contact WHERE Id = :contactId];

            firstName = String.isBlank(firstName) ? student.FirstName : firstName;
            lastName = String.isBlank(lastName) ? student.LastName : lastName;

            Case advisingQueueCase = new Case(
                    LSB_CAS_FirstName__c = firstName,
                    LSB_CAS_LastName__c = lastName,
                    sfal__StudentID__c = student.LSB_ExternalID__c,
                    SuppliedEmail = contactEmail,
                    ContactId = contactId,
                    RecordTypeId = advisingQueueRT.Id,
                    sfal__Topic__c = advisingQueueTopic.Id,
                    Origin = LSB_Constants.CASE_ORIGIN_SLC_HELP_DESK,
                    Subject = String.format(
                            '{0} - {1} {2} - {3}',
                            new List<String>{
                                    LSB_Constants.CASE_ORIGIN_SLC_HELP_DESK,
                                    firstName,
                                    lastName,
                                    String.valueOf(Date.today())
                            }
                    )
            );

            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.useDefaultRule = true;
            advisingQueueCase.setOptions(dmlOpts);

            insert advisingQueueCase;
        } catch(Exception ex) {
            System.debug('Cannot insert advising queue case for contact with Id: ' +
                    contactId + ' and name: ' + firstName + ' ' + lastName +
                    '\nMessage: ' + ex.getMessage() +
                    '\nStack Trace: ' + ex.getStackTraceString());
            hasError = true;
        }

        return hasError;
    }

}