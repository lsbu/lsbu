@isTest
private class LSB_ContactHelperTest {
    private static final String COMMUNITY_USER_EMAIL = 'john.snowman12345@pwc.test.com';

    @testSetup
    static void createTestData() {
        insert new List<hed__Trigger_Handler__c>{
                TestDataClass.createTriggerHandlerRecord('Contact'),
                TestDataClass.createTriggerHandlerRecord('ContactPointConsent')
        };

        TestDataClass.insertDataUsePurpose();

        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact student = new Contact(
                FirstName = 'John',
                LastName = 'Snowman',
                hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                hed__Preferred_Email__c = LSB_Constants.CONTACT_PREFERRED_EMAIL_PERSONAL,
                Email = COMMUNITY_USER_EMAIL,
                LSB_CON_SourceSystem__c = 'QL',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY',
                AccountId = a.Id);

        insert student;

        createConsents();
    }

    @isTest
    private static void testInsertContact() {
        Account a = new Account(Name='Test Account Name 2');
        insert a;

        Test.startTest();

        Contact student = new Contact(
                FirstName = 'Tom',
                LastName = 'Snowman',
                hed__AlternateEmail__c = 'tom.snowman@tommys.com',
                hed__PreferredPhone__c = LSB_Constants.CONTACT_PREFERRED_HOME_PHONE,
                hed__Preferred_Email__c = LSB_Constants.CONTACT_PREFERRED_EMAIL_UNIVERSITY,
                Email = 'tom.snowman@tommys.com',
                hed__UniversityEmail__c = 'tom.snowman@university.com',
                MobilePhone = '1111111',
                HomePhone = '222222',
                LSB_CON_SourceSystem__c = 'QL',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1235QWERTY',
                AccountId = a.Id);

        insert student;

        Test.stopTest();

        student = [
            SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName
            FROM Contact
            WHERE Id = :student.Id
        ];

        System.assert(student.IndividualId != null);
        System.assertEquals(student.FirstName, student.Individual.FirstName);
        System.assertEquals(student.LastName, student.Individual.LastName);

        List<ContactPointPhone> contactPointPhones = [
                SELECT Id, LSB_ExternalId__c, ParentId, ActiveFromDate
                FROM ContactPointPhone WHERE LSB_COP_Contact__c = :student.Id];

        System.assertEquals(2, contactPointPhones.size());

        for (ContactPointPhone contactPoint : contactPointPhones) {
            System.assertEquals(student.IndividualId, contactPoint.ParentId);
            System.assert(contactPoint.LSB_ExternalId__c != null);
            System.assertEquals(System.today(), contactPoint.ActiveFromDate);
            System.assert(contactPoint.LSB_ExternalId__c.startsWith(student.Id + '#'));
        }

        List<ContactPointEmail> contactPointEmails = [
                SELECT Id, LSB_ExternalId__c, ParentId, ActiveFromDate
                FROM ContactPointEmail WHERE LSB_COE_Contact__c = :student.Id];

        System.assertEquals(2, contactPointEmails.size());

        for (ContactPointEmail contactPoint : contactPointEmails) {
            System.assertEquals(student.IndividualId, contactPoint.ParentId);
            System.assert(contactPoint.LSB_ExternalId__c != null);
            System.assertEquals(System.today(), contactPoint.ActiveFromDate);
            System.assert(contactPoint.LSB_ExternalId__c.startsWith(student.Id + '#'));
        }

        List<ContactPointConsent> relatedConsents = [
                SELECT Id, LSB_ExternalId__c, ContactPointId, EffectiveFrom, PrivacyConsentStatus
                FROM ContactPointConsent WHERE LSB_COC_Contact__c = :student.Id];

        System.assertEquals(10, relatedConsents.size());

        for (ContactPointConsent consent : relatedConsents) {
            System.assert(consent.ContactPointId != null);
            System.assert(consent.LSB_ExternalId__c != null);
            System.assert(consent.LSB_ExternalId__c.startsWith(student.Id + '#'));
        }
    }

    @isTest
    private static void testUpdateContactData() {
        Contact student = [
                SELECT Id, FirstName, LastName, hed__AlternateEmail__c, hed__UniversityEmail__c, hed__Preferred_Email__c,
                        hed__PreferredPhone__c, MobilePhone, HomePhone,
                        (SELECT Id FROM Contact_Point_Consents__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];


        System.assert(student.hed__AlternateEmail__c != null);
        System.assert(student.hed__UniversityEmail__c == null);
        System.assert(student.MobilePhone == null);
        System.assert(student.HomePhone == null);
        System.assertEquals(2, student.Contact_Point_Consents__r.size());

        student.hed__AlternateEmail__c = '';
        student.hed__UniversityEmail__c = 'j.snowman@testlsbu.com';
        student.hed__Preferred_Email__c = LSB_Constants.CONTACT_PREFERRED_EMAIL_UNIVERSITY;
        student.MobilePhone = '123123';
        student.HomePhone = '2222222';
        student.hed__PreferredPhone__c = LSB_Constants.CONTACT_PREFERRED_HOME_PHONE;

        Test.startTest();

        update student;

        Test.stopTest();

        student = [
                SELECT Id, FirstName, LastName, hed__AlternateEmail__c, hed__UniversityEmail__c, hed__Preferred_Email__c,
                        hed__PreferredPhone__c, MobilePhone, HomePhone,
                    (SELECT Id FROM Contact_Point_Consents__r)
                FROM Contact
                WHERE Id = :student.Id
        ];


        System.assert(student.hed__AlternateEmail__c == null);
        System.assert(student.hed__UniversityEmail__c != null);
        System.assert(student.MobilePhone != null);
        System.assert(student.HomePhone != null);
        System.assertEquals(8, student.Contact_Point_Consents__r.size());
    }

    @isTest
    private static void testUpdatePrimaryData() {
        Contact student = [
                SELECT Id, FirstName, LastName, hed__AlternateEmail__c, hed__UniversityEmail__c, hed__Preferred_Email__c,
                        hed__PreferredPhone__c, MobilePhone, HomePhone,
                    (SELECT Id, IsPrimary, EmailAddress FROM Contact_Point_Emails__r),
                    (SELECT Id, IsPrimary, TelephoneNumber FROM Contact_Point_Phones__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        System.assertEquals(COMMUNITY_USER_EMAIL, student.hed__AlternateEmail__c);
        System.assert(student.hed__UniversityEmail__c == null);
        System.assert(student.MobilePhone == null);
        System.assert(student.HomePhone == null);
        System.assertEquals(1, student.Contact_Point_Emails__r.size());
        System.assertEquals(true, student.Contact_Point_Emails__r.get(0).IsPrimary);
        System.assertEquals(COMMUNITY_USER_EMAIL, student.Contact_Point_Emails__r.get(0).EmailAddress);

        student.hed__UniversityEmail__c = 'j.snowman@testlsbu.com';
        student.hed__Preferred_Email__c = LSB_Constants.CONTACT_PREFERRED_EMAIL_UNIVERSITY;
        student.MobilePhone = '123123';
        student.HomePhone = '2222222';
        student.hed__PreferredPhone__c = LSB_Constants.CONTACT_PREFERRED_HOME_PHONE;

        Test.startTest();

        update student;

        Test.stopTest();

        student = [
                SELECT Id, FirstName, LastName, hed__AlternateEmail__c, hed__UniversityEmail__c, hed__Preferred_Email__c,
                        hed__PreferredPhone__c, MobilePhone, HomePhone,
                (SELECT Id, IsPrimary, EmailAddress FROM Contact_Point_Emails__r),
                (SELECT Id, IsPrimary, TelephoneNumber FROM Contact_Point_Phones__r)
                FROM Contact
                WHERE Id = :student.Id
        ];

        System.assertEquals(2, student.Contact_Point_Emails__r.size());
        System.assertEquals(2, student.Contact_Point_Phones__r.size());

        for (ContactPointEmail email : student.Contact_Point_Emails__r) {
            System.assertEquals(email.EmailAddress == student.hed__UniversityEmail__c, email.IsPrimary);
        }

        for (ContactPointPhone phone : student.Contact_Point_Phones__r) {
            System.assertEquals(phone.TelephoneNumber == student.HomePhone, phone.IsPrimary);
        }
    }

    @isTest
    private static void testStopNotification() {
        Contact student = [
                SELECT Id, LSB_CON_StopEmailNotifications__c,
                    (SELECT Id, PrivacyConsentStatus FROM Contact_Point_Consents__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        System.assert(!student.LSB_CON_StopEmailNotifications__c);

        for (ContactPointConsent consent : student.Contact_Point_Consents__r) {
            System.assert(LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN == consent.PrivacyConsentStatus ||
                    LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_NOTSEEN == consent.PrivacyConsentStatus);
        }

        student.LSB_CON_StopEmailNotifications__c = true;

        Test.startTest();

        update student;

        Test.stopTest();

        student = [
                SELECT Id, LSB_CON_StopEmailNotifications__c,
                (SELECT Id, PrivacyConsentStatus FROM Contact_Point_Consents__r)
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        System.assert(student.LSB_CON_StopEmailNotifications__c);

        for (ContactPointConsent consent : student.Contact_Point_Consents__r) {
            System.assertEquals(LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT, consent.PrivacyConsentStatus);
        }
    }

    @isTest
    private static void testDeleteContact() {
        Contact student = [
                SELECT Id, FirstName, LastName, IndividualId, Individual.FirstName, Individual.LastName
                FROM Contact
                WHERE Email = :COMMUNITY_USER_EMAIL
        ];

        Id individualId = student.IndividualId;

        Test.startTest();

        delete student;

        Test.stopTest();

        System.assert([SELECT count() FROM Individual WHERE Id = :individualId] == 0);
    }

    @IsTest
    private static void shouldChangeEffectiveToWhenStatusChangedToOptOut() {
        ContactPointConsent contactPointConsent = [SELECT Id, EffectiveFrom, EffectiveTo FROM ContactPointConsent][0];
        Datetime effectiveFromPreviousValue = contactPointConsent.EffectiveFrom;

        Test.startTest();
        Datetime updateTime = Datetime.now();
        contactPointConsent.PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT;
        update contactPointConsent;
        Test.stopTest();

        ContactPointConsent result = [SELECT Id, EffectiveFrom, EffectiveTo FROM ContactPointConsent][0];
        System.assertEquals(effectiveFromPreviousValue, result.EffectiveFrom);
        System.assertEquals(updateTime.minute(), result.EffectiveTo.minute());
    }

    @IsTest
    private static void shouldChangeEffectiveFromWhenStatusChangedToOptIn() {
        ContactPointConsent contactPointConsent = [SELECT Id, EffectiveFrom, EffectiveTo FROM ContactPointConsent][0];
        contactPointConsent.PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT;
        update contactPointConsent;

        Test.startTest();
        Datetime updateTime = Datetime.now();
        contactPointConsent.PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN;
        update contactPointConsent;
        Test.stopTest();

        ContactPointConsent result = [SELECT Id, EffectiveFrom, EffectiveTo FROM ContactPointConsent][0];
        System.assertEquals(updateTime.minute(), result.EffectiveFrom.minute());
        System.assertEquals(null, result.EffectiveTo);
    }

    private static void createConsents() {
        ContactPointEmail contactPointEmail = new ContactPointEmail(EmailAddress = 'tes38bhns5wws5827@h734hf7.com');
        insert contactPointEmail;
        ContactPointConsent contactPointConsent = new ContactPointConsent(Name = 'testConsent', ContactPointId = contactPointEmail.Id);
        insert contactPointConsent;
    }
}