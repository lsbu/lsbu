/**
 * Created by ociszek001 on 04.03.2021.
 */

public with sharing class LSB_RecordFieldDetailsController {
    @AuraEnabled(cacheable=true)
    public static String fetchRecord(String objectApiName, String contactFieldApiName) {
        List<User> contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];

        String queryStr = 'Select Id FROM ' + String.escapeSingleQuotes(objectApiName) + ' WHERE ' + String.escapeSingleQuotes(contactFieldApiName) + '=\'' + contactId.get(0).ContactId + '\' ';
        List<sObject> spList = Database.query(queryStr) ;

        if (!spList.isEmpty()) {
            return spList.get(0).Id;
        }
        return '';
    }
}