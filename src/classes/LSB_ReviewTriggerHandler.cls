public with sharing class LSB_ReviewTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;
    private LSB_ReviewHelper reviewHelper;

    public LSB_ReviewTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
        this.reviewHelper = new LSB_ReviewHelper();
    }

    public void beforeInsert(List<SObject> newList) {}

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
        reviewHelper.processNewReviews((List<LSB_REW_Review__c>) newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        reviewHelper.processUpdatedReviews((Map<Id, LSB_REW_Review__c>) newItems, (Map<Id, LSB_REW_Review__c>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> newItems) {}
}