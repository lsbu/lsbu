public without sharing class LSB_EmailMessageHelper {

    /**
     * @description update Last Name of Contact associated with parent Case
     * @param emailMessages list of EmailMessages
     */
    public static void updateCaseContactLastName(List<EmailMessage> emailMessages) {
        Map<Id, Case> parentCasesMap = new Map<Id, Case>();
        List<SObject> contactsAndAccounts = new List<SObject>();
        for (EmailMessage emailMessage : emailMessages) {
            if (emailMessage.Incoming
                && emailMessage.ParentId.getSobjectType() == Case.getSObjectType()) {
                parentCasesMap.put(emailMessage.ParentId, null);
            }
        }
        if (parentCasesMap.isEmpty()) {
            return;
        }

        hed__Hierarchy_Settings__c hierarchySettings = hed__Hierarchy_Settings__c.getOrgDefaults();

        parentCasesMap = new Map<Id, Case>([
            SELECT Id,
                LSB_CAS_LastName__c,
                Contact.LastName,
                Account.Name
            FROM Case
            WHERE Id = :parentCasesMap.keySet()
                AND Origin = :LSB_Constants.CASE_ORIGIN_EMAIL
                AND ContactId != NULL
        ]);
        for (EmailMessage emailMessage : emailMessages) {
            if (emailMessage.Incoming
                && !String.isBlank(emailMessage.FromName)
                && emailMessage.ParentId.getSobjectType() == Case.getSObjectType()
                && parentCasesMap.containsKey(emailMessage.ParentId)
                ) {
                Case parentCase = parentCasesMap.get(emailMessage.ParentId);
                parentCase.LSB_CAS_LastName__c = emailMessage.FromName;
                parentCase.Contact.LastName = emailMessage.FromName;
                if (parentCase.AccountId != null
                    && hierarchySettings != null
                    && String.isNotBlank(hierarchySettings.hed__Admin_Account_Naming_Format__c)) {
                    parentCase.Account.Name = hierarchySettings.hed__Admin_Account_Naming_Format__c.replace('{!LastName}', parentCase.Contact.LastName);
                    contactsAndAccounts.add(parentCase.Account);
                }
                contactsAndAccounts.add(parentCase.Contact);
            }
        }
        update parentCasesMap.values();
        update contactsAndAccounts;
    }
}