/**
 * @descritpion Helper class contains methods that operate on User records.
 *
 * @date 04.09.2020.
 */
public without sharing class LSB_UserHelper {

    public static final String LOGIN_STATUS_INVALID_PASSWORD = LSB_Constants.LOGIN_STATUS_INVALID_PASSWORD;
    public static final String LOGIN_STATUS_SUCCESS = LSB_Constants.LOGIN_STATUS_SUCCESS;

    /**
     * @description Method using to reset user password.
     *
     * @param userId Id of user to reset password.
     *
     * @return Error message or empty string if success.
     */
    public static String resetPassword(String userId) {
        try {
            System.resetPassword((Id) userId, true);
            return '';
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    /**
     * @description Method is using to create Community User.
     * It is invoked from lsb_CommunitySelfRegister lcw component.
     *
     * @param newUser User record.
     * @param password Valid password that will be set for the User.
     *
     * @return URL to which will be redirected User after logging in.
     * @author Ewa Latoszek <ewa.latoszek@pwc.com>
     */
    @AuraEnabled
    public static String createCommunityUser(User newUser, String password) {
         try {
            Contact contactRecord = [
                    SELECT ID, LSB_ExternalID__c
                    FROM Contact
                    WHERE Email = :newUser.Email AND LSB_CON_SourceSystem__c = :LSB_Constants.CONTACT_SOURCE_SYSTEM_QL
                    LIMIT 1
            ];

            newUser.ProfileId = [
                    SELECT Id
                    FROM Profile
                    WHERE Name = :LSB_CommunitySelfRegisterController.SELFREG_PROFILE_NAME
                    LIMIT 1].Id;
            newUser.ContactId = contactRecord.Id;
            newUser.LSB_UER_ExternalID__c = contactRecord.LSB_ExternalID__c;
            newUser.IsActive = true;
            newUser.LanguageLocaleKey = newUser.LanguageLocaleKey == null ?
                    LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE :
                    newUser.LanguageLocaleKey;
            newUser.LocaleSidKey = newUser.LocaleSidKey == null ?
                    LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE :
                    newUser.LocaleSidKey;
            newUser.EmailEncodingKey = newUser.EmailEncodingKey == null ?
                    LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING :
                    newUser.EmailEncodingKey;
            newUser.TimeZoneSidKey = UserInfo.getTimezone().getID();
            newUser = handleUnsetRequiredFields(newUser);

            insert newUser;

            contactRecord.LSB_CON_User__c = newUser.Id;

            update contactRecord;

            PageReference lgn = new PageReference(LSB_CommunitySelfRegisterController.SELFREG_SUCCESS_PAGE_NAME);
            return lgn.getUrl();
        } catch(Exception ex) {
            System.debug('Exception: ' + ex.getMessage());
            return null;
        }
}

    // Loops over required fields that were not passed in to set to some default value
    private static User handleUnsetRequiredFields(User u) {
        if (String.isBlank(u.Username)) {
            u.Username = generateUsername(u);
        }
        if (String.isBlank(u.Alias)) {
            u.Alias = generateAlias(u);
        }
        if (String.isBlank(u.CommunityNickname)) {
            u.CommunityNickname = generateCommunityNickname(u);
        }
        return u;
    }

    private static Id getUserIdByUsername(String username) {
        List<User> users = [SELECT Id FROM User WHERE Username =: username];
        if (users.isEmpty()) {
            return null;
        }
        return users[0].Id;
    }

    private static String getLatestLoginAttemptResultMessage(Id userId) {
        String loginMessage;
        List<LoginHistory> loginHistoryRecords = [SELECT Id, UserId, LoginTime, Status FROM LoginHistory WHERE
                UserId =: userId ORDER BY LoginTime DESC LIMIT 1];
        if(!loginHistoryRecords.isEmpty()) {
            for (LoginHistory loginAttempt : loginHistoryRecords) {
                if (loginAttempt.Status != LOGIN_STATUS_INVALID_PASSWORD && loginAttempt.Status != LOGIN_STATUS_SUCCESS) {
                    loginMessage = System.Label.LSB_AccountLockedLoginMessage;
                } else {
                    loginMessage = loginAttempt.Status;
                }
            }
        }
        return loginMessage;
    }

    private static String generateAlias(User u) {
        return u.FirstName.toLowerCase().substring(0, 1) + u.LastName.toLowerCase().substring(0, 4);
    }

    private static String generateUsername(User u) {
        return u.Email;
    }

    private static String generateCommunityNickname(User u) {
        String nickname = u.FirstName.toLowerCase() + '.' + u.LastName.toLowerCase();

        if ([SELECT Id FROM User WHERE CommunityNickname = :nickname] != null) {
            nickname = u.Email;
        }

        return nickname;
    }

    /**
    * @description Method is using for login to Community.
    * It is invoked from LSB_CommunityLoginController class.
    *
    * @param username Username.
    * @param password Password that will be used for login.
    * @param startURL URL where user will be redirected after logging in.
    *
    * @return LoginResult wrapper with start URL or error message.
    */
    public static LSB_UserHelper.LoginResult loginToCommunity(String username, String password, String startURL) {
        LSB_UserHelper.LoginResult loginResult = new LSB_UserHelper.LoginResult();
        try {
            PageReference lgn = Site.login(username, password, startURL);
            loginResult.resultUrl = lgn.getUrl();
            return loginResult;
        } catch (Exception ex) {
            System.debug('Login exception >>> '+ ex.getMessage());
            Id userId = getUserIdByUsername(username);
            if (userId != null) {
                String message = getLatestLoginAttemptResultMessage(userId);
                loginResult.loginMessage = message;
            } else {
                loginResult.loginMessage = ex.getMessage();
            }
            return loginResult;
        }
    }

    public class LoginResult {
        @AuraEnabled public String resultUrl { get; set ;}
        @AuraEnabled public String loginMessage { get; set ;}
    }
}