public without sharing class LSB_Utils {

    static final Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();

    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';

    /**
     * @description Method used to get picklist values with dependencies.
     *
     * @param objectName API name of sObject
     * @param controllingField API name of controlled picklist
     * @param dependentField API name of dependent picklist
     *
     * @return Labels of controlled field picklist values to dependent picklist value labels.
     */
    public static Map<String, List<String>> getDependentOptions(String objectName, String controllingField, String dependentField) {
        Map<String, List<String>> objResults = new Map<String, List<String>>();

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();

        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);

        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();

        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }

        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;
    }

    /**
     * @description overloaded version of method used to get picklist values with dependencies. Uses api names as keys instead of labels
     *
     * @param objectName API name of sObject
     * @param controllingField API name of controlled picklist
     * @param dependentField API name of dependent picklist
     *
     * @return Labels of controlled field picklist values to dependent picklist value labels.
     */
    public static Map<String, List<String>> getDependentOptions(String objectName, String controllingField, String dependentField, Boolean byName) {
        Map<String, List<String>> objResults = new Map<String, List<String>>();

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();

        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);

        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();

        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getValue();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }

        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.value;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;
    }
    /**
     * @description Method used to get binary representation of decimal.
     *
     * @param val decimal to convert.
     *
     * @return binary representation of decimal.
     */
    private static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }

    /**
     * @description Method used to convert base64 value to binary.
     *
     * @param validFor base64 value to convert.
     *
     * @return base64 value in binary.
     */
    private static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';

        String validForBits = '';

        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }

        return validForBits;
    }

    /**
     * @description Method used to wrap picklist entries.
     *
     * @param PLEs list of Schema.PicklistEntry.
     *
     * @return List of wrapper instances.
     */
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }

    /**
     * @description Wrapper for PicklistEntry used for serialize.
     */
    public class PicklistEntryWrapper {
        @AuraEnabled
        public String active { get; set; }
        @AuraEnabled
        public String defaultValue { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public String validFor { get; set; }
    }

    public static LSB_Utils.PicklistEntryWrapper getPicklistEntryWrapper(String value, String label) {
        PicklistEntryWrapper entry = new PicklistEntryWrapper();

        entry.label = label;
        entry.value = value;

        return entry;
    }

    /**
     * @description build dynamic query string that can be use with Database.query
     *
     * @param objectApiName - object API name
     * @param fields - set of fields that will be retrieved in SOQL query
     *
     * @return query string
     */
    public static String buildDynamicQuery(String objectApiName, Set<String> fields) {
        return String.format('SELECT {0} FROM {1}', new List<String>{
            String.join(new List<String>(fields), ','), objectApiName
        });
    }

    public static Id getCaseRecordTypeIdByDeveloperName(String developerName) {
        Map<String, Schema.RecordTypeInfo> recordTypesByDeveloperName = Case.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName();
        return recordTypesByDeveloperName.get(developerName).getRecordTypeId();
    }

    /**
     * @description fetch Picklist Values by Record Type. The method reaches out to LSB_PicklistValuesToRecordTypeMap__mdt metadata configuration
     * @param objectApiName - object API name
     * @param picklistApiName picklist field API name
     * @param recordTypeDeveloperName record type developer name
     *
     * @return List<LSB_Utils.PicklistEntryWrapper>
     */
    @AuraEnabled(Cacheable=true)
    public static List<LSB_Utils.PicklistEntryWrapper> fetchPicklistEntryWrappers(String objectApiName, String picklistApiName,
        String recordTypeDeveloperName) {
        List<LSB_Utils.PicklistEntryWrapper> picklistEntryWrappers = new List<LSB_Utils.PicklistEntryWrapper>();
        List<LSB_PicklistValuesToRecordTypeMap__mdt> availablePicklistValuesRecord = [
            SELECT Id,
                ObjectAPIName__c,
                PicklistAPIName__c,
                RecordTypeDeveloperName__c,
                AvailablePicklistValues__c
            FROM LSB_PicklistValuesToRecordTypeMap__mdt
            WHERE ObjectAPIName__c = :objectApiName
                AND PicklistAPIName__c = :picklistApiName
                AND RecordTypeDeveloperName__c = :recordTypeDeveloperName
            LIMIT 1
        ];
        Set<String> availablePicklistValuesSet;
        if (!availablePicklistValuesRecord.isEmpty()) {
            availablePicklistValuesSet = new Set<String>(availablePicklistValuesRecord[0].AvailablePicklistValues__c.replace('\r\n','\n').split('\n'));
        }
        for (Schema.PicklistEntry pe : GlobalDescribeMap.get(objectApiName).getDescribe().fields.getMap().get(picklistApiName).getDescribe().getPicklistValues()) {
            if (pe.isActive()) {
                if (availablePicklistValuesSet == null
                    || (availablePicklistValuesSet != null
                    && availablePicklistValuesSet.contains(pe.getValue()))
                    ) {
                    LSB_Utils.PicklistEntryWrapper pew = new LSB_Utils.PicklistEntryWrapper();
                    pew.value = pe.getValue();
                    pew.label = pe.getLabel();
                    picklistEntryWrappers.add(pew);
                }
            }
        }


        return picklistEntryWrappers;
    }

    public static Map<String, List<Group>> getTeamNameToRelatedGroups(Set<String> teamNames) {
        Map<String, String> roleDeveloperName2RoleLabel = new Map<String, String>();

        for (UserRole role : [SELECT Id, Name, DeveloperName FROM UserRole WHERE Name LIKE :teamNames]) {
            roleDeveloperName2RoleLabel.put(role.DeveloperName, getTeamNameFromRoleName(role.Name));
        }

        Map<String, List<Group>> roleTeams = new Map<String, List<Group>>();

        for (Group roleGroup : [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN :roleDeveloperName2RoleLabel.keySet() AND Type = 'Role']) {
            String teamName = roleDeveloperName2RoleLabel.get(roleGroup.DeveloperName);

            if (!roleTeams.containsKey(teamName)) {
                roleTeams.put(teamName, new List<Group>());
            }

            roleTeams.get(teamName).add(roleGroup);
        }

        return roleTeams;
    }

    public static String getTeamNameFromRoleName(String roleName) {
        return roleName.contains(']') ? roleName.substringBeforeLast(']') + ']' : '';
    }

    /**
     * @description fetch Status Definitions
     *
     * @param objectApiName
     * @param picklistApiName
     * @param recordTypeDeveloperName
     *
     * @return
     */
    @AuraEnabled(Cacheable=true)
    public static String fetchStatusDefinitions(String objectApiName, String picklistApiName,
        String recordTypeDeveloperName) {
        List<LSB_PicklistValuesToRecordTypeMap__mdt> availablePicklistValuesRecord = [
            SELECT StatusDefinitions__c
            FROM LSB_PicklistValuesToRecordTypeMap__mdt
            WHERE ObjectAPIName__c = :objectApiName
                AND PicklistAPIName__c = :picklistApiName
                AND RecordTypeDeveloperName__c = :recordTypeDeveloperName
            LIMIT 1
        ];
        if (availablePicklistValuesRecord.isEmpty()) {
            return '';
        } else {
            return availablePicklistValuesRecord[0].StatusDefinitions__c;
        }
    }

    public static String getConfigFromMetadataByDeveloperName(String configName) {
        List<LSB_Configuration__mdt> configRecords = [SELECT DeveloperName, LSB_Value__c, LSB_Description__c FROM LSB_Configuration__mdt WHERE DeveloperName =: configName];

        if (configRecords.isEmpty()) {
            return null;
        }

        return configRecords[0].LSB_Value__c;
    }

    public static SObject getRecordByObjectNameAndRecordNameStoredInMetadataConfig(String objectApiName, String recordNameMtd) {
        String recordName = getConfigFromMetadataByDeveloperName(recordNameMtd);

        if (String.isNotBlank(recordName)) {
            String query = 'SELECT Id, Name FROM ' + objectApiName + ' WHERE Name =: recordName';
            List<SObject> records = Database.query(query);
            if (!records.isEmpty()) {
                return records[0];
            }
        }

        return null;
    }

}