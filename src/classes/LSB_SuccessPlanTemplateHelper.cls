public without sharing class LSB_SuccessPlanTemplateHelper {

    private static final String PRIVATE_ACCESS_TYPE = '0';
    private static final String DEFAULT_SHARE_ACCESS = 'Edit';

    public void applyDefaultTeam(List<sfal__SuccessPlanTemplate__c> newPlans) {
        List<sfal__SuccessPlanTemplate__Share> sharingToInsert = new List<sfal__SuccessPlanTemplate__Share>();
        List<sfal__SuccessPlanTemplate__c> privatePlans = new List<sfal__SuccessPlanTemplate__c>();
        List<sfal__SuccessPlanTemplate__c> openPlans = new List<sfal__SuccessPlanTemplate__c>();
        Set<Id> userIds = new Set<Id>();

        for (sfal__SuccessPlanTemplate__c plan : newPlans) {
            userIds.add(plan.CreatedById);

            if (plan.LSB_Type__c == PRIVATE_ACCESS_TYPE) {
                privatePlans.add(plan);
            } else {
                openPlans.add(plan);
            }
        }

        Map<Id, User> createdByUsersByIds = new Map<Id, User>(
        [SELECT Id, UserRole.Name, ManagerId FROM User WHERE Id = :userIds]
        );

        sharingToInsert.addAll(processPrivatePlans(privatePlans, createdByUsersByIds));
        sharingToInsert.addAll(processOpenPlans(openPlans, createdByUsersByIds));

        if (!sharingToInsert.isEmpty()) {
            insert sharingToInsert;
        }
    }

    public void handleTypeChange(List<sfal__SuccessPlanTemplate__c> newPlans, Map<Id, sfal__SuccessPlanTemplate__c> oldPlans) {
        Map<Id, sfal__SuccessPlanTemplate__c> applyNewSharing = new Map<Id, sfal__SuccessPlanTemplate__c>();

        for (sfal__SuccessPlanTemplate__c plan : newPlans) {
            if (plan.LSB_Type__c != oldPlans.get(plan.Id).LSB_Type__c &&
                    (plan.LSB_Type__c == PRIVATE_ACCESS_TYPE ||
                            oldPlans.get(plan.Id).LSB_Type__c == PRIVATE_ACCESS_TYPE)) {
                applyNewSharing.put(plan.Id, plan);
            }
        }

        String reason = Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c;

        delete [SELECT Id
            FROM sfal__SuccessPlanTemplate__Share
            WHERE RowCause = :reason AND ParentId IN :applyNewSharing.keySet()];

        applyDefaultTeam(applyNewSharing.values());
    }

    private static List<sfal__SuccessPlanTemplate__Share> processPrivatePlans(List<sfal__SuccessPlanTemplate__c> privatePlans, Map<Id, User> createdByUsersByIds) {
        List<sfal__SuccessPlanTemplate__Share> sharingToInsert = new List<sfal__SuccessPlanTemplate__Share>();

        for (sfal__SuccessPlanTemplate__c plan : privatePlans) {
            if (createdByUsersByIds.get(plan.CreatedById).ManagerId != null) {
                sharingToInsert.add(
                        new sfal__SuccessPlanTemplate__Share(
                                AccessLevel = DEFAULT_SHARE_ACCESS,
                                ParentId = plan.Id,
                                UserOrGroupId = createdByUsersByIds.get(plan.CreatedById).ManagerId,
                                RowCause = Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c
                        ));
            }
        }

        return sharingToInsert;
    }

    private static List<sfal__SuccessPlanTemplate__Share> processOpenPlans(List<sfal__SuccessPlanTemplate__c> openPlans, Map<Id, User> createdByUsersByIds) {
        List<sfal__SuccessPlanTemplate__Share> sharingToInsert = new List<sfal__SuccessPlanTemplate__Share>();

        Set<String> teamNames = new Set<String>();
        for (sfal__SuccessPlanTemplate__c plan : openPlans) {
            String teamName = LSB_Utils.getTeamNameFromRoleName(createdByUsersByIds.get(plan.CreatedById).UserRole.Name);
            teamNames.add(teamName + '%');
        }

        Map<String, List<Group>> roleTeams = LSB_Utils.getTeamNameToRelatedGroups(teamNames);

        for (sfal__SuccessPlanTemplate__c plan : openPlans) {
            String teamName = LSB_Utils.getTeamNameFromRoleName(createdByUsersByIds.get(plan.CreatedById).UserRole.Name);
            for (Group team : roleTeams.get(teamName)) {
                sharingToInsert.add(
                        new sfal__SuccessPlanTemplate__Share(
                                AccessLevel = DEFAULT_SHARE_ACCESS,
                                ParentId = plan.Id,
                                UserOrGroupId = team.Id,
                                RowCause = Schema.sfal__SuccessPlanTemplate__Share.RowCause.LSB_ApexTriggerReason__c
                        ));
            }
        }

        return sharingToInsert;
    }
}