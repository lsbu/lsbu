@IsTest
class LSB_CaseCommentsControllerTest {

    @TestSetup
    static void testData() {

        Account account = new Account(Name = 'Test Account');
        insert account;

        Contact contact = new Contact(AccountId = account.Id, FirstName = 'Test', LastName = 'Contact', LSB_CON_CurrentRole__c = 'Prospect');
        insert contact;

        Case testCase = new Case(Subject = 'Test Case', ContactId = contact.Id, LSB_CAS_ContactRole__c = 'Prospect');
        insert testCase;

        CaseComment testCaseComment = new CaseComment(
            ParentId = testCase.Id,
            CommentBody = 'Test 123',
            IsPublished = true
        );

        insert testCaseComment;
    }

    @IsTest
    static void fetchCaseCommentsTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        List<LSB_CaseActivitiesHelper.MessageWrapper> caseComments = LSB_CaseCommentsController.getCaseMessagesByParentId(testCase.Id);
        Test.stopTest();
        System.assertEquals(1, caseComments.size());
    }

    @IsTest
    static void createCaseCommentTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        LSB_CaseCommentsController.createCaseComment(testCase.Id, 'Test Message');
        Test.stopTest();
        List<LSB_CaseActivitiesHelper.MessageWrapper> caseComments = LSB_CaseCommentsController.getCaseMessagesByParentId(testCase.Id);
        System.assertEquals(2, caseComments.size());
    }

}