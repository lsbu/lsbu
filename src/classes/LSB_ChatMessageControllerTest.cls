@IsTest
public with sharing class LSB_ChatMessageControllerTest {

    private static String DATE_FOR_REVIEW_3_MONTHS = LSB_Constants.THREE_MONTHS_FOR_REVIEW;
    private static String SEARCH_FIELD_NAME = 'LSB_KGE_ChatbotResponseLong__c';

    @IsTest
    private static void shouldReturnArticleById() {
        Knowledge__kav testArticle = new Knowledge__kav();
        testArticle.Title = 'Unit Test ChatMessage Apex class';
        testArticle.UrlName = 'Unit-Test-ChatMessage-Apex-Class';
        testArticle.LSB_KGE_DateForReview__c = DATE_FOR_REVIEW_3_MONTHS;
        testArticle.LSB_KGE_FaqCategory2__c = 'EnrolmentAndInduction';
        testArticle.IsVisibleInPkb = true;
        testArticle.LSB_KGE_ChatbotResponse__c = 'Test';
        testArticle.LSB_KGE_ChatbotResponseLong__c = 'Test';

        insert testArticle;

        LSB_ChatMessageController.ArticleWrapper result = LSB_ChatMessageController.getKnowledgeDataById(testArticle.Id, SEARCH_FIELD_NAME);

        System.assertEquals(testArticle.Id, result.id);
    }

    @IsTest
    private static void shouldReturnNullWhenNoArticleFound() {
        LSB_ChatMessageController.ArticleWrapper result = LSB_ChatMessageController.getKnowledgeDataById('ka03L000000', SEARCH_FIELD_NAME);

        System.assertEquals(null, result);
    }
}