public with sharing class LSB_AddCaseTeamMemberController {

    private static final String USER_TYPE_STANDARD = 'Standard';

    @AuraEnabled
    public static Boolean addCaseTeamMember(Id caseId, Id memberId, String roleJSON) {
        return LSB_CaseTeamHelper.addCaseTeamMember(caseId, memberId, roleJSON);

    }

    @AuraEnabled(Cacheable = true)
    public static List<User> fetchUsers(String userName) {
        String searchString = '%' + userName + '%';
        return [
                SELECT Id, Name
                FROM User
                WHERE Name LIKE :searchString
                AND UserType = :USER_TYPE_STANDARD
                AND IsActive = true
                LIMIT 15
        ];
    }

    @AuraEnabled
    public static List<LSB_CaseTeamHelper.TeamRoleWrapper> getCaseTeamRoles() {
        return LSB_CaseTeamHelper.getBasicCaseTeamRoles();
    }

    @AuraEnabled
    public static Boolean checkIfConfigExist(String roleName) {
        List<LSB_CRC_CaseTeamRoleConfiguration__c> roleConfigRecords = LSB_CaseTeamHelper.getRoleConfigRecordsByName(roleName);
        if (!roleConfigRecords.isEmpty()) {
            return true;
        }
        return false;
    }

}