public without sharing class LSB_PreviousYearResultsController {

    private static final String STATUS_PASSED = 'P';
    private static final String NOT_APPLICABLE = 'N/A';
    private static final String YES = Label.LSB_PreviousYearResultsYes;
    private static final String NO = Label.LSB_PreviousYearResultsNo;

    public class AcademicYearTabWrapper {
        @AuraEnabled
        public String tabName;
        @AuraEnabled
        public String overallCourseGrade;
        @AuraEnabled
        public List<ModuleWrapper> moduleWrappers;
        @AuraEnabled
        public Id termId;
    }

    public class ModuleWrapper {
        @AuraEnabled
        public Boolean isExpanded = false;
        @AuraEnabled
        public String styleClass = 'module__collapsed';
        @AuraEnabled
        public String moduleName;
        @AuraEnabled
        public Boolean outcome;
        @AuraEnabled
        public String marks;
        @AuraEnabled
        public String semester;
        @AuraEnabled
        public String attempts;
        @AuraEnabled
        public String resit;
        @AuraEnabled
        public String credits;
        @AuraEnabled
        public List<CourseworkWrapper> courseworkAndExams;
    }

    public class CourseworkWrapper {
        @AuraEnabled
        public String name;
        @AuraEnabled
        public String grade;

        CourseworkWrapper(String name, String grade) {
            this.name = name;
            this.grade = grade;
        }
    }

    @AuraEnabled(Cacheable=true)
    public static List<AcademicYearTabWrapper> getPreviousYearResults() {

        List<AcademicYearTabWrapper> academicYearTabWrappers = new List<AcademicYearTabWrapper>();

        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, ContactId FROM User WHERE Id = :userId];
        Id contactId = u.ContactId;

        Map<Id, hed__Term__c> termsByIds = new Map<Id, hed__Term__c>();

        for (hed__Program_Enrollment__c pe : getProgramEnrollmentsForPreviousYears(contactId)) {
            AcademicYearTabWrapper academicYearTabWrapper = new AcademicYearTabWrapper();
            academicYearTabWrapper.overallCourseGrade = pe.LSB_PEN_OverallCourseGrade__c;
            academicYearTabWrapper.tabName = (String) pe.get('tabName');
            academicYearTabWrapper.termId = pe.LSB_PEN_Term__c;
            academicYearTabWrapper.moduleWrappers = new List<ModuleWrapper>();
            termsByIds.put(pe.LSB_PEN_Term__c, null);
            academicYearTabWrappers.add(academicYearTabWrapper);
        }

        termsByIds.putAll(getTermsWithTermGrades(contactId, termsByIds));

        for (AcademicYearTabWrapper previousAcademicYear : academicYearTabWrappers) {
            hed__Term__c academicTerm = termsByIds.get(previousAcademicYear.termId);
            if (academicTerm != null) {
                for (hed__Term_Grade__c tg : academicTerm.hed__Term_Grades__r) {
                    ModuleWrapper moduleWrapper = new ModuleWrapper();
                    moduleWrapper.outcome = tg.LSB_TDE_ModuleGrade__c == STATUS_PASSED;
                    if (!moduleWrapper.outcome) {
                        moduleWrapper.styleClass = 'module__collapsed module__failed';
                    }
                    moduleWrapper.marks = tg.LSB_TDE_ModuleMarks__c;
                    moduleWrapper.attempts = tg.LSB_TDE_Attempt__c;
                    moduleWrapper.semester = tg.LSB_TDE_Semester__c;
                    moduleWrapper.resit = tg.LSB_TDE_Resit__c ? YES : NO;
                    moduleWrapper.credits = String.valueOf(tg.LSB_TDE_ModuleCredit__c);
                    moduleWrapper.moduleName = tg.hed__Course_Connection__r.LSB_CCN_ModuleInstanceName__c;
                    moduleWrapper.courseworkAndExams = new List<CourseworkWrapper>();
                    loadCourseWorkAndExams(tg, moduleWrapper);
                    previousAcademicYear.moduleWrappers.add(moduleWrapper);
                }
            }
        }

        return academicYearTabWrappers;
    }

    private static void loadCourseWorkAndExams(hed__Term_Grade__c tg, ModuleWrapper moduleWrapper) {
        if (String.isNotBlank(tg.LSB_TDE_CourseWork1__c) && tg.LSB_TDE_CourseWork1__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_CourseWork1__c.getDescribe().getLabel(),
                    tg.LSB_TDE_CourseWork1__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_CourseWork2__c) && tg.LSB_TDE_CourseWork2__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_CourseWork2__c.getDescribe().getLabel(),
                    tg.LSB_TDE_CourseWork2__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_CourseWork3__c) && tg.LSB_TDE_CourseWork3__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_CourseWork3__c.getDescribe().getLabel(),
                    tg.LSB_TDE_CourseWork3__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_CourseWork4__c) && tg.LSB_TDE_CourseWork4__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_CourseWork4__c.getDescribe().getLabel(),
                    tg.LSB_TDE_CourseWork4__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_CourseWork5__c) && tg.LSB_TDE_CourseWork5__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_CourseWork5__c.getDescribe().getLabel(),
                    tg.LSB_TDE_CourseWork5__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_CourseWork6__c) && tg.LSB_TDE_CourseWork6__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_CourseWork6__c.getDescribe().getLabel(),
                    tg.LSB_TDE_CourseWork6__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_Exam1__c) && tg.LSB_TDE_Exam1__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_Exam1__c.getDescribe().getLabel(),
                    tg.LSB_TDE_Exam1__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_Exam2__c) && tg.LSB_TDE_Exam2__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_Exam2__c.getDescribe().getLabel(),
                    tg.LSB_TDE_Exam2__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_Exam3__c) && tg.LSB_TDE_Exam3__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_Exam3__c.getDescribe().getLabel(),
                    tg.LSB_TDE_Exam3__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_Exam4__c) && tg.LSB_TDE_Exam4__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_Exam4__c.getDescribe().getLabel(),
                    tg.LSB_TDE_Exam4__c
                )
            );
        }
        if (String.isNotBlank(tg.LSB_TDE_Exam5__c) && tg.LSB_TDE_Exam5__c != NOT_APPLICABLE) {
            moduleWrapper.courseworkAndExams.add(
                new CourseworkWrapper(
                    hed__Term_Grade__c.LSB_TDE_Exam5__c.getDescribe().getLabel(),
                    tg.LSB_TDE_Exam5__c
                )
            );
        }
    }

    private static List<hed__Program_Enrollment__c> getProgramEnrollmentsForPreviousYears(Id contactId) {
        return [
            SELECT Id,
                toLabel(hed__Class_Standing__c) tabName,
                LSB_PEN_OverallCourseGrade__c,
                LSB_PEN_Term__c
            FROM hed__Program_Enrollment__c
            WHERE LSB_PEN_LatestEnrolmentRecord__c = FALSE
                AND hed__Enrollment_Status__c = :LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED
                AND hed__Contact__c = :contactId
            ORDER BY hed__Class_Standing__c ASC
        ];
    }

    private static List<hed__Term__c> getTermsWithTermGrades(Id contactId, Map<Id, hed__Term__c> termsByIds) {
        return [
            SELECT Id, (
                SELECT Id,
                    hed__Course_Connection__r.LSB_CCN_ModuleInstanceName__c,
                    LSB_TDE_ModuleGrade__c,
                    LSB_TDE_ModuleMarks__c,
                    LSB_TDE_Attempt__c,
                    LSB_TDE_Semester__c,
                    LSB_TDE_Resit__c,
                    LSB_TDE_ModuleCredit__c,
                    LSB_TDE_CourseWork1__c,
                    LSB_TDE_CourseWork2__c,
                    LSB_TDE_CourseWork3__c,
                    LSB_TDE_CourseWork4__c,
                    LSB_TDE_CourseWork5__c,
                    LSB_TDE_CourseWork6__c,
                    LSB_TDE_Exam1__c,
                    LSB_TDE_Exam2__c,
                    LSB_TDE_Exam3__c,
                    LSB_TDE_Exam4__c,
                    LSB_TDE_Exam5__c
                FROM hed__Term_Grades__r
                WHERE hed__Contact__c = :contactId
                    AND hed__Course_Connection__r.RecordType.DeveloperName = :LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME
                    AND hed__Course_Connection__r.LSB_CCN_LatestEnrolmentRecord__c = FALSE
                    AND hed__Course_Connection__r.LSB_CCN_ModuleEnrolmentStatus__c = :LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED
                    AND hed__Course_Connection__r.hed__Program_Enrollment__r.hed__Enrollment_Status__c = :LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED
                    ORDER BY hed__Course_Connection__r.LSB_CCN_Semester__c DESC
            )
            FROM hed__Term__c
            WHERE Id IN :termsByIds.keySet()
        ];
    }

}