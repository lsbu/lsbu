public without sharing class LSB_AttachmentsHelper {

    public static ContentVersion saveFile(String parentId, String fileName, Blob blobData) {
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = fileName;
        contentVersion.PathOnClient = '/' + fileName;
        contentVersion.VersionData = blobData;
        contentVersion.IsMajorVersion = true;
        try {
            insert contentVersion;
        } catch (Exception e) {
            System.debug('Error while saving the ContentVersion: ' + e.getMessage());
        }

        ContentVersion insertedVersion = [SELECT Id, LSB_CVR_DownloadLink__c, ContentDocumentId, Title FROM ContentVersion WHERE Id = :contentVersion.Id ];

        ContentDocumentLink documentLink = new ContentDocumentLink();
        documentLink.ContentDocumentId = insertedVersion.ContentDocumentId;
        documentLink.LinkedEntityId = parentId;
        documentLink.ShareType = 'V';
        documentLink.Visibility = 'AllUsers';

        try {
            insert documentLink;
        } catch (Exception e) {
            System.debug('Error while saving the ContentDocumentLink: ' + e.getMessage());
        }

        return insertedVersion;
    }

    /**
     * @description get content versions
     *
     * @param parentRecordId - parent record Id
     *
     * @return list of content versions
     */
    public static List<ContentVersion> getContentVersions(Id parentRecordId) {
        Set<Id> contentDocumentsIds = new Set<Id>();
        for (ContentDocumentLink contentDocumentLink : [
                SELECT Id, ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :parentRecordId
        ]) {
            contentDocumentsIds.add(contentDocumentLink.ContentDocumentId);
        }
        if (!contentDocumentsIds.isEmpty()) {
            return [
                    SELECT Id, Title, FileExtension, ContentDocumentId, LSB_CVR_DownloadLink__c, CreatedById
                    FROM ContentVersion
                    WHERE ContentDocumentId IN :contentDocumentsIds
            ];
        } else {
            return new List<ContentVersion>();
        }
    }

    /**
     * @description delete attachments
     *
     * @param documentId - id of attachment
     */
    public static void deleteAttachment(Id documentId) {
        ContentDocument contentDocument = [SELECT Id FROM ContentDocument WHERE Id = :documentId LIMIT 1];
        delete contentDocument;
    }
}