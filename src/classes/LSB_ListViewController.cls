public with sharing class LSB_ListViewController {

    static final Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();

    /**
     * @description fetch records
     *
     * @param objectApiName object API name
     * @param fields list of field api names
     * @param queryFilter query filter
     *
     * @return List<SObject>
     */
    @AuraEnabled
    public static List<SObject> fetchRecords(String objectApiName, List<String> fields, String queryFilter) {
        return Database.query(buildDynamicQuery(objectApiName, getFieldsToSelectWithLabels(objectApiName, fields), queryFilter));
    }

    /**
     * @description fetch List View columns for given object and field set
     *
     * @param objectApiName - object API name
     * @param fieldSetApiName - field set API name
     *
     * @return List<LSB_ListViewController.ListViewColumn>
     */
    @AuraEnabled(Cacheable=true)
    public static List<ListViewColumn> fetchListViewColumns(String objectApiName, String fieldSetApiName) {
        List<ListViewColumn> listViewColumns = new List<ListViewColumn>();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
        if (SObjectTypeObj == null) {
            AuraHandledException ex = new AuraHandledException('Object Not Found: ' + objectApiName);
            ex.setMessage('Object Not Found: ' + objectApiName);
            throw ex;
        }
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);
        if (fieldSetObj == null) {
            AuraHandledException ex = new AuraHandledException('Field Set Not Found: ' + fieldSetApiName);
            ex.setMessage('Field Set Not Found: ' + fieldSetApiName);
            throw ex;
        }
        if (fieldSetObj.getFields().isEmpty()) {
            AuraHandledException ex = new AuraHandledException('Field Set Is Empty: ' + fieldSetApiName);
            ex.setMessage('Field Set Is Empty: ' + fieldSetApiName);
            throw ex;
        }
        for (FieldSetMember fsm : fieldSetObj.getFields()) {
            listViewColumns.add(new ListViewColumn(fsm));
        }

        return listViewColumns;
    }

    /**
     * @description Wrapper class for columns
     */
    public class ListViewColumn {
        @AuraEnabled
        public String fieldNameOrPath { get; set; }
        @AuraEnabled
        public String relationshipName { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public Boolean hidden { get; set; }
        @AuraEnabled
        public Boolean sortable { get; set; }
        @AuraEnabled
        public Boolean sortUp { get; set; }
        @AuraEnabled
        public Boolean sortBy { get; set; }
        @AuraEnabled
        public Boolean isHtmlFormatted { get; set; }

        public ListViewColumn(Schema.FieldSetMember fieldSetMember) {
            DescribeFieldResult fieldResult = fieldSetMember.getSObjectField().getDescribe();
            this.fieldNameOrPath = fieldSetMember.getFieldPath() != null ? fieldSetMember.getFieldPath() : fieldResult.getName();
            this.relationshipName = fieldResult.getRelationshipName();
            this.label = fieldResult.getLabel();
            this.type = String.valueOf(fieldResult.getType());
            this.hidden = !fieldResult.isAccessible();
            this.sortable = fieldResult.isSortable();
            this.sortBy = false;
            this.sortUp = false;
            this.isHtmlFormatted = fieldResult.isHtmlFormatted();
        }
    }

    private static String buildDynamicQuery(String objectApiName, List<String> fields, String queryFilter) {
        List<RecordType> recordTypes = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType = :objectApiName
        ];
        if (recordTypes.size() > 0) {
            return String.format('SELECT RecordTypeId, RecordType.DeveloperName, {0} FROM {1} {2}', new List<String>{
                String.join(new List<String>(fields), ','), objectApiName, queryFilter
            });
        } else {
            return String.format('SELECT {0} FROM {1} {2}', new List<String>{
                String.join(new List<String>(fields), ','), objectApiName, queryFilter
            });
        }
    }

    /**
     * @description action on record in Task list View
     * @param recordId - record Id
     * @param newStatus - new task status
     */
    @AuraEnabled
    public static void changeTaskStatus(String recordId, String newStatus) {
        Task task = new Task(Id = recordId, Status = newStatus);
        update task;
    }

    /**
     * @description fetch record with given fields
     *
     * @param objectApiName - object API name
     * @param recordId - record Id
     * @param fields - list of fields
     *
     * @return SObject record
     */
    @AuraEnabled
    public static SObject fetchRecord(String objectApiName, String recordId, List<String> fields) {
        String query = buildDynamicQuery(objectApiName, getFieldsToSelectWithLabels(objectApiName, fields), ' WHERE Id = \'' + recordId + '\'');
        return Database.query(query);
    }

    /**
     * @description withdraw case
     *
     * @param recordId
     * @param closedReason
     *
     * @return Case
     */
    @AuraEnabled
    public static Case withdrawCase(String recordId, String closedReason) {
        Case caseToUpdate = [
            SELECT LSB_CAS_Response__c,
                Status,
                LSB_CAS_ClosedReason__c,
                OwnerId,
                LSB_CAS_CaseOwnerIsUser__c
            FROM Case
            WHERE Id = :recordId
            LIMIT 1
        ];

        caseToUpdate.Status = LSB_Constants.CASE_STATUS_CLOSED;
        caseToUpdate.LSB_CAS_ClosedReason__c = closedReason;
        caseToUpdate.LSB_CAS_Response__c = String.format(System.Label.LSB_CaseWasWithdrawnWithReason, new String[]{
            String.valueOf(closedReason)
        });
        caseToUpdate.OwnerId = !caseToUpdate.LSB_CAS_CaseOwnerIsUser__c ? UserInfo.getUserId() : caseToUpdate.OwnerId;
        update caseToUpdate;
        return caseToUpdate;
    }

    @AuraEnabled
    public static Boolean isEcClaim(String caseId) {
        List<Case> ecClaims = new List<Case>([
            SELECT Id
            FROM Case
            WHERE Id = :caseId
                AND RecordType.DeveloperName = :LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME
        ]);
        return !ecClaims.isEmpty();
    }

    @AuraEnabled
    public static Case withdrawClaim(Id caseId, String withdrawalReason, String withdrawalComment) {
        Case caseToUpdate = [
            SELECT Status,
                LSB_CAS_WithdrawalReasons__c,
                LSB_CAS_WithdrawalComments__c
            FROM Case
            WHERE Id = :caseId
            LIMIT 1
        ];

        caseToUpdate.Status = LSB_Constants.CASE_EC_CLAIM_REQUEST_WITHDRAWN_BY_STUDENT;
        caseToUpdate.LSB_CAS_WithdrawalReasons__c = withdrawalReason;
        caseToUpdate.LSB_CAS_WithdrawalComments__c = withdrawalComment;

        update caseToUpdate;
        return caseToUpdate;
    }

    private static List<String> getFieldsToSelectWithLabels(String objectApiName, List<String> fields) {
        Map<String, SObjectField> fieldMap = GlobalDescribeMap.get(objectApiName).getDescribe().fields.getMap();
        List<String> fieldsToSelect = new List<String>();
        for (String field : fields) {
            fieldsToSelect.add(field);
            if (fieldMap.containsKey(field) && fieldMap.get(field).getDescribe().getType() == DisplayType.PICKLIST) {
                fieldsToSelect.add('toLabel(' + field + ') ' + field + 'Label');
            }
        }
        return fieldsToSelect;
    }

    @AuraEnabled(Cacheable=true)
    public static String getEcClaimRecordTypeId() {
        return LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME);
    }

    @AuraEnabled
    public static Case submitReviewRequest(Id caseId, String reviewReason, String reviewComment) {
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.useDefaultRule = true;
        Case caseToUpdate = new Case(
            Id = caseId,
            LSB_CAS_ReviewReason__c = reviewReason,
            LSB_CAS_ReviewRequestComments__c = reviewComment,
            Status = LSB_Constants.CASE_STATUS_REQUEST_BEING_REVIEWED
        );
        caseToUpdate.setOptions(dmlOpts);

        update caseToUpdate;
        return caseToUpdate;
    }
}