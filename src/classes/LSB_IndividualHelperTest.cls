@IsTest
private class LSB_IndividualHelperTest {
    @TestSetup
    static void loadData() {
        User usr = TestDataClass.insertUser('Advisor');

        insert new List<hed__Trigger_Handler__c>{
                new hed__Trigger_Handler__c(
                        hed__Active__c = true,
                        hed__Class__c = 'LSB_TriggerDispatcher',
                        hed__Load_Order__c = 1,
                        hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                        hed__Object__c = 'Individual'
                ),
                new hed__Trigger_Handler__c(
                        hed__Active__c = true,
                        hed__Class__c = 'LSB_TriggerDispatcher',
                        hed__Load_Order__c = 1,
                        hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                        hed__Object__c = 'Contact'
                ),
                new hed__Trigger_Handler__c(
                        hed__Active__c = true,
                        hed__Class__c = 'LSB_TriggerDispatcher',
                        hed__Load_Order__c = 1,
                        hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                        hed__Object__c = 'Address__c'
                )
        };
        TestDataClass.insertDataUsePurpose();
        TestDataClass.insertContact();

    }
    @IsTest
    static void updateContactPointConsentTest() {
        List<User> users = [SELECT Id FROM User WHERE Username=:TestDataClass.userName];
        List<Contact> contactList = [
                SELECT
                        Id,
                        IndividualId
                FROM Contact
                WHERE hed__AlternateEmail__c = :TestDataClass.communityUserEmail
        ];
        System.assertEquals(1, contactList.size());

        List<DataUsePurpose> dup = [
                SELECT
                        Id
                FROM DataUsePurpose
                WHERE LSB_Type__c = :LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST
        ];
        System.assertEquals(1, dup.size());

        List<ContactPointEmail> cpe = [SELECT Id FROM ContactPointEmail WHERE LSB_COE_Contact__c = :contactList.get(0).Id];
        System.assertEquals(true, !cpe.isEmpty());

        ContactPointConsent cpc = new ContactPointConsent(
                ContactPointId = cpe.get(0).Id,
                Name = LSB_Constants.CONTACT_POINT_CONSENT_NAME_LEGITIMATE_INTEREST,
                PrivacyConsentStatus = LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN,
                CaptureDate = System.now(),
                EffectiveFrom = System.now(),
                LSB_COC_Contact__c = contactList.get(0).Id,
                DataUsePurposeId = dup.get(0).Id
        );

        insert cpc;

        List<Individual> individuals = [
                SELECT
                        Id,
                        HasOptedOutSolicit,
                        HasOptedOutProcessing
                FROM
                        Individual
                WHERE Id = :contactList.get(0).IndividualId
        ];
        System.assertEquals(1, individuals.size());
        System.assertEquals(false, individuals.get(0).HasOptedOutProcessing);
        System.assertEquals(false, individuals.get(0).HasOptedOutSolicit);

        individuals.get(0).HasOptedOutProcessing = true;
        individuals.get(0).HasOptedOutSolicit = true;
        System.runAs(users.get(0)){
        Test.startTest();
        update individuals;
        Test.stopTest();
    }

        List<ContactPointConsent> contactPointConsentsLegitimate = [
                SELECT
                        Id,
                        PrivacyConsentStatus,
                        DataUsePurpose.LSB_Type__c
                FROM ContactPointConsent
                WHERE LSB_COC_Contact__c = :contactList.get(0).Id
                AND DataUsePurpose.LSB_Type__c = :LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST
        ];

        System.assertEquals(LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT, contactPointConsentsLegitimate.get(0).PrivacyConsentStatus);

        List<ContactPointConsent> contactPointConsentsExplicit = [
                SELECT
                        Id,
                        PrivacyConsentStatus,
                        DataUsePurpose.LSB_Type__c
                FROM ContactPointConsent
                WHERE LSB_COC_Contact__c = :contactList.get(0).Id
                AND DataUsePurpose.LSB_Type__c = :LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST
        ];
        System.assertEquals(LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT, contactPointConsentsExplicit.get(0).PrivacyConsentStatus);
    }
}