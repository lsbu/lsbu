@IsTest
public with sharing class LSB_SurveyRedirectControllerTest {

    private static final String LAST_VISITED_PAGE = 'www.lsbu.ac.uk';
    private static final String TRANSCRIPT_CONTENT_TEXT = 'This is test transcript content text. Cheers!';


    @IsTest
    private static void shouldReturnNullWhenNoSurvey() {
        setup();

        LSB_SurveyRedirectController controller = new LSB_SurveyRedirectController();

        PageReference result = controller.redirectToSurvey();
        System.assertEquals(null, result);
    }

    @IsTest
    private static void shouldReturnThankYouMessage() {
        LSB_SurveyRedirectController controller = new LSB_SurveyRedirectController();

        String result = controller.getThankYouMessage();
        System.assertNotEquals(null, result);
    }

    @IsTest
    private static void shouldSaveTranscriptAsFileAndReturnRedirectUrl() {
        setup();

        LSB_SurveyRedirectController controller = new LSB_SurveyRedirectController();

        PageReference result = controller.getTranscriptFromFile();

        System.assert(String.isNotBlank(result.getUrl()));
    }

    private static void setup() {
        LSB_CommunityUrl__c communityUrlSettings = new LSB_CommunityUrl__c();
        communityUrlSettings.LSB_CommunityURL__c = 'www.myAccount.com';
        insert communityUrlSettings;

        Profile profile = [SELECT Id FROM Profile WHERE Name='Advisor'];
        User agentUSer = new User(FirstName = 'John', LastName = 'Doe', Username = 'john58872659836@doe-test-lsbu.com', Email = 'john@doe.com',
                Alias = 'test', LanguageLocaleKey='en_US', LocaleSidKey = 'en_US', ProfileId = profile.Id,
                TimeZoneSidKey = 'America/New_York', EmailEncodingKey='UTF-8');
        insert agentUSer;
        Account householdAccount = new Account(Name = 'Test Household');
        insert householdAccount;

        Contact testContact = new Contact(AccountId = householdAccount.Id, LastName = 'Test', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = 'Test123' , LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT);
        insert testContact;

        Id enquiryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('LSB_CAS_Enquiry').getRecordTypeId();

        Case testCase = new Case(Status = LSB_Constants.CASE_STATUS_RECEIVED, ContactId = testContact.Id, RecordTypeId = enquiryRecordTypeId, LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT);
        insert testCase;

        LiveChatVisitor tesVisitor = new LiveChatVisitor();
        insert tesVisitor;

        LiveChatTranscript transcript = new LiveChatTranscript(CaseId = testCase.Id, LiveChatVisitorId = tesVisitor.Id, Body = '<p>Test</p>');
        insert transcript;

        LSB_SurveyRedirectController.Root root = new LSB_SurveyRedirectController.Root();
        LSB_SurveyRedirectController.Agent agent = new LSB_SurveyRedirectController.Agent();
        agent.agentName = 'Test Agent Name';
        agent.userId = agentUSer.Id;
        root.agent = agent;
        String agentParam = JSON.serialize(root);

        PageReference testPage = Page.LSB_SurveyRedirectPage;
        testPage.getParameters().put(LSB_Constants.CHAT_KEY_PARAM_NAME, String.valueOf(transcript.ChatKey));
        testPage.getParameters().put(LSB_Constants.CHAT_DETAILS_PARAM_NAME, String.valueOf(agentParam));
        testPage.getParameters().put(LSB_Constants.CHAT_LAST_VISITED_PAGE_PARAM_NAME, LAST_VISITED_PAGE);
        testPage.getParameters().put(LSB_Constants.CHAT_TRANSCRIPT_PARAM_NAME, TRANSCRIPT_CONTENT_TEXT);

        Test.setCurrentPage(testPage);
    }
}