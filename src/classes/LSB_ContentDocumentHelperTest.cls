@IsTest
class LSB_ContentDocumentHelperTest {

    private static final String COMMUNITY_USER_EMAIL = 'john.snowman720275@pwc.com';
    private static final String COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME = LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME;

    @TestSetup
    static void createTestData() {
        insert new hed__Trigger_Handler__c(
            hed__Active__c = true,
            hed__Class__c = 'LSB_TriggerDispatcher',
            hed__Load_Order__c = 1,
            hed__Trigger_Action__c = 'AfterInsert',
            hed__Object__c = 'ContentDocumentLink'
        );

        insert new hed__Trigger_Handler__c(
            hed__Active__c = true,
            hed__Class__c = 'LSB_TriggerDispatcher',
            hed__Load_Order__c = 1,
            hed__Trigger_Action__c = 'BeforeDelete',
            hed__Object__c = 'ContentDocument'
        );

        Account testAccount = new Account(Name = 'Test Account Name');
        insert testAccount;
        Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
            Email = COMMUNITY_USER_EMAIL, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email',
            LSB_ExternalID__c = '1234QWERTY', AccountId = testAccount.Id, LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_STUDENT);
        insert studentContact;
        LSB_UserHelper.createCommunityUser(
            new User(
                FirstName = 'John',
                LastName = 'Snow',
                Email = COMMUNITY_USER_EMAIL
            ),
            'KMDyzfik#lMtTXN6@dl6uU'
        );

        hed__Term__c courseTerm = new hed__Term__c(
            hed__Start_Date__c = Date.today() - 2,
            hed__End_Date__c = Date.today() + 2,
            hed__Account__c = testAccount.Id
        );

        insert courseTerm;

        hed__Course__c course = new hed__Course__c(
            Name = 'Test',
            hed__Account__c = testAccount.Id
        );

        insert course;

        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
            Name = 'Test Course',
            hed__Course__c = course.Id,
            hed__Term__c = courseTerm.Id
        );

        insert courseOffering;

        hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
            LSB_PEN_LatestEnrolmentRecord__c = true,
            hed__Enrollment_Status__c = 'EFE',
            hed__Contact__c = studentContact.Id
        );

        insert programEnrollment;

        hed__Course_Enrollment__c module = new LSB_CourseEnrollmentBuilder()
            .withRecordType(COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME)
            .withProgramEnrollment(programEnrollment.Id)
            .withModuleEnrollmentStatus(LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED)
            .withCourseOffering(courseOffering.Id)
            .createAndSave();

        hed__Course_Enrollment__c coursework = new LSB_CourseEnrollmentBuilder()
            .withRecordType(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME)
            .withParentModuleComponent(module.Id)
            .withComponentType(LSB_Constants.ENROLLMENT_COMPONENT_TYPE_COURSEWORK)
            .withHandInDate(Date.today().addDays(10))
            .createAndSave();

        Case claimCase = new LSB_CaseBuilder().withRecordType(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME)
            .withRequestType(LSB_Constants.CLAIM_REQUEST_TYPE_EC)
            .withRelatedAssessment(coursework.Id)
            .withSubmitWithin5WorkingDays('Yes')
            .withRelatedModule(module.Id)
            .create();

        claimCase.LSB_CAS_ContactRole__c = 'Student';
        claimCase.LSB_CAS_NatureOfEnquiry__c = 'EC / Late submission request';
        claimCase.LSB_CAS_Topic__c = 'EC / Late submission request';

        String caseJson = JSON.serialize(claimCase);

        LSB_ECClaimService.ClaimSaveResultWrapper result = LSB_CommCreateClaimController.saveClaimCase(caseJson);

    }

    @IsTest
    static void setEcClaimEvidenceFlagOnInsert() {
        Case testEcClaim = [SELECT Id FROM Case LIMIT 1];
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );

        insert contentVersionInsert;

        ContentDocument contentDocument = [SELECT Id FROM ContentDocument LIMIT 1];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = testEcClaim.Id;
        contentDocumentLink.ContentDocumentId = contentDocument.Id;
        contentDocumentLink.Visibility = 'AllUsers';

        Test.startTest();
        insert contentDocumentLink;
        Test.stopTest();

        Case testEcClaimToAssert = [SELECT Id, LSB_CAS_EvidenceUploaded__c FROM Case LIMIT 1];

        System.assertEquals(true, testEcClaimToAssert.LSB_CAS_EvidenceUploaded__c);
    }

    @IsTest
    static void setEcClaimEvidenceFlagOnDelete() {
        Case testEcClaim = [SELECT Id FROM Case LIMIT 1];
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );

        insert contentVersionInsert;

        ContentDocument contentDocument = [SELECT Id FROM ContentDocument LIMIT 1];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = testEcClaim.Id;
        contentDocumentLink.ContentDocumentId = contentDocument.Id;
        contentDocumentLink.Visibility = 'AllUsers';

        insert contentDocumentLink;

        Test.startTest();
        delete contentDocument;
        Test.stopTest();

        Case testEcClaimToAssert = [SELECT Id, LSB_CAS_EvidenceUploaded__c FROM Case LIMIT 1];

        System.assertEquals(false, testEcClaimToAssert.LSB_CAS_EvidenceUploaded__c);
    }

}