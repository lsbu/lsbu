public with sharing class LSB_RelationshipTriggerHandler implements ITriggerHandler {
    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;

    public LSB_RelationshipTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
    }

    public void beforeInsert(List<SObject> newList) {}

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
        LSB_RelationshipHelper.createSupportArrangementSharingOnRelationshipCreationAndEdit(null, (Map<Id, hed__Relationship__c>) newItems);
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_RelationshipHelper.createSupportArrangementSharingOnRelationshipCreationAndEdit(
            (Map<Id, hed__Relationship__c>) oldItems,
            (Map<Id, hed__Relationship__c>) newItems
        );
        LSB_RelationshipHelper.deleteSupportArrangementSharingOnRelationshipDeletionAndEdit(
            (Map<Id, hed__Relationship__c>) oldItems,
            (Map<Id, hed__Relationship__c>) newItems
        );
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
        LSB_RelationshipHelper.deleteSupportArrangementSharingOnRelationshipDeletionAndEdit(
            (Map<Id, hed__Relationship__c>) oldItems,
            null
        );
    }

    public void afterUndelete(Map<Id, SObject> newItems) {}
}