public without sharing class LSB_SuccessPlanHelper {

    private static final String PRIVATE_ACCESS_TYPE = '0';
    private static final String DEFAULT_SHARE_ACCESS = 'Edit';

    public void copyValuesFromTemplate(List<sfal__SuccessPlan__c> newPlans) {
        Set<Id> templateIds = new Set<Id>();

        for (sfal__SuccessPlan__c plan : newPlans) {
            templateIds.add(plan.sfal__FromTemplate__c);
        }

        Map<Id, sfal__SuccessPlanTemplate__c> templatesMap = new Map<Id, sfal__SuccessPlanTemplate__c>(
            [SELECT Id, LSB_Type__c FROM sfal__SuccessPlanTemplate__c WHERE Id IN :templateIds]
        );

        for (sfal__SuccessPlan__c plan : newPlans) {
            if (templatesMap.containsKey(plan.sfal__FromTemplate__c)) {
                plan.LSB_Type__c = templatesMap.get(plan.sfal__FromTemplate__c).LSB_Type__c;
            }
        }
    }

    public void applyDefaultTeam(List<sfal__SuccessPlan__c> newPlans) {
        List<sfal__SuccessPlan__Share> sharingToInsert = new List<sfal__SuccessPlan__Share>();
        List<sfal__SuccessPlan__c> privatePlans = new List<sfal__SuccessPlan__c>();
        List<sfal__SuccessPlan__c> openPlans = new List<sfal__SuccessPlan__c>();
        Set<Id> userIds = new Set<Id>();

        for (sfal__SuccessPlan__c plan : newPlans) {
            userIds.add(plan.CreatedById);

            if (plan.LSB_Type__c == PRIVATE_ACCESS_TYPE) {
                privatePlans.add(plan);
            } else {
                openPlans.add(plan);
            }
        }

        Map<Id, User> createdByUsersByIds = new Map<Id, User>(
            [SELECT Id, UserRole.Name, ManagerId FROM User WHERE Id = :userIds]
        );

        sharingToInsert.addAll(processPrivatePlans(privatePlans, createdByUsersByIds));
        sharingToInsert.addAll(processOpenPlans(openPlans, createdByUsersByIds));

        if (!sharingToInsert.isEmpty()) {
            insert sharingToInsert;
        }
    }

    public void handleTypeChange(List<sfal__SuccessPlan__c> newPlans, Map<Id, sfal__SuccessPlan__c> oldPlans) {
        Map<Id, sfal__SuccessPlan__c> applyNewSharing = new Map<Id, sfal__SuccessPlan__c>();

        for (sfal__SuccessPlan__c plan : newPlans) {
            if (plan.LSB_Type__c != oldPlans.get(plan.Id).LSB_Type__c &&
                    (plan.LSB_Type__c == PRIVATE_ACCESS_TYPE ||
                    oldPlans.get(plan.Id).LSB_Type__c == PRIVATE_ACCESS_TYPE)) {
                applyNewSharing.put(plan.Id, plan);
            }
        }

        String reason = Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c;

        delete [SELECT Id
            FROM sfal__SuccessPlan__Share
            WHERE RowCause = :reason AND ParentId IN :applyNewSharing.keySet()];

        applyDefaultTeam(applyNewSharing.values());
    }

    private static List<sfal__SuccessPlan__Share> processPrivatePlans(List<sfal__SuccessPlan__c> privatePlans, Map<Id, User> createdByUsersByIds) {
        List<sfal__SuccessPlan__Share> sharingToInsert = new List<sfal__SuccessPlan__Share>();

        for (sfal__SuccessPlan__c plan : privatePlans) {
            if (createdByUsersByIds.get(plan.CreatedById).ManagerId != null) {
                sharingToInsert.add(
                        new sfal__SuccessPlan__Share(
                                AccessLevel = DEFAULT_SHARE_ACCESS,
                                ParentId = plan.Id,
                                UserOrGroupId = createdByUsersByIds.get(plan.CreatedById).ManagerId,
                                RowCause = Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c
                        ));
            }
        }

        return sharingToInsert;
    }

    private static List<sfal__SuccessPlan__Share> processOpenPlans(List<sfal__SuccessPlan__c> openPlans, Map<Id, User> createdByUsersByIds) {
        List<sfal__SuccessPlan__Share> sharingToInsert = new List<sfal__SuccessPlan__Share>();

        Set<String> teamNames = new Set<String>();
        for (sfal__SuccessPlan__c plan : openPlans) {
            String teamName = getTeamName(createdByUsersByIds.get(plan.CreatedById).UserRole.Name);
            if (String.isNotBlank(teamName)) {
                teamNames.add(teamName + '%');
            }
        }

        if (teamNames.isEmpty()) {
            return sharingToInsert;
        }

        Map<String, String> roleDeveloperName2RoleLabel = new Map<String, String>();

        for (UserRole role : [SELECT Id, Name, DeveloperName FROM UserRole WHERE Name LIKE :teamNames]) {
            roleDeveloperName2RoleLabel.put(role.DeveloperName, getTeamName(role.Name));
        }

        Map<String, List<Group>> roleTeams = new Map<String, List<Group>>();

        for (Group roleGroup : [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN :roleDeveloperName2RoleLabel.keySet() AND Type = 'Role']) {
            String teamName = roleDeveloperName2RoleLabel.get(roleGroup.DeveloperName);

            if (!roleTeams.containsKey(teamName)) {
                roleTeams.put(teamName, new List<Group>());
            }

            roleTeams.get(teamName).add(roleGroup);
        }

        for (sfal__SuccessPlan__c plan : openPlans) {
            String teamName = getTeamName(createdByUsersByIds.get(plan.CreatedById).UserRole.Name);
            for (Group team : roleTeams.get(teamName)) {
                sharingToInsert.add(
                        new sfal__SuccessPlan__Share(
                                AccessLevel = DEFAULT_SHARE_ACCESS,
                                ParentId = plan.Id,
                                UserOrGroupId = team.Id,
                                RowCause = Schema.sfal__SuccessPlan__Share.RowCause.LSB_ApexTriggerReason__c
                        ));
            }
        }

        return sharingToInsert;
    }

    private static String getTeamName(String roleName) {
        return roleName.contains(']') ? roleName.substringBeforeLast(']') + ']' : '';
    }

}