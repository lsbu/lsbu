public with sharing class LSB_CommCreateClaimController {

    private static final String NO_SCREEN_CONFIG_RECORD_ERROR = 'No LSB_EC_ScreenAssessmentConfig found. Please create valid custom metadata records.';

    @AuraEnabled
    public static LSB_ECClaimService.ClaimSaveResultWrapper saveClaimCase(String caseData) {
        try {

            Case caseToCreate = (Case) JSON.deserialize(caseData, Case.class);
            if (caseToCreate.LSB_CAS_RelatedAssessment__c != null) {
                LSB_CourseEnrollmentHelper courseEnrollmentHelper = new LSB_CourseEnrollmentHelper();
                List<hed__Course_Enrollment__c> relatedAssessments = courseEnrollmentHelper.getCourseEnrollmentById(caseToCreate.LSB_CAS_RelatedAssessment__c);
                if (!relatedAssessments.isEmpty()) {
                    LSB_ECClaimService claimService = new LSB_ECClaimService();
                    return claimService.createNewClaim(caseToCreate, relatedAssessments[0]);
                }
                return null;
            }
            return null;

        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getConfigData(String contactId) {
        Id claimRecordTypeId = LSB_Utils.getCaseRecordTypeIdByDeveloperName(LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME);
        Boolean hasActiveSupportArrangements = checkIfHasActiveSupportArrangements(contactId);

        List<LSB_ECSettings__mdt> configRecords = [SELECT LSB_RequestTypeApiName__c, LSB_RequestTypeLabel__c, LSB_SortOrder__c,
                LSB_RequestTypeDescription__c, LSB_SignpostingFirstPart__c, LSB_SignpostingSecondPart__c
                FROM LSB_ECSettings__mdt
                ORDER BY LSB_SortOrder__c ASC];
        List<Schema.PicklistEntry> picklistEntries = Case.LSB_CAS_RequestType__c.getDescribe().getPicklistValues();
        LSB_CommCreateClaimController.ClaimConfigWrapper configWrapper = createConfigWrapper(claimRecordTypeId, configRecords, picklistEntries,
                hasActiveSupportArrangements);

        return JSON.serialize(configWrapper);
    }

    private static Boolean checkIfHasActiveSupportArrangements(String contactId) {
        List<LSB_SUD_SupportArrangementDetail__c> supportArrangementDetails = [SELECT Id FROM LSB_SUD_SupportArrangementDetail__c
        WHERE LSB_SUD_Active__c = TRUE
        AND LSB_SUD_Contact__c =: contactId];
        if (supportArrangementDetails.isEmpty()) {
            return false;
        }
        return true;
    }

    public static LSB_CommCreateClaimController.ClaimConfigWrapper createConfigWrapper(Id claimRecordTypeId, List<LSB_ECSettings__mdt> configRecords,
            List<Schema.PicklistEntry> picklistEntries, Boolean hasActiveSupportArrangements) {

        List<RequestTypeConfig> configData = new List<RequestTypeConfig>();
        for (LSB_ECSettings__mdt configRecord : configRecords) {
            for (Schema.PicklistEntry picklistEntry : picklistEntries) {
                if (configRecord.LSB_RequestTypeApiName__c == picklistEntry.getValue()) {
                    configData.add(new RequestTypeConfig(
                            configRecord.LSB_RequestTypeApiName__c,
                            configRecord.LSB_RequestTypeLabel__c,
                            configRecord.LSB_RequestTypeDescription__c,
                            configRecord.LSB_SignpostingFirstPart__c,
                            configRecord.LSB_SignpostingSecondPart__c
                    ));
                }
            }
        }
         return new ClaimConfigWrapper(claimRecordTypeId, configData, hasActiveSupportArrangements);
    }

    @AuraEnabled
    public static List<LSB_CourseEnrollmentHelper.ModuleWrapper> getModuleData(String contactId, String requestType) {
        LSB_CourseEnrollmentHelper courseEnrollmentHelper = new LSB_CourseEnrollmentHelper();
        List<LSB_CourseEnrollmentHelper.ModuleWrapper> result = courseEnrollmentHelper.getModulesWithAssessments(contactId, requestType);
        return result;
    }

    @AuraEnabled
    public static LSB_EC_ScreenAssessmentConfig__mdt getECScreenAssessmentConfig(Boolean isRetrospective, Boolean isExam, Boolean isResit) {
        List<LSB_EC_ScreenAssessmentConfig__mdt> screenConfigs = [SELECT LSB_AbleToAttendExamOptions__c, LSB_AbleToSubmit5WorkingDaysOptions__c, LSB_DetailsFieldDescription__c,
                LSB_DetailsMandatory__c, LSB_IsExam__c, LSB_IsResit__c, LSB_IsRetrospective__c, LSB_ScreenTitle__c, LSB_ShowAbleToAttendExam__c, LSB_ShowAbleSubmit5WorkingDays__c,
                LSB_FirstParagraph__c, LSB_SecondParagraph__c, LSB_SingleSelectFieldDescription__c
                FROM LSB_EC_ScreenAssessmentConfig__mdt
                WHERE LSB_IsRetrospective__c =: isRetrospective AND LSB_IsExam__c =: isExam AND LSB_IsResit__c =: isResit];
        if (screenConfigs.isEmpty()) {
            throw new AuraHandledException(NO_SCREEN_CONFIG_RECORD_ERROR);
        }
        return screenConfigs[0];
    }

    class ClaimConfigWrapper {
        @AuraEnabled public Boolean hasActiveSupportArrangements { get; set; }
        @AuraEnabled public Id claimRecordTypeId { get; set; }
        @AuraEnabled public List<RequestTypeConfig> configData { get; set; }

        public ClaimConfigWrapper(Id claimRecordTypeId, List<RequestTypeConfig> configData, Boolean hasActiveSupportArrangements) {
            this.hasActiveSupportArrangements = hasActiveSupportArrangements;
            this.claimRecordTypeId = claimRecordTypeId;
            this.configData = configData;
        }
    }

    class RequestTypeConfig {
        @AuraEnabled public String value { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String description { get; set; }
        @AuraEnabled public String signpostingFirstParagraph { get; set; }
        @AuraEnabled public String signpostingSecondParagraph { get; set; }

        public RequestTypeConfig(String value, String label, String description, String signpostingFirstParagraph, String signpostingSecondParagraph) {
            this.value = value;
            this.label = label;
            this.description = description;
            this.signpostingFirstParagraph = signpostingFirstParagraph;
            this.signpostingSecondParagraph = signpostingSecondParagraph;
        }
    }
}