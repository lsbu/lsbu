/**
 * Created by pslowinski001 on 01.06.2021.
 */

public with sharing class LSB_MassAssignmentPageController {

    public ApexPages.StandardSetController setController { get; set; }
    public List<SObject> recordsList = new List<SObject>();
    public Integer recordsListSize { get; set; }

    public LSB_MassAssignmentPageController(ApexPages.StandardSetController setController) {
        System.debug('LSB_MassAssignmentPageController>>>');
        this.setController = setController;
        if (recordsList.isEmpty()) {
            this.recordsList = getAllARecordsFromListView();
        }
    }

    private List<SObject> findRecords() {
        List<SObject> records = setController.getSelected();
        System.debug('records>>>'+records.size());
        if (records.isEmpty()) {
            return new List<SObject>();
        }
        return records;
    }

    private List<SObject> getAllARecordsFromListView() {
        List<SObject> allAccounts = new List<SObject>();
        while (setController.getHasNext()) {
            allAccounts.addAll(setController.getRecords());
            setController.next();
        }
        allAccounts.addAll(setController.getRecords());
        if (allAccounts.isEmpty()) {
//            setIsAccountsEmpty(true);
        }
        this.recordsListSize = allAccounts.size();
        return allAccounts;
    }
}