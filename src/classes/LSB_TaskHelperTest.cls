@isTest
private class LSB_TaskHelperTest {
    private static final String COMMUNITY_USER_EMAIL = 'john.snowman12345@pwc.test.com';
    private static final String TEMPLATE_NAME_ONE = 'Template Name One';
    private static final String TEMPLATE_NAME_TWO = 'Template Name Two';

    @testSetup
    private static void createdData() {
        insert new hed__Trigger_Handler__c(
                hed__Active__c = true,
                hed__Class__c = 'LSB_TriggerDispatcher',
                hed__Load_Order__c = 1,
                hed__Trigger_Action__c = 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate;AfterDelete;AfterUndelete',
                hed__Object__c = 'Task'
        );

        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact student = new Contact(
                FirstName = 'John',
                LastName = 'Snowman',
                hed__AlternateEmail__c = COMMUNITY_USER_EMAIL,
                Email = COMMUNITY_USER_EMAIL,
                LSB_CON_SourceSystem__c = 'QL',
                LSB_ChannelOfPreference__c = 'Email',
                LSB_ExternalID__c = '1234QWERTY',
                AccountId = a.Id);

        insert student;

        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(),'facebook_logo','facebook_logo/jpg',
                'facebook_logo', 'facebook logo'));


        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(),'twitter_logo','twitter_logo/jpg',
                'twitter_logo', 'twitter logo'));

        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(),'instagram_logo','instagram_logo/jpg',
                'instagram_logo', 'instagram logo'));


        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(),'linkedin_logo','linkedin_logo/jpg',
                'linkedin_logo', 'linkedin logo'));


        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(),'youtube_logo','youtube_logo/jpg',
                'youtube_logo', 'youtube logo'));

        insertUser(student.Id);

        sfal__SuccessPlan__c plan = new sfal__SuccessPlan__c(Name = 'Plan 1', LSB_Type__c = '0', sfal__Advisee__c = student.Id);
        insert plan;

        createTemplates();
    }

    @future
    private static void insertUser(Id contactId) {
        User studentUser = new User(
                ContactId = contactId,
                UserName = COMMUNITY_USER_EMAIL,
                FirstName = 'John',
                LastName = 'Snowman',
                Email = COMMUNITY_USER_EMAIL,
                ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Student%' LIMIT 1].Id,
                Alias = 'snow',
                TimeZoneSidKey = UserInfo.getTimezone().getID(),
                LocaleSidKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LOCALE,
                EmailEncodingKey = LSB_CommunitySelfRegisterController.SELFREG_USER_ENCODING,
                LanguageLocaleKey = LSB_CommunitySelfRegisterController.SELFREG_USER_LANGUAGE
        );

        insert studentUser;
    }

    @isTest
    private static void testFillDataMethod() {
        sfal__SuccessPlan__c plan = [SELECT Id FROM sfal__SuccessPlan__c WHERE Name = 'Plan 1'];
        Id studentId = [SELECT Id FROM User WHERE UserName = :COMMUNITY_USER_EMAIL].Id;
        Task planTask = new Task(WhatId = plan.Id, Subject = 'Test');

        Test.startTest();

        insert planTask;

        Test.stopTest();

        planTask = [SELECT WhoId, OwnerId FROM Task WHERE Id = :planTask.Id];

        System.assert(planTask.WhoId == null);
        System.assertEquals(studentId, planTask.OwnerId);
    }

    @isTest
    private static void shouldFilterTasksToCreate() {
        List<LSB_TTE_TaskTemplate__c> testTemplates = [SELECT Id FROM LSB_TTE_TaskTemplate__c];
        User testStudentUser = [SELECT Id, ContactId FROM User WHERE UserName = :COMMUNITY_USER_EMAIL][0];
        List<Task> tasksToCreate = new List<Task>();
        for (LSB_TTE_TaskTemplate__c template : testTemplates) {
            tasksToCreate.add(createTask(testStudentUser.Id, testStudentUser.ContactId, template.Id));
            tasksToCreate.add(createTask(testStudentUser.Id, testStudentUser.ContactId, template.Id));
            tasksToCreate.add(createTask(testStudentUser.Id, testStudentUser.ContactId, template.Id));
        }


        LSB_TaskHelper.createTasks(tasksToCreate);

        List<Task> createdTasks = [SELECT Id From Task];
        System.assertEquals(testTemplates.size(), createdTasks.size());
    }

    private static Task createTask(String ownerId, String whoId, String taskTemplateId) {
        Task newTask = new Task(
                WhoId = whoId,
                OwnerId = ownerId,
                LSB_ACT_TaskTemplateId__c = taskTemplateId,
                Subject = 'Test subject'
        );

        return newTask;
    }

    private static void createTemplates() {
        List<LSB_TTE_TaskTemplate__c> templates = new List<LSB_TTE_TaskTemplate__c>();
        templates.add(createTemplateByName(TEMPLATE_NAME_ONE));
        templates.add(createTemplateByName(TEMPLATE_NAME_TWO));
        insert templates;
    }

    private static LSB_TTE_TaskTemplate__c createTemplateByName(String templateName) {
        LSB_TTE_TaskTemplate__c template = new LSB_TTE_TaskTemplate__c (
                LSB_TTE_DueDateOffset__c = 7,
                LSB_TTE_Priority__c = 'Low',
                Name = templateName
        );

        return template;
    }
}