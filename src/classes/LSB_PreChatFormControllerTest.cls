/**
 * Created by elatoszek001 on 11.12.2020.
 */

@isTest
private class LSB_PreChatFormControllerTest {

    private static final String STUDENT_USER_EMAIL = 'student12345@pwc.test.com';

    @testSetup
    static void createdData() {

        Account a = new Account(Name = 'Test Account Name Student');
        insert a;

        Contact student = new Contact(
            LastName = 'Student',
            hed__AlternateEmail__c = STUDENT_USER_EMAIL,
            Email = STUDENT_USER_EMAIL,
            LSB_CON_SourceSystem__c = 'QL',
            LSB_ChannelOfPreference__c = 'Email',
            LSB_ExternalID__c = 'QWERTY1234',
            AccountId = a.Id);

        insert student;
    }

    @isTest
    static void testGetConfigurationMethod() {
        LSB_PreChatFormController.LSB_PreChatConfiguration configuration = LSB_PreChatFormController.getConfiguration();

        System.assert(!String.isBlank(configuration.contactRoleFieldLabel));
        System.assert(!String.isBlank(configuration.natureOfEnquiryFieldLabel));
        System.assert(!String.isBlank(configuration.topicFieldLabel));
        System.assert(!String.isBlank(configuration.subjectFieldLabel));
        System.assert(configuration.picklistConfiguration != null);
    }

    @isTest
    static void testValidateUser() {
        Contact contact = LSB_PreChatFormController.validateUser(STUDENT_USER_EMAIL);
        System.assert(contact != null);
        System.assertNotEquals(contact.FirstName, null);
        System.assertNotEquals(contact.LastName,null);
        contact = LSB_PreChatFormController.validateUser('dummyemail@mail.com');
        System.assert(contact == null);
    }
}