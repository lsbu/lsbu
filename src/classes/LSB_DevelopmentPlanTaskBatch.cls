public class LSB_DevelopmentPlanTaskBatch implements Database.Batchable<SObject>, Schedulable {
    public static final String TASK_SUBJECT = LSB_Configuration__mdt.getInstance('LSB_DevelopmentPlanTaskSubject').LSB_Value__c;
    private static final String TASK_SUBTYPE = LSB_Configuration__mdt.getInstance('LSB_DevelopmentPlanTaskSubtype').LSB_Value__c;
    private static final String TASK_DESCRIPTION = Label.LSB_DevelopmentPlanTaskDescription;

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query = '';
        query += 'SELECT Id, hed__Contact__c, hed__Contact__r.LSB_CON_User__c';
        query += ' FROM hed__Program_Enrollment__c';
        query += ' WHERE LSB_PEN_DevelopmentPlanTaskToBeCreated__c = true ';
        query += ' LIMIT 8000';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<hed__Program_Enrollment__c> scope) {
        LSB_TaskHelper.insertTasksForProgramEnrollments(
            scope,
            TASK_SUBJECT,
            TASK_DESCRIPTION,
            TASK_SUBTYPE,
            'Medium',
            Date.today().addDays(28)
        );
    }

    public void finish(Database.BatchableContext BC)
    {
    }

    public void execute(SchedulableContext sc)
    {
        LSB_DevelopmentPlanTaskBatch schedulableBatch = new LSB_DevelopmentPlanTaskBatch();
        Database.executeBatch(schedulableBatch);
    }
}