@IsTest
public with sharing class LSB_CommunityLoginControllerTest {

    @IsTest
    static void shouldReturnErrorLoginMessageWhenInvalidUsernameAndPass() {
        String username = 'login@test.com';
        LSB_UserHelper.LoginResult result = LSB_CommunityLoginController.login(username, 'testPassword', null);

        System.assert(String.isNotBlank(result.loginMessage));
    }
}