@IsTest
class LSB_CommSelfAssessmentHelperTest {

    @TestSetup
    static void createTestData() {
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(), 'Footer', 'footerTitle/jpg',
            TestDataClass.footerDeveloperName, 'Footer Title'));
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(), 'facebook_logo', 'facebook_logo/jpg',
            TestDataClass.socialImageDeveloperName, 'facebook logo'));
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(), 'twitter_logo', 'twitter_logo/jpg',
            'twitter_logo', 'twitter logo'));
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(), 'instagram_logo', 'instagram_logo/jpg',
            'instagram_logo', 'instagram logo'));
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(), 'linkedin_logo', 'linkedin_logo/jpg',
            'linkedin_logo', 'linkedin logo'));
        TestDataClass.insertDocument(new TestDataClass.DocumentWrapper(UserInfo.getUserId(), 'youtube_logo', 'youtube_logo/jpg',
            'youtube_logo', 'youtube logo'));

        TestDataClass.createStudent();
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        List<LSB_SAQ_SelfAssessmentQuestion__c> selfAssessmentQuestions = new List<LSB_SAQ_SelfAssessmentQuestion__c>();

        Schema.DescribeFieldResult fieldResult = LSB_SAQ_SelfAssessmentQuestion__c.LSB_SAQ_Category__c.getDescribe();
        for (Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()) {
            selfAssessmentQuestions.add(
                new LSB_SAQ_SelfAssessmentQuestion__c(
                    LSB_SAQ_Category__c = pickListVal.getValue(),
                    LSB_SAQ_SurveyQuestionFullQuestion__c = 'example question',
                    LSB_SAQ_SortOrder__c = 10,
                    LSB_SAQ_IncludeInPersonalDevChart__c = true
                )
            );
        }

        insert selfAssessmentQuestions;

        List<LSB_SAR_SelfAssessmentResponse__c> selfAssessmentResponses = new List<LSB_SAR_SelfAssessmentResponse__c>();

        for (LSB_SAQ_SelfAssessmentQuestion__c saq : selfAssessmentQuestions) {
            selfAssessmentResponses.add(
                new LSB_SAR_SelfAssessmentResponse__c(
                    LSB_SAR_SelfAssessmentQuestion__c = saq.Id,
                    LSB_SAR_ResponseValue__c = 'example response',
                    LSB_SAR_StartDate__c = Datetime.now(),
                    LSB_SAR_Contact__c = contact.Id,
                    LSB_SAR_KnowledgePublicUrl__c = 'www.google.com',
                    LSB_SAR_IsInitialResponse__c = true
                )
            );
        }

        insert selfAssessmentResponses;

    }

    @IsTest
    static void getAssessmentWrapperDataTest() {
        Contact contact = [SELECT Id FROM Contact WHERE LSB_CON_CurrentRole__c = :LSB_Constants.CONTACT_ROLE_STUDENT LIMIT 1];
        User studentUser = [SELECT Id FROM User WHERE ContactId = :contact.Id];
        Schema.DescribeFieldResult fieldResult = LSB_SAQ_SelfAssessmentQuestion__c.LSB_SAQ_Category__c.getDescribe();
        Set<String> picklistValues = new Set<String>();
        for (PicklistEntry pe : fieldResult.getPicklistValues()) {
            picklistValues.add(pe.getValue());
        }
        System.runAs(studentUser) {
            Test.startTest();
            LSB_CommSelfAssessmentHelper.SelfAssessmentWrapper sarw = LSB_CommSelfAssessmentHelper.getAssessmentWrapperData();
            Test.stopTest();
            System.assertEquals(fieldResult.getPicklistValues().size() + 1, sarw.responseData.size());
            for (String category : sarw.responseData.keySet()) {
                if (picklistValues.contains(category)) {

                    System.assertEquals(1, sarw.responseData.get(category).size());
                    System.assertEquals(true, sarw.responseData.get(category)[0].sectionItems[0].LSB_SAR_IsCurrent__c);
                }
            }
        }
    }

    @IsTest
    static void getTabNamesFromPicklistValuesTest() {
        Set<String> categories = LSB_CommSelfAssessmentHelper.getTabNamesFromPicklistValues();
        System.assertNotEquals(true, categories.isEmpty());
    }

    @IsTest
    static void fetchAssessmentDataTest() {
        Contact contact = [SELECT Id FROM Contact WHERE LSB_CON_CurrentRole__c = :LSB_Constants.CONTACT_ROLE_STUDENT LIMIT 1];
        User studentUser = [SELECT Id FROM User WHERE ContactId = :contact.Id];
        System.runAs(studentUser) {
            String jsonString = LSB_CommSelfAssessmentController.fetchAssessmentData();
            System.assertNotEquals(null, jsonString);
        }
    }

    @IsTest
    static void handleSaveResponseTest() {
        Contact contact = [SELECT Id FROM Contact WHERE LSB_CON_CurrentRole__c = :LSB_Constants.CONTACT_ROLE_STUDENT LIMIT 1];
        User studentUser = [SELECT Id FROM User WHERE ContactId = :contact.Id];
        LSB_SAR_SelfAssessmentResponse__c selfAssessmentResponse = [SELECT Id, LSB_SAR_SelfAssessmentQuestion__c, LSB_SAR_ResponseValue__c FROM LSB_SAR_SelfAssessmentResponse__c WHERE LSB_SAR_IsCurrent__c = TRUE LIMIT 1];
        selfAssessmentResponse.LSB_SAR_ResponseValue__c = 'update response';
        System.runAs(studentUser) {
            Test.startTest();
            LSB_CommSelfAssessmentController.handleSaveResponses(new List<LSB_SAR_SelfAssessmentResponse__c>{
                selfAssessmentResponse
            });
            Test.stopTest();
        }
        List<LSB_SAR_SelfAssessmentResponse__c> selfAssessmentResponses = [
            SELECT Id, LSB_SAR_IsCurrent__c
            FROM LSB_SAR_SelfAssessmentResponse__c
            WHERE LSB_SAR_SelfAssessmentQuestion__c = :selfAssessmentResponse.LSB_SAR_SelfAssessmentQuestion__c
        ];
        System.assertEquals(2, selfAssessmentResponses.size());
    }


}