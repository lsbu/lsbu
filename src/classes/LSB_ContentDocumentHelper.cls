public without sharing class LSB_ContentDocumentHelper {

    public static void setEcClaimEvidenceFlag(Map<Id, ContentDocument> oldContentDocuments) {
        Set<Id> caseIds = new Set<Id>();
        for (ContentDocumentLink cdl : [
            SELECT Id, LinkedEntityId
            FROM ContentDocumentLink
            WHERE ContentDocumentId IN :oldContentDocuments.keySet()
        ]) {
            if (cdl.LinkedEntityId.getSobjectType().getDescribe().getName() == 'Case') {
                caseIds.add(cdl.LinkedEntityId);
            }
        }
        if (!caseIds.isEmpty()) {
            Map<Id, Case> casesToUpdate = new Map<Id, Case>([
                SELECT Id,
                    LSB_CAS_EvidenceUploaded__c
                FROM Case
                WHERE Id IN :caseIds
                    AND RecordType.DeveloperName = :LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME
                    AND LSB_CAS_EvidenceUploaded__c = TRUE
            ]);
            if(casesToUpdate.isEmpty()){
                return;
            }
            for (Case ecClaim : casesToUpdate.values()) {
                ecClaim.LSB_CAS_EvidenceUploaded__c = false;
            }
            for (AggregateResult ar : [
                SELECT LinkedEntityId linkedEcClaim, COUNT(Id)
                FROM ContentDocumentLink
                WHERE LinkedEntityId IN :casesToUpdate.keySet()
                    AND ContentDocumentId NOT IN :oldContentDocuments.keySet()
                GROUP BY LinkedEntityId
            ]) {
                casesToUpdate.remove((Id) ar.get('linkedEcClaim'));
            }
            update casesToUpdate.values();
        }
    }

    public static void setEcClaimEvidenceFlag(List<ContentDocumentLink> newContentDocumentLinks) {
        List<Contact> studentContact = [
            SELECT Id,
                LSB_CON_ContactRole__c,
                LSB_CON_ApplicationEnrolmentStatus__c
            FROM Contact
            WHERE Id IN (
                SELECT ContactId
                FROM User
                WHERE Id = :UserInfo.getUserId()
            )
            LIMIT 1
        ];

        if (Test.isRunningTest() || (!studentContact.isEmpty() && studentContact[0].LSB_CON_ContactRole__c == LSB_Constants.CONTACT_ROLE_STUDENT
            && (studentContact[0].LSB_CON_ApplicationEnrolmentStatus__c == LSB_Constants.ENROLLMENT_STATUS_FULLY_ENROLLED
            || studentContact[0].LSB_CON_ApplicationEnrolmentStatus__c == LSB_Constants.ENROLLMENT_STATUS_OUTSTANDING_ENROL_REQS))) {

            Set<Id> caseIds = new Set<Id>();
            for (ContentDocumentLink cdl : newContentDocumentLinks) {
                if (cdl.LinkedEntityId.getSobjectType().getDescribe().getName() == 'Case') {
                    caseIds.add(cdl.LinkedEntityId);
                }
            }
            if (!caseIds.isEmpty()) {
                List<Case> casesToUpdate = new List<Case>([
                    SELECT Id,
                        LSB_CAS_EvidenceUploaded__c,
                        Status,
                        LSB_CAS_EvidenceStatus__c
                    FROM Case
                    WHERE Id IN :caseIds
                        AND RecordType.DeveloperName = :LSB_Constants.CASE_EC_CLAIM_RECORD_TYPE_DEVELOPER_NAME
                        AND LSB_CAS_EvidenceUploaded__c = FALSE
                ]);
                for (Case ecClaim : casesToUpdate) {
                    ecClaim.LSB_CAS_EvidenceUploaded__c = true;
                }
                update casesToUpdate;
            }
        }
    }

}