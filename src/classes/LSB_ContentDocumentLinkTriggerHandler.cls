public without sharing class LSB_ContentDocumentLinkTriggerHandler implements ITriggerHandler {
    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;

    public LSB_ContentDocumentLinkTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
    }

    public void beforeInsert(List<SObject> newList) {
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {
    }

    public void afterInsert(Map<Id, SObject> newItems) {
        LSB_ContentDocumentHelper.setEcClaimEvidenceFlag((List<ContentDocumentLink>) newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
    }

    public void afterUndelete(Map<Id, SObject> newItems) {
    }
}