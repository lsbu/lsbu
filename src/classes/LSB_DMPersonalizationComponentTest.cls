/**
 * Created by kgromek002 on 05.08.2021.
 */
@IsTest
class LSB_DMPersonalizationComponentTest {
    private static final string USERNAME = 'testuser@testuserlsbu.com' ;
    private static final string CHILD_GROUP_NAME = 'Test Group Child';
    private static final string PARENT_GROUP_NAME = 'Test_Group_PARENT';
    @TestSetup
    static void insertTestData() {


        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User testUser = new User(Alias = 'testu', Email = 'testuser@testuser.com', LastName = 'Testing',
                ProfileId = p.Id, Username = USERNAME,EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles');
        insert testUser;
        
        Group testGroupParent = new Group(Name  = PARENT_GROUP_NAME);
        insert testGroupParent;


        Group testGroupChild = new Group(Name = CHILD_GROUP_NAME);
        insert testGroupChild;

        GroupMember testGroupMemberUser = new GroupMember(
                GroupId = testGroupChild.Id,
                UserOrGroupId = testUser.Id
        );

        insert testGroupMemberUser;


        GroupMember testGroupMemberGroup = new GroupMember(
                GroupId = testGroupParent.Id,
                UserOrGroupId = testGroupChild.Id
        );

        insert testGroupMemberGroup;


    }
        @IsTest
        static void testUserFetch(){



        Test.startTest();
        List<LSB_DMPersonalizationComponentController.GroupWrapper> distributedMarketingGroups = LSB_DMPersonalizationComponentController.getAllUsersFromChildGroups(PARENT_GROUP_NAME);
        Test.stopTest();

         Boolean isGroupFound = false;

         for(LSB_DMPersonalizationComponentController.GroupWrapper gW : distributedMarketingGroups) {
            if (gW.name == CHILD_GROUP_NAME) {
                isGroupFound = true;
                system.assertEquals(USERNAME, gW.userList[0].Username);
            }
            system.assert(isGroupFound);
         }

        }
    }