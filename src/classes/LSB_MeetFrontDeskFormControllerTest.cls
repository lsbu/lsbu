@isTest
private class LSB_MeetFrontDeskFormControllerTest {

    private static final String TEST_STUDENT_ID = '123123123';
    private static final String ALL_INTERNAL_USERS = 'All Internal Users';

    @testSetup
    private static void loadData() {
        Account a = new Account(Name='Test Account Name');
        insert a;

        List<Contact> students = new List<Contact> {
            new Contact(FirstName = 'Antonina', LastName = 'Kowalska', Email = 'antosia@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1212121212', AccountId = a.Id),
            new Contact(FirstName = 'Boleslaw', LastName = 'Chrobry', Email = 'bolek@chrobry.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1212121213', AccountId = a.Id),
            new Contact(FirstName = 'Jadwiga', LastName = 'Jagiello', Email = 'jagiello@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1212121214', AccountId = a.Id),
            new Contact(FirstName = 'Wilhelm', LastName = 'Grimm', Email = 'will.grimm@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = TEST_STUDENT_ID, AccountId = a.Id),
            new Contact(FirstName = 'John', LastName = 'Snowman', Email = 'john@snowman.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '12341234123', AccountId = a.Id)

        };

        insert students;

        RecordType advisingQueueRT = [
                SELECT Id
                FROM RecordType
                WHERE DeveloperName = :LSB_Constants.CASE_ADVISING_QUEUE_RECORD_TYPE_DEVELOPER_NAME];

        sfal__Topic__c advisingQueueTopic = new sfal__Topic__c(Name = LSB_Constants.TOPIC_SLC_HELP_DESC_NAME, LSB_TIC_AppointmentSharingSetting__c = ALL_INTERNAL_USERS);
        insert advisingQueueTopic;

        List<Case> existingAdvisingQueueCases = new List<Case> {
            new Case(ContactId = students.get(0).Id, RecordTypeId = advisingQueueRT.Id, sfal__Topic__c = advisingQueueTopic.Id, Origin = LSB_Constants.CASE_ORIGIN_SLC_HELP_DESK),
            new Case(ContactId = students.get(1).Id, RecordTypeId = advisingQueueRT.Id, sfal__Topic__c = advisingQueueTopic.Id, Origin = LSB_Constants.CASE_ORIGIN_SLC_HELP_DESK),
            new Case(ContactId = students.get(2).Id, RecordTypeId = advisingQueueRT.Id, sfal__Topic__c = advisingQueueTopic.Id, Origin = LSB_Constants.CASE_ORIGIN_SLC_HELP_DESK)
        };

        insert existingAdvisingQueueCases;
    }

    @isTest
    private static void testStudentIdSuccessValidation() {
        Contact student = null;

        Test.startTest();
        try {
            student = LSB_MeetFrontDeskFormController.fetchStudentData(TEST_STUDENT_ID);
        } catch (Exception ex) {
            System.assert(false, 'Problem with student id validation');
        }
        Test.stopTest();

        System.assert(student != null);
    }

    @isTest
    private static void testStudentIdFailedValidation() {
        Boolean isError = false;

        Test.startTest();
        try {
            LSB_MeetFrontDeskFormController.fetchStudentData('dummyvalue123');
        } catch (Exception ex) {
            isError = true;
        }
        Test.stopTest();

        System.assert(isError);
    }

    @isTest
    private static void testFetchTotalInQueue() {
        Integer total = 0;

        Test.startTest();
        total = LSB_MeetFrontDeskFormController.fetchTotalInQueue();
        Test.stopTest();

        System.assertEquals(3, total);
    }

    @isTest
    private static void testCreateAdvisingQueueCase() {
        Boolean hasError = false;
        Contact student = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE LSB_ExternalID__c = :TEST_STUDENT_ID];
        Integer casesCountBefore = [SELECT count() FROM Case WHERE ContactId = :student.Id];

        Test.startTest();
        hasError = LSB_MeetFrontDeskFormController.createAdvisingQueueCase(
            student.Id,
            student.FirstName,
            student.LastName,
            student.Email
        );
        Test.stopTest();

        System.assert(!hasError);

        Integer casesCountAfter = [SELECT count() FROM Case WHERE ContactId = :student.Id];
        System.assertEquals(casesCountBefore + 1, casesCountAfter);
    }
}