@IsTest
class LSB_PreviousYearResultsControllerTest {

    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';

    @TestSetup
    static void createTestData() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Account accountRecord = new Account(Name = 'Administrative Test Account');
            insert accountRecord;

            Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
            insert studentContact;

            Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];

            User studentUser = new User(
                Alias = 'jsnow',
                Email = studentContact.Email,
                EmailEncodingKey = 'UTF-8',
                LastName = studentContact.LastName,
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_GB',
                ProfileId = studentProfile.Id,
                ContactId = studentContact.Id,
                TimeZoneSidKey = 'Europe/London',
                UserName = STUDENT_USERNAME);

            insert studentUser;

            hed__Term__c courseTerm = new hed__Term__c(
                hed__Start_Date__c = Date.today() - 2,
                hed__End_Date__c = Date.today() + 2,
                hed__Account__c = accountRecord.Id
            );

            insert courseTerm;

            hed__Course__c course = new hed__Course__c(
                Name = 'Test',
                hed__Account__c = accountRecord.Id
            );

            insert course;

            hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
                Name = 'Test Course',
                hed__Course__c = course.Id,
                hed__Term__c = courseTerm.Id
            );

            insert courseOffering;

            hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
                LSB_PEN_LatestEnrolmentRecord__c = false,
                hed__Enrollment_Status__c = 'EFE',
                hed__Contact__c = studentContact.Id,
                LSB_PEN_OverallCourseGrade__c = 'P',
                LSB_PEN_Term__c = courseTerm.Id
            );

            insert programEnrollment;

            Id moduleEnrollmentRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'LSB_Module' AND SobjectType = 'hed__Course_Enrollment__c'].Id;

            hed__Course_Enrollment__c courseEnrollment = new hed__Course_Enrollment__c(
                hed__Contact__c = studentContact.Id,
                hed__Course_Offering__c = courseOffering.Id,
                hed__Program_Enrollment__c = programEnrollment.Id,
                LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                RecordTypeId = moduleEnrollmentRecordTypeId
            );

            insert courseEnrollment;

            hed__Term_Grade__c termGrade = new hed__Term_Grade__c(
                hed__Contact__c = studentContact.Id,
                hed__Course_Connection__c = courseEnrollment.Id,
                hed__Term__c = courseTerm.Id,
                hed__Course_Offering__c = courseOffering.Id,
                LSB_TDE_ModuleGrade__c = 'P',
                LSB_TDE_ModuleMarks__c = '10',
                LSB_TDE_Attempt__c = '5',
                LSB_TDE_CourseWork1__c = '20',
                LSB_TDE_CourseWork2__c = '30',
                LSB_TDE_CourseWork3__c = '15',
                LSB_TDE_CourseWork4__c = '90',
                LSB_TDE_CourseWork5__c = '10',
                LSB_TDE_CourseWork6__c = '100',
                LSB_TDE_Exam1__c = '10',
                LSB_TDE_Exam2__c = '20',
                LSB_TDE_Exam3__c = '30',
                LSB_TDE_Exam4__c = '40',
                LSB_TDE_Exam5__c = '60'
            );

            insert termGrade;
        }
    }

    @IsTest
    static void getPreviousYearResultsTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME LIMIT 1];
        List<LSB_PreviousYearResultsController.AcademicYearTabWrapper> academicYearTabWrappers;
        System.runAs(studentUser) {
            Test.startTest();
            academicYearTabWrappers = LSB_PreviousYearResultsController.getPreviousYearResults();
            Test.stopTest();
        }
        hed__Program_Enrollment__c pe = [SELECT LSB_PEN_OverallCourseGrade__c FROM hed__Program_Enrollment__c LIMIT 1];
        hed__Term_Grade__c tg = [
            SELECT Id,
                hed__Course_Connection__r.LSB_CCN_ModuleInstanceName__c,
                LSB_TDE_ModuleGrade__c,
                LSB_TDE_ModuleMarks__c,
                LSB_TDE_Attempt__c,
                LSB_TDE_Semester__c,
                LSB_TDE_Resit__c,
                LSB_TDE_ModuleCredit__c,
                LSB_TDE_CourseWork1__c,
                LSB_TDE_CourseWork2__c,
                LSB_TDE_CourseWork3__c,
                LSB_TDE_CourseWork4__c,
                LSB_TDE_CourseWork5__c,
                LSB_TDE_CourseWork6__c,
                LSB_TDE_Exam1__c,
                LSB_TDE_Exam2__c,
                LSB_TDE_Exam3__c,
                LSB_TDE_Exam4__c,
                LSB_TDE_Exam5__c
            FROM hed__Term_Grade__c
            LIMIT 1
        ];
        System.assertEquals(1, academicYearTabWrappers.size());
        System.assertEquals(pe.LSB_PEN_OverallCourseGrade__c, academicYearTabWrappers[0].overallCourseGrade);
        System.assertEquals(1, academicYearTabWrappers[0].moduleWrappers.size());
        System.assertEquals(true, academicYearTabWrappers[0].moduleWrappers[0].outcome);
        System.assertEquals(tg.LSB_TDE_Attempt__c, academicYearTabWrappers[0].moduleWrappers[0].attempts);
        System.assertEquals(tg.LSB_TDE_ModuleCredit__c, Decimal.valueOf(academicYearTabWrappers[0].moduleWrappers[0].credits));
        System.assertEquals('No', academicYearTabWrappers[0].moduleWrappers[0].resit);
        System.assertEquals(11, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams.size());
        System.assertEquals(tg.LSB_TDE_CourseWork1__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[0].grade);
        System.assertEquals(tg.LSB_TDE_CourseWork2__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[1].grade);
        System.assertEquals(tg.LSB_TDE_CourseWork3__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[2].grade);
        System.assertEquals(tg.LSB_TDE_CourseWork4__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[3].grade);
        System.assertEquals(tg.LSB_TDE_CourseWork5__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[4].grade);
        System.assertEquals(tg.LSB_TDE_CourseWork6__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[5].grade);
        System.assertEquals(tg.LSB_TDE_Exam1__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[6].grade);
        System.assertEquals(tg.LSB_TDE_Exam2__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[7].grade);
        System.assertEquals(tg.LSB_TDE_Exam3__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[8].grade);
        System.assertEquals(tg.LSB_TDE_Exam4__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[9].grade);
        System.assertEquals(tg.LSB_TDE_Exam5__c, academicYearTabWrappers[0].moduleWrappers[0].courseworkAndExams[10].grade);


    }

}