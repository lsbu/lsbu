@IsTest
private class LSB_MyModulesTest {

    private static final String STUDENT_USERNAME = 'mr.student@lsbu.com';

    @TestSetup
    private static void setup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Account accountRecord = new Account(Name = 'Administrative Test Account');
            insert accountRecord;

            Contact studentContact = new Contact(FirstName = 'John', LastName = 'Snow', hed__AlternateEmail__c = 'john.snow@gmail.com', Email = 'john.snow@gmail.com', LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = accountRecord.Id);
            insert studentContact;

            Profile studentProfile = [SELECT Id FROM Profile WHERE Name = :LSB_Constants.STUDENT_PROFILE_NAME];

            User studentUser = new User(
                    Alias = 'jsnow',
                    Email = studentContact.Email,
                    EmailEncodingKey = 'UTF-8',
                    LastName = studentContact.LastName,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_GB',
                    ProfileId = studentProfile.Id,
                    ContactId = studentContact.Id,
                    TimeZoneSidKey = 'Europe/London',
                    UserName = STUDENT_USERNAME);

            insert studentUser;

            hed__Term__c courseTerm = new hed__Term__c(
                    hed__Start_Date__c = Date.today() - 2,
                    hed__End_Date__c = Date.today() + 2,
                    hed__Account__c = accountRecord.Id
            );

            insert courseTerm;

            hed__Course__c course = new hed__Course__c(
                    Name = 'Test',
                    hed__Account__c = accountRecord.Id
            );

            insert course;

            hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(
                    Name = 'Test Course',
                    hed__Course__c = course.Id,
                    hed__Term__c = courseTerm.Id
            );

            insert courseOffering;

            hed__Program_Enrollment__c programEnrollment = new hed__Program_Enrollment__c(
                    LSB_PEN_LatestEnrolmentRecord__c = true,
                    hed__Enrollment_Status__c = 'EFE',
                    hed__Contact__c = studentContact.Id
            );

            insert programEnrollment;

            Id moduleEnrollmentRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'LSB_Module' AND SobjectType = 'hed__Course_Enrollment__c'].Id;

            hed__Course_Enrollment__c courseEnrollment = new hed__Course_Enrollment__c(
                    hed__Contact__c = studentContact.Id,
                    hed__Course_Offering__c = courseOffering.Id,
                    hed__Program_Enrollment__c = programEnrollment.Id,
                    LSB_CCN_ModuleEnrolmentStatus__c = 'EFE',
                    RecordTypeId = moduleEnrollmentRecordTypeId
            );

            insert courseEnrollment;
        }
    }

    @IsTest
    private static void getMyModulesNamesTest() {
        User studentUser = [SELECT Id FROM User WHERE Username = :STUDENT_USERNAME];

        System.runAs(studentUser) {
            Test.startTest();
            List<String> results = LSB_MyModules.getMyModulesNames();
            Test.stopTest();

            System.assertEquals(1, results.size());
            System.assertEquals('Test Course', results[0]);
        }
    }
}