public with sharing class LSB_CaseArticlesController {

    @AuraEnabled(Cacheable=true)
    public static List<Knowledge__kav> fetchKnowledgeArticleVersions(String caseId) {
        return [
            SELECT Id, Title
            FROM Knowledge__kav
            WHERE Id IN (
                SELECT KnowledgeArticleVersionId
                FROM CaseArticle
                WHERE CaseId = :caseId
            )
        ];
    }

}