public inherited sharing class LSB_ContactSelector {

    /**
     * @description Method using to fetch Contact records from database based on the given email.
     * @param emails Set of emails.
     * @return Map of email to contact.
     */
    public static Map<String, Contact> fetchContactsByEmails(Set<String> emails) {
        Map<String, Contact> email2contact = new Map<String, Contact>();

        for (Contact contactRecord : [
                SELECT Id, FirstName, LastName, Email, LSB_CON_ContactRole__c,
                        hed__AlternateEmail__c, hed__UniversityEmail__c
                FROM Contact
                WHERE hed__AlternateEmail__c LIKE :emails OR hed__UniversityEmail__c LIKE :emails]) {
            if (String.isNotBlank(contactRecord.hed__AlternateEmail__c)) {
                email2contact.put(contactRecord.hed__AlternateEmail__c.replaceAll('@', '\\.'), contactRecord);
            }

            if (String.isNotBlank(contactRecord.hed__UniversityEmail__c)) {
                email2contact.put(contactRecord.hed__UniversityEmail__c.replaceAll('@', '\\.'), contactRecord);
            }
        }

        return email2contact;
    }

    /**
     * @description Method using to fetch Contact records from database based on the given contact data.
     * @param contactData List of data.
     * @return Map of contact data id to contact.
     */
    public static Map<String, Contact> fetchContactsByContactData(List<ContactData> contactData) {
        Map<String, Contact> contactDataId2contact = new Map<String, Contact>();
        Set<String> contactDataIds = new Set<String>();
        Set<String> lastNames = new Set<String>();

        for (ContactData data : contactData) {
            contactDataIds.add(data.id);
            lastNames.add(data.lastName);
        }

        for (Contact contactRecord : [
                SELECT Id, FirstName, LastName, Email, LSB_CON_ContactRole__c,
                        hed__AlternateEmail__c, hed__UniversityEmail__c, MobilePhone,
                        LSB_CON_ContactDataId__c
                FROM Contact
                WHERE LSB_CON_ContactDataId__c IN :contactDataIds AND MobilePhone != NULL AND LastName IN :lastNames]) {
            if (contactDataIds.contains(contactRecord.LSB_CON_ContactDataId__c)) {
                contactDataId2contact.put(contactRecord.LSB_CON_ContactDataId__c, contactRecord);
            }
        }

        return contactDataId2contact;
    }

    /**
     * @description Wrapper class for contact data.
     */
    public class ContactData {
        public String mobilePhone { public get; private set; }
        public String firstName { public get; private set; }
        public String lastName { public get; private set; }
        public String email { public get; private set; }
        public final String id { public get; private set; }

        public ContactData(String firstName, String lastName, String mobilePhone, String email) {
            this.mobilePhone = mobilePhone;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;

            if (this.mobilePhone != null) {
                String formatedMobilePhone = String.valueOf(this.mobilePhone);

                if (formatedMobilePhone.containsWhitespace()) {
                    formatedMobilePhone = formatedMobilePhone.replaceAll(' ', '');
                }

                if (formatedMobilePhone.contains('-')) {
                    formatedMobilePhone = formatedMobilePhone.replaceAll('-', '');
                }

                if (formatedMobilePhone.contains(')')) {
                    formatedMobilePhone = formatedMobilePhone.replaceAll(')', '');
                }

                if (formatedMobilePhone.contains('(')) {
                    formatedMobilePhone = formatedMobilePhone.replaceAll('(', '');
                }

                if (formatedMobilePhone.contains('+')) {
                    formatedMobilePhone = formatedMobilePhone.replaceAll('\\+', '00');
                }

                this.id = formatedMobilePhone + '#' +
                        this.firstName + '#' + this.lastName;
            } else {
                this.id = '';
            }


        }

        /**
         * @description It is using to create Contact record. It is not doing any operation on the database.
         * @return Contact record.
         */
        public Contact returnContactRecord() {
            Contact record = new Contact(
                FirstName = this.firstName,
                LastName = this.lastName,
                Email = this.email,
                LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT
            );

            if (this.mobilePhone != null) {
                record.MobilePhone = this.mobilePhone;
                record.hed__PreferredPhone__c = LSB_Constants.CONTACT_PREFERRED_PHONE_MOBILE;
            }

            return record;
        }
    }

}