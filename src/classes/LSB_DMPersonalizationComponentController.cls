global with sharing class LSB_DMPersonalizationComponentController implements mcdm_15.BulkSendPlugin {

    global static Map<String, Map<String, String>> getBulkSendPluginData(List<mcdm_15.JourneyApproval> approvals, Map<String, String> pluginData) {
        Map<String, Map<String, String>> IdsToCustomData = new Map<String, Map<String, String>>();

        if (pluginData.size() > 0) {
            for (mcdm_15.JourneyApproval approval : approvals) {
                IdsToCustomData.put(approval.objectId, pluginData);
            }
        }

        return IdsToCustomData;
    }

    @AuraEnabled
    public static List<GroupWrapper> getAllUsersFromChildGroups(String parentGroupName)  {
        Set<Id> usersIds   = new Set<Id>();

        List<Group> distributedMarketingGroups = [
                SELECT Id, Name, (SELECT UserOrGroupId,GroupId FROM GroupMembers)
                FROM Group
                WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName  = :parentGroupName)];

        List<GroupWrapper> groupUser = new List<GroupWrapper>();

        for (Group g :distributedMarketingGroups) {
              for (GroupMember groupUsers : g.GroupMembers) {
                  usersIds.add(groupUsers.UserOrGroupId);
              }
        }

        Map<Id, User> users = new Map<Id, User> ([SELECT Email,Name, Username FROM User WHERE Id IN :usersIds]);

        for(Group g :distributedMarketingGroups) {
            List<User> userList = new List<User>();

            for (GroupMember gM : g.GroupMembers) {
                if (users.containsKey(gM.UserOrGroupId)) {
                    userList.add(users.get(gM.UserOrGroupId));
                }
            }

            groupUser.add(new GroupWrapper(g.Name, userList));
        }


        return groupUser;
    }

    @TestVisible
    private class GroupWrapper {
        @AuraEnabled
        public String name { get; set; }

        @AuraEnabled
        public List<User> userList { get; set; }

        public GroupWrapper(String name, List<User> users) {
            this.name = name;
            this.userList = users;
        }
    }
}