global virtual class LSB_InboundSocialPostHandler implements Social.InboundSocialPostHandler {

    final static Integer CONTENT_MAX_LENGTH = SocialPost.Content.getDescribe().getLength();
    final static Integer SUBJECT_MAX_LENGTH = Case.Subject.getDescribe().getLength();
    final static String SOCIAL_STUDIO_RECORD_TYPE_NAME = 'Social Studio';
    Boolean isNewCaseCreated = false;

// If true, use the active case assignment rule if one is found
    global virtual Boolean getUsingCaseAssignmentRule() {
        return false;
    }


    global virtual String getCaseSubject(SocialPost post, String personaCurrentRole) {
        Set<String> postTags = getPostTags(post);
        Map<String, String> tagsToTopicPicklistValues = getPostLabelsMapping();

        String caseSubject = '';
        for (String postTag : postTags) {
            if (tagsToTopicPicklistValues.keySet().contains(postTag)) {
                caseSubject = tagsToTopicPicklistValues.get(postTag);
                break;
            }
        }

        if(caseSubject == ''){
            if(!String.isBlank(personaCurrentRole)) {
                caseSubject = tagsToTopicPicklistValues.get(personaCurrentRole);
            } else {
                caseSubject = 'Other Student Services';
            }
        }
        return caseSubject;
    }

    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        Social.InboundSocialPostResult result = new Social.InboundSocialPostResult();
        result.setSuccess(true);
        matchPost(post);
        matchPersona(persona);

        if ((post.Content != null) && (post.Content.length() > CONTENT_MAX_LENGTH)) {
            post.Content = post.Content.abbreviate(CONTENT_MAX_LENGTH);
        }

        if (post.Id != null) {
            handleExistingPost(post, persona);
            return result;
        }

        setReplyTo(post, persona);
        upsert post;
        buildPersona(persona, post);
        Case parentCase = buildParentCase(post, persona, rawData);
        setRelationshipsOnPost(post, persona, parentCase);
        setModeration(post, rawData);
        update post;

        if (isNewCaseCreated) {
            updateCaseSource(post, parentCase);
        }

        return result;
    }

    private void setModeration(SocialPost post, Map<String, Object> rawData) {
//if we don't automatically create a case, we should flag the post as requiring moderator review.
        if (post.parentId == null && !isUnsentParent(rawData))
            post.reviewedStatus = 'Needed';
    }

    private void updateCaseSource(SocialPost post, Case parentCase) {
        if (parentCase != null) {
            parentCase.SourceId = post.Id;
//update as a new sobject to prevent undoing any changes done by insert triggers
            update new Case(Id = parentCase.Id, SourceId = parentCase.SourceId);
        }

    }

    private void handleExistingPost(SocialPost post, SocialPersona persona) {
        List<SocialPost> existingPosts = [Select Recipient, IsOutbound from SocialPost where id = :post.Id limit 1];

// for any existing outbound post, we don't overwrite its recipient field
        if (!existingPosts.isEmpty() && existingPosts[0].IsOutBound == true && String.isNotBlank(existingPosts[0].Recipient)) {
            post.Recipient = existingPosts[0].Recipient;
        }

        update post;
        if (persona.id != null)
            updatePersona(persona);
    }

    private void setReplyTo(SocialPost post, SocialPersona persona) {
        SocialPost replyTo = findReplyTo(post, persona);
        if (replyTo.id != null) {
            post.replyToId = replyTo.id;
            post.replyTo = replyTo;
        }
    }

    private SocialPersona buildPersona(SocialPersona persona, SocialPost post) {
        if (persona.Id == null)
            createPersona(persona, post); else
                updatePersona(persona);

        return persona;
    }

    private void updatePersona(SocialPersona persona) {
        try {
            if (socialPersonaShouldBeUpdated(persona)) {
                update persona;
            }
        } catch (Exception e) {
            System.debug('Error updating social persona: ' + e.getMessage());
        }
    }

    private Case buildParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        if (!isUnsentParent(rawData)) {
            Case parentCase = findParentCase(post, persona);
            if (parentCase != null) {
                if (!parentCase.IsClosed) {
                    return parentCase;
                } else if (caseShouldBeReopened(parentCase)) {
                    reopenCase(parentCase);
                    return parentCase;
                }
            }
            if (shouldCreateCase(post, rawData)) {
                isNewCaseCreated = true;
                return createCase(post, persona);
            }
        }

        return null;
    }

    private boolean caseShouldBeReopened(Case c) {
        return c.id != null && c.isClosed && c.closedDate != null;
    }

    private boolean socialPersonaShouldBeUpdated(SocialPersona persona) {
//Do not update if persona was updated within a day because SocialPersona doesn't change often
        SocialPersona p = [SELECT Id, LastModifiedDate FROM SocialPersona WHERE Id = :persona.Id LIMIT 1];
        return !((p.LastModifiedDate != null) && (System.now() < p.LastModifiedDate.addDays(1)));
    }

    private void setRelationshipsOnPost(SocialPost postToUpdate, SocialPersona persona, Case parentCase) {
        if (persona.Id != null) {
            postToUpdate.PersonaId = persona.Id;

            if (persona.ParentId.getSObjectType() != SocialPost.sObjectType) {
                postToUpdate.WhoId = persona.ParentId;
            }
        }
        if (parentCase != null) {
            postToUpdate.ParentId = parentCase.Id;
        }
    }

    private Case createCase(SocialPost post, SocialPersona persona) {
        Contact personaContact;
        Case newCase = new Case();
        if (persona != null && persona.ParentId != null) {
            if (persona.ParentId.getSObjectType() == Contact.sObjectType) {
                newCase.ContactId = persona.ParentId;
                personaContact = getPersonaContactDetails(persona);
            } else if (persona.ParentId.getSObjectType() == Account.sObjectType) {
                newCase.AccountId = persona.ParentId;
            }
        }
        if (post != null && post.Provider != null) {
            newCase.Origin = post.Provider;
        }


        newCase.RecordTypeId = getCaseRecordTypeId();
        newCase.Origin = 'Social Studio';
        newCase.Status = 'Received';
        newCase.LSB_CAS_ClosedReason__c = '';
        newCase.Description = getNewCaseDescription(post, persona);
        if(personaContact != null) {
            newCase.LSB_CAS_ContactRole__c = personaContact.LSB_CON_CurrentRole__c;
        }

        String caseSubject = getCaseSubject(post, newCase.LSB_CAS_ContactRole__c).abbreviate(SUBJECT_MAX_LENGTH);

        newCase.subject = caseSubject;
        newCase.LSB_CAS_Topic__c = newCase.subject;

        newCase.LSB_CAS_NatureOfEnquiry__c = 'Social Studio Enquiry';

        if (getUsingCaseAssignmentRule()) {
//Find the active assignment rules on case
            AssignmentRule[] rules = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];

            if (rules.size() > 0) {
//Creating the DMLOptions for "Assign using active assignment rules" checkbox
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.assignmentRuleId = rules[0].id;

//Setting the DMLOption on Case instance
                newCase.setOptions(dmlOpts);
            }
        }

        insert newCase;
        return newCase;
    }

    private Case findParentCase(SocialPost post, SocialPersona persona) {
        Case parentCase = null;
        if (!isChat(post) && (isReplyingToOutboundPost(post) && isSocialPostRecipientSameAsPersona(post.ReplyTo, persona)) || (!isReplyingToOutboundPost(post) && isReplyingToSelf(post, persona))) {
            parentCase = findParentCaseFromPostReply(post);
            if (isParentCaseValid(parentCase)) {
                return parentCase;
            }
        }

        parentCase = findParentCaseFromPersonaAndRecipient(post, persona);
        if (parentCase == null && isChat(post)) {
            parentCase = findParentCaseOfChatFromPersonaAndRecipient(post, persona);
        }
        return parentCase;
    }

    private boolean isChat(SocialPost post) {
        return post.messageType == 'Private' || post.messageType == 'Direct';
    }

    private boolean isParentCaseValid(Case parentCase) {
        return parentCase != null && (!parentCase.IsClosed || caseShouldBeReopened(parentCase));
    }

    private Case findParentCaseFromPostReply(SocialPost post) {
        if (post.ReplyTo != null && String.isNotBlank(post.ReplyTo.ParentId)) {
            List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE Id = :post.ReplyTo.ParentId LIMIT 1];
            if (!cases.isEmpty()) {
                return cases[0];
            }
        }
        return null;
    }

// reply to outbound post
    private boolean isReplyingToOutboundPost(SocialPost post) {
        return (post != null && post.ReplyTo != null && post.ReplyTo.IsOutbound);
    }

// replyTo.recipient == inboundSocialPost.persona.externalId
    private boolean isSocialPostRecipientSameAsPersona(SocialPost postWithRecipient, SocialPersona persona) {
        return (postWithRecipient != null && postWithRecipient.Recipient == persona.ExternalId);
    }

// is replying to self
    private boolean isReplyingToSelf(SocialPost post, SocialPersona persona) {
        return (post != null &&
                persona != null &&
                String.isNotBlank(persona.Id) &&
                post.ReplyTo != null &&
                String.isNotBlank(post.ReplyTo.PersonaId) &&
                post.ReplyTo.PersonaId == persona.id);
    }

    private Case findParentCaseFromPersonaAndRecipient(SocialPost post, SocialPersona persona) {
        SocialPost lastestInboundPostWithSamePersonaAndRecipient = findLatestInboundPostBasedOnPersonaAndRecipient(post, persona);
        if (lastestInboundPostWithSamePersonaAndRecipient != null) {
            List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE id = :lastestInboundPostWithSamePersonaAndRecipient.parentId LIMIT 1];
            if (!cases.isEmpty()) {
                return cases[0];
            }
        }
        return null;
    }

    private Case findParentCaseOfChatFromPersonaAndRecipient(SocialPost post, SocialPersona persona) {
        SocialPost lastestReplyToPost = findLatestOutboundReplyToPostBasedOnPersonaAndRecipient(post, persona);
        if (lastestReplyToPost != null) {
            List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE id = :lastestReplyToPost.parentId LIMIT 1];
            if (!cases.isEmpty()) {
                return cases[0];
            }
        }
        return null;
    }

    private void reopenCase(Case parentCase) {
        SObject[] status = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = false AND IsDefault = true];
        parentCase.Status = 'Being Reviewed';
        update parentCase;
    }

    private void matchPost(SocialPost post) {
        if (post.Id != null) return;

        performR6PostIdCheck(post);

        if (post.Id == null) {
            performExternalPostIdCheck(post);
        }
    }

    private void performR6PostIdCheck(SocialPost post) {
        if (post.R6PostId == null) return;
        List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE R6PostId = :post.R6PostId LIMIT 1];
        if (!postList.isEmpty()) {
            post.Id = postList[0].Id;
        }
    }

    private void performExternalPostIdCheck(SocialPost post) {
        if (post.provider == 'Facebook' && post.messageType == 'Private') return;
        if (post.provider == null || post.externalPostId == null) return;
        List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE ExternalPostId = :post.ExternalPostId AND Provider = :post.provider LIMIT 1];
        if (!postList.isEmpty()) {
            post.Id = postList[0].Id;
        }
    }

    private SocialPost findReplyTo(SocialPost post, SocialPersona persona) {
        if (post.replyToId != null && post.replyTo == null)
            return findReplyToBasedOnReplyToId(post);
        if (post.responseContextExternalId != null) {
            if ((post.provider == 'Facebook' && post.messageType == 'Private') || (post.provider == 'Twitter' && post.messageType == 'Direct')) {
                SocialPost replyTo = findReplyToBasedOnResponseContextExternalPostIdAndProvider(post);
                if (replyTo.id != null)
                    return replyTo;
            }
            return findReplyToBasedOnExternalPostIdAndProvider(post);
        }
        return new SocialPost();
    }

    private SocialPost findReplyToBasedOnReplyToId(SocialPost post) {
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId, Recipient FROM SocialPost WHERE id = :post.replyToId LIMIT 1];
        if (posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }

    private SocialPost findReplyToBasedOnExternalPostIdAndProvider(SocialPost post) {
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId, Recipient FROM SocialPost WHERE Provider = :post.provider AND ExternalPostId = :post.responseContextExternalId LIMIT 1];
        if (posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }

    private SocialPost findReplyToBasedOnResponseContextExternalPostIdAndProvider(SocialPost post) {
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE Provider = :post.provider AND Recipient = :post.Recipient AND responseContextExternalId = :post.responseContextExternalId ORDER BY posted DESC NULLS LAST LIMIT 1];
        if (posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }

    private SocialPost findLatestInboundPostBasedOnPersonaAndRecipient(SocialPost post, SocialPersona persona) {
        if (persona != null && String.isNotBlank(persona.Id) && post != null && String.isNotBlank(post.Recipient)) {
            List<SocialPost> posts = [SELECT Id, ParentId FROM SocialPost WHERE Provider = :post.provider AND Recipient = :post.Recipient AND PersonaId = :persona.id AND IsOutbound = false ORDER BY CreatedDate DESC LIMIT 1];
            if (!posts.isEmpty()) {
                return posts[0];
            }
        }
        return null;
    }

    private SocialPost findLatestOutboundReplyToPostBasedOnPersonaAndRecipient(SocialPost post, SocialPersona persona) {
        if (persona != null && String.isNotBlank(persona.Id) && post != null) {
            List<ExternalSocialAccount> accounts = [SELECT Id FROM ExternalSocialAccount WHERE ExternalAccountId = :post.Recipient];
            if (!accounts.isEmpty()) {
                ExternalSocialAccount account = accounts[0];
                List<SocialPost> posts = [SELECT Id, ParentId FROM SocialPost WHERE Provider = :post.provider AND Recipient = :persona.ExternalId AND OutboundSocialAccountId = :account.Id AND IsOutbound = true ORDER BY CreatedDate DESC LIMIT 1];
                if (!posts.isEmpty()) {
                    return posts[0];
                }
            }
        }
        return null;
    }

    private void matchPersona(SocialPersona persona) {
        if (persona != null) {
            List<SocialPersona> personaList = new List<SocialPersona>();
            if (persona.Provider != 'Other') {
                if (String.isNotBlank(persona.ExternalId)) {
                    personaList = [
                            SELECT Id, ParentId
                            FROM SocialPersona
                            WHERE
                            Provider = :persona.Provider AND
                            ExternalId = :persona.ExternalId
                            LIMIT 1
                    ];
                } else if (String.isNotBlank(persona.Name)) {
//this is a best-effort attempt to match: persona.Name is not guaranteed to be unique for all networks
                    personaList = [
                            SELECT Id, ParentId
                            FROM SocialPersona
                            WHERE
                            Provider = :persona.Provider AND
                            Name = :persona.Name
                            LIMIT 1
                    ];
                }
            } else if (persona.Provider == 'Other' && String.isNotBlank(persona.ExternalId) && String.isNotBlank(persona.MediaProvider)) {
                personaList = [
                        SELECT Id, ParentId
                        FROM SocialPersona
                        WHERE
                        MediaProvider = :persona.MediaProvider AND
                        ExternalId = :persona.ExternalId
                        LIMIT 1
                ];
            } else if (persona.Provider == 'Other' && String.isNotBlank(persona.Name) && String.isNotBlank(persona.MediaProvider)) {
                personaList = [
                        SELECT Id, ParentId
                        FROM SocialPersona
                        WHERE
                        MediaProvider = :persona.MediaProvider AND
                        Name = :persona.Name
                        LIMIT 1
                ];
            }

            if (!personaList.isEmpty()) {
                persona.Id = personaList[0].Id;
                persona.ParentId = personaList[0].ParentId;
            }
        }
    }

    private void createPersona(SocialPersona persona, SocialPost post) {
        if (persona == null || String.isNotBlank(persona.Id) || !isThereEnoughInformationToCreatePersona(persona))
            return;

        persona.ParentId = post.Id;
        insert persona;
    }

    private boolean isThereEnoughInformationToCreatePersona(SocialPersona persona) {
        return String.isNotBlank(persona.Name) &&
                String.isNotBlank(persona.Provider) &&
                String.isNotBlank(persona.MediaProvider);
    }

    private boolean shouldCreateCase(SocialPost post, Map<String, Object> rawData) {
        return !isUnsentParent(rawData) && (!hasSkipCreateCaseIndicator(rawData));
    }

    private boolean isUnsentParent(Map<String, Object> rawData) {
        Object unsentParent = rawData.get('unsentParent');
        return unsentParent != null && 'true'.equalsIgnoreCase(String.valueOf(unsentParent));
    }

    private boolean hasSkipCreateCaseIndicator(Map<String, Object> rawData) {
        Object skipCreateCase = rawData.get('skipCreateCase');
        return skipCreateCase != null && 'true'.equalsIgnoreCase(String.valueOf(skipCreateCase));
    }

    private Set<String> getPostTags(SocialPost post) {
        Set<String> postTags = new Set<String>();
        if (post.postTags != null)
            postTags.addAll(post.postTags.split(',', 0));
        return postTags;
    }

    private Set<String> getTopicPicklistValues() {
        Set<String> topicValues= new Set<String>();
        List<Schema.PicklistEntry> ple = Case.LSB_CAS_Topic__c.getDescribe().getPicklistValues();
        for( Schema.PicklistEntry topicVal : ple){
            topicValues.add(topicVal.getValue());
        }
        return topicValues;
    }

    private Id getCaseRecordTypeId() {
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(SOCIAL_STUDIO_RECORD_TYPE_NAME).getRecordTypeId();
        return recordTypeId;
    }

    private Contact getPersonaContactDetails(SocialPersona persona) {
        List<Contact> contacts = new List<Contact>();
        if (persona.ParentId.getSobjectType() == Contact.sObjectType) {
            contacts = [SELECT LSB_CON_CurrentRole__c FROM Contact WHERE id = :persona.ParentId];
        }
        if (!contacts.isEmpty())
            return contacts.get(0); else
                return null;

    }

    private String getNewCaseDescription(SocialPost post, SocialPersona persona) {
        String description = 'Social Post ' + post.MessageType + ' ' + persona.RealName + ' Channel ' + post.MediaProvider;
        return description;
    }


    private Map<String, String> getPostLabelsMapping(){
        Map<String, String> postLabelToSocialStudioLabel = new Map<String, String>();

        Map<String, LSB_Social_Post_Labels__mdt> socialPostLabelMappings = LSB_Social_Post_Labels__mdt.getAll();
        for(LSB_Social_Post_Labels__mdt mapping : socialPostLabelMappings.values()) {
            if(mapping.LSB_Social_Studio_Label__c != null) {
                postLabelToSocialStudioLabel.put(mapping.LSB_Social_Studio_Label__c, mapping.LSB_Picklist_Value__c);
            } else {
                postLabelToSocialStudioLabel.put(mapping.LSB_Current_Role__c, mapping.LSB_Picklist_Value__c);
            }
        }
        return postLabelToSocialStudioLabel;
    }

}