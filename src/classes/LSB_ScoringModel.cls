public with sharing class LSB_ScoringModel {

    @AuraEnabled
    public static ContactWrapper checkIfContactIsPreApplicant (Id contactId) {
        ContactWrapper contactWrapper = new ContactWrapper();
        List<hed__Affiliation__c> affilations = [SELECT hed__Contact__c, hed__Contact__r.LSB_CON_Profile_Percentage__c, hed__Contact__r.LSB_CON_Score__c from hed__Affiliation__c where hed__Role__c='Prospect' AND hed__Status__c='Current'];

        for (hed__Affiliation__c aff : affilations) {
            if (aff.hed__Contact__c == contactId) {
                contactWrapper.score = aff.hed__Contact__r.LSB_CON_Score__c;
                contactWrapper.percentage = aff.hed__Contact__r.LSB_CON_Profile_Percentage__c;
            }
        }
        return contactWrapper;
    }

    public class ContactWrapper {
        @AuraEnabled
        public Decimal score = 0;
        @AuraEnabled
        public Decimal percentage = 0;
    }
}