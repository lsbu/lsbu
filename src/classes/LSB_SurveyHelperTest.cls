@isTest
public with sharing class LSB_SurveyHelperTest {

    private static final String DUMMY_SURVEY_INVITATION_ID = '0Ki3L00000001ABCDE';
    private static final String CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME = LSB_Constants.CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME;
    private static final String CASE_STATUS_RECEIVED = LSB_Constants.CASE_STATUS_RECEIVED;

    @TestSetup
    private static void setup() {
        Contact testContact = new Contact(LastName = 'Test', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = 'Test123');
        insert testContact;

        Id enquiryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(CASE_ENQUIRY_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId();

        Case testCase = new Case(Status = CASE_STATUS_RECEIVED, ContactId = testContact.Id, RecordTypeId = enquiryRecordTypeId, LSB_CAS_ContactRole__c = LSB_Constants.CONTACT_ROLE_PROSPECT);
        insert testCase;

        LiveChatVisitor tesVisitor = new LiveChatVisitor();
        insert tesVisitor;

        LiveChatTranscript transcript = new LiveChatTranscript(CaseId = testCase.Id, LiveChatVisitorId = tesVisitor.Id);
        insert transcript;
    }

    @IsTest
    private static void shouldReturnNullWhenNoSurveyInvitationFound() {
        SurveyInvitation result = LSB_SurveyHelper.getSurveyInvitationById(DUMMY_SURVEY_INVITATION_ID);

        System.assertEquals(null, result);
    }

    @IsTest
    private static void shouldGetLiveChatTranscriptByChatKey() {
        LiveChatTranscript transcript = [SELECT Id, ChatKey FROM LiveChatTranscript][0];

        LiveChatTranscript result = LSB_SurveyHelper.getLiveChatTranscript(transcript.ChatKey);

        System.assertEquals(transcript.Id, result.Id);
    }

    @IsTest
    private static void shouldReturnNullWhenNoLiveChatTranscriptFound() {
        LiveChatTranscript result = LSB_SurveyHelper.getLiveChatTranscript('abc');

        System.assertEquals(null, result);
    }

    @IsTest
    private static void shouldGetNameFromCustomLabel() {
        String result = LSB_SurveyHelper.getSurveyNameFromCustomLabel('Test');

        System.assert(String.isNotBlank(result));
    }

    @IsTest
    private static void shouldNotCreateSurveyInvitationWhenNoSurvey() {
        Survey testSurvey = new Survey();

        Boolean hasError = false;

        Test.startTest();
        try {
            LSB_SurveyHelper.createSurveyInvitation(testSurvey);
        } catch(Exception ex) {
            hasError = true;
        }
        Test.stopTest();

        System.assert(hasError);
    }
}