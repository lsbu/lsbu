public without sharing class LSB_ContactPointHelper {

    public static final Id DEFAULT_LEGITIMATE_INTEREST_DATA_USE_PURPOSE_ID = null;
    public static final Id DEFAULT_EXPLICIT_INTEREST_DATA_USE_PURPOSE_ID = null;

    private static Map<String, LSB_Utils.PicklistEntryWrapper> CONTACT_POINT_OBJECT_NAME_TO_TYPE = new Map<String, LSB_Utils.PicklistEntryWrapper> {
        SObjectType.ContactPointEmail.getName() =>
                LSB_Utils.getPicklistEntryWrapper(
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_EMAIL,
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_EMAIL),
        SObjectType.ContactPointPhone.getName() =>
                LSB_Utils.getPicklistEntryWrapper(
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_PHONE,
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_PHONE),
        SObjectType.ContactPointAddress.getName() =>
                LSB_Utils.getPicklistEntryWrapper(
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS,
                        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS_LABEL)
    };

    private static Map<String, String> CONTACT_POINT_SUBTYPE_TO_CASE_CONSENT_FIELD_API = new Map<String, String> {
        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_EMAIL => Case.LSB_CAS_EmailOptIn__c.getDescribe().getName(),
        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_PHONE + LSB_Constants.CONTACT_POINT_CONSENT_PHONE_CALL => Case.LSB_CAS_PhoneOptIn__c.getDescribe().getName(),
        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_PHONE + LSB_Constants.CONTACT_POINT_CONSENT_PHONE_SMS => Case.LSB_CAS_SMSOptIn__c.getDescribe().getName(),
        LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_TYPE_MAILINGADDRESS_LABEL => Case.LSB_CAS_PostOptIn__c.getDescribe().getName()
    };

    static {
        List<DataUsePurpose> defaultDataUsePurposes = [SELECT Id, LSB_Type__c FROM DataUsePurpose WHERE LSB_DAP_IsDefault__c = true];

        for (DataUsePurpose record : defaultDataUsePurposes) {
            if (record.LSB_Type__c == LSB_Constants.DATA_USE_PURPOSE_TYPE_EXPLICIT_INTEREST) {
                DEFAULT_EXPLICIT_INTEREST_DATA_USE_PURPOSE_ID = record.Id;
            } else if (record.LSB_Type__c == LSB_Constants.DATA_USE_PURPOSE_TYPE_LEGITIMATE_INTEREST) {
                DEFAULT_LEGITIMATE_INTEREST_DATA_USE_PURPOSE_ID = record.Id;
            }
        }
    }

    public static ContactPointPhone createContactPointPhone(String phone, Contact contactRecord, String type) {
        return new ContactPointPhone(
                LSB_ExternalId__c = getContactPointExternalId(contactRecord, type),
                TelephoneNumber = phone,
                ActiveFromDate = System.today(),
                PhoneType = type,
                IsPrimary = type.equals(contactRecord.hed__PreferredPhone__c),
                ParentId = contactRecord.IndividualId,
                LSB_COP_Contact__c = contactRecord.Id
        );
    }

    public static ContactPointEmail createContactPointEmail(String email, Contact contactRecord, String type) {
        return new ContactPointEmail(
                LSB_ExternalId__c = getContactPointExternalId(contactRecord, type),
                EmailAddress = email,
                ActiveFromDate = System.today(),
                IsPrimary = type.equals(contactRecord.hed__Preferred_Email__c),
                ParentId = contactRecord.IndividualId,
                LSB_COE_Contact__c = contactRecord.Id
        );
    }

    public static ContactPointAddress createContactPointAddress(hed__Address__c address, Boolean isDefaultUpdated, Boolean isNew, String externalId) {
        ContactPointAddress contactPointAddress = new ContactPointAddress(
                Name = address.hed__Address_Type__c + ' Address',
                AddressType = address.hed__Address_Type__c,
                City = address.hed__MailingCity__c,
                Country = address.hed__MailingCountry__c,
                Latitude = address.hed__Geolocation__Latitude__s,
                Longitude = address.hed__Geolocation__Longitude__s,
                ParentId = address.LSB_ADR_IndividualId__c,
                PostalCode = address.hed__MailingPostalCode__c,
                State = address.hed__MailingState__c,
                Street = address.hed__MailingStreet__c,
                IsDefault = address.hed__Default_Address__c,
                IsPrimary = address.hed__Default_Address__c,
                LSB_COA_Contact__c = address.hed__Parent_Contact__c,
                LSB_COA_Address__c = address.Id,
                LSB_COA_ExternalId__c = externalId
        );

        if (isNew) {
            contactPointAddress.ActiveFromDate = Date.today();
        }


        if (isDefaultUpdated && !address.hed__Default_Address__c) {
            contactPointAddress.ActiveToDate = Date.today();
        }

        return contactPointAddress;
    }


    public static ContactPointConsent createContactPointConsent(String externalId, String dataUsePurposeType, String privacyStatus, Contact contactRecord, Id contactPointId, Id dataUsePurposeId) {
        LSB_Utils.PicklistEntryWrapper contactPointType = CONTACT_POINT_OBJECT_NAME_TO_TYPE.get(contactPointId.getSObjectType().getDescribe().getName());
        String type = contactPointType.label +
                (externalId.contains(' (') ? ' (' + externalId.substringAfter('(').substringBefore(')') + ')' : '');

        return new ContactPointConsent(
                LSB_ExternalId__c = externalId,
                ContactPointId = contactPointId,
                Name = type + ' channel - ' + dataUsePurposeType.toLowerCase(),
                CaptureContactPointType = contactPointType.value,
                PrivacyConsentStatus = privacyStatus,
                CaptureDate = System.now(),
                EffectiveFrom = System.now(),
                LSB_COC_Subtype__c = type,
                LSB_COC_Contact__c = contactRecord.Id,
                CaptureSource = contactRecord.LSB_CON_SourceSystem__c,
                DataUsePurposeId = dataUsePurposeId
        );
    }

    public static String getContactPointExternalId(Contact contactRecord, String type) {
        return contactRecord.Id + '#' + type;
    }

    public static String getCaseConsentFieldAPI(ContactPointConsent consent) {
        return CONTACT_POINT_SUBTYPE_TO_CASE_CONSENT_FIELD_API.get(consent.LSB_COC_Subtype__c);
    }

    public static String convertBooleanToPrivacyStatus(Boolean consentStatus) {
        return consentStatus ? LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTIN :
                LSB_Constants.CONTACT_POINT_CONSENT_PRIVACY_CONSENT_STATUS_OPTOUT;
    }
}