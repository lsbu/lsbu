public without sharing class LSB_TopicHelper {

    @TestVisible private static final String keyPrefixOfKnowledge = Schema.getGlobalDescribe().get('Knowledge__kav').getDescribe().getKeyPrefix();
    private static final String keyPrefixOfTopic = Schema.getGlobalDescribe().get('Topic').getDescribe().getKeyPrefix();
    private String communityId;

    public LSB_TopicHelper(String communityId) {
        this.communityId = communityId;
    }

    public List<ConnectApi.Topic> getTopicsByTopicType(ConnectApi.ManagedTopicType topicType) {
        List<ConnectApi.Topic> topicList = new List<ConnectApi.Topic>();
        if (String.isNotBlank(this.communityId)) {
            ConnectApi.ManagedTopicCollection mtCollection = ConnectAPI.ManagedTopics.getManagedTopics(this.communityId, topicType);

            for (ConnectApi.ManagedTopic managedTopic : mtCollection.managedTopics) {
                topicList.add(managedTopic.topic);
            }
        }
        return topicList;
    }

    public Id getTopicIdByEntityId(String entityId) {
        if (entityId == null) {
            return null;
        }

        Id topicId = entityId.substring(0,3) == keyPrefixOfTopic ? entityId : null;

        if (entityId.substring(0,3) == keyPrefixOfKnowledge){
            List<TopicAssignment> topicAssignments = [SELECT TopicId FROM TopicAssignment WHERE EntityId = :entityId];

            for (TopicAssignment topicAssignment : topicAssignments) {
                topicId = topicAssignment.TopicId;
            }
        }

        return topicId;
    }
}