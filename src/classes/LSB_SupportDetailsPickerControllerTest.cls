@IsTest
class LSB_SupportDetailsPickerControllerTest {

    private static final String SUPPORT_AGREEMENT_DETAILS_FIELDS = 'LSB_SupportArrangementDetailsFields';
    private static final String SUPPORT_AGREEMENT_DETAILS_API_NAME = LSB_SUD_SupportArrangementDetail__c.getSObjectType().getDescribe().getName();

    private static final String SUPPORT_PROFILE_API_NAME = LSB_SUE_SupportProfile__c.getSObjectType().getDescribe().getName();

    private static final String SUPPORT_PROFILE_DETAILS_MHWB_FIELDS = 'LSB_SupportProfileDetailsMHWB';
    private static final String SUPPORT_PROFILE_DETAILS_DDS_FIELDS = 'LSB_SupportProfileDetailsDDS';
    private static final String SUPPORT_PROFILE_DETAILS_API_NAME = LSB_SPD_SupportProfileDetails__c.getSObjectType().getDescribe().getName();

    private static final String MASTER_FIELD_SET = 'LSB_SupportMasterFieldsForPicker';

    @TestSetup
    static void createTestData() {
        List<LSB_SUM_SupportArrangementMaster__c> supportArrangementMasters = new List<LSB_SUM_SupportArrangementMaster__c>();

        supportArrangementMasters.add(
            new LSB_SUM_SupportArrangementMaster__c(
                Object_Used_In__c = 'Support Profile',
				LSB_SUM_ObjectUsedInRecordType__c = 'Disability and Dyslexia Support',		
                LSB_SUM_SupportArrangementCategory__c = 'Disability and Dyslexia Support',
                Name = 'Disability and Dyslexia Support 1'
            )
        );

        supportArrangementMasters.add(
            new LSB_SUM_SupportArrangementMaster__c(
                Object_Used_In__c = 'Support Profile',
				LSB_SUM_ObjectUsedInRecordType__c = 'Disability and Dyslexia Support',
                LSB_SUM_SupportArrangementCategory__c = 'Disability and Dyslexia Support',
                Name = 'Disability and Dyslexia Support 2'
            )
        );

        supportArrangementMasters.add(
            new LSB_SUM_SupportArrangementMaster__c(
                Object_Used_In__c = 'Support Profile',
				LSB_SUM_ObjectUsedInRecordType__c = 'MHWB Concerns',		
                LSB_SUM_SupportArrangementCategory__c = 'Academic or University Concern',
                Name = 'Academic or University Concern 1'
            )
        );

        supportArrangementMasters.add(
            new LSB_SUM_SupportArrangementMaster__c(
                Object_Used_In__c = 'Support Profile',
				LSB_SUM_ObjectUsedInRecordType__c = 'MHWB Concerns',
                LSB_SUM_SupportArrangementCategory__c = 'Academic or University Concern',
                Name = 'Academic or University Concern 2'

            )
        );

        insert supportArrangementMasters;

        Account a = new Account(Name = 'Test Account Name');
        insert a;

        Contact studentContact = new Contact
            (FirstName = 'John', LastName = 'Snowman', hed__AlternateEmail__c = TestDataClass.communityUserEmail, LSB_CON_CurrentRole__c = LSB_Constants.CONTACT_ROLE_STUDENT, Email = TestDataClass.communityUserEmail, LSB_CON_SourceSystem__c = 'QL', LSB_ChannelOfPreference__c = 'Email', LSB_ExternalID__c = '1234QWERTY', AccountId = a.Id);
        insert studentContact;

        List<LSB_SUE_SupportProfile__c> supportProfiles = new List<LSB_SUE_SupportProfile__c>();

        supportProfiles.add(
            new LSB_SUE_SupportProfile__c(
                LSB_SUE_Contact__c = studentContact.Id,
                LSB_SUE_Status__c = 'New',
                Name = 'Test Support Profile'
            )
        );

        insert supportProfiles;

        RecordType recordTypeMHWB = [SELECT Id FROM RecordType WHERE DeveloperName = 'LSB_SPD_MHWB' LIMIT 1];
        RecordType recordTypeDDS = [SELECT Id FROM RecordType WHERE DeveloperName = 'LSB_SPD_DDS' LIMIT 1];

        List<LSB_SPD_SupportProfileDetails__c> supportProfileDetails = new List<LSB_SPD_SupportProfileDetails__c>();

        supportProfileDetails.add(
            new LSB_SPD_SupportProfileDetails__c(
                LSB_SPD_Active__c = true,
                RecordTypeId = recordTypeMHWB.Id,
                SupportProfileMHWB__c = supportProfiles[0].Id,
                LSB_SPD_SupportProfileCategory__c = supportArrangementMasters[3].LSB_SUM_SupportArrangementCategory__c,
                LSB_SPD_SupportMaster__c = supportArrangementMasters[3].Id,
                SupportProfileMaster__c = supportProfiles[0].Id
            )
        );

        supportProfileDetails.add(
            new LSB_SPD_SupportProfileDetails__c(
                LSB_SPD_Active__c = true,
                RecordTypeId = recordTypeDDS.Id,
                SupportProfileDDS__c = supportProfiles[0].Id,
                LSB_SPD_SupportProfileCategory__c = supportArrangementMasters[1].LSB_SUM_SupportArrangementCategory__c,
                LSB_SPD_SupportMaster__c = supportArrangementMasters[1].Id,
                SupportProfileMaster__c = supportProfiles[0].Id
            )
        );

        insert supportProfileDetails;
    }

    @IsTest
    static void getFieldNamesFromFieldSetsTest() {
        List<String> fieldsList = LSB_SupportDetailsPickerController.getFieldNamesFromFieldSets(SUPPORT_AGREEMENT_DETAILS_API_NAME, SUPPORT_AGREEMENT_DETAILS_FIELDS);
        System.assertNotEquals(0, fieldsList.size());
    }

    @IsTest
    static void fetchColumnDataTest() {
        List<LSB_SupportDetailsPickerController.ColumnData> columnData = LSB_SupportDetailsPickerController.fetchColumnData(SUPPORT_AGREEMENT_DETAILS_API_NAME, SUPPORT_AGREEMENT_DETAILS_FIELDS);
        System.assertNotEquals(0, columnData.size());
    }

    @IsTest
    static void fetchSupportMastersTest() {
        LSB_SUE_SupportProfile__c supportProfile = [SELECT Id FROM LSB_SUE_SupportProfile__c LIMIT 1];
        List<LSB_SUM_SupportArrangementMaster__c> supportArrangementMastersMHWB = LSB_SupportDetailsPickerController.fetchSupportMasters(SUPPORT_PROFILE_DETAILS_API_NAME, supportProfile.Id, MASTER_FIELD_SET, LSB_SupportDetailsPickerController.MHWB_CONCERNS);
        System.assertEquals(1, supportArrangementMastersMHWB.size());
        List<LSB_SUM_SupportArrangementMaster__c> supportArrangementMastersDDS = LSB_SupportDetailsPickerController.fetchSupportMasters(SUPPORT_PROFILE_DETAILS_API_NAME, supportProfile.Id, MASTER_FIELD_SET, LSB_SupportDetailsPickerController.DISABILITY_AND_DYSLEXIA_SUPPORT);
        System.assertEquals(1, supportArrangementMastersDDS.size());
    }

    @IsTest
    static void fetchSupportDetailsTest() {
        LSB_SUE_SupportProfile__c supportProfile = [SELECT Id FROM LSB_SUE_SupportProfile__c LIMIT 1];
        List<SObject> supportProfileDetailsMHWB = LSB_SupportDetailsPickerController.fetchSupportDetails(SUPPORT_PROFILE_DETAILS_API_NAME, SUPPORT_PROFILE_DETAILS_MHWB_FIELDS, SUPPORT_PROFILE_API_NAME, supportProfile.Id, LSB_SupportDetailsPickerController.MHWB_CONCERNS);
        System.assertEquals(1, supportProfileDetailsMHWB.size());
        List<SObject> supportProfileDetailsDDS = LSB_SupportDetailsPickerController.fetchSupportDetails(SUPPORT_PROFILE_DETAILS_API_NAME, SUPPORT_PROFILE_DETAILS_DDS_FIELDS, SUPPORT_PROFILE_API_NAME, supportProfile.Id, LSB_SupportDetailsPickerController.DISABILITY_AND_DYSLEXIA_SUPPORT);
        System.assertEquals(1, supportProfileDetailsDDS.size());
    }

}