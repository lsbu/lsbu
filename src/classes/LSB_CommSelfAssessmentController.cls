public without sharing class LSB_CommSelfAssessmentController {

    private static final String TEMPLATE_DEVELOPER_NAME = 'LSB_SelfAssessmentUpdateConfirmation';
    private static final string EMAIL_SENDER_CONFIG_NAME = 'LSB_PersonalDevelopmentPlanUpdate';

    @AuraEnabled(Cacheable = true)
    public static String fetchAssessmentData() {
        LSB_CommSelfAssessmentHelper.SelfAssessmentWrapper selfAssessmentWrapper = LSB_CommSelfAssessmentHelper.getAssessmentWrapperData();
        String serializedData = JSON.serialize(selfAssessmentWrapper);
        return serializedData;
    }

    @AuraEnabled
    public static void handleSaveResponses(List<LSB_SAR_SelfAssessmentResponse__c> responses) {
        if (responses == null || responses.isEmpty()) {
            return;
        }
        update responses;
        sendEmailConfirmation();
    }

    private static void sendEmailConfirmation() {
        EmailTemplate emailTemplate = [
            SELECT Id,Subject, Body
            FROM EmailTemplate
            WHERE DeveloperName = :TEMPLATE_DEVELOPER_NAME
            LIMIT 1
        ];

        Contact contact = [
            SELECT Id
            FROM Contact
            WHERE Id
                IN (
                    SELECT ContactId
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
                )
            LIMIT 1
        ];

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setTemplateID(emailTemplate.Id);
        mail.setTargetObjectId(contact.Id);
        mail.setWhatId(contact.Id);
        mail.setSaveAsActivity(false);
        mail.setBccSender(false);
        mail.setUseSignature(false);

        if (!Test.isRunningTest()) {
            String emailSender = [
                SELECT LSB_Value__c
                FROM LSB_Configuration__mdt
                WHERE DeveloperName = :EMAIL_SENDER_CONFIG_NAME
                LIMIT 1
            ].LSB_Value__c;
            List<OrgWideEmailAddress> orgWideEmailAddresses = [
                SELECT Address
                FROM OrgWideEmailAddress
                WHERE DisplayName = :emailSender
                LIMIT 1
            ];
            if (!orgWideEmailAddresses.isEmpty()) {
                mail.setOrgWideEmailAddressId(orgWideEmailAddresses[0].Id);
            }
        }
        mails.add(mail);

        Messaging.sendEmail(mails, false);
    }
}