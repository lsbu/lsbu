@IsTest
public with sharing class LSB_ChatBotRedirectTest {

    @TestSetup
    private static void testSetup() {
        Knowledge__kav knowledgeKav = new Knowledge__kav(
            UrlName = 'TestPassword123',
            Title = 'Test',
            Summary = 'Test',
            IsVisibleInPkb = true,
            LSB_KGE_FaqCategory2__c = 'Other',
            LSB_KGE_ChatbotResponse__c = 'Test',
            LSB_KGE_ChatbotResponseLong__c = 'Test');
        insert knowledgeKav;

        knowledge__kav knowledgeKav2 = [
            SELECT Id, Title, KnowledgeArticleId
            FROM knowledge__kav
            WHERE Id = :knowledgeKav.Id
        ];
        KbManagement.PublishingService.publishArticle(knowledgeKav2.KnowledgeArticleId, true);

        Account account = new Account(Name = 'Test Account');
        insert account;

        Contact contact = new Contact(AccountId = account.Id, FirstName = 'Test', LastName = 'Contact', LSB_CON_CurrentRole__c = 'Prospect');
        insert contact;

        Case testCase = new Case(Subject = 'Test Case', ContactId = contact.Id, LSB_CAS_ContactRole__c = 'Prospect');
        insert testCase;
    }

    @IsTest
    private static void getKnowledgeArticlesTest() {
        Case testCase = [SELECT Id FROM Case LIMIT 1];
        Knowledge__kav knowledgeKav = [SELECT Id FROM Knowledge__kav LIMIT 1];
        Id [] fixedSearchResults = new Id[1];
        fixedSearchResults[0] = knowledgeKav.Id;
        Test.setFixedSearchResults(fixedSearchResults);

        List<List<SObject>> resultsList;
        Test.startTest();
        LSB_ChatBotRedirect.UserInput userInput = new LSB_ChatBotRedirect.UserInput();
        userInput.userInput = 'Test';
        userInput.caseId = testCase.Id;
        List<LSB_ChatBotRedirect.UserInput> userInputs = new List<LSB_ChatBotRedirect.UserInput>{
            userInput
        };
        resultsList = LSB_ChatBotRedirect.getKnowledgeArticles(userInputs);
        Test.stopTest();

        System.assertEquals(1, resultsList.size());
        System.assertEquals(2, resultsList[0].size());
        List<Knowledge__kav> knowledgeKavs = (List<Knowledge__kav>) resultsList[0];
        System.assertEquals(false, knowledgeKavs[0].LSB_KGE_NoneAboveFlag__c);
        System.assertEquals(true, knowledgeKavs[1].LSB_KGE_NoneAboveFlag__c);
    }

}