public without sharing class LSB_MyAssessmentDeadlines {
    private static final String COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_ID = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByDeveloperName().get(LSB_Constants.COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId();
    private static final String COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_ID = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByDeveloperName().get(LSB_Constants.COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId();

    @AuraEnabled
    public static AssessmentListsWrapper getAssessments() {
        List<AssessmentWrapper> pastAssessmentWrappers = new List<AssessmentWrapper>();
        List<AssessmentWrapper> upcomingAssessmentWrappers = new List<AssessmentWrapper>();

        Set<Id> moduleIds = new Set<Id>();
        List<hed__Course_Enrollment__c> moduleEnrollments = LSB_MyModules.getMyModulesEnrollments();
        for (hed__Course_Enrollment__c moduleEnrollment : moduleEnrollments) {
            moduleIds.add(moduleEnrollment.Id);
        }

        Map<Id,hed__Course_Enrollment__c> componentById = new Map<Id,hed__Course_Enrollment__c>();
        List<hed__Course_Enrollment__c> components = [SELECT LSB_CCN_ModuleInstanceName__c, LSB_CCN_ParentModuleComponent__c, LSB_CCN_ParentModuleComponent__r.LSB_CCN_ModuleInstanceName__c, LSB_CCN_HandInDate__c, LSB_CCN_ComponentDescription__c, LSB_CCN_SubComponentDescription__c  FROM hed__Course_Enrollment__c WHERE RecordTypeId = :COURSE_ENROLLMENT_COMPONENT_RECORD_TYPE_ID AND LSB_CCN_ParentModuleComponent__c IN: moduleIds];
        for (hed__Course_Enrollment__c component : components) {
            componentById.put(component.Id, component);
        }

        Map<Id,hed__Course_Enrollment__c> subcomponentById = new Map<Id,hed__Course_Enrollment__c>();
        Set<Id> parentComponentIds = new Set<Id>();
        List<hed__Course_Enrollment__c> subcomponents = [SELECT LSB_CCN_ModuleInstanceName__c, LSB_CCN_ParentModuleComponent__c, LSB_CCN_ParentModuleComponent__r.LSB_CCN_ParentModuleComponent__r.LSB_CCN_ModuleInstanceName__c, LSB_CCN_HandInDate__c, LSB_CCN_ComponentDescription__c, LSB_CCN_SubComponentDescription__c  FROM hed__Course_Enrollment__c WHERE RecordTypeId =: COURSE_ENROLLMENT_SUBCOMPONENT_RECORD_TYPE_ID AND LSB_CCN_ParentModuleComponent__c IN: componentById.keySet()];
        for (hed__Course_Enrollment__c subcomponent : subcomponents) {
            subcomponentById.put(subcomponent.Id, subcomponent);
            parentComponentIds.add(subcomponent.LSB_CCN_ParentModuleComponent__c);
        }

        for (Id componentId : componentById.keySet()) {
            if (parentComponentIds.contains(componentId)) {
                continue;
            }
            AssessmentWrapper assessmentWrapper = new AssessmentWrapper(componentById.get(componentId).LSB_CCN_ComponentDescription__c, componentById.get(componentId).LSB_CCN_ParentModuleComponent__r.LSB_CCN_ModuleInstanceName__c, componentById.get(componentId).LSB_CCN_HandInDate__c);
            if (Date.today() > componentById.get(componentId).LSB_CCN_HandInDate__c) {
                pastAssessmentWrappers.add(assessmentWrapper);
            } else {
                upcomingAssessmentWrappers.add(assessmentWrapper);

            }
        }

        for (Id subcomponentId : subcomponentById.keySet()) {
            AssessmentWrapper assessmentWrapper = new AssessmentWrapper(subcomponentById.get(subcomponentId).LSB_CCN_SubComponentDescription__c, subcomponentById.get(subcomponentId).LSB_CCN_ParentModuleComponent__r.LSB_CCN_ParentModuleComponent__r.LSB_CCN_ModuleInstanceName__c, subcomponentById.get(subcomponentId).LSB_CCN_HandInDate__c);
            if (Date.today() > subcomponentById.get(subcomponentId).LSB_CCN_HandInDate__c) {
                pastAssessmentWrappers.add(assessmentWrapper);
            } else {
                upcomingAssessmentWrappers.add(assessmentWrapper);

            }
        }

        pastAssessmentWrappers.sort();
        upcomingAssessmentWrappers.sort();

        AssessmentListsWrapper assessmentList = new AssessmentListsWrapper();
        assessmentList.upcomingAssessments = upcomingAssessmentWrappers;
        assessmentList.pastAssessments = pastAssessmentWrappers;
        return assessmentList;
    }

    public class AssessmentListsWrapper {
        @AuraEnabled
        public List<AssessmentWrapper> upcomingAssessments;
        @AuraEnabled
        public List<AssessmentWrapper> pastAssessments;
    }

    public class AssessmentWrapper implements Comparable {
        @AuraEnabled
        public String componentOrSubComponentDesc;
        @AuraEnabled
        public String moduleName;
        @AuraEnabled
        public Date handInDate;
        @AuraEnabled
        public Boolean isPastAssessment;

        public AssessmentWrapper(String componentOrSubComponentDesc, String moduleName, Date handInDate) {
            this.componentOrSubComponentDesc = componentOrSubComponentDesc;
            this.moduleName = moduleName;
            this.handInDate = handInDate;
        }

        public Integer compareTo(Object instance)
        {
            AssessmentWrapper that = (AssessmentWrapper)instance;
            if (this.handInDate == null) return 1;
            if (this.handInDate > that.handInDate) return 1;
            if (this.handInDate < that.handInDate) return -1;
            return 0;
        }
    }
}