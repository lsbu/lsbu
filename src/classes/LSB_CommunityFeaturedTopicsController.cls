public with sharing class LSB_CommunityFeaturedTopicsController {

    private static LSB_TopicHelper topicHelper;

    static {
        topicHelper = new LSB_TopicHelper(getCommunityIdByName(LSB_Constants.COMMUNITY_NAME));
    }

    /**
     * @description Method is using to retrieve featured topics from Community Managed Topics.
     * @return Featured Topics.
     */
    @AuraEnabled
    public static List<ConnectApi.Topic> fetchTopics() {
        return topicHelper.getTopicsByTopicType(ConnectApi.ManagedTopicType.Featured);
    }

    /**
     * @description Method is using to retrieve all content topics from Community Managed Topics.
     * @return Content Topics.
     */
    @AuraEnabled
    public static List<ConnectApi.Topic> getAllContentTopics() {
        return topicHelper.getTopicsByTopicType(ConnectApi.ManagedTopicType.Content);
    }

    /**
     * @description Method is using to retrieve topic Id from TopicAssignment, which is related to given EntityId.
     * @return Topic Id
     */
    @AuraEnabled
    public static Id fetchTopicIdByEntityId(String entityId) {
        return topicHelper.getTopicIdByEntityId(entityId);
    }

    private static Id getCommunityIdByName(String communityName) {
        return [SELECT Id FROM Network WHERE Name = :communityName][0].Id;
    }
}