public without sharing class LSB_TaskHelper {

    private static final String MISSING_CASE_ADVISEE_RECORD = ' missing Case Advisee Record';
    private static final String MISSING_SUPPORT_PROFILE_ADVISEE_RECORD = ' missing Support Profile Record';
    private static final String MISSING_COMMUNITY_USER_RECORD = ' missing Community User Record';

    public void fillData(List<Task> newTasks) {
        DescribeSObjectResult describe = sfal__SuccessPlan__c.sObjectType.getDescribe();
        String successPlanIdPrefix = describe.getKeyPrefix();
        Set<Id> successPlanIds = new Set<Id>();

        for (Task taskRecord : newTasks) {
            if (!String.isBlank(taskRecord.WhatId) && String.valueOf(taskRecord.WhatId).startsWith(successPlanIdPrefix)) {
                if (!String.isBlank(taskRecord.WhoId)) {
                    taskRecord.WhoId = null;
                }

                successPlanIds.add(taskRecord.WhatId);
            }
        }

        if (successPlanIds.isEmpty()) {
            return;
        }

        Map<Id, Id> successPlanId2StudentUserId = new Map<Id, Id>();
        Map<Id, Id> adviseeContactId2SuccessPlanIds = new Map<Id, Id>();

        for (sfal__SuccessPlan__c successPlan : [SELECT Id, sfal__Advisee__c FROM sfal__SuccessPlan__c WHERE Id IN :successPlanIds]) {
            adviseeContactId2SuccessPlanIds.put(successPlan.sfal__Advisee__c, successPlan.Id);
        }

        for (User student : [SELECT Id, ContactId FROM User WHERE ContactId IN :adviseeContactId2SuccessPlanIds.keySet()]) {
            successPlanId2StudentUserId.put(
                adviseeContactId2SuccessPlanIds.get(student.ContactId),
                student.Id
            );
        }

        for (Task taskRecord : newTasks) {
            if (!String.isBlank(taskRecord.WhatId) && String.valueOf(taskRecord.WhatId).startsWith(successPlanIdPrefix)) {
                taskRecord.OwnerId = successPlanId2StudentUserId.get(taskRecord.WhatId);
            }
        }

    }

    @InvocableMethod(label='Create Tasks' description='It creates Tasks records avoiding duplicates' category='Task')
    public static void createTasks(List<Task> tasks) {
        List<Task> tasksToCreate = filterTaskByOwnerAndTaskTemplateAndWhoId(tasks);

        if (!tasksToCreate.isEmpty()) {
            try {
                insert tasksToCreate;
            } catch (Exception e) {
                System.debug('Create Tasks method Task insert error: ' + e.getMessage());
            }
        }
    }

    private static List<Task> filterTaskByOwnerAndTaskTemplateAndWhoId(List<Task> tasks) {
        List<Task> tasksToCreate = new List<Task>();
        Map<String, List<Task>> ownerId2Tasks = new Map<String, List<Task>>();

        for (Task task : tasks) {
            if (ownerId2Tasks.containsKey(task.OwnerId)) {
                Boolean hasDuplicate = false;
                for (Task taskInlist : ownerId2Tasks.get(task.OwnerId)) {
                    if (task.LSB_ACT_TaskTemplateId__c == taskInlist.LSB_ACT_TaskTemplateId__c && task.WhoId == taskInlist.WhoId) {
                        hasDuplicate = true;
                    }
                }
                if (!hasDuplicate) {
                    ownerId2Tasks.get(task.OwnerId).add(task);
                }
            } else {
                ownerId2Tasks.put(task.OwnerId, new List<Task> { task });
            }
        }

        for (String ownerId : ownerId2Tasks.keySet()) {
            tasksToCreate.addAll(ownerId2Tasks.get(ownerId));
        }
        return tasksToCreate;
    }

    public void createTaskFromObjectList(Database.BatchableContext BC, Task taskData, List<Contact> whatIdObjects, LSB_MassAssignmentBatchType batchType) {

        LogWrapper logWrapper = new LogWrapper();
        List<Task> tasksToCreate = new List<Task>();
        for (Contact singleObject : whatIdObjects) {
            if (singleObject.LSB_CON_User__c == null) {
                logError(singleObject, logWrapper, MISSING_COMMUNITY_USER_RECORD);
            } else if (batchType == LSB_MassAssignmentBatchType.ADVISEE && singleObject.Cases.size() == 0) {
                logError(singleObject, logWrapper, MISSING_CASE_ADVISEE_RECORD);
            } else if (batchType == LSB_MassAssignmentBatchType.ADVISEE && singleObject.Cases.size() == 1) {
                tasksToCreate.add(createTaskForAdviseeCase(taskData, singleObject));
            } else if (batchType == LSB_MassAssignmentBatchType.SUPPORT_PROFILE && singleObject.Support_Profiles__r.size() == 0) {
                logError(singleObject, logWrapper, MISSING_SUPPORT_PROFILE_ADVISEE_RECORD);
            } else {
                tasksToCreate.add(createTaskForSupportProfile(taskData, ((Contact) singleObject)));
            }
        }

        List<Database.SaveResult> saveResults = Database.insert(tasksToCreate, false);

        createMassAssignmentLog(BC, logWrapper, whatIdObjects, saveResults);
    }

    private void logError(Contact singleObject, LogWrapper logWrapper, String message) {
        logWrapper.failed++;
        logWrapper.failedIds += singleObject.Id + '\r\n';
        logWrapper.errors += '[' + singleObject.Id + '] ' + message + '\r\n';
    }

    private void createMassAssignmentLog(Database.BatchableContext BC, LogWrapper logWrapper, List<SObject> whatIdObjects, List<Database.SaveResult> saveResults) {

        for (Integer i = 0; i < saveResults.size(); i++) {
            if (saveResults[i].isSuccess()) {
                logWrapper.completed++;
            } else {
                logWrapper.failed++;
                logWrapper.failedIds += whatIdObjects[i].Id + '\r\n';
                logWrapper.errors += '[' + whatIdObjects[i].Id + '] ' + saveResults[i].getErrors().get(0).getMessage() + '\r\n';
            }
        }

        insert new LSB_MAL_MassAssignmentLog__c(
                LSB_MAL_SubmitDate__c = Datetime.now(),
                LSB_MAL_JobId__c = BC.getJobId(),
                LSB_MAL_Submitter__c = UserInfo.getUserId(),
                LSB_MAL_Failed__c = logWrapper.failed,
                LSB_MAL_Completed__c = logWrapper.completed,
                LSB_MAL_FailedIds__c = logWrapper.failedIds,
                LSB_MAL_Errors__c = logWrapper.errors
        );
    }

    public Task createTaskForAdviseeCase(Task taskData, Contact contact) {
        Task taskToCreate = createTask(taskData, contact);
        taskToCreate.WhatId = contact.Cases[0].Id;
        return taskToCreate;
    }
    public Task createTaskForSupportProfile(Task taskData, Contact contact) {
        Task taskToCreate = createTask(taskData, contact);
        taskToCreate.WhatId = contact.Support_Profiles__r[0].Id;
        return taskToCreate;
    }


    public Task createTask(Task taskData, Contact contact) {
        Task taskToCreate = new Task(
            Subject = taskData.Subject,
            Priority = taskData.Priority,
            ActivityDate = taskData.ActivityDate,
            Description = taskData.Description,
            LSB_ACT_AssignedBy__c = taskData.LSB_ACT_AssignedBy__c,
            OwnerId = contact.LSB_CON_User__c
        );
        return taskToCreate;
    }

    private static Task createTask(
        String subject,
        String description,
        String subtype,
        String priority,
        Date dueDate,
        hed__Program_Enrollment__c programEnrollment
    ) {
       return new Task(
            Subject = subject,
            Description = description,
            LSB_ACT_TaskSubtype__c = subtype,
            Priority = priority,
            ActivityDate = dueDate,
            WhoId = programEnrollment.hed__Contact__c,
            LSB_ACT_SourceProgramEnrollment__c = programEnrollment.Id,
            OwnerId = programEnrollment.hed__Contact__r.LSB_CON_User__c
       );
    }

    public static void insertTasksForProgramEnrollments(
        List<hed__Program_Enrollment__c> programEnrollments,
        String subject,
        String description,
        String subtype,
        String priority,
        Date dueDate
    ) {
        List<Task> tasksList = new List<Task>();
        for (hed__Program_Enrollment__c programEnrollment : programEnrollments) {
            if ( String.isBlank(programEnrollment.hed__Contact__r?.LSB_CON_User__c)) {
                continue;
            }
            tasksList.add(LSB_TaskHelper.createTask(
                subject,
                description,
                subtype,
                priority,
                dueDate,
                programEnrollment
            ));
        }

        Database.insert(tasksList, false);
    }

    public class LogWrapper {
        String failedIds = '';
        String errors = '';
        Integer failed = 0, completed = 0;
    }
}