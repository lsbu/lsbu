public with sharing class LSB_PostChatPageController {

    @AuraEnabled
    public static String redirectToSurvey(String chatKey) {
        String url = LSB_SurveyHelper.getRelatedSurveyInvitationLink(chatKey);
        if (url != null) {
            return url;
        }
        return null;
    }

    @AuraEnabled
    public static ContentVersion saveTranscriptFile(String chatKey, String fileName, String fileData) {
        LiveChatTranscript chatTranscript = LSB_SurveyHelper.getLiveChatTranscript(chatKey);
        if (chatTranscript != null) {
            Id parentId = chatTranscript.CaseId;
            Blob blobData = Blob.valueOf(fileData);
            if (parentId != null) {
                return LSB_AttachmentsHelper.saveFile(parentId, fileName, blobData);
            }
        }
        return null;
    }
}