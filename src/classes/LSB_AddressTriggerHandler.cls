public without sharing class LSB_AddressTriggerHandler implements ITriggerHandler {

    private hed.TDTM_Runnable.dmlWrapper dmlWrapper;
    private LSB_AddressHelper addressHelper;

    public LSB_AddressTriggerHandler(hed.TDTM_Runnable.dmlWrapper dmlWrapper) {
        this.dmlWrapper = dmlWrapper;
        this.addressHelper = new LSB_AddressHelper();
    }

    public void beforeInsert(List<SObject> newList) {
        LSB_AddressHelper.setDefaultAddressType(newList);
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {
    }

    public void afterInsert(Map<Id, SObject> newItems) {
        Map<String, ContactPointAddress> newContactPointAddressesByExternalId = LSB_AddressHelper.syncWithContactPoints((Map<Id, hed__Address__c>) newItems, new Map<Id, hed__Address__c>());
        LSB_AddressHelper.createContactPointConsents((Map<Id, hed__Address__c>) newItems, newContactPointAddressesByExternalId);
        LSB_AddressHelper.updatePostalCodeDeprivationFlagAndAreaOfPermanentResidence(newItems.values(), null);
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        LSB_AddressHelper.syncWithContactPoints((Map<Id, hed__Address__c>) newItems, (Map<Id, hed__Address__c>) oldItems);
        LSB_AddressHelper.updatePostalCodeDeprivationFlagAndAreaOfPermanentResidence(newItems.values(), (Map<Id, hed__Address__c>) oldItems);
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
        LSB_AddressHelper.deleteContactPoints((Map<Id, hed__Address__c>) oldItems);
        LSB_AddressHelper.cleanPostalCodeDeprivationFlagAndAreaOfPermanentResidence(oldItems.values());
    }

    public void afterUndelete(Map<Id, SObject> newItems) {
    }
}