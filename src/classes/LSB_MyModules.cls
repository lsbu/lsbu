public without sharing class LSB_MyModules {

    private static final String COURSE_ENROLLMENT_MODULE_RECORD_TYPE_ID = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByDeveloperName().get(LSB_Constants.COURSE_ENROLLMENT_MODULE_RECORD_TYPE_DEVELOPER_NAME).getRecordTypeId();
    private static final String EFE_STATUS = 'EFE';

    @AuraEnabled
    public static List<String> getMyModulesNames() {
        List<String> listOfModules = new List<String>();

        List<hed__Course_Enrollment__c> moduleEnrollments = getMyModulesEnrollments();

        for (hed__Course_Enrollment__c moduleEnrollment : moduleEnrollments) {
            listOfModules.add(moduleEnrollment.LSB_CCN_ModuleInstanceName__c);
        }

        return listOfModules;
    }

    public static List<hed__Course_Enrollment__c> getMyModulesEnrollments() {
        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, ContactId FROM User WHERE Id = :userId];
        Id contactId = u.ContactId;

        List<hed__Course_Enrollment__c> moduleEnrollments = [SELECT LSB_CCN_ModuleInstanceName__c FROM hed__Course_Enrollment__c
            WHERE hed__Program_Enrollment__c IN (SELECT Id FROM hed__Program_Enrollment__c WHERE LSB_PEN_LatestEnrolmentRecord__c = TRUE AND hed__Enrollment_Status__c =: EFE_STATUS AND hed__Contact__c = : contactId)
            AND RecordTypeId = :COURSE_ENROLLMENT_MODULE_RECORD_TYPE_ID AND LSB_CCN_ModuleEnrolmentStatus__c =: EFE_STATUS];

        return moduleEnrollments;
    }
}