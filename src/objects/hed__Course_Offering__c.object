<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <allowInChatterGroups>false</allowInChatterGroups>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Joins Courses and Terms to contain information related to a single occurrence of a Course.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>LSB_COF_ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Sync MC/QL</description>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>90</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_ModuleCredit__c</fullName>
        <externalId>false</externalId>
        <label>Module Credit</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_ModuleID__c</fullName>
        <externalId>false</externalId>
        <formula>hed__Course__r.hed__Course_ID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Module ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_ModuleInstance__c</fullName>
        <externalId>false</externalId>
        <label>Module Instance</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_SessionCode__c</fullName>
        <externalId>false</externalId>
        <label>Session Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_SessionDescription__c</fullName>
        <externalId>false</externalId>
        <label>Session Description</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_SourceSystemID__c</fullName>
        <description>Sync QL</description>
        <externalId>false</externalId>
        <label>LSBU SourceSystemID</label>
        <length>90</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSB_COF_SourceSystem__c</fullName>
        <description>Sync QL</description>
        <externalId>false</externalId>
        <label>Source System</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>LSB_SourceSystem</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>hed__Course__c</fullName>
        <deprecated>false</deprecated>
        <description>Lookup to the Course record that this Course Offering is associated with.</description>
        <externalId>false</externalId>
        <label>Course</label>
        <referenceTo>hed__Course__c</referenceTo>
        <relationshipLabel>Course Offerings</relationshipLabel>
        <relationshipName>Course_Offerings</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>hed__Term__c</fullName>
        <deprecated>false</deprecated>
        <description>Lookup to the Term record that represents when this Course Offering takes place.</description>
        <externalId>false</externalId>
        <label>Term</label>
        <referenceTo>hed__Term__c</referenceTo>
        <relationshipLabel>Course Offerings</relationshipLabel>
        <relationshipName>Course_Offerings</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Course Offering</label>
    <nameField>
        <label>Course Offering ID</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Course Offerings</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
