/**
 * Created by kgromek002 on 05.08.2021.
 */

({
    // Fetches members in the same group as the logged in user and returns
    // a promise.
    // https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/js_promises.htm
    fetchGroupMembers : function(cmp) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = cmp.get('c.getGroupMembers');
            action.setCallback(this, function(response) {
                if (!cmp.isValid() || response.getState() !== 'SUCCESS') {
                    reject(response.getError());
                } else {
                    cmp.set('v.groupMembers', response.getReturnValue());
                    resolve(response.getReturnValue());
                }
            });

            $A.enqueueAction(action);
        }));
    }
})