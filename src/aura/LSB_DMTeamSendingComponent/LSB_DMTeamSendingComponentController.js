/**
 * Created by kgromek002 on 05.08.2021.
 */

({
    init: function(cmp, event, helper) {
        cmp.set('v.selectedMembers', []);
    },

    // Handler called when component is ready and receives a list of
    // members being personalized including all relevant information
    // required to customize member personalization
    messagePersonalizationReady: function(cmp, event, helper) {
        var selectedMembers = event.getParam('arguments').members;
        cmp.set('v.selectedMembers', selectedMembers);
        
        helper.fetchGroupMembers(cmp)
            .then(function(groupMembers) {
                if (selectedMembers.length) {
                    // In this example, the custom subject applies to all members
                    // so we can grab the customData from the first member
                    var data = selectedMembers[0].customData || {};


                    // Go through group members update selected value if a value
                    // was previously selected

                    for (var i=0; i<groupMembers.length; i++) {
                        if (groupMembers[i].LSB_DM_Email__c == data.customSendFromEmail) {
                            cmp.find("selectGroupMembers").set("v.value", i);
                        }
                    }
                }
                var spinner = cmp.find('mySpinner');
                $A.util.addClass(spinner, 'slds-hide');
            });
    },

    // Handles changes to send from group member drop down
    // and updates all members custom data
    handleSelectChanged: function(cmp, event) {
        var members = cmp.get('v.groupMembers'),
            selectedMembers = cmp.get('v.selectedMembers'),
            id,
            dataProvidedEvent,
            value = cmp.find('selectGroupMembers').get('v.value'),
            data;

        if (value != "") {
            cmp.set('v.sendFromValue', value);
            data = {
                customSendFromEmail: members[value].LSB_DM_Email__c,
                customSendFromName: members[value].Name,
                selectedGroup: '',
                selectedUser: '',
                usersInGroupBackup: ''
            };
        } else {
            data = {};
        }

        // Loop through each member and fire an event to update its custom
        // data with the new changes
        selectedMembers.forEach(function(member) {
            dataProvidedEvent = $A.get('e.mcdm_15:JourneyApprovalsDataProvided');
            // Depending on whether we're dealing with a campaign or quick send
            // use the campaignMemberId or fallback to the objectId which will
            // be present in all other cases.
            id = member.campaignMemberId || member.objectId;

            dataProvidedEvent.setParams({
                id: id,
                data: data
            });
            dataProvidedEvent.fire();
        });
    }
})