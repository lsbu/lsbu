/**
 * Created by kgromek002 on 23.08.2021.
 */

({
    init: function(cmp) {
        cmp.set('v.selectedMembers', []);
    },

    messagePersonalizationReady: function(cmp, event) {
        var selectedMembers = event.getParam('arguments').members;
        var customData = event.getParam('arguments').customData;
        cmp.set('v.selectedMembers', selectedMembers);

        if (selectedMembers.length) {
            var data = selectedMembers[0].customData || {};
            cmp.set('v.customSubjectLine', data.customSubjectLine || '');
        } else if (customData != null) {
            var data = customData.autosend || {};
            cmp.set('v.customSubjectLine', data.customSubjectLine || '');
        }

        var spinner = cmp.find('mySpinner');
        $A.util.addClass(spinner, 'slds-hide');
    },

    handleInputChanged: function(cmp) {
        // Get the data set from the component in the modal to pass to the
        // apex class for bulk send CPI, this will be appended to the logic
        // performed in the apex class.
        var data = {
            customSubjectLine: cmp.get('v.customSubjectLine')
        };

        var selectedMembers = cmp.get('v.selectedMembers'),
            id,
            dataProvidedEvent;

        if (selectedMembers.length) {
            selectedMembers.forEach(function(member) {
                dataProvidedEvent = $A.get('e.mcdm_15:JourneyApprovalsDataProvided');
                // Handles campaign vs quick send object IDs for preview updates
                id = member.campaignMemberId || member.objectId;
                dataProvidedEvent.setParams({
                    id: id,
                    data: data
                });
                dataProvidedEvent.fire();
            });
        } else {
            dataProvidedEvent = $A.get('e.mcdm_15:JourneyApprovalsDataProvided');
            // Supports autosend flow
            dataProvidedEvent.setParams({
                id: 'autosend',
                data: data
            });
            dataProvidedEvent.fire();
        }
    }
})