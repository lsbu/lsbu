({
    launchFlow : function (cmp, event) {
        cmp.set("v.flowLaunched", true);
        var flow = cmp.find("flowData");
        flow.startFlow("LSB_CreateSupportProfileCase");
    },

    closeModal : function (cmp, event) {
        cmp.set("v.flowLaunched", false);
    },

    statusChange : function (cmp, event) {
        if (event.getParam('status') === "FINISHED") {
            cmp.set("v.flowLaunched", false);
        } else if (event.getParam('status') === "STARTED") {
            let modalHeader = document.getElementsByClassName('slds-modal__header')[0];
            modalHeader.focus();
        }
    },

    handleOnKeyDown : function (cmp, event) {
        let modalHeader = document.getElementsByClassName('slds-modal__header')[0];
        let accessibleByTab = document.getElementsByClassName('accessibleByTab')[0];
        if (event.shiftKey && event.key === 'Tab' && event.target.className === 'slds-modal__header') {
            accessibleByTab.focus();
        } else if (!event.shiftKey && event.key === 'Tab' && event.target.className === 'accessibleByTab') {
            modalHeader.focus();
        }
    }
});