({   
    navigateToPage : function(component, event, helper) {
        var address = '/s/my-support';
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
    }
})