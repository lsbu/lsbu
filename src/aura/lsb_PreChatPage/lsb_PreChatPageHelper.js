({
    /**
     * Event which fires the function to start a chat request (by accessing the chat API component)
     *
     * @param cmp - The component for this state.
     */
    onStartButtonClick: function (component) {
        const fieldsConfiguration = component.get("v.fieldsConfiguration");

        var prechatFieldComponents = component.get("v.prechatFieldComponents");
        var apiNamesMap = this.createAPINamesMap(component.find("prechatAPI").getPrechatFields());
        var fields = this.createFieldsArray(apiNamesMap, prechatFieldComponents);

        // validation
        var allValid = true;
        var requiredSelectLists = component.find('prechatSelectField');
        var inputs = component.find('prechatField');
        var isNotStudent = !fieldsConfiguration.isStudent;

        var subject, topic;
        let subjectFieldLabel = $A.get("$Label.c.LSB_ChatCaseSubjectFieldLabel");

        if (Array.isArray(inputs)) {
            const studentFound = component.get("v.studentFound");
            const regExpEmailFormat = this.setRegExpForEmail(isNotStudent);
            allValid = inputs.reduce(function (validSoFar, inputCmp) {
                if (inputCmp.get("v.value").trim().length == 0) {
                    inputCmp.setCustomValidity("Complete this field");
                } else if (inputCmp.get("v.type") == 'inputemail' && !(inputCmp.get("v.value").match(regExpEmailFormat))) {
                    inputCmp.setCustomValidity(isNotStudent ? $A.get("$Label.c.LSB_LSBUEmailValidationErrorMessage2") : $A.get("$Label.c.LSB_LSBUEmailValidationErrorMessage"));
                } else if (inputCmp.get("v.type") == 'inputemail'
                    && inputCmp.get("v.value").match(regExpEmailFormat) && !isNotStudent && !studentFound) {
                    inputCmp.setCustomValidity($A.get("$Label.c.LSB_LSBUUserSearchErrorMessage"));
                } else {
                    inputCmp.setCustomValidity("");
                }
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        } else {
            inputs.showHelpMessageIfInvalid();
            allValid = inputs.checkValidity();
        }

        if (!Array.isArray(requiredSelectLists)) {
            var value = requiredSelectLists.get("v.value");
            if (value == undefined || value == "") {
                allValid = false;
                requiredSelectLists.set("v.errors", [{message: "Select value"}]);
                var styles = requiredSelectLists.get("v.class");
                requiredSelectList.set("v.class", "inputField slds-style-select select slds-has-error");
            } else {
                requiredSelectLists.set("v.errors", []);
            }
        } else {
            requiredSelectLists.forEach(function (field) {
                var value = field.get("v.value");
                if ((value == undefined || value == "") && field.get("v.label") != fieldsConfiguration.tellUsAboutYourselfLabel) {
                    allValid = false;
                    field.set("v.errors", [{message: "Select value"}]);
                    var styles = field.get("v.class");
                    field.set("v.class", "inputField slds-style-select select slds-has-error");
                } else {
                    field.set("v.errors", []);
                }

                if (field.get("v.label") == fieldsConfiguration.topicFieldLabel) {
                    topic = value;
                }
            });
        }

        if (!allValid) {
            component.set("v.validationSuccessful", false);
        }

        inputs.forEach(function (field) {
            if (field.get("v.label") == subjectFieldLabel) {
                subject = field.get("v.value");
            }
        });
        if (allValid && component.find("prechatAPI").validateFields(fields).valid) {
            var event = new CustomEvent(
                "setCustomFields",
                {
                    detail: {
                        callback: component.find("prechatAPI").startChat.bind(this, fields),
                        subject: subject,
                        topic: topic,
                        preChatInfo: JSON.stringify(fields)
                    }
                }
            );
            // Dispatch the event.
            document.dispatchEvent(event);
        } else {
            console.warn("Prechat fields did not pass validation!");
        }
    },

    /**
     * Create an array of field objects to start a chat from an array of pre-chat fields
     *
     * @param fields - Array of pre-chat field Objects.
     * @returns An array of field objects.
     */
    createFieldsArray: function (apiNames, fieldCmps) {
        let subjectFieldLabel = $A.get("$Label.c.LSB_ChatCaseSubjectFieldLabel");
        if (fieldCmps.length) {
            return fieldCmps.map(function (fieldCmp) {
                return {
                    label: fieldCmp.get("v.label") == subjectFieldLabel ? 'Subject' : fieldCmp.get("v.label"),
                    value: fieldCmp.get("v.value"),
                    name: apiNames[(fieldCmp.get("v.label") == subjectFieldLabel ? 'Subject' : fieldCmp.get("v.label"))]
                };
            }.bind(this));
        } else {
            return [];
        }
    },

    /**
     * Create map of field label to field API name from the pre-chat fields array.
     *
     * @param fields - Array of prechat field Objects.
     * @returns An array of field objects.
     */
    createAPINamesMap: function (fields) {
        var values = {};

        fields.forEach(function (field) {
            values[field.label] = field.name;
        });

        return values;
    },

    validateEmailAddress: function (component) {
        const fieldsConfiguration = component.get("v.fieldsConfiguration");

        var isNotStudent = !fieldsConfiguration.isStudent;
        var regExpEmailFormat = this.setRegExpForEmail(isNotStudent);
        var regularFields = component.find("prechatField");
        var picklistFields = component.find("prechatSelectField");

        let emailVal = regularFields.find(element => element.get("v.label") === fieldsConfiguration.emailLabel);
        let validationFailed = false;

        const contactRole = picklistFields.find(element => element.get("v.label") === fieldsConfiguration.tellUsAboutYourselfLabel);
        regularFields.forEach((inputCmp) => {
            if (inputCmp.get("v.type") == 'inputemail' && !(inputCmp.get("v.value").match(regExpEmailFormat))) {
                inputCmp.setCustomValidity(isNotStudent ? $A.get("$Label.c.LSB_LSBUEmailValidationErrorMessage2") : $A.get("$Label.c.LSB_LSBUEmailValidationErrorMessage"));
                validationFailed = true;
                inputCmp.showHelpMessageIfInvalid();
            } else if (inputCmp.get("v.type") == 'inputemail') {
                inputCmp.setCustomValidity("");
                inputCmp.showHelpMessageIfInvalid();
                inputCmp.checkValidity();
            }
        });

        if (validationFailed) {
            return;
        }
        var action = component.get("c.validateUser");
        action.setParam("emailValue", emailVal.get("v.value"));
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseVal = response.getReturnValue();
                if (responseVal && (contactRole.get("v.value") == responseVal.LSB_CON_ContactRole__c
                    || (fieldsConfiguration.studentContactRoleConstant == responseVal.LSB_CON_ContactRole__c))) {
                    regularFields.forEach((inputCmp) => {
                        if (inputCmp.get("v.label") == 'First Name') {
                            inputCmp.set("v.value", responseVal.FirstName);
                        }
                        if (inputCmp.get("v.label") == 'Last Name') {
                            inputCmp.set("v.value", responseVal.LastName);
                        }
                    });
                    let hcwcFields = fieldsConfiguration.sObjectsLabelsMap.UserFound;

                    this.facetRerender(component, contactRole.get("v.value"), hcwcFields);
                    component.set("v.buttonStatus", "final");
                    component.set("v.studentFound", true);
                } else {
                    component.set("v.studentFound", false);
                    if (!isNotStudent) {
                        regularFields.forEach((inputCmp) => {
                            if (inputCmp.get("v.type") == 'inputemail') {
                                inputCmp.setCustomValidity($A.get("$Label.c.LSB_LSBUUserSearchErrorMessage"));
                                inputCmp.showHelpMessageIfInvalid();
                                inputCmp.checkValidity();
                            }
                        });
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    setRegExpForEmail: function(isNotStudent) {
        const regExpEmailFormatSimple = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const regExpEmailFormat = isNotStudent ? regExpEmailFormatSimple : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@lsbu.ac.uk$/;
        return regExpEmailFormat;
    },

    /**
     * Create an array in the format $A.createComponents expects
     *
     * Example:
     * [["componentType", {attributeName: "attributeValue", ...}]]
     *
     * @param prechatFields - Array of pre-chat field Objects.
     * @returns Array that can be passed to $A.createComponents
     */
    getPrechatFieldAttributesArray: function (component, prechatFields) {
        // $A.createComponents first parameter is an array of arrays. Each array contains the type of component being created, and an Object defining the attributes.
        var prechatFieldsInfoArray = [];
        var onCommunity = false;
        let subjectFieldLabel = $A.get("$Label.c.LSB_ChatCaseSubjectFieldLabel");
        var action = component.get("c.getConfiguration");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsConfiguration = response.getReturnValue();
                component.set("v.fieldsConfiguration", fieldsConfiguration);
                component.set("v.onCommunity", fieldsConfiguration.insideCommunity);
                onCommunity = fieldsConfiguration.insideCommunity;

                if (!onCommunity) {
                    component.set("v.buttonDisabled", true);
                    component.set("v.buttonStatus", "next");
                }
                // For each field, prepare the type and attributes to pass to $A.createComponents
                prechatFields.forEach(function (field) {
                    var componentName = (field.type === "inputSplitName") ? "text" : field.type;
                    var componentInfoArray = ["lightning:input"];
                    var initialVisibility = (onCommunity
                            && (field.name == fieldsConfiguration.tellUsAboutYourselfName
                            || field.name == fieldsConfiguration.contactCaseRoleApiName
                            || field.value != null))
                        || (!onCommunity
                            && field.name != fieldsConfiguration.tellUsAboutYourselfName);


                    var attributes = {
                        "aura:id": field.type === "inputSelect" ? "prechatSelectField" : "prechatField",
                        id: field.type === "inputSelect" ? "prechatSelectField" : "prechatField",
                        required: field.required,
                        label: field.label == fieldsConfiguration.subjectFieldLabel ? subjectFieldLabel : field.label,
                        disabled: (field.label == fieldsConfiguration.topicFieldLabel && onCommunity) || field.readOnly,
                        class: (initialVisibility ? "slds-hide" : "inputField " + field.className),
                        value: field.label == fieldsConfiguration.contactRoleFieldLabel && onCommunity ?
                            fieldsConfiguration.picklistConfiguration.value : field.value
                    };

                    // Special handling for options for an input:select (picklist) component
                    if (field.type === "inputSelect" && field.picklistOptions) {
                        var picklistOptionsFinal = [];
                        picklistOptionsFinal.push({
                            "label": "",
                            "value": "",
                            "class": "optionClass noneOption",
                            "selected": "true"
                        });

                        field.picklistOptions.forEach(function (option) {
                            if (field.label == fieldsConfiguration.natureOfEnquiryFieldLabel) {
                                fieldsConfiguration.picklistConfiguration.dependentValues.forEach(function (picklistEntry) {
                                    if (picklistEntry.value == option.value) {
                                        picklistOptionsFinal.push(option);
                                        if (!onCommunity) {
                                            attributes.value = option.value;
                                            attributes.class = "slds-hide";
                                        }
                                    }
                                });
                            } else if (!onCommunity && field.label == fieldsConfiguration.topicFieldLabel) {
                                    fieldsConfiguration.availableTopicsForGuest.forEach(function (picklistEntry) {
                                    if (picklistEntry.value == option.value) {
                                        picklistOptionsFinal.push(option);
                                    }
                                });
                            } else if (!onCommunity && field.name == fieldsConfiguration.contactCaseRoleApiName) {
                                fieldsConfiguration.availableRolesWithLabels.forEach(function (picklistEntry) {
                                    if (picklistEntry.value == option.value) {
                                        option.label = picklistEntry.label;
                                        picklistOptionsFinal.push(option);
                                    }
                                });
                            } else {
                                picklistOptionsFinal.push(option);
                            }
                        });

                        componentInfoArray = ["ui:inputSelect"];
                        attributes.options = picklistOptionsFinal;

                        attributes.labelClass = (field.label == fieldsConfiguration.contactRoleFieldLabel || attributes.class == "slds-hide") ?
                            "noneOption" : "slds-form-element__label slds-no-flex";
                        attributes.change = component.getReference("c.checkValidity");
                    } else {
                        attributes.type = componentName;
                        attributes.maxlength = field.maxLength;
                    }

                    attributes.autocomplete = "off";
                    attributes.onblur = component.getReference("c.handleOnBlur");

                    // Append the attributes Object containing the required attributes to render this pre-chat field
                    componentInfoArray.push(attributes);

                    // Append this componentInfoArray to the fieldAttributesArray
                    prechatFieldsInfoArray.push(componentInfoArray);
                });
                if (!onCommunity){
                    this.sortArray(prechatFieldsInfoArray, "label", "Tell Us about Yourself");
                }
                // Make asynchronous Aura call to create pre-chat field components
                $A.createComponents(
                    prechatFieldsInfoArray,
                    function (components, status, errorMessage) {
                        if (status === "SUCCESS") {
                            component.set("v.prechatFieldComponents", components);
                        }
                    }
                );

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    facetRerender: function (component, role, fieldsToDisplay, cleanData) {
         var prechatFields = component.find("prechatAPI").getPrechatFields();
         var prechatFieldsInfoArray = [];
         var fieldsConfiguration = component.get("v.fieldsConfiguration");
         var regularFields = component.find("prechatField");
         var picklistFields = component.find("prechatSelectField");

         let subjectFieldLabel = $A.get("$Label.c.LSB_ChatCaseSubjectFieldLabel");
         let cleanDataScope = ["FirstName", "LastName"];
         prechatFields.forEach(function (field) {
             var componentName = (field.type === "inputSplitName") ? "text" : field.type;
             var componentInfoArray = ["lightning:input"];
             var initialVisibility = false;
             var currentValue = field.type == "inputSelect" ? picklistFields.find(element => element.get("v.label") === field.label) : regularFields.find(element => element.get("v.label") === field.label);
             var enqRole = fieldsConfiguration.contactRoleValuesMap[role];
             if (currentValue) {
                 currentValue = currentValue.get("v.value");
             }

             if (fieldsToDisplay) {
                 initialVisibility = fieldsToDisplay.includes(field.label);
             }

             var attributes = {
                 "aura:id": field.type === "inputSelect" ? "prechatSelectField" : "prechatField",
                 id: field.type === "inputSelect" ? "prechatSelectField" : "prechatField",
                 required: field.required,
                 label: field.label == fieldsConfiguration.subjectFieldLabel ? subjectFieldLabel : field.label,
                 disabled: field.readOnly,
                 class: initialVisibility ? "inputField " + field.className : "slds-hide",
                 value: currentValue
             };
             if (cleanData && cleanDataScope.includes(field.name)) {
                 attributes.value = "";
             }

             if (field.type === "inputSelect" && field.picklistOptions) {
                 var picklistOptionsFinal = [];

                 if (field.label != fieldsConfiguration.natureOfEnquiryFieldLabel) {
                     picklistOptionsFinal.push({
                         "label": "",
                         "value": "",
                         "class": "optionClass noneOption",
                         "selected": "true"
                     });
                 }

                 if (field.label == fieldsConfiguration.topicFieldLabel) {
                     fieldsConfiguration.picklistConfiguration.dependentValues.forEach(function (picklistEntry) {
                         if (picklistEntry.value == enqRole) {
                             picklistEntry.dependentValues.forEach(function (option) {
                                 picklistOptionsFinal.push(option);
                             });
                         }
                     });
                 }

                 field.picklistOptions.forEach(function (option) {
                     if (field.label == fieldsConfiguration.natureOfEnquiryFieldLabel) {
                         fieldsConfiguration.picklistConfiguration.dependentValues.forEach(function (picklistEntry) {
                             if (picklistEntry.value == option.value) {
                                 picklistOptionsFinal.push(option);
                                 attributes.value = option.value;
                                 attributes.class = "slds-hide";
                             }
                         });
                     } else if (field.name == fieldsConfiguration.contactCaseRoleApiName) {
                         fieldsConfiguration.availableRolesWithLabels.forEach(function (picklistEntry) {
                             if (picklistEntry.value == option.value) {
                                 option.label = picklistEntry.label;
                                 picklistOptionsFinal.push(option);
                             }
                         });
                     } else if (field.label != fieldsConfiguration.topicFieldLabel) {
                         picklistOptionsFinal.push(option);
                     }
                 });

                 if (field.label == fieldsConfiguration.natureOfEnquiryFieldLabel) {
                     attributes.value = enqRole;
                 }
                 componentInfoArray = ["ui:inputSelect"];
                 attributes.options = picklistOptionsFinal;

                 attributes.labelClass = (field.label == fieldsConfiguration.contactRoleFieldLabel || attributes.class == "slds-hide") ?
                     "noneOption" : "slds-form-element__label slds-no-flex";
                 attributes.change = component.getReference("c.checkValidity");
             } else {
                 attributes.type = componentName;
                 attributes.maxlength = field.maxLength;
             }
             attributes.autocomplete = "off";
             attributes.onblur = component.getReference("c.handleOnBlur");

             // Append the attributes Object containing the required attributes to render this pre-chat field
             componentInfoArray.push(attributes);

             // Append this componentInfoArray to the fieldAttributesArray
             prechatFieldsInfoArray.push(componentInfoArray);
         });
         this.fieldDestructor(component);
         this.sortArray(prechatFieldsInfoArray, "label", "Tell Us about Yourself");

         // Make asynchronous Aura call to create pre-chat field components
         $A.createComponents(
             prechatFieldsInfoArray,
             function (components, status, errorMessage) {
                 if (status === "SUCCESS") {
                     component.set("v.prechatFieldComponents", components);
                 }
             }
         );
     },

    fieldDestructor: function(component) {
        component.set("v.prechatFieldComponents", []);
        var regularFields = component.find("prechatField");
        var picklistFields = component.find("prechatSelectField");
        regularFields.forEach(function (field) {
            field.destroy();
        });
        picklistFields.forEach(function (picklistFields) {
            picklistFields.destroy();
        });
    },

    sortArray: function (theArray, sortProperty, sortValue) {
        theArray.sort(function(x,y){return x[1][sortProperty] == sortValue ? -1 : y[1][sortProperty] == sortValue ? 1 : 0; });
    }
});