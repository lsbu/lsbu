({
    /**
     * On initialization of this component, set the prechatFields attribute and render pre-chat fields.
     *
     * @param cmp - The component for this state.
     * @param evt - The Aura event.
     * @param hlp - The helper for this state.
     */
    onInit: function (component, evt, helper) {
        var prechatFields = component.find("prechatAPI").getPrechatFields();
        helper.getPrechatFieldAttributesArray(component, prechatFields);
    },

    /**
     * Event which fires when start button is clicked in pre-chat
     *
     * @param cmp - The component for this state.
     * @param evt - The Aura event.
     * @param hlp - The helper for this state.
     */
    handleStartButtonClick: function (component, event, helper) {
        switch (component.get("v.buttonStatus")) {
            case "final":
                helper.onStartButtonClick(component);
                break;
        }
    },

    /**
     * Event which fires on change select list field.
     *
     * @param component - The component for this state.
     * @param event - The Aura event.
     * @param helper - The helper for this state.
     */

    checkValidity: function (component, event, helper) {
        var field = event.getSource();
        var fieldsConfiguration = component.get("v.fieldsConfiguration");
        var picklistFields = component.find("prechatSelectField");

        if (field.get("v.required") && field.get("v.value") == undefined || field.get("v.value") == "") {
            field.set("v.class", "inputField slds-style-select select slds-has-error");
            field.set("v.errors", [{message: "Select value"}]);
        } else if (field.get("v.required")) {
            field.set("v.class", "inputField slds-style-select select");
            field.set("v.errors", []);
        }

        if (field.get("v.label") == fieldsConfiguration.tellUsAboutYourselfLabel) {
            if (field.get("v.value")) {
                fieldsConfiguration.isStudent = field.get("v.value") == fieldsConfiguration.studentContactRoleConstant;
                let val = field.get("v.value");
                let fieldSet = fieldsConfiguration.isStudent ? fieldsConfiguration.sObjectsLabelsMap.EmailOnly
                    : fieldsConfiguration.sObjectsLabelsMap.FullView;
                picklistFields.forEach(function (plField) {
                    if (plField.get("v.label") == fieldsConfiguration.contactRoleFieldLabel) {
                        let value = field.get("v.value");
                        plField.set("v.value", value);
                    }
                });
                component.set("v.buttonStatus", fieldsConfiguration.isStudent ? "validate" : "final");
                component.set("v.buttonDisabled", false);
                helper.facetRerender(component, val, fieldSet, true);
            }
        }

        if (field.get("v.label") == fieldsConfiguration.natureOfEnquiryFieldLabel) {
            var natureOfEnquiry = field.get("v.value");
            var availableTopics;

            fieldsConfiguration.picklistConfiguration.dependentValues.forEach(function (picklistEntry) {
                if (picklistEntry.value == natureOfEnquiry) {
                    availableTopics = picklistEntry.dependentValues;
                }
            });

            if (availableTopics !== undefined) {
                picklistFields.forEach(function (field) {
                    if (field.get("v.label") == fieldsConfiguration.topicFieldLabel) {
                        var picklistOptionsFinal = [];
                        picklistOptionsFinal.push({
                            "label": "",
                            "value": "",
                            "class": "optionClass noneOption",
                            "selected": "true"
                        });
                        availableTopics.forEach(function (picklistValue) {
                            picklistOptionsFinal.push({
                                "label": picklistValue.label,
                                "value": picklistValue.value,
                                "class": "optionClass"
                            });
                        });
                        field.set("v.options", picklistOptionsFinal);
                        field.set("v.disabled", false);
                    }
                });
            }

        }
    },

    handleOnKeyPress: function (component, event, helper) {
        if (event.which === 13) {
            switch (component.get("v.buttonStatus")) {
                case "final":
                    helper.onStartButtonClick(component);
                    break;
                case "validate":
                    helper.validateEmailAddress(component);
                    break;
            }
        }
    },

    handleOnBlur: function (component, event, helper) {
        var field = event.getSource();
        var fieldsConfiguration = component.get("v.fieldsConfiguration");
        if (field.get("v.label") == fieldsConfiguration.emailLabel) {
            let val = field.get("v.value");
            helper.validateEmailAddress(component);
        }
    }
});