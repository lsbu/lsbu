({
    init: function(cmp, event, helper) {
        cmp.set('v.selectedMembers', []);
    },

    // Handler called when component is ready and receives a list of
    // members being personalized including all relevant information
    // required to customize member personalization
    messagePersonalizationReady: function(cmp, event, helper) {
        var selectedMembers = event.getParam('arguments').members;
        cmp.set('v.selectedMembers', selectedMembers);

        helper.fetchGroupMembers(cmp)
            .then(function(groupMembers) {
                if (selectedMembers.length) {
                    // In this example, the custom subject applies to all members
                    // so we can grab the customData from the first member
                    var data = selectedMembers[0].customData || {};
                    cmp.find("selectGroup").set("v.value", data.selectedGroup);
                    cmp.find("selectGroupMembers").set("v.value", data.selectedUser);
                    cmp.set("v.selectedUserId", data.selectedUser);
                    if (data.usersInGroupBackup) {
                        cmp.set("v.usersInGroup", JSON.parse(data.usersInGroupBackup));
                    }
                }
                var spinner = cmp.find('mySpinner');
                $A.util.addClass(spinner, 'slds-hide');
            });
    },

    handleGroupChanged: function (cmp, event) {
        let _usersInGroup = [];
        let groupName = cmp.get('v.groupName');
        _usersInGroup = cmp.get('v.groupMembers').find(item => item.Name === groupName);
        cmp.set('v.usersInGroup', _usersInGroup.Users);
    },

    // Handles changes to send from group member drop down
    // and updates all members custom data
    handleSelectChanged: function(cmp, event) {
        let _usersInGroup = [];
        cmp.set('v.usersInGroup', _usersInGroup);
        let groupName = cmp.get('v.groupName');
        cmp.set('v.groupNameBackup', groupName);
        _usersInGroup = cmp.get('v.groupMembers').find(item => item.Name === groupName);
        cmp.set('v.usersInGroup', _usersInGroup.Users);
    },

    handleUserChanged: function(cmp, event) {
        let userId = cmp.get('v.selectedUserId');
        let user = cmp.get('v.usersInGroup').find(item => item.Id === userId);

        var members = cmp.get('v.usersInGroup'),
            selectedMembers = cmp.get('v.selectedMembers'),
            id,
            dataProvidedEvent,
            value = cmp.find('selectGroupMembers').get('v.value'),
            data;
        
        if (value != "") {
            cmp.set('v.sendFromValue', value);
            data = {
                customSendFromEmail: user.Email,
                customSendFromName: user.Name,
                selectedGroup: cmp.get('v.groupName'),
                selectedUser: cmp.get('v.selectedUserId'),
                usersInGroupBackup: JSON.stringify(cmp.get('v.usersInGroup'))

            };
            
        } else {
            data = {};
        }


        // Loop through each member and fire an event to update its custom
        // data with the new changes
        selectedMembers.forEach(function(member) {
            dataProvidedEvent = $A.get('e.mcdm_15:JourneyApprovalsDataProvided');
            // Depending on whether we're dealing with a campaign or quick send
            // use the campaignMemberId or fallback to the objectId which will
            // be present in all other cases.
            id = member.campaignMemberId || member.objectId;
            dataProvidedEvent.setParams({
                id: id,
                data: data
            });
            dataProvidedEvent.fire();
        });

    }
})