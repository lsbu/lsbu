/**
 * Created by kgromek002 on 10.08.2021.
 */

({
    // Fetches members in the same group as the logged in user and returns
    // a promise.
    // https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/js_promises.htm
    fetchGroupMembers : function(cmp) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = cmp.get('c.getAllUsersFromChildGroups');
            action.setParams({parentGroupName:'LSB_DistributedMarketingTeams'});
            action.setCallback(this, function(response) {
                if (!cmp.isValid() || response.getState() !== 'SUCCESS') {
                    reject(response.getError());
                } else {
                    let _groupMembers = [];
                    for (let item of response.getReturnValue()) {
                        _groupMembers.push({
                            Name: item.name,
                            Users: item.userList
                        });
                    }

                    cmp.set('v.groupMembers', _groupMembers);


                    // cmp.set('v.groupMembers', response.getReturnValue());
                    resolve(response.getReturnValue());
                }
            });

            $A.enqueueAction(action);
        }));
    }
})