/**
 * Created by ociszek001 on 10.02.2021.
 */

({
    handleDeflectionEvent: function (component, event, helper) {
        var appEvent = $A.get("e.selfService:caseCreateFieldChange");
        appEvent.setParams({
            "modifiedField": "Description",
            "modifiedFieldValue": event.getParam('descriptionValue')
        });
        appEvent.fire();
    }
});