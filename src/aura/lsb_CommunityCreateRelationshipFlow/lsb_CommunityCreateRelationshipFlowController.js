({
    launchFlow : function (cmp, event) {
        cmp.set("v.flowLaunched", true);
        var flow = cmp.find("flowData");
        flow.startFlow("CreateRelationshipFlow");
    },

    closeModal : function (cmp, event) {
        cmp.set("v.flowLaunched", false);
    },

    statusChange : function (cmp, event) {
        if (event.getParam('status') === "FINISHED") {
            cmp.set("v.flowLaunched", false);
        }
    }
});