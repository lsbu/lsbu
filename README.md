# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository stores metadata, code and configuration for deployment for LSBU Salesforce project.

Version: 1.0

### How do I get set up? ###

1. [Branch](https://support.atlassian.com/bitbucket-cloud/docs/branch-a-repository/) this repo - create a feature/bugfix/hotfix branch according to our branching model and your current work.
2. Clone your branch locally: git clone https://<git_username>@bitbucket.org/lsbu/lsbu.git or using git client like SourceTree.
3. Commit your changes
4. Push to the remote branch
5. Create new Pull Request to *source* branch (as described in our branching strategy)

### Contribution guidelines ###

1. Remember about at least 85% code coverage of your code.
2. Before merging to one of the main branches, code review is required. It will be done based on our internal standards.

### Who do I talk to? ###

Admin: Ewa Latoszek (ewa.latoszek@pwc.com)